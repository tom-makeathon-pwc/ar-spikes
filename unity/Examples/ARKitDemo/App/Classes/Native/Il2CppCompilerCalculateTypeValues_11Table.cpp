﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t3034683441;
// System.Security.Cryptography.OidCollection
struct OidCollection_t2127506323;
// System.Collections.ArrayList
struct ArrayList_t1294140779;
// System.Collections.IEnumerator
struct IEnumerator_t1842762036;
// System.Collections.IDictionary
struct IDictionary_t2298573854;
// System.UInt16[]
struct UInt16U5BU5D_t516420673;
// System.String[]
struct StringU5BU5D_t369357837;
// System.Text.RegularExpressions.MRUList/Node
struct Node_t2575764264;
// System.Collections.Hashtable
struct Hashtable_t3677817270;
// System.Text.RegularExpressions.MRUList
struct MRUList_t1460135803;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1075250625;
// System.Text.RegularExpressions.Match
struct Match_t3699055374;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1430738153;
// System.String
struct String_t;
// System.Security.Cryptography.Oid
struct Oid_t2597199105;
// System.Byte[]
struct ByteU5BU5D_t3287329517;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3491703694;
// System.Collections.IList
struct IList_t2170747348;
// System.Version
struct Version_t1723234366;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t1759804909;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t3835006551;
// System.Collections.IComparer
struct IComparer_t1271067330;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1922660610;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1791566554;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2757139860;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t3002548650;
// System.Uri
struct Uri_t2779109183;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2184608466;
// System.Net.ICredentials
struct ICredentials_t3839923688;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t3837984319;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t2608258608;
// System.Char[]
struct CharU5BU5D_t83643201;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t2949493571;
// System.Collections.Stack
struct Stack_t1147195501;
// System.Int32[]
struct Int32U5BU5D_t3565237794;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t1282494621;
// System.Void
struct Void_t2642135423;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct X509ExtensionCollection_t3540009410;
// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1522942069;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct X500DistinguishedName_t2418891383;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t3002548651;
// System.Text.RegularExpressions.QuickSearch
struct QuickSearch_t2754904521;
// System.Text.RegularExpressions.Interpreter/RepeatContext
struct RepeatContext_t2609418170;
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t3068193751;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t3562104089;
// System.Boolean[]
struct BooleanU5BU5D_t2577425931;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t975501551;
// System.Text.RegularExpressions.Regex
struct Regex_t2687244226;
// System.Text.RegularExpressions.IMachine
struct IMachine_t2110016686;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t3084121107;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t491885043;
// System.Net.IWebProxy
struct IWebProxy_t2500706654;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t1294012727;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2804961572;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1517963221;
// Mono.Security.X509.X509Store
struct X509Store_t703944648;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct X509ChainElementCollection_t762555534;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct X509ChainPolicy_t1059945142;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t2565515437;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_t2233104543;
// System.Security.Cryptography.X509Certificates.X509Store
struct X509Store_t754901440;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t770297212;
// System.Text.RegularExpressions.FactoryCache
struct FactoryCache_t3371693116;
// System.Text.RegularExpressions.IMachineFactory
struct IMachineFactory_t294360644;
// System.IAsyncResult
struct IAsyncResult_t614244269;
// System.AsyncCallback
struct AsyncCallback_t869574496;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3590496811;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1238597439;
// System.Net.ServicePoint
struct ServicePoint_t3316975054;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CAPTURECOLLECTION_T1282494621_H
#define CAPTURECOLLECTION_T1282494621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CaptureCollection
struct  CaptureCollection_t1282494621  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Capture[] System.Text.RegularExpressions.CaptureCollection::list
	CaptureU5BU5D_t3034683441* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CaptureCollection_t1282494621, ___list_0)); }
	inline CaptureU5BU5D_t3034683441* get_list_0() const { return ___list_0; }
	inline CaptureU5BU5D_t3034683441** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(CaptureU5BU5D_t3034683441* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECOLLECTION_T1282494621_H
#ifndef OIDENUMERATOR_T2140332956_H
#define OIDENUMERATOR_T2140332956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_t2140332956  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::_collection
	OidCollection_t2127506323 * ____collection_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__collection_0() { return static_cast<int32_t>(offsetof(OidEnumerator_t2140332956, ____collection_0)); }
	inline OidCollection_t2127506323 * get__collection_0() const { return ____collection_0; }
	inline OidCollection_t2127506323 ** get_address_of__collection_0() { return &____collection_0; }
	inline void set__collection_0(OidCollection_t2127506323 * value)
	{
		____collection_0 = value;
		Il2CppCodeGenWriteBarrier((&____collection_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(OidEnumerator_t2140332956, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDENUMERATOR_T2140332956_H
#ifndef REPEATCONTEXT_T2609418170_H
#define REPEATCONTEXT_T2609418170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter/RepeatContext
struct  RepeatContext_t2609418170  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::start
	int32_t ___start_0;
	// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::min
	int32_t ___min_1;
	// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::max
	int32_t ___max_2;
	// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::lazy
	bool ___lazy_3;
	// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::expr_pc
	int32_t ___expr_pc_4;
	// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter/RepeatContext::previous
	RepeatContext_t2609418170 * ___previous_5;
	// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_lazy_3() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___lazy_3)); }
	inline bool get_lazy_3() const { return ___lazy_3; }
	inline bool* get_address_of_lazy_3() { return &___lazy_3; }
	inline void set_lazy_3(bool value)
	{
		___lazy_3 = value;
	}

	inline static int32_t get_offset_of_expr_pc_4() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___expr_pc_4)); }
	inline int32_t get_expr_pc_4() const { return ___expr_pc_4; }
	inline int32_t* get_address_of_expr_pc_4() { return &___expr_pc_4; }
	inline void set_expr_pc_4(int32_t value)
	{
		___expr_pc_4 = value;
	}

	inline static int32_t get_offset_of_previous_5() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___previous_5)); }
	inline RepeatContext_t2609418170 * get_previous_5() const { return ___previous_5; }
	inline RepeatContext_t2609418170 ** get_address_of_previous_5() { return &___previous_5; }
	inline void set_previous_5(RepeatContext_t2609418170 * value)
	{
		___previous_5 = value;
		Il2CppCodeGenWriteBarrier((&___previous_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(RepeatContext_t2609418170, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPEATCONTEXT_T2609418170_H
#ifndef X509CHAINELEMENTCOLLECTION_T762555534_H
#define X509CHAINELEMENTCOLLECTION_T762555534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct  X509ChainElementCollection_t762555534  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ChainElementCollection::_list
	ArrayList_t1294140779 * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ChainElementCollection_t762555534, ____list_0)); }
	inline ArrayList_t1294140779 * get__list_0() const { return ____list_0; }
	inline ArrayList_t1294140779 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t1294140779 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTCOLLECTION_T762555534_H
#ifndef X509CHAINELEMENTENUMERATOR_T3654624516_H
#define X509CHAINELEMENTENUMERATOR_T3654624516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct  X509ChainElementEnumerator_t3654624516  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ChainElementEnumerator_t3654624516, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTENUMERATOR_T3654624516_H
#ifndef PATTERNCOMPILER_T295022667_H
#define PATTERNCOMPILER_T295022667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_t295022667  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t1294140779 * ___pgm_0;

public:
	inline static int32_t get_offset_of_pgm_0() { return static_cast<int32_t>(offsetof(PatternCompiler_t295022667, ___pgm_0)); }
	inline ArrayList_t1294140779 * get_pgm_0() const { return ___pgm_0; }
	inline ArrayList_t1294140779 ** get_address_of_pgm_0() { return &___pgm_0; }
	inline void set_pgm_0(ArrayList_t1294140779 * value)
	{
		___pgm_0 = value;
		Il2CppCodeGenWriteBarrier((&___pgm_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATTERNCOMPILER_T295022667_H
#ifndef X509EXTENSIONCOLLECTION_T3540009410_H
#define X509EXTENSIONCOLLECTION_T3540009410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_t3540009410  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t1294140779 * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t3540009410, ____list_0)); }
	inline ArrayList_t1294140779 * get__list_0() const { return ____list_0; }
	inline ArrayList_t1294140779 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t1294140779 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T3540009410_H
#ifndef X509EXTENSIONENUMERATOR_T3667217117_H
#define X509EXTENSIONENUMERATOR_T3667217117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
struct  X509ExtensionEnumerator_t3667217117  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ExtensionEnumerator_t3667217117, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONENUMERATOR_T3667217117_H
#ifndef INTERPRETERFACTORY_T2435289893_H
#define INTERPRETERFACTORY_T2435289893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.InterpreterFactory
struct  InterpreterFactory_t2435289893  : public RuntimeObject
{
public:
	// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::mapping
	RuntimeObject* ___mapping_0;
	// System.UInt16[] System.Text.RegularExpressions.InterpreterFactory::pattern
	UInt16U5BU5D_t516420673* ___pattern_1;
	// System.String[] System.Text.RegularExpressions.InterpreterFactory::namesMapping
	StringU5BU5D_t369357837* ___namesMapping_2;
	// System.Int32 System.Text.RegularExpressions.InterpreterFactory::gap
	int32_t ___gap_3;

public:
	inline static int32_t get_offset_of_mapping_0() { return static_cast<int32_t>(offsetof(InterpreterFactory_t2435289893, ___mapping_0)); }
	inline RuntimeObject* get_mapping_0() const { return ___mapping_0; }
	inline RuntimeObject** get_address_of_mapping_0() { return &___mapping_0; }
	inline void set_mapping_0(RuntimeObject* value)
	{
		___mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_0), value);
	}

	inline static int32_t get_offset_of_pattern_1() { return static_cast<int32_t>(offsetof(InterpreterFactory_t2435289893, ___pattern_1)); }
	inline UInt16U5BU5D_t516420673* get_pattern_1() const { return ___pattern_1; }
	inline UInt16U5BU5D_t516420673** get_address_of_pattern_1() { return &___pattern_1; }
	inline void set_pattern_1(UInt16U5BU5D_t516420673* value)
	{
		___pattern_1 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_1), value);
	}

	inline static int32_t get_offset_of_namesMapping_2() { return static_cast<int32_t>(offsetof(InterpreterFactory_t2435289893, ___namesMapping_2)); }
	inline StringU5BU5D_t369357837* get_namesMapping_2() const { return ___namesMapping_2; }
	inline StringU5BU5D_t369357837** get_address_of_namesMapping_2() { return &___namesMapping_2; }
	inline void set_namesMapping_2(StringU5BU5D_t369357837* value)
	{
		___namesMapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___namesMapping_2), value);
	}

	inline static int32_t get_offset_of_gap_3() { return static_cast<int32_t>(offsetof(InterpreterFactory_t2435289893, ___gap_3)); }
	inline int32_t get_gap_3() const { return ___gap_3; }
	inline int32_t* get_address_of_gap_3() { return &___gap_3; }
	inline void set_gap_3(int32_t value)
	{
		___gap_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPRETERFACTORY_T2435289893_H
#ifndef LINKREF_T3851818329_H
#define LINKREF_T3851818329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkRef
struct  LinkRef_t3851818329  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKREF_T3851818329_H
#ifndef CATEGORYUTILS_T2595343782_H
#define CATEGORYUTILS_T2595343782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CategoryUtils
struct  CategoryUtils_t2595343782  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYUTILS_T2595343782_H
#ifndef NODE_T2575764264_H
#define NODE_T2575764264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MRUList/Node
struct  Node_t2575764264  : public RuntimeObject
{
public:
	// System.Object System.Text.RegularExpressions.MRUList/Node::value
	RuntimeObject * ___value_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList/Node::previous
	Node_t2575764264 * ___previous_1;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList/Node::next
	Node_t2575764264 * ___next_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Node_t2575764264, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_previous_1() { return static_cast<int32_t>(offsetof(Node_t2575764264, ___previous_1)); }
	inline Node_t2575764264 * get_previous_1() const { return ___previous_1; }
	inline Node_t2575764264 ** get_address_of_previous_1() { return &___previous_1; }
	inline void set_previous_1(Node_t2575764264 * value)
	{
		___previous_1 = value;
		Il2CppCodeGenWriteBarrier((&___previous_1), value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Node_t2575764264, ___next_2)); }
	inline Node_t2575764264 * get_next_2() const { return ___next_2; }
	inline Node_t2575764264 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(Node_t2575764264 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T2575764264_H
#ifndef MRULIST_T1460135803_H
#define MRULIST_T1460135803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MRUList
struct  MRUList_t1460135803  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::head
	Node_t2575764264 * ___head_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::tail
	Node_t2575764264 * ___tail_1;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(MRUList_t1460135803, ___head_0)); }
	inline Node_t2575764264 * get_head_0() const { return ___head_0; }
	inline Node_t2575764264 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(Node_t2575764264 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((&___head_0), value);
	}

	inline static int32_t get_offset_of_tail_1() { return static_cast<int32_t>(offsetof(MRUList_t1460135803, ___tail_1)); }
	inline Node_t2575764264 * get_tail_1() const { return ___tail_1; }
	inline Node_t2575764264 ** get_address_of_tail_1() { return &___tail_1; }
	inline void set_tail_1(Node_t2575764264 * value)
	{
		___tail_1 = value;
		Il2CppCodeGenWriteBarrier((&___tail_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRULIST_T1460135803_H
#ifndef FACTORYCACHE_T3371693116_H
#define FACTORYCACHE_T3371693116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.FactoryCache
struct  FactoryCache_t3371693116  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.FactoryCache::capacity
	int32_t ___capacity_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.FactoryCache::factories
	Hashtable_t3677817270 * ___factories_1;
	// System.Text.RegularExpressions.MRUList System.Text.RegularExpressions.FactoryCache::mru_list
	MRUList_t1460135803 * ___mru_list_2;

public:
	inline static int32_t get_offset_of_capacity_0() { return static_cast<int32_t>(offsetof(FactoryCache_t3371693116, ___capacity_0)); }
	inline int32_t get_capacity_0() const { return ___capacity_0; }
	inline int32_t* get_address_of_capacity_0() { return &___capacity_0; }
	inline void set_capacity_0(int32_t value)
	{
		___capacity_0 = value;
	}

	inline static int32_t get_offset_of_factories_1() { return static_cast<int32_t>(offsetof(FactoryCache_t3371693116, ___factories_1)); }
	inline Hashtable_t3677817270 * get_factories_1() const { return ___factories_1; }
	inline Hashtable_t3677817270 ** get_address_of_factories_1() { return &___factories_1; }
	inline void set_factories_1(Hashtable_t3677817270 * value)
	{
		___factories_1 = value;
		Il2CppCodeGenWriteBarrier((&___factories_1), value);
	}

	inline static int32_t get_offset_of_mru_list_2() { return static_cast<int32_t>(offsetof(FactoryCache_t3371693116, ___mru_list_2)); }
	inline MRUList_t1460135803 * get_mru_list_2() const { return ___mru_list_2; }
	inline MRUList_t1460135803 ** get_address_of_mru_list_2() { return &___mru_list_2; }
	inline void set_mru_list_2(MRUList_t1460135803 * value)
	{
		___mru_list_2 = value;
		Il2CppCodeGenWriteBarrier((&___mru_list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYCACHE_T3371693116_H
#ifndef ENUMERATOR_T197996776_H
#define ENUMERATOR_T197996776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct  Enumerator_t197996776  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection/Enumerator::index
	int32_t ___index_0;
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchCollection/Enumerator::coll
	MatchCollection_t1075250625 * ___coll_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Enumerator_t197996776, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_coll_1() { return static_cast<int32_t>(offsetof(Enumerator_t197996776, ___coll_1)); }
	inline MatchCollection_t1075250625 * get_coll_1() const { return ___coll_1; }
	inline MatchCollection_t1075250625 ** get_address_of_coll_1() { return &___coll_1; }
	inline void set_coll_1(MatchCollection_t1075250625 * value)
	{
		___coll_1 = value;
		Il2CppCodeGenWriteBarrier((&___coll_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T197996776_H
#ifndef MATCHCOLLECTION_T1075250625_H
#define MATCHCOLLECTION_T1075250625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t1075250625  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t3699055374 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t1294140779 * ___list_1;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(MatchCollection_t1075250625, ___current_0)); }
	inline Match_t3699055374 * get_current_0() const { return ___current_0; }
	inline Match_t3699055374 ** get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(Match_t3699055374 * value)
	{
		___current_0 = value;
		Il2CppCodeGenWriteBarrier((&___current_0), value);
	}

	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(MatchCollection_t1075250625, ___list_1)); }
	inline ArrayList_t1294140779 * get_list_1() const { return ___list_1; }
	inline ArrayList_t1294140779 ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(ArrayList_t1294140779 * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier((&___list_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_T1075250625_H
#ifndef GROUPCOLLECTION_T3084121107_H
#define GROUPCOLLECTION_T3084121107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t3084121107  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::list
	GroupU5BU5D_t1430738153* ___list_0;
	// System.Int32 System.Text.RegularExpressions.GroupCollection::gap
	int32_t ___gap_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(GroupCollection_t3084121107, ___list_0)); }
	inline GroupU5BU5D_t1430738153* get_list_0() const { return ___list_0; }
	inline GroupU5BU5D_t1430738153** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(GroupU5BU5D_t1430738153* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_gap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t3084121107, ___gap_1)); }
	inline int32_t get_gap_1() const { return ___gap_1; }
	inline int32_t* get_address_of_gap_1() { return &___gap_1; }
	inline void set_gap_1(int32_t value)
	{
		___gap_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_T3084121107_H
#ifndef CAPTURE_T3723888528_H
#define CAPTURE_T3723888528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t3723888528  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Capture::index
	int32_t ___index_0;
	// System.Int32 System.Text.RegularExpressions.Capture::length
	int32_t ___length_1;
	// System.String System.Text.RegularExpressions.Capture::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Capture_t3723888528, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Capture_t3723888528, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Capture_t3723888528, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T3723888528_H
#ifndef ASNENCODEDDATA_T2608258608_H
#define ASNENCODEDDATA_T2608258608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t2608258608  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t2597199105 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t3287329517* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t2608258608, ____oid_0)); }
	inline Oid_t2597199105 * get__oid_0() const { return ____oid_0; }
	inline Oid_t2597199105 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_t2597199105 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t2608258608, ____raw_1)); }
	inline ByteU5BU5D_t3287329517* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_t3287329517** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_t3287329517* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

struct AsnEncodedData_t2608258608_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapA_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_2() { return static_cast<int32_t>(offsetof(AsnEncodedData_t2608258608_StaticFields, ___U3CU3Ef__switchU24mapA_2)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapA_2() const { return ___U3CU3Ef__switchU24mapA_2; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapA_2() { return &___U3CU3Ef__switchU24mapA_2; }
	inline void set_U3CU3Ef__switchU24mapA_2(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapA_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T2608258608_H
#ifndef OID_T2597199105_H
#define OID_T2597199105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Oid
struct  Oid_t2597199105  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.Oid::_value
	String_t* ____value_0;
	// System.String System.Security.Cryptography.Oid::_name
	String_t* ____name_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(Oid_t2597199105, ____value_0)); }
	inline String_t* get__value_0() const { return ____value_0; }
	inline String_t** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(String_t* value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(Oid_t2597199105, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}
};

struct Oid_t2597199105_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Oid::<>f__switch$map10
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24map10_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_2() { return static_cast<int32_t>(offsetof(Oid_t2597199105_StaticFields, ___U3CU3Ef__switchU24map10_2)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24map10_2() const { return ___U3CU3Ef__switchU24map10_2; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24map10_2() { return &___U3CU3Ef__switchU24map10_2; }
	inline void set_U3CU3Ef__switchU24map10_2(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24map10_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map10_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_T2597199105_H
#ifndef OIDCOLLECTION_T2127506323_H
#define OIDCOLLECTION_T2127506323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidCollection
struct  OidCollection_t2127506323  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::_list
	ArrayList_t1294140779 * ____list_0;
	// System.Boolean System.Security.Cryptography.OidCollection::_readOnly
	bool ____readOnly_1;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(OidCollection_t2127506323, ____list_0)); }
	inline ArrayList_t1294140779 * get__list_0() const { return ____list_0; }
	inline ArrayList_t1294140779 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t1294140779 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}

	inline static int32_t get_offset_of__readOnly_1() { return static_cast<int32_t>(offsetof(OidCollection_t2127506323, ____readOnly_1)); }
	inline bool get__readOnly_1() const { return ____readOnly_1; }
	inline bool* get_address_of__readOnly_1() { return &____readOnly_1; }
	inline void set__readOnly_1(bool value)
	{
		____readOnly_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDCOLLECTION_T2127506323_H
#ifndef X509CERTIFICATEENUMERATOR_T2456269188_H
#define X509CERTIFICATEENUMERATOR_T2456269188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_t2456269188  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_t2456269188, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_T2456269188_H
#ifndef INTERVALCOLLECTION_T621877006_H
#define INTERVALCOLLECTION_T621877006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection
struct  IntervalCollection_t621877006  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Text.RegularExpressions.IntervalCollection::intervals
	ArrayList_t1294140779 * ___intervals_0;

public:
	inline static int32_t get_offset_of_intervals_0() { return static_cast<int32_t>(offsetof(IntervalCollection_t621877006, ___intervals_0)); }
	inline ArrayList_t1294140779 * get_intervals_0() const { return ___intervals_0; }
	inline ArrayList_t1294140779 ** get_address_of_intervals_0() { return &___intervals_0; }
	inline void set_intervals_0(ArrayList_t1294140779 * value)
	{
		___intervals_0 = value;
		Il2CppCodeGenWriteBarrier((&___intervals_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERVALCOLLECTION_T621877006_H
#ifndef BASEMACHINE_T3233935325_H
#define BASEMACHINE_T3233935325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.BaseMachine
struct  BaseMachine_t3233935325  : public RuntimeObject
{
public:
	// System.Boolean System.Text.RegularExpressions.BaseMachine::needs_groups_or_captures
	bool ___needs_groups_or_captures_0;

public:
	inline static int32_t get_offset_of_needs_groups_or_captures_0() { return static_cast<int32_t>(offsetof(BaseMachine_t3233935325, ___needs_groups_or_captures_0)); }
	inline bool get_needs_groups_or_captures_0() const { return ___needs_groups_or_captures_0; }
	inline bool* get_address_of_needs_groups_or_captures_0() { return &___needs_groups_or_captures_0; }
	inline void set_needs_groups_or_captures_0(bool value)
	{
		___needs_groups_or_captures_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMACHINE_T3233935325_H
#ifndef ENUMERATOR_T2950357022_H
#define ENUMERATOR_T2950357022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection/Enumerator
struct  Enumerator_t2950357022  : public RuntimeObject
{
public:
	// System.Collections.IList System.Text.RegularExpressions.IntervalCollection/Enumerator::list
	RuntimeObject* ___list_0;
	// System.Int32 System.Text.RegularExpressions.IntervalCollection/Enumerator::ptr
	int32_t ___ptr_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2950357022, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_ptr_1() { return static_cast<int32_t>(offsetof(Enumerator_t2950357022, ___ptr_1)); }
	inline int32_t get_ptr_1() const { return ___ptr_1; }
	inline int32_t* get_address_of_ptr_1() { return &___ptr_1; }
	inline void set_ptr_1(int32_t value)
	{
		___ptr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2950357022_H
#ifndef GLOBALPROXYSELECTION_T3979368509_H
#define GLOBALPROXYSELECTION_T3979368509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalProxySelection
struct  GlobalProxySelection_t3979368509  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALPROXYSELECTION_T3979368509_H
#ifndef HTTPREQUESTCREATOR_T2740415761_H
#define HTTPREQUESTCREATOR_T2740415761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpRequestCreator
struct  HttpRequestCreator_t2740415761  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTCREATOR_T2740415761_H
#ifndef HTTPVERSION_T2726102792_H
#define HTTPVERSION_T2726102792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpVersion
struct  HttpVersion_t2726102792  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t2726102792_StaticFields
{
public:
	// System.Version System.Net.HttpVersion::Version10
	Version_t1723234366 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t1723234366 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t2726102792_StaticFields, ___Version10_0)); }
	inline Version_t1723234366 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t1723234366 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t1723234366 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t2726102792_StaticFields, ___Version11_1)); }
	inline Version_t1723234366 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t1723234366 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t1723234366 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T2726102792_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T1369902973_H
#define NAMEOBJECTCOLLECTIONBASE_T1369902973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t1369902973  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t3677817270 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t1759804909 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t1294140779 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t1922660610 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t1791566554 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_ItemsContainer_0)); }
	inline Hashtable_t3677817270 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t3677817270 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t3677817270 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_NullKeyItem_1)); }
	inline _Item_t1759804909 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t1759804909 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t1759804909 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_ItemsArray_2)); }
	inline ArrayList_t1294140779 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t1294140779 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t1294140779 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___infoCopy_7)); }
	inline SerializationInfo_t1922660610 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t1922660610 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t1922660610 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___keyscoll_8)); }
	inline KeysCollection_t1791566554 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t1791566554 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t1791566554 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t1369902973, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T1369902973_H
#ifndef VALUETYPE_T1364887298_H
#define VALUETYPE_T1364887298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1364887298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_com
{
};
#endif // VALUETYPE_T1364887298_H
#ifndef IPV6ADDRESS_T987378652_H
#define IPV6ADDRESS_T987378652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6Address
struct  IPv6Address_t987378652  : public RuntimeObject
{
public:
	// System.UInt16[] System.Net.IPv6Address::address
	UInt16U5BU5D_t516420673* ___address_0;
	// System.Int32 System.Net.IPv6Address::prefixLength
	int32_t ___prefixLength_1;
	// System.Int64 System.Net.IPv6Address::scopeId
	int64_t ___scopeId_2;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6Address_t987378652, ___address_0)); }
	inline UInt16U5BU5D_t516420673* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t516420673** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t516420673* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_prefixLength_1() { return static_cast<int32_t>(offsetof(IPv6Address_t987378652, ___prefixLength_1)); }
	inline int32_t get_prefixLength_1() const { return ___prefixLength_1; }
	inline int32_t* get_address_of_prefixLength_1() { return &___prefixLength_1; }
	inline void set_prefixLength_1(int32_t value)
	{
		___prefixLength_1 = value;
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(IPv6Address_t987378652, ___scopeId_2)); }
	inline int64_t get_scopeId_2() const { return ___scopeId_2; }
	inline int64_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int64_t value)
	{
		___scopeId_2 = value;
	}
};

struct IPv6Address_t987378652_StaticFields
{
public:
	// System.Net.IPv6Address System.Net.IPv6Address::Loopback
	IPv6Address_t987378652 * ___Loopback_3;
	// System.Net.IPv6Address System.Net.IPv6Address::Unspecified
	IPv6Address_t987378652 * ___Unspecified_4;

public:
	inline static int32_t get_offset_of_Loopback_3() { return static_cast<int32_t>(offsetof(IPv6Address_t987378652_StaticFields, ___Loopback_3)); }
	inline IPv6Address_t987378652 * get_Loopback_3() const { return ___Loopback_3; }
	inline IPv6Address_t987378652 ** get_address_of_Loopback_3() { return &___Loopback_3; }
	inline void set_Loopback_3(IPv6Address_t987378652 * value)
	{
		___Loopback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_3), value);
	}

	inline static int32_t get_offset_of_Unspecified_4() { return static_cast<int32_t>(offsetof(IPv6Address_t987378652_StaticFields, ___Unspecified_4)); }
	inline IPv6Address_t987378652 * get_Unspecified_4() const { return ___Unspecified_4; }
	inline IPv6Address_t987378652 ** get_address_of_Unspecified_4() { return &___Unspecified_4; }
	inline void set_Unspecified_4(IPv6Address_t987378652 * value)
	{
		___Unspecified_4 = value;
		Il2CppCodeGenWriteBarrier((&___Unspecified_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESS_T987378652_H
#ifndef COLLECTIONBASE_T4008832874_H
#define COLLECTIONBASE_T4008832874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t4008832874  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t1294140779 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t4008832874, ___list_0)); }
	inline ArrayList_t1294140779 * get_list_0() const { return ___list_0; }
	inline ArrayList_t1294140779 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t1294140779 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T4008832874_H
#ifndef X509CERTIFICATE_T934499855_H
#define X509CERTIFICATE_T934499855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_t934499855  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::x509
	X509Certificate_t3002548650 * ___x509_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::cachedCertificateHash
	ByteU5BU5D_t3287329517* ___cachedCertificateHash_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_3;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_4;

public:
	inline static int32_t get_offset_of_x509_0() { return static_cast<int32_t>(offsetof(X509Certificate_t934499855, ___x509_0)); }
	inline X509Certificate_t3002548650 * get_x509_0() const { return ___x509_0; }
	inline X509Certificate_t3002548650 ** get_address_of_x509_0() { return &___x509_0; }
	inline void set_x509_0(X509Certificate_t3002548650 * value)
	{
		___x509_0 = value;
		Il2CppCodeGenWriteBarrier((&___x509_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_t934499855, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_cachedCertificateHash_2() { return static_cast<int32_t>(offsetof(X509Certificate_t934499855, ___cachedCertificateHash_2)); }
	inline ByteU5BU5D_t3287329517* get_cachedCertificateHash_2() const { return ___cachedCertificateHash_2; }
	inline ByteU5BU5D_t3287329517** get_address_of_cachedCertificateHash_2() { return &___cachedCertificateHash_2; }
	inline void set_cachedCertificateHash_2(ByteU5BU5D_t3287329517* value)
	{
		___cachedCertificateHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCertificateHash_2), value);
	}

	inline static int32_t get_offset_of_issuer_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_t934499855, ___issuer_name_3)); }
	inline String_t* get_issuer_name_3() const { return ___issuer_name_3; }
	inline String_t** get_address_of_issuer_name_3() { return &___issuer_name_3; }
	inline void set_issuer_name_3(String_t* value)
	{
		___issuer_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_3), value);
	}

	inline static int32_t get_offset_of_subject_name_4() { return static_cast<int32_t>(offsetof(X509Certificate_t934499855, ___subject_name_4)); }
	inline String_t* get_subject_name_4() const { return ___subject_name_4; }
	inline String_t** get_address_of_subject_name_4() { return &___subject_name_4; }
	inline void set_subject_name_4(String_t* value)
	{
		___subject_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T934499855_H
#ifndef SPKEY_T2934702501_H
#define SPKEY_T2934702501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/SPKey
struct  SPKey_t2934702501  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePointManager/SPKey::uri
	Uri_t2779109183 * ___uri_0;
	// System.Boolean System.Net.ServicePointManager/SPKey::use_connect
	bool ___use_connect_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(SPKey_t2934702501, ___uri_0)); }
	inline Uri_t2779109183 * get_uri_0() const { return ___uri_0; }
	inline Uri_t2779109183 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t2779109183 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_use_connect_1() { return static_cast<int32_t>(offsetof(SPKey_t2934702501, ___use_connect_1)); }
	inline bool get_use_connect_1() const { return ___use_connect_1; }
	inline bool* get_address_of_use_connect_1() { return &___use_connect_1; }
	inline void set_use_connect_1(bool value)
	{
		___use_connect_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPKEY_T2934702501_H
#ifndef MARSHALBYREFOBJECT_T532690895_H
#define MARSHALBYREFOBJECT_T532690895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t532690895  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2184608466 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t532690895, ____identity_0)); }
	inline ServerIdentity_t2184608466 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2184608466 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2184608466 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T532690895_H
#ifndef X509CERTIFICATE2ENUMERATOR_T3449845280_H
#define X509CERTIFICATE2ENUMERATOR_T3449845280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct  X509Certificate2Enumerator_t3449845280  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509Certificate2Enumerator_t3449845280, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2ENUMERATOR_T3449845280_H
#ifndef WEBPROXY_T2130953411_H
#define WEBPROXY_T2130953411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t2130953411  : public RuntimeObject
{
public:
	// System.Uri System.Net.WebProxy::address
	Uri_t2779109183 * ___address_0;
	// System.Boolean System.Net.WebProxy::bypassOnLocal
	bool ___bypassOnLocal_1;
	// System.Collections.ArrayList System.Net.WebProxy::bypassList
	ArrayList_t1294140779 * ___bypassList_2;
	// System.Net.ICredentials System.Net.WebProxy::credentials
	RuntimeObject* ___credentials_3;
	// System.Boolean System.Net.WebProxy::useDefaultCredentials
	bool ___useDefaultCredentials_4;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(WebProxy_t2130953411, ___address_0)); }
	inline Uri_t2779109183 * get_address_0() const { return ___address_0; }
	inline Uri_t2779109183 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(Uri_t2779109183 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_bypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t2130953411, ___bypassOnLocal_1)); }
	inline bool get_bypassOnLocal_1() const { return ___bypassOnLocal_1; }
	inline bool* get_address_of_bypassOnLocal_1() { return &___bypassOnLocal_1; }
	inline void set_bypassOnLocal_1(bool value)
	{
		___bypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_bypassList_2() { return static_cast<int32_t>(offsetof(WebProxy_t2130953411, ___bypassList_2)); }
	inline ArrayList_t1294140779 * get_bypassList_2() const { return ___bypassList_2; }
	inline ArrayList_t1294140779 ** get_address_of_bypassList_2() { return &___bypassList_2; }
	inline void set_bypassList_2(ArrayList_t1294140779 * value)
	{
		___bypassList_2 = value;
		Il2CppCodeGenWriteBarrier((&___bypassList_2), value);
	}

	inline static int32_t get_offset_of_credentials_3() { return static_cast<int32_t>(offsetof(WebProxy_t2130953411, ___credentials_3)); }
	inline RuntimeObject* get_credentials_3() const { return ___credentials_3; }
	inline RuntimeObject** get_address_of_credentials_3() { return &___credentials_3; }
	inline void set_credentials_3(RuntimeObject* value)
	{
		___credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_3), value);
	}

	inline static int32_t get_offset_of_useDefaultCredentials_4() { return static_cast<int32_t>(offsetof(WebProxy_t2130953411, ___useDefaultCredentials_4)); }
	inline bool get_useDefaultCredentials_4() const { return ___useDefaultCredentials_4; }
	inline bool* get_address_of_useDefaultCredentials_4() { return &___useDefaultCredentials_4; }
	inline void set_useDefaultCredentials_4(bool value)
	{
		___useDefaultCredentials_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXY_T2130953411_H
#ifndef PUBLICKEY_T1522942069_H
#define PUBLICKEY_T1522942069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.PublicKey
struct  PublicKey_t1522942069  : public RuntimeObject
{
public:
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::_key
	AsymmetricAlgorithm_t3837984319 * ____key_0;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_keyValue
	AsnEncodedData_t2608258608 * ____keyValue_1;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_params
	AsnEncodedData_t2608258608 * ____params_2;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::_oid
	Oid_t2597199105 * ____oid_3;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(PublicKey_t1522942069, ____key_0)); }
	inline AsymmetricAlgorithm_t3837984319 * get__key_0() const { return ____key_0; }
	inline AsymmetricAlgorithm_t3837984319 ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(AsymmetricAlgorithm_t3837984319 * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__keyValue_1() { return static_cast<int32_t>(offsetof(PublicKey_t1522942069, ____keyValue_1)); }
	inline AsnEncodedData_t2608258608 * get__keyValue_1() const { return ____keyValue_1; }
	inline AsnEncodedData_t2608258608 ** get_address_of__keyValue_1() { return &____keyValue_1; }
	inline void set__keyValue_1(AsnEncodedData_t2608258608 * value)
	{
		____keyValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyValue_1), value);
	}

	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(PublicKey_t1522942069, ____params_2)); }
	inline AsnEncodedData_t2608258608 * get__params_2() const { return ____params_2; }
	inline AsnEncodedData_t2608258608 ** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(AsnEncodedData_t2608258608 * value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}

	inline static int32_t get_offset_of__oid_3() { return static_cast<int32_t>(offsetof(PublicKey_t1522942069, ____oid_3)); }
	inline Oid_t2597199105 * get__oid_3() const { return ____oid_3; }
	inline Oid_t2597199105 ** get_address_of__oid_3() { return &____oid_3; }
	inline void set__oid_3(Oid_t2597199105 * value)
	{
		____oid_3 = value;
		Il2CppCodeGenWriteBarrier((&____oid_3), value);
	}
};

struct PublicKey_t1522942069_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.PublicKey::<>f__switch$map9
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24map9_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_4() { return static_cast<int32_t>(offsetof(PublicKey_t1522942069_StaticFields, ___U3CU3Ef__switchU24map9_4)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24map9_4() const { return ___U3CU3Ef__switchU24map9_4; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24map9_4() { return &___U3CU3Ef__switchU24map9_4; }
	inline void set_U3CU3Ef__switchU24map9_4(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24map9_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEY_T1522942069_H
#ifndef EXPRESSION_T1722083296_H
#define EXPRESSION_T1722083296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Expression
struct  Expression_t1722083296  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T1722083296_H
#ifndef QUICKSEARCH_T2754904521_H
#define QUICKSEARCH_T2754904521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.QuickSearch
struct  QuickSearch_t2754904521  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.QuickSearch::str
	String_t* ___str_0;
	// System.Int32 System.Text.RegularExpressions.QuickSearch::len
	int32_t ___len_1;
	// System.Boolean System.Text.RegularExpressions.QuickSearch::ignore
	bool ___ignore_2;
	// System.Boolean System.Text.RegularExpressions.QuickSearch::reverse
	bool ___reverse_3;
	// System.Byte[] System.Text.RegularExpressions.QuickSearch::shift
	ByteU5BU5D_t3287329517* ___shift_4;
	// System.Collections.Hashtable System.Text.RegularExpressions.QuickSearch::shiftExtended
	Hashtable_t3677817270 * ___shiftExtended_5;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_len_1() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___len_1)); }
	inline int32_t get_len_1() const { return ___len_1; }
	inline int32_t* get_address_of_len_1() { return &___len_1; }
	inline void set_len_1(int32_t value)
	{
		___len_1 = value;
	}

	inline static int32_t get_offset_of_ignore_2() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___ignore_2)); }
	inline bool get_ignore_2() const { return ___ignore_2; }
	inline bool* get_address_of_ignore_2() { return &___ignore_2; }
	inline void set_ignore_2(bool value)
	{
		___ignore_2 = value;
	}

	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___reverse_3)); }
	inline bool get_reverse_3() const { return ___reverse_3; }
	inline bool* get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(bool value)
	{
		___reverse_3 = value;
	}

	inline static int32_t get_offset_of_shift_4() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___shift_4)); }
	inline ByteU5BU5D_t3287329517* get_shift_4() const { return ___shift_4; }
	inline ByteU5BU5D_t3287329517** get_address_of_shift_4() { return &___shift_4; }
	inline void set_shift_4(ByteU5BU5D_t3287329517* value)
	{
		___shift_4 = value;
		Il2CppCodeGenWriteBarrier((&___shift_4), value);
	}

	inline static int32_t get_offset_of_shiftExtended_5() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521, ___shiftExtended_5)); }
	inline Hashtable_t3677817270 * get_shiftExtended_5() const { return ___shiftExtended_5; }
	inline Hashtable_t3677817270 ** get_address_of_shiftExtended_5() { return &___shiftExtended_5; }
	inline void set_shiftExtended_5(Hashtable_t3677817270 * value)
	{
		___shiftExtended_5 = value;
		Il2CppCodeGenWriteBarrier((&___shiftExtended_5), value);
	}
};

struct QuickSearch_t2754904521_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.QuickSearch::THRESHOLD
	int32_t ___THRESHOLD_6;

public:
	inline static int32_t get_offset_of_THRESHOLD_6() { return static_cast<int32_t>(offsetof(QuickSearch_t2754904521_StaticFields, ___THRESHOLD_6)); }
	inline int32_t get_THRESHOLD_6() const { return ___THRESHOLD_6; }
	inline int32_t* get_address_of_THRESHOLD_6() { return &___THRESHOLD_6; }
	inline void set_THRESHOLD_6(int32_t value)
	{
		___THRESHOLD_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKSEARCH_T2754904521_H
#ifndef PARSER_T3991154808_H
#define PARSER_T3991154808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Parser
struct  Parser_t3991154808  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Syntax.Parser::pattern
	String_t* ___pattern_0;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ptr
	int32_t ___ptr_1;
	// System.Collections.ArrayList System.Text.RegularExpressions.Syntax.Parser::caps
	ArrayList_t1294140779 * ___caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.Syntax.Parser::refs
	Hashtable_t3677817270 * ___refs_3;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::num_groups
	int32_t ___num_groups_4;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::gap
	int32_t ___gap_5;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_ptr_1() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___ptr_1)); }
	inline int32_t get_ptr_1() const { return ___ptr_1; }
	inline int32_t* get_address_of_ptr_1() { return &___ptr_1; }
	inline void set_ptr_1(int32_t value)
	{
		___ptr_1 = value;
	}

	inline static int32_t get_offset_of_caps_2() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___caps_2)); }
	inline ArrayList_t1294140779 * get_caps_2() const { return ___caps_2; }
	inline ArrayList_t1294140779 ** get_address_of_caps_2() { return &___caps_2; }
	inline void set_caps_2(ArrayList_t1294140779 * value)
	{
		___caps_2 = value;
		Il2CppCodeGenWriteBarrier((&___caps_2), value);
	}

	inline static int32_t get_offset_of_refs_3() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___refs_3)); }
	inline Hashtable_t3677817270 * get_refs_3() const { return ___refs_3; }
	inline Hashtable_t3677817270 ** get_address_of_refs_3() { return &___refs_3; }
	inline void set_refs_3(Hashtable_t3677817270 * value)
	{
		___refs_3 = value;
		Il2CppCodeGenWriteBarrier((&___refs_3), value);
	}

	inline static int32_t get_offset_of_num_groups_4() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___num_groups_4)); }
	inline int32_t get_num_groups_4() const { return ___num_groups_4; }
	inline int32_t* get_address_of_num_groups_4() { return &___num_groups_4; }
	inline void set_num_groups_4(int32_t value)
	{
		___num_groups_4 = value;
	}

	inline static int32_t get_offset_of_gap_5() { return static_cast<int32_t>(offsetof(Parser_t3991154808, ___gap_5)); }
	inline int32_t get_gap_5() const { return ___gap_5; }
	inline int32_t* get_address_of_gap_5() { return &___gap_5; }
	inline void set_gap_5(int32_t value)
	{
		___gap_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3991154808_H
#ifndef ENUM_T3173835468_H
#define ENUM_T3173835468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t3173835468  : public ValueType_t1364887298
{
public:

public:
};

struct Enum_t3173835468_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t83643201* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t3173835468_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t83643201* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t83643201** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t83643201* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t3173835468_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t3173835468_marshaled_com
{
};
#endif // ENUM_T3173835468_H
#ifndef COMPOSITEEXPRESSION_T374080217_H
#define COMPOSITEEXPRESSION_T374080217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.CompositeExpression
struct  CompositeExpression_t374080217  : public Expression_t1722083296
{
public:
	// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::expressions
	ExpressionCollection_t2949493571 * ___expressions_0;

public:
	inline static int32_t get_offset_of_expressions_0() { return static_cast<int32_t>(offsetof(CompositeExpression_t374080217, ___expressions_0)); }
	inline ExpressionCollection_t2949493571 * get_expressions_0() const { return ___expressions_0; }
	inline ExpressionCollection_t2949493571 ** get_address_of_expressions_0() { return &___expressions_0; }
	inline void set_expressions_0(ExpressionCollection_t2949493571 * value)
	{
		___expressions_0 = value;
		Il2CppCodeGenWriteBarrier((&___expressions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEEXPRESSION_T374080217_H
#ifndef LINK_T3403178030_H
#define LINK_T3403178030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
struct  Link_t3403178030 
{
public:
	// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link::base_addr
	int32_t ___base_addr_0;
	// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link::offset_addr
	int32_t ___offset_addr_1;

public:
	inline static int32_t get_offset_of_base_addr_0() { return static_cast<int32_t>(offsetof(Link_t3403178030, ___base_addr_0)); }
	inline int32_t get_base_addr_0() const { return ___base_addr_0; }
	inline int32_t* get_address_of_base_addr_0() { return &___base_addr_0; }
	inline void set_base_addr_0(int32_t value)
	{
		___base_addr_0 = value;
	}

	inline static int32_t get_offset_of_offset_addr_1() { return static_cast<int32_t>(offsetof(Link_t3403178030, ___offset_addr_1)); }
	inline int32_t get_offset_addr_1() const { return ___offset_addr_1; }
	inline int32_t* get_address_of_offset_addr_1() { return &___offset_addr_1; }
	inline void set_offset_addr_1(int32_t value)
	{
		___offset_addr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T3403178030_H
#ifndef LINKSTACK_T2080309226_H
#define LINKSTACK_T2080309226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t2080309226  : public LinkRef_t3851818329
{
public:
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t1147195501 * ___stack_0;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(LinkStack_t2080309226, ___stack_0)); }
	inline Stack_t1147195501 * get_stack_0() const { return ___stack_0; }
	inline Stack_t1147195501 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(Stack_t1147195501 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSTACK_T2080309226_H
#ifndef TIMESPAN_T457147580_H
#define TIMESPAN_T457147580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t457147580 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t457147580_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t457147580  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t457147580  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t457147580  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t457147580  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t457147580 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t457147580  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t457147580  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t457147580 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t457147580  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___Zero_2)); }
	inline TimeSpan_t457147580  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t457147580 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t457147580  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T457147580_H
#ifndef MARK_T3700292706_H
#define MARK_T3700292706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Mark
struct  Mark_t3700292706 
{
public:
	// System.Int32 System.Text.RegularExpressions.Mark::Start
	int32_t ___Start_0;
	// System.Int32 System.Text.RegularExpressions.Mark::End
	int32_t ___End_1;
	// System.Int32 System.Text.RegularExpressions.Mark::Previous
	int32_t ___Previous_2;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(Mark_t3700292706, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(Mark_t3700292706, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Previous_2() { return static_cast<int32_t>(offsetof(Mark_t3700292706, ___Previous_2)); }
	inline int32_t get_Previous_2() const { return ___Previous_2; }
	inline int32_t* get_address_of_Previous_2() { return &___Previous_2; }
	inline void set_Previous_2(int32_t value)
	{
		___Previous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARK_T3700292706_H
#ifndef INTSTACK_T3571941486_H
#define INTSTACK_T3571941486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter/IntStack
struct  IntStack_t3571941486 
{
public:
	// System.Int32[] System.Text.RegularExpressions.Interpreter/IntStack::values
	Int32U5BU5D_t3565237794* ___values_0;
	// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(IntStack_t3571941486, ___values_0)); }
	inline Int32U5BU5D_t3565237794* get_values_0() const { return ___values_0; }
	inline Int32U5BU5D_t3565237794** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(Int32U5BU5D_t3565237794* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(IntStack_t3571941486, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.Interpreter/IntStack
struct IntStack_t3571941486_marshaled_pinvoke
{
	int32_t* ___values_0;
	int32_t ___count_1;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.Interpreter/IntStack
struct IntStack_t3571941486_marshaled_com
{
	int32_t* ___values_0;
	int32_t ___count_1;
};
#endif // INTSTACK_T3571941486_H
#ifndef GROUP_T4116754232_H
#define GROUP_T4116754232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t4116754232  : public Capture_t3723888528
{
public:
	// System.Boolean System.Text.RegularExpressions.Group::success
	bool ___success_4;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::captures
	CaptureCollection_t1282494621 * ___captures_5;

public:
	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Group_t4116754232, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_captures_5() { return static_cast<int32_t>(offsetof(Group_t4116754232, ___captures_5)); }
	inline CaptureCollection_t1282494621 * get_captures_5() const { return ___captures_5; }
	inline CaptureCollection_t1282494621 ** get_address_of_captures_5() { return &___captures_5; }
	inline void set_captures_5(CaptureCollection_t1282494621 * value)
	{
		___captures_5 = value;
		Il2CppCodeGenWriteBarrier((&___captures_5), value);
	}
};

struct Group_t4116754232_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::Fail
	Group_t4116754232 * ___Fail_3;

public:
	inline static int32_t get_offset_of_Fail_3() { return static_cast<int32_t>(offsetof(Group_t4116754232_StaticFields, ___Fail_3)); }
	inline Group_t4116754232 * get_Fail_3() const { return ___Fail_3; }
	inline Group_t4116754232 ** get_address_of_Fail_3() { return &___Fail_3; }
	inline void set_Fail_3(Group_t4116754232 * value)
	{
		___Fail_3 = value;
		Il2CppCodeGenWriteBarrier((&___Fail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T4116754232_H
#ifndef EXPRESSIONCOLLECTION_T2949493571_H
#define EXPRESSIONCOLLECTION_T2949493571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct  ExpressionCollection_t2949493571  : public CollectionBase_t4008832874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONCOLLECTION_T2949493571_H
#ifndef NAMEVALUECOLLECTION_T2257575727_H
#define NAMEVALUECOLLECTION_T2257575727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t2257575727  : public NameObjectCollectionBase_t1369902973
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t369357837* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t369357837* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t2257575727, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t369357837* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t369357837** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t369357837* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t2257575727, ___cachedAll_11)); }
	inline StringU5BU5D_t369357837* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t369357837** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t369357837* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T2257575727_H
#ifndef INTERVAL_T545543078_H
#define INTERVAL_T545543078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interval
struct  Interval_t545543078 
{
public:
	// System.Int32 System.Text.RegularExpressions.Interval::low
	int32_t ___low_0;
	// System.Int32 System.Text.RegularExpressions.Interval::high
	int32_t ___high_1;
	// System.Boolean System.Text.RegularExpressions.Interval::contiguous
	bool ___contiguous_2;

public:
	inline static int32_t get_offset_of_low_0() { return static_cast<int32_t>(offsetof(Interval_t545543078, ___low_0)); }
	inline int32_t get_low_0() const { return ___low_0; }
	inline int32_t* get_address_of_low_0() { return &___low_0; }
	inline void set_low_0(int32_t value)
	{
		___low_0 = value;
	}

	inline static int32_t get_offset_of_high_1() { return static_cast<int32_t>(offsetof(Interval_t545543078, ___high_1)); }
	inline int32_t get_high_1() const { return ___high_1; }
	inline int32_t* get_address_of_high_1() { return &___high_1; }
	inline void set_high_1(int32_t value)
	{
		___high_1 = value;
	}

	inline static int32_t get_offset_of_contiguous_2() { return static_cast<int32_t>(offsetof(Interval_t545543078, ___contiguous_2)); }
	inline bool get_contiguous_2() const { return ___contiguous_2; }
	inline bool* get_address_of_contiguous_2() { return &___contiguous_2; }
	inline void set_contiguous_2(bool value)
	{
		___contiguous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.Interval
struct Interval_t545543078_marshaled_pinvoke
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.Interval
struct Interval_t545543078_marshaled_com
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
#endif // INTERVAL_T545543078_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef X509EXTENSION_T3333384912_H
#define X509EXTENSION_T3333384912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Extension
struct  X509Extension_t3333384912  : public AsnEncodedData_t2608258608
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::_critical
	bool ____critical_3;

public:
	inline static int32_t get_offset_of__critical_3() { return static_cast<int32_t>(offsetof(X509Extension_t3333384912, ____critical_3)); }
	inline bool get__critical_3() const { return ____critical_3; }
	inline bool* get_address_of__critical_3() { return &____critical_3; }
	inline void set__critical_3(bool value)
	{
		____critical_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T3333384912_H
#ifndef X509CERTIFICATECOLLECTION_T3590496811_H
#define X509CERTIFICATECOLLECTION_T3590496811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct  X509CertificateCollection_t3590496811  : public CollectionBase_t4008832874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T3590496811_H
#ifndef X509CERTIFICATE2_T770297212_H
#define X509CERTIFICATE2_T770297212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct  X509Certificate2_t770297212  : public X509Certificate_t934499855
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2::_archived
	bool ____archived_5;
	// System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2::_extensions
	X509ExtensionCollection_t3540009410 * ____extensions_6;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_name
	String_t* ____name_7;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_serial
	String_t* ____serial_8;
	// System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2::_publicKey
	PublicKey_t1522942069 * ____publicKey_9;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::issuer_name
	X500DistinguishedName_t2418891383 * ___issuer_name_10;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::subject_name
	X500DistinguishedName_t2418891383 * ___subject_name_11;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2::signature_algorithm
	Oid_t2597199105 * ___signature_algorithm_12;
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2::_cert
	X509Certificate_t3002548651 * ____cert_13;

public:
	inline static int32_t get_offset_of__archived_5() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____archived_5)); }
	inline bool get__archived_5() const { return ____archived_5; }
	inline bool* get_address_of__archived_5() { return &____archived_5; }
	inline void set__archived_5(bool value)
	{
		____archived_5 = value;
	}

	inline static int32_t get_offset_of__extensions_6() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____extensions_6)); }
	inline X509ExtensionCollection_t3540009410 * get__extensions_6() const { return ____extensions_6; }
	inline X509ExtensionCollection_t3540009410 ** get_address_of__extensions_6() { return &____extensions_6; }
	inline void set__extensions_6(X509ExtensionCollection_t3540009410 * value)
	{
		____extensions_6 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_6), value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__serial_8() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____serial_8)); }
	inline String_t* get__serial_8() const { return ____serial_8; }
	inline String_t** get_address_of__serial_8() { return &____serial_8; }
	inline void set__serial_8(String_t* value)
	{
		____serial_8 = value;
		Il2CppCodeGenWriteBarrier((&____serial_8), value);
	}

	inline static int32_t get_offset_of__publicKey_9() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____publicKey_9)); }
	inline PublicKey_t1522942069 * get__publicKey_9() const { return ____publicKey_9; }
	inline PublicKey_t1522942069 ** get_address_of__publicKey_9() { return &____publicKey_9; }
	inline void set__publicKey_9(PublicKey_t1522942069 * value)
	{
		____publicKey_9 = value;
		Il2CppCodeGenWriteBarrier((&____publicKey_9), value);
	}

	inline static int32_t get_offset_of_issuer_name_10() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ___issuer_name_10)); }
	inline X500DistinguishedName_t2418891383 * get_issuer_name_10() const { return ___issuer_name_10; }
	inline X500DistinguishedName_t2418891383 ** get_address_of_issuer_name_10() { return &___issuer_name_10; }
	inline void set_issuer_name_10(X500DistinguishedName_t2418891383 * value)
	{
		___issuer_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_10), value);
	}

	inline static int32_t get_offset_of_subject_name_11() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ___subject_name_11)); }
	inline X500DistinguishedName_t2418891383 * get_subject_name_11() const { return ___subject_name_11; }
	inline X500DistinguishedName_t2418891383 ** get_address_of_subject_name_11() { return &___subject_name_11; }
	inline void set_subject_name_11(X500DistinguishedName_t2418891383 * value)
	{
		___subject_name_11 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_11), value);
	}

	inline static int32_t get_offset_of_signature_algorithm_12() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ___signature_algorithm_12)); }
	inline Oid_t2597199105 * get_signature_algorithm_12() const { return ___signature_algorithm_12; }
	inline Oid_t2597199105 ** get_address_of_signature_algorithm_12() { return &___signature_algorithm_12; }
	inline void set_signature_algorithm_12(Oid_t2597199105 * value)
	{
		___signature_algorithm_12 = value;
		Il2CppCodeGenWriteBarrier((&___signature_algorithm_12), value);
	}

	inline static int32_t get_offset_of__cert_13() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212, ____cert_13)); }
	inline X509Certificate_t3002548651 * get__cert_13() const { return ____cert_13; }
	inline X509Certificate_t3002548651 ** get_address_of__cert_13() { return &____cert_13; }
	inline void set__cert_13(X509Certificate_t3002548651 * value)
	{
		____cert_13 = value;
		Il2CppCodeGenWriteBarrier((&____cert_13), value);
	}
};

struct X509Certificate2_t770297212_StaticFields
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::empty_error
	String_t* ___empty_error_14;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::commonName
	ByteU5BU5D_t3287329517* ___commonName_15;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::email
	ByteU5BU5D_t3287329517* ___email_16;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::signedData
	ByteU5BU5D_t3287329517* ___signedData_17;

public:
	inline static int32_t get_offset_of_empty_error_14() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212_StaticFields, ___empty_error_14)); }
	inline String_t* get_empty_error_14() const { return ___empty_error_14; }
	inline String_t** get_address_of_empty_error_14() { return &___empty_error_14; }
	inline void set_empty_error_14(String_t* value)
	{
		___empty_error_14 = value;
		Il2CppCodeGenWriteBarrier((&___empty_error_14), value);
	}

	inline static int32_t get_offset_of_commonName_15() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212_StaticFields, ___commonName_15)); }
	inline ByteU5BU5D_t3287329517* get_commonName_15() const { return ___commonName_15; }
	inline ByteU5BU5D_t3287329517** get_address_of_commonName_15() { return &___commonName_15; }
	inline void set_commonName_15(ByteU5BU5D_t3287329517* value)
	{
		___commonName_15 = value;
		Il2CppCodeGenWriteBarrier((&___commonName_15), value);
	}

	inline static int32_t get_offset_of_email_16() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212_StaticFields, ___email_16)); }
	inline ByteU5BU5D_t3287329517* get_email_16() const { return ___email_16; }
	inline ByteU5BU5D_t3287329517** get_address_of_email_16() { return &___email_16; }
	inline void set_email_16(ByteU5BU5D_t3287329517* value)
	{
		___email_16 = value;
		Il2CppCodeGenWriteBarrier((&___email_16), value);
	}

	inline static int32_t get_offset_of_signedData_17() { return static_cast<int32_t>(offsetof(X509Certificate2_t770297212_StaticFields, ___signedData_17)); }
	inline ByteU5BU5D_t3287329517* get_signedData_17() const { return ___signedData_17; }
	inline ByteU5BU5D_t3287329517** get_address_of_signedData_17() { return &___signedData_17; }
	inline void set_signedData_17(ByteU5BU5D_t3287329517* value)
	{
		___signedData_17 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2_T770297212_H
#ifndef X500DISTINGUISHEDNAME_T2418891383_H
#define X500DISTINGUISHEDNAME_T2418891383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct  X500DistinguishedName_t2418891383  : public AsnEncodedData_t2608258608
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t2418891383, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAME_T2418891383_H
#ifndef X509CHAINSTATUSFLAGS_T2843688617_H
#define X509CHAINSTATUSFLAGS_T2843688617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t2843688617 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t2843688617, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T2843688617_H
#ifndef INTERPRETER_T881416833_H
#define INTERPRETER_T881416833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter
struct  Interpreter_t881416833  : public BaseMachine_t3233935325
{
public:
	// System.UInt16[] System.Text.RegularExpressions.Interpreter::program
	UInt16U5BU5D_t516420673* ___program_1;
	// System.Int32 System.Text.RegularExpressions.Interpreter::program_start
	int32_t ___program_start_2;
	// System.String System.Text.RegularExpressions.Interpreter::text
	String_t* ___text_3;
	// System.Int32 System.Text.RegularExpressions.Interpreter::text_end
	int32_t ___text_end_4;
	// System.Int32 System.Text.RegularExpressions.Interpreter::group_count
	int32_t ___group_count_5;
	// System.Int32 System.Text.RegularExpressions.Interpreter::match_min
	int32_t ___match_min_6;
	// System.Text.RegularExpressions.QuickSearch System.Text.RegularExpressions.Interpreter::qs
	QuickSearch_t2754904521 * ___qs_7;
	// System.Int32 System.Text.RegularExpressions.Interpreter::scan_ptr
	int32_t ___scan_ptr_8;
	// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter::repeat
	RepeatContext_t2609418170 * ___repeat_9;
	// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter::fast
	RepeatContext_t2609418170 * ___fast_10;
	// System.Text.RegularExpressions.Interpreter/IntStack System.Text.RegularExpressions.Interpreter::stack
	IntStack_t3571941486  ___stack_11;
	// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter::deep
	RepeatContext_t2609418170 * ___deep_12;
	// System.Text.RegularExpressions.Mark[] System.Text.RegularExpressions.Interpreter::marks
	MarkU5BU5D_t3068193751* ___marks_13;
	// System.Int32 System.Text.RegularExpressions.Interpreter::mark_start
	int32_t ___mark_start_14;
	// System.Int32 System.Text.RegularExpressions.Interpreter::mark_end
	int32_t ___mark_end_15;
	// System.Int32[] System.Text.RegularExpressions.Interpreter::groups
	Int32U5BU5D_t3565237794* ___groups_16;

public:
	inline static int32_t get_offset_of_program_1() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___program_1)); }
	inline UInt16U5BU5D_t516420673* get_program_1() const { return ___program_1; }
	inline UInt16U5BU5D_t516420673** get_address_of_program_1() { return &___program_1; }
	inline void set_program_1(UInt16U5BU5D_t516420673* value)
	{
		___program_1 = value;
		Il2CppCodeGenWriteBarrier((&___program_1), value);
	}

	inline static int32_t get_offset_of_program_start_2() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___program_start_2)); }
	inline int32_t get_program_start_2() const { return ___program_start_2; }
	inline int32_t* get_address_of_program_start_2() { return &___program_start_2; }
	inline void set_program_start_2(int32_t value)
	{
		___program_start_2 = value;
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_text_end_4() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___text_end_4)); }
	inline int32_t get_text_end_4() const { return ___text_end_4; }
	inline int32_t* get_address_of_text_end_4() { return &___text_end_4; }
	inline void set_text_end_4(int32_t value)
	{
		___text_end_4 = value;
	}

	inline static int32_t get_offset_of_group_count_5() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___group_count_5)); }
	inline int32_t get_group_count_5() const { return ___group_count_5; }
	inline int32_t* get_address_of_group_count_5() { return &___group_count_5; }
	inline void set_group_count_5(int32_t value)
	{
		___group_count_5 = value;
	}

	inline static int32_t get_offset_of_match_min_6() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___match_min_6)); }
	inline int32_t get_match_min_6() const { return ___match_min_6; }
	inline int32_t* get_address_of_match_min_6() { return &___match_min_6; }
	inline void set_match_min_6(int32_t value)
	{
		___match_min_6 = value;
	}

	inline static int32_t get_offset_of_qs_7() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___qs_7)); }
	inline QuickSearch_t2754904521 * get_qs_7() const { return ___qs_7; }
	inline QuickSearch_t2754904521 ** get_address_of_qs_7() { return &___qs_7; }
	inline void set_qs_7(QuickSearch_t2754904521 * value)
	{
		___qs_7 = value;
		Il2CppCodeGenWriteBarrier((&___qs_7), value);
	}

	inline static int32_t get_offset_of_scan_ptr_8() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___scan_ptr_8)); }
	inline int32_t get_scan_ptr_8() const { return ___scan_ptr_8; }
	inline int32_t* get_address_of_scan_ptr_8() { return &___scan_ptr_8; }
	inline void set_scan_ptr_8(int32_t value)
	{
		___scan_ptr_8 = value;
	}

	inline static int32_t get_offset_of_repeat_9() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___repeat_9)); }
	inline RepeatContext_t2609418170 * get_repeat_9() const { return ___repeat_9; }
	inline RepeatContext_t2609418170 ** get_address_of_repeat_9() { return &___repeat_9; }
	inline void set_repeat_9(RepeatContext_t2609418170 * value)
	{
		___repeat_9 = value;
		Il2CppCodeGenWriteBarrier((&___repeat_9), value);
	}

	inline static int32_t get_offset_of_fast_10() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___fast_10)); }
	inline RepeatContext_t2609418170 * get_fast_10() const { return ___fast_10; }
	inline RepeatContext_t2609418170 ** get_address_of_fast_10() { return &___fast_10; }
	inline void set_fast_10(RepeatContext_t2609418170 * value)
	{
		___fast_10 = value;
		Il2CppCodeGenWriteBarrier((&___fast_10), value);
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___stack_11)); }
	inline IntStack_t3571941486  get_stack_11() const { return ___stack_11; }
	inline IntStack_t3571941486 * get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(IntStack_t3571941486  value)
	{
		___stack_11 = value;
	}

	inline static int32_t get_offset_of_deep_12() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___deep_12)); }
	inline RepeatContext_t2609418170 * get_deep_12() const { return ___deep_12; }
	inline RepeatContext_t2609418170 ** get_address_of_deep_12() { return &___deep_12; }
	inline void set_deep_12(RepeatContext_t2609418170 * value)
	{
		___deep_12 = value;
		Il2CppCodeGenWriteBarrier((&___deep_12), value);
	}

	inline static int32_t get_offset_of_marks_13() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___marks_13)); }
	inline MarkU5BU5D_t3068193751* get_marks_13() const { return ___marks_13; }
	inline MarkU5BU5D_t3068193751** get_address_of_marks_13() { return &___marks_13; }
	inline void set_marks_13(MarkU5BU5D_t3068193751* value)
	{
		___marks_13 = value;
		Il2CppCodeGenWriteBarrier((&___marks_13), value);
	}

	inline static int32_t get_offset_of_mark_start_14() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___mark_start_14)); }
	inline int32_t get_mark_start_14() const { return ___mark_start_14; }
	inline int32_t* get_address_of_mark_start_14() { return &___mark_start_14; }
	inline void set_mark_start_14(int32_t value)
	{
		___mark_start_14 = value;
	}

	inline static int32_t get_offset_of_mark_end_15() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___mark_end_15)); }
	inline int32_t get_mark_end_15() const { return ___mark_end_15; }
	inline int32_t* get_address_of_mark_end_15() { return &___mark_end_15; }
	inline void set_mark_end_15(int32_t value)
	{
		___mark_end_15 = value;
	}

	inline static int32_t get_offset_of_groups_16() { return static_cast<int32_t>(offsetof(Interpreter_t881416833, ___groups_16)); }
	inline Int32U5BU5D_t3565237794* get_groups_16() const { return ___groups_16; }
	inline Int32U5BU5D_t3565237794** get_address_of_groups_16() { return &___groups_16; }
	inline void set_groups_16(Int32U5BU5D_t3565237794* value)
	{
		___groups_16 = value;
		Il2CppCodeGenWriteBarrier((&___groups_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPRETER_T881416833_H
#ifndef ASNDECODESTATUS_T3809782524_H
#define ASNDECODESTATUS_T3809782524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t3809782524 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t3809782524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T3809782524_H
#ifndef X509CERTIFICATE2COLLECTION_T1517963221_H
#define X509CERTIFICATE2COLLECTION_T1517963221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct  X509Certificate2Collection_t1517963221  : public X509CertificateCollection_t3590496811
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2COLLECTION_T1517963221_H
#ifndef X500DISTINGUISHEDNAMEFLAGS_T245693987_H
#define X500DISTINGUISHEDNAMEFLAGS_T245693987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
struct  X500DistinguishedNameFlags_t245693987 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X500DistinguishedNameFlags_t245693987, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAMEFLAGS_T245693987_H
#ifndef STORENAME_T854849834_H
#define STORENAME_T854849834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreName
struct  StoreName_t854849834 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreName_t854849834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORENAME_T854849834_H
#ifndef GROUP_T1053066773_H
#define GROUP_T1053066773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Group
struct  Group_t1053066773  : public CompositeExpression_t374080217
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T1053066773_H
#ifndef STORELOCATION_T1581816920_H
#define STORELOCATION_T1581816920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreLocation
struct  StoreLocation_t1581816920 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreLocation_t1581816920, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELOCATION_T1581816920_H
#ifndef ADDRESSFAMILY_T214287616_H
#define ADDRESSFAMILY_T214287616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t214287616 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t214287616, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T214287616_H
#ifndef OPENFLAGS_T2472073502_H
#define OPENFLAGS_T2472073502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_t2472073502 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpenFlags_t2472073502, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_T2472073502_H
#ifndef WEBHEADERCOLLECTION_T1238597439_H
#define WEBHEADERCOLLECTION_T1238597439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_t1238597439  : public NameValueCollection_t2257575727
{
public:
	// System.Boolean System.Net.WebHeaderCollection::internallyCreated
	bool ___internallyCreated_15;

public:
	inline static int32_t get_offset_of_internallyCreated_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1238597439, ___internallyCreated_15)); }
	inline bool get_internallyCreated_15() const { return ___internallyCreated_15; }
	inline bool* get_address_of_internallyCreated_15() { return &___internallyCreated_15; }
	inline void set_internallyCreated_15(bool value)
	{
		___internallyCreated_15 = value;
	}
};

struct WebHeaderCollection_t1238597439_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.WebHeaderCollection::restricted
	Hashtable_t3677817270 * ___restricted_12;
	// System.Collections.Hashtable System.Net.WebHeaderCollection::multiValue
	Hashtable_t3677817270 * ___multiValue_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Net.WebHeaderCollection::restricted_response
	Dictionary_2_t3562104089 * ___restricted_response_14;
	// System.Boolean[] System.Net.WebHeaderCollection::allowed_chars
	BooleanU5BU5D_t2577425931* ___allowed_chars_16;

public:
	inline static int32_t get_offset_of_restricted_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1238597439_StaticFields, ___restricted_12)); }
	inline Hashtable_t3677817270 * get_restricted_12() const { return ___restricted_12; }
	inline Hashtable_t3677817270 ** get_address_of_restricted_12() { return &___restricted_12; }
	inline void set_restricted_12(Hashtable_t3677817270 * value)
	{
		___restricted_12 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_12), value);
	}

	inline static int32_t get_offset_of_multiValue_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1238597439_StaticFields, ___multiValue_13)); }
	inline Hashtable_t3677817270 * get_multiValue_13() const { return ___multiValue_13; }
	inline Hashtable_t3677817270 ** get_address_of_multiValue_13() { return &___multiValue_13; }
	inline void set_multiValue_13(Hashtable_t3677817270 * value)
	{
		___multiValue_13 = value;
		Il2CppCodeGenWriteBarrier((&___multiValue_13), value);
	}

	inline static int32_t get_offset_of_restricted_response_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1238597439_StaticFields, ___restricted_response_14)); }
	inline Dictionary_2_t3562104089 * get_restricted_response_14() const { return ___restricted_response_14; }
	inline Dictionary_2_t3562104089 ** get_address_of_restricted_response_14() { return &___restricted_response_14; }
	inline void set_restricted_response_14(Dictionary_2_t3562104089 * value)
	{
		___restricted_response_14 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_response_14), value);
	}

	inline static int32_t get_offset_of_allowed_chars_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1238597439_StaticFields, ___allowed_chars_16)); }
	inline BooleanU5BU5D_t2577425931* get_allowed_chars_16() const { return ___allowed_chars_16; }
	inline BooleanU5BU5D_t2577425931** get_address_of_allowed_chars_16() { return &___allowed_chars_16; }
	inline void set_allowed_chars_16(BooleanU5BU5D_t2577425931* value)
	{
		___allowed_chars_16 = value;
		Il2CppCodeGenWriteBarrier((&___allowed_chars_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T1238597439_H
#ifndef AUTHENTICATIONLEVEL_T3804947215_H
#define AUTHENTICATIONLEVEL_T3804947215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_t3804947215 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationLevel_t3804947215, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_T3804947215_H
#ifndef SECURITYPROTOCOLTYPE_T352736498_H
#define SECURITYPROTOCOLTYPE_T352736498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t352736498 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t352736498, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T352736498_H
#ifndef DATETIMEKIND_T508394208_H
#define DATETIMEKIND_T508394208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t508394208 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t508394208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T508394208_H
#ifndef PATTERNLINKSTACK_T3550044269_H
#define PATTERNLINKSTACK_T3550044269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
struct  PatternLinkStack_t3550044269  : public LinkStack_t2080309226
{
public:
	// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::link
	Link_t3403178030  ___link_1;

public:
	inline static int32_t get_offset_of_link_1() { return static_cast<int32_t>(offsetof(PatternLinkStack_t3550044269, ___link_1)); }
	inline Link_t3403178030  get_link_1() const { return ___link_1; }
	inline Link_t3403178030 * get_address_of_link_1() { return &___link_1; }
	inline void set_link_1(Link_t3403178030  value)
	{
		___link_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATTERNLINKSTACK_T3550044269_H
#ifndef DELEGATE_T1563516729_H
#define DELEGATE_T1563516729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1563516729  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t975501551 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___data_8)); }
	inline DelegateData_t975501551 * get_data_8() const { return ___data_8; }
	inline DelegateData_t975501551 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t975501551 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1563516729_H
#ifndef MODE_T2316998792_H
#define MODE_T2316998792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter/Mode
struct  Mode_t2316998792 
{
public:
	// System.Int32 System.Text.RegularExpressions.Interpreter/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2316998792, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2316998792_H
#ifndef OPCODE_T3191944080_H
#define OPCODE_T3191944080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.OpCode
struct  OpCode_t3191944080 
{
public:
	// System.UInt16 System.Text.RegularExpressions.OpCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpCode_t3191944080, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T3191944080_H
#ifndef X509FINDTYPE_T130291178_H
#define X509FINDTYPE_T130291178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509FindType
struct  X509FindType_t130291178 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509FindType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509FindType_t130291178, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509FINDTYPE_T130291178_H
#ifndef OPFLAGS_T3593480483_H
#define OPFLAGS_T3593480483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.OpFlags
struct  OpFlags_t3593480483 
{
public:
	// System.UInt16 System.Text.RegularExpressions.OpFlags::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpFlags_t3593480483, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPFLAGS_T3593480483_H
#ifndef POSITION_T2204692691_H
#define POSITION_T2204692691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Position
struct  Position_t2204692691 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Position::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t2204692691, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T2204692691_H
#ifndef X509REVOCATIONMODE_T1434479791_H
#define X509REVOCATIONMODE_T1434479791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t1434479791 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationMode_t1434479791, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T1434479791_H
#ifndef MATCH_T3699055374_H
#define MATCH_T3699055374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3699055374  : public Group_t4116754232
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::regex
	Regex_t2687244226 * ___regex_6;
	// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.Match::machine
	RuntimeObject* ___machine_7;
	// System.Int32 System.Text.RegularExpressions.Match::text_length
	int32_t ___text_length_8;
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::groups
	GroupCollection_t3084121107 * ___groups_9;

public:
	inline static int32_t get_offset_of_regex_6() { return static_cast<int32_t>(offsetof(Match_t3699055374, ___regex_6)); }
	inline Regex_t2687244226 * get_regex_6() const { return ___regex_6; }
	inline Regex_t2687244226 ** get_address_of_regex_6() { return &___regex_6; }
	inline void set_regex_6(Regex_t2687244226 * value)
	{
		___regex_6 = value;
		Il2CppCodeGenWriteBarrier((&___regex_6), value);
	}

	inline static int32_t get_offset_of_machine_7() { return static_cast<int32_t>(offsetof(Match_t3699055374, ___machine_7)); }
	inline RuntimeObject* get_machine_7() const { return ___machine_7; }
	inline RuntimeObject** get_address_of_machine_7() { return &___machine_7; }
	inline void set_machine_7(RuntimeObject* value)
	{
		___machine_7 = value;
		Il2CppCodeGenWriteBarrier((&___machine_7), value);
	}

	inline static int32_t get_offset_of_text_length_8() { return static_cast<int32_t>(offsetof(Match_t3699055374, ___text_length_8)); }
	inline int32_t get_text_length_8() const { return ___text_length_8; }
	inline int32_t* get_address_of_text_length_8() { return &___text_length_8; }
	inline void set_text_length_8(int32_t value)
	{
		___text_length_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(Match_t3699055374, ___groups_9)); }
	inline GroupCollection_t3084121107 * get_groups_9() const { return ___groups_9; }
	inline GroupCollection_t3084121107 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(GroupCollection_t3084121107 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___groups_9), value);
	}
};

struct Match_t3699055374_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::empty
	Match_t3699055374 * ___empty_10;

public:
	inline static int32_t get_offset_of_empty_10() { return static_cast<int32_t>(offsetof(Match_t3699055374_StaticFields, ___empty_10)); }
	inline Match_t3699055374 * get_empty_10() const { return ___empty_10; }
	inline Match_t3699055374 ** get_address_of_empty_10() { return &___empty_10; }
	inline void set_empty_10(Match_t3699055374 * value)
	{
		___empty_10 = value;
		Il2CppCodeGenWriteBarrier((&___empty_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T3699055374_H
#ifndef REGEXOPTIONS_T1974644192_H
#define REGEXOPTIONS_T1974644192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t1974644192 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t1974644192, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T1974644192_H
#ifndef X509NAMETYPE_T1476498007_H
#define X509NAMETYPE_T1476498007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509NameType
struct  X509NameType_t1476498007 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509NameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509NameType_t1476498007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETYPE_T1476498007_H
#ifndef X509REVOCATIONFLAG_T1382034120_H
#define X509REVOCATIONFLAG_T1382034120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_t1382034120 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationFlag_t1382034120, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_T1382034120_H
#ifndef CATEGORY_T3796563005_H
#define CATEGORY_T3796563005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Category
struct  Category_t3796563005 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Category::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Category_t3796563005, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORY_T3796563005_H
#ifndef X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T159978081_H
#define X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T159978081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
struct  X509SubjectKeyIdentifierHashAlgorithm_t159978081 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierHashAlgorithm_t159978081, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T159978081_H
#ifndef X509KEYUSAGEFLAGS_T1186641826_H
#define X509KEYUSAGEFLAGS_T1186641826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t1186641826 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t1186641826, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T1186641826_H
#ifndef X509VERIFICATIONFLAGS_T2520468694_H
#define X509VERIFICATIONFLAGS_T2520468694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t2520468694 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t2520468694, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T2520468694_H
#ifndef X509SUBJECTKEYIDENTIFIEREXTENSION_T2010369761_H
#define X509SUBJECTKEYIDENTIFIEREXTENSION_T2010369761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
struct  X509SubjectKeyIdentifierExtension_t2010369761  : public X509Extension_t3333384912
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_subjectKeyIdentifier
	ByteU5BU5D_t3287329517* ____subjectKeyIdentifier_6;
	// System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_ski
	String_t* ____ski_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__subjectKeyIdentifier_6() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2010369761, ____subjectKeyIdentifier_6)); }
	inline ByteU5BU5D_t3287329517* get__subjectKeyIdentifier_6() const { return ____subjectKeyIdentifier_6; }
	inline ByteU5BU5D_t3287329517** get_address_of__subjectKeyIdentifier_6() { return &____subjectKeyIdentifier_6; }
	inline void set__subjectKeyIdentifier_6(ByteU5BU5D_t3287329517* value)
	{
		____subjectKeyIdentifier_6 = value;
		Il2CppCodeGenWriteBarrier((&____subjectKeyIdentifier_6), value);
	}

	inline static int32_t get_offset_of__ski_7() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2010369761, ____ski_7)); }
	inline String_t* get__ski_7() const { return ____ski_7; }
	inline String_t** get_address_of__ski_7() { return &____ski_7; }
	inline void set__ski_7(String_t* value)
	{
		____ski_7 = value;
		Il2CppCodeGenWriteBarrier((&____ski_7), value);
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2010369761, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIEREXTENSION_T2010369761_H
#ifndef WEBREQUEST_T3661130957_H
#define WEBREQUEST_T3661130957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t3661130957  : public MarshalByRefObject_t532690895
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::authentication_level
	int32_t ___authentication_level_4;

public:
	inline static int32_t get_offset_of_authentication_level_4() { return static_cast<int32_t>(offsetof(WebRequest_t3661130957, ___authentication_level_4)); }
	inline int32_t get_authentication_level_4() const { return ___authentication_level_4; }
	inline int32_t* get_address_of_authentication_level_4() { return &___authentication_level_4; }
	inline void set_authentication_level_4(int32_t value)
	{
		___authentication_level_4 = value;
	}
};

struct WebRequest_t3661130957_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.WebRequest::prefixes
	HybridDictionary_t491885043 * ___prefixes_1;
	// System.Boolean System.Net.WebRequest::isDefaultWebProxySet
	bool ___isDefaultWebProxySet_2;
	// System.Net.IWebProxy System.Net.WebRequest::defaultWebProxy
	RuntimeObject* ___defaultWebProxy_3;
	// System.Object System.Net.WebRequest::lockobj
	RuntimeObject * ___lockobj_5;

public:
	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(WebRequest_t3661130957_StaticFields, ___prefixes_1)); }
	inline HybridDictionary_t491885043 * get_prefixes_1() const { return ___prefixes_1; }
	inline HybridDictionary_t491885043 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(HybridDictionary_t491885043 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_isDefaultWebProxySet_2() { return static_cast<int32_t>(offsetof(WebRequest_t3661130957_StaticFields, ___isDefaultWebProxySet_2)); }
	inline bool get_isDefaultWebProxySet_2() const { return ___isDefaultWebProxySet_2; }
	inline bool* get_address_of_isDefaultWebProxySet_2() { return &___isDefaultWebProxySet_2; }
	inline void set_isDefaultWebProxySet_2(bool value)
	{
		___isDefaultWebProxySet_2 = value;
	}

	inline static int32_t get_offset_of_defaultWebProxy_3() { return static_cast<int32_t>(offsetof(WebRequest_t3661130957_StaticFields, ___defaultWebProxy_3)); }
	inline RuntimeObject* get_defaultWebProxy_3() const { return ___defaultWebProxy_3; }
	inline RuntimeObject** get_address_of_defaultWebProxy_3() { return &___defaultWebProxy_3; }
	inline void set_defaultWebProxy_3(RuntimeObject* value)
	{
		___defaultWebProxy_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultWebProxy_3), value);
	}

	inline static int32_t get_offset_of_lockobj_5() { return static_cast<int32_t>(offsetof(WebRequest_t3661130957_StaticFields, ___lockobj_5)); }
	inline RuntimeObject * get_lockobj_5() const { return ___lockobj_5; }
	inline RuntimeObject ** get_address_of_lockobj_5() { return &___lockobj_5; }
	inline void set_lockobj_5(RuntimeObject * value)
	{
		___lockobj_5 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T3661130957_H
#ifndef IPADDRESS_T1685194060_H
#define IPADDRESS_T1685194060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t1685194060  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t516420673* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t516420673* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t516420673** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t516420673* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}
};

struct IPAddress_t1685194060_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t1685194060 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t1685194060 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t1685194060 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t1685194060 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t1685194060 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t1685194060 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t1685194060 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___Any_4)); }
	inline IPAddress_t1685194060 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t1685194060 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t1685194060 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t1685194060 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t1685194060 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t1685194060 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___Loopback_6)); }
	inline IPAddress_t1685194060 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t1685194060 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t1685194060 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___None_7)); }
	inline IPAddress_t1685194060 * get_None_7() const { return ___None_7; }
	inline IPAddress_t1685194060 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t1685194060 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t1685194060 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t1685194060 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t1685194060 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t1685194060 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t1685194060 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t1685194060 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t1685194060_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t1685194060 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t1685194060 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t1685194060 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T1685194060_H
#ifndef MULTICASTDELEGATE_T1280656641_H
#define MULTICASTDELEGATE_T1280656641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1280656641  : public Delegate_t1563516729
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1280656641 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1280656641 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___prev_9)); }
	inline MulticastDelegate_t1280656641 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1280656641 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1280656641 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___kpm_next_10)); }
	inline MulticastDelegate_t1280656641 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1280656641 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1280656641 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1280656641_H
#ifndef DATETIME_T972933412_H
#define DATETIME_T972933412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t972933412 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t457147580  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t972933412, ___ticks_0)); }
	inline TimeSpan_t457147580  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t457147580 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t457147580  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t972933412, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t972933412_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t972933412  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t972933412  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t369357837* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t369357837* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t369357837* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t369357837* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t369357837* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t369357837* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t369357837* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3565237794* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3565237794* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MaxValue_2)); }
	inline DateTime_t972933412  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t972933412 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t972933412  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MinValue_3)); }
	inline DateTime_t972933412  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t972933412 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t972933412  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t369357837* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t369357837** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t369357837* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t369357837* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t369357837** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t369357837* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t369357837* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t369357837** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t369357837* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t369357837* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t369357837** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t369357837* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t369357837* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t369357837** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t369357837* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t369357837* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t369357837** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t369357837* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t369357837* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t369357837** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t369357837* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t3565237794* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t3565237794** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t3565237794* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t3565237794* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t3565237794** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t3565237794* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T972933412_H
#ifndef SERVICEPOINTMANAGER_T382256627_H
#define SERVICEPOINTMANAGER_T382256627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_t382256627  : public RuntimeObject
{
public:

public:
};

struct ServicePointManager_t382256627_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t491885043 * ___servicePoints_0;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	RuntimeObject* ___policy_1;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_2;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_3;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_4;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_5;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_6;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_7;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_8;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::server_cert_cb
	RemoteCertificateValidationCallback_t2804961572 * ___server_cert_cb_9;

public:
	inline static int32_t get_offset_of_servicePoints_0() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___servicePoints_0)); }
	inline HybridDictionary_t491885043 * get_servicePoints_0() const { return ___servicePoints_0; }
	inline HybridDictionary_t491885043 ** get_address_of_servicePoints_0() { return &___servicePoints_0; }
	inline void set_servicePoints_0(HybridDictionary_t491885043 * value)
	{
		___servicePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoints_0), value);
	}

	inline static int32_t get_offset_of_policy_1() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___policy_1)); }
	inline RuntimeObject* get_policy_1() const { return ___policy_1; }
	inline RuntimeObject** get_address_of_policy_1() { return &___policy_1; }
	inline void set_policy_1(RuntimeObject* value)
	{
		___policy_1 = value;
		Il2CppCodeGenWriteBarrier((&___policy_1), value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_2() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___defaultConnectionLimit_2)); }
	inline int32_t get_defaultConnectionLimit_2() const { return ___defaultConnectionLimit_2; }
	inline int32_t* get_address_of_defaultConnectionLimit_2() { return &___defaultConnectionLimit_2; }
	inline void set_defaultConnectionLimit_2(int32_t value)
	{
		___defaultConnectionLimit_2 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_3() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___maxServicePointIdleTime_3)); }
	inline int32_t get_maxServicePointIdleTime_3() const { return ___maxServicePointIdleTime_3; }
	inline int32_t* get_address_of_maxServicePointIdleTime_3() { return &___maxServicePointIdleTime_3; }
	inline void set_maxServicePointIdleTime_3(int32_t value)
	{
		___maxServicePointIdleTime_3 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_4() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___maxServicePoints_4)); }
	inline int32_t get_maxServicePoints_4() const { return ___maxServicePoints_4; }
	inline int32_t* get_address_of_maxServicePoints_4() { return &___maxServicePoints_4; }
	inline void set_maxServicePoints_4(int32_t value)
	{
		___maxServicePoints_4 = value;
	}

	inline static int32_t get_offset_of__checkCRL_5() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ____checkCRL_5)); }
	inline bool get__checkCRL_5() const { return ____checkCRL_5; }
	inline bool* get_address_of__checkCRL_5() { return &____checkCRL_5; }
	inline void set__checkCRL_5(bool value)
	{
		____checkCRL_5 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_6() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ____securityProtocol_6)); }
	inline int32_t get__securityProtocol_6() const { return ____securityProtocol_6; }
	inline int32_t* get_address_of__securityProtocol_6() { return &____securityProtocol_6; }
	inline void set__securityProtocol_6(int32_t value)
	{
		____securityProtocol_6 = value;
	}

	inline static int32_t get_offset_of_expectContinue_7() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___expectContinue_7)); }
	inline bool get_expectContinue_7() const { return ___expectContinue_7; }
	inline bool* get_address_of_expectContinue_7() { return &___expectContinue_7; }
	inline void set_expectContinue_7(bool value)
	{
		___expectContinue_7 = value;
	}

	inline static int32_t get_offset_of_useNagle_8() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___useNagle_8)); }
	inline bool get_useNagle_8() const { return ___useNagle_8; }
	inline bool* get_address_of_useNagle_8() { return &___useNagle_8; }
	inline void set_useNagle_8(bool value)
	{
		___useNagle_8 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_9() { return static_cast<int32_t>(offsetof(ServicePointManager_t382256627_StaticFields, ___server_cert_cb_9)); }
	inline RemoteCertificateValidationCallback_t2804961572 * get_server_cert_cb_9() const { return ___server_cert_cb_9; }
	inline RemoteCertificateValidationCallback_t2804961572 ** get_address_of_server_cert_cb_9() { return &___server_cert_cb_9; }
	inline void set_server_cert_cb_9(RemoteCertificateValidationCallback_t2804961572 * value)
	{
		___server_cert_cb_9 = value;
		Il2CppCodeGenWriteBarrier((&___server_cert_cb_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGER_T382256627_H
#ifndef X509STORE_T754901440_H
#define X509STORE_T754901440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Store
struct  X509Store_t754901440  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Store::_name
	String_t* ____name_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Store::_location
	int32_t ____location_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::list
	X509Certificate2Collection_t1517963221 * ___list_2;
	// System.Security.Cryptography.X509Certificates.OpenFlags System.Security.Cryptography.X509Certificates.X509Store::_flags
	int32_t ____flags_3;
	// Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::store
	X509Store_t703944648 * ___store_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(X509Store_t754901440, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__location_1() { return static_cast<int32_t>(offsetof(X509Store_t754901440, ____location_1)); }
	inline int32_t get__location_1() const { return ____location_1; }
	inline int32_t* get_address_of__location_1() { return &____location_1; }
	inline void set__location_1(int32_t value)
	{
		____location_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(X509Store_t754901440, ___list_2)); }
	inline X509Certificate2Collection_t1517963221 * get_list_2() const { return ___list_2; }
	inline X509Certificate2Collection_t1517963221 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(X509Certificate2Collection_t1517963221 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(X509Store_t754901440, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(X509Store_t754901440, ___store_4)); }
	inline X509Store_t703944648 * get_store_4() const { return ___store_4; }
	inline X509Store_t703944648 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(X509Store_t703944648 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}
};

struct X509Store_t754901440_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Store::<>f__switch$mapF
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapF_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_5() { return static_cast<int32_t>(offsetof(X509Store_t754901440_StaticFields, ___U3CU3Ef__switchU24mapF_5)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapF_5() const { return ___U3CU3Ef__switchU24mapF_5; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapF_5() { return &___U3CU3Ef__switchU24mapF_5; }
	inline void set_U3CU3Ef__switchU24mapF_5(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapF_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T754901440_H
#ifndef X509ENHANCEDKEYUSAGEEXTENSION_T3022440765_H
#define X509ENHANCEDKEYUSAGEEXTENSION_T3022440765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
struct  X509EnhancedKeyUsageExtension_t3022440765  : public X509Extension_t3333384912
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_enhKeyUsage
	OidCollection_t2127506323 * ____enhKeyUsage_4;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_status
	int32_t ____status_5;

public:
	inline static int32_t get_offset_of__enhKeyUsage_4() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t3022440765, ____enhKeyUsage_4)); }
	inline OidCollection_t2127506323 * get__enhKeyUsage_4() const { return ____enhKeyUsage_4; }
	inline OidCollection_t2127506323 ** get_address_of__enhKeyUsage_4() { return &____enhKeyUsage_4; }
	inline void set__enhKeyUsage_4(OidCollection_t2127506323 * value)
	{
		____enhKeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&____enhKeyUsage_4), value);
	}

	inline static int32_t get_offset_of__status_5() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t3022440765, ____status_5)); }
	inline int32_t get__status_5() const { return ____status_5; }
	inline int32_t* get_address_of__status_5() { return &____status_5; }
	inline void set__status_5(int32_t value)
	{
		____status_5 = value;
	}
};

struct X509EnhancedKeyUsageExtension_t3022440765_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::<>f__switch$mapE
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapE_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_6() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t3022440765_StaticFields, ___U3CU3Ef__switchU24mapE_6)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapE_6() const { return ___U3CU3Ef__switchU24mapE_6; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapE_6() { return &___U3CU3Ef__switchU24mapE_6; }
	inline void set_U3CU3Ef__switchU24mapE_6(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapE_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapE_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ENHANCEDKEYUSAGEEXTENSION_T3022440765_H
#ifndef REGULAREXPRESSION_T679831448_H
#define REGULAREXPRESSION_T679831448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.RegularExpression
struct  RegularExpression_t679831448  : public Group_t1053066773
{
public:
	// System.Int32 System.Text.RegularExpressions.Syntax.RegularExpression::group_count
	int32_t ___group_count_1;

public:
	inline static int32_t get_offset_of_group_count_1() { return static_cast<int32_t>(offsetof(RegularExpression_t679831448, ___group_count_1)); }
	inline int32_t get_group_count_1() const { return ___group_count_1; }
	inline int32_t* get_address_of_group_count_1() { return &___group_count_1; }
	inline void set_group_count_1(int32_t value)
	{
		___group_count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGULAREXPRESSION_T679831448_H
#ifndef X509BASICCONSTRAINTSEXTENSION_T3280602463_H
#define X509BASICCONSTRAINTSEXTENSION_T3280602463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
struct  X509BasicConstraintsExtension_t3280602463  : public X509Extension_t3333384912
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_certificateAuthority
	bool ____certificateAuthority_6;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_hasPathLengthConstraint
	bool ____hasPathLengthConstraint_7;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_pathLengthConstraint
	int32_t ____pathLengthConstraint_8;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_status
	int32_t ____status_9;

public:
	inline static int32_t get_offset_of__certificateAuthority_6() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t3280602463, ____certificateAuthority_6)); }
	inline bool get__certificateAuthority_6() const { return ____certificateAuthority_6; }
	inline bool* get_address_of__certificateAuthority_6() { return &____certificateAuthority_6; }
	inline void set__certificateAuthority_6(bool value)
	{
		____certificateAuthority_6 = value;
	}

	inline static int32_t get_offset_of__hasPathLengthConstraint_7() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t3280602463, ____hasPathLengthConstraint_7)); }
	inline bool get__hasPathLengthConstraint_7() const { return ____hasPathLengthConstraint_7; }
	inline bool* get_address_of__hasPathLengthConstraint_7() { return &____hasPathLengthConstraint_7; }
	inline void set__hasPathLengthConstraint_7(bool value)
	{
		____hasPathLengthConstraint_7 = value;
	}

	inline static int32_t get_offset_of__pathLengthConstraint_8() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t3280602463, ____pathLengthConstraint_8)); }
	inline int32_t get__pathLengthConstraint_8() const { return ____pathLengthConstraint_8; }
	inline int32_t* get_address_of__pathLengthConstraint_8() { return &____pathLengthConstraint_8; }
	inline void set__pathLengthConstraint_8(int32_t value)
	{
		____pathLengthConstraint_8 = value;
	}

	inline static int32_t get_offset_of__status_9() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t3280602463, ____status_9)); }
	inline int32_t get__status_9() const { return ____status_9; }
	inline int32_t* get_address_of__status_9() { return &____status_9; }
	inline void set__status_9(int32_t value)
	{
		____status_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509BASICCONSTRAINTSEXTENSION_T3280602463_H
#ifndef KEY_T2877783392_H
#define KEY_T2877783392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.FactoryCache/Key
struct  Key_t2877783392  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.FactoryCache/Key::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.FactoryCache/Key::options
	int32_t ___options_1;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Key_t2877783392, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_options_1() { return static_cast<int32_t>(offsetof(Key_t2877783392, ___options_1)); }
	inline int32_t get_options_1() const { return ___options_1; }
	inline int32_t* get_address_of_options_1() { return &___options_1; }
	inline void set_options_1(int32_t value)
	{
		___options_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEY_T2877783392_H
#ifndef X509CHAIN_T3119249338_H
#define X509CHAIN_T3119249338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t3119249338  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Chain::location
	int32_t ___location_0;
	// System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509Chain::elements
	X509ChainElementCollection_t762555534 * ___elements_1;
	// System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509Chain::policy
	X509ChainPolicy_t1059945142 * ___policy_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::status
	X509ChainStatusU5BU5D_t2565515437* ___status_3;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509Chain::max_path_length
	int32_t ___max_path_length_5;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Chain::working_issuer_name
	X500DistinguishedName_t2418891383 * ___working_issuer_name_6;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Chain::working_public_key
	AsymmetricAlgorithm_t3837984319 * ___working_public_key_7;
	// System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509Chain::bce_restriction
	X509ChainElement_t2233104543 * ___bce_restriction_8;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::roots
	X509Store_t754901440 * ___roots_9;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::cas
	X509Store_t754901440 * ___cas_10;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Chain::collection
	X509Certificate2Collection_t1517963221 * ___collection_11;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___location_0)); }
	inline int32_t get_location_0() const { return ___location_0; }
	inline int32_t* get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(int32_t value)
	{
		___location_0 = value;
	}

	inline static int32_t get_offset_of_elements_1() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___elements_1)); }
	inline X509ChainElementCollection_t762555534 * get_elements_1() const { return ___elements_1; }
	inline X509ChainElementCollection_t762555534 ** get_address_of_elements_1() { return &___elements_1; }
	inline void set_elements_1(X509ChainElementCollection_t762555534 * value)
	{
		___elements_1 = value;
		Il2CppCodeGenWriteBarrier((&___elements_1), value);
	}

	inline static int32_t get_offset_of_policy_2() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___policy_2)); }
	inline X509ChainPolicy_t1059945142 * get_policy_2() const { return ___policy_2; }
	inline X509ChainPolicy_t1059945142 ** get_address_of_policy_2() { return &___policy_2; }
	inline void set_policy_2(X509ChainPolicy_t1059945142 * value)
	{
		___policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___policy_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___status_3)); }
	inline X509ChainStatusU5BU5D_t2565515437* get_status_3() const { return ___status_3; }
	inline X509ChainStatusU5BU5D_t2565515437** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(X509ChainStatusU5BU5D_t2565515437* value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_max_path_length_5() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___max_path_length_5)); }
	inline int32_t get_max_path_length_5() const { return ___max_path_length_5; }
	inline int32_t* get_address_of_max_path_length_5() { return &___max_path_length_5; }
	inline void set_max_path_length_5(int32_t value)
	{
		___max_path_length_5 = value;
	}

	inline static int32_t get_offset_of_working_issuer_name_6() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___working_issuer_name_6)); }
	inline X500DistinguishedName_t2418891383 * get_working_issuer_name_6() const { return ___working_issuer_name_6; }
	inline X500DistinguishedName_t2418891383 ** get_address_of_working_issuer_name_6() { return &___working_issuer_name_6; }
	inline void set_working_issuer_name_6(X500DistinguishedName_t2418891383 * value)
	{
		___working_issuer_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___working_issuer_name_6), value);
	}

	inline static int32_t get_offset_of_working_public_key_7() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___working_public_key_7)); }
	inline AsymmetricAlgorithm_t3837984319 * get_working_public_key_7() const { return ___working_public_key_7; }
	inline AsymmetricAlgorithm_t3837984319 ** get_address_of_working_public_key_7() { return &___working_public_key_7; }
	inline void set_working_public_key_7(AsymmetricAlgorithm_t3837984319 * value)
	{
		___working_public_key_7 = value;
		Il2CppCodeGenWriteBarrier((&___working_public_key_7), value);
	}

	inline static int32_t get_offset_of_bce_restriction_8() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___bce_restriction_8)); }
	inline X509ChainElement_t2233104543 * get_bce_restriction_8() const { return ___bce_restriction_8; }
	inline X509ChainElement_t2233104543 ** get_address_of_bce_restriction_8() { return &___bce_restriction_8; }
	inline void set_bce_restriction_8(X509ChainElement_t2233104543 * value)
	{
		___bce_restriction_8 = value;
		Il2CppCodeGenWriteBarrier((&___bce_restriction_8), value);
	}

	inline static int32_t get_offset_of_roots_9() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___roots_9)); }
	inline X509Store_t754901440 * get_roots_9() const { return ___roots_9; }
	inline X509Store_t754901440 ** get_address_of_roots_9() { return &___roots_9; }
	inline void set_roots_9(X509Store_t754901440 * value)
	{
		___roots_9 = value;
		Il2CppCodeGenWriteBarrier((&___roots_9), value);
	}

	inline static int32_t get_offset_of_cas_10() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___cas_10)); }
	inline X509Store_t754901440 * get_cas_10() const { return ___cas_10; }
	inline X509Store_t754901440 ** get_address_of_cas_10() { return &___cas_10; }
	inline void set_cas_10(X509Store_t754901440 * value)
	{
		___cas_10 = value;
		Il2CppCodeGenWriteBarrier((&___cas_10), value);
	}

	inline static int32_t get_offset_of_collection_11() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338, ___collection_11)); }
	inline X509Certificate2Collection_t1517963221 * get_collection_11() const { return ___collection_11; }
	inline X509Certificate2Collection_t1517963221 ** get_address_of_collection_11() { return &___collection_11; }
	inline void set_collection_11(X509Certificate2Collection_t1517963221 * value)
	{
		___collection_11 = value;
		Il2CppCodeGenWriteBarrier((&___collection_11), value);
	}
};

struct X509Chain_t3119249338_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::Empty
	X509ChainStatusU5BU5D_t2565515437* ___Empty_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapB
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapB_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapC
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapC_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapD
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24mapD_14;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338_StaticFields, ___Empty_4)); }
	inline X509ChainStatusU5BU5D_t2565515437* get_Empty_4() const { return ___Empty_4; }
	inline X509ChainStatusU5BU5D_t2565515437** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(X509ChainStatusU5BU5D_t2565515437* value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_12() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338_StaticFields, ___U3CU3Ef__switchU24mapB_12)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapB_12() const { return ___U3CU3Ef__switchU24mapB_12; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapB_12() { return &___U3CU3Ef__switchU24mapB_12; }
	inline void set_U3CU3Ef__switchU24mapB_12(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapB_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_13() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338_StaticFields, ___U3CU3Ef__switchU24mapC_13)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapC_13() const { return ___U3CU3Ef__switchU24mapC_13; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapC_13() { return &___U3CU3Ef__switchU24mapC_13; }
	inline void set_U3CU3Ef__switchU24mapC_13(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapC_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_14() { return static_cast<int32_t>(offsetof(X509Chain_t3119249338_StaticFields, ___U3CU3Ef__switchU24mapD_14)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24mapD_14() const { return ___U3CU3Ef__switchU24mapD_14; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24mapD_14() { return &___U3CU3Ef__switchU24mapD_14; }
	inline void set_U3CU3Ef__switchU24mapD_14(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24mapD_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapD_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T3119249338_H
#ifndef X509CHAINELEMENT_T2233104543_H
#define X509CHAINELEMENT_T2233104543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElement
struct  X509ChainElement_t2233104543  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainElement::certificate
	X509Certificate2_t770297212 * ___certificate_0;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainElement::status
	X509ChainStatusU5BU5D_t2565515437* ___status_1;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainElement::info
	String_t* ___info_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainElement::compressed_status_flags
	int32_t ___compressed_status_flags_3;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(X509ChainElement_t2233104543, ___certificate_0)); }
	inline X509Certificate2_t770297212 * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate2_t770297212 ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate2_t770297212 * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(X509ChainElement_t2233104543, ___status_1)); }
	inline X509ChainStatusU5BU5D_t2565515437* get_status_1() const { return ___status_1; }
	inline X509ChainStatusU5BU5D_t2565515437** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(X509ChainStatusU5BU5D_t2565515437* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(X509ChainElement_t2233104543, ___info_2)); }
	inline String_t* get_info_2() const { return ___info_2; }
	inline String_t** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(String_t* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_compressed_status_flags_3() { return static_cast<int32_t>(offsetof(X509ChainElement_t2233104543, ___compressed_status_flags_3)); }
	inline int32_t get_compressed_status_flags_3() const { return ___compressed_status_flags_3; }
	inline int32_t* get_address_of_compressed_status_flags_3() { return &___compressed_status_flags_3; }
	inline void set_compressed_status_flags_3(int32_t value)
	{
		___compressed_status_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENT_T2233104543_H
#ifndef X509CHAINSTATUS_T3509631940_H
#define X509CHAINSTATUS_T3509631940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t3509631940 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t3509631940, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t3509631940, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t3509631940_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t3509631940_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T3509631940_H
#ifndef X509KEYUSAGEEXTENSION_T1028005009_H
#define X509KEYUSAGEEXTENSION_T1028005009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
struct  X509KeyUsageExtension_t1028005009  : public X509Extension_t3333384912
{
public:
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_keyUsages
	int32_t ____keyUsages_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__keyUsages_7() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1028005009, ____keyUsages_7)); }
	inline int32_t get__keyUsages_7() const { return ____keyUsages_7; }
	inline int32_t* get_address_of__keyUsages_7() { return &____keyUsages_7; }
	inline void set__keyUsages_7(int32_t value)
	{
		____keyUsages_7 = value;
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1028005009, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEEXTENSION_T1028005009_H
#ifndef REGEX_T2687244226_H
#define REGEX_T2687244226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t2687244226  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.Regex::machineFactory
	RuntimeObject* ___machineFactory_1;
	// System.Collections.IDictionary System.Text.RegularExpressions.Regex::mapping
	RuntimeObject* ___mapping_2;
	// System.Int32 System.Text.RegularExpressions.Regex::group_count
	int32_t ___group_count_3;
	// System.Int32 System.Text.RegularExpressions.Regex::gap
	int32_t ___gap_4;
	// System.String[] System.Text.RegularExpressions.Regex::group_names
	StringU5BU5D_t369357837* ___group_names_5;
	// System.Int32[] System.Text.RegularExpressions.Regex::group_numbers
	Int32U5BU5D_t3565237794* ___group_numbers_6;
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_7;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_8;

public:
	inline static int32_t get_offset_of_machineFactory_1() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___machineFactory_1)); }
	inline RuntimeObject* get_machineFactory_1() const { return ___machineFactory_1; }
	inline RuntimeObject** get_address_of_machineFactory_1() { return &___machineFactory_1; }
	inline void set_machineFactory_1(RuntimeObject* value)
	{
		___machineFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___machineFactory_1), value);
	}

	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___mapping_2)); }
	inline RuntimeObject* get_mapping_2() const { return ___mapping_2; }
	inline RuntimeObject** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(RuntimeObject* value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_group_count_3() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___group_count_3)); }
	inline int32_t get_group_count_3() const { return ___group_count_3; }
	inline int32_t* get_address_of_group_count_3() { return &___group_count_3; }
	inline void set_group_count_3(int32_t value)
	{
		___group_count_3 = value;
	}

	inline static int32_t get_offset_of_gap_4() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___gap_4)); }
	inline int32_t get_gap_4() const { return ___gap_4; }
	inline int32_t* get_address_of_gap_4() { return &___gap_4; }
	inline void set_gap_4(int32_t value)
	{
		___gap_4 = value;
	}

	inline static int32_t get_offset_of_group_names_5() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___group_names_5)); }
	inline StringU5BU5D_t369357837* get_group_names_5() const { return ___group_names_5; }
	inline StringU5BU5D_t369357837** get_address_of_group_names_5() { return &___group_names_5; }
	inline void set_group_names_5(StringU5BU5D_t369357837* value)
	{
		___group_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___group_names_5), value);
	}

	inline static int32_t get_offset_of_group_numbers_6() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___group_numbers_6)); }
	inline Int32U5BU5D_t3565237794* get_group_numbers_6() const { return ___group_numbers_6; }
	inline Int32U5BU5D_t3565237794** get_address_of_group_numbers_6() { return &___group_numbers_6; }
	inline void set_group_numbers_6(Int32U5BU5D_t3565237794* value)
	{
		___group_numbers_6 = value;
		Il2CppCodeGenWriteBarrier((&___group_numbers_6), value);
	}

	inline static int32_t get_offset_of_pattern_7() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___pattern_7)); }
	inline String_t* get_pattern_7() const { return ___pattern_7; }
	inline String_t** get_address_of_pattern_7() { return &___pattern_7; }
	inline void set_pattern_7(String_t* value)
	{
		___pattern_7 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_7), value);
	}

	inline static int32_t get_offset_of_roptions_8() { return static_cast<int32_t>(offsetof(Regex_t2687244226, ___roptions_8)); }
	inline int32_t get_roptions_8() const { return ___roptions_8; }
	inline int32_t* get_address_of_roptions_8() { return &___roptions_8; }
	inline void set_roptions_8(int32_t value)
	{
		___roptions_8 = value;
	}
};

struct Regex_t2687244226_StaticFields
{
public:
	// System.Text.RegularExpressions.FactoryCache System.Text.RegularExpressions.Regex::cache
	FactoryCache_t3371693116 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Regex_t2687244226_StaticFields, ___cache_0)); }
	inline FactoryCache_t3371693116 * get_cache_0() const { return ___cache_0; }
	inline FactoryCache_t3371693116 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(FactoryCache_t3371693116 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T2687244226_H
#ifndef COSTDELEGATE_T4259336196_H
#define COSTDELEGATE_T4259336196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection/CostDelegate
struct  CostDelegate_t4259336196  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COSTDELEGATE_T4259336196_H
#ifndef SERVICEPOINT_T3316975054_H
#define SERVICEPOINT_T3316975054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_t3316975054  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_t2779109183 * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t972933412  ___idleSince_4;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_5;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_6;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_7;
	// System.Object System.Net.ServicePoint::locker
	RuntimeObject * ___locker_8;
	// System.Object System.Net.ServicePoint::hostE
	RuntimeObject * ___hostE_9;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_10;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___uri_0)); }
	inline Uri_t2779109183 * get_uri_0() const { return ___uri_0; }
	inline Uri_t2779109183 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t2779109183 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___idleSince_4)); }
	inline DateTime_t972933412  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t972933412 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t972933412  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_usesProxy_5() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___usesProxy_5)); }
	inline bool get_usesProxy_5() const { return ___usesProxy_5; }
	inline bool* get_address_of_usesProxy_5() { return &___usesProxy_5; }
	inline void set_usesProxy_5(bool value)
	{
		___usesProxy_5 = value;
	}

	inline static int32_t get_offset_of_sendContinue_6() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___sendContinue_6)); }
	inline bool get_sendContinue_6() const { return ___sendContinue_6; }
	inline bool* get_address_of_sendContinue_6() { return &___sendContinue_6; }
	inline void set_sendContinue_6(bool value)
	{
		___sendContinue_6 = value;
	}

	inline static int32_t get_offset_of_useConnect_7() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___useConnect_7)); }
	inline bool get_useConnect_7() const { return ___useConnect_7; }
	inline bool* get_address_of_useConnect_7() { return &___useConnect_7; }
	inline void set_useConnect_7(bool value)
	{
		___useConnect_7 = value;
	}

	inline static int32_t get_offset_of_locker_8() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___locker_8)); }
	inline RuntimeObject * get_locker_8() const { return ___locker_8; }
	inline RuntimeObject ** get_address_of_locker_8() { return &___locker_8; }
	inline void set_locker_8(RuntimeObject * value)
	{
		___locker_8 = value;
		Il2CppCodeGenWriteBarrier((&___locker_8), value);
	}

	inline static int32_t get_offset_of_hostE_9() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___hostE_9)); }
	inline RuntimeObject * get_hostE_9() const { return ___hostE_9; }
	inline RuntimeObject ** get_address_of_hostE_9() { return &___hostE_9; }
	inline void set_hostE_9(RuntimeObject * value)
	{
		___hostE_9 = value;
		Il2CppCodeGenWriteBarrier((&___hostE_9), value);
	}

	inline static int32_t get_offset_of_useNagle_10() { return static_cast<int32_t>(offsetof(ServicePoint_t3316975054, ___useNagle_10)); }
	inline bool get_useNagle_10() const { return ___useNagle_10; }
	inline bool* get_address_of_useNagle_10() { return &___useNagle_10; }
	inline void set_useNagle_10(bool value)
	{
		___useNagle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINT_T3316975054_H
#ifndef X509CHAINPOLICY_T1059945142_H
#define X509CHAINPOLICY_T1059945142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct  X509ChainPolicy_t1059945142  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::apps
	OidCollection_t2127506323 * ___apps_0;
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::cert
	OidCollection_t2127506323 * ___cert_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainPolicy::store
	X509Certificate2Collection_t1517963221 * ___store_2;
	// System.Security.Cryptography.X509Certificates.X509RevocationFlag System.Security.Cryptography.X509Certificates.X509ChainPolicy::rflag
	int32_t ___rflag_3;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.Security.Cryptography.X509Certificates.X509ChainPolicy::mode
	int32_t ___mode_4;
	// System.TimeSpan System.Security.Cryptography.X509Certificates.X509ChainPolicy::timeout
	TimeSpan_t457147580  ___timeout_5;
	// System.Security.Cryptography.X509Certificates.X509VerificationFlags System.Security.Cryptography.X509Certificates.X509ChainPolicy::vflags
	int32_t ___vflags_6;
	// System.DateTime System.Security.Cryptography.X509Certificates.X509ChainPolicy::vtime
	DateTime_t972933412  ___vtime_7;

public:
	inline static int32_t get_offset_of_apps_0() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___apps_0)); }
	inline OidCollection_t2127506323 * get_apps_0() const { return ___apps_0; }
	inline OidCollection_t2127506323 ** get_address_of_apps_0() { return &___apps_0; }
	inline void set_apps_0(OidCollection_t2127506323 * value)
	{
		___apps_0 = value;
		Il2CppCodeGenWriteBarrier((&___apps_0), value);
	}

	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___cert_1)); }
	inline OidCollection_t2127506323 * get_cert_1() const { return ___cert_1; }
	inline OidCollection_t2127506323 ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(OidCollection_t2127506323 * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier((&___cert_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___store_2)); }
	inline X509Certificate2Collection_t1517963221 * get_store_2() const { return ___store_2; }
	inline X509Certificate2Collection_t1517963221 ** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(X509Certificate2Collection_t1517963221 * value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_rflag_3() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___rflag_3)); }
	inline int32_t get_rflag_3() const { return ___rflag_3; }
	inline int32_t* get_address_of_rflag_3() { return &___rflag_3; }
	inline void set_rflag_3(int32_t value)
	{
		___rflag_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_timeout_5() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___timeout_5)); }
	inline TimeSpan_t457147580  get_timeout_5() const { return ___timeout_5; }
	inline TimeSpan_t457147580 * get_address_of_timeout_5() { return &___timeout_5; }
	inline void set_timeout_5(TimeSpan_t457147580  value)
	{
		___timeout_5 = value;
	}

	inline static int32_t get_offset_of_vflags_6() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___vflags_6)); }
	inline int32_t get_vflags_6() const { return ___vflags_6; }
	inline int32_t* get_address_of_vflags_6() { return &___vflags_6; }
	inline void set_vflags_6(int32_t value)
	{
		___vflags_6 = value;
	}

	inline static int32_t get_offset_of_vtime_7() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t1059945142, ___vtime_7)); }
	inline DateTime_t972933412  get_vtime_7() const { return ___vtime_7; }
	inline DateTime_t972933412 * get_address_of_vtime_7() { return &___vtime_7; }
	inline void set_vtime_7(DateTime_t972933412  value)
	{
		___vtime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINPOLICY_T1059945142_H
#ifndef HTTPWEBREQUEST_T1705825389_H
#define HTTPWEBREQUEST_T1705825389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest
struct  HttpWebRequest_t1705825389  : public WebRequest_t3661130957
{
public:
	// System.Uri System.Net.HttpWebRequest::requestUri
	Uri_t2779109183 * ___requestUri_6;
	// System.Uri System.Net.HttpWebRequest::actualUri
	Uri_t2779109183 * ___actualUri_7;
	// System.Boolean System.Net.HttpWebRequest::hostChanged
	bool ___hostChanged_8;
	// System.Boolean System.Net.HttpWebRequest::allowAutoRedirect
	bool ___allowAutoRedirect_9;
	// System.Boolean System.Net.HttpWebRequest::allowBuffering
	bool ___allowBuffering_10;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.HttpWebRequest::certificates
	X509CertificateCollection_t3590496811 * ___certificates_11;
	// System.String System.Net.HttpWebRequest::connectionGroup
	String_t* ___connectionGroup_12;
	// System.Int64 System.Net.HttpWebRequest::contentLength
	int64_t ___contentLength_13;
	// System.Net.WebHeaderCollection System.Net.HttpWebRequest::webHeaders
	WebHeaderCollection_t1238597439 * ___webHeaders_14;
	// System.Boolean System.Net.HttpWebRequest::keepAlive
	bool ___keepAlive_15;
	// System.Int32 System.Net.HttpWebRequest::maxAutoRedirect
	int32_t ___maxAutoRedirect_16;
	// System.String System.Net.HttpWebRequest::mediaType
	String_t* ___mediaType_17;
	// System.String System.Net.HttpWebRequest::method
	String_t* ___method_18;
	// System.String System.Net.HttpWebRequest::initialMethod
	String_t* ___initialMethod_19;
	// System.Boolean System.Net.HttpWebRequest::pipelined
	bool ___pipelined_20;
	// System.Version System.Net.HttpWebRequest::version
	Version_t1723234366 * ___version_21;
	// System.Net.IWebProxy System.Net.HttpWebRequest::proxy
	RuntimeObject* ___proxy_22;
	// System.Boolean System.Net.HttpWebRequest::sendChunked
	bool ___sendChunked_23;
	// System.Net.ServicePoint System.Net.HttpWebRequest::servicePoint
	ServicePoint_t3316975054 * ___servicePoint_24;
	// System.Int32 System.Net.HttpWebRequest::timeout
	int32_t ___timeout_25;
	// System.Int32 System.Net.HttpWebRequest::redirects
	int32_t ___redirects_26;
	// System.Object System.Net.HttpWebRequest::locker
	RuntimeObject * ___locker_27;
	// System.Int32 System.Net.HttpWebRequest::readWriteTimeout
	int32_t ___readWriteTimeout_29;

public:
	inline static int32_t get_offset_of_requestUri_6() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___requestUri_6)); }
	inline Uri_t2779109183 * get_requestUri_6() const { return ___requestUri_6; }
	inline Uri_t2779109183 ** get_address_of_requestUri_6() { return &___requestUri_6; }
	inline void set_requestUri_6(Uri_t2779109183 * value)
	{
		___requestUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_6), value);
	}

	inline static int32_t get_offset_of_actualUri_7() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___actualUri_7)); }
	inline Uri_t2779109183 * get_actualUri_7() const { return ___actualUri_7; }
	inline Uri_t2779109183 ** get_address_of_actualUri_7() { return &___actualUri_7; }
	inline void set_actualUri_7(Uri_t2779109183 * value)
	{
		___actualUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___actualUri_7), value);
	}

	inline static int32_t get_offset_of_hostChanged_8() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___hostChanged_8)); }
	inline bool get_hostChanged_8() const { return ___hostChanged_8; }
	inline bool* get_address_of_hostChanged_8() { return &___hostChanged_8; }
	inline void set_hostChanged_8(bool value)
	{
		___hostChanged_8 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_9() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___allowAutoRedirect_9)); }
	inline bool get_allowAutoRedirect_9() const { return ___allowAutoRedirect_9; }
	inline bool* get_address_of_allowAutoRedirect_9() { return &___allowAutoRedirect_9; }
	inline void set_allowAutoRedirect_9(bool value)
	{
		___allowAutoRedirect_9 = value;
	}

	inline static int32_t get_offset_of_allowBuffering_10() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___allowBuffering_10)); }
	inline bool get_allowBuffering_10() const { return ___allowBuffering_10; }
	inline bool* get_address_of_allowBuffering_10() { return &___allowBuffering_10; }
	inline void set_allowBuffering_10(bool value)
	{
		___allowBuffering_10 = value;
	}

	inline static int32_t get_offset_of_certificates_11() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___certificates_11)); }
	inline X509CertificateCollection_t3590496811 * get_certificates_11() const { return ___certificates_11; }
	inline X509CertificateCollection_t3590496811 ** get_address_of_certificates_11() { return &___certificates_11; }
	inline void set_certificates_11(X509CertificateCollection_t3590496811 * value)
	{
		___certificates_11 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_11), value);
	}

	inline static int32_t get_offset_of_connectionGroup_12() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___connectionGroup_12)); }
	inline String_t* get_connectionGroup_12() const { return ___connectionGroup_12; }
	inline String_t** get_address_of_connectionGroup_12() { return &___connectionGroup_12; }
	inline void set_connectionGroup_12(String_t* value)
	{
		___connectionGroup_12 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_12), value);
	}

	inline static int32_t get_offset_of_contentLength_13() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___contentLength_13)); }
	inline int64_t get_contentLength_13() const { return ___contentLength_13; }
	inline int64_t* get_address_of_contentLength_13() { return &___contentLength_13; }
	inline void set_contentLength_13(int64_t value)
	{
		___contentLength_13 = value;
	}

	inline static int32_t get_offset_of_webHeaders_14() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___webHeaders_14)); }
	inline WebHeaderCollection_t1238597439 * get_webHeaders_14() const { return ___webHeaders_14; }
	inline WebHeaderCollection_t1238597439 ** get_address_of_webHeaders_14() { return &___webHeaders_14; }
	inline void set_webHeaders_14(WebHeaderCollection_t1238597439 * value)
	{
		___webHeaders_14 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_14), value);
	}

	inline static int32_t get_offset_of_keepAlive_15() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___keepAlive_15)); }
	inline bool get_keepAlive_15() const { return ___keepAlive_15; }
	inline bool* get_address_of_keepAlive_15() { return &___keepAlive_15; }
	inline void set_keepAlive_15(bool value)
	{
		___keepAlive_15 = value;
	}

	inline static int32_t get_offset_of_maxAutoRedirect_16() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___maxAutoRedirect_16)); }
	inline int32_t get_maxAutoRedirect_16() const { return ___maxAutoRedirect_16; }
	inline int32_t* get_address_of_maxAutoRedirect_16() { return &___maxAutoRedirect_16; }
	inline void set_maxAutoRedirect_16(int32_t value)
	{
		___maxAutoRedirect_16 = value;
	}

	inline static int32_t get_offset_of_mediaType_17() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___mediaType_17)); }
	inline String_t* get_mediaType_17() const { return ___mediaType_17; }
	inline String_t** get_address_of_mediaType_17() { return &___mediaType_17; }
	inline void set_mediaType_17(String_t* value)
	{
		___mediaType_17 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_17), value);
	}

	inline static int32_t get_offset_of_method_18() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___method_18)); }
	inline String_t* get_method_18() const { return ___method_18; }
	inline String_t** get_address_of_method_18() { return &___method_18; }
	inline void set_method_18(String_t* value)
	{
		___method_18 = value;
		Il2CppCodeGenWriteBarrier((&___method_18), value);
	}

	inline static int32_t get_offset_of_initialMethod_19() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___initialMethod_19)); }
	inline String_t* get_initialMethod_19() const { return ___initialMethod_19; }
	inline String_t** get_address_of_initialMethod_19() { return &___initialMethod_19; }
	inline void set_initialMethod_19(String_t* value)
	{
		___initialMethod_19 = value;
		Il2CppCodeGenWriteBarrier((&___initialMethod_19), value);
	}

	inline static int32_t get_offset_of_pipelined_20() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___pipelined_20)); }
	inline bool get_pipelined_20() const { return ___pipelined_20; }
	inline bool* get_address_of_pipelined_20() { return &___pipelined_20; }
	inline void set_pipelined_20(bool value)
	{
		___pipelined_20 = value;
	}

	inline static int32_t get_offset_of_version_21() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___version_21)); }
	inline Version_t1723234366 * get_version_21() const { return ___version_21; }
	inline Version_t1723234366 ** get_address_of_version_21() { return &___version_21; }
	inline void set_version_21(Version_t1723234366 * value)
	{
		___version_21 = value;
		Il2CppCodeGenWriteBarrier((&___version_21), value);
	}

	inline static int32_t get_offset_of_proxy_22() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___proxy_22)); }
	inline RuntimeObject* get_proxy_22() const { return ___proxy_22; }
	inline RuntimeObject** get_address_of_proxy_22() { return &___proxy_22; }
	inline void set_proxy_22(RuntimeObject* value)
	{
		___proxy_22 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_22), value);
	}

	inline static int32_t get_offset_of_sendChunked_23() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___sendChunked_23)); }
	inline bool get_sendChunked_23() const { return ___sendChunked_23; }
	inline bool* get_address_of_sendChunked_23() { return &___sendChunked_23; }
	inline void set_sendChunked_23(bool value)
	{
		___sendChunked_23 = value;
	}

	inline static int32_t get_offset_of_servicePoint_24() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___servicePoint_24)); }
	inline ServicePoint_t3316975054 * get_servicePoint_24() const { return ___servicePoint_24; }
	inline ServicePoint_t3316975054 ** get_address_of_servicePoint_24() { return &___servicePoint_24; }
	inline void set_servicePoint_24(ServicePoint_t3316975054 * value)
	{
		___servicePoint_24 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_24), value);
	}

	inline static int32_t get_offset_of_timeout_25() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___timeout_25)); }
	inline int32_t get_timeout_25() const { return ___timeout_25; }
	inline int32_t* get_address_of_timeout_25() { return &___timeout_25; }
	inline void set_timeout_25(int32_t value)
	{
		___timeout_25 = value;
	}

	inline static int32_t get_offset_of_redirects_26() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___redirects_26)); }
	inline int32_t get_redirects_26() const { return ___redirects_26; }
	inline int32_t* get_address_of_redirects_26() { return &___redirects_26; }
	inline void set_redirects_26(int32_t value)
	{
		___redirects_26 = value;
	}

	inline static int32_t get_offset_of_locker_27() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___locker_27)); }
	inline RuntimeObject * get_locker_27() const { return ___locker_27; }
	inline RuntimeObject ** get_address_of_locker_27() { return &___locker_27; }
	inline void set_locker_27(RuntimeObject * value)
	{
		___locker_27 = value;
		Il2CppCodeGenWriteBarrier((&___locker_27), value);
	}

	inline static int32_t get_offset_of_readWriteTimeout_29() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389, ___readWriteTimeout_29)); }
	inline int32_t get_readWriteTimeout_29() const { return ___readWriteTimeout_29; }
	inline int32_t* get_address_of_readWriteTimeout_29() { return &___readWriteTimeout_29; }
	inline void set_readWriteTimeout_29(int32_t value)
	{
		___readWriteTimeout_29 = value;
	}
};

struct HttpWebRequest_t1705825389_StaticFields
{
public:
	// System.Int32 System.Net.HttpWebRequest::defaultMaxResponseHeadersLength
	int32_t ___defaultMaxResponseHeadersLength_28;

public:
	inline static int32_t get_offset_of_defaultMaxResponseHeadersLength_28() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1705825389_StaticFields, ___defaultMaxResponseHeadersLength_28)); }
	inline int32_t get_defaultMaxResponseHeadersLength_28() const { return ___defaultMaxResponseHeadersLength_28; }
	inline int32_t* get_address_of_defaultMaxResponseHeadersLength_28() { return &___defaultMaxResponseHeadersLength_28; }
	inline void set_defaultMaxResponseHeadersLength_28(int32_t value)
	{
		___defaultMaxResponseHeadersLength_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUEST_T1705825389_H
#ifndef FTPWEBREQUEST_T3617697923_H
#define FTPWEBREQUEST_T3617697923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t3617697923  : public WebRequest_t3661130957
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t2779109183 * ___requestUri_6;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	RuntimeObject* ___proxy_7;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_8;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_9;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_10;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_11;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_12;
	// System.Object System.Net.FtpWebRequest::locker
	RuntimeObject * ___locker_13;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::callback
	RemoteCertificateValidationCallback_t2804961572 * ___callback_15;

public:
	inline static int32_t get_offset_of_requestUri_6() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___requestUri_6)); }
	inline Uri_t2779109183 * get_requestUri_6() const { return ___requestUri_6; }
	inline Uri_t2779109183 ** get_address_of_requestUri_6() { return &___requestUri_6; }
	inline void set_requestUri_6(Uri_t2779109183 * value)
	{
		___requestUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_6), value);
	}

	inline static int32_t get_offset_of_proxy_7() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___proxy_7)); }
	inline RuntimeObject* get_proxy_7() const { return ___proxy_7; }
	inline RuntimeObject** get_address_of_proxy_7() { return &___proxy_7; }
	inline void set_proxy_7(RuntimeObject* value)
	{
		___proxy_7 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_7), value);
	}

	inline static int32_t get_offset_of_timeout_8() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___timeout_8)); }
	inline int32_t get_timeout_8() const { return ___timeout_8; }
	inline int32_t* get_address_of_timeout_8() { return &___timeout_8; }
	inline void set_timeout_8(int32_t value)
	{
		___timeout_8 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_9() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___rwTimeout_9)); }
	inline int32_t get_rwTimeout_9() const { return ___rwTimeout_9; }
	inline int32_t* get_address_of_rwTimeout_9() { return &___rwTimeout_9; }
	inline void set_rwTimeout_9(int32_t value)
	{
		___rwTimeout_9 = value;
	}

	inline static int32_t get_offset_of_binary_10() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___binary_10)); }
	inline bool get_binary_10() const { return ___binary_10; }
	inline bool* get_address_of_binary_10() { return &___binary_10; }
	inline void set_binary_10(bool value)
	{
		___binary_10 = value;
	}

	inline static int32_t get_offset_of_usePassive_11() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___usePassive_11)); }
	inline bool get_usePassive_11() const { return ___usePassive_11; }
	inline bool* get_address_of_usePassive_11() { return &___usePassive_11; }
	inline void set_usePassive_11(bool value)
	{
		___usePassive_11 = value;
	}

	inline static int32_t get_offset_of_method_12() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___method_12)); }
	inline String_t* get_method_12() const { return ___method_12; }
	inline String_t** get_address_of_method_12() { return &___method_12; }
	inline void set_method_12(String_t* value)
	{
		___method_12 = value;
		Il2CppCodeGenWriteBarrier((&___method_12), value);
	}

	inline static int32_t get_offset_of_locker_13() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___locker_13)); }
	inline RuntimeObject * get_locker_13() const { return ___locker_13; }
	inline RuntimeObject ** get_address_of_locker_13() { return &___locker_13; }
	inline void set_locker_13(RuntimeObject * value)
	{
		___locker_13 = value;
		Il2CppCodeGenWriteBarrier((&___locker_13), value);
	}

	inline static int32_t get_offset_of_callback_15() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923, ___callback_15)); }
	inline RemoteCertificateValidationCallback_t2804961572 * get_callback_15() const { return ___callback_15; }
	inline RemoteCertificateValidationCallback_t2804961572 ** get_address_of_callback_15() { return &___callback_15; }
	inline void set_callback_15(RemoteCertificateValidationCallback_t2804961572 * value)
	{
		___callback_15 = value;
		Il2CppCodeGenWriteBarrier((&___callback_15), value);
	}
};

struct FtpWebRequest_t3617697923_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t369357837* ___supportedCommands_14;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::<>f__am$cache1C
	RemoteCertificateValidationCallback_t2804961572 * ___U3CU3Ef__amU24cache1C_16;

public:
	inline static int32_t get_offset_of_supportedCommands_14() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923_StaticFields, ___supportedCommands_14)); }
	inline StringU5BU5D_t369357837* get_supportedCommands_14() const { return ___supportedCommands_14; }
	inline StringU5BU5D_t369357837** get_address_of_supportedCommands_14() { return &___supportedCommands_14; }
	inline void set_supportedCommands_14(StringU5BU5D_t369357837* value)
	{
		___supportedCommands_14 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCommands_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_16() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3617697923_StaticFields, ___U3CU3Ef__amU24cache1C_16)); }
	inline RemoteCertificateValidationCallback_t2804961572 * get_U3CU3Ef__amU24cache1C_16() const { return ___U3CU3Ef__amU24cache1C_16; }
	inline RemoteCertificateValidationCallback_t2804961572 ** get_address_of_U3CU3Ef__amU24cache1C_16() { return &___U3CU3Ef__amU24cache1C_16; }
	inline void set_U3CU3Ef__amU24cache1C_16(RemoteCertificateValidationCallback_t2804961572 * value)
	{
		___U3CU3Ef__amU24cache1C_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1C_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBREQUEST_T3617697923_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (FtpWebRequest_t3617697923), -1, sizeof(FtpWebRequest_t3617697923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1100[11] = 
{
	FtpWebRequest_t3617697923::get_offset_of_requestUri_6(),
	FtpWebRequest_t3617697923::get_offset_of_proxy_7(),
	FtpWebRequest_t3617697923::get_offset_of_timeout_8(),
	FtpWebRequest_t3617697923::get_offset_of_rwTimeout_9(),
	FtpWebRequest_t3617697923::get_offset_of_binary_10(),
	FtpWebRequest_t3617697923::get_offset_of_usePassive_11(),
	FtpWebRequest_t3617697923::get_offset_of_method_12(),
	FtpWebRequest_t3617697923::get_offset_of_locker_13(),
	FtpWebRequest_t3617697923_StaticFields::get_offset_of_supportedCommands_14(),
	FtpWebRequest_t3617697923::get_offset_of_callback_15(),
	FtpWebRequest_t3617697923_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (GlobalProxySelection_t3979368509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (HttpRequestCreator_t2740415761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (HttpVersion_t2726102792), -1, sizeof(HttpVersion_t2726102792_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1103[2] = 
{
	HttpVersion_t2726102792_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t2726102792_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (HttpWebRequest_t1705825389), -1, sizeof(HttpWebRequest_t1705825389_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1104[24] = 
{
	HttpWebRequest_t1705825389::get_offset_of_requestUri_6(),
	HttpWebRequest_t1705825389::get_offset_of_actualUri_7(),
	HttpWebRequest_t1705825389::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1705825389::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1705825389::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1705825389::get_offset_of_certificates_11(),
	HttpWebRequest_t1705825389::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1705825389::get_offset_of_contentLength_13(),
	HttpWebRequest_t1705825389::get_offset_of_webHeaders_14(),
	HttpWebRequest_t1705825389::get_offset_of_keepAlive_15(),
	HttpWebRequest_t1705825389::get_offset_of_maxAutoRedirect_16(),
	HttpWebRequest_t1705825389::get_offset_of_mediaType_17(),
	HttpWebRequest_t1705825389::get_offset_of_method_18(),
	HttpWebRequest_t1705825389::get_offset_of_initialMethod_19(),
	HttpWebRequest_t1705825389::get_offset_of_pipelined_20(),
	HttpWebRequest_t1705825389::get_offset_of_version_21(),
	HttpWebRequest_t1705825389::get_offset_of_proxy_22(),
	HttpWebRequest_t1705825389::get_offset_of_sendChunked_23(),
	HttpWebRequest_t1705825389::get_offset_of_servicePoint_24(),
	HttpWebRequest_t1705825389::get_offset_of_timeout_25(),
	HttpWebRequest_t1705825389::get_offset_of_redirects_26(),
	HttpWebRequest_t1705825389::get_offset_of_locker_27(),
	HttpWebRequest_t1705825389_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_28(),
	HttpWebRequest_t1705825389::get_offset_of_readWriteTimeout_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (IPAddress_t1685194060), -1, sizeof(IPAddress_t1685194060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1107[11] = 
{
	IPAddress_t1685194060::get_offset_of_m_Address_0(),
	IPAddress_t1685194060::get_offset_of_m_Family_1(),
	IPAddress_t1685194060::get_offset_of_m_Numbers_2(),
	IPAddress_t1685194060::get_offset_of_m_ScopeId_3(),
	IPAddress_t1685194060_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1685194060_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1685194060_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1685194060_StaticFields::get_offset_of_None_7(),
	IPAddress_t1685194060_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1685194060_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1685194060_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (IPv6Address_t987378652), -1, sizeof(IPv6Address_t987378652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1108[5] = 
{
	IPv6Address_t987378652::get_offset_of_address_0(),
	IPv6Address_t987378652::get_offset_of_prefixLength_1(),
	IPv6Address_t987378652::get_offset_of_scopeId_2(),
	IPv6Address_t987378652_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t987378652_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (SecurityProtocolType_t352736498)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1111[3] = 
{
	SecurityProtocolType_t352736498::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (ServicePoint_t3316975054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1112[11] = 
{
	ServicePoint_t3316975054::get_offset_of_uri_0(),
	ServicePoint_t3316975054::get_offset_of_connectionLimit_1(),
	ServicePoint_t3316975054::get_offset_of_maxIdleTime_2(),
	ServicePoint_t3316975054::get_offset_of_currentConnections_3(),
	ServicePoint_t3316975054::get_offset_of_idleSince_4(),
	ServicePoint_t3316975054::get_offset_of_usesProxy_5(),
	ServicePoint_t3316975054::get_offset_of_sendContinue_6(),
	ServicePoint_t3316975054::get_offset_of_useConnect_7(),
	ServicePoint_t3316975054::get_offset_of_locker_8(),
	ServicePoint_t3316975054::get_offset_of_hostE_9(),
	ServicePoint_t3316975054::get_offset_of_useNagle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (ServicePointManager_t382256627), -1, sizeof(ServicePointManager_t382256627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1113[10] = 
{
	ServicePointManager_t382256627_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t382256627_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t382256627_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t382256627_StaticFields::get_offset_of_server_cert_cb_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (SPKey_t2934702501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1114[2] = 
{
	SPKey_t2934702501::get_offset_of_uri_0(),
	SPKey_t2934702501::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (WebHeaderCollection_t1238597439), -1, sizeof(WebHeaderCollection_t1238597439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1115[5] = 
{
	WebHeaderCollection_t1238597439_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t1238597439_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t1238597439_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t1238597439::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t1238597439_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (WebProxy_t2130953411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1116[5] = 
{
	WebProxy_t2130953411::get_offset_of_address_0(),
	WebProxy_t2130953411::get_offset_of_bypassOnLocal_1(),
	WebProxy_t2130953411::get_offset_of_bypassList_2(),
	WebProxy_t2130953411::get_offset_of_credentials_3(),
	WebProxy_t2130953411::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (WebRequest_t3661130957), -1, sizeof(WebRequest_t3661130957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1117[5] = 
{
	WebRequest_t3661130957_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t3661130957_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t3661130957_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t3661130957::get_offset_of_authentication_level_4(),
	WebRequest_t3661130957_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (OpenFlags_t2472073502)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1118[6] = 
{
	OpenFlags_t2472073502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (PublicKey_t1522942069), -1, sizeof(PublicKey_t1522942069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1119[5] = 
{
	PublicKey_t1522942069::get_offset_of__key_0(),
	PublicKey_t1522942069::get_offset_of__keyValue_1(),
	PublicKey_t1522942069::get_offset_of__params_2(),
	PublicKey_t1522942069::get_offset_of__oid_3(),
	PublicKey_t1522942069_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (StoreLocation_t1581816920)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1120[3] = 
{
	StoreLocation_t1581816920::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (StoreName_t854849834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1121[9] = 
{
	StoreName_t854849834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (X500DistinguishedName_t2418891383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1122[1] = 
{
	X500DistinguishedName_t2418891383::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (X500DistinguishedNameFlags_t245693987)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1123[11] = 
{
	X500DistinguishedNameFlags_t245693987::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (X509BasicConstraintsExtension_t3280602463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1124[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t3280602463::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t3280602463::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t3280602463::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t3280602463::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (X509Certificate2_t770297212), -1, sizeof(X509Certificate2_t770297212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1125[13] = 
{
	X509Certificate2_t770297212::get_offset_of__archived_5(),
	X509Certificate2_t770297212::get_offset_of__extensions_6(),
	X509Certificate2_t770297212::get_offset_of__name_7(),
	X509Certificate2_t770297212::get_offset_of__serial_8(),
	X509Certificate2_t770297212::get_offset_of__publicKey_9(),
	X509Certificate2_t770297212::get_offset_of_issuer_name_10(),
	X509Certificate2_t770297212::get_offset_of_subject_name_11(),
	X509Certificate2_t770297212::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t770297212::get_offset_of__cert_13(),
	X509Certificate2_t770297212_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t770297212_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t770297212_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t770297212_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (X509Certificate2Collection_t1517963221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (X509Certificate2Enumerator_t3449845280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1127[1] = 
{
	X509Certificate2Enumerator_t3449845280::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (X509CertificateCollection_t3590496811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (X509CertificateEnumerator_t2456269188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1129[1] = 
{
	X509CertificateEnumerator_t2456269188::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (X509Chain_t3119249338), -1, sizeof(X509Chain_t3119249338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1130[15] = 
{
	X509Chain_t3119249338::get_offset_of_location_0(),
	X509Chain_t3119249338::get_offset_of_elements_1(),
	X509Chain_t3119249338::get_offset_of_policy_2(),
	X509Chain_t3119249338::get_offset_of_status_3(),
	X509Chain_t3119249338_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t3119249338::get_offset_of_max_path_length_5(),
	X509Chain_t3119249338::get_offset_of_working_issuer_name_6(),
	X509Chain_t3119249338::get_offset_of_working_public_key_7(),
	X509Chain_t3119249338::get_offset_of_bce_restriction_8(),
	X509Chain_t3119249338::get_offset_of_roots_9(),
	X509Chain_t3119249338::get_offset_of_cas_10(),
	X509Chain_t3119249338::get_offset_of_collection_11(),
	X509Chain_t3119249338_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_12(),
	X509Chain_t3119249338_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_13(),
	X509Chain_t3119249338_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (X509ChainElement_t2233104543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1131[4] = 
{
	X509ChainElement_t2233104543::get_offset_of_certificate_0(),
	X509ChainElement_t2233104543::get_offset_of_status_1(),
	X509ChainElement_t2233104543::get_offset_of_info_2(),
	X509ChainElement_t2233104543::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (X509ChainElementCollection_t762555534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1132[1] = 
{
	X509ChainElementCollection_t762555534::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (X509ChainElementEnumerator_t3654624516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1133[1] = 
{
	X509ChainElementEnumerator_t3654624516::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (X509ChainPolicy_t1059945142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1134[8] = 
{
	X509ChainPolicy_t1059945142::get_offset_of_apps_0(),
	X509ChainPolicy_t1059945142::get_offset_of_cert_1(),
	X509ChainPolicy_t1059945142::get_offset_of_store_2(),
	X509ChainPolicy_t1059945142::get_offset_of_rflag_3(),
	X509ChainPolicy_t1059945142::get_offset_of_mode_4(),
	X509ChainPolicy_t1059945142::get_offset_of_timeout_5(),
	X509ChainPolicy_t1059945142::get_offset_of_vflags_6(),
	X509ChainPolicy_t1059945142::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (X509ChainStatus_t3509631940)+ sizeof (RuntimeObject), sizeof(X509ChainStatus_t3509631940_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1135[2] = 
{
	X509ChainStatus_t3509631940::get_offset_of_status_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	X509ChainStatus_t3509631940::get_offset_of_info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (X509ChainStatusFlags_t2843688617)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1136[24] = 
{
	X509ChainStatusFlags_t2843688617::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (X509EnhancedKeyUsageExtension_t3022440765), -1, sizeof(X509EnhancedKeyUsageExtension_t3022440765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1137[3] = 
{
	X509EnhancedKeyUsageExtension_t3022440765::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t3022440765::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t3022440765_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (X509Extension_t3333384912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1138[1] = 
{
	X509Extension_t3333384912::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (X509ExtensionCollection_t3540009410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1139[1] = 
{
	X509ExtensionCollection_t3540009410::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (X509ExtensionEnumerator_t3667217117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1140[1] = 
{
	X509ExtensionEnumerator_t3667217117::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (X509FindType_t130291178)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1141[16] = 
{
	X509FindType_t130291178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (X509KeyUsageExtension_t1028005009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1142[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1028005009::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t1028005009::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (X509KeyUsageFlags_t1186641826)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1143[11] = 
{
	X509KeyUsageFlags_t1186641826::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (X509NameType_t1476498007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1144[7] = 
{
	X509NameType_t1476498007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (X509RevocationFlag_t1382034120)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1145[4] = 
{
	X509RevocationFlag_t1382034120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (X509RevocationMode_t1434479791)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1146[4] = 
{
	X509RevocationMode_t1434479791::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (X509Store_t754901440), -1, sizeof(X509Store_t754901440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1147[6] = 
{
	X509Store_t754901440::get_offset_of__name_0(),
	X509Store_t754901440::get_offset_of__location_1(),
	X509Store_t754901440::get_offset_of_list_2(),
	X509Store_t754901440::get_offset_of__flags_3(),
	X509Store_t754901440::get_offset_of_store_4(),
	X509Store_t754901440_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (X509SubjectKeyIdentifierExtension_t2010369761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1148[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2010369761::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t2010369761::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t2010369761::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t159978081)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1149[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t159978081::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (X509VerificationFlags_t2520468694)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1150[15] = 
{
	X509VerificationFlags_t2520468694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (AsnDecodeStatus_t3809782524)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1151[7] = 
{
	AsnDecodeStatus_t3809782524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (AsnEncodedData_t2608258608), -1, sizeof(AsnEncodedData_t2608258608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1152[3] = 
{
	AsnEncodedData_t2608258608::get_offset_of__oid_0(),
	AsnEncodedData_t2608258608::get_offset_of__raw_1(),
	AsnEncodedData_t2608258608_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (Oid_t2597199105), -1, sizeof(Oid_t2597199105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1153[3] = 
{
	Oid_t2597199105::get_offset_of__value_0(),
	Oid_t2597199105::get_offset_of__name_1(),
	Oid_t2597199105_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (OidCollection_t2127506323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1154[2] = 
{
	OidCollection_t2127506323::get_offset_of__list_0(),
	OidCollection_t2127506323::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (OidEnumerator_t2140332956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1155[2] = 
{
	OidEnumerator_t2140332956::get_offset_of__collection_0(),
	OidEnumerator_t2140332956::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { sizeof (BaseMachine_t3233935325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1156[1] = 
{
	BaseMachine_t3233935325::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (Capture_t3723888528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1157[3] = 
{
	Capture_t3723888528::get_offset_of_index_0(),
	Capture_t3723888528::get_offset_of_length_1(),
	Capture_t3723888528::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (CaptureCollection_t1282494621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1158[1] = 
{
	CaptureCollection_t1282494621::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (Group_t4116754232), -1, sizeof(Group_t4116754232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1159[3] = 
{
	Group_t4116754232_StaticFields::get_offset_of_Fail_3(),
	Group_t4116754232::get_offset_of_success_4(),
	Group_t4116754232::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (GroupCollection_t3084121107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1160[2] = 
{
	GroupCollection_t3084121107::get_offset_of_list_0(),
	GroupCollection_t3084121107::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (Match_t3699055374), -1, sizeof(Match_t3699055374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1161[5] = 
{
	Match_t3699055374::get_offset_of_regex_6(),
	Match_t3699055374::get_offset_of_machine_7(),
	Match_t3699055374::get_offset_of_text_length_8(),
	Match_t3699055374::get_offset_of_groups_9(),
	Match_t3699055374_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (MatchCollection_t1075250625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1162[2] = 
{
	MatchCollection_t1075250625::get_offset_of_current_0(),
	MatchCollection_t1075250625::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (Enumerator_t197996776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1163[2] = 
{
	Enumerator_t197996776::get_offset_of_index_0(),
	Enumerator_t197996776::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (Regex_t2687244226), -1, sizeof(Regex_t2687244226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1164[9] = 
{
	Regex_t2687244226_StaticFields::get_offset_of_cache_0(),
	Regex_t2687244226::get_offset_of_machineFactory_1(),
	Regex_t2687244226::get_offset_of_mapping_2(),
	Regex_t2687244226::get_offset_of_group_count_3(),
	Regex_t2687244226::get_offset_of_gap_4(),
	Regex_t2687244226::get_offset_of_group_names_5(),
	Regex_t2687244226::get_offset_of_group_numbers_6(),
	Regex_t2687244226::get_offset_of_pattern_7(),
	Regex_t2687244226::get_offset_of_roptions_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (RegexOptions_t1974644192)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1165[10] = 
{
	RegexOptions_t1974644192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (OpCode_t3191944080)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1166[26] = 
{
	OpCode_t3191944080::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (OpFlags_t3593480483)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1167[6] = 
{
	OpFlags_t3593480483::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (Position_t2204692691)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1168[11] = 
{
	Position_t2204692691::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (FactoryCache_t3371693116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[3] = 
{
	FactoryCache_t3371693116::get_offset_of_capacity_0(),
	FactoryCache_t3371693116::get_offset_of_factories_1(),
	FactoryCache_t3371693116::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (Key_t2877783392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1172[2] = 
{
	Key_t2877783392::get_offset_of_pattern_0(),
	Key_t2877783392::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (MRUList_t1460135803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1173[2] = 
{
	MRUList_t1460135803::get_offset_of_head_0(),
	MRUList_t1460135803::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (Node_t2575764264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1174[3] = 
{
	Node_t2575764264::get_offset_of_value_0(),
	Node_t2575764264::get_offset_of_previous_1(),
	Node_t2575764264::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (Category_t3796563005)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1175[146] = 
{
	Category_t3796563005::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (CategoryUtils_t2595343782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (LinkRef_t3851818329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (InterpreterFactory_t2435289893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1179[4] = 
{
	InterpreterFactory_t2435289893::get_offset_of_mapping_0(),
	InterpreterFactory_t2435289893::get_offset_of_pattern_1(),
	InterpreterFactory_t2435289893::get_offset_of_namesMapping_2(),
	InterpreterFactory_t2435289893::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (PatternCompiler_t295022667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1180[1] = 
{
	PatternCompiler_t295022667::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (PatternLinkStack_t3550044269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1181[1] = 
{
	PatternLinkStack_t3550044269::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (Link_t3403178030)+ sizeof (RuntimeObject), sizeof(Link_t3403178030 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1182[2] = 
{
	Link_t3403178030::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Link_t3403178030::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (LinkStack_t2080309226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1183[1] = 
{
	LinkStack_t2080309226::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (Mark_t3700292706)+ sizeof (RuntimeObject), sizeof(Mark_t3700292706 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1184[3] = 
{
	Mark_t3700292706::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t3700292706::get_offset_of_End_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t3700292706::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (Interpreter_t881416833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1185[16] = 
{
	Interpreter_t881416833::get_offset_of_program_1(),
	Interpreter_t881416833::get_offset_of_program_start_2(),
	Interpreter_t881416833::get_offset_of_text_3(),
	Interpreter_t881416833::get_offset_of_text_end_4(),
	Interpreter_t881416833::get_offset_of_group_count_5(),
	Interpreter_t881416833::get_offset_of_match_min_6(),
	Interpreter_t881416833::get_offset_of_qs_7(),
	Interpreter_t881416833::get_offset_of_scan_ptr_8(),
	Interpreter_t881416833::get_offset_of_repeat_9(),
	Interpreter_t881416833::get_offset_of_fast_10(),
	Interpreter_t881416833::get_offset_of_stack_11(),
	Interpreter_t881416833::get_offset_of_deep_12(),
	Interpreter_t881416833::get_offset_of_marks_13(),
	Interpreter_t881416833::get_offset_of_mark_start_14(),
	Interpreter_t881416833::get_offset_of_mark_end_15(),
	Interpreter_t881416833::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (IntStack_t3571941486)+ sizeof (RuntimeObject), sizeof(IntStack_t3571941486_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1186[2] = 
{
	IntStack_t3571941486::get_offset_of_values_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntStack_t3571941486::get_offset_of_count_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (RepeatContext_t2609418170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1187[7] = 
{
	RepeatContext_t2609418170::get_offset_of_start_0(),
	RepeatContext_t2609418170::get_offset_of_min_1(),
	RepeatContext_t2609418170::get_offset_of_max_2(),
	RepeatContext_t2609418170::get_offset_of_lazy_3(),
	RepeatContext_t2609418170::get_offset_of_expr_pc_4(),
	RepeatContext_t2609418170::get_offset_of_previous_5(),
	RepeatContext_t2609418170::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (Mode_t2316998792)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1188[4] = 
{
	Mode_t2316998792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (Interval_t545543078)+ sizeof (RuntimeObject), sizeof(Interval_t545543078_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1189[3] = 
{
	Interval_t545543078::get_offset_of_low_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Interval_t545543078::get_offset_of_high_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Interval_t545543078::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (IntervalCollection_t621877006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[1] = 
{
	IntervalCollection_t621877006::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (Enumerator_t2950357022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1191[2] = 
{
	Enumerator_t2950357022::get_offset_of_list_0(),
	Enumerator_t2950357022::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (CostDelegate_t4259336196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (Parser_t3991154808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1193[6] = 
{
	Parser_t3991154808::get_offset_of_pattern_0(),
	Parser_t3991154808::get_offset_of_ptr_1(),
	Parser_t3991154808::get_offset_of_caps_2(),
	Parser_t3991154808::get_offset_of_refs_3(),
	Parser_t3991154808::get_offset_of_num_groups_4(),
	Parser_t3991154808::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (QuickSearch_t2754904521), -1, sizeof(QuickSearch_t2754904521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1194[7] = 
{
	QuickSearch_t2754904521::get_offset_of_str_0(),
	QuickSearch_t2754904521::get_offset_of_len_1(),
	QuickSearch_t2754904521::get_offset_of_ignore_2(),
	QuickSearch_t2754904521::get_offset_of_reverse_3(),
	QuickSearch_t2754904521::get_offset_of_shift_4(),
	QuickSearch_t2754904521::get_offset_of_shiftExtended_5(),
	QuickSearch_t2754904521_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (ExpressionCollection_t2949493571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (Expression_t1722083296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (CompositeExpression_t374080217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1197[1] = 
{
	CompositeExpression_t374080217::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (Group_t1053066773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (RegularExpression_t679831448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1199[1] = 
{
	RegularExpression_t679831448::get_offset_of_group_count_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
