﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t3150912798_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t3150912798_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t3150912798_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t3150912798_0_0_0;
extern "C" void Escape_t4062129116_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t4062129116_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t4062129116_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t4062129116_0_0_0;
extern "C" void PreviousInfo_t487304873_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t487304873_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t487304873_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t487304873_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t1532208872();
extern const RuntimeType AppDomainInitializer_t1532208872_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t170667719();
extern const RuntimeType Swapper_t170667719_0_0_0;
extern "C" void DictionaryEntry_t2422360636_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t2422360636_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t2422360636_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t2422360636_0_0_0;
extern "C" void Slot_t2203870133_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2203870133_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2203870133_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2203870133_0_0_0;
extern "C" void Slot_t2314815295_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2314815295_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2314815295_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2314815295_0_0_0;
extern "C" void Enum_t3173835468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t3173835468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t3173835468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t3173835468_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4080908931();
extern const RuntimeType ReadDelegate_t4080908931_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4257518600();
extern const RuntimeType WriteDelegate_t4257518600_0_0_0;
extern "C" void MonoIOStat_t197159053_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t197159053_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t197159053_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t197159053_0_0_0;
extern "C" void MonoEnumInfo_t297070929_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t297070929_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t297070929_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t297070929_0_0_0;
extern "C" void CustomAttributeNamedArgument_t3690676406_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t3690676406_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t3690676406_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t3690676406_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2525571267_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2525571267_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2525571267_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2525571267_0_0_0;
extern "C" void ILTokenInfo_t1423368019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t1423368019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t1423368019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t1423368019_0_0_0;
extern "C" void MonoResource_t1625298336_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t1625298336_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t1625298336_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t1625298336_0_0_0;
extern "C" void MonoWin32Resource_t1452687774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t1452687774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t1452687774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoWin32Resource_t1452687774_0_0_0;
extern "C" void RefEmitPermissionSet_t3060109815_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t3060109815_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t3060109815_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t3060109815_0_0_0;
extern "C" void MonoEventInfo_t1734283913_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t1734283913_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t1734283913_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t1734283913_0_0_0;
extern "C" void MonoMethodInfo_t4254535314_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t4254535314_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t4254535314_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t4254535314_0_0_0;
extern "C" void MonoPropertyInfo_t1194906867_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t1194906867_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t1194906867_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t1194906867_0_0_0;
extern "C" void ParameterModifier_t1946944212_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1946944212_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1946944212_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1946944212_0_0_0;
extern "C" void ResourceCacheItem_t3011921618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t3011921618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t3011921618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t3011921618_0_0_0;
extern "C" void ResourceInfo_t3106642170_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3106642170_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3106642170_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3106642170_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t3414101031();
extern const RuntimeType CrossContextDelegate_t3414101031_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t2300529940();
extern const RuntimeType CallbackHandler_t2300529940_0_0_0;
extern "C" void SerializationEntry_t1218002009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t1218002009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t1218002009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t1218002009_0_0_0;
extern "C" void StreamingContext_t4252281328_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t4252281328_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t4252281328_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t4252281328_0_0_0;
extern "C" void DSAParameters_t751310058_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t751310058_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t751310058_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t751310058_0_0_0;
extern "C" void RSAParameters_t4174691487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t4174691487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t4174691487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t4174691487_0_0_0;
extern "C" void SecurityFrame_t2813770691_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t2813770691_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t2813770691_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t2813770691_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1035859370();
extern const RuntimeType ThreadStart_t1035859370_0_0_0;
extern "C" void ValueType_t1364887298_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t1364887298_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t1364887298_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t1364887298_0_0_0;
extern "C" void X509ChainStatus_t3509631940_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t3509631940_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t3509631940_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t3509631940_0_0_0;
extern "C" void IntStack_t3571941486_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t3571941486_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t3571941486_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t3571941486_0_0_0;
extern "C" void Interval_t545543078_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t545543078_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t545543078_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t545543078_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t4259336196();
extern const RuntimeType CostDelegate_t4259336196_0_0_0;
extern "C" void UriScheme_t1574639958_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t1574639958_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t1574639958_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t1574639958_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t966290873();
extern const RuntimeType Action_t966290873_0_0_0;
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t2429328822_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t657888911();
extern const RuntimeType LogCallback_t657888911_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t2129875537();
extern const RuntimeType LowMemoryCallback_t2129875537_0_0_0;
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t1484565475_0_0_0;
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t4006347538_0_0_0;
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1227466744_0_0_0;
extern "C" void Coroutine_t4212313979_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t4212313979_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t4212313979_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t4212313979_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t2617999110();
extern const RuntimeType CSSMeasureFunc_t2617999110_0_0_0;
extern "C" void CullingGroup_t635124431_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t635124431_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t635124431_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t635124431_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t3421641282();
extern const RuntimeType StateChanged_t3421641282_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t850082040();
extern const RuntimeType DisplaysUpdatedDelegate_t850082040_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t1298730314();
extern const RuntimeType UnityAction_t1298730314_0_0_0;
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t227884036_0_0_0;
extern "C" void Gradient_t1395232743_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t1395232743_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t1395232743_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t1395232743_0_0_0;
extern "C" void Object_t1970767703_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1970767703_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1970767703_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t1970767703_0_0_0;
extern "C" void PlayableBinding_t181640494_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t181640494_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t181640494_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t181640494_0_0_0;
extern "C" void RectOffset_t83369214_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t83369214_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t83369214_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t83369214_0_0_0;
extern "C" void ResourceRequest_t230212484_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t230212484_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t230212484_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t230212484_0_0_0;
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2962125979_0_0_0;
extern "C" void HitInfo_t214434488_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t214434488_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t214434488_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t214434488_0_0_0;
extern "C" void TrackedReference_t774033239_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t774033239_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t774033239_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t774033239_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t634135197();
extern const RuntimeType RequestAtlasCallback_t634135197_0_0_0;
extern "C" void WorkRequest_t4243437884_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t4243437884_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t4243437884_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t4243437884_0_0_0;
extern "C" void WaitForSeconds_t4277075805_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t4277075805_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t4277075805_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t4277075805_0_0_0;
extern "C" void YieldInstruction_t3362187334_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3362187334_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3362187334_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t3362187334_0_0_0;
extern "C" void RaycastHit2D_t266689378_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t266689378_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t266689378_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t266689378_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t332127390();
extern const RuntimeType FontTextureRebuildCallback_t332127390_0_0_0;
extern "C" void TextGenerationSettings_t1023477914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1023477914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1023477914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1023477914_0_0_0;
extern "C" void TextGenerator_t2088037707_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t2088037707_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t2088037707_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t2088037707_0_0_0;
extern "C" void AnimationEvent_t2647497664_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2647497664_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2647497664_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t2647497664_0_0_0;
extern "C" void AnimatorTransitionInfo_t3218642921_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t3218642921_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t3218642921_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t3218642921_0_0_0;
extern "C" void HumanBone_t3347038890_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t3347038890_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t3347038890_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t3347038890_0_0_0;
extern "C" void SkeletonBone_t39311066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t39311066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t39311066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t39311066_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3858203868();
extern const RuntimeType PCMReaderCallback_t3858203868_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1105901276();
extern const RuntimeType PCMSetPositionCallback_t1105901276_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t530010739();
extern const RuntimeType AudioConfigurationChangeHandler_t530010739_0_0_0;
extern "C" void GcAchievementData_t2938297544_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t2938297544_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t2938297544_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t2938297544_0_0_0;
extern "C" void GcAchievementDescriptionData_t4257577009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t4257577009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t4257577009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t4257577009_0_0_0;
extern "C" void GcLeaderboard_t2259580636_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t2259580636_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t2259580636_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t2259580636_0_0_0;
extern "C" void GcScoreData_t3653438230_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t3653438230_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t3653438230_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t3653438230_0_0_0;
extern "C" void GcUserProfileData_t2303609216_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2303609216_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2303609216_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2303609216_0_0_0;
extern "C" void Event_t872956717_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t872956717_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t872956717_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t872956717_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t1852235621();
extern const RuntimeType WindowFunction_t1852235621_0_0_0;
extern "C" void GUIContent_t3890455993_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3890455993_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3890455993_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3890455993_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t2500130054();
extern const RuntimeType SkinChangedDelegate_t2500130054_0_0_0;
extern "C" void GUIStyle_t4058570518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t4058570518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t4058570518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t4058570518_0_0_0;
extern "C" void GUIStyleState_t1457118775_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1457118775_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1457118775_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1457118775_0_0_0;
extern "C" void Collision_t3580189182_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t3580189182_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t3580189182_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t3580189182_0_0_0;
extern "C" void ControllerColliderHit_t174542347_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t174542347_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t174542347_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t174542347_0_0_0;
extern "C" void RaycastHit_t2851673566_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t2851673566_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t2851673566_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t2851673566_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t2262493815();
extern const RuntimeType WillRenderCanvases_t2262493815_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3037819885();
extern const RuntimeType SessionStateChanged_t3037819885_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t76233307();
extern const RuntimeType UpdatedEventHandler_t76233307_0_0_0;
extern "C" void RaycastResult_t463000792_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t463000792_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t463000792_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t463000792_0_0_0;
extern "C" void ColorTween_t1361506897_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t1361506897_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t1361506897_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t1361506897_0_0_0;
extern "C" void FloatTween_t3352968507_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t3352968507_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t3352968507_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t3352968507_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1213950634();
extern const RuntimeType OnValidateInput_t1213950634_0_0_0;
extern "C" void Navigation_t4246164788_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t4246164788_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t4246164788_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t4246164788_0_0_0;
extern "C" void SpriteState_t2305809087_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t2305809087_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t2305809087_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t2305809087_0_0_0;
extern "C" void ARAnchor_t2396185323_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARAnchor_t2396185323_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARAnchor_t2396185323_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARAnchor_t2396185323_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictionaryVisitorHandler_t1022013023();
extern const RuntimeType DictionaryVisitorHandler_t1022013023_0_0_0;
extern "C" void ARHitTestResult_t1183581528_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARHitTestResult_t1183581528_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARHitTestResult_t1183581528_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARHitTestResult_t1183581528_0_0_0;
extern "C" void ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitFaceTrackingConfiguration_t2739091181_0_0_0;
extern "C" void ARKitSessionConfiguration_t384353898_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitSessionConfiguration_t384353898_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitSessionConfiguration_t384353898_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitSessionConfiguration_t384353898_0_0_0;
extern "C" void ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitWorldTrackingSessionConfiguration_t4171655616_0_0_0;
extern "C" void ARPlaneAnchor_t2525223154_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARPlaneAnchor_t2525223154_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARPlaneAnchor_t2525223154_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARPlaneAnchor_t2525223154_0_0_0;
extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARUserAnchor_t2452746139_0_0_0;
extern "C" void UnityARCamera_t3270530332_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARCamera_t3270530332_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARCamera_t3270530332_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARCamera_t3270530332_0_0_0;
extern "C" void UnityARHitTestResult_t2724124046_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARHitTestResult_t2724124046_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARHitTestResult_t2724124046_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARHitTestResult_t2724124046_0_0_0;
extern "C" void UnityARLightData_t3404656299_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARLightData_t3404656299_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARLightData_t3404656299_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARLightData_t3404656299_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorAdded_t1360026176();
extern const RuntimeType ARAnchorAdded_t1360026176_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorRemoved_t617657937();
extern const RuntimeType ARAnchorRemoved_t617657937_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorUpdated_t2652221944();
extern const RuntimeType ARAnchorUpdated_t2652221944_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionCallback_t1432673640();
extern const RuntimeType ARSessionCallback_t1432673640_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionFailed_t916421009();
extern const RuntimeType ARSessionFailed_t916421009_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorAdded_t389345868();
extern const RuntimeType ARUserAnchorAdded_t389345868_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorRemoved_t877041919();
extern const RuntimeType ARUserAnchorRemoved_t877041919_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorUpdated_t3351790518();
extern const RuntimeType ARUserAnchorUpdated_t3351790518_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1133215702();
extern const RuntimeType internal_ARAnchorAdded_t1133215702_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t1159356933();
extern const RuntimeType internal_ARAnchorRemoved_t1159356933_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2228873274();
extern const RuntimeType internal_ARAnchorUpdated_t2228873274_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorAdded_t338421685();
extern const RuntimeType internal_ARFaceAnchorAdded_t338421685_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorRemoved_t1507865825();
extern const RuntimeType internal_ARFaceAnchorRemoved_t1507865825_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorUpdated_t4219811705();
extern const RuntimeType internal_ARFaceAnchorUpdated_t4219811705_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFrameUpdate_t4139772791();
extern const RuntimeType internal_ARFrameUpdate_t4139772791_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t2450502912();
extern const RuntimeType internal_ARSessionTrackingChanged_t2450502912_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t1053280061();
extern const RuntimeType internal_ARUserAnchorAdded_t1053280061_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t535060684();
extern const RuntimeType internal_ARUserAnchorRemoved_t535060684_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t1262387502();
extern const RuntimeType internal_ARUserAnchorUpdated_t1262387502_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[130] = 
{
	{ NULL, Context_t3150912798_marshal_pinvoke, Context_t3150912798_marshal_pinvoke_back, Context_t3150912798_marshal_pinvoke_cleanup, NULL, NULL, &Context_t3150912798_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t4062129116_marshal_pinvoke, Escape_t4062129116_marshal_pinvoke_back, Escape_t4062129116_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t4062129116_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t487304873_marshal_pinvoke, PreviousInfo_t487304873_marshal_pinvoke_back, PreviousInfo_t487304873_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t487304873_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t1532208872, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t1532208872_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t170667719, NULL, NULL, NULL, NULL, NULL, &Swapper_t170667719_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t2422360636_marshal_pinvoke, DictionaryEntry_t2422360636_marshal_pinvoke_back, DictionaryEntry_t2422360636_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t2422360636_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t2203870133_marshal_pinvoke, Slot_t2203870133_marshal_pinvoke_back, Slot_t2203870133_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2203870133_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t2314815295_marshal_pinvoke, Slot_t2314815295_marshal_pinvoke_back, Slot_t2314815295_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2314815295_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t3173835468_marshal_pinvoke, Enum_t3173835468_marshal_pinvoke_back, Enum_t3173835468_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t3173835468_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4080908931, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4080908931_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4257518600, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4257518600_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t197159053_marshal_pinvoke, MonoIOStat_t197159053_marshal_pinvoke_back, MonoIOStat_t197159053_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t197159053_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t297070929_marshal_pinvoke, MonoEnumInfo_t297070929_marshal_pinvoke_back, MonoEnumInfo_t297070929_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t297070929_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t3690676406_marshal_pinvoke, CustomAttributeNamedArgument_t3690676406_marshal_pinvoke_back, CustomAttributeNamedArgument_t3690676406_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t3690676406_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2525571267_marshal_pinvoke, CustomAttributeTypedArgument_t2525571267_marshal_pinvoke_back, CustomAttributeTypedArgument_t2525571267_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2525571267_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t1423368019_marshal_pinvoke, ILTokenInfo_t1423368019_marshal_pinvoke_back, ILTokenInfo_t1423368019_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t1423368019_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t1625298336_marshal_pinvoke, MonoResource_t1625298336_marshal_pinvoke_back, MonoResource_t1625298336_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t1625298336_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t1452687774_marshal_pinvoke, MonoWin32Resource_t1452687774_marshal_pinvoke_back, MonoWin32Resource_t1452687774_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t1452687774_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, RefEmitPermissionSet_t3060109815_marshal_pinvoke, RefEmitPermissionSet_t3060109815_marshal_pinvoke_back, RefEmitPermissionSet_t3060109815_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t3060109815_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t1734283913_marshal_pinvoke, MonoEventInfo_t1734283913_marshal_pinvoke_back, MonoEventInfo_t1734283913_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t1734283913_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t4254535314_marshal_pinvoke, MonoMethodInfo_t4254535314_marshal_pinvoke_back, MonoMethodInfo_t4254535314_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t4254535314_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t1194906867_marshal_pinvoke, MonoPropertyInfo_t1194906867_marshal_pinvoke_back, MonoPropertyInfo_t1194906867_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t1194906867_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1946944212_marshal_pinvoke, ParameterModifier_t1946944212_marshal_pinvoke_back, ParameterModifier_t1946944212_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1946944212_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t3011921618_marshal_pinvoke, ResourceCacheItem_t3011921618_marshal_pinvoke_back, ResourceCacheItem_t3011921618_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t3011921618_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3106642170_marshal_pinvoke, ResourceInfo_t3106642170_marshal_pinvoke_back, ResourceInfo_t3106642170_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3106642170_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t3414101031, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t3414101031_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t2300529940, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t2300529940_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t1218002009_marshal_pinvoke, SerializationEntry_t1218002009_marshal_pinvoke_back, SerializationEntry_t1218002009_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t1218002009_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t4252281328_marshal_pinvoke, StreamingContext_t4252281328_marshal_pinvoke_back, StreamingContext_t4252281328_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t4252281328_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t751310058_marshal_pinvoke, DSAParameters_t751310058_marshal_pinvoke_back, DSAParameters_t751310058_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t751310058_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t4174691487_marshal_pinvoke, RSAParameters_t4174691487_marshal_pinvoke_back, RSAParameters_t4174691487_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t4174691487_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t2813770691_marshal_pinvoke, SecurityFrame_t2813770691_marshal_pinvoke_back, SecurityFrame_t2813770691_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t2813770691_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1035859370, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1035859370_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t1364887298_marshal_pinvoke, ValueType_t1364887298_marshal_pinvoke_back, ValueType_t1364887298_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t1364887298_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t3509631940_marshal_pinvoke, X509ChainStatus_t3509631940_marshal_pinvoke_back, X509ChainStatus_t3509631940_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t3509631940_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t3571941486_marshal_pinvoke, IntStack_t3571941486_marshal_pinvoke_back, IntStack_t3571941486_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t3571941486_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t545543078_marshal_pinvoke, Interval_t545543078_marshal_pinvoke_back, Interval_t545543078_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t545543078_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t4259336196, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t4259336196_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t1574639958_marshal_pinvoke, UriScheme_t1574639958_marshal_pinvoke_back, UriScheme_t1574639958_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t1574639958_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t966290873, NULL, NULL, NULL, NULL, NULL, &Action_t966290873_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t2429328822_marshal_pinvoke, AnimationCurve_t2429328822_marshal_pinvoke_back, AnimationCurve_t2429328822_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t2429328822_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t657888911, NULL, NULL, NULL, NULL, NULL, &LogCallback_t657888911_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t2129875537, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t2129875537_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t1484565475_marshal_pinvoke, AssetBundleCreateRequest_t1484565475_marshal_pinvoke_back, AssetBundleCreateRequest_t1484565475_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t1484565475_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t4006347538_marshal_pinvoke, AssetBundleRequest_t4006347538_marshal_pinvoke_back, AssetBundleRequest_t4006347538_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t4006347538_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1227466744_marshal_pinvoke, AsyncOperation_t1227466744_marshal_pinvoke_back, AsyncOperation_t1227466744_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1227466744_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, Coroutine_t4212313979_marshal_pinvoke, Coroutine_t4212313979_marshal_pinvoke_back, Coroutine_t4212313979_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t4212313979_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t2617999110, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t2617999110_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t635124431_marshal_pinvoke, CullingGroup_t635124431_marshal_pinvoke_back, CullingGroup_t635124431_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t635124431_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t3421641282, NULL, NULL, NULL, NULL, NULL, &StateChanged_t3421641282_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t850082040, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t850082040_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t1298730314, NULL, NULL, NULL, NULL, NULL, &UnityAction_t1298730314_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t227884036_marshal_pinvoke, FailedToLoadScriptObject_t227884036_marshal_pinvoke_back, FailedToLoadScriptObject_t227884036_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t227884036_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t1395232743_marshal_pinvoke, Gradient_t1395232743_marshal_pinvoke_back, Gradient_t1395232743_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t1395232743_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t1970767703_marshal_pinvoke, Object_t1970767703_marshal_pinvoke_back, Object_t1970767703_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1970767703_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t181640494_marshal_pinvoke, PlayableBinding_t181640494_marshal_pinvoke_back, PlayableBinding_t181640494_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t181640494_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t83369214_marshal_pinvoke, RectOffset_t83369214_marshal_pinvoke_back, RectOffset_t83369214_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t83369214_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t230212484_marshal_pinvoke, ResourceRequest_t230212484_marshal_pinvoke_back, ResourceRequest_t230212484_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t230212484_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2962125979_marshal_pinvoke, ScriptableObject_t2962125979_marshal_pinvoke_back, ScriptableObject_t2962125979_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2962125979_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t214434488_marshal_pinvoke, HitInfo_t214434488_marshal_pinvoke_back, HitInfo_t214434488_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t214434488_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t774033239_marshal_pinvoke, TrackedReference_t774033239_marshal_pinvoke_back, TrackedReference_t774033239_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t774033239_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t634135197, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t634135197_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t4243437884_marshal_pinvoke, WorkRequest_t4243437884_marshal_pinvoke_back, WorkRequest_t4243437884_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t4243437884_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t4277075805_marshal_pinvoke, WaitForSeconds_t4277075805_marshal_pinvoke_back, WaitForSeconds_t4277075805_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t4277075805_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t3362187334_marshal_pinvoke, YieldInstruction_t3362187334_marshal_pinvoke_back, YieldInstruction_t3362187334_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3362187334_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastHit2D_t266689378_marshal_pinvoke, RaycastHit2D_t266689378_marshal_pinvoke_back, RaycastHit2D_t266689378_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t266689378_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t332127390, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t332127390_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1023477914_marshal_pinvoke, TextGenerationSettings_t1023477914_marshal_pinvoke_back, TextGenerationSettings_t1023477914_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1023477914_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t2088037707_marshal_pinvoke, TextGenerator_t2088037707_marshal_pinvoke_back, TextGenerator_t2088037707_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t2088037707_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, AnimationEvent_t2647497664_marshal_pinvoke, AnimationEvent_t2647497664_marshal_pinvoke_back, AnimationEvent_t2647497664_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2647497664_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t3218642921_marshal_pinvoke, AnimatorTransitionInfo_t3218642921_marshal_pinvoke_back, AnimatorTransitionInfo_t3218642921_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t3218642921_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t3347038890_marshal_pinvoke, HumanBone_t3347038890_marshal_pinvoke_back, HumanBone_t3347038890_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t3347038890_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t39311066_marshal_pinvoke, SkeletonBone_t39311066_marshal_pinvoke_back, SkeletonBone_t39311066_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t39311066_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t3858203868, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t3858203868_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1105901276, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1105901276_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t530010739, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t530010739_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ NULL, GcAchievementData_t2938297544_marshal_pinvoke, GcAchievementData_t2938297544_marshal_pinvoke_back, GcAchievementData_t2938297544_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t2938297544_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t4257577009_marshal_pinvoke, GcAchievementDescriptionData_t4257577009_marshal_pinvoke_back, GcAchievementDescriptionData_t4257577009_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t4257577009_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t2259580636_marshal_pinvoke, GcLeaderboard_t2259580636_marshal_pinvoke_back, GcLeaderboard_t2259580636_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t2259580636_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t3653438230_marshal_pinvoke, GcScoreData_t3653438230_marshal_pinvoke_back, GcScoreData_t3653438230_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t3653438230_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2303609216_marshal_pinvoke, GcUserProfileData_t2303609216_marshal_pinvoke_back, GcUserProfileData_t2303609216_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2303609216_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t872956717_marshal_pinvoke, Event_t872956717_marshal_pinvoke_back, Event_t872956717_marshal_pinvoke_cleanup, NULL, NULL, &Event_t872956717_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t1852235621, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t1852235621_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3890455993_marshal_pinvoke, GUIContent_t3890455993_marshal_pinvoke_back, GUIContent_t3890455993_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3890455993_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t2500130054, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t2500130054_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t4058570518_marshal_pinvoke, GUIStyle_t4058570518_marshal_pinvoke_back, GUIStyle_t4058570518_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t4058570518_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1457118775_marshal_pinvoke, GUIStyleState_t1457118775_marshal_pinvoke_back, GUIStyleState_t1457118775_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1457118775_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, Collision_t3580189182_marshal_pinvoke, Collision_t3580189182_marshal_pinvoke_back, Collision_t3580189182_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t3580189182_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t174542347_marshal_pinvoke, ControllerColliderHit_t174542347_marshal_pinvoke_back, ControllerColliderHit_t174542347_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t174542347_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t2851673566_marshal_pinvoke, RaycastHit_t2851673566_marshal_pinvoke_back, RaycastHit_t2851673566_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t2851673566_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t2262493815, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t2262493815_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3037819885, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3037819885_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t76233307, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t76233307_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, RaycastResult_t463000792_marshal_pinvoke, RaycastResult_t463000792_marshal_pinvoke_back, RaycastResult_t463000792_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t463000792_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t1361506897_marshal_pinvoke, ColorTween_t1361506897_marshal_pinvoke_back, ColorTween_t1361506897_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t1361506897_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t3352968507_marshal_pinvoke, FloatTween_t3352968507_marshal_pinvoke_back, FloatTween_t3352968507_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t3352968507_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ DelegatePInvokeWrapper_OnValidateInput_t1213950634, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t1213950634_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t4246164788_marshal_pinvoke, Navigation_t4246164788_marshal_pinvoke_back, Navigation_t4246164788_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t4246164788_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t2305809087_marshal_pinvoke, SpriteState_t2305809087_marshal_pinvoke_back, SpriteState_t2305809087_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t2305809087_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ARAnchor_t2396185323_marshal_pinvoke, ARAnchor_t2396185323_marshal_pinvoke_back, ARAnchor_t2396185323_marshal_pinvoke_cleanup, NULL, NULL, &ARAnchor_t2396185323_0_0_0 } /* UnityEngine.XR.iOS.ARAnchor */,
	{ DelegatePInvokeWrapper_DictionaryVisitorHandler_t1022013023, NULL, NULL, NULL, NULL, NULL, &DictionaryVisitorHandler_t1022013023_0_0_0 } /* UnityEngine.XR.iOS.ARFaceAnchor/DictionaryVisitorHandler */,
	{ NULL, ARHitTestResult_t1183581528_marshal_pinvoke, ARHitTestResult_t1183581528_marshal_pinvoke_back, ARHitTestResult_t1183581528_marshal_pinvoke_cleanup, NULL, NULL, &ARHitTestResult_t1183581528_0_0_0 } /* UnityEngine.XR.iOS.ARHitTestResult */,
	{ NULL, ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke, ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke_back, ARKitFaceTrackingConfiguration_t2739091181_marshal_pinvoke_cleanup, NULL, NULL, &ARKitFaceTrackingConfiguration_t2739091181_0_0_0 } /* UnityEngine.XR.iOS.ARKitFaceTrackingConfiguration */,
	{ NULL, ARKitSessionConfiguration_t384353898_marshal_pinvoke, ARKitSessionConfiguration_t384353898_marshal_pinvoke_back, ARKitSessionConfiguration_t384353898_marshal_pinvoke_cleanup, NULL, NULL, &ARKitSessionConfiguration_t384353898_0_0_0 } /* UnityEngine.XR.iOS.ARKitSessionConfiguration */,
	{ NULL, ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke, ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke_back, ARKitWorldTrackingSessionConfiguration_t4171655616_marshal_pinvoke_cleanup, NULL, NULL, &ARKitWorldTrackingSessionConfiguration_t4171655616_0_0_0 } /* UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration */,
	{ NULL, ARPlaneAnchor_t2525223154_marshal_pinvoke, ARPlaneAnchor_t2525223154_marshal_pinvoke_back, ARPlaneAnchor_t2525223154_marshal_pinvoke_cleanup, NULL, NULL, &ARPlaneAnchor_t2525223154_0_0_0 } /* UnityEngine.XR.iOS.ARPlaneAnchor */,
	{ NULL, ARUserAnchor_t2452746139_marshal_pinvoke, ARUserAnchor_t2452746139_marshal_pinvoke_back, ARUserAnchor_t2452746139_marshal_pinvoke_cleanup, NULL, NULL, &ARUserAnchor_t2452746139_0_0_0 } /* UnityEngine.XR.iOS.ARUserAnchor */,
	{ NULL, UnityARCamera_t3270530332_marshal_pinvoke, UnityARCamera_t3270530332_marshal_pinvoke_back, UnityARCamera_t3270530332_marshal_pinvoke_cleanup, NULL, NULL, &UnityARCamera_t3270530332_0_0_0 } /* UnityEngine.XR.iOS.UnityARCamera */,
	{ NULL, UnityARHitTestResult_t2724124046_marshal_pinvoke, UnityARHitTestResult_t2724124046_marshal_pinvoke_back, UnityARHitTestResult_t2724124046_marshal_pinvoke_cleanup, NULL, NULL, &UnityARHitTestResult_t2724124046_0_0_0 } /* UnityEngine.XR.iOS.UnityARHitTestResult */,
	{ NULL, UnityARLightData_t3404656299_marshal_pinvoke, UnityARLightData_t3404656299_marshal_pinvoke_back, UnityARLightData_t3404656299_marshal_pinvoke_cleanup, NULL, NULL, &UnityARLightData_t3404656299_0_0_0 } /* UnityEngine.XR.iOS.UnityARLightData */,
	{ DelegatePInvokeWrapper_ARAnchorAdded_t1360026176, NULL, NULL, NULL, NULL, NULL, &ARAnchorAdded_t1360026176_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded */,
	{ DelegatePInvokeWrapper_ARAnchorRemoved_t617657937, NULL, NULL, NULL, NULL, NULL, &ARAnchorRemoved_t617657937_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARAnchorUpdated_t2652221944, NULL, NULL, NULL, NULL, NULL, &ARAnchorUpdated_t2652221944_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_ARSessionCallback_t1432673640, NULL, NULL, NULL, NULL, NULL, &ARSessionCallback_t1432673640_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback */,
	{ DelegatePInvokeWrapper_ARSessionFailed_t916421009, NULL, NULL, NULL, NULL, NULL, &ARSessionFailed_t916421009_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed */,
	{ DelegatePInvokeWrapper_ARUserAnchorAdded_t389345868, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorAdded_t389345868_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_ARUserAnchorRemoved_t877041919, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorRemoved_t877041919_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARUserAnchorUpdated_t3351790518, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorUpdated_t3351790518_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARAnchorAdded_t1133215702, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorAdded_t1133215702_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARAnchorRemoved_t1159356933, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorRemoved_t1159356933_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2228873274, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorUpdated_t2228873274_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorAdded_t338421685, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorAdded_t338421685_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorRemoved_t1507865825, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorRemoved_t1507865825_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorUpdated_t4219811705, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorUpdated_t4219811705_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFrameUpdate_t4139772791, NULL, NULL, NULL, NULL, NULL, &internal_ARFrameUpdate_t4139772791_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate */,
	{ DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t2450502912, NULL, NULL, NULL, NULL, NULL, &internal_ARSessionTrackingChanged_t2450502912_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t1053280061, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorAdded_t1053280061_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t535060684, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorRemoved_t535060684_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t1262387502, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorUpdated_t1262387502_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated */,
	NULL,
};
