﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged
struct ARSessionTrackingChanged_t2903091015;
// System.IAsyncResult
struct IAsyncResult_t614244269;
// System.AsyncCallback
struct AsyncCallback_t869574496;
// UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_t2452746139;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded
struct ARUserAnchorAdded_t389345868;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved
struct ARUserAnchorRemoved_t877041919;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated
struct ARUserAnchorUpdated_t3351790518;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct internal_ARAnchorAdded_t1133215702;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct internal_ARAnchorRemoved_t1159356933;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct internal_ARAnchorUpdated_t2228873274;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded
struct internal_ARFaceAnchorAdded_t338421685;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved
struct internal_ARFaceAnchorRemoved_t1507865825;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated
struct internal_ARFaceAnchorUpdated_t4219811705;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct internal_ARFrameUpdate_t4139772791;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged
struct internal_ARSessionTrackingChanged_t2450502912;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded
struct internal_ARUserAnchorAdded_t1053280061;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved
struct internal_ARUserAnchorRemoved_t535060684;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated
struct internal_ARUserAnchorUpdated_t1262387502;
// UnityEngine.XR.iOS.UnityARUserAnchorComponent
struct UnityARUserAnchorComponent_t2848590843;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3829899482;
// System.String
struct String_t;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t4258909960;
// UnityEngine.Component
struct Component_t789413749;
// UnityEngine.GameObject
struct GameObject_t1318052361;
// UnityEngine.Object
struct Object_t1970767703;
// UnityEngine.Transform
struct Transform_t532597831;
// UnityEngine.XR.iOS.UnityARUtility
struct UnityARUtility_t4023213543;
// UnityEngine.MeshFilter
struct MeshFilter_t1334808991;
// UnityEngine.XR.iOS.UnityARVideo
struct UnityARVideo_t2486736449;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t2274630099;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2673047760;
// UnityEngine.Texture
struct Texture_t2838694469;
// UnityEngine.Material
struct Material_t2712136762;
// UnityEngine.Camera
struct Camera_t989002943;
// UnityEngine.Texture2D
struct Texture2D_t878840578;
// System.Single[]
struct SingleU5BU5D_t2905636975;
// UnityEngine.XR.iOS.UnityARDirectionalLightEstimate
struct UnityARDirectionalLightEstimate_t3433403765;
// UnityEngine.XR.iOS.UnityRemoteVideo
struct UnityRemoteVideo_t3391959971;
// System.Byte[]
struct ByteU5BU5D_t3287329517;
// UnityEngine.XR.iOS.ConnectToEditor
struct ConnectToEditor_t1050945949;
// UnityPointCloudExample
struct UnityPointCloudExample_t859887967;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1128502775;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t185548372;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t3541706630;
// System.IO.MemoryStream
struct MemoryStream_t3384139290;
// System.IO.Stream
struct Stream_t3046340190;
// Utils.serializableARKitInit
struct serializableARKitInit_t403564667;
// Utils.serializableARSessionConfiguration
struct serializableARSessionConfiguration_t3208486129;
// Utils.serializableFromEditorMessage
struct serializableFromEditorMessage_t1383667999;
// Utils.serializablePointCloud
struct serializablePointCloud_t2991476747;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492;
// Utils.serializableSHC
struct serializableSHC_t2973324561;
// Utils.serializableUnityARCamera
struct serializableUnityARCamera_t3181320657;
// Utils.serializableUnityARMatrix4x4
struct serializableUnityARMatrix4x4_t3548294260;
// Utils.serializableUnityARLightData
struct serializableUnityARLightData_t1864664250;
// Utils.SerializableVector4
struct SerializableVector4_t3177127415;
// Utils.serializableUnityARPlaneAnchor
struct serializableUnityARPlaneAnchor_t3395010059;
// System.Text.Encoding
struct Encoding_t3638904762;
// System.Object[]
struct ObjectU5BU5D_t1568665923;
// System.Char[]
struct CharU5BU5D_t83643201;
// System.Text.DecoderFallback
struct DecoderFallback_t527759059;
// System.Text.EncoderFallback
struct EncoderFallback_t3711323963;
// System.Reflection.Assembly
struct Assembly_t2241791680;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t4209643444;
// UnityEngine.MeshCollider
struct MeshCollider_t3189750085;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2116050852;
// System.Void
struct Void_t2642135423;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t975501551;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2088810939;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t3115768224;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t1360026176;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_t2652221944;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t617657937;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorAdded
struct ARFaceAnchorAdded_t2853404520;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorUpdated
struct ARFaceAnchorUpdated_t2137861905;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorRemoved
struct ARFaceAnchorRemoved_t656883358;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_t916421009;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback
struct ARSessionCallback_t1432673640;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t2620733104;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t781534341;

extern RuntimeClass* UnityARCamera_t3270530332_il2cpp_TypeInfo_var;
extern const uint32_t ARSessionTrackingChanged_BeginInvoke_m2513636707_MetadataUsageId;
struct ARUserAnchor_t2452746139_marshaled_pinvoke;
struct ARUserAnchor_t2452746139;;
struct ARUserAnchor_t2452746139_marshaled_pinvoke;;
extern RuntimeClass* ARUserAnchor_t2452746139_il2cpp_TypeInfo_var;
extern const uint32_t ARUserAnchorAdded_BeginInvoke_m2356247880_MetadataUsageId;
extern const uint32_t ARUserAnchorRemoved_BeginInvoke_m2172053629_MetadataUsageId;
extern const uint32_t ARUserAnchorUpdated_BeginInvoke_m2439863858_MetadataUsageId;
extern RuntimeClass* UnityARAnchorData_t4160663845_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARAnchorAdded_BeginInvoke_m2968559439_MetadataUsageId;
extern const uint32_t internal_ARAnchorRemoved_BeginInvoke_m3876512937_MetadataUsageId;
extern const uint32_t internal_ARAnchorUpdated_BeginInvoke_m3557893065_MetadataUsageId;
extern RuntimeClass* UnityARFaceAnchorData_t2557914456_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARFaceAnchorAdded_BeginInvoke_m1170739208_MetadataUsageId;
extern const uint32_t internal_ARFaceAnchorRemoved_BeginInvoke_m2452832694_MetadataUsageId;
extern const uint32_t internal_ARFaceAnchorUpdated_BeginInvoke_m2347222039_MetadataUsageId;
extern RuntimeClass* internal_UnityARCamera_t2639053733_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARFrameUpdate_BeginInvoke_m331441237_MetadataUsageId;
extern const uint32_t internal_ARSessionTrackingChanged_BeginInvoke_m222583015_MetadataUsageId;
extern RuntimeClass* UnityARUserAnchorData_t1928721163_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARUserAnchorAdded_BeginInvoke_m388150249_MetadataUsageId;
extern const uint32_t internal_ARUserAnchorRemoved_BeginInvoke_m1610706283_MetadataUsageId;
extern const uint32_t internal_ARUserAnchorUpdated_BeginInvoke_m1710900897_MetadataUsageId;
extern RuntimeClass* ARUserAnchorUpdated_t3351790518_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var;
extern RuntimeClass* ARUserAnchorRemoved_t877041919_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityARUserAnchorComponent_GameObjectAnchorUpdated_m2627889658_RuntimeMethod_var;
extern const RuntimeMethod* UnityARUserAnchorComponent_AnchorRemoved_m2694785441_RuntimeMethod_var;
extern const uint32_t UnityARUserAnchorComponent_Awake_m695952178_MetadataUsageId;
extern RuntimeClass* Object_t1970767703_il2cpp_TypeInfo_var;
extern const uint32_t UnityARUserAnchorComponent_AnchorRemoved_m2694785441_MetadataUsageId;
extern const uint32_t UnityARUserAnchorComponent_OnDestroy_m4107013468_MetadataUsageId;
extern RuntimeClass* Marshal_t436595762_il2cpp_TypeInfo_var;
extern const uint32_t UnityARUserAnchorData_get_identifierStr_m3595119354_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t2375577114_il2cpp_TypeInfo_var;
extern const uint32_t UnityARUserAnchorData_UnityARUserAnchorDataFromGameObject_m3481842473_MetadataUsageId;
extern RuntimeClass* UnityARUtility_t4023213543_il2cpp_TypeInfo_var;
extern const uint32_t UnityARUtility_InitializePlanePrefab_m1931429385_MetadataUsageId;
extern RuntimeClass* GameObject_t1318052361_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1318052361_m3083066963_RuntimeMethod_var;
extern const uint32_t UnityARUtility_CreatePlaneInScene_m2545193760_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisMeshFilter_t1334808991_m2726185362_RuntimeMethod_var;
extern const uint32_t UnityARUtility_UpdatePlaneWithAnchorTransform_m796920091_MetadataUsageId;
extern const uint32_t UnityARUtility__cctor_m990246171_MetadataUsageId;
extern RuntimeClass* ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityARVideo_UpdateFrame_m1610612707_RuntimeMethod_var;
extern const uint32_t UnityARVideo_Start_m742982592_MetadataUsageId;
extern const uint32_t UnityARVideo_UpdateFrame_m1610612707_MetadataUsageId;
extern RuntimeClass* CommandBuffer_t2673047760_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t989002943_m3380036349_RuntimeMethod_var;
extern const uint32_t UnityARVideo_InitializeCommandBuffer_m1137915321_MetadataUsageId;
extern const uint32_t UnityARVideo_OnDestroy_m2358615504_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4120529429;
extern Il2CppCodeGenString* _stringLiteral194183554;
extern Il2CppCodeGenString* _stringLiteral2552841393;
extern const uint32_t UnityARVideo_OnPreRender_m618482650_MetadataUsageId;
extern RuntimeClass* UnityARDirectionalLightEstimate_t3433403765_il2cpp_TypeInfo_var;
extern const uint32_t UnityMarshalLightData_op_Implicit_m1490147344_MetadataUsageId;
extern const RuntimeMethod* UnityRemoteVideo_UpdateCamera_m15137629_RuntimeMethod_var;
extern const uint32_t UnityRemoteVideo_Start_m1559177737_MetadataUsageId;
extern const uint32_t UnityRemoteVideo_UpdateCamera_m15137629_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var;
extern const uint32_t UnityRemoteVideo_InitializeTextures_m1297643449_MetadataUsageId;
extern const uint32_t UnityRemoteVideo_OnDestroy_m3971316263_MetadataUsageId;
extern const uint32_t UnityRemoteVideo_OnPreRender_m1215563648_MetadataUsageId;
extern RuntimeClass* List_1_t1128502775_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityPointCloudExample_ARFrameUpdated_m2754349252_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1148151875_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m695416883_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Start_m4121028873_MetadataUsageId;
extern RuntimeClass* Vector4_t380635127_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m4071041560_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Update_m3035936549_MetadataUsageId;
extern RuntimeClass* BinaryFormatter_t3541706630_il2cpp_TypeInfo_var;
extern RuntimeClass* MemoryStream_t3384139290_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3363289168_il2cpp_TypeInfo_var;
extern const uint32_t ObjectSerializationExtension_SerializeToByteArray_m3367459946_MetadataUsageId;
extern RuntimeClass* serializableARSessionConfiguration_t3208486129_il2cpp_TypeInfo_var;
extern const uint32_t serializableARSessionConfiguration_op_Implicit_m343128120_MetadataUsageId;
extern RuntimeClass* BitConverter_t1229281639_il2cpp_TypeInfo_var;
extern RuntimeClass* serializablePointCloud_t2991476747_il2cpp_TypeInfo_var;
extern const uint32_t serializablePointCloud_op_Implicit_m4280869743_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t974944492_il2cpp_TypeInfo_var;
extern const uint32_t serializablePointCloud_op_Implicit_m3149335011_MetadataUsageId;
extern RuntimeClass* serializableSHC_t2973324561_il2cpp_TypeInfo_var;
extern const uint32_t serializableSHC_op_Implicit_m2702198786_MetadataUsageId;
extern RuntimeClass* SingleU5BU5D_t2905636975_il2cpp_TypeInfo_var;
extern const uint32_t serializableSHC_op_Implicit_m4253342705_MetadataUsageId;
extern RuntimeClass* serializableUnityARCamera_t3181320657_il2cpp_TypeInfo_var;
extern const uint32_t serializableUnityARCamera_op_Implicit_m3148128646_MetadataUsageId;
extern RuntimeClass* SerializableVector4_t3177127415_il2cpp_TypeInfo_var;
extern const uint32_t serializableUnityARLightData__ctor_m2325970984_MetadataUsageId;
extern RuntimeClass* serializableUnityARLightData_t1864664250_il2cpp_TypeInfo_var;
extern const uint32_t serializableUnityARLightData_op_Implicit_m1903111810_MetadataUsageId;
extern const uint32_t serializableUnityARLightData_op_Implicit_m322329394_MetadataUsageId;
extern RuntimeClass* serializableUnityARMatrix4x4_t3548294260_il2cpp_TypeInfo_var;
extern const uint32_t serializableUnityARMatrix4x4_op_Implicit_m1426572080_MetadataUsageId;
extern const uint32_t serializableUnityARMatrix4x4_op_Implicit_m319493325_MetadataUsageId;
extern RuntimeClass* Encoding_t3638904762_il2cpp_TypeInfo_var;
extern RuntimeClass* serializableUnityARPlaneAnchor_t3395010059_il2cpp_TypeInfo_var;
extern const uint32_t serializableUnityARPlaneAnchor_op_Implicit_m66415631_MetadataUsageId;
extern const uint32_t serializableUnityARPlaneAnchor_op_Implicit_m3608928610_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1863352746_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4079274232;
extern const uint32_t SerializableVector4_ToString_m2464175451_MetadataUsageId;
extern const uint32_t SerializableVector4_op_Implicit_m2125629402_MetadataUsageId;
struct Vector3_t329709361 ;

struct SingleU5BU5D_t2905636975;
struct ByteU5BU5D_t3287329517;
struct Vector3U5BU5D_t974944492;
struct ObjectU5BU5D_t1568665923;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STREAM_T3046340190_H
#define STREAM_T3046340190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3046340190  : public RuntimeObject
{
public:

public:
};

struct Stream_t3046340190_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3046340190 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3046340190_StaticFields, ___Null_0)); }
	inline Stream_t3046340190 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3046340190 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3046340190 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3046340190_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t83643201* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t83643201* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t83643201** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t83643201* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef SERIALIZABLEPOINTCLOUD_T2991476747_H
#define SERIALIZABLEPOINTCLOUD_T2991476747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializablePointCloud
struct  serializablePointCloud_t2991476747  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t3287329517* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t2991476747, ___pointCloudData_0)); }
	inline ByteU5BU5D_t3287329517* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t3287329517** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t3287329517* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T2991476747_H
#ifndef SERIALIZABLESHC_T2973324561_H
#define SERIALIZABLESHC_T2973324561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableSHC
struct  serializableSHC_t2973324561  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializableSHC::shcData
	ByteU5BU5D_t3287329517* ___shcData_0;

public:
	inline static int32_t get_offset_of_shcData_0() { return static_cast<int32_t>(offsetof(serializableSHC_t2973324561, ___shcData_0)); }
	inline ByteU5BU5D_t3287329517* get_shcData_0() const { return ___shcData_0; }
	inline ByteU5BU5D_t3287329517** get_address_of_shcData_0() { return &___shcData_0; }
	inline void set_shcData_0(ByteU5BU5D_t3287329517* value)
	{
		___shcData_0 = value;
		Il2CppCodeGenWriteBarrier((&___shcData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLESHC_T2973324561_H
#ifndef SERIALIZABLEUNITYARMATRIX4X4_T3548294260_H
#define SERIALIZABLEUNITYARMATRIX4X4_T3548294260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARMatrix4x4
struct  serializableUnityARMatrix4x4_t3548294260  : public RuntimeObject
{
public:
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column0
	SerializableVector4_t3177127415 * ___column0_0;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column1
	SerializableVector4_t3177127415 * ___column1_1;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column2
	SerializableVector4_t3177127415 * ___column2_2;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column3
	SerializableVector4_t3177127415 * ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t3548294260, ___column0_0)); }
	inline SerializableVector4_t3177127415 * get_column0_0() const { return ___column0_0; }
	inline SerializableVector4_t3177127415 ** get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(SerializableVector4_t3177127415 * value)
	{
		___column0_0 = value;
		Il2CppCodeGenWriteBarrier((&___column0_0), value);
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t3548294260, ___column1_1)); }
	inline SerializableVector4_t3177127415 * get_column1_1() const { return ___column1_1; }
	inline SerializableVector4_t3177127415 ** get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(SerializableVector4_t3177127415 * value)
	{
		___column1_1 = value;
		Il2CppCodeGenWriteBarrier((&___column1_1), value);
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t3548294260, ___column2_2)); }
	inline SerializableVector4_t3177127415 * get_column2_2() const { return ___column2_2; }
	inline SerializableVector4_t3177127415 ** get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(SerializableVector4_t3177127415 * value)
	{
		___column2_2 = value;
		Il2CppCodeGenWriteBarrier((&___column2_2), value);
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t3548294260, ___column3_3)); }
	inline SerializableVector4_t3177127415 * get_column3_3() const { return ___column3_3; }
	inline SerializableVector4_t3177127415 ** get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(SerializableVector4_t3177127415 * value)
	{
		___column3_3 = value;
		Il2CppCodeGenWriteBarrier((&___column3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARMATRIX4X4_T3548294260_H
#ifndef SERIALIZABLEVECTOR4_T3177127415_H
#define SERIALIZABLEVECTOR4_T3177127415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector4
struct  SerializableVector4_t3177127415  : public RuntimeObject
{
public:
	// System.Single Utils.SerializableVector4::x
	float ___x_0;
	// System.Single Utils.SerializableVector4::y
	float ___y_1;
	// System.Single Utils.SerializableVector4::z
	float ___z_2;
	// System.Single Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t3177127415, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t3177127415, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t3177127415, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t3177127415, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T3177127415_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T3484002545_H
#define OBJECTSERIALIZATIONEXTENSION_T3484002545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t3484002545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T3484002545_H
#ifndef ENCODING_T3638904762_H
#define ENCODING_T3638904762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t3638904762  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t527759059 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t3711323963 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___decoder_fallback_3)); }
	inline DecoderFallback_t527759059 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t527759059 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t527759059 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___encoder_fallback_4)); }
	inline EncoderFallback_t3711323963 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t3711323963 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t3711323963 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t3638904762, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t3638904762_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t2241791680 * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t1568665923* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t3638904762 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t3638904762 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t3638904762 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t3638904762 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t3638904762 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t3638904762 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t3638904762 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t3638904762 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t3638904762 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t3638904762 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t3638904762 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t2241791680 * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t2241791680 ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t2241791680 * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t1568665923* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t1568665923** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t1568665923* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t3638904762 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t3638904762 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t3638904762 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t3638904762 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t3638904762 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t3638904762 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t3638904762 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t3638904762 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t3638904762 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t3638904762 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t3638904762 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t3638904762 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t3638904762 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t3638904762 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t3638904762 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t3638904762 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t3638904762 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t3638904762 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t3638904762 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t3638904762 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t3638904762 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t3638904762 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t3638904762 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t3638904762 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t3638904762 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t3638904762 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t3638904762 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t3638904762 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t3638904762 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t3638904762 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t3638904762 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t3638904762 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t3638904762 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t3638904762_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T3638904762_H
#ifndef LIST_1_T1128502775_H
#define LIST_1_T1128502775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t1128502775  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t4209643444* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1128502775, ____items_1)); }
	inline GameObjectU5BU5D_t4209643444* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t4209643444** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t4209643444* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1128502775, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1128502775, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1128502775_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t4209643444* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1128502775_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t4209643444* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t4209643444** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t4209643444* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1128502775_H
#ifndef UNITYARUTILITY_T4023213543_H
#define UNITYARUTILITY_T4023213543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t4023213543  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t3189750085 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t1334808991 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t4023213543, ___meshCollider_0)); }
	inline MeshCollider_t3189750085 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t3189750085 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t3189750085 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t4023213543, ___meshFilter_1)); }
	inline MeshFilter_t1334808991 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t1334808991 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t1334808991 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t4023213543_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t1318052361 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t4023213543_StaticFields, ___planePrefab_2)); }
	inline GameObject_t1318052361 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1318052361 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1318052361 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T4023213543_H
#ifndef VALUETYPE_T1364887298_H
#define VALUETYPE_T1364887298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1364887298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_com
{
};
#endif // VALUETYPE_T1364887298_H
#ifndef QUATERNION_T2761156409_H
#define QUATERNION_T2761156409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2761156409 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2761156409_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2761156409  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2761156409  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2761156409 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2761156409  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2761156409_H
#ifndef MATRIX4X4_T2375577114_H
#define MATRIX4X4_T2375577114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2375577114 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2375577114_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2375577114  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2375577114  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2375577114  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2375577114 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2375577114  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2375577114  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2375577114 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2375577114  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2375577114_H
#ifndef VECTOR4_T380635127_H
#define VECTOR4_T380635127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t380635127 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t380635127_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t380635127  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t380635127  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t380635127  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t380635127  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___zeroVector_5)); }
	inline Vector4_t380635127  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t380635127 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t380635127  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___oneVector_6)); }
	inline Vector4_t380635127  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t380635127 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t380635127  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t380635127  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t380635127 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t380635127  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t380635127  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t380635127 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t380635127  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T380635127_H
#ifndef BOOLEAN_T569405246_H
#define BOOLEAN_T569405246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t569405246 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t569405246, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t569405246_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T569405246_H
#ifndef GCHANDLE_T714486722_H
#define GCHANDLE_T714486722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t714486722 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t714486722, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T714486722_H
#ifndef INT32_T499004851_H
#define INT32_T499004851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t499004851 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t499004851, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T499004851_H
#ifndef UNITYARLIGHTESTIMATE_T2392533559_H
#define UNITYARLIGHTESTIMATE_T2392533559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARLightEstimate
struct  UnityARLightEstimate_t2392533559 
{
public:
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientIntensity
	float ___ambientIntensity_0;
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientColorTemperature
	float ___ambientColorTemperature_1;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_t2392533559, ___ambientIntensity_0)); }
	inline float get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline float* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(float value)
	{
		___ambientIntensity_0 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_1() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_t2392533559, ___ambientColorTemperature_1)); }
	inline float get_ambientColorTemperature_1() const { return ___ambientColorTemperature_1; }
	inline float* get_address_of_ambientColorTemperature_1() { return &___ambientColorTemperature_1; }
	inline void set_ambientColorTemperature_1(float value)
	{
		___ambientColorTemperature_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARLIGHTESTIMATE_T2392533559_H
#ifndef BYTE_T2815932036_H
#define BYTE_T2815932036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t2815932036 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t2815932036, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T2815932036_H
#ifndef SINGLE_T1863352746_H
#define SINGLE_T1863352746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1863352746 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1863352746, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1863352746_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2116050852 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2116050852 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2116050852 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2116050852 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2116050852 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2116050852 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2116050852 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2116050852 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef UINT32_T3311932136_H
#define UINT32_T3311932136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3311932136 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3311932136, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3311932136_H
#ifndef RESOLUTION_T1041838152_H
#define RESOLUTION_T1041838152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resolution
struct  Resolution_t1041838152 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T1041838152_H
#ifndef VECTOR3_T329709361_H
#define VECTOR3_T329709361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t329709361 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t329709361_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t329709361  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t329709361  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t329709361  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t329709361  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t329709361  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t329709361  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t329709361  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t329709361  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t329709361  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t329709361  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___zeroVector_4)); }
	inline Vector3_t329709361  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t329709361 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t329709361  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___oneVector_5)); }
	inline Vector3_t329709361  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t329709361 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t329709361  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___upVector_6)); }
	inline Vector3_t329709361  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t329709361 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t329709361  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___downVector_7)); }
	inline Vector3_t329709361  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t329709361 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t329709361  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___leftVector_8)); }
	inline Vector3_t329709361  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t329709361 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t329709361  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___rightVector_9)); }
	inline Vector3_t329709361  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t329709361 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t329709361  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___forwardVector_10)); }
	inline Vector3_t329709361  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t329709361 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t329709361  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___backVector_11)); }
	inline Vector3_t329709361  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t329709361 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t329709361  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t329709361  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t329709361 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t329709361  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t329709361  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t329709361 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t329709361  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T329709361_H
#ifndef INT64_T3070791913_H
#define INT64_T3070791913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3070791913 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3070791913, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3070791913_H
#ifndef MEMORYSTREAM_T3384139290_H
#define MEMORYSTREAM_T3384139290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t3384139290  : public Stream_t3046340190
{
public:
	// System.Boolean System.IO.MemoryStream::canWrite
	bool ___canWrite_1;
	// System.Boolean System.IO.MemoryStream::allowGetBuffer
	bool ___allowGetBuffer_2;
	// System.Int32 System.IO.MemoryStream::capacity
	int32_t ___capacity_3;
	// System.Int32 System.IO.MemoryStream::length
	int32_t ___length_4;
	// System.Byte[] System.IO.MemoryStream::internalBuffer
	ByteU5BU5D_t3287329517* ___internalBuffer_5;
	// System.Int32 System.IO.MemoryStream::initialIndex
	int32_t ___initialIndex_6;
	// System.Boolean System.IO.MemoryStream::expandable
	bool ___expandable_7;
	// System.Boolean System.IO.MemoryStream::streamClosed
	bool ___streamClosed_8;
	// System.Int32 System.IO.MemoryStream::position
	int32_t ___position_9;
	// System.Int32 System.IO.MemoryStream::dirty_bytes
	int32_t ___dirty_bytes_10;

public:
	inline static int32_t get_offset_of_canWrite_1() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___canWrite_1)); }
	inline bool get_canWrite_1() const { return ___canWrite_1; }
	inline bool* get_address_of_canWrite_1() { return &___canWrite_1; }
	inline void set_canWrite_1(bool value)
	{
		___canWrite_1 = value;
	}

	inline static int32_t get_offset_of_allowGetBuffer_2() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___allowGetBuffer_2)); }
	inline bool get_allowGetBuffer_2() const { return ___allowGetBuffer_2; }
	inline bool* get_address_of_allowGetBuffer_2() { return &___allowGetBuffer_2; }
	inline void set_allowGetBuffer_2(bool value)
	{
		___allowGetBuffer_2 = value;
	}

	inline static int32_t get_offset_of_capacity_3() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___capacity_3)); }
	inline int32_t get_capacity_3() const { return ___capacity_3; }
	inline int32_t* get_address_of_capacity_3() { return &___capacity_3; }
	inline void set_capacity_3(int32_t value)
	{
		___capacity_3 = value;
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_internalBuffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___internalBuffer_5)); }
	inline ByteU5BU5D_t3287329517* get_internalBuffer_5() const { return ___internalBuffer_5; }
	inline ByteU5BU5D_t3287329517** get_address_of_internalBuffer_5() { return &___internalBuffer_5; }
	inline void set_internalBuffer_5(ByteU5BU5D_t3287329517* value)
	{
		___internalBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalBuffer_5), value);
	}

	inline static int32_t get_offset_of_initialIndex_6() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___initialIndex_6)); }
	inline int32_t get_initialIndex_6() const { return ___initialIndex_6; }
	inline int32_t* get_address_of_initialIndex_6() { return &___initialIndex_6; }
	inline void set_initialIndex_6(int32_t value)
	{
		___initialIndex_6 = value;
	}

	inline static int32_t get_offset_of_expandable_7() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___expandable_7)); }
	inline bool get_expandable_7() const { return ___expandable_7; }
	inline bool* get_address_of_expandable_7() { return &___expandable_7; }
	inline void set_expandable_7(bool value)
	{
		___expandable_7 = value;
	}

	inline static int32_t get_offset_of_streamClosed_8() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___streamClosed_8)); }
	inline bool get_streamClosed_8() const { return ___streamClosed_8; }
	inline bool* get_address_of_streamClosed_8() { return &___streamClosed_8; }
	inline void set_streamClosed_8(bool value)
	{
		___streamClosed_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___position_9)); }
	inline int32_t get_position_9() const { return ___position_9; }
	inline int32_t* get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(int32_t value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_dirty_bytes_10() { return static_cast<int32_t>(offsetof(MemoryStream_t3384139290, ___dirty_bytes_10)); }
	inline int32_t get_dirty_bytes_10() const { return ___dirty_bytes_10; }
	inline int32_t* get_address_of_dirty_bytes_10() { return &___dirty_bytes_10; }
	inline void set_dirty_bytes_10(int32_t value)
	{
		___dirty_bytes_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T3384139290_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T2642135423_H
#define VOID_T2642135423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2642135423 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2642135423_H
#ifndef ENUM_T3173835468_H
#define ENUM_T3173835468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t3173835468  : public ValueType_t1364887298
{
public:

public:
};

struct Enum_t3173835468_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t83643201* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t3173835468_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t83643201* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t83643201** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t83643201* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t3173835468_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t3173835468_marshaled_com
{
};
#endif // ENUM_T3173835468_H
#ifndef TYPEFILTERLEVEL_T677136017_H
#define TYPEFILTERLEVEL_T677136017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.TypeFilterLevel
struct  TypeFilterLevel_t677136017 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.TypeFilterLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilterLevel_t677136017, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERLEVEL_T677136017_H
#ifndef TEXTUREFORMAT_T2369697549_H
#define TEXTUREFORMAT_T2369697549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2369697549 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2369697549, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2369697549_H
#ifndef FILTERMODE_T4175348091_H
#define FILTERMODE_T4175348091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t4175348091 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t4175348091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T4175348091_H
#ifndef TEXTUREWRAPMODE_T287651694_H
#define TEXTUREWRAPMODE_T287651694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t287651694 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t287651694, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T287651694_H
#ifndef LIGHTDATATYPE_T4254216542_H
#define LIGHTDATATYPE_T4254216542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.LightDataType
struct  LightDataType_t4254216542 
{
public:
	// System.Int32 UnityEngine.XR.iOS.LightDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightDataType_t4254216542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTDATATYPE_T4254216542_H
#ifndef FORMATTERTYPESTYLE_T1604256750_H
#define FORMATTERTYPESTYLE_T1604256750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterTypeStyle
struct  FormatterTypeStyle_t1604256750 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterTypeStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterTypeStyle_t1604256750, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERTYPESTYLE_T1604256750_H
#ifndef FORMATTERASSEMBLYSTYLE_T3750086223_H
#define FORMATTERASSEMBLYSTYLE_T3750086223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_t3750086223 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_t3750086223, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_T3750086223_H
#ifndef MARSHALDIRECTIONALLIGHTESTIMATE_T3103061175_H
#define MARSHALDIRECTIONALLIGHTESTIMATE_T3103061175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MarshalDirectionalLightEstimate
struct  MarshalDirectionalLightEstimate_t3103061175 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.MarshalDirectionalLightEstimate::primaryDirAndIntensity
	Vector4_t380635127  ___primaryDirAndIntensity_0;
	// System.IntPtr UnityEngine.XR.iOS.MarshalDirectionalLightEstimate::sphericalHarmonicCoefficientsPtr
	intptr_t ___sphericalHarmonicCoefficientsPtr_1;

public:
	inline static int32_t get_offset_of_primaryDirAndIntensity_0() { return static_cast<int32_t>(offsetof(MarshalDirectionalLightEstimate_t3103061175, ___primaryDirAndIntensity_0)); }
	inline Vector4_t380635127  get_primaryDirAndIntensity_0() const { return ___primaryDirAndIntensity_0; }
	inline Vector4_t380635127 * get_address_of_primaryDirAndIntensity_0() { return &___primaryDirAndIntensity_0; }
	inline void set_primaryDirAndIntensity_0(Vector4_t380635127  value)
	{
		___primaryDirAndIntensity_0 = value;
	}

	inline static int32_t get_offset_of_sphericalHarmonicCoefficientsPtr_1() { return static_cast<int32_t>(offsetof(MarshalDirectionalLightEstimate_t3103061175, ___sphericalHarmonicCoefficientsPtr_1)); }
	inline intptr_t get_sphericalHarmonicCoefficientsPtr_1() const { return ___sphericalHarmonicCoefficientsPtr_1; }
	inline intptr_t* get_address_of_sphericalHarmonicCoefficientsPtr_1() { return &___sphericalHarmonicCoefficientsPtr_1; }
	inline void set_sphericalHarmonicCoefficientsPtr_1(intptr_t value)
	{
		___sphericalHarmonicCoefficientsPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALDIRECTIONALLIGHTESTIMATE_T3103061175_H
#ifndef UNITYARDIRECTIONALLIGHTESTIMATE_T3433403765_H
#define UNITYARDIRECTIONALLIGHTESTIMATE_T3433403765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARDirectionalLightEstimate
struct  UnityARDirectionalLightEstimate_t3433403765  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARDirectionalLightEstimate::primaryLightDirection
	Vector3_t329709361  ___primaryLightDirection_0;
	// System.Single UnityEngine.XR.iOS.UnityARDirectionalLightEstimate::primaryLightIntensity
	float ___primaryLightIntensity_1;
	// System.Single[] UnityEngine.XR.iOS.UnityARDirectionalLightEstimate::sphericalHarmonicsCoefficients
	SingleU5BU5D_t2905636975* ___sphericalHarmonicsCoefficients_2;

public:
	inline static int32_t get_offset_of_primaryLightDirection_0() { return static_cast<int32_t>(offsetof(UnityARDirectionalLightEstimate_t3433403765, ___primaryLightDirection_0)); }
	inline Vector3_t329709361  get_primaryLightDirection_0() const { return ___primaryLightDirection_0; }
	inline Vector3_t329709361 * get_address_of_primaryLightDirection_0() { return &___primaryLightDirection_0; }
	inline void set_primaryLightDirection_0(Vector3_t329709361  value)
	{
		___primaryLightDirection_0 = value;
	}

	inline static int32_t get_offset_of_primaryLightIntensity_1() { return static_cast<int32_t>(offsetof(UnityARDirectionalLightEstimate_t3433403765, ___primaryLightIntensity_1)); }
	inline float get_primaryLightIntensity_1() const { return ___primaryLightIntensity_1; }
	inline float* get_address_of_primaryLightIntensity_1() { return &___primaryLightIntensity_1; }
	inline void set_primaryLightIntensity_1(float value)
	{
		___primaryLightIntensity_1 = value;
	}

	inline static int32_t get_offset_of_sphericalHarmonicsCoefficients_2() { return static_cast<int32_t>(offsetof(UnityARDirectionalLightEstimate_t3433403765, ___sphericalHarmonicsCoefficients_2)); }
	inline SingleU5BU5D_t2905636975* get_sphericalHarmonicsCoefficients_2() const { return ___sphericalHarmonicsCoefficients_2; }
	inline SingleU5BU5D_t2905636975** get_address_of_sphericalHarmonicsCoefficients_2() { return &___sphericalHarmonicsCoefficients_2; }
	inline void set_sphericalHarmonicsCoefficients_2(SingleU5BU5D_t2905636975* value)
	{
		___sphericalHarmonicsCoefficients_2 = value;
		Il2CppCodeGenWriteBarrier((&___sphericalHarmonicsCoefficients_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARDIRECTIONALLIGHTESTIMATE_T3433403765_H
#ifndef CUBEMAPFACE_T4057844954_H
#define CUBEMAPFACE_T4057844954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t4057844954 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t4057844954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T4057844954_H
#ifndef UNITYARFACEGEOMETRY_T458757803_H
#define UNITYARFACEGEOMETRY_T458757803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceGeometry
struct  UnityARFaceGeometry_t458757803 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::vertexCount
	int32_t ___vertexCount_0;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::vertices
	intptr_t ___vertices_1;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinateCount
	int32_t ___textureCoordinateCount_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinates
	intptr_t ___textureCoordinates_3;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::triangleCount
	int32_t ___triangleCount_4;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::triangleIndices
	intptr_t ___triangleIndices_5;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___vertices_1)); }
	inline intptr_t get_vertices_1() const { return ___vertices_1; }
	inline intptr_t* get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(intptr_t value)
	{
		___vertices_1 = value;
	}

	inline static int32_t get_offset_of_textureCoordinateCount_2() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___textureCoordinateCount_2)); }
	inline int32_t get_textureCoordinateCount_2() const { return ___textureCoordinateCount_2; }
	inline int32_t* get_address_of_textureCoordinateCount_2() { return &___textureCoordinateCount_2; }
	inline void set_textureCoordinateCount_2(int32_t value)
	{
		___textureCoordinateCount_2 = value;
	}

	inline static int32_t get_offset_of_textureCoordinates_3() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___textureCoordinates_3)); }
	inline intptr_t get_textureCoordinates_3() const { return ___textureCoordinates_3; }
	inline intptr_t* get_address_of_textureCoordinates_3() { return &___textureCoordinates_3; }
	inline void set_textureCoordinates_3(intptr_t value)
	{
		___textureCoordinates_3 = value;
	}

	inline static int32_t get_offset_of_triangleCount_4() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___triangleCount_4)); }
	inline int32_t get_triangleCount_4() const { return ___triangleCount_4; }
	inline int32_t* get_address_of_triangleCount_4() { return &___triangleCount_4; }
	inline void set_triangleCount_4(int32_t value)
	{
		___triangleCount_4 = value;
	}

	inline static int32_t get_offset_of_triangleIndices_5() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t458757803, ___triangleIndices_5)); }
	inline intptr_t get_triangleIndices_5() const { return ___triangleIndices_5; }
	inline intptr_t* get_address_of_triangleIndices_5() { return &___triangleIndices_5; }
	inline void set_triangleIndices_5(intptr_t value)
	{
		___triangleIndices_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEGEOMETRY_T458757803_H
#ifndef ARPLANEANCHORALIGNMENT_T3689212207_H
#define ARPLANEANCHORALIGNMENT_T3689212207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t3689212207 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t3689212207, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T3689212207_H
#ifndef ARTRACKINGSTATEREASON_T2037315270_H
#define ARTRACKINGSTATEREASON_T2037315270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2037315270 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2037315270, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2037315270_H
#ifndef ARTRACKINGSTATE_T391567183_H
#define ARTRACKINGSTATE_T391567183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t391567183 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t391567183, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T391567183_H
#ifndef UNITYVIDEOPARAMS_T909694111_H
#define UNITYVIDEOPARAMS_T909694111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityVideoParams
struct  UnityVideoParams_t909694111 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yWidth
	int32_t ___yWidth_0;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yHeight
	int32_t ___yHeight_1;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::screenOrientation
	int32_t ___screenOrientation_2;
	// System.Single UnityEngine.XR.iOS.UnityVideoParams::texCoordScale
	float ___texCoordScale_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityVideoParams::cvPixelBufferPtr
	intptr_t ___cvPixelBufferPtr_4;

public:
	inline static int32_t get_offset_of_yWidth_0() { return static_cast<int32_t>(offsetof(UnityVideoParams_t909694111, ___yWidth_0)); }
	inline int32_t get_yWidth_0() const { return ___yWidth_0; }
	inline int32_t* get_address_of_yWidth_0() { return &___yWidth_0; }
	inline void set_yWidth_0(int32_t value)
	{
		___yWidth_0 = value;
	}

	inline static int32_t get_offset_of_yHeight_1() { return static_cast<int32_t>(offsetof(UnityVideoParams_t909694111, ___yHeight_1)); }
	inline int32_t get_yHeight_1() const { return ___yHeight_1; }
	inline int32_t* get_address_of_yHeight_1() { return &___yHeight_1; }
	inline void set_yHeight_1(int32_t value)
	{
		___yHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenOrientation_2() { return static_cast<int32_t>(offsetof(UnityVideoParams_t909694111, ___screenOrientation_2)); }
	inline int32_t get_screenOrientation_2() const { return ___screenOrientation_2; }
	inline int32_t* get_address_of_screenOrientation_2() { return &___screenOrientation_2; }
	inline void set_screenOrientation_2(int32_t value)
	{
		___screenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_texCoordScale_3() { return static_cast<int32_t>(offsetof(UnityVideoParams_t909694111, ___texCoordScale_3)); }
	inline float get_texCoordScale_3() const { return ___texCoordScale_3; }
	inline float* get_address_of_texCoordScale_3() { return &___texCoordScale_3; }
	inline void set_texCoordScale_3(float value)
	{
		___texCoordScale_3 = value;
	}

	inline static int32_t get_offset_of_cvPixelBufferPtr_4() { return static_cast<int32_t>(offsetof(UnityVideoParams_t909694111, ___cvPixelBufferPtr_4)); }
	inline intptr_t get_cvPixelBufferPtr_4() const { return ___cvPixelBufferPtr_4; }
	inline intptr_t* get_address_of_cvPixelBufferPtr_4() { return &___cvPixelBufferPtr_4; }
	inline void set_cvPixelBufferPtr_4(intptr_t value)
	{
		___cvPixelBufferPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVIDEOPARAMS_T909694111_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T1383667999_H
#define SERIALIZABLEFROMEDITORMESSAGE_T1383667999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t1383667999  : public RuntimeObject
{
public:
	// System.Guid Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// Utils.serializableARKitInit Utils.serializableFromEditorMessage::arkitConfigMsg
	serializableARKitInit_t403564667 * ___arkitConfigMsg_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t1383667999, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_arkitConfigMsg_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t1383667999, ___arkitConfigMsg_1)); }
	inline serializableARKitInit_t403564667 * get_arkitConfigMsg_1() const { return ___arkitConfigMsg_1; }
	inline serializableARKitInit_t403564667 ** get_address_of_arkitConfigMsg_1() { return &___arkitConfigMsg_1; }
	inline void set_arkitConfigMsg_1(serializableARKitInit_t403564667 * value)
	{
		___arkitConfigMsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___arkitConfigMsg_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T1383667999_H
#ifndef GCHANDLETYPE_T4280026598_H
#define GCHANDLETYPE_T4280026598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandleType
struct  GCHandleType_t4280026598 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GCHandleType_t4280026598, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLETYPE_T4280026598_H
#ifndef UNITYARPLANEDETECTION_T4236545235_H
#define UNITYARPLANEDETECTION_T4236545235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t4236545235 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t4236545235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T4236545235_H
#ifndef UNITYARALIGNMENT_T2394317114_H
#define UNITYARALIGNMENT_T2394317114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t2394317114 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t2394317114, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T2394317114_H
#ifndef DELEGATE_T1563516729_H
#define DELEGATE_T1563516729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1563516729  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t975501551 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___data_8)); }
	inline DelegateData_t975501551 * get_data_8() const { return ___data_8; }
	inline DelegateData_t975501551 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t975501551 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1563516729_H
#ifndef COMMANDBUFFER_T2673047760_H
#define COMMANDBUFFER_T2673047760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t2673047760  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t2673047760, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T2673047760_H
#ifndef ARTEXTUREHANDLES_T1453164273_H
#define ARTEXTUREHANDLES_T1453164273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTextureHandles
struct  ARTextureHandles_t1453164273 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureY
	intptr_t ___textureY_0;
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureCbCr
	intptr_t ___textureCbCr_1;

public:
	inline static int32_t get_offset_of_textureY_0() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1453164273, ___textureY_0)); }
	inline intptr_t get_textureY_0() const { return ___textureY_0; }
	inline intptr_t* get_address_of_textureY_0() { return &___textureY_0; }
	inline void set_textureY_0(intptr_t value)
	{
		___textureY_0 = value;
	}

	inline static int32_t get_offset_of_textureCbCr_1() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1453164273, ___textureCbCr_1)); }
	inline intptr_t get_textureCbCr_1() const { return ___textureCbCr_1; }
	inline intptr_t* get_address_of_textureCbCr_1() { return &___textureCbCr_1; }
	inline void set_textureCbCr_1(intptr_t value)
	{
		___textureCbCr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTEXTUREHANDLES_T1453164273_H
#ifndef STREAMINGCONTEXTSTATES_T1978022100_H
#define STREAMINGCONTEXTSTATES_T1978022100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t1978022100 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t1978022100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T1978022100_H
#ifndef OBJECT_T1970767703_H
#define OBJECT_T1970767703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1970767703  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1970767703, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1970767703_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1970767703_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1970767703_H
#ifndef CAMERAEVENT_T2770737965_H
#define CAMERAEVENT_T2770737965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t2770737965 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t2770737965, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T2770737965_H
#ifndef UNITYARMATRIX4X4_T758723042_H
#define UNITYARMATRIX4X4_T758723042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t758723042 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t380635127  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t380635127  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t380635127  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t380635127  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t758723042, ___column0_0)); }
	inline Vector4_t380635127  get_column0_0() const { return ___column0_0; }
	inline Vector4_t380635127 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t380635127  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t758723042, ___column1_1)); }
	inline Vector4_t380635127  get_column1_1() const { return ___column1_1; }
	inline Vector4_t380635127 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t380635127  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t758723042, ___column2_2)); }
	inline Vector4_t380635127  get_column2_2() const { return ___column2_2; }
	inline Vector4_t380635127 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t380635127  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t758723042, ___column3_3)); }
	inline Vector4_t380635127  get_column3_3() const { return ___column3_3; }
	inline Vector4_t380635127 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t380635127  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T758723042_H
#ifndef UNITYARSESSIONRUNOPTION_T2827180039_H
#define UNITYARSESSIONRUNOPTION_T2827180039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t2827180039 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t2827180039, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T2827180039_H
#ifndef BUILTINRENDERTEXTURETYPE_T3831699340_H
#define BUILTINRENDERTEXTURETYPE_T3831699340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t3831699340 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t3831699340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T3831699340_H
#ifndef ARUSERANCHOR_T2452746139_H
#define ARUSERANCHOR_T2452746139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARUserAnchor
struct  ARUserAnchor_t2452746139 
{
public:
	// System.String UnityEngine.XR.iOS.ARUserAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARUserAnchor::transform
	Matrix4x4_t2375577114  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARUserAnchor_t2452746139, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARUserAnchor_t2452746139, ___transform_1)); }
	inline Matrix4x4_t2375577114  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t2375577114 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t2375577114  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_t2452746139_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t2375577114  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_t2452746139_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t2375577114  ___transform_1;
};
#endif // ARUSERANCHOR_T2452746139_H
#ifndef UNITYARANCHORDATA_T4160663845_H
#define UNITYARANCHORDATA_T4160663845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t4160663845 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t758723042  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t380635127  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t380635127  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t4160663845, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t4160663845, ___transform_1)); }
	inline UnityARMatrix4x4_t758723042  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t758723042  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t4160663845, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t4160663845, ___center_3)); }
	inline Vector4_t380635127  get_center_3() const { return ___center_3; }
	inline Vector4_t380635127 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t380635127  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t4160663845, ___extent_4)); }
	inline Vector4_t380635127  get_extent_4() const { return ___extent_4; }
	inline Vector4_t380635127 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t380635127  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T4160663845_H
#ifndef SERIALIZABLEUNITYARPLANEANCHOR_T3395010059_H
#define SERIALIZABLEUNITYARPLANEANCHOR_T3395010059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARPlaneAnchor
struct  serializableUnityARPlaneAnchor_t3395010059  : public RuntimeObject
{
public:
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARPlaneAnchor::worldTransform
	serializableUnityARMatrix4x4_t3548294260 * ___worldTransform_0;
	// Utils.SerializableVector4 Utils.serializableUnityARPlaneAnchor::center
	SerializableVector4_t3177127415 * ___center_1;
	// Utils.SerializableVector4 Utils.serializableUnityARPlaneAnchor::extent
	SerializableVector4_t3177127415 * ___extent_2;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment Utils.serializableUnityARPlaneAnchor::planeAlignment
	int64_t ___planeAlignment_3;
	// System.Byte[] Utils.serializableUnityARPlaneAnchor::identifierStr
	ByteU5BU5D_t3287329517* ___identifierStr_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3395010059, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t3548294260 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t3548294260 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t3548294260 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_center_1() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3395010059, ___center_1)); }
	inline SerializableVector4_t3177127415 * get_center_1() const { return ___center_1; }
	inline SerializableVector4_t3177127415 ** get_address_of_center_1() { return &___center_1; }
	inline void set_center_1(SerializableVector4_t3177127415 * value)
	{
		___center_1 = value;
		Il2CppCodeGenWriteBarrier((&___center_1), value);
	}

	inline static int32_t get_offset_of_extent_2() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3395010059, ___extent_2)); }
	inline SerializableVector4_t3177127415 * get_extent_2() const { return ___extent_2; }
	inline SerializableVector4_t3177127415 ** get_address_of_extent_2() { return &___extent_2; }
	inline void set_extent_2(SerializableVector4_t3177127415 * value)
	{
		___extent_2 = value;
		Il2CppCodeGenWriteBarrier((&___extent_2), value);
	}

	inline static int32_t get_offset_of_planeAlignment_3() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3395010059, ___planeAlignment_3)); }
	inline int64_t get_planeAlignment_3() const { return ___planeAlignment_3; }
	inline int64_t* get_address_of_planeAlignment_3() { return &___planeAlignment_3; }
	inline void set_planeAlignment_3(int64_t value)
	{
		___planeAlignment_3 = value;
	}

	inline static int32_t get_offset_of_identifierStr_4() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3395010059, ___identifierStr_4)); }
	inline ByteU5BU5D_t3287329517* get_identifierStr_4() const { return ___identifierStr_4; }
	inline ByteU5BU5D_t3287329517** get_address_of_identifierStr_4() { return &___identifierStr_4; }
	inline void set_identifierStr_4(ByteU5BU5D_t3287329517* value)
	{
		___identifierStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARPLANEANCHOR_T3395010059_H
#ifndef SERIALIZABLEUNITYARLIGHTDATA_T1864664250_H
#define SERIALIZABLEUNITYARLIGHTDATA_T1864664250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARLightData
struct  serializableUnityARLightData_t1864664250  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.LightDataType Utils.serializableUnityARLightData::whichLight
	int32_t ___whichLight_0;
	// Utils.serializableSHC Utils.serializableUnityARLightData::lightSHC
	serializableSHC_t2973324561 * ___lightSHC_1;
	// Utils.SerializableVector4 Utils.serializableUnityARLightData::primaryLightDirAndIntensity
	SerializableVector4_t3177127415 * ___primaryLightDirAndIntensity_2;
	// System.Single Utils.serializableUnityARLightData::ambientIntensity
	float ___ambientIntensity_3;
	// System.Single Utils.serializableUnityARLightData::ambientColorTemperature
	float ___ambientColorTemperature_4;

public:
	inline static int32_t get_offset_of_whichLight_0() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t1864664250, ___whichLight_0)); }
	inline int32_t get_whichLight_0() const { return ___whichLight_0; }
	inline int32_t* get_address_of_whichLight_0() { return &___whichLight_0; }
	inline void set_whichLight_0(int32_t value)
	{
		___whichLight_0 = value;
	}

	inline static int32_t get_offset_of_lightSHC_1() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t1864664250, ___lightSHC_1)); }
	inline serializableSHC_t2973324561 * get_lightSHC_1() const { return ___lightSHC_1; }
	inline serializableSHC_t2973324561 ** get_address_of_lightSHC_1() { return &___lightSHC_1; }
	inline void set_lightSHC_1(serializableSHC_t2973324561 * value)
	{
		___lightSHC_1 = value;
		Il2CppCodeGenWriteBarrier((&___lightSHC_1), value);
	}

	inline static int32_t get_offset_of_primaryLightDirAndIntensity_2() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t1864664250, ___primaryLightDirAndIntensity_2)); }
	inline SerializableVector4_t3177127415 * get_primaryLightDirAndIntensity_2() const { return ___primaryLightDirAndIntensity_2; }
	inline SerializableVector4_t3177127415 ** get_address_of_primaryLightDirAndIntensity_2() { return &___primaryLightDirAndIntensity_2; }
	inline void set_primaryLightDirAndIntensity_2(SerializableVector4_t3177127415 * value)
	{
		___primaryLightDirAndIntensity_2 = value;
		Il2CppCodeGenWriteBarrier((&___primaryLightDirAndIntensity_2), value);
	}

	inline static int32_t get_offset_of_ambientIntensity_3() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t1864664250, ___ambientIntensity_3)); }
	inline float get_ambientIntensity_3() const { return ___ambientIntensity_3; }
	inline float* get_address_of_ambientIntensity_3() { return &___ambientIntensity_3; }
	inline void set_ambientIntensity_3(float value)
	{
		___ambientIntensity_3 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_4() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t1864664250, ___ambientColorTemperature_4)); }
	inline float get_ambientColorTemperature_4() const { return ___ambientColorTemperature_4; }
	inline float* get_address_of_ambientColorTemperature_4() { return &___ambientColorTemperature_4; }
	inline void set_ambientColorTemperature_4(float value)
	{
		___ambientColorTemperature_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARLIGHTDATA_T1864664250_H
#ifndef UNITYARUSERANCHORDATA_T1928721163_H
#define UNITYARUSERANCHORDATA_T1928721163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorData
struct  UnityARUserAnchorData_t1928721163 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARUserAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARUserAnchorData::transform
	UnityARMatrix4x4_t758723042  ___transform_1;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t1928721163, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t1928721163, ___transform_1)); }
	inline UnityARMatrix4x4_t758723042  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t758723042  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORDATA_T1928721163_H
#ifndef SERIALIZABLEUNITYARCAMERA_T3181320657_H
#define SERIALIZABLEUNITYARCAMERA_T3181320657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARCamera
struct  serializableUnityARCamera_t3181320657  : public RuntimeObject
{
public:
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::worldTransform
	serializableUnityARMatrix4x4_t3548294260 * ___worldTransform_0;
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::projectionMatrix
	serializableUnityARMatrix4x4_t3548294260 * ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState Utils.serializableUnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason Utils.serializableUnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams Utils.serializableUnityARCamera::videoParams
	UnityVideoParams_t909694111  ___videoParams_4;
	// Utils.serializableUnityARLightData Utils.serializableUnityARCamera::lightData
	serializableUnityARLightData_t1864664250 * ___lightData_5;
	// Utils.serializablePointCloud Utils.serializableUnityARCamera::pointCloud
	serializablePointCloud_t2991476747 * ___pointCloud_6;
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::displayTransform
	serializableUnityARMatrix4x4_t3548294260 * ___displayTransform_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t3548294260 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t3548294260 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t3548294260 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___projectionMatrix_1)); }
	inline serializableUnityARMatrix4x4_t3548294260 * get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline serializableUnityARMatrix4x4_t3548294260 ** get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(serializableUnityARMatrix4x4_t3548294260 * value)
	{
		___projectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___projectionMatrix_1), value);
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___videoParams_4)); }
	inline UnityVideoParams_t909694111  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t909694111 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t909694111  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___lightData_5)); }
	inline serializableUnityARLightData_t1864664250 * get_lightData_5() const { return ___lightData_5; }
	inline serializableUnityARLightData_t1864664250 ** get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(serializableUnityARLightData_t1864664250 * value)
	{
		___lightData_5 = value;
		Il2CppCodeGenWriteBarrier((&___lightData_5), value);
	}

	inline static int32_t get_offset_of_pointCloud_6() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___pointCloud_6)); }
	inline serializablePointCloud_t2991476747 * get_pointCloud_6() const { return ___pointCloud_6; }
	inline serializablePointCloud_t2991476747 ** get_address_of_pointCloud_6() { return &___pointCloud_6; }
	inline void set_pointCloud_6(serializablePointCloud_t2991476747 * value)
	{
		___pointCloud_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloud_6), value);
	}

	inline static int32_t get_offset_of_displayTransform_7() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t3181320657, ___displayTransform_7)); }
	inline serializableUnityARMatrix4x4_t3548294260 * get_displayTransform_7() const { return ___displayTransform_7; }
	inline serializableUnityARMatrix4x4_t3548294260 ** get_address_of_displayTransform_7() { return &___displayTransform_7; }
	inline void set_displayTransform_7(serializableUnityARMatrix4x4_t3548294260 * value)
	{
		___displayTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___displayTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARCAMERA_T3181320657_H
#ifndef ARKITWORLDTRACKINGSESSIONCONFIGURATION_T4171655616_H
#define ARKITWORLDTRACKINGSESSIONCONFIGURATION_T4171655616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct  ARKitWorldTrackingSessionConfiguration_t4171655616 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t4171655616, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t4171655616, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t4171655616, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t4171655616, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct ARKitWorldTrackingSessionConfiguration_t4171655616_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct ARKitWorldTrackingSessionConfiguration_t4171655616_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
#endif // ARKITWORLDTRACKINGSESSIONCONFIGURATION_T4171655616_H
#ifndef SERIALIZABLEARSESSIONCONFIGURATION_T3208486129_H
#define SERIALIZABLEARSESSIONCONFIGURATION_T3208486129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableARSessionConfiguration
struct  serializableARSessionConfiguration_t3208486129  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment Utils.serializableARSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection Utils.serializableARSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean Utils.serializableARSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean Utils.serializableARSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t3208486129, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t3208486129, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t3208486129, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t3208486129, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARSESSIONCONFIGURATION_T3208486129_H
#ifndef SERIALIZABLEARKITINIT_T403564667_H
#define SERIALIZABLEARKITINIT_T403564667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableARKitInit
struct  serializableARKitInit_t403564667  : public RuntimeObject
{
public:
	// Utils.serializableARSessionConfiguration Utils.serializableARKitInit::config
	serializableARSessionConfiguration_t3208486129 * ___config_0;
	// UnityEngine.XR.iOS.UnityARSessionRunOption Utils.serializableARKitInit::runOption
	int32_t ___runOption_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(serializableARKitInit_t403564667, ___config_0)); }
	inline serializableARSessionConfiguration_t3208486129 * get_config_0() const { return ___config_0; }
	inline serializableARSessionConfiguration_t3208486129 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(serializableARSessionConfiguration_t3208486129 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_runOption_1() { return static_cast<int32_t>(offsetof(serializableARKitInit_t403564667, ___runOption_1)); }
	inline int32_t get_runOption_1() const { return ___runOption_1; }
	inline int32_t* get_address_of_runOption_1() { return &___runOption_1; }
	inline void set_runOption_1(int32_t value)
	{
		___runOption_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARKITINIT_T403564667_H
#ifndef STREAMINGCONTEXT_T4252281328_H
#define STREAMINGCONTEXT_T4252281328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t4252281328 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t4252281328, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t4252281328, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4252281328_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4252281328_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T4252281328_H
#ifndef MULTICASTDELEGATE_T1280656641_H
#define MULTICASTDELEGATE_T1280656641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1280656641  : public Delegate_t1563516729
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1280656641 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1280656641 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___prev_9)); }
	inline MulticastDelegate_t1280656641 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1280656641 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1280656641 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___kpm_next_10)); }
	inline MulticastDelegate_t1280656641 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1280656641 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1280656641 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1280656641_H
#ifndef UNITYARFACEANCHORDATA_T2557914456_H
#define UNITYARFACEANCHORDATA_T2557914456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceAnchorData
struct  UnityARFaceAnchorData_t2557914456 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARFaceAnchorData::transform
	UnityARMatrix4x4_t758723042  ___transform_1;
	// UnityEngine.XR.iOS.UnityARFaceGeometry UnityEngine.XR.iOS.UnityARFaceAnchorData::faceGeometry
	UnityARFaceGeometry_t458757803  ___faceGeometry_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::blendShapes
	intptr_t ___blendShapes_3;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2557914456, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2557914456, ___transform_1)); }
	inline UnityARMatrix4x4_t758723042  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t758723042  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_faceGeometry_2() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2557914456, ___faceGeometry_2)); }
	inline UnityARFaceGeometry_t458757803  get_faceGeometry_2() const { return ___faceGeometry_2; }
	inline UnityARFaceGeometry_t458757803 * get_address_of_faceGeometry_2() { return &___faceGeometry_2; }
	inline void set_faceGeometry_2(UnityARFaceGeometry_t458757803  value)
	{
		___faceGeometry_2 = value;
	}

	inline static int32_t get_offset_of_blendShapes_3() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2557914456, ___blendShapes_3)); }
	inline intptr_t get_blendShapes_3() const { return ___blendShapes_3; }
	inline intptr_t* get_address_of_blendShapes_3() { return &___blendShapes_3; }
	inline void set_blendShapes_3(intptr_t value)
	{
		___blendShapes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEANCHORDATA_T2557914456_H
#ifndef MATERIAL_T2712136762_H
#define MATERIAL_T2712136762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2712136762  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2712136762_H
#ifndef UNITYMARSHALLIGHTDATA_T989795789_H
#define UNITYMARSHALLIGHTDATA_T989795789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityMarshalLightData
struct  UnityMarshalLightData_t989795789 
{
public:
	// UnityEngine.XR.iOS.LightDataType UnityEngine.XR.iOS.UnityMarshalLightData::arLightingType
	int32_t ___arLightingType_0;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.UnityMarshalLightData::arLightEstimate
	UnityARLightEstimate_t2392533559  ___arLightEstimate_1;
	// UnityEngine.XR.iOS.MarshalDirectionalLightEstimate UnityEngine.XR.iOS.UnityMarshalLightData::arDirectonalLightEstimate
	MarshalDirectionalLightEstimate_t3103061175  ___arDirectonalLightEstimate_2;

public:
	inline static int32_t get_offset_of_arLightingType_0() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t989795789, ___arLightingType_0)); }
	inline int32_t get_arLightingType_0() const { return ___arLightingType_0; }
	inline int32_t* get_address_of_arLightingType_0() { return &___arLightingType_0; }
	inline void set_arLightingType_0(int32_t value)
	{
		___arLightingType_0 = value;
	}

	inline static int32_t get_offset_of_arLightEstimate_1() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t989795789, ___arLightEstimate_1)); }
	inline UnityARLightEstimate_t2392533559  get_arLightEstimate_1() const { return ___arLightEstimate_1; }
	inline UnityARLightEstimate_t2392533559 * get_address_of_arLightEstimate_1() { return &___arLightEstimate_1; }
	inline void set_arLightEstimate_1(UnityARLightEstimate_t2392533559  value)
	{
		___arLightEstimate_1 = value;
	}

	inline static int32_t get_offset_of_arDirectonalLightEstimate_2() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t989795789, ___arDirectonalLightEstimate_2)); }
	inline MarshalDirectionalLightEstimate_t3103061175  get_arDirectonalLightEstimate_2() const { return ___arDirectonalLightEstimate_2; }
	inline MarshalDirectionalLightEstimate_t3103061175 * get_address_of_arDirectonalLightEstimate_2() { return &___arDirectonalLightEstimate_2; }
	inline void set_arDirectonalLightEstimate_2(MarshalDirectionalLightEstimate_t3103061175  value)
	{
		___arDirectonalLightEstimate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYMARSHALLIGHTDATA_T989795789_H
#ifndef ARPLANEANCHOR_T2525223154_H
#define ARPLANEANCHOR_T2525223154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t2525223154 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t2375577114  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t329709361  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t329709361  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2525223154, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2525223154, ___transform_1)); }
	inline Matrix4x4_t2375577114  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t2375577114 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t2375577114  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2525223154, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2525223154, ___center_3)); }
	inline Vector3_t329709361  get_center_3() const { return ___center_3; }
	inline Vector3_t329709361 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t329709361  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2525223154, ___extent_4)); }
	inline Vector3_t329709361  get_extent_4() const { return ___extent_4; }
	inline Vector3_t329709361 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t329709361  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t2525223154_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t2375577114  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t329709361  ___center_3;
	Vector3_t329709361  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t2525223154_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t2375577114  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t329709361  ___center_3;
	Vector3_t329709361  ___extent_4;
};
#endif // ARPLANEANCHOR_T2525223154_H
#ifndef UNITYARLIGHTDATA_T3404656299_H
#define UNITYARLIGHTDATA_T3404656299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARLightData
struct  UnityARLightData_t3404656299 
{
public:
	// UnityEngine.XR.iOS.LightDataType UnityEngine.XR.iOS.UnityARLightData::arLightingType
	int32_t ___arLightingType_0;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.UnityARLightData::arLightEstimate
	UnityARLightEstimate_t2392533559  ___arLightEstimate_1;
	// UnityEngine.XR.iOS.UnityARDirectionalLightEstimate UnityEngine.XR.iOS.UnityARLightData::arDirectonalLightEstimate
	UnityARDirectionalLightEstimate_t3433403765 * ___arDirectonalLightEstimate_2;

public:
	inline static int32_t get_offset_of_arLightingType_0() { return static_cast<int32_t>(offsetof(UnityARLightData_t3404656299, ___arLightingType_0)); }
	inline int32_t get_arLightingType_0() const { return ___arLightingType_0; }
	inline int32_t* get_address_of_arLightingType_0() { return &___arLightingType_0; }
	inline void set_arLightingType_0(int32_t value)
	{
		___arLightingType_0 = value;
	}

	inline static int32_t get_offset_of_arLightEstimate_1() { return static_cast<int32_t>(offsetof(UnityARLightData_t3404656299, ___arLightEstimate_1)); }
	inline UnityARLightEstimate_t2392533559  get_arLightEstimate_1() const { return ___arLightEstimate_1; }
	inline UnityARLightEstimate_t2392533559 * get_address_of_arLightEstimate_1() { return &___arLightEstimate_1; }
	inline void set_arLightEstimate_1(UnityARLightEstimate_t2392533559  value)
	{
		___arLightEstimate_1 = value;
	}

	inline static int32_t get_offset_of_arDirectonalLightEstimate_2() { return static_cast<int32_t>(offsetof(UnityARLightData_t3404656299, ___arDirectonalLightEstimate_2)); }
	inline UnityARDirectionalLightEstimate_t3433403765 * get_arDirectonalLightEstimate_2() const { return ___arDirectonalLightEstimate_2; }
	inline UnityARDirectionalLightEstimate_t3433403765 ** get_address_of_arDirectonalLightEstimate_2() { return &___arDirectonalLightEstimate_2; }
	inline void set_arDirectonalLightEstimate_2(UnityARDirectionalLightEstimate_t3433403765 * value)
	{
		___arDirectonalLightEstimate_2 = value;
		Il2CppCodeGenWriteBarrier((&___arDirectonalLightEstimate_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARLightData
struct UnityARLightData_t3404656299_marshaled_pinvoke
{
	int32_t ___arLightingType_0;
	UnityARLightEstimate_t2392533559  ___arLightEstimate_1;
	UnityARDirectionalLightEstimate_t3433403765 * ___arDirectonalLightEstimate_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARLightData
struct UnityARLightData_t3404656299_marshaled_com
{
	int32_t ___arLightingType_0;
	UnityARLightEstimate_t2392533559  ___arLightEstimate_1;
	UnityARDirectionalLightEstimate_t3433403765 * ___arDirectonalLightEstimate_2;
};
#endif // UNITYARLIGHTDATA_T3404656299_H
#ifndef RENDERTARGETIDENTIFIER_T3642434912_H
#define RENDERTARGETIDENTIFIER_T3642434912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t3642434912 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T3642434912_H
#ifndef COMPONENT_T789413749_H
#define COMPONENT_T789413749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t789413749  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T789413749_H
#ifndef GAMEOBJECT_T1318052361_H
#define GAMEOBJECT_T1318052361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1318052361  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1318052361_H
#ifndef TEXTURE_T2838694469_H
#define TEXTURE_T2838694469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2838694469  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2838694469_H
#ifndef ARUSERANCHORREMOVED_T877041919_H
#define ARUSERANCHORREMOVED_T877041919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved
struct  ARUserAnchorRemoved_t877041919  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORREMOVED_T877041919_H
#ifndef BEHAVIOUR_T2441856611_H
#define BEHAVIOUR_T2441856611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2441856611  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2441856611_H
#ifndef INTERNAL_ARFACEANCHORADDED_T338421685_H
#define INTERNAL_ARFACEANCHORADDED_T338421685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded
struct  internal_ARFaceAnchorAdded_t338421685  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORADDED_T338421685_H
#ifndef ARUSERANCHORADDED_T389345868_H
#define ARUSERANCHORADDED_T389345868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded
struct  ARUserAnchorAdded_t389345868  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORADDED_T389345868_H
#ifndef INTERNAL_ARANCHORADDED_T1133215702_H
#define INTERNAL_ARANCHORADDED_T1133215702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t1133215702  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T1133215702_H
#ifndef ASYNCCALLBACK_T869574496_H
#define ASYNCCALLBACK_T869574496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t869574496  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T869574496_H
#ifndef INTERNAL_ARANCHORREMOVED_T1159356933_H
#define INTERNAL_ARANCHORREMOVED_T1159356933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t1159356933  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T1159356933_H
#ifndef TEXTURE2D_T878840578_H
#define TEXTURE2D_T878840578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t878840578  : public Texture_t2838694469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T878840578_H
#ifndef UNITYARCAMERA_T3270530332_H
#define UNITYARCAMERA_T3270530332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t3270530332 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t758723042  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t758723042  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.UnityARCamera::videoParams
	UnityVideoParams_t909694111  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityARLightData UnityEngine.XR.iOS.UnityARCamera::lightData
	UnityARLightData_t3404656299  ___lightData_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::displayTransform
	UnityARMatrix4x4_t758723042  ___displayTransform_6;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t974944492* ___pointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t758723042  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t758723042  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t758723042  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t758723042  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___videoParams_4)); }
	inline UnityVideoParams_t909694111  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t909694111 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t909694111  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___lightData_5)); }
	inline UnityARLightData_t3404656299  get_lightData_5() const { return ___lightData_5; }
	inline UnityARLightData_t3404656299 * get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(UnityARLightData_t3404656299  value)
	{
		___lightData_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___displayTransform_6)); }
	inline UnityARMatrix4x4_t758723042  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_t758723042  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_7() { return static_cast<int32_t>(offsetof(UnityARCamera_t3270530332, ___pointCloudData_7)); }
	inline Vector3U5BU5D_t974944492* get_pointCloudData_7() const { return ___pointCloudData_7; }
	inline Vector3U5BU5D_t974944492** get_address_of_pointCloudData_7() { return &___pointCloudData_7; }
	inline void set_pointCloudData_7(Vector3U5BU5D_t974944492* value)
	{
		___pointCloudData_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t3270530332_marshaled_pinvoke
{
	UnityARMatrix4x4_t758723042  ___worldTransform_0;
	UnityARMatrix4x4_t758723042  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	UnityVideoParams_t909694111  ___videoParams_4;
	UnityARLightData_t3404656299_marshaled_pinvoke ___lightData_5;
	UnityARMatrix4x4_t758723042  ___displayTransform_6;
	Vector3_t329709361 * ___pointCloudData_7;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t3270530332_marshaled_com
{
	UnityARMatrix4x4_t758723042  ___worldTransform_0;
	UnityARMatrix4x4_t758723042  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	UnityVideoParams_t909694111  ___videoParams_4;
	UnityARLightData_t3404656299_marshaled_com ___lightData_5;
	UnityARMatrix4x4_t758723042  ___displayTransform_6;
	Vector3_t329709361 * ___pointCloudData_7;
};
#endif // UNITYARCAMERA_T3270530332_H
#ifndef INTERNAL_ARANCHORUPDATED_T2228873274_H
#define INTERNAL_ARANCHORUPDATED_T2228873274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t2228873274  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T2228873274_H
#ifndef ARUSERANCHORUPDATED_T3351790518_H
#define ARUSERANCHORUPDATED_T3351790518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated
struct  ARUserAnchorUpdated_t3351790518  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORUPDATED_T3351790518_H
#ifndef INTERNAL_ARUSERANCHORADDED_T1053280061_H
#define INTERNAL_ARUSERANCHORADDED_T1053280061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded
struct  internal_ARUserAnchorAdded_t1053280061  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORADDED_T1053280061_H
#ifndef INTERNAL_ARFACEANCHORREMOVED_T1507865825_H
#define INTERNAL_ARFACEANCHORREMOVED_T1507865825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved
struct  internal_ARFaceAnchorRemoved_t1507865825  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORREMOVED_T1507865825_H
#ifndef INTERNAL_ARUSERANCHORUPDATED_T1262387502_H
#define INTERNAL_ARUSERANCHORUPDATED_T1262387502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated
struct  internal_ARUserAnchorUpdated_t1262387502  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORUPDATED_T1262387502_H
#ifndef INTERNAL_ARUSERANCHORREMOVED_T535060684_H
#define INTERNAL_ARUSERANCHORREMOVED_T535060684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved
struct  internal_ARUserAnchorRemoved_t535060684  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORREMOVED_T535060684_H
#ifndef MESHFILTER_T1334808991_H
#define MESHFILTER_T1334808991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t1334808991  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T1334808991_H
#ifndef TRANSFORM_T532597831_H
#define TRANSFORM_T532597831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t532597831  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T532597831_H
#ifndef BINARYFORMATTER_T3541706630_H
#define BINARYFORMATTER_T3541706630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct  BinaryFormatter_t3541706630  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::assembly_format
	int32_t ___assembly_format_0;
	// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::binder
	SerializationBinder_t2088810939 * ___binder_1;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::context
	StreamingContext_t4252281328  ___context_2;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::surrogate_selector
	RuntimeObject* ___surrogate_selector_3;
	// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::type_format
	int32_t ___type_format_4;
	// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::filter_level
	int32_t ___filter_level_5;

public:
	inline static int32_t get_offset_of_assembly_format_0() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___assembly_format_0)); }
	inline int32_t get_assembly_format_0() const { return ___assembly_format_0; }
	inline int32_t* get_address_of_assembly_format_0() { return &___assembly_format_0; }
	inline void set_assembly_format_0(int32_t value)
	{
		___assembly_format_0 = value;
	}

	inline static int32_t get_offset_of_binder_1() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___binder_1)); }
	inline SerializationBinder_t2088810939 * get_binder_1() const { return ___binder_1; }
	inline SerializationBinder_t2088810939 ** get_address_of_binder_1() { return &___binder_1; }
	inline void set_binder_1(SerializationBinder_t2088810939 * value)
	{
		___binder_1 = value;
		Il2CppCodeGenWriteBarrier((&___binder_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___context_2)); }
	inline StreamingContext_t4252281328  get_context_2() const { return ___context_2; }
	inline StreamingContext_t4252281328 * get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(StreamingContext_t4252281328  value)
	{
		___context_2 = value;
	}

	inline static int32_t get_offset_of_surrogate_selector_3() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___surrogate_selector_3)); }
	inline RuntimeObject* get_surrogate_selector_3() const { return ___surrogate_selector_3; }
	inline RuntimeObject** get_address_of_surrogate_selector_3() { return &___surrogate_selector_3; }
	inline void set_surrogate_selector_3(RuntimeObject* value)
	{
		___surrogate_selector_3 = value;
		Il2CppCodeGenWriteBarrier((&___surrogate_selector_3), value);
	}

	inline static int32_t get_offset_of_type_format_4() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___type_format_4)); }
	inline int32_t get_type_format_4() const { return ___type_format_4; }
	inline int32_t* get_address_of_type_format_4() { return &___type_format_4; }
	inline void set_type_format_4(int32_t value)
	{
		___type_format_4 = value;
	}

	inline static int32_t get_offset_of_filter_level_5() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630, ___filter_level_5)); }
	inline int32_t get_filter_level_5() const { return ___filter_level_5; }
	inline int32_t* get_address_of_filter_level_5() { return &___filter_level_5; }
	inline void set_filter_level_5(int32_t value)
	{
		___filter_level_5 = value;
	}
};

struct BinaryFormatter_t3541706630_StaticFields
{
public:
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::<DefaultSurrogateSelector>k__BackingField
	RuntimeObject* ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BinaryFormatter_t3541706630_StaticFields, ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() const { return ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6() { return &___U3CDefaultSurrogateSelectorU3Ek__BackingField_6; }
	inline void set_U3CDefaultSurrogateSelectorU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CDefaultSurrogateSelectorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultSurrogateSelectorU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFORMATTER_T3541706630_H
#ifndef INTERNAL_UNITYARCAMERA_T2639053733_H
#define INTERNAL_UNITYARCAMERA_T2639053733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t2639053733 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t758723042  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t758723042  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.internal_UnityARCamera::videoParams
	UnityVideoParams_t909694111  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityMarshalLightData UnityEngine.XR.iOS.internal_UnityARCamera::lightData
	UnityMarshalLightData_t989795789  ___lightData_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::displayTransform
	UnityARMatrix4x4_t758723042  ___displayTransform_6;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t758723042  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t758723042  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t758723042  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t758723042  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___videoParams_4)); }
	inline UnityVideoParams_t909694111  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t909694111 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t909694111  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___lightData_5)); }
	inline UnityMarshalLightData_t989795789  get_lightData_5() const { return ___lightData_5; }
	inline UnityMarshalLightData_t989795789 * get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(UnityMarshalLightData_t989795789  value)
	{
		___lightData_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___displayTransform_6)); }
	inline UnityARMatrix4x4_t758723042  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_t758723042 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_t758723042  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_7() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2639053733, ___getPointCloudData_7)); }
	inline uint32_t get_getPointCloudData_7() const { return ___getPointCloudData_7; }
	inline uint32_t* get_address_of_getPointCloudData_7() { return &___getPointCloudData_7; }
	inline void set_getPointCloudData_7(uint32_t value)
	{
		___getPointCloudData_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T2639053733_H
#ifndef INTERNAL_ARFACEANCHORUPDATED_T4219811705_H
#define INTERNAL_ARFACEANCHORUPDATED_T4219811705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated
struct  internal_ARFaceAnchorUpdated_t4219811705  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORUPDATED_T4219811705_H
#ifndef ARFRAMEUPDATE_T2274630099_H
#define ARFRAMEUPDATE_T2274630099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t2274630099  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T2274630099_H
#ifndef INTERNAL_ARSESSIONTRACKINGCHANGED_T2450502912_H
#define INTERNAL_ARSESSIONTRACKINGCHANGED_T2450502912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged
struct  internal_ARSessionTrackingChanged_t2450502912  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARSESSIONTRACKINGCHANGED_T2450502912_H
#ifndef INTERNAL_ARFRAMEUPDATE_T4139772791_H
#define INTERNAL_ARFRAMEUPDATE_T4139772791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t4139772791  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T4139772791_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T4258909960_H
#define UNITYARSESSIONNATIVEINTERFACE_T4258909960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t4258909960  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	intptr_t ___m_NativeARSession_14;

public:
	inline static int32_t get_offset_of_m_NativeARSession_14() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960, ___m_NativeARSession_14)); }
	inline intptr_t get_m_NativeARSession_14() const { return ___m_NativeARSession_14; }
	inline intptr_t* get_address_of_m_NativeARSession_14() { return &___m_NativeARSession_14; }
	inline void set_m_NativeARSession_14(intptr_t value)
	{
		___m_NativeARSession_14 = value;
	}
};

struct UnityARSessionNativeInterface_t4258909960_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t2274630099 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t1360026176 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_t2652221944 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t617657937 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorAddedEvent
	ARUserAnchorAdded_t389345868 * ___ARUserAnchorAddedEvent_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorUpdatedEvent
	ARUserAnchorUpdated_t3351790518 * ___ARUserAnchorUpdatedEvent_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorRemovedEvent
	ARUserAnchorRemoved_t877041919 * ___ARUserAnchorRemovedEvent_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFaceAnchorAddedEvent
	ARFaceAnchorAdded_t2853404520 * ___ARFaceAnchorAddedEvent_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFaceAnchorUpdatedEvent
	ARFaceAnchorUpdated_t2137861905 * ___ARFaceAnchorUpdatedEvent_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFaceAnchorRemovedEvent
	ARFaceAnchorRemoved_t656883358 * ___ARFaceAnchorRemovedEvent_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_t916421009 * ___ARSessionFailedEvent_10;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionInterruptedEvent
	ARSessionCallback_t1432673640 * ___ARSessionInterruptedEvent_11;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessioninterruptionEndedEvent
	ARSessionCallback_t1432673640 * ___ARSessioninterruptionEndedEvent_12;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionTrackingChangedEvent
	ARSessionTrackingChanged_t2903091015 * ___ARSessionTrackingChangedEvent_13;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t3270530332  ___s_Camera_15;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t4258909960 * ___s_UnityARSessionNativeInterface_16;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache0
	internal_ARFrameUpdate_t4139772791 * ___U3CU3Ef__mgU24cache0_17;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache1
	ARSessionFailed_t916421009 * ___U3CU3Ef__mgU24cache1_18;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache2
	ARSessionCallback_t1432673640 * ___U3CU3Ef__mgU24cache2_19;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache3
	ARSessionCallback_t1432673640 * ___U3CU3Ef__mgU24cache3_20;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache4
	internal_ARSessionTrackingChanged_t2450502912 * ___U3CU3Ef__mgU24cache4_21;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache5
	internal_ARAnchorAdded_t1133215702 * ___U3CU3Ef__mgU24cache5_22;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache6
	internal_ARAnchorUpdated_t2228873274 * ___U3CU3Ef__mgU24cache6_23;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache7
	internal_ARAnchorRemoved_t1159356933 * ___U3CU3Ef__mgU24cache7_24;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache8
	internal_ARUserAnchorAdded_t1053280061 * ___U3CU3Ef__mgU24cache8_25;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache9
	internal_ARUserAnchorUpdated_t1262387502 * ___U3CU3Ef__mgU24cache9_26;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cacheA
	internal_ARUserAnchorRemoved_t535060684 * ___U3CU3Ef__mgU24cacheA_27;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cacheB
	internal_ARFaceAnchorAdded_t338421685 * ___U3CU3Ef__mgU24cacheB_28;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cacheC
	internal_ARFaceAnchorUpdated_t4219811705 * ___U3CU3Ef__mgU24cacheC_29;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cacheD
	internal_ARFaceAnchorRemoved_t1507865825 * ___U3CU3Ef__mgU24cacheD_30;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t2274630099 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t2274630099 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t2274630099 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t1360026176 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t1360026176 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t1360026176 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_t2652221944 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_t2652221944 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_t2652221944 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t617657937 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t617657937 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t617657937 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorAddedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARUserAnchorAddedEvent_4)); }
	inline ARUserAnchorAdded_t389345868 * get_ARUserAnchorAddedEvent_4() const { return ___ARUserAnchorAddedEvent_4; }
	inline ARUserAnchorAdded_t389345868 ** get_address_of_ARUserAnchorAddedEvent_4() { return &___ARUserAnchorAddedEvent_4; }
	inline void set_ARUserAnchorAddedEvent_4(ARUserAnchorAdded_t389345868 * value)
	{
		___ARUserAnchorAddedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorAddedEvent_4), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorUpdatedEvent_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARUserAnchorUpdatedEvent_5)); }
	inline ARUserAnchorUpdated_t3351790518 * get_ARUserAnchorUpdatedEvent_5() const { return ___ARUserAnchorUpdatedEvent_5; }
	inline ARUserAnchorUpdated_t3351790518 ** get_address_of_ARUserAnchorUpdatedEvent_5() { return &___ARUserAnchorUpdatedEvent_5; }
	inline void set_ARUserAnchorUpdatedEvent_5(ARUserAnchorUpdated_t3351790518 * value)
	{
		___ARUserAnchorUpdatedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorUpdatedEvent_5), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorRemovedEvent_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARUserAnchorRemovedEvent_6)); }
	inline ARUserAnchorRemoved_t877041919 * get_ARUserAnchorRemovedEvent_6() const { return ___ARUserAnchorRemovedEvent_6; }
	inline ARUserAnchorRemoved_t877041919 ** get_address_of_ARUserAnchorRemovedEvent_6() { return &___ARUserAnchorRemovedEvent_6; }
	inline void set_ARUserAnchorRemovedEvent_6(ARUserAnchorRemoved_t877041919 * value)
	{
		___ARUserAnchorRemovedEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorRemovedEvent_6), value);
	}

	inline static int32_t get_offset_of_ARFaceAnchorAddedEvent_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARFaceAnchorAddedEvent_7)); }
	inline ARFaceAnchorAdded_t2853404520 * get_ARFaceAnchorAddedEvent_7() const { return ___ARFaceAnchorAddedEvent_7; }
	inline ARFaceAnchorAdded_t2853404520 ** get_address_of_ARFaceAnchorAddedEvent_7() { return &___ARFaceAnchorAddedEvent_7; }
	inline void set_ARFaceAnchorAddedEvent_7(ARFaceAnchorAdded_t2853404520 * value)
	{
		___ARFaceAnchorAddedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___ARFaceAnchorAddedEvent_7), value);
	}

	inline static int32_t get_offset_of_ARFaceAnchorUpdatedEvent_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARFaceAnchorUpdatedEvent_8)); }
	inline ARFaceAnchorUpdated_t2137861905 * get_ARFaceAnchorUpdatedEvent_8() const { return ___ARFaceAnchorUpdatedEvent_8; }
	inline ARFaceAnchorUpdated_t2137861905 ** get_address_of_ARFaceAnchorUpdatedEvent_8() { return &___ARFaceAnchorUpdatedEvent_8; }
	inline void set_ARFaceAnchorUpdatedEvent_8(ARFaceAnchorUpdated_t2137861905 * value)
	{
		___ARFaceAnchorUpdatedEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___ARFaceAnchorUpdatedEvent_8), value);
	}

	inline static int32_t get_offset_of_ARFaceAnchorRemovedEvent_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARFaceAnchorRemovedEvent_9)); }
	inline ARFaceAnchorRemoved_t656883358 * get_ARFaceAnchorRemovedEvent_9() const { return ___ARFaceAnchorRemovedEvent_9; }
	inline ARFaceAnchorRemoved_t656883358 ** get_address_of_ARFaceAnchorRemovedEvent_9() { return &___ARFaceAnchorRemovedEvent_9; }
	inline void set_ARFaceAnchorRemovedEvent_9(ARFaceAnchorRemoved_t656883358 * value)
	{
		___ARFaceAnchorRemovedEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___ARFaceAnchorRemovedEvent_9), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARSessionFailedEvent_10)); }
	inline ARSessionFailed_t916421009 * get_ARSessionFailedEvent_10() const { return ___ARSessionFailedEvent_10; }
	inline ARSessionFailed_t916421009 ** get_address_of_ARSessionFailedEvent_10() { return &___ARSessionFailedEvent_10; }
	inline void set_ARSessionFailedEvent_10(ARSessionFailed_t916421009 * value)
	{
		___ARSessionFailedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_10), value);
	}

	inline static int32_t get_offset_of_ARSessionInterruptedEvent_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARSessionInterruptedEvent_11)); }
	inline ARSessionCallback_t1432673640 * get_ARSessionInterruptedEvent_11() const { return ___ARSessionInterruptedEvent_11; }
	inline ARSessionCallback_t1432673640 ** get_address_of_ARSessionInterruptedEvent_11() { return &___ARSessionInterruptedEvent_11; }
	inline void set_ARSessionInterruptedEvent_11(ARSessionCallback_t1432673640 * value)
	{
		___ARSessionInterruptedEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionInterruptedEvent_11), value);
	}

	inline static int32_t get_offset_of_ARSessioninterruptionEndedEvent_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARSessioninterruptionEndedEvent_12)); }
	inline ARSessionCallback_t1432673640 * get_ARSessioninterruptionEndedEvent_12() const { return ___ARSessioninterruptionEndedEvent_12; }
	inline ARSessionCallback_t1432673640 ** get_address_of_ARSessioninterruptionEndedEvent_12() { return &___ARSessioninterruptionEndedEvent_12; }
	inline void set_ARSessioninterruptionEndedEvent_12(ARSessionCallback_t1432673640 * value)
	{
		___ARSessioninterruptionEndedEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessioninterruptionEndedEvent_12), value);
	}

	inline static int32_t get_offset_of_ARSessionTrackingChangedEvent_13() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___ARSessionTrackingChangedEvent_13)); }
	inline ARSessionTrackingChanged_t2903091015 * get_ARSessionTrackingChangedEvent_13() const { return ___ARSessionTrackingChangedEvent_13; }
	inline ARSessionTrackingChanged_t2903091015 ** get_address_of_ARSessionTrackingChangedEvent_13() { return &___ARSessionTrackingChangedEvent_13; }
	inline void set_ARSessionTrackingChangedEvent_13(ARSessionTrackingChanged_t2903091015 * value)
	{
		___ARSessionTrackingChangedEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionTrackingChangedEvent_13), value);
	}

	inline static int32_t get_offset_of_s_Camera_15() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___s_Camera_15)); }
	inline UnityARCamera_t3270530332  get_s_Camera_15() const { return ___s_Camera_15; }
	inline UnityARCamera_t3270530332 * get_address_of_s_Camera_15() { return &___s_Camera_15; }
	inline void set_s_Camera_15(UnityARCamera_t3270530332  value)
	{
		___s_Camera_15 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_16() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___s_UnityARSessionNativeInterface_16)); }
	inline UnityARSessionNativeInterface_t4258909960 * get_s_UnityARSessionNativeInterface_16() const { return ___s_UnityARSessionNativeInterface_16; }
	inline UnityARSessionNativeInterface_t4258909960 ** get_address_of_s_UnityARSessionNativeInterface_16() { return &___s_UnityARSessionNativeInterface_16; }
	inline void set_s_UnityARSessionNativeInterface_16(UnityARSessionNativeInterface_t4258909960 * value)
	{
		___s_UnityARSessionNativeInterface_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_17() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache0_17)); }
	inline internal_ARFrameUpdate_t4139772791 * get_U3CU3Ef__mgU24cache0_17() const { return ___U3CU3Ef__mgU24cache0_17; }
	inline internal_ARFrameUpdate_t4139772791 ** get_address_of_U3CU3Ef__mgU24cache0_17() { return &___U3CU3Ef__mgU24cache0_17; }
	inline void set_U3CU3Ef__mgU24cache0_17(internal_ARFrameUpdate_t4139772791 * value)
	{
		___U3CU3Ef__mgU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_18() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache1_18)); }
	inline ARSessionFailed_t916421009 * get_U3CU3Ef__mgU24cache1_18() const { return ___U3CU3Ef__mgU24cache1_18; }
	inline ARSessionFailed_t916421009 ** get_address_of_U3CU3Ef__mgU24cache1_18() { return &___U3CU3Ef__mgU24cache1_18; }
	inline void set_U3CU3Ef__mgU24cache1_18(ARSessionFailed_t916421009 * value)
	{
		___U3CU3Ef__mgU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_19() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache2_19)); }
	inline ARSessionCallback_t1432673640 * get_U3CU3Ef__mgU24cache2_19() const { return ___U3CU3Ef__mgU24cache2_19; }
	inline ARSessionCallback_t1432673640 ** get_address_of_U3CU3Ef__mgU24cache2_19() { return &___U3CU3Ef__mgU24cache2_19; }
	inline void set_U3CU3Ef__mgU24cache2_19(ARSessionCallback_t1432673640 * value)
	{
		___U3CU3Ef__mgU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_20() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache3_20)); }
	inline ARSessionCallback_t1432673640 * get_U3CU3Ef__mgU24cache3_20() const { return ___U3CU3Ef__mgU24cache3_20; }
	inline ARSessionCallback_t1432673640 ** get_address_of_U3CU3Ef__mgU24cache3_20() { return &___U3CU3Ef__mgU24cache3_20; }
	inline void set_U3CU3Ef__mgU24cache3_20(ARSessionCallback_t1432673640 * value)
	{
		___U3CU3Ef__mgU24cache3_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_21() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache4_21)); }
	inline internal_ARSessionTrackingChanged_t2450502912 * get_U3CU3Ef__mgU24cache4_21() const { return ___U3CU3Ef__mgU24cache4_21; }
	inline internal_ARSessionTrackingChanged_t2450502912 ** get_address_of_U3CU3Ef__mgU24cache4_21() { return &___U3CU3Ef__mgU24cache4_21; }
	inline void set_U3CU3Ef__mgU24cache4_21(internal_ARSessionTrackingChanged_t2450502912 * value)
	{
		___U3CU3Ef__mgU24cache4_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_22() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache5_22)); }
	inline internal_ARAnchorAdded_t1133215702 * get_U3CU3Ef__mgU24cache5_22() const { return ___U3CU3Ef__mgU24cache5_22; }
	inline internal_ARAnchorAdded_t1133215702 ** get_address_of_U3CU3Ef__mgU24cache5_22() { return &___U3CU3Ef__mgU24cache5_22; }
	inline void set_U3CU3Ef__mgU24cache5_22(internal_ARAnchorAdded_t1133215702 * value)
	{
		___U3CU3Ef__mgU24cache5_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_23() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache6_23)); }
	inline internal_ARAnchorUpdated_t2228873274 * get_U3CU3Ef__mgU24cache6_23() const { return ___U3CU3Ef__mgU24cache6_23; }
	inline internal_ARAnchorUpdated_t2228873274 ** get_address_of_U3CU3Ef__mgU24cache6_23() { return &___U3CU3Ef__mgU24cache6_23; }
	inline void set_U3CU3Ef__mgU24cache6_23(internal_ARAnchorUpdated_t2228873274 * value)
	{
		___U3CU3Ef__mgU24cache6_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_24() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache7_24)); }
	inline internal_ARAnchorRemoved_t1159356933 * get_U3CU3Ef__mgU24cache7_24() const { return ___U3CU3Ef__mgU24cache7_24; }
	inline internal_ARAnchorRemoved_t1159356933 ** get_address_of_U3CU3Ef__mgU24cache7_24() { return &___U3CU3Ef__mgU24cache7_24; }
	inline void set_U3CU3Ef__mgU24cache7_24(internal_ARAnchorRemoved_t1159356933 * value)
	{
		___U3CU3Ef__mgU24cache7_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_25() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache8_25)); }
	inline internal_ARUserAnchorAdded_t1053280061 * get_U3CU3Ef__mgU24cache8_25() const { return ___U3CU3Ef__mgU24cache8_25; }
	inline internal_ARUserAnchorAdded_t1053280061 ** get_address_of_U3CU3Ef__mgU24cache8_25() { return &___U3CU3Ef__mgU24cache8_25; }
	inline void set_U3CU3Ef__mgU24cache8_25(internal_ARUserAnchorAdded_t1053280061 * value)
	{
		___U3CU3Ef__mgU24cache8_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_26() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cache9_26)); }
	inline internal_ARUserAnchorUpdated_t1262387502 * get_U3CU3Ef__mgU24cache9_26() const { return ___U3CU3Ef__mgU24cache9_26; }
	inline internal_ARUserAnchorUpdated_t1262387502 ** get_address_of_U3CU3Ef__mgU24cache9_26() { return &___U3CU3Ef__mgU24cache9_26; }
	inline void set_U3CU3Ef__mgU24cache9_26(internal_ARUserAnchorUpdated_t1262387502 * value)
	{
		___U3CU3Ef__mgU24cache9_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_27() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cacheA_27)); }
	inline internal_ARUserAnchorRemoved_t535060684 * get_U3CU3Ef__mgU24cacheA_27() const { return ___U3CU3Ef__mgU24cacheA_27; }
	inline internal_ARUserAnchorRemoved_t535060684 ** get_address_of_U3CU3Ef__mgU24cacheA_27() { return &___U3CU3Ef__mgU24cacheA_27; }
	inline void set_U3CU3Ef__mgU24cacheA_27(internal_ARUserAnchorRemoved_t535060684 * value)
	{
		___U3CU3Ef__mgU24cacheA_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_28() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cacheB_28)); }
	inline internal_ARFaceAnchorAdded_t338421685 * get_U3CU3Ef__mgU24cacheB_28() const { return ___U3CU3Ef__mgU24cacheB_28; }
	inline internal_ARFaceAnchorAdded_t338421685 ** get_address_of_U3CU3Ef__mgU24cacheB_28() { return &___U3CU3Ef__mgU24cacheB_28; }
	inline void set_U3CU3Ef__mgU24cacheB_28(internal_ARFaceAnchorAdded_t338421685 * value)
	{
		___U3CU3Ef__mgU24cacheB_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_29() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cacheC_29)); }
	inline internal_ARFaceAnchorUpdated_t4219811705 * get_U3CU3Ef__mgU24cacheC_29() const { return ___U3CU3Ef__mgU24cacheC_29; }
	inline internal_ARFaceAnchorUpdated_t4219811705 ** get_address_of_U3CU3Ef__mgU24cacheC_29() { return &___U3CU3Ef__mgU24cacheC_29; }
	inline void set_U3CU3Ef__mgU24cacheC_29(internal_ARFaceAnchorUpdated_t4219811705 * value)
	{
		___U3CU3Ef__mgU24cacheC_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_30() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t4258909960_StaticFields, ___U3CU3Ef__mgU24cacheD_30)); }
	inline internal_ARFaceAnchorRemoved_t1507865825 * get_U3CU3Ef__mgU24cacheD_30() const { return ___U3CU3Ef__mgU24cacheD_30; }
	inline internal_ARFaceAnchorRemoved_t1507865825 ** get_address_of_U3CU3Ef__mgU24cacheD_30() { return &___U3CU3Ef__mgU24cacheD_30; }
	inline void set_U3CU3Ef__mgU24cacheD_30(internal_ARFaceAnchorRemoved_t1507865825 * value)
	{
		___U3CU3Ef__mgU24cacheD_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T4258909960_H
#ifndef CAMERA_T989002943_H
#define CAMERA_T989002943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t989002943  : public Behaviour_t2441856611
{
public:

public:
};

struct Camera_t989002943_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t2620733104 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t2620733104 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t2620733104 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t2620733104 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t2620733104 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t2620733104 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t2620733104 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t2620733104 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t2620733104 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t2620733104 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T989002943_H
#ifndef ARSESSIONTRACKINGCHANGED_T2903091015_H
#define ARSESSIONTRACKINGCHANGED_T2903091015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged
struct  ARSessionTrackingChanged_t2903091015  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONTRACKINGCHANGED_T2903091015_H
#ifndef MONOBEHAVIOUR_T3829899482_H
#define MONOBEHAVIOUR_T3829899482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3829899482  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3829899482_H
#ifndef UNITYREMOTEVIDEO_T3391959971_H
#define UNITYREMOTEVIDEO_T3391959971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityRemoteVideo
struct  UnityRemoteVideo_t3391959971  : public MonoBehaviour_t3829899482
{
public:
	// UnityEngine.XR.iOS.ConnectToEditor UnityEngine.XR.iOS.UnityRemoteVideo::connectToEditor
	ConnectToEditor_t1050945949 * ___connectToEditor_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityRemoteVideo::m_Session
	UnityARSessionNativeInterface_t4258909960 * ___m_Session_3;
	// System.Boolean UnityEngine.XR.iOS.UnityRemoteVideo::bTexturesInitialized
	bool ___bTexturesInitialized_4;
	// System.Int32 UnityEngine.XR.iOS.UnityRemoteVideo::currentFrameIndex
	int32_t ___currentFrameIndex_5;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes
	ByteU5BU5D_t3287329517* ___m_textureYBytes_6;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes
	ByteU5BU5D_t3287329517* ___m_textureUVBytes_7;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes2
	ByteU5BU5D_t3287329517* ___m_textureYBytes2_8;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes2
	ByteU5BU5D_t3287329517* ___m_textureUVBytes2_9;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedYArray
	GCHandle_t714486722  ___m_pinnedYArray_10;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedUVArray
	GCHandle_t714486722  ___m_pinnedUVArray_11;

public:
	inline static int32_t get_offset_of_connectToEditor_2() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___connectToEditor_2)); }
	inline ConnectToEditor_t1050945949 * get_connectToEditor_2() const { return ___connectToEditor_2; }
	inline ConnectToEditor_t1050945949 ** get_address_of_connectToEditor_2() { return &___connectToEditor_2; }
	inline void set_connectToEditor_2(ConnectToEditor_t1050945949 * value)
	{
		___connectToEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectToEditor_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t4258909960 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t4258909960 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t4258909960 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_4() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___bTexturesInitialized_4)); }
	inline bool get_bTexturesInitialized_4() const { return ___bTexturesInitialized_4; }
	inline bool* get_address_of_bTexturesInitialized_4() { return &___bTexturesInitialized_4; }
	inline void set_bTexturesInitialized_4(bool value)
	{
		___bTexturesInitialized_4 = value;
	}

	inline static int32_t get_offset_of_currentFrameIndex_5() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___currentFrameIndex_5)); }
	inline int32_t get_currentFrameIndex_5() const { return ___currentFrameIndex_5; }
	inline int32_t* get_address_of_currentFrameIndex_5() { return &___currentFrameIndex_5; }
	inline void set_currentFrameIndex_5(int32_t value)
	{
		___currentFrameIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_textureYBytes_6() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_textureYBytes_6)); }
	inline ByteU5BU5D_t3287329517* get_m_textureYBytes_6() const { return ___m_textureYBytes_6; }
	inline ByteU5BU5D_t3287329517** get_address_of_m_textureYBytes_6() { return &___m_textureYBytes_6; }
	inline void set_m_textureYBytes_6(ByteU5BU5D_t3287329517* value)
	{
		___m_textureYBytes_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes_6), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes_7() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_textureUVBytes_7)); }
	inline ByteU5BU5D_t3287329517* get_m_textureUVBytes_7() const { return ___m_textureUVBytes_7; }
	inline ByteU5BU5D_t3287329517** get_address_of_m_textureUVBytes_7() { return &___m_textureUVBytes_7; }
	inline void set_m_textureUVBytes_7(ByteU5BU5D_t3287329517* value)
	{
		___m_textureUVBytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes_7), value);
	}

	inline static int32_t get_offset_of_m_textureYBytes2_8() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_textureYBytes2_8)); }
	inline ByteU5BU5D_t3287329517* get_m_textureYBytes2_8() const { return ___m_textureYBytes2_8; }
	inline ByteU5BU5D_t3287329517** get_address_of_m_textureYBytes2_8() { return &___m_textureYBytes2_8; }
	inline void set_m_textureYBytes2_8(ByteU5BU5D_t3287329517* value)
	{
		___m_textureYBytes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes2_8), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes2_9() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_textureUVBytes2_9)); }
	inline ByteU5BU5D_t3287329517* get_m_textureUVBytes2_9() const { return ___m_textureUVBytes2_9; }
	inline ByteU5BU5D_t3287329517** get_address_of_m_textureUVBytes2_9() { return &___m_textureUVBytes2_9; }
	inline void set_m_textureUVBytes2_9(ByteU5BU5D_t3287329517* value)
	{
		___m_textureUVBytes2_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes2_9), value);
	}

	inline static int32_t get_offset_of_m_pinnedYArray_10() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_pinnedYArray_10)); }
	inline GCHandle_t714486722  get_m_pinnedYArray_10() const { return ___m_pinnedYArray_10; }
	inline GCHandle_t714486722 * get_address_of_m_pinnedYArray_10() { return &___m_pinnedYArray_10; }
	inline void set_m_pinnedYArray_10(GCHandle_t714486722  value)
	{
		___m_pinnedYArray_10 = value;
	}

	inline static int32_t get_offset_of_m_pinnedUVArray_11() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t3391959971, ___m_pinnedUVArray_11)); }
	inline GCHandle_t714486722  get_m_pinnedUVArray_11() const { return ___m_pinnedUVArray_11; }
	inline GCHandle_t714486722 * get_address_of_m_pinnedUVArray_11() { return &___m_pinnedUVArray_11; }
	inline void set_m_pinnedUVArray_11(GCHandle_t714486722  value)
	{
		___m_pinnedUVArray_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREMOTEVIDEO_T3391959971_H
#ifndef UNITYARUSERANCHORCOMPONENT_T2848590843_H
#define UNITYARUSERANCHORCOMPONENT_T2848590843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorComponent
struct  UnityARUserAnchorComponent_t2848590843  : public MonoBehaviour_t3829899482
{
public:
	// System.String UnityEngine.XR.iOS.UnityARUserAnchorComponent::m_AnchorId
	String_t* ___m_AnchorId_2;

public:
	inline static int32_t get_offset_of_m_AnchorId_2() { return static_cast<int32_t>(offsetof(UnityARUserAnchorComponent_t2848590843, ___m_AnchorId_2)); }
	inline String_t* get_m_AnchorId_2() const { return ___m_AnchorId_2; }
	inline String_t** get_address_of_m_AnchorId_2() { return &___m_AnchorId_2; }
	inline void set_m_AnchorId_2(String_t* value)
	{
		___m_AnchorId_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnchorId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORCOMPONENT_T2848590843_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T859887967_H
#define UNITYPOINTCLOUDEXAMPLE_T859887967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t859887967  : public MonoBehaviour_t3829899482
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t1318052361 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t1128502775 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t974944492* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t859887967, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t859887967, ___PointCloudPrefab_3)); }
	inline GameObject_t1318052361 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t1318052361 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t1318052361 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t859887967, ___pointCloudObjects_4)); }
	inline List_1_t1128502775 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t1128502775 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t1128502775 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t859887967, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t974944492* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t974944492** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t974944492* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T859887967_H
#ifndef CONNECTTOEDITOR_T1050945949_H
#define CONNECTTOEDITOR_T1050945949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectToEditor
struct  ConnectToEditor_t1050945949  : public MonoBehaviour_t3829899482
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.ConnectToEditor::playerConnection
	PlayerConnection_t781534341 * ___playerConnection_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.ConnectToEditor::m_session
	UnityARSessionNativeInterface_t4258909960 * ___m_session_3;
	// System.Int32 UnityEngine.XR.iOS.ConnectToEditor::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.ConnectToEditor::frameBufferTex
	Texture2D_t878840578 * ___frameBufferTex_5;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(ConnectToEditor_t1050945949, ___playerConnection_2)); }
	inline PlayerConnection_t781534341 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t781534341 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t781534341 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(ConnectToEditor_t1050945949, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t4258909960 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t4258909960 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t4258909960 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(ConnectToEditor_t1050945949, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_frameBufferTex_5() { return static_cast<int32_t>(offsetof(ConnectToEditor_t1050945949, ___frameBufferTex_5)); }
	inline Texture2D_t878840578 * get_frameBufferTex_5() const { return ___frameBufferTex_5; }
	inline Texture2D_t878840578 ** get_address_of_frameBufferTex_5() { return &___frameBufferTex_5; }
	inline void set_frameBufferTex_5(Texture2D_t878840578 * value)
	{
		___frameBufferTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameBufferTex_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOEDITOR_T1050945949_H
#ifndef UNITYARVIDEO_T2486736449_H
#define UNITYARVIDEO_T2486736449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2486736449  : public MonoBehaviour_t3829899482
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t2712136762 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t2673047760 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t878840578 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t878840578 * ____videoTextureCbCr_5;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARVideo::_displayTransform
	Matrix4x4_t2375577114  ____displayTransform_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ___m_ClearMaterial_2)); }
	inline Material_t2712136762 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t2712136762 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t2712136762 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t2673047760 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t2673047760 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t2673047760 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ____videoTextureY_4)); }
	inline Texture2D_t878840578 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t878840578 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t878840578 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ____videoTextureCbCr_5)); }
	inline Texture2D_t878840578 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t878840578 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t878840578 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of__displayTransform_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ____displayTransform_6)); }
	inline Matrix4x4_t2375577114  get__displayTransform_6() const { return ____displayTransform_6; }
	inline Matrix4x4_t2375577114 * get_address_of__displayTransform_6() { return &____displayTransform_6; }
	inline void set__displayTransform_6(Matrix4x4_t2375577114  value)
	{
		____displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2486736449, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2486736449_H
// System.Single[]
struct SingleU5BU5D_t2905636975  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3287329517  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t329709361  m_Items[1];

public:
	inline Vector3_t329709361  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t329709361  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t1568665923  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke(const ARUserAnchor_t2452746139& unmarshaled, ARUserAnchor_t2452746139_marshaled_pinvoke& marshaled);
extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke_back(const ARUserAnchor_t2452746139_marshaled_pinvoke& marshaled, ARUserAnchor_t2452746139& unmarshaled);
extern "C" void ARUserAnchor_t2452746139_marshal_pinvoke_cleanup(ARUserAnchor_t2452746139_marshaled_pinvoke& marshaled);

// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3149908048_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m799948634_gshared (GameObject_t1318052361 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2674875975_gshared (Component_t789413749 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2233273281_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4064363414_gshared (List_1_t185548372 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2669777438_gshared (List_1_t185548372 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARSessionTrackingChanged_Invoke_m3010169940 (ARSessionTrackingChanged_t2903091015 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorAdded_Invoke_m1327168017 (ARUserAnchorAdded_t389345868 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorRemoved_Invoke_m121198821 (ARUserAnchorRemoved_t877041919 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorUpdated_Invoke_m129708817 (ARUserAnchorUpdated_t3351790518 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m656113583 (internal_ARAnchorAdded_t1133215702 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m3601691325 (internal_ARAnchorRemoved_t1159356933 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m1269740464 (internal_ARAnchorUpdated_t2228873274 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorAdded_Invoke_m1719007874 (internal_ARFaceAnchorAdded_t338421685 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorRemoved_Invoke_m1431848752 (internal_ARFaceAnchorRemoved_t1507865825 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorUpdated_Invoke_m1171236065 (internal_ARFaceAnchorUpdated_t4219811705 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m869157158 (internal_ARFrameUpdate_t4139772791 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARSessionTrackingChanged_Invoke_m3556349571 (internal_ARSessionTrackingChanged_t2450502912 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorAdded_Invoke_m4248432723 (internal_ARUserAnchorAdded_t1053280061 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorRemoved_Invoke_m2022671894 (internal_ARUserAnchorRemoved_t535060684 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorUpdated_Invoke_m2747335307 (internal_ARUserAnchorUpdated_t1262387502 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2458272640 (MonoBehaviour_t3829899482 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARUserAnchorUpdated__ctor_m2339262563 (ARUserAnchorUpdated_t3351790518 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARUserAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_add_ARUserAnchorUpdatedEvent_m1405629722 (RuntimeObject * __this /* static, unused */, ARUserAnchorUpdated_t3351790518 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARUserAnchorRemoved__ctor_m4091998379 (ARUserAnchorRemoved_t877041919 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARUserAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_add_ARUserAnchorRemovedEvent_m1242199825 (RuntimeObject * __this /* static, unused */, ARUserAnchorRemoved_t877041919 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t4258909960 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m3297568107 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1318052361 * Component_get_gameObject_m2399756717 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARUserAnchorData UnityEngine.XR.iOS.UnityARSessionNativeInterface::AddUserAnchorFromGameObject(UnityEngine.GameObject)
extern "C"  UnityARUserAnchorData_t1928721163  UnityARSessionNativeInterface_AddUserAnchorFromGameObject_m3599754494 (UnityARSessionNativeInterface_t4258909960 * __this, GameObject_t1318052361 * ___go0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.XR.iOS.UnityARUserAnchorData::get_identifierStr()
extern "C"  String_t* UnityARUserAnchorData_get_identifierStr_m3595119354 (UnityARUserAnchorData_t1928721163 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2427690684 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m935298371 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARUserAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_remove_ARUserAnchorUpdatedEvent_m4204906472 (RuntimeObject * __this /* static, unused */, ARUserAnchorUpdated_t3351790518 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARUserAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_remove_ARUserAnchorRemovedEvent_m2646181465 (RuntimeObject * __this /* static, unused */, ARUserAnchorRemoved_t877041919 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RemoveUserAnchor(System.String)
extern "C"  void UnityARSessionNativeInterface_RemoveUserAnchor_m238817152 (UnityARSessionNativeInterface_t4258909960 * __this, String_t* ___anchorIdentifier0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr)
extern "C"  String_t* Marshal_PtrToStringAuto_m945446500 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t532597831 * GameObject_get_transform_m1187966899 (GameObject_t1318052361 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t329709361  Transform_get_position_m609417876 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2761156409  Transform_get_rotation_m1089223433 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t329709361  Transform_get_localScale_m1865232606 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2375577114  Matrix4x4_TRS_m2667050262 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  p0, Quaternion_t2761156409  p1, Vector3_t329709361  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t380635127  Matrix4x4_GetColumn_m1167652957 (Matrix4x4_t2375577114 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2095069727 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4170278078 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * p0, Object_t1970767703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1318052361_m3083066963(__this /* static, unused */, p0, method) ((  GameObject_t1318052361 * (*) (RuntimeObject * /* static, unused */, GameObject_t1318052361 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3149908048_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m2351111389 (GameObject_t1318052361 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m2629735859 (Object_t1970767703 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1318052361 * UnityARUtility_UpdatePlaneWithAnchorTransform_m796920091 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___plane0, ARPlaneAnchor_t2525223154  ___arPlaneAnchor1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t329709361  UnityARMatrixOps_GetPosition_m2593915132 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2375577114  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m1768136472 (Transform_t532597831 * __this, Vector3_t329709361  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t2761156409  UnityARMatrixOps_GetRotation_m759098689 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2375577114  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m4058359888 (Transform_t532597831 * __this, Quaternion_t2761156409  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.MeshFilter>()
#define GameObject_GetComponentInChildren_TisMeshFilter_t1334808991_m2726185362(__this, method) ((  MeshFilter_t1334808991 * (*) (GameObject_t1318052361 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m799948634_gshared)(__this, method)
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3984700005 (Vector3_t329709361 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3356822685 (Transform_t532597831 * __this, Vector3_t329709361  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3967901824 (Transform_t532597831 * __this, Vector3_t329709361  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m1552988806 (ARFrameUpdate_t2274630099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1052241241 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t2274630099 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m3548282205 (Matrix4x4_t2375577114 * __this, int32_t p0, Vector4_t380635127  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m125783349 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t3642434912  RenderTargetIdentifier_op_Implicit_m2181182592 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m3942920201 (CommandBuffer_t2673047760 * __this, Texture_t2838694469 * p0, RenderTargetIdentifier_t3642434912  p1, Material_t2712136762 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t989002943_m3380036349(__this, method) ((  Camera_t989002943 * (*) (Component_t789413749 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2674875975_gshared)(__this, method)
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m3104925877 (Camera_t989002943 * __this, int32_t p0, CommandBuffer_t2673047760 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m1713736922 (Camera_t989002943 * __this, int32_t p0, CommandBuffer_t2673047760 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m734046990 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t2274630099 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARVideoTextureHandles()
extern "C"  ARTextureHandles_t1453164273  UnityARSessionNativeInterface_GetARVideoTextureHandles_m1818998859 (UnityARSessionNativeInterface_t4258909960 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3008674881 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m1137915321 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t1041838152  Screen_get_currentResolution_m205929278 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1910042615 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * p0, Object_t1970767703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m2175655275 (Resolution_t1041838152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m3498799763 (Resolution_t1041838152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  Texture2D_t878840578 * Texture2D_CreateExternalTexture_m4069785793 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, bool p3, bool p4, intptr_t p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m1565425947 (Texture_t2838694469 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m2103720371 (Texture_t2838694469 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m825494064 (Material_t2712136762 * __this, String_t* p0, Texture_t2838694469 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern "C"  void Texture2D_UpdateExternalTexture_m4242697313 (Texture2D_t878840578 * __this, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m2294933890 (Material_t2712136762 * __this, String_t* p0, Matrix4x4_t2375577114  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityMarshalLightData::.ctor(UnityEngine.XR.iOS.LightDataType,UnityEngine.XR.iOS.UnityARLightEstimate,UnityEngine.XR.iOS.MarshalDirectionalLightEstimate)
extern "C"  void UnityMarshalLightData__ctor_m208288196 (UnityMarshalLightData_t989795789 * __this, int32_t ___ldt0, UnityARLightEstimate_t2392533559  ___ule1, MarshalDirectionalLightEstimate_t3103061175  ___mdle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine.XR.iOS.MarshalDirectionalLightEstimate::get_SphericalHarmonicCoefficients()
extern "C"  SingleU5BU5D_t2905636975* MarshalDirectionalLightEstimate_get_SphericalHarmonicCoefficients_m3181496191 (MarshalDirectionalLightEstimate_t3103061175 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARDirectionalLightEstimate::.ctor(System.Single[],UnityEngine.Vector3,System.Single)
extern "C"  void UnityARDirectionalLightEstimate__ctor_m3195205346 (UnityARDirectionalLightEstimate_t3433403765 * __this, SingleU5BU5D_t2905636975* ___SHC0, Vector3_t329709361  ___direction1, float ___intensity2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARLightData::.ctor(UnityEngine.XR.iOS.LightDataType,UnityEngine.XR.iOS.UnityARLightEstimate,UnityEngine.XR.iOS.UnityARDirectionalLightEstimate)
extern "C"  void UnityARLightData__ctor_m2108807113 (UnityARLightData_t3404656299 * __this, int32_t ___ldt0, UnityARLightEstimate_t2392533559  ___ule1, UnityARDirectionalLightEstimate_t3433403765 * ___udle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::InitializeTextures(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityRemoteVideo_InitializeTextures_m1297643449 (UnityRemoteVideo_t3391959971 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object)
extern "C"  GCHandle_t714486722  GCHandle_Alloc_m3660244495 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C"  void GCHandle_Free_m2887282835 (GCHandle_t714486722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C"  GCHandle_t714486722  GCHandle_Alloc_m4236757112 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.GCHandle::AddrOfPinnedObject()
extern "C"  intptr_t GCHandle_AddrOfPinnedObject_m560236266 (GCHandle_t714486722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::ByteArrayForFrame(System.Int32,System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_ByteArrayForFrame_m589747338 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, ByteU5BU5D_t3287329517* ___array01, ByteU5BU5D_t3287329517* ___array12, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCapturePixelData(System.Boolean,System.IntPtr,System.IntPtr)
extern "C"  void UnityARSessionNativeInterface_SetCapturePixelData_m2227686583 (UnityARSessionNativeInterface_t4258909960 * __this, bool ___enable0, intptr_t ___pYByteArray1, intptr_t ___pUVByteArray2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::YByteArrayForFrame(System.Int32)
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_YByteArrayForFrame_m189319441 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.XR.iOS.UnityRemoteVideo::PinByteArray(System.Runtime.InteropServices.GCHandle&,System.Byte[])
extern "C"  intptr_t UnityRemoteVideo_PinByteArray_m2804681258 (UnityRemoteVideo_t3391959971 * __this, GCHandle_t714486722 * ___handle0, ByteU5BU5D_t3287329517* ___array1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::UVByteArrayForFrame(System.Int32)
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_UVByteArrayForFrame_m955222255 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.ConnectionMessageIds::get_screenCaptureYMsgId()
extern "C"  Guid_t  ConnectionMessageIds_get_screenCaptureYMsgId_m3305274427 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ConnectToEditor::SendToEditor(System.Guid,System.Byte[])
extern "C"  void ConnectToEditor_SendToEditor_m1297661023 (ConnectToEditor_t1050945949 * __this, Guid_t  ___msgId0, ByteU5BU5D_t3287329517* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Guid UnityEngine.XR.iOS.ConnectionMessageIds::get_screenCaptureUVMsgId()
extern "C"  Guid_t  ConnectionMessageIds_get_screenCaptureUVMsgId_m3914639669 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m1148151875(__this, method) ((  void (*) (List_1_t1128502775 *, const RuntimeMethod*))List_1__ctor_m2233273281_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m695416883(__this, p0, method) ((  void (*) (List_1_t1128502775 *, GameObject_t1318052361 *, const RuntimeMethod*))List_1_Add_m4064363414_gshared)(__this, p0, method)
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t380635127  Vector4_op_Implicit_m3043984250 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m4071041560(__this, p0, method) ((  GameObject_t1318052361 * (*) (List_1_t1128502775 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2669777438_gshared)(__this, p0, method)
// System.Int64 System.Math::Min(System.Int64,System.Int64)
extern "C"  int64_t Math_Min_m3765675025 (RuntimeObject * __this /* static, unused */, int64_t p0, int64_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C"  void BinaryFormatter__ctor_m2525162057 (BinaryFormatter_t3541706630 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.MemoryStream::.ctor()
extern "C"  void MemoryStream__ctor_m298230343 (MemoryStream_t3384139290 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern "C"  void BinaryFormatter_Serialize_m1779404934 (BinaryFormatter_t3541706630 * __this, Stream_t3046340190 * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableARSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void serializableARSessionConfiguration__ctor_m2691530054 (serializableARSessionConfiguration_t3208486129 * __this, int32_t ___align0, int32_t ___planeDet1, bool ___getPtCloud2, bool ___enableLightEst3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void ARKitWorldTrackingSessionConfiguration__ctor_m4004678005 (ARKitWorldTrackingSessionConfiguration_t4171655616 * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Single)
extern "C"  ByteU5BU5D_t3287329517* BitConverter_GetBytes_m2229711275 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Buffer_BlockCopy_m2996674574 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializablePointCloud::.ctor(System.Byte[])
extern "C"  void serializablePointCloud__ctor_m2980893286 (serializablePointCloud_t2991476747 * __this, ByteU5BU5D_t3287329517* ___inputPoints0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single System.BitConverter::ToSingle(System.Byte[],System.Int32)
extern "C"  float BitConverter_ToSingle_m3546845596 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3287329517* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableSHC::.ctor(System.Byte[])
extern "C"  void serializableSHC__ctor_m3060006280 (serializableSHC_t2973324561 * __this, ByteU5BU5D_t3287329517* ___inputSHCData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableUnityARLightData Utils.serializableUnityARLightData::op_Implicit(UnityEngine.XR.iOS.UnityARLightData)
extern "C"  serializableUnityARLightData_t1864664250 * serializableUnityARLightData_op_Implicit_m1903111810 (RuntimeObject * __this /* static, unused */, UnityARLightData_t3404656299  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(UnityEngine.XR.iOS.UnityARMatrix4x4)
extern "C"  serializableUnityARMatrix4x4_t3548294260 * serializableUnityARMatrix4x4_op_Implicit_m1426572080 (RuntimeObject * __this /* static, unused */, UnityARMatrix4x4_t758723042  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializablePointCloud Utils.serializablePointCloud::op_Implicit(UnityEngine.Vector3[])
extern "C"  serializablePointCloud_t2991476747 * serializablePointCloud_op_Implicit_m4280869743 (RuntimeObject * __this /* static, unused */, Vector3U5BU5D_t974944492* ___vecPointCloud0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableUnityARCamera::.ctor(Utils.serializableUnityARMatrix4x4,Utils.serializableUnityARMatrix4x4,UnityEngine.XR.iOS.ARTrackingState,UnityEngine.XR.iOS.ARTrackingStateReason,UnityEngine.XR.iOS.UnityVideoParams,UnityEngine.XR.iOS.UnityARLightData,Utils.serializableUnityARMatrix4x4,Utils.serializablePointCloud)
extern "C"  void serializableUnityARCamera__ctor_m1894601043 (serializableUnityARCamera_t3181320657 * __this, serializableUnityARMatrix4x4_t3548294260 * ___wt0, serializableUnityARMatrix4x4_t3548294260 * ___pm1, int32_t ___ats2, int32_t ___atsr3, UnityVideoParams_t909694111  ___uvp4, UnityARLightData_t3404656299  ___lightDat5, serializableUnityARMatrix4x4_t3548294260 * ___dt6, serializablePointCloud_t2991476747 * ___spc7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(Utils.serializableUnityARMatrix4x4)
extern "C"  UnityARMatrix4x4_t758723042  serializableUnityARMatrix4x4_op_Implicit_m2448964739 (RuntimeObject * __this /* static, unused */, serializableUnityARMatrix4x4_t3548294260 * ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARLightData Utils.serializableUnityARLightData::op_Implicit(Utils.serializableUnityARLightData)
extern "C"  UnityARLightData_t3404656299  serializableUnityARLightData_op_Implicit_m322329394 (RuntimeObject * __this /* static, unused */, serializableUnityARLightData_t1864664250 * ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Utils.serializablePointCloud::op_Implicit(Utils.serializablePointCloud)
extern "C"  Vector3U5BU5D_t974944492* serializablePointCloud_op_Implicit_m3149335011 (RuntimeObject * __this /* static, unused */, serializablePointCloud_t2991476747 * ___spc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARCamera::.ctor(UnityEngine.XR.iOS.UnityARMatrix4x4,UnityEngine.XR.iOS.UnityARMatrix4x4,UnityEngine.XR.iOS.ARTrackingState,UnityEngine.XR.iOS.ARTrackingStateReason,UnityEngine.XR.iOS.UnityVideoParams,UnityEngine.XR.iOS.UnityARLightData,UnityEngine.XR.iOS.UnityARMatrix4x4,UnityEngine.Vector3[])
extern "C"  void UnityARCamera__ctor_m1980968502 (UnityARCamera_t3270530332 * __this, UnityARMatrix4x4_t758723042  ___wt0, UnityARMatrix4x4_t758723042  ___pm1, int32_t ___ats2, int32_t ___atsr3, UnityVideoParams_t909694111  ___uvp4, UnityARLightData_t3404656299  ___lightDat5, UnityARMatrix4x4_t758723042  ___dt6, Vector3U5BU5D_t974944492* ___pointCloud7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableSHC Utils.serializableSHC::op_Implicit(System.Single[])
extern "C"  serializableSHC_t2973324561 * serializableSHC_op_Implicit_m2702198786 (RuntimeObject * __this /* static, unused */, SingleU5BU5D_t2905636975* ___floatsSHC0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.SerializableVector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableVector4__ctor_m1389529421 (SerializableVector4_t3177127415 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableUnityARLightData::.ctor(UnityEngine.XR.iOS.UnityARLightData)
extern "C"  void serializableUnityARLightData__ctor_m2325970984 (serializableUnityARLightData_t1864664250 * __this, UnityARLightData_t3404656299  ___lightData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARLightEstimate::.ctor(System.Single,System.Single)
extern "C"  void UnityARLightEstimate__ctor_m302426687 (UnityARLightEstimate_t2392533559 * __this, float ___intensity0, float ___temperature1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single[] Utils.serializableSHC::op_Implicit(Utils.serializableSHC)
extern "C"  SingleU5BU5D_t2905636975* serializableSHC_op_Implicit_m4253342705 (RuntimeObject * __this /* static, unused */, serializableSHC_t2973324561 * ___spc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.SerializableVector4 Utils.SerializableVector4::op_Implicit(UnityEngine.Vector4)
extern "C"  SerializableVector4_t3177127415 * SerializableVector4_op_Implicit_m2125629402 (RuntimeObject * __this /* static, unused */, Vector4_t380635127  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableUnityARMatrix4x4::.ctor(Utils.SerializableVector4,Utils.SerializableVector4,Utils.SerializableVector4,Utils.SerializableVector4)
extern "C"  void serializableUnityARMatrix4x4__ctor_m2539109597 (serializableUnityARMatrix4x4_t3548294260 * __this, SerializableVector4_t3177127415 * ___v00, SerializableVector4_t3177127415 * ___v11, SerializableVector4_t3177127415 * ___v22, SerializableVector4_t3177127415 * ___v33, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 Utils.SerializableVector4::op_Implicit(Utils.SerializableVector4)
extern "C"  Vector4_t380635127  SerializableVector4_op_Implicit_m946317734 (RuntimeObject * __this /* static, unused */, SerializableVector4_t3177127415 * ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARMatrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void UnityARMatrix4x4__ctor_m2409038275 (UnityARMatrix4x4_t758723042 * __this, Vector4_t380635127  ___c00, Vector4_t380635127  ___c11, Vector4_t380635127  ___c22, Vector4_t380635127  ___c33, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m3113358119 (Matrix4x4_t2375577114 * __this, Vector4_t380635127  p0, Vector4_t380635127  p1, Vector4_t380635127  p2, Vector4_t380635127  p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(UnityEngine.Matrix4x4)
extern "C"  serializableUnityARMatrix4x4_t3548294260 * serializableUnityARMatrix4x4_op_Implicit_m319493325 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2375577114  ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C"  Encoding_t3638904762 * Encoding_get_UTF8_m3605462633 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Utils.serializableUnityARPlaneAnchor::.ctor(Utils.serializableUnityARMatrix4x4,Utils.SerializableVector4,Utils.SerializableVector4,UnityEngine.XR.iOS.ARPlaneAnchorAlignment,System.Byte[])
extern "C"  void serializableUnityARPlaneAnchor__ctor_m1264816476 (serializableUnityARPlaneAnchor_t3395010059 * __this, serializableUnityARMatrix4x4_t3548294260 * ___wt0, SerializableVector4_t3177127415 * ___ctr1, SerializableVector4_t3177127415 * ___ext2, int64_t ___apaa3, ByteU5BU5D_t3287329517* ___idstr4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(Utils.serializableUnityARMatrix4x4)
extern "C"  Matrix4x4_t2375577114  serializableUnityARMatrix4x4_op_Implicit_m615038945 (RuntimeObject * __this /* static, unused */, serializableUnityARMatrix4x4_t3548294260 * ___rValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1554370993 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t1568665923* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m3770092030 (Vector4_t380635127 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void ARSessionTrackingChanged__ctor_m2654273567 (ARSessionTrackingChanged_t2903091015 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARSessionTrackingChanged_Invoke_m3010169940 (ARSessionTrackingChanged_t2903091015 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARSessionTrackingChanged_Invoke_m3010169940((ARSessionTrackingChanged_t2903091015 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged::BeginInvoke(UnityEngine.XR.iOS.UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARSessionTrackingChanged_BeginInvoke_m2513636707 (ARSessionTrackingChanged_t2903091015 * __this, UnityARCamera_t3270530332  ___camera0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARSessionTrackingChanged_BeginInvoke_m2513636707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARCamera_t3270530332_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged::EndInvoke(System.IAsyncResult)
extern "C"  void ARSessionTrackingChanged_EndInvoke_m2964807752 (ARSessionTrackingChanged_t2903091015 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARUserAnchorAdded_t389345868 (ARUserAnchorAdded_t389345868 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARUserAnchor_t2452746139_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARUserAnchor_t2452746139_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARUserAnchor_t2452746139_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARUserAnchor_t2452746139_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void ARUserAnchorAdded__ctor_m3687725670 (ARUserAnchorAdded_t389345868 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorAdded_Invoke_m1327168017 (ARUserAnchorAdded_t389345868 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARUserAnchorAdded_Invoke_m1327168017((ARUserAnchorAdded_t389345868 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.ARUserAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARUserAnchorAdded_BeginInvoke_m2356247880 (ARUserAnchorAdded_t389345868 * __this, ARUserAnchor_t2452746139  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUserAnchorAdded_BeginInvoke_m2356247880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARUserAnchor_t2452746139_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void ARUserAnchorAdded_EndInvoke_m3007140813 (ARUserAnchorAdded_t389345868 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARUserAnchorRemoved_t877041919 (ARUserAnchorRemoved_t877041919 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARUserAnchor_t2452746139_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARUserAnchor_t2452746139_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARUserAnchor_t2452746139_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARUserAnchor_t2452746139_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARUserAnchorRemoved__ctor_m4091998379 (ARUserAnchorRemoved_t877041919 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorRemoved_Invoke_m121198821 (ARUserAnchorRemoved_t877041919 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARUserAnchorRemoved_Invoke_m121198821((ARUserAnchorRemoved_t877041919 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.ARUserAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARUserAnchorRemoved_BeginInvoke_m2172053629 (ARUserAnchorRemoved_t877041919 * __this, ARUserAnchor_t2452746139  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUserAnchorRemoved_BeginInvoke_m2172053629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARUserAnchor_t2452746139_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void ARUserAnchorRemoved_EndInvoke_m4179834497 (ARUserAnchorRemoved_t877041919 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARUserAnchorUpdated_t3351790518 (ARUserAnchorUpdated_t3351790518 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARUserAnchor_t2452746139_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARUserAnchor_t2452746139_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARUserAnchor_t2452746139_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARUserAnchor_t2452746139_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARUserAnchorUpdated__ctor_m2339262563 (ARUserAnchorUpdated_t3351790518 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void ARUserAnchorUpdated_Invoke_m129708817 (ARUserAnchorUpdated_t3351790518 * __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARUserAnchorUpdated_Invoke_m129708817((ARUserAnchorUpdated_t3351790518 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARUserAnchor_t2452746139  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.ARUserAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARUserAnchorUpdated_BeginInvoke_m2439863858 (ARUserAnchorUpdated_t3351790518 * __this, ARUserAnchor_t2452746139  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUserAnchorUpdated_BeginInvoke_m2439863858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARUserAnchor_t2452746139_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void ARUserAnchorUpdated_EndInvoke_m3037378646 (ARUserAnchorUpdated_t3351790518 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1133215702 (internal_ARAnchorAdded_t1133215702 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t4160663845 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorAdded__ctor_m1178791246 (internal_ARAnchorAdded_t1133215702 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m656113583 (internal_ARAnchorAdded_t1133215702 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorAdded_Invoke_m656113583((internal_ARAnchorAdded_t1133215702 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorAdded_BeginInvoke_m2968559439 (internal_ARAnchorAdded_t1133215702 * __this, UnityARAnchorData_t4160663845  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorAdded_BeginInvoke_m2968559439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t4160663845_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorAdded_EndInvoke_m2627337622 (internal_ARAnchorAdded_t1133215702 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t1159356933 (internal_ARAnchorRemoved_t1159356933 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t4160663845 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorRemoved__ctor_m4227616229 (internal_ARAnchorRemoved_t1159356933 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m3601691325 (internal_ARAnchorRemoved_t1159356933 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorRemoved_Invoke_m3601691325((internal_ARAnchorRemoved_t1159356933 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorRemoved_BeginInvoke_m3876512937 (internal_ARAnchorRemoved_t1159356933 * __this, UnityARAnchorData_t4160663845  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorRemoved_BeginInvoke_m3876512937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t4160663845_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorRemoved_EndInvoke_m1722441813 (internal_ARAnchorRemoved_t1159356933 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2228873274 (internal_ARAnchorUpdated_t2228873274 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t4160663845 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorUpdated__ctor_m940197072 (internal_ARAnchorUpdated_t2228873274 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m1269740464 (internal_ARAnchorUpdated_t2228873274 * __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorUpdated_Invoke_m1269740464((internal_ARAnchorUpdated_t2228873274 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t4160663845  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorUpdated_BeginInvoke_m3557893065 (internal_ARAnchorUpdated_t2228873274 * __this, UnityARAnchorData_t4160663845  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorUpdated_BeginInvoke_m3557893065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t4160663845_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorUpdated_EndInvoke_m2567901127 (internal_ARAnchorUpdated_t2228873274 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFaceAnchorAdded_t338421685 (internal_ARFaceAnchorAdded_t338421685 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARFaceAnchorData_t2557914456 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFaceAnchorAdded__ctor_m4085783792 (internal_ARFaceAnchorAdded_t338421685 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorAdded_Invoke_m1719007874 (internal_ARFaceAnchorAdded_t338421685 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFaceAnchorAdded_Invoke_m1719007874((internal_ARFaceAnchorAdded_t338421685 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.UnityARFaceAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFaceAnchorAdded_BeginInvoke_m1170739208 (internal_ARFaceAnchorAdded_t338421685 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFaceAnchorAdded_BeginInvoke_m1170739208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARFaceAnchorData_t2557914456_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFaceAnchorAdded_EndInvoke_m3737615453 (internal_ARFaceAnchorAdded_t338421685 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFaceAnchorRemoved_t1507865825 (internal_ARFaceAnchorRemoved_t1507865825 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARFaceAnchorData_t2557914456 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFaceAnchorRemoved__ctor_m3274411438 (internal_ARFaceAnchorRemoved_t1507865825 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorRemoved_Invoke_m1431848752 (internal_ARFaceAnchorRemoved_t1507865825 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFaceAnchorRemoved_Invoke_m1431848752((internal_ARFaceAnchorRemoved_t1507865825 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.UnityARFaceAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFaceAnchorRemoved_BeginInvoke_m2452832694 (internal_ARFaceAnchorRemoved_t1507865825 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFaceAnchorRemoved_BeginInvoke_m2452832694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARFaceAnchorData_t2557914456_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFaceAnchorRemoved_EndInvoke_m536275619 (internal_ARFaceAnchorRemoved_t1507865825 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFaceAnchorUpdated_t4219811705 (internal_ARFaceAnchorUpdated_t4219811705 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARFaceAnchorData_t2557914456 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFaceAnchorUpdated__ctor_m923806037 (internal_ARFaceAnchorUpdated_t4219811705 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARFaceAnchorData)
extern "C"  void internal_ARFaceAnchorUpdated_Invoke_m1171236065 (internal_ARFaceAnchorUpdated_t4219811705 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFaceAnchorUpdated_Invoke_m1171236065((internal_ARFaceAnchorUpdated_t4219811705 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.UnityARFaceAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFaceAnchorUpdated_BeginInvoke_m2347222039 (internal_ARFaceAnchorUpdated_t4219811705 * __this, UnityARFaceAnchorData_t2557914456  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFaceAnchorUpdated_BeginInvoke_m2347222039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARFaceAnchorData_t2557914456_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFaceAnchorUpdated_EndInvoke_m3601342818 (internal_ARFaceAnchorUpdated_t4219811705 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFrameUpdate_t4139772791 (internal_ARFrameUpdate_t4139772791 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(internal_UnityARCamera_t2639053733 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___camera0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFrameUpdate__ctor_m3443179420 (internal_ARFrameUpdate_t4139772791 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m869157158 (internal_ARFrameUpdate_t4139772791 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFrameUpdate_Invoke_m869157158((internal_ARFrameUpdate_t4139772791 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::BeginInvoke(UnityEngine.XR.iOS.internal_UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFrameUpdate_BeginInvoke_m331441237 (internal_ARFrameUpdate_t4139772791 * __this, internal_UnityARCamera_t2639053733  ___camera0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFrameUpdate_BeginInvoke_m331441237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(internal_UnityARCamera_t2639053733_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFrameUpdate_EndInvoke_m4129018666 (internal_ARFrameUpdate_t4139772791 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t2450502912 (internal_ARSessionTrackingChanged_t2450502912 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(internal_UnityARCamera_t2639053733 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___camera0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARSessionTrackingChanged__ctor_m3157039935 (internal_ARSessionTrackingChanged_t2450502912 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARSessionTrackingChanged_Invoke_m3556349571 (internal_ARSessionTrackingChanged_t2450502912 * __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARSessionTrackingChanged_Invoke_m3556349571((internal_ARSessionTrackingChanged_t2450502912 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, internal_UnityARCamera_t2639053733  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged::BeginInvoke(UnityEngine.XR.iOS.internal_UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARSessionTrackingChanged_BeginInvoke_m222583015 (internal_ARSessionTrackingChanged_t2450502912 * __this, internal_UnityARCamera_t2639053733  ___camera0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARSessionTrackingChanged_BeginInvoke_m222583015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(internal_UnityARCamera_t2639053733_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARSessionTrackingChanged_EndInvoke_m1276121802 (internal_ARSessionTrackingChanged_t2450502912 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t1053280061 (internal_ARUserAnchorAdded_t1053280061 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARUserAnchorData_t1928721163 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARUserAnchorAdded__ctor_m1778325415 (internal_ARUserAnchorAdded_t1053280061 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorAdded_Invoke_m4248432723 (internal_ARUserAnchorAdded_t1053280061 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARUserAnchorAdded_Invoke_m4248432723((internal_ARUserAnchorAdded_t1053280061 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.UnityARUserAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARUserAnchorAdded_BeginInvoke_m388150249 (internal_ARUserAnchorAdded_t1053280061 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARUserAnchorAdded_BeginInvoke_m388150249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARUserAnchorData_t1928721163_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARUserAnchorAdded_EndInvoke_m1579897367 (internal_ARUserAnchorAdded_t1053280061 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t535060684 (internal_ARUserAnchorRemoved_t535060684 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARUserAnchorData_t1928721163 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARUserAnchorRemoved__ctor_m2846110239 (internal_ARUserAnchorRemoved_t535060684 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorRemoved_Invoke_m2022671894 (internal_ARUserAnchorRemoved_t535060684 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARUserAnchorRemoved_Invoke_m2022671894((internal_ARUserAnchorRemoved_t535060684 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.UnityARUserAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARUserAnchorRemoved_BeginInvoke_m1610706283 (internal_ARUserAnchorRemoved_t535060684 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARUserAnchorRemoved_BeginInvoke_m1610706283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARUserAnchorData_t1928721163_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARUserAnchorRemoved_EndInvoke_m3838266230 (internal_ARUserAnchorRemoved_t535060684 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t1262387502 (internal_ARUserAnchorUpdated_t1262387502 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARUserAnchorData_t1928721163 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARUserAnchorUpdated__ctor_m3321842099 (internal_ARUserAnchorUpdated_t1262387502 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARUserAnchorData)
extern "C"  void internal_ARUserAnchorUpdated_Invoke_m2747335307 (internal_ARUserAnchorUpdated_t1262387502 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARUserAnchorUpdated_Invoke_m2747335307((internal_ARUserAnchorUpdated_t1262387502 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARUserAnchorData_t1928721163  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.UnityARUserAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARUserAnchorUpdated_BeginInvoke_m1710900897 (internal_ARUserAnchorUpdated_t1262387502 * __this, UnityARUserAnchorData_t1928721163  ___anchorData0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARUserAnchorUpdated_BeginInvoke_m1710900897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARUserAnchorData_t1928721163_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARUserAnchorUpdated_EndInvoke_m2715190215 (internal_ARUserAnchorUpdated_t1262387502 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::.ctor()
extern "C"  void UnityARUserAnchorComponent__ctor_m3163001309 (UnityARUserAnchorComponent_t2848590843 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2458272640(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.XR.iOS.UnityARUserAnchorComponent::get_AnchorId()
extern "C"  String_t* UnityARUserAnchorComponent_get_AnchorId_m1159628693 (UnityARUserAnchorComponent_t2848590843 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string AnchorId  { get { return m_AnchorId; } }
		String_t* L_0 = __this->get_m_AnchorId_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public string AnchorId  { get { return m_AnchorId; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::Awake()
extern "C"  void UnityARUserAnchorComponent_Awake_m695952178 (UnityARUserAnchorComponent_t2848590843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUserAnchorComponent_Awake_m695952178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARUserAnchorData_t1928721163  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent += GameObjectAnchorUpdated;
		intptr_t L_0 = (intptr_t)UnityARUserAnchorComponent_GameObjectAnchorUpdated_m2627889658_RuntimeMethod_var;
		ARUserAnchorUpdated_t3351790518 * L_1 = (ARUserAnchorUpdated_t3351790518 *)il2cpp_codegen_object_new(ARUserAnchorUpdated_t3351790518_il2cpp_TypeInfo_var);
		ARUserAnchorUpdated__ctor_m2339262563(L_1, __this, L_0, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent += GameObjectAnchorUpdated;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARUserAnchorUpdatedEvent_m1405629722(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorRemovedEvent += AnchorRemoved;
		intptr_t L_2 = (intptr_t)UnityARUserAnchorComponent_AnchorRemoved_m2694785441_RuntimeMethod_var;
		ARUserAnchorRemoved_t877041919 * L_3 = (ARUserAnchorRemoved_t877041919 *)il2cpp_codegen_object_new(ARUserAnchorRemoved_t877041919_il2cpp_TypeInfo_var);
		ARUserAnchorRemoved__ctor_m4091998379(L_3, __this, L_2, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorRemovedEvent += AnchorRemoved;
		UnityARSessionNativeInterface_add_ARUserAnchorRemovedEvent_m1242199825(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface ().AddUserAnchorFromGameObject(this.gameObject).identifierStr;
		// this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface ().AddUserAnchorFromGameObject(this.gameObject).identifierStr;
		UnityARSessionNativeInterface_t4258909960 * L_4 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3297568107(NULL /*static, unused*/, /*hidden argument*/NULL);
		// this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface ().AddUserAnchorFromGameObject(this.gameObject).identifierStr;
		GameObject_t1318052361 * L_5 = Component_get_gameObject_m2399756717(__this, /*hidden argument*/NULL);
		// this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface ().AddUserAnchorFromGameObject(this.gameObject).identifierStr;
		NullCheck(L_4);
		UnityARUserAnchorData_t1928721163  L_6 = UnityARSessionNativeInterface_AddUserAnchorFromGameObject_m3599754494(L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface ().AddUserAnchorFromGameObject(this.gameObject).identifierStr;
		String_t* L_7 = UnityARUserAnchorData_get_identifierStr_m3595119354((&V_0), /*hidden argument*/NULL);
		__this->set_m_AnchorId_2(L_7);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::Start()
extern "C"  void UnityARUserAnchorComponent_Start_m4198847293 (UnityARUserAnchorComponent_t2848590843 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::AnchorRemoved(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void UnityARUserAnchorComponent_AnchorRemoved_m2694785441 (UnityARUserAnchorComponent_t2848590843 * __this, ARUserAnchor_t2452746139  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUserAnchorComponent_AnchorRemoved_m2694785441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (anchor.identifier.Equals(m_AnchorId))
		String_t* L_0 = (&___anchor0)->get_identifier_0();
		String_t* L_1 = __this->get_m_AnchorId_2();
		// if (anchor.identifier.Equals(m_AnchorId))
		NullCheck(L_0);
		bool L_2 = String_Equals_m2427690684(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		// Destroy(this.gameObject);
		// Destroy(this.gameObject);
		GameObject_t1318052361 * L_3 = Component_get_gameObject_m2399756717(__this, /*hidden argument*/NULL);
		// Destroy(this.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_Destroy_m935298371(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::OnDestroy()
extern "C"  void UnityARUserAnchorComponent_OnDestroy_m4107013468 (UnityARUserAnchorComponent_t2848590843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUserAnchorComponent_OnDestroy_m4107013468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent -= GameObjectAnchorUpdated;
		intptr_t L_0 = (intptr_t)UnityARUserAnchorComponent_GameObjectAnchorUpdated_m2627889658_RuntimeMethod_var;
		ARUserAnchorUpdated_t3351790518 * L_1 = (ARUserAnchorUpdated_t3351790518 *)il2cpp_codegen_object_new(ARUserAnchorUpdated_t3351790518_il2cpp_TypeInfo_var);
		ARUserAnchorUpdated__ctor_m2339262563(L_1, __this, L_0, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent -= GameObjectAnchorUpdated;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_remove_ARUserAnchorUpdatedEvent_m4204906472(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorRemovedEvent -= AnchorRemoved;
		intptr_t L_2 = (intptr_t)UnityARUserAnchorComponent_AnchorRemoved_m2694785441_RuntimeMethod_var;
		ARUserAnchorRemoved_t877041919 * L_3 = (ARUserAnchorRemoved_t877041919 *)il2cpp_codegen_object_new(ARUserAnchorRemoved_t877041919_il2cpp_TypeInfo_var);
		ARUserAnchorRemoved__ctor_m4091998379(L_3, __this, L_2, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARUserAnchorRemovedEvent -= AnchorRemoved;
		UnityARSessionNativeInterface_remove_ARUserAnchorRemovedEvent_m2646181465(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.GetARSessionNativeInterface ().RemoveUserAnchor(this.m_AnchorId);
		UnityARSessionNativeInterface_t4258909960 * L_4 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3297568107(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_m_AnchorId_2();
		// UnityARSessionNativeInterface.GetARSessionNativeInterface ().RemoveUserAnchor(this.m_AnchorId);
		NullCheck(L_4);
		UnityARSessionNativeInterface_RemoveUserAnchor_m238817152(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUserAnchorComponent::GameObjectAnchorUpdated(UnityEngine.XR.iOS.ARUserAnchor)
extern "C"  void UnityARUserAnchorComponent_GameObjectAnchorUpdated_m2627889658 (UnityARUserAnchorComponent_t2848590843 * __this, ARUserAnchor_t2452746139  ___anchor0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.String UnityEngine.XR.iOS.UnityARUserAnchorData::get_identifierStr()
extern "C"  String_t* UnityARUserAnchorData_get_identifierStr_m3595119354 (UnityARUserAnchorData_t1928721163 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUserAnchorData_get_identifierStr_m3595119354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// public string identifierStr { get { return Marshal.PtrToStringAuto(this.ptrIdentifier); } }
		intptr_t L_0 = __this->get_ptrIdentifier_0();
		// public string identifierStr { get { return Marshal.PtrToStringAuto(this.ptrIdentifier); } }
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t436595762_il2cpp_TypeInfo_var);
		String_t* L_1 = Marshal_PtrToStringAuto_m945446500(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public string identifierStr { get { return Marshal.PtrToStringAuto(this.ptrIdentifier); } }
		String_t* L_2 = V_0;
		return L_2;
	}
}
extern "C"  String_t* UnityARUserAnchorData_get_identifierStr_m3595119354_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	UnityARUserAnchorData_t1928721163 * _thisAdjusted = reinterpret_cast<UnityARUserAnchorData_t1928721163 *>(__this + 1);
	return UnityARUserAnchorData_get_identifierStr_m3595119354(_thisAdjusted, method);
}
// UnityEngine.XR.iOS.UnityARUserAnchorData UnityEngine.XR.iOS.UnityARUserAnchorData::UnityARUserAnchorDataFromGameObject(UnityEngine.GameObject)
extern "C"  UnityARUserAnchorData_t1928721163  UnityARUserAnchorData_UnityARUserAnchorDataFromGameObject_m3481842473 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUserAnchorData_UnityARUserAnchorDataFromGameObject_m3481842473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	UnityARUserAnchorData_t1928721163  V_1;
	memset(&V_1, 0, sizeof(V_1));
	UnityARUserAnchorData_t1928721163  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		GameObject_t1318052361 * L_0 = ___go0;
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_0);
		Transform_t532597831 * L_1 = GameObject_get_transform_m1187966899(L_0, /*hidden argument*/NULL);
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_1);
		Vector3_t329709361  L_2 = Transform_get_position_m609417876(L_1, /*hidden argument*/NULL);
		GameObject_t1318052361 * L_3 = ___go0;
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_3);
		Transform_t532597831 * L_4 = GameObject_get_transform_m1187966899(L_3, /*hidden argument*/NULL);
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_4);
		Quaternion_t2761156409  L_5 = Transform_get_rotation_m1089223433(L_4, /*hidden argument*/NULL);
		GameObject_t1318052361 * L_6 = ___go0;
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_6);
		Transform_t532597831 * L_7 = GameObject_get_transform_m1187966899(L_6, /*hidden argument*/NULL);
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		NullCheck(L_7);
		Vector3_t329709361  L_8 = Transform_get_localScale_m1865232606(L_7, /*hidden argument*/NULL);
		// Matrix4x4 matrix = Matrix4x4.TRS(go.transform.position, go.transform.rotation, go.transform.localScale);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2375577114_il2cpp_TypeInfo_var);
		Matrix4x4_t2375577114  L_9 = Matrix4x4_TRS_m2667050262(NULL /*static, unused*/, L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		// UnityARUserAnchorData ad = new UnityARUserAnchorData();
		Initobj (UnityARUserAnchorData_t1928721163_il2cpp_TypeInfo_var, (&V_1));
		// ad.transform.column0 = matrix.GetColumn(0);
		UnityARMatrix4x4_t758723042 * L_10 = (&V_1)->get_address_of_transform_1();
		// ad.transform.column0 = matrix.GetColumn(0);
		Vector4_t380635127  L_11 = Matrix4x4_GetColumn_m1167652957((&V_0), 0, /*hidden argument*/NULL);
		L_10->set_column0_0(L_11);
		// ad.transform.column1 = matrix.GetColumn(1);
		UnityARMatrix4x4_t758723042 * L_12 = (&V_1)->get_address_of_transform_1();
		// ad.transform.column1 = matrix.GetColumn(1);
		Vector4_t380635127  L_13 = Matrix4x4_GetColumn_m1167652957((&V_0), 1, /*hidden argument*/NULL);
		L_12->set_column1_1(L_13);
		// ad.transform.column2 = matrix.GetColumn(2);
		UnityARMatrix4x4_t758723042 * L_14 = (&V_1)->get_address_of_transform_1();
		// ad.transform.column2 = matrix.GetColumn(2);
		Vector4_t380635127  L_15 = Matrix4x4_GetColumn_m1167652957((&V_0), 2, /*hidden argument*/NULL);
		L_14->set_column2_2(L_15);
		// ad.transform.column3 = matrix.GetColumn(3);
		UnityARMatrix4x4_t758723042 * L_16 = (&V_1)->get_address_of_transform_1();
		// ad.transform.column3 = matrix.GetColumn(3);
		Vector4_t380635127  L_17 = Matrix4x4_GetColumn_m1167652957((&V_0), 3, /*hidden argument*/NULL);
		L_16->set_column3_3(L_17);
		// return ad;
		UnityARUserAnchorData_t1928721163  L_18 = V_1;
		V_2 = L_18;
		goto IL_0087;
	}

IL_0087:
	{
		// }
		UnityARUserAnchorData_t1928721163  L_19 = V_2;
		return L_19;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.ctor()
extern "C"  void UnityARUtility__ctor_m3720786599 (UnityARUtility_t4023213543 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::InitializePlanePrefab(UnityEngine.GameObject)
extern "C"  void UnityARUtility_InitializePlanePrefab_m1931429385 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_InitializePlanePrefab_m1931429385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// planePrefab = go;
		GameObject_t1318052361 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t4023213543_il2cpp_TypeInfo_var);
		((UnityARUtility_t4023213543_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t4023213543_il2cpp_TypeInfo_var))->set_planePrefab_2(L_0);
		// }
		return;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::CreatePlaneInScene(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1318052361 * UnityARUtility_CreatePlaneInScene_m2545193760 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t2525223154  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_CreatePlaneInScene_m2545193760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1318052361 * V_0 = NULL;
	GameObject_t1318052361 * V_1 = NULL;
	{
		// if (planePrefab != null) {
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t4023213543_il2cpp_TypeInfo_var);
		GameObject_t1318052361 * L_0 = ((UnityARUtility_t4023213543_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t4023213543_il2cpp_TypeInfo_var))->get_planePrefab_2();
		// if (planePrefab != null) {
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// plane = GameObject.Instantiate(planePrefab);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t4023213543_il2cpp_TypeInfo_var);
		GameObject_t1318052361 * L_2 = ((UnityARUtility_t4023213543_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t4023213543_il2cpp_TypeInfo_var))->get_planePrefab_2();
		// plane = GameObject.Instantiate(planePrefab);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		GameObject_t1318052361 * L_3 = Object_Instantiate_TisGameObject_t1318052361_m3083066963(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1318052361_m3083066963_RuntimeMethod_var);
		V_0 = L_3;
		goto IL_002b;
	}

IL_0023:
	{
		// plane = new GameObject (); //put in a blank gameObject to get at least a transform to manipulate
		GameObject_t1318052361 * L_4 = (GameObject_t1318052361 *)il2cpp_codegen_object_new(GameObject_t1318052361_il2cpp_TypeInfo_var);
		GameObject__ctor_m2351111389(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002b:
	{
		// plane.name = arPlaneAnchor.identifier;
		GameObject_t1318052361 * L_5 = V_0;
		String_t* L_6 = (&___arPlaneAnchor0)->get_identifier_0();
		// plane.name = arPlaneAnchor.identifier;
		NullCheck(L_5);
		Object_set_name_m2629735859(L_5, L_6, /*hidden argument*/NULL);
		// return UpdatePlaneWithAnchorTransform(plane, arPlaneAnchor);
		GameObject_t1318052361 * L_7 = V_0;
		ARPlaneAnchor_t2525223154  L_8 = ___arPlaneAnchor0;
		// return UpdatePlaneWithAnchorTransform(plane, arPlaneAnchor);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t4023213543_il2cpp_TypeInfo_var);
		GameObject_t1318052361 * L_9 = UnityARUtility_UpdatePlaneWithAnchorTransform_m796920091(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0045;
	}

IL_0045:
	{
		// }
		GameObject_t1318052361 * L_10 = V_1;
		return L_10;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1318052361 * UnityARUtility_UpdatePlaneWithAnchorTransform_m796920091 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___plane0, ARPlaneAnchor_t2525223154  ___arPlaneAnchor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_UpdatePlaneWithAnchorTransform_m796920091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t1334808991 * V_0 = NULL;
	GameObject_t1318052361 * V_1 = NULL;
	{
		// plane.transform.position = UnityARMatrixOps.GetPosition (arPlaneAnchor.transform);
		GameObject_t1318052361 * L_0 = ___plane0;
		// plane.transform.position = UnityARMatrixOps.GetPosition (arPlaneAnchor.transform);
		NullCheck(L_0);
		Transform_t532597831 * L_1 = GameObject_get_transform_m1187966899(L_0, /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_2 = (&___arPlaneAnchor1)->get_transform_1();
		// plane.transform.position = UnityARMatrixOps.GetPosition (arPlaneAnchor.transform);
		Vector3_t329709361  L_3 = UnityARMatrixOps_GetPosition_m2593915132(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// plane.transform.position = UnityARMatrixOps.GetPosition (arPlaneAnchor.transform);
		NullCheck(L_1);
		Transform_set_position_m1768136472(L_1, L_3, /*hidden argument*/NULL);
		// plane.transform.rotation = UnityARMatrixOps.GetRotation (arPlaneAnchor.transform);
		GameObject_t1318052361 * L_4 = ___plane0;
		// plane.transform.rotation = UnityARMatrixOps.GetRotation (arPlaneAnchor.transform);
		NullCheck(L_4);
		Transform_t532597831 * L_5 = GameObject_get_transform_m1187966899(L_4, /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_6 = (&___arPlaneAnchor1)->get_transform_1();
		// plane.transform.rotation = UnityARMatrixOps.GetRotation (arPlaneAnchor.transform);
		Quaternion_t2761156409  L_7 = UnityARMatrixOps_GetRotation_m759098689(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// plane.transform.rotation = UnityARMatrixOps.GetRotation (arPlaneAnchor.transform);
		NullCheck(L_5);
		Transform_set_rotation_m4058359888(L_5, L_7, /*hidden argument*/NULL);
		// MeshFilter mf = plane.GetComponentInChildren<MeshFilter> ();
		GameObject_t1318052361 * L_8 = ___plane0;
		// MeshFilter mf = plane.GetComponentInChildren<MeshFilter> ();
		NullCheck(L_8);
		MeshFilter_t1334808991 * L_9 = GameObject_GetComponentInChildren_TisMeshFilter_t1334808991_m2726185362(L_8, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshFilter_t1334808991_m2726185362_RuntimeMethod_var);
		V_0 = L_9;
		// if (mf != null) {
		MeshFilter_t1334808991 * L_10 = V_0;
		// if (mf != null) {
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_10, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c9;
		}
	}
	{
		// mf.gameObject.transform.localScale = new Vector3(arPlaneAnchor.extent.x * 0.1f ,arPlaneAnchor.extent.y * 0.1f ,arPlaneAnchor.extent.z * 0.1f );
		MeshFilter_t1334808991 * L_12 = V_0;
		// mf.gameObject.transform.localScale = new Vector3(arPlaneAnchor.extent.x * 0.1f ,arPlaneAnchor.extent.y * 0.1f ,arPlaneAnchor.extent.z * 0.1f );
		NullCheck(L_12);
		GameObject_t1318052361 * L_13 = Component_get_gameObject_m2399756717(L_12, /*hidden argument*/NULL);
		// mf.gameObject.transform.localScale = new Vector3(arPlaneAnchor.extent.x * 0.1f ,arPlaneAnchor.extent.y * 0.1f ,arPlaneAnchor.extent.z * 0.1f );
		NullCheck(L_13);
		Transform_t532597831 * L_14 = GameObject_get_transform_m1187966899(L_13, /*hidden argument*/NULL);
		Vector3_t329709361 * L_15 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_16 = L_15->get_x_1();
		Vector3_t329709361 * L_17 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_18 = L_17->get_y_2();
		Vector3_t329709361 * L_19 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_20 = L_19->get_z_3();
		// mf.gameObject.transform.localScale = new Vector3(arPlaneAnchor.extent.x * 0.1f ,arPlaneAnchor.extent.y * 0.1f ,arPlaneAnchor.extent.z * 0.1f );
		Vector3_t329709361  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m3984700005((&L_21), ((float)((float)L_16*(float)(0.1f))), ((float)((float)L_18*(float)(0.1f))), ((float)((float)L_20*(float)(0.1f))), /*hidden argument*/NULL);
		// mf.gameObject.transform.localScale = new Vector3(arPlaneAnchor.extent.x * 0.1f ,arPlaneAnchor.extent.y * 0.1f ,arPlaneAnchor.extent.z * 0.1f );
		NullCheck(L_14);
		Transform_set_localScale_m3356822685(L_14, L_21, /*hidden argument*/NULL);
		// mf.gameObject.transform.localPosition = new Vector3(arPlaneAnchor.center.x,arPlaneAnchor.center.y, -arPlaneAnchor.center.z);
		MeshFilter_t1334808991 * L_22 = V_0;
		// mf.gameObject.transform.localPosition = new Vector3(arPlaneAnchor.center.x,arPlaneAnchor.center.y, -arPlaneAnchor.center.z);
		NullCheck(L_22);
		GameObject_t1318052361 * L_23 = Component_get_gameObject_m2399756717(L_22, /*hidden argument*/NULL);
		// mf.gameObject.transform.localPosition = new Vector3(arPlaneAnchor.center.x,arPlaneAnchor.center.y, -arPlaneAnchor.center.z);
		NullCheck(L_23);
		Transform_t532597831 * L_24 = GameObject_get_transform_m1187966899(L_23, /*hidden argument*/NULL);
		Vector3_t329709361 * L_25 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_26 = L_25->get_x_1();
		Vector3_t329709361 * L_27 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_28 = L_27->get_y_2();
		Vector3_t329709361 * L_29 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_30 = L_29->get_z_3();
		// mf.gameObject.transform.localPosition = new Vector3(arPlaneAnchor.center.x,arPlaneAnchor.center.y, -arPlaneAnchor.center.z);
		Vector3_t329709361  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m3984700005((&L_31), L_26, L_28, ((-L_30)), /*hidden argument*/NULL);
		// mf.gameObject.transform.localPosition = new Vector3(arPlaneAnchor.center.x,arPlaneAnchor.center.y, -arPlaneAnchor.center.z);
		NullCheck(L_24);
		Transform_set_localPosition_m3967901824(L_24, L_31, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		// return plane;
		GameObject_t1318052361 * L_32 = ___plane0;
		V_1 = L_32;
		goto IL_00d0;
	}

IL_00d0:
	{
		// }
		GameObject_t1318052361 * L_33 = V_1;
		return L_33;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.cctor()
extern "C"  void UnityARUtility__cctor_m990246171 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility__cctor_m990246171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static GameObject planePrefab = null;
		((UnityARUtility_t4023213543_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t4023213543_il2cpp_TypeInfo_var))->set_planePrefab_2((GameObject_t1318052361 *)NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::.ctor()
extern "C"  void UnityARVideo__ctor_m2451982169 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2458272640(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::Start()
extern "C"  void UnityARVideo_Start_m742982592 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_Start_m742982592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateFrame;
		intptr_t L_0 = (intptr_t)UnityARVideo_UpdateFrame_m1610612707_RuntimeMethod_var;
		ARFrameUpdate_t2274630099 * L_1 = (ARFrameUpdate_t2274630099 *)il2cpp_codegen_object_new(ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1552988806(L_1, __this, L_0, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateFrame;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1052241241(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// bCommandBufferInitialized = false;
		__this->set_bCommandBufferInitialized_7((bool)0);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::UpdateFrame(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityARVideo_UpdateFrame_m1610612707 (UnityARVideo_t2486736449 * __this, UnityARCamera_t3270530332  ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_UpdateFrame_m1610612707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// _displayTransform = new Matrix4x4();
		Initobj (Matrix4x4_t2375577114_il2cpp_TypeInfo_var, (&V_0));
		Matrix4x4_t2375577114  L_0 = V_0;
		__this->set__displayTransform_6(L_0);
		// _displayTransform.SetColumn(0, cam.displayTransform.column0);
		Matrix4x4_t2375577114 * L_1 = __this->get_address_of__displayTransform_6();
		UnityARMatrix4x4_t758723042 * L_2 = (&___cam0)->get_address_of_displayTransform_6();
		Vector4_t380635127  L_3 = L_2->get_column0_0();
		// _displayTransform.SetColumn(0, cam.displayTransform.column0);
		Matrix4x4_SetColumn_m3548282205(L_1, 0, L_3, /*hidden argument*/NULL);
		// _displayTransform.SetColumn(1, cam.displayTransform.column1);
		Matrix4x4_t2375577114 * L_4 = __this->get_address_of__displayTransform_6();
		UnityARMatrix4x4_t758723042 * L_5 = (&___cam0)->get_address_of_displayTransform_6();
		Vector4_t380635127  L_6 = L_5->get_column1_1();
		// _displayTransform.SetColumn(1, cam.displayTransform.column1);
		Matrix4x4_SetColumn_m3548282205(L_4, 1, L_6, /*hidden argument*/NULL);
		// _displayTransform.SetColumn(2, cam.displayTransform.column2);
		Matrix4x4_t2375577114 * L_7 = __this->get_address_of__displayTransform_6();
		UnityARMatrix4x4_t758723042 * L_8 = (&___cam0)->get_address_of_displayTransform_6();
		Vector4_t380635127  L_9 = L_8->get_column2_2();
		// _displayTransform.SetColumn(2, cam.displayTransform.column2);
		Matrix4x4_SetColumn_m3548282205(L_7, 2, L_9, /*hidden argument*/NULL);
		// _displayTransform.SetColumn(3, cam.displayTransform.column3);
		Matrix4x4_t2375577114 * L_10 = __this->get_address_of__displayTransform_6();
		UnityARMatrix4x4_t758723042 * L_11 = (&___cam0)->get_address_of_displayTransform_6();
		Vector4_t380635127  L_12 = L_11->get_column3_3();
		// _displayTransform.SetColumn(3, cam.displayTransform.column3);
		Matrix4x4_SetColumn_m3548282205(L_10, 3, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m1137915321 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_InitializeCommandBuffer_m1137915321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_VideoCommandBuffer = new CommandBuffer();
		// m_VideoCommandBuffer = new CommandBuffer();
		CommandBuffer_t2673047760 * L_0 = (CommandBuffer_t2673047760 *)il2cpp_codegen_object_new(CommandBuffer_t2673047760_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m125783349(L_0, /*hidden argument*/NULL);
		__this->set_m_VideoCommandBuffer_3(L_0);
		// m_VideoCommandBuffer.Blit(null, BuiltinRenderTextureType.CurrentActive, m_ClearMaterial);
		CommandBuffer_t2673047760 * L_1 = __this->get_m_VideoCommandBuffer_3();
		// m_VideoCommandBuffer.Blit(null, BuiltinRenderTextureType.CurrentActive, m_ClearMaterial);
		RenderTargetIdentifier_t3642434912  L_2 = RenderTargetIdentifier_op_Implicit_m2181182592(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Material_t2712136762 * L_3 = __this->get_m_ClearMaterial_2();
		// m_VideoCommandBuffer.Blit(null, BuiltinRenderTextureType.CurrentActive, m_ClearMaterial);
		NullCheck(L_1);
		CommandBuffer_Blit_m3942920201(L_1, (Texture_t2838694469 *)NULL, L_2, L_3, /*hidden argument*/NULL);
		// GetComponent<Camera>().AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		// GetComponent<Camera>().AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		Camera_t989002943 * L_4 = Component_GetComponent_TisCamera_t989002943_m3380036349(__this, /*hidden argument*/Component_GetComponent_TisCamera_t989002943_m3380036349_RuntimeMethod_var);
		CommandBuffer_t2673047760 * L_5 = __this->get_m_VideoCommandBuffer_3();
		// GetComponent<Camera>().AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		NullCheck(L_4);
		Camera_AddCommandBuffer_m3104925877(L_4, ((int32_t)10), L_5, /*hidden argument*/NULL);
		// bCommandBufferInitialized = true;
		__this->set_bCommandBufferInitialized_7((bool)1);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnDestroy()
extern "C"  void UnityARVideo_OnDestroy_m2358615504 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnDestroy_m2358615504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<Camera>().RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		// GetComponent<Camera>().RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		Camera_t989002943 * L_0 = Component_GetComponent_TisCamera_t989002943_m3380036349(__this, /*hidden argument*/Component_GetComponent_TisCamera_t989002943_m3380036349_RuntimeMethod_var);
		CommandBuffer_t2673047760 * L_1 = __this->get_m_VideoCommandBuffer_3();
		// GetComponent<Camera>().RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);
		NullCheck(L_0);
		Camera_RemoveCommandBuffer_m1713736922(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent -= UpdateFrame;
		intptr_t L_2 = (intptr_t)UnityARVideo_UpdateFrame_m1610612707_RuntimeMethod_var;
		ARFrameUpdate_t2274630099 * L_3 = (ARFrameUpdate_t2274630099 *)il2cpp_codegen_object_new(ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1552988806(L_3, __this, L_2, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent -= UpdateFrame;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m734046990(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// bCommandBufferInitialized = false;
		__this->set_bCommandBufferInitialized_7((bool)0);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnPreRender()
extern "C"  void UnityARVideo_OnPreRender_m618482650 (UnityARVideo_t2486736449 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnPreRender_m618482650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARTextureHandles_t1453164273  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t1041838152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// ARTextureHandles handles = UnityARSessionNativeInterface.GetARSessionNativeInterface ().GetARVideoTextureHandles();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t4258909960 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3297568107(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ARTextureHandles handles = UnityARSessionNativeInterface.GetARSessionNativeInterface ().GetARVideoTextureHandles();
		NullCheck(L_0);
		ARTextureHandles_t1453164273  L_1 = UnityARSessionNativeInterface_GetARVideoTextureHandles_m1818998859(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		intptr_t L_2 = (&V_0)->get_textureY_0();
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		bool L_3 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		intptr_t L_4 = (&V_0)->get_textureCbCr_1();
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		bool L_5 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_4, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}

IL_0038:
	{
		// return;
		goto IL_015b;
	}

IL_003e:
	{
		// if (!bCommandBufferInitialized) {
		bool L_6 = __this->get_bCommandBufferInitialized_7();
		if (L_6)
		{
			goto IL_0051;
		}
	}
	{
		// InitializeCommandBuffer ();
		// InitializeCommandBuffer ();
		UnityARVideo_InitializeCommandBuffer_m1137915321(__this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		// Resolution currentResolution = Screen.currentResolution;
		Resolution_t1041838152  L_7 = Screen_get_currentResolution_m205929278(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_7;
		// if (_videoTextureY == null) {
		Texture2D_t878840578 * L_8 = __this->get__videoTextureY_4();
		// if (_videoTextureY == null) {
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_8, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00bc;
		}
	}
	{
		// _videoTextureY = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		// _videoTextureY = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		int32_t L_10 = Resolution_get_width_m2175655275((&V_1), /*hidden argument*/NULL);
		// _videoTextureY = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		int32_t L_11 = Resolution_get_height_m3498799763((&V_1), /*hidden argument*/NULL);
		intptr_t L_12 = (&V_0)->get_textureY_0();
		// _videoTextureY = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		Texture2D_t878840578 * L_13 = Texture2D_CreateExternalTexture_m4069785793(NULL /*static, unused*/, L_10, L_11, ((int32_t)63), (bool)0, (bool)0, L_12, /*hidden argument*/NULL);
		__this->set__videoTextureY_4(L_13);
		// _videoTextureY.filterMode = FilterMode.Bilinear;
		Texture2D_t878840578 * L_14 = __this->get__videoTextureY_4();
		// _videoTextureY.filterMode = FilterMode.Bilinear;
		NullCheck(L_14);
		Texture_set_filterMode_m1565425947(L_14, 1, /*hidden argument*/NULL);
		// _videoTextureY.wrapMode = TextureWrapMode.Repeat;
		Texture2D_t878840578 * L_15 = __this->get__videoTextureY_4();
		// _videoTextureY.wrapMode = TextureWrapMode.Repeat;
		NullCheck(L_15);
		Texture_set_wrapMode_m2103720371(L_15, 0, /*hidden argument*/NULL);
		// m_ClearMaterial.SetTexture("_textureY", _videoTextureY);
		Material_t2712136762 * L_16 = __this->get_m_ClearMaterial_2();
		Texture2D_t878840578 * L_17 = __this->get__videoTextureY_4();
		// m_ClearMaterial.SetTexture("_textureY", _videoTextureY);
		NullCheck(L_16);
		Material_SetTexture_m825494064(L_16, _stringLiteral4120529429, L_17, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		// if (_videoTextureCbCr == null) {
		Texture2D_t878840578 * L_18 = __this->get__videoTextureCbCr_5();
		// if (_videoTextureCbCr == null) {
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_18, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0121;
		}
	}
	{
		// _videoTextureCbCr = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		// _videoTextureCbCr = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		int32_t L_20 = Resolution_get_width_m2175655275((&V_1), /*hidden argument*/NULL);
		// _videoTextureCbCr = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		int32_t L_21 = Resolution_get_height_m3498799763((&V_1), /*hidden argument*/NULL);
		intptr_t L_22 = (&V_0)->get_textureCbCr_1();
		// _videoTextureCbCr = Texture2D.CreateExternalTexture(currentResolution.width, currentResolution.height,
		Texture2D_t878840578 * L_23 = Texture2D_CreateExternalTexture_m4069785793(NULL /*static, unused*/, L_20, L_21, ((int32_t)62), (bool)0, (bool)0, L_22, /*hidden argument*/NULL);
		__this->set__videoTextureCbCr_5(L_23);
		// _videoTextureCbCr.filterMode = FilterMode.Bilinear;
		Texture2D_t878840578 * L_24 = __this->get__videoTextureCbCr_5();
		// _videoTextureCbCr.filterMode = FilterMode.Bilinear;
		NullCheck(L_24);
		Texture_set_filterMode_m1565425947(L_24, 1, /*hidden argument*/NULL);
		// _videoTextureCbCr.wrapMode = TextureWrapMode.Repeat;
		Texture2D_t878840578 * L_25 = __this->get__videoTextureCbCr_5();
		// _videoTextureCbCr.wrapMode = TextureWrapMode.Repeat;
		NullCheck(L_25);
		Texture_set_wrapMode_m2103720371(L_25, 0, /*hidden argument*/NULL);
		// m_ClearMaterial.SetTexture("_textureCbCr", _videoTextureCbCr);
		Material_t2712136762 * L_26 = __this->get_m_ClearMaterial_2();
		Texture2D_t878840578 * L_27 = __this->get__videoTextureCbCr_5();
		// m_ClearMaterial.SetTexture("_textureCbCr", _videoTextureCbCr);
		NullCheck(L_26);
		Material_SetTexture_m825494064(L_26, _stringLiteral194183554, L_27, /*hidden argument*/NULL);
	}

IL_0121:
	{
		// _videoTextureY.UpdateExternalTexture(handles.textureY);
		Texture2D_t878840578 * L_28 = __this->get__videoTextureY_4();
		intptr_t L_29 = (&V_0)->get_textureY_0();
		// _videoTextureY.UpdateExternalTexture(handles.textureY);
		NullCheck(L_28);
		Texture2D_UpdateExternalTexture_m4242697313(L_28, L_29, /*hidden argument*/NULL);
		// _videoTextureCbCr.UpdateExternalTexture(handles.textureCbCr);
		Texture2D_t878840578 * L_30 = __this->get__videoTextureCbCr_5();
		intptr_t L_31 = (&V_0)->get_textureCbCr_1();
		// _videoTextureCbCr.UpdateExternalTexture(handles.textureCbCr);
		NullCheck(L_30);
		Texture2D_UpdateExternalTexture_m4242697313(L_30, L_31, /*hidden argument*/NULL);
		// m_ClearMaterial.SetMatrix("_DisplayTransform", _displayTransform);
		Material_t2712136762 * L_32 = __this->get_m_ClearMaterial_2();
		Matrix4x4_t2375577114  L_33 = __this->get__displayTransform_6();
		// m_ClearMaterial.SetMatrix("_DisplayTransform", _displayTransform);
		NullCheck(L_32);
		Material_SetMatrix_m2294933890(L_32, _stringLiteral2552841393, L_33, /*hidden argument*/NULL);
	}

IL_015b:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityMarshalLightData::.ctor(UnityEngine.XR.iOS.LightDataType,UnityEngine.XR.iOS.UnityARLightEstimate,UnityEngine.XR.iOS.MarshalDirectionalLightEstimate)
extern "C"  void UnityMarshalLightData__ctor_m208288196 (UnityMarshalLightData_t989795789 * __this, int32_t ___ldt0, UnityARLightEstimate_t2392533559  ___ule1, MarshalDirectionalLightEstimate_t3103061175  ___mdle2, const RuntimeMethod* method)
{
	{
		// arLightingType = ldt;
		int32_t L_0 = ___ldt0;
		__this->set_arLightingType_0(L_0);
		// arLightEstimate = ule;
		UnityARLightEstimate_t2392533559  L_1 = ___ule1;
		__this->set_arLightEstimate_1(L_1);
		// arDirectonalLightEstimate = mdle;
		MarshalDirectionalLightEstimate_t3103061175  L_2 = ___mdle2;
		__this->set_arDirectonalLightEstimate_2(L_2);
		// }
		return;
	}
}
extern "C"  void UnityMarshalLightData__ctor_m208288196_AdjustorThunk (RuntimeObject * __this, int32_t ___ldt0, UnityARLightEstimate_t2392533559  ___ule1, MarshalDirectionalLightEstimate_t3103061175  ___mdle2, const RuntimeMethod* method)
{
	UnityMarshalLightData_t989795789 * _thisAdjusted = reinterpret_cast<UnityMarshalLightData_t989795789 *>(__this + 1);
	UnityMarshalLightData__ctor_m208288196(_thisAdjusted, ___ldt0, ___ule1, ___mdle2, method);
}
// UnityEngine.XR.iOS.UnityARLightData UnityEngine.XR.iOS.UnityMarshalLightData::op_Implicit(UnityEngine.XR.iOS.UnityMarshalLightData)
extern "C"  UnityARLightData_t3404656299  UnityMarshalLightData_op_Implicit_m1490147344 (RuntimeObject * __this /* static, unused */, UnityMarshalLightData_t989795789  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMarshalLightData_op_Implicit_m1490147344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARDirectionalLightEstimate_t3433403765 * V_0 = NULL;
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t329709361  V_2;
	memset(&V_2, 0, sizeof(V_2));
	SingleU5BU5D_t2905636975* V_3 = NULL;
	UnityARLightData_t3404656299  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// UnityARDirectionalLightEstimate udle = null;
		V_0 = (UnityARDirectionalLightEstimate_t3433403765 *)NULL;
		// if (rValue.arLightingType == LightDataType.DirectionalLightEstimate) {
		int32_t L_0 = (&___rValue0)->get_arLightingType_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		// Vector4 lightDirAndIntensity = rValue.arDirectonalLightEstimate.primaryDirAndIntensity;
		MarshalDirectionalLightEstimate_t3103061175 * L_1 = (&___rValue0)->get_address_of_arDirectonalLightEstimate_2();
		Vector4_t380635127  L_2 = L_1->get_primaryDirAndIntensity_0();
		V_1 = L_2;
		// Vector3 lightDir = new Vector3 (lightDirAndIntensity.x, lightDirAndIntensity.y, lightDirAndIntensity.z);
		float L_3 = (&V_1)->get_x_1();
		float L_4 = (&V_1)->get_y_2();
		float L_5 = (&V_1)->get_z_3();
		// Vector3 lightDir = new Vector3 (lightDirAndIntensity.x, lightDirAndIntensity.y, lightDirAndIntensity.z);
		Vector3__ctor_m3984700005((&V_2), L_3, L_4, L_5, /*hidden argument*/NULL);
		// float[] shc = rValue.arDirectonalLightEstimate.SphericalHarmonicCoefficients;
		MarshalDirectionalLightEstimate_t3103061175 * L_6 = (&___rValue0)->get_address_of_arDirectonalLightEstimate_2();
		// float[] shc = rValue.arDirectonalLightEstimate.SphericalHarmonicCoefficients;
		SingleU5BU5D_t2905636975* L_7 = MarshalDirectionalLightEstimate_get_SphericalHarmonicCoefficients_m3181496191(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		// udle = new UnityARDirectionalLightEstimate (shc, lightDir, lightDirAndIntensity.w);
		SingleU5BU5D_t2905636975* L_8 = V_3;
		Vector3_t329709361  L_9 = V_2;
		float L_10 = (&V_1)->get_w_4();
		// udle = new UnityARDirectionalLightEstimate (shc, lightDir, lightDirAndIntensity.w);
		UnityARDirectionalLightEstimate_t3433403765 * L_11 = (UnityARDirectionalLightEstimate_t3433403765 *)il2cpp_codegen_object_new(UnityARDirectionalLightEstimate_t3433403765_il2cpp_TypeInfo_var);
		UnityARDirectionalLightEstimate__ctor_m3195205346(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0057:
	{
		// return new UnityARLightData(rValue.arLightingType, rValue.arLightEstimate, udle);
		int32_t L_12 = (&___rValue0)->get_arLightingType_0();
		UnityARLightEstimate_t2392533559  L_13 = (&___rValue0)->get_arLightEstimate_1();
		UnityARDirectionalLightEstimate_t3433403765 * L_14 = V_0;
		// return new UnityARLightData(rValue.arLightingType, rValue.arLightEstimate, udle);
		UnityARLightData_t3404656299  L_15;
		memset(&L_15, 0, sizeof(L_15));
		UnityARLightData__ctor_m2108807113((&L_15), L_12, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		goto IL_0072;
	}

IL_0072:
	{
		// }
		UnityARLightData_t3404656299  L_16 = V_4;
		return L_16;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::.ctor()
extern "C"  void UnityRemoteVideo__ctor_m1065551706 (UnityRemoteVideo_t3391959971 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2458272640(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::Start()
extern "C"  void UnityRemoteVideo_Start_m1559177737 (UnityRemoteVideo_t3391959971 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRemoteVideo_Start_m1559177737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Session = UnityARSessionNativeInterface.GetARSessionNativeInterface ();
		// m_Session = UnityARSessionNativeInterface.GetARSessionNativeInterface ();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t4258909960 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3297568107(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Session_3(L_0);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateCamera;
		intptr_t L_1 = (intptr_t)UnityRemoteVideo_UpdateCamera_m15137629_RuntimeMethod_var;
		ARFrameUpdate_t2274630099 * L_2 = (ARFrameUpdate_t2274630099 *)il2cpp_codegen_object_new(ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1552988806(L_2, __this, L_1, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateCamera;
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1052241241(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// currentFrameIndex = 0;
		__this->set_currentFrameIndex_5(0);
		// bTexturesInitialized = false;
		__this->set_bTexturesInitialized_4((bool)0);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::UpdateCamera(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityRemoteVideo_UpdateCamera_m15137629 (UnityRemoteVideo_t3391959971 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRemoteVideo_UpdateCamera_m15137629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!bTexturesInitialized) {
		bool L_0 = __this->get_bTexturesInitialized_4();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		// InitializeTextures (camera);
		UnityARCamera_t3270530332  L_1 = ___camera0;
		// InitializeTextures (camera);
		UnityRemoteVideo_InitializeTextures_m1297643449(__this, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent -= UpdateCamera;
		intptr_t L_2 = (intptr_t)UnityRemoteVideo_UpdateCamera_m15137629_RuntimeMethod_var;
		ARFrameUpdate_t2274630099 * L_3 = (ARFrameUpdate_t2274630099 *)il2cpp_codegen_object_new(ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1552988806(L_3, __this, L_2, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent -= UpdateCamera;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m734046990(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::InitializeTextures(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityRemoteVideo_InitializeTextures_m1297643449 (UnityRemoteVideo_t3391959971 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRemoteVideo_InitializeTextures_m1297643449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int numYBytes = camera.videoParams.yWidth * camera.videoParams.yHeight;
		UnityVideoParams_t909694111 * L_0 = (&___camera0)->get_address_of_videoParams_4();
		int32_t L_1 = L_0->get_yWidth_0();
		UnityVideoParams_t909694111 * L_2 = (&___camera0)->get_address_of_videoParams_4();
		int32_t L_3 = L_2->get_yHeight_1();
		V_0 = ((int32_t)((int32_t)L_1*(int32_t)L_3));
		// int numUVBytes = camera.videoParams.yWidth * camera.videoParams.yHeight / 2; //quarter resolution, but two bytes per pixel
		UnityVideoParams_t909694111 * L_4 = (&___camera0)->get_address_of_videoParams_4();
		int32_t L_5 = L_4->get_yWidth_0();
		UnityVideoParams_t909694111 * L_6 = (&___camera0)->get_address_of_videoParams_4();
		int32_t L_7 = L_6->get_yHeight_1();
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)L_7))/(int32_t)2));
		// m_textureYBytes = new byte[numYBytes];
		int32_t L_8 = V_0;
		__this->set_m_textureYBytes_6(((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)L_8)));
		// m_textureUVBytes = new byte[numUVBytes];
		int32_t L_9 = V_1;
		__this->set_m_textureUVBytes_7(((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)L_9)));
		// m_textureYBytes2 = new byte[numYBytes];
		int32_t L_10 = V_0;
		__this->set_m_textureYBytes2_8(((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)L_10)));
		// m_textureUVBytes2 = new byte[numUVBytes];
		int32_t L_11 = V_1;
		__this->set_m_textureUVBytes2_9(((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		// m_pinnedYArray = GCHandle.Alloc (m_textureYBytes);
		ByteU5BU5D_t3287329517* L_12 = __this->get_m_textureYBytes_6();
		// m_pinnedYArray = GCHandle.Alloc (m_textureYBytes);
		GCHandle_t714486722  L_13 = GCHandle_Alloc_m3660244495(NULL /*static, unused*/, (RuntimeObject *)(RuntimeObject *)L_12, /*hidden argument*/NULL);
		__this->set_m_pinnedYArray_10(L_13);
		// m_pinnedUVArray = GCHandle.Alloc (m_textureUVBytes);
		ByteU5BU5D_t3287329517* L_14 = __this->get_m_textureUVBytes_7();
		// m_pinnedUVArray = GCHandle.Alloc (m_textureUVBytes);
		GCHandle_t714486722  L_15 = GCHandle_Alloc_m3660244495(NULL /*static, unused*/, (RuntimeObject *)(RuntimeObject *)L_14, /*hidden argument*/NULL);
		__this->set_m_pinnedUVArray_11(L_15);
		// bTexturesInitialized = true;
		__this->set_bTexturesInitialized_4((bool)1);
		// }
		return;
	}
}
// System.IntPtr UnityEngine.XR.iOS.UnityRemoteVideo::PinByteArray(System.Runtime.InteropServices.GCHandle&,System.Byte[])
extern "C"  intptr_t UnityRemoteVideo_PinByteArray_m2804681258 (UnityRemoteVideo_t3391959971 * __this, GCHandle_t714486722 * ___handle0, ByteU5BU5D_t3287329517* ___array1, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// handle.Free ();
		GCHandle_t714486722 * L_0 = ___handle0;
		// handle.Free ();
		GCHandle_Free_m2887282835(L_0, /*hidden argument*/NULL);
		// handle = GCHandle.Alloc (array, GCHandleType.Pinned);
		GCHandle_t714486722 * L_1 = ___handle0;
		ByteU5BU5D_t3287329517* L_2 = ___array1;
		// handle = GCHandle.Alloc (array, GCHandleType.Pinned);
		GCHandle_t714486722  L_3 = GCHandle_Alloc_m4236757112(NULL /*static, unused*/, (RuntimeObject *)(RuntimeObject *)L_2, 3, /*hidden argument*/NULL);
		*(GCHandle_t714486722 *)L_1 = L_3;
		// return handle.AddrOfPinnedObject ();
		GCHandle_t714486722 * L_4 = ___handle0;
		// return handle.AddrOfPinnedObject ();
		intptr_t L_5 = GCHandle_AddrOfPinnedObject_m560236266(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		// }
		intptr_t L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::ByteArrayForFrame(System.Int32,System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_ByteArrayForFrame_m589747338 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, ByteU5BU5D_t3287329517* ___array01, ByteU5BU5D_t3287329517* ___array12, const RuntimeMethod* method)
{
	ByteU5BU5D_t3287329517* V_0 = NULL;
	ByteU5BU5D_t3287329517* G_B3_0 = NULL;
	{
		// return frame == 1 ? array1 : array0;
		int32_t L_0 = ___frame0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000e;
		}
	}
	{
		ByteU5BU5D_t3287329517* L_1 = ___array12;
		G_B3_0 = L_1;
		goto IL_000f;
	}

IL_000e:
	{
		ByteU5BU5D_t3287329517* L_2 = ___array01;
		G_B3_0 = L_2;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		// }
		ByteU5BU5D_t3287329517* L_3 = V_0;
		return L_3;
	}
}
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::YByteArrayForFrame(System.Int32)
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_YByteArrayForFrame_m189319441 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, const RuntimeMethod* method)
{
	ByteU5BU5D_t3287329517* V_0 = NULL;
	{
		// return ByteArrayForFrame (frame, m_textureYBytes, m_textureYBytes2);
		int32_t L_0 = ___frame0;
		ByteU5BU5D_t3287329517* L_1 = __this->get_m_textureYBytes_6();
		ByteU5BU5D_t3287329517* L_2 = __this->get_m_textureYBytes2_8();
		// return ByteArrayForFrame (frame, m_textureYBytes, m_textureYBytes2);
		ByteU5BU5D_t3287329517* L_3 = UnityRemoteVideo_ByteArrayForFrame_m589747338(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		ByteU5BU5D_t3287329517* L_4 = V_0;
		return L_4;
	}
}
// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::UVByteArrayForFrame(System.Int32)
extern "C"  ByteU5BU5D_t3287329517* UnityRemoteVideo_UVByteArrayForFrame_m955222255 (UnityRemoteVideo_t3391959971 * __this, int32_t ___frame0, const RuntimeMethod* method)
{
	ByteU5BU5D_t3287329517* V_0 = NULL;
	{
		// return ByteArrayForFrame (frame, m_textureUVBytes, m_textureUVBytes2);
		int32_t L_0 = ___frame0;
		ByteU5BU5D_t3287329517* L_1 = __this->get_m_textureUVBytes_7();
		ByteU5BU5D_t3287329517* L_2 = __this->get_m_textureUVBytes2_9();
		// return ByteArrayForFrame (frame, m_textureUVBytes, m_textureUVBytes2);
		ByteU5BU5D_t3287329517* L_3 = UnityRemoteVideo_ByteArrayForFrame_m589747338(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		ByteU5BU5D_t3287329517* L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::OnDestroy()
extern "C"  void UnityRemoteVideo_OnDestroy_m3971316263 (UnityRemoteVideo_t3391959971 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRemoteVideo_OnDestroy_m3971316263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Session.SetCapturePixelData (false, IntPtr.Zero, IntPtr.Zero);
		UnityARSessionNativeInterface_t4258909960 * L_0 = __this->get_m_Session_3();
		// m_Session.SetCapturePixelData (false, IntPtr.Zero, IntPtr.Zero);
		NullCheck(L_0);
		UnityARSessionNativeInterface_SetCapturePixelData_m2227686583(L_0, (bool)0, (intptr_t)(0), (intptr_t)(0), /*hidden argument*/NULL);
		// m_pinnedYArray.Free ();
		GCHandle_t714486722 * L_1 = __this->get_address_of_m_pinnedYArray_10();
		// m_pinnedYArray.Free ();
		GCHandle_Free_m2887282835(L_1, /*hidden argument*/NULL);
		// m_pinnedUVArray.Free ();
		GCHandle_t714486722 * L_2 = __this->get_address_of_m_pinnedUVArray_11();
		// m_pinnedUVArray.Free ();
		GCHandle_Free_m2887282835(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityRemoteVideo::OnPreRender()
extern "C"  void UnityRemoteVideo_OnPreRender_m1215563648 (UnityRemoteVideo_t3391959971 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRemoteVideo_OnPreRender_m1215563648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARTextureHandles_t1453164273  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t1041838152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// ARTextureHandles handles = m_Session.GetARVideoTextureHandles();
		UnityARSessionNativeInterface_t4258909960 * L_0 = __this->get_m_Session_3();
		// ARTextureHandles handles = m_Session.GetARVideoTextureHandles();
		NullCheck(L_0);
		ARTextureHandles_t1453164273  L_1 = UnityARSessionNativeInterface_GetARVideoTextureHandles_m1818998859(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		intptr_t L_2 = (&V_0)->get_textureY_0();
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		bool L_3 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		intptr_t L_4 = (&V_0)->get_textureCbCr_1();
		// if (handles.textureY == System.IntPtr.Zero || handles.textureCbCr == System.IntPtr.Zero)
		bool L_5 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_4, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}

IL_0039:
	{
		// return;
		goto IL_00dd;
	}

IL_003f:
	{
		// if (!bTexturesInitialized)
		bool L_6 = __this->get_bTexturesInitialized_4();
		if (L_6)
		{
			goto IL_004f;
		}
	}
	{
		// return;
		goto IL_00dd;
	}

IL_004f:
	{
		// currentFrameIndex = (currentFrameIndex + 1) % 2;
		int32_t L_7 = __this->get_currentFrameIndex_5();
		__this->set_currentFrameIndex_5(((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)1))%(int32_t)2)));
		// Resolution currentResolution = Screen.currentResolution;
		Resolution_t1041838152  L_8 = Screen_get_currentResolution_m205929278(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_8;
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		UnityARSessionNativeInterface_t4258909960 * L_9 = __this->get_m_Session_3();
		GCHandle_t714486722 * L_10 = __this->get_address_of_m_pinnedYArray_10();
		int32_t L_11 = __this->get_currentFrameIndex_5();
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		ByteU5BU5D_t3287329517* L_12 = UnityRemoteVideo_YByteArrayForFrame_m189319441(__this, L_11, /*hidden argument*/NULL);
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		intptr_t L_13 = UnityRemoteVideo_PinByteArray_m2804681258(__this, L_10, L_12, /*hidden argument*/NULL);
		GCHandle_t714486722 * L_14 = __this->get_address_of_m_pinnedUVArray_11();
		int32_t L_15 = __this->get_currentFrameIndex_5();
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		ByteU5BU5D_t3287329517* L_16 = UnityRemoteVideo_UVByteArrayForFrame_m955222255(__this, L_15, /*hidden argument*/NULL);
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		intptr_t L_17 = UnityRemoteVideo_PinByteArray_m2804681258(__this, L_14, L_16, /*hidden argument*/NULL);
		// m_Session.SetCapturePixelData (true, PinByteArray(ref m_pinnedYArray,YByteArrayForFrame(currentFrameIndex)), PinByteArray(ref m_pinnedUVArray,UVByteArrayForFrame(currentFrameIndex)));
		NullCheck(L_9);
		UnityARSessionNativeInterface_SetCapturePixelData_m2227686583(L_9, (bool)1, L_13, L_17, /*hidden argument*/NULL);
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureYMsgId, YByteArrayForFrame(1-currentFrameIndex));
		ConnectToEditor_t1050945949 * L_18 = __this->get_connectToEditor_2();
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureYMsgId, YByteArrayForFrame(1-currentFrameIndex));
		Guid_t  L_19 = ConnectionMessageIds_get_screenCaptureYMsgId_m3305274427(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_currentFrameIndex_5();
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureYMsgId, YByteArrayForFrame(1-currentFrameIndex));
		ByteU5BU5D_t3287329517* L_21 = UnityRemoteVideo_YByteArrayForFrame_m189319441(__this, ((int32_t)((int32_t)1-(int32_t)L_20)), /*hidden argument*/NULL);
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureYMsgId, YByteArrayForFrame(1-currentFrameIndex));
		NullCheck(L_18);
		ConnectToEditor_SendToEditor_m1297661023(L_18, L_19, L_21, /*hidden argument*/NULL);
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureUVMsgId, UVByteArrayForFrame(1-currentFrameIndex));
		ConnectToEditor_t1050945949 * L_22 = __this->get_connectToEditor_2();
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureUVMsgId, UVByteArrayForFrame(1-currentFrameIndex));
		Guid_t  L_23 = ConnectionMessageIds_get_screenCaptureUVMsgId_m3914639669(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_currentFrameIndex_5();
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureUVMsgId, UVByteArrayForFrame(1-currentFrameIndex));
		ByteU5BU5D_t3287329517* L_25 = UnityRemoteVideo_UVByteArrayForFrame_m955222255(__this, ((int32_t)((int32_t)1-(int32_t)L_24)), /*hidden argument*/NULL);
		// connectToEditor.SendToEditor (ConnectionMessageIds.screenCaptureUVMsgId, UVByteArrayForFrame(1-currentFrameIndex));
		NullCheck(L_22);
		ConnectToEditor_SendToEditor_m1297661023(L_22, L_23, L_25, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		// }
		return;
	}
}
// System.Void UnityPointCloudExample::.ctor()
extern "C"  void UnityPointCloudExample__ctor_m179955642 (UnityPointCloudExample_t859887967 * __this, const RuntimeMethod* method)
{
	{
		// public uint numPointsToShow = 100;
		__this->set_numPointsToShow_2(((int32_t)100));
		// public GameObject PointCloudPrefab = null;
		__this->set_PointCloudPrefab_3((GameObject_t1318052361 *)NULL);
		MonoBehaviour__ctor_m2458272640(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityPointCloudExample::Start()
extern "C"  void UnityPointCloudExample_Start_m4121028873 (UnityPointCloudExample_t859887967 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Start_m4121028873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdated;
		intptr_t L_0 = (intptr_t)UnityPointCloudExample_ARFrameUpdated_m2754349252_RuntimeMethod_var;
		ARFrameUpdate_t2274630099 * L_1 = (ARFrameUpdate_t2274630099 *)il2cpp_codegen_object_new(ARFrameUpdate_t2274630099_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1552988806(L_1, __this, L_0, /*hidden argument*/NULL);
		// UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdated;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t4258909960_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1052241241(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// if (PointCloudPrefab != null)
		GameObject_t1318052361 * L_2 = __this->get_PointCloudPrefab_3();
		// if (PointCloudPrefab != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_2, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0061;
		}
	}
	{
		// pointCloudObjects = new List<GameObject> ();
		// pointCloudObjects = new List<GameObject> ();
		List_1_t1128502775 * L_4 = (List_1_t1128502775 *)il2cpp_codegen_object_new(List_1_t1128502775_il2cpp_TypeInfo_var);
		List_1__ctor_m1148151875(L_4, /*hidden argument*/List_1__ctor_m1148151875_RuntimeMethod_var);
		__this->set_pointCloudObjects_4(L_4);
		// for (int i =0; i < numPointsToShow; i++)
		V_0 = 0;
		goto IL_0052;
	}

IL_0036:
	{
		// pointCloudObjects.Add (Instantiate (PointCloudPrefab));
		List_1_t1128502775 * L_5 = __this->get_pointCloudObjects_4();
		GameObject_t1318052361 * L_6 = __this->get_PointCloudPrefab_3();
		// pointCloudObjects.Add (Instantiate (PointCloudPrefab));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		GameObject_t1318052361 * L_7 = Object_Instantiate_TisGameObject_t1318052361_m3083066963(NULL /*static, unused*/, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1318052361_m3083066963_RuntimeMethod_var);
		// pointCloudObjects.Add (Instantiate (PointCloudPrefab));
		NullCheck(L_5);
		List_1_Add_m695416883(L_5, L_7, /*hidden argument*/List_1_Add_m695416883_RuntimeMethod_var);
		// for (int i =0; i < numPointsToShow; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0052:
	{
		// for (int i =0; i < numPointsToShow; i++)
		int32_t L_9 = V_0;
		uint32_t L_10 = __this->get_numPointsToShow_2();
		if ((((int64_t)(((int64_t)((int64_t)L_9)))) < ((int64_t)(((int64_t)((uint64_t)L_10))))))
		{
			goto IL_0036;
		}
	}
	{
	}

IL_0061:
	{
		// }
		return;
	}
}
// System.Void UnityPointCloudExample::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityPointCloudExample_ARFrameUpdated_m2754349252 (UnityPointCloudExample_t859887967 * __this, UnityARCamera_t3270530332  ___camera0, const RuntimeMethod* method)
{
	{
		// m_PointCloudData = camera.pointCloudData;
		Vector3U5BU5D_t974944492* L_0 = (&___camera0)->get_pointCloudData_7();
		__this->set_m_PointCloudData_5(L_0);
		// }
		return;
	}
}
// System.Void UnityPointCloudExample::Update()
extern "C"  void UnityPointCloudExample_Update_m3035936549 (UnityPointCloudExample_t859887967 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Update_m3035936549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1318052361 * V_2 = NULL;
	{
		// if (PointCloudPrefab != null && m_PointCloudData != null)
		GameObject_t1318052361 * L_0 = __this->get_PointCloudPrefab_3();
		// if (PointCloudPrefab != null && m_PointCloudData != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0091;
		}
	}
	{
		Vector3U5BU5D_t974944492* L_2 = __this->get_m_PointCloudData_5();
		if (!L_2)
		{
			goto IL_0091;
		}
	}
	{
		// for (int count = 0; count < Math.Min (m_PointCloudData.Length, numPointsToShow); count++)
		V_0 = 0;
		goto IL_0074;
	}

IL_0025:
	{
		// Vector4 vert = m_PointCloudData [count];
		Vector3U5BU5D_t974944492* L_3 = __this->get_m_PointCloudData_5();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		// Vector4 vert = m_PointCloudData [count];
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t380635127_il2cpp_TypeInfo_var);
		Vector4_t380635127  L_5 = Vector4_op_Implicit_m3043984250(NULL /*static, unused*/, (*(Vector3_t329709361 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), /*hidden argument*/NULL);
		V_1 = L_5;
		// GameObject point = pointCloudObjects [count];
		List_1_t1128502775 * L_6 = __this->get_pointCloudObjects_4();
		int32_t L_7 = V_0;
		// GameObject point = pointCloudObjects [count];
		NullCheck(L_6);
		GameObject_t1318052361 * L_8 = List_1_get_Item_m4071041560(L_6, L_7, /*hidden argument*/List_1_get_Item_m4071041560_RuntimeMethod_var);
		V_2 = L_8;
		// point.transform.position = new Vector3(vert.x, vert.y, vert.z);
		GameObject_t1318052361 * L_9 = V_2;
		// point.transform.position = new Vector3(vert.x, vert.y, vert.z);
		NullCheck(L_9);
		Transform_t532597831 * L_10 = GameObject_get_transform_m1187966899(L_9, /*hidden argument*/NULL);
		float L_11 = (&V_1)->get_x_1();
		float L_12 = (&V_1)->get_y_2();
		float L_13 = (&V_1)->get_z_3();
		// point.transform.position = new Vector3(vert.x, vert.y, vert.z);
		Vector3_t329709361  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3984700005((&L_14), L_11, L_12, L_13, /*hidden argument*/NULL);
		// point.transform.position = new Vector3(vert.x, vert.y, vert.z);
		NullCheck(L_10);
		Transform_set_position_m1768136472(L_10, L_14, /*hidden argument*/NULL);
		// for (int count = 0; count < Math.Min (m_PointCloudData.Length, numPointsToShow); count++)
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0074:
	{
		// for (int count = 0; count < Math.Min (m_PointCloudData.Length, numPointsToShow); count++)
		int32_t L_16 = V_0;
		Vector3U5BU5D_t974944492* L_17 = __this->get_m_PointCloudData_5();
		NullCheck(L_17);
		uint32_t L_18 = __this->get_numPointsToShow_2();
		// for (int count = 0; count < Math.Min (m_PointCloudData.Length, numPointsToShow); count++)
		int64_t L_19 = Math_Min_m3765675025(NULL /*static, unused*/, (((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length))))))), (((int64_t)((uint64_t)L_18))), /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_16)))) < ((int64_t)L_19)))
		{
			goto IL_0025;
		}
	}
	{
	}

IL_0091:
	{
		// }
		return;
	}
}
// System.Byte[] Utils.ObjectSerializationExtension::SerializeToByteArray(System.Object)
extern "C"  ByteU5BU5D_t3287329517* ObjectSerializationExtension_SerializeToByteArray_m3367459946 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectSerializationExtension_SerializeToByteArray_m3367459946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3287329517* V_0 = NULL;
	BinaryFormatter_t3541706630 * V_1 = NULL;
	MemoryStream_t3384139290 * V_2 = NULL;
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (obj == null)
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// return null;
		V_0 = (ByteU5BU5D_t3287329517*)NULL;
		goto IL_003d;
	}

IL_000f:
	{
		// var bf = new BinaryFormatter();
		BinaryFormatter_t3541706630 * L_1 = (BinaryFormatter_t3541706630 *)il2cpp_codegen_object_new(BinaryFormatter_t3541706630_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m2525162057(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		// using (var ms = new MemoryStream())
		MemoryStream_t3384139290 * L_2 = (MemoryStream_t3384139290 *)il2cpp_codegen_object_new(MemoryStream_t3384139290_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m298230343(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		// bf.Serialize(ms, obj);
		BinaryFormatter_t3541706630 * L_3 = V_1;
		MemoryStream_t3384139290 * L_4 = V_2;
		RuntimeObject * L_5 = ___obj0;
		// bf.Serialize(ms, obj);
		NullCheck(L_3);
		BinaryFormatter_Serialize_m1779404934(L_3, L_4, L_5, /*hidden argument*/NULL);
		// return ms.ToArray();
		MemoryStream_t3384139290 * L_6 = V_2;
		// return ms.ToArray();
		NullCheck(L_6);
		ByteU5BU5D_t3287329517* L_7 = VirtFuncInvoker0< ByteU5BU5D_t3287329517* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_6);
		V_0 = L_7;
		IL2CPP_LEAVE(0x3D, FINALLY_0030);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t3384139290 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			MemoryStream_t3384139290 * L_9 = V_2;
			// using (var ms = new MemoryStream())
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3363289168_il2cpp_TypeInfo_var, L_9);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_003d:
	{
		// }
		ByteU5BU5D_t3287329517* L_10 = V_0;
		return L_10;
	}
}
// System.Void Utils.serializableARKitInit::.ctor(Utils.serializableARSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void serializableARKitInit__ctor_m1036210106 (serializableARKitInit_t403564667 * __this, serializableARSessionConfiguration_t3208486129 * ___cfg0, int32_t ___option1, const RuntimeMethod* method)
{
	{
		// public serializableARKitInit(serializableARSessionConfiguration cfg, UnityARSessionRunOption option)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// config = cfg;
		serializableARSessionConfiguration_t3208486129 * L_0 = ___cfg0;
		__this->set_config_0(L_0);
		// runOption = option;
		int32_t L_1 = ___option1;
		__this->set_runOption_1(L_1);
		// }
		return;
	}
}
// System.Void Utils.serializableARSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void serializableARSessionConfiguration__ctor_m2691530054 (serializableARSessionConfiguration_t3208486129 * __this, int32_t ___align0, int32_t ___planeDet1, bool ___getPtCloud2, bool ___enableLightEst3, const RuntimeMethod* method)
{
	{
		// public serializableARSessionConfiguration(UnityARAlignment align, UnityARPlaneDetection planeDet, bool getPtCloud, bool enableLightEst)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// alignment = align;
		int32_t L_0 = ___align0;
		__this->set_alignment_0(L_0);
		// planeDetection = planeDet;
		int32_t L_1 = ___planeDet1;
		__this->set_planeDetection_1(L_1);
		// getPointCloudData = getPtCloud;
		bool L_2 = ___getPtCloud2;
		__this->set_getPointCloudData_2(L_2);
		// enableLightEstimation = enableLightEst;
		bool L_3 = ___enableLightEst3;
		__this->set_enableLightEstimation_3(L_3);
		// }
		return;
	}
}
// Utils.serializableARSessionConfiguration Utils.serializableARSessionConfiguration::op_Implicit(UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration)
extern "C"  serializableARSessionConfiguration_t3208486129 * serializableARSessionConfiguration_op_Implicit_m343128120 (RuntimeObject * __this /* static, unused */, ARKitWorldTrackingSessionConfiguration_t4171655616  ___awtsc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableARSessionConfiguration_op_Implicit_m343128120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableARSessionConfiguration_t3208486129 * V_0 = NULL;
	{
		// return new serializableARSessionConfiguration (awtsc.alignment, awtsc.planeDetection, awtsc.getPointCloudData, awtsc.enableLightEstimation);
		int32_t L_0 = (&___awtsc0)->get_alignment_0();
		int32_t L_1 = (&___awtsc0)->get_planeDetection_1();
		bool L_2 = (&___awtsc0)->get_getPointCloudData_2();
		bool L_3 = (&___awtsc0)->get_enableLightEstimation_3();
		// return new serializableARSessionConfiguration (awtsc.alignment, awtsc.planeDetection, awtsc.getPointCloudData, awtsc.enableLightEstimation);
		serializableARSessionConfiguration_t3208486129 * L_4 = (serializableARSessionConfiguration_t3208486129 *)il2cpp_codegen_object_new(serializableARSessionConfiguration_t3208486129_il2cpp_TypeInfo_var);
		serializableARSessionConfiguration__ctor_m2691530054(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		serializableARSessionConfiguration_t3208486129 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration Utils.serializableARSessionConfiguration::op_Implicit(Utils.serializableARSessionConfiguration)
extern "C"  ARKitWorldTrackingSessionConfiguration_t4171655616  serializableARSessionConfiguration_op_Implicit_m4044503825 (RuntimeObject * __this /* static, unused */, serializableARSessionConfiguration_t3208486129 * ___sasc0, const RuntimeMethod* method)
{
	ARKitWorldTrackingSessionConfiguration_t4171655616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new ARKitWorldTrackingSessionConfiguration (sasc.alignment, sasc.planeDetection, sasc.getPointCloudData, sasc.enableLightEstimation);
		serializableARSessionConfiguration_t3208486129 * L_0 = ___sasc0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_alignment_0();
		serializableARSessionConfiguration_t3208486129 * L_2 = ___sasc0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_planeDetection_1();
		serializableARSessionConfiguration_t3208486129 * L_4 = ___sasc0;
		NullCheck(L_4);
		bool L_5 = L_4->get_getPointCloudData_2();
		serializableARSessionConfiguration_t3208486129 * L_6 = ___sasc0;
		NullCheck(L_6);
		bool L_7 = L_6->get_enableLightEstimation_3();
		// return new ARKitWorldTrackingSessionConfiguration (sasc.alignment, sasc.planeDetection, sasc.getPointCloudData, sasc.enableLightEstimation);
		ARKitWorldTrackingSessionConfiguration_t4171655616  L_8;
		memset(&L_8, 0, sizeof(L_8));
		ARKitWorldTrackingSessionConfiguration__ctor_m4004678005((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0024;
	}

IL_0024:
	{
		// }
		ARKitWorldTrackingSessionConfiguration_t4171655616  L_9 = V_0;
		return L_9;
	}
}
// System.Void Utils.serializableFromEditorMessage::.ctor()
extern "C"  void serializableFromEditorMessage__ctor_m3241550520 (serializableFromEditorMessage_t1383667999 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils.serializablePointCloud::.ctor(System.Byte[])
extern "C"  void serializablePointCloud__ctor_m2980893286 (serializablePointCloud_t2991476747 * __this, ByteU5BU5D_t3287329517* ___inputPoints0, const RuntimeMethod* method)
{
	{
		// public serializablePointCloud(byte [] inputPoints)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// pointCloudData = inputPoints;
		ByteU5BU5D_t3287329517* L_0 = ___inputPoints0;
		__this->set_pointCloudData_0(L_0);
		// }
		return;
	}
}
// Utils.serializablePointCloud Utils.serializablePointCloud::op_Implicit(UnityEngine.Vector3[])
extern "C"  serializablePointCloud_t2991476747 * serializablePointCloud_op_Implicit_m4280869743 (RuntimeObject * __this /* static, unused */, Vector3U5BU5D_t974944492* ___vecPointCloud0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializablePointCloud_op_Implicit_m4280869743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3287329517* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	serializablePointCloud_t2991476747 * V_3 = NULL;
	{
		// if (vecPointCloud != null)
		Vector3U5BU5D_t974944492* L_0 = ___vecPointCloud0;
		if (!L_0)
		{
			goto IL_0093;
		}
	}
	{
		// byte [] createBuf = new byte[vecPointCloud.Length * sizeof(float) * 3];
		Vector3U5BU5D_t974944492* L_1 = ___vecPointCloud0;
		NullCheck(L_1);
		V_0 = ((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))*(int32_t)4))*(int32_t)3))));
		// for(int i = 0; i < vecPointCloud.Length; i++)
		V_1 = 0;
		goto IL_007e;
	}

IL_001c:
	{
		// int bufferStart = i * 3;
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((int32_t)L_2*(int32_t)3));
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t974944492* L_3 = ___vecPointCloud0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		float L_5 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))->get_x_1();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1229281639_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3287329517* L_6 = BitConverter_GetBytes_m2229711275(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_7 = V_0;
		int32_t L_8 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].x ), 0, createBuf, (bufferStart)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m2996674574(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, 0, (RuntimeArray *)(RuntimeArray *)L_7, ((int32_t)((int32_t)L_8*(int32_t)4)), 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t974944492* L_9 = ___vecPointCloud0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		float L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_y_2();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		ByteU5BU5D_t3287329517* L_12 = BitConverter_GetBytes_m2229711275(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_13 = V_0;
		int32_t L_14 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].y ), 0, createBuf, (bufferStart+1)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m2996674574(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_12, 0, (RuntimeArray *)(RuntimeArray *)L_13, ((int32_t)((int32_t)((int32_t)((int32_t)L_14+(int32_t)1))*(int32_t)4)), 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		Vector3U5BU5D_t974944492* L_15 = ___vecPointCloud0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		float L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_z_3();
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		ByteU5BU5D_t3287329517* L_18 = BitConverter_GetBytes_m2229711275(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_19 = V_0;
		int32_t L_20 = V_2;
		// Buffer.BlockCopy( BitConverter.GetBytes( vecPointCloud[i].z ), 0, createBuf, (bufferStart+2)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m2996674574(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_18, 0, (RuntimeArray *)(RuntimeArray *)L_19, ((int32_t)((int32_t)((int32_t)((int32_t)L_20+(int32_t)2))*(int32_t)4)), 4, /*hidden argument*/NULL);
		// for(int i = 0; i < vecPointCloud.Length; i++)
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_007e:
	{
		// for(int i = 0; i < vecPointCloud.Length; i++)
		int32_t L_22 = V_1;
		Vector3U5BU5D_t974944492* L_23 = ___vecPointCloud0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		// return new serializablePointCloud (createBuf);
		ByteU5BU5D_t3287329517* L_24 = V_0;
		// return new serializablePointCloud (createBuf);
		serializablePointCloud_t2991476747 * L_25 = (serializablePointCloud_t2991476747 *)il2cpp_codegen_object_new(serializablePointCloud_t2991476747_il2cpp_TypeInfo_var);
		serializablePointCloud__ctor_m2980893286(L_25, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		goto IL_00a0;
	}

IL_0093:
	{
		// return new serializablePointCloud(null);
		// return new serializablePointCloud(null);
		serializablePointCloud_t2991476747 * L_26 = (serializablePointCloud_t2991476747 *)il2cpp_codegen_object_new(serializablePointCloud_t2991476747_il2cpp_TypeInfo_var);
		serializablePointCloud__ctor_m2980893286(L_26, (ByteU5BU5D_t3287329517*)(ByteU5BU5D_t3287329517*)NULL, /*hidden argument*/NULL);
		V_3 = L_26;
		goto IL_00a0;
	}

IL_00a0:
	{
		// }
		serializablePointCloud_t2991476747 * L_27 = V_3;
		return L_27;
	}
}
// UnityEngine.Vector3[] Utils.serializablePointCloud::op_Implicit(Utils.serializablePointCloud)
extern "C"  Vector3U5BU5D_t974944492* serializablePointCloud_op_Implicit_m3149335011 (RuntimeObject * __this /* static, unused */, serializablePointCloud_t2991476747 * ___spc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializablePointCloud_op_Implicit_m3149335011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3U5BU5D_t974944492* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3U5BU5D_t974944492* V_4 = NULL;
	{
		// if (spc.pointCloudData != null)
		serializablePointCloud_t2991476747 * L_0 = ___spc0;
		NullCheck(L_0);
		ByteU5BU5D_t3287329517* L_1 = L_0->get_pointCloudData_0();
		if (!L_1)
		{
			goto IL_0092;
		}
	}
	{
		// int numVectors = spc.pointCloudData.Length / (3 * sizeof(float));
		serializablePointCloud_t2991476747 * L_2 = ___spc0;
		NullCheck(L_2);
		ByteU5BU5D_t3287329517* L_3 = L_2->get_pointCloudData_0();
		NullCheck(L_3);
		V_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))/(int32_t)((int32_t)12)));
		// Vector3 [] pointCloudVec = new Vector3[numVectors];
		int32_t L_4 = V_0;
		V_1 = ((Vector3U5BU5D_t974944492*)SZArrayNew(Vector3U5BU5D_t974944492_il2cpp_TypeInfo_var, (uint32_t)L_4));
		// for (int i = 0; i < numVectors; i++)
		V_2 = 0;
		goto IL_0083;
	}

IL_0027:
	{
		// int bufferStart = i * 3;
		int32_t L_5 = V_2;
		V_3 = ((int32_t)((int32_t)L_5*(int32_t)3));
		// pointCloudVec [i].x = BitConverter.ToSingle (spc.pointCloudData, (bufferStart) * sizeof(float));
		Vector3U5BU5D_t974944492* L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		serializablePointCloud_t2991476747 * L_8 = ___spc0;
		NullCheck(L_8);
		ByteU5BU5D_t3287329517* L_9 = L_8->get_pointCloudData_0();
		int32_t L_10 = V_3;
		// pointCloudVec [i].x = BitConverter.ToSingle (spc.pointCloudData, (bufferStart) * sizeof(float));
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1229281639_il2cpp_TypeInfo_var);
		float L_11 = BitConverter_ToSingle_m3546845596(NULL /*static, unused*/, L_9, ((int32_t)((int32_t)L_10*(int32_t)4)), /*hidden argument*/NULL);
		((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->set_x_1(L_11);
		// pointCloudVec [i].y = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+1) * sizeof(float));
		Vector3U5BU5D_t974944492* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		serializablePointCloud_t2991476747 * L_14 = ___spc0;
		NullCheck(L_14);
		ByteU5BU5D_t3287329517* L_15 = L_14->get_pointCloudData_0();
		int32_t L_16 = V_3;
		// pointCloudVec [i].y = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+1) * sizeof(float));
		float L_17 = BitConverter_ToSingle_m3546845596(NULL /*static, unused*/, L_15, ((int32_t)((int32_t)((int32_t)((int32_t)L_16+(int32_t)1))*(int32_t)4)), /*hidden argument*/NULL);
		((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->set_y_2(L_17);
		// pointCloudVec [i].z = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+2) * sizeof(float));
		Vector3U5BU5D_t974944492* L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		serializablePointCloud_t2991476747 * L_20 = ___spc0;
		NullCheck(L_20);
		ByteU5BU5D_t3287329517* L_21 = L_20->get_pointCloudData_0();
		int32_t L_22 = V_3;
		// pointCloudVec [i].z = BitConverter.ToSingle (spc.pointCloudData, (bufferStart+2) * sizeof(float));
		float L_23 = BitConverter_ToSingle_m3546845596(NULL /*static, unused*/, L_21, ((int32_t)((int32_t)((int32_t)((int32_t)L_22+(int32_t)2))*(int32_t)4)), /*hidden argument*/NULL);
		((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->set_z_3(L_23);
		// for (int i = 0; i < numVectors; i++)
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0083:
	{
		// for (int i = 0; i < numVectors; i++)
		int32_t L_25 = V_2;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0027;
		}
	}
	{
		// return pointCloudVec;
		Vector3U5BU5D_t974944492* L_27 = V_1;
		V_4 = L_27;
		goto IL_009b;
	}

IL_0092:
	{
		// return null;
		V_4 = (Vector3U5BU5D_t974944492*)NULL;
		goto IL_009b;
	}

IL_009b:
	{
		// }
		Vector3U5BU5D_t974944492* L_28 = V_4;
		return L_28;
	}
}
// System.Void Utils.serializableSHC::.ctor(System.Byte[])
extern "C"  void serializableSHC__ctor_m3060006280 (serializableSHC_t2973324561 * __this, ByteU5BU5D_t3287329517* ___inputSHCData0, const RuntimeMethod* method)
{
	{
		// public serializableSHC(byte [] inputSHCData)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// shcData = inputSHCData;
		ByteU5BU5D_t3287329517* L_0 = ___inputSHCData0;
		__this->set_shcData_0(L_0);
		// }
		return;
	}
}
// Utils.serializableSHC Utils.serializableSHC::op_Implicit(System.Single[])
extern "C"  serializableSHC_t2973324561 * serializableSHC_op_Implicit_m2702198786 (RuntimeObject * __this /* static, unused */, SingleU5BU5D_t2905636975* ___floatsSHC0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableSHC_op_Implicit_m2702198786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3287329517* V_0 = NULL;
	int32_t V_1 = 0;
	serializableSHC_t2973324561 * V_2 = NULL;
	{
		// if (floatsSHC != null)
		SingleU5BU5D_t2905636975* L_0 = ___floatsSHC0;
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		// byte [] createBuf = new byte[floatsSHC.Length * sizeof(float)];
		SingleU5BU5D_t2905636975* L_1 = ___floatsSHC0;
		NullCheck(L_1);
		V_0 = ((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))*(int32_t)4))));
		// for(int i = 0; i < floatsSHC.Length; i++)
		V_1 = 0;
		goto IL_0033;
	}

IL_001a:
	{
		// Buffer.BlockCopy( BitConverter.GetBytes( floatsSHC[i] ), 0, createBuf, (i)*sizeof(float), sizeof(float) );
		SingleU5BU5D_t2905636975* L_2 = ___floatsSHC0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		float L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		// Buffer.BlockCopy( BitConverter.GetBytes( floatsSHC[i] ), 0, createBuf, (i)*sizeof(float), sizeof(float) );
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1229281639_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3287329517* L_6 = BitConverter_GetBytes_m2229711275(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_7 = V_0;
		int32_t L_8 = V_1;
		// Buffer.BlockCopy( BitConverter.GetBytes( floatsSHC[i] ), 0, createBuf, (i)*sizeof(float), sizeof(float) );
		Buffer_BlockCopy_m2996674574(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, 0, (RuntimeArray *)(RuntimeArray *)L_7, ((int32_t)((int32_t)L_8*(int32_t)4)), 4, /*hidden argument*/NULL);
		// for(int i = 0; i < floatsSHC.Length; i++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0033:
	{
		// for(int i = 0; i < floatsSHC.Length; i++)
		int32_t L_10 = V_1;
		SingleU5BU5D_t2905636975* L_11 = ___floatsSHC0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		// return new serializableSHC (createBuf);
		ByteU5BU5D_t3287329517* L_12 = V_0;
		// return new serializableSHC (createBuf);
		serializableSHC_t2973324561 * L_13 = (serializableSHC_t2973324561 *)il2cpp_codegen_object_new(serializableSHC_t2973324561_il2cpp_TypeInfo_var);
		serializableSHC__ctor_m3060006280(L_13, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		goto IL_0055;
	}

IL_0048:
	{
		// return new serializableSHC(null);
		// return new serializableSHC(null);
		serializableSHC_t2973324561 * L_14 = (serializableSHC_t2973324561 *)il2cpp_codegen_object_new(serializableSHC_t2973324561_il2cpp_TypeInfo_var);
		serializableSHC__ctor_m3060006280(L_14, (ByteU5BU5D_t3287329517*)(ByteU5BU5D_t3287329517*)NULL, /*hidden argument*/NULL);
		V_2 = L_14;
		goto IL_0055;
	}

IL_0055:
	{
		// }
		serializableSHC_t2973324561 * L_15 = V_2;
		return L_15;
	}
}
// System.Single[] Utils.serializableSHC::op_Implicit(Utils.serializableSHC)
extern "C"  SingleU5BU5D_t2905636975* serializableSHC_op_Implicit_m4253342705 (RuntimeObject * __this /* static, unused */, serializableSHC_t2973324561 * ___spc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableSHC_op_Implicit_m4253342705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SingleU5BU5D_t2905636975* V_1 = NULL;
	int32_t V_2 = 0;
	SingleU5BU5D_t2905636975* V_3 = NULL;
	{
		// if (spc.shcData != null)
		serializableSHC_t2973324561 * L_0 = ___spc0;
		NullCheck(L_0);
		ByteU5BU5D_t3287329517* L_1 = L_0->get_shcData_0();
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		// int numFloats = spc.shcData.Length / (sizeof(float));
		serializableSHC_t2973324561 * L_2 = ___spc0;
		NullCheck(L_2);
		ByteU5BU5D_t3287329517* L_3 = L_2->get_shcData_0();
		NullCheck(L_3);
		V_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))/(int32_t)4));
		// float [] shcFloats = new float[numFloats];
		int32_t L_4 = V_0;
		V_1 = ((SingleU5BU5D_t2905636975*)SZArrayNew(SingleU5BU5D_t2905636975_il2cpp_TypeInfo_var, (uint32_t)L_4));
		// for (int i = 0; i < numFloats; i++)
		V_2 = 0;
		goto IL_003d;
	}

IL_0026:
	{
		// shcFloats [i] = BitConverter.ToSingle (spc.shcData, i * sizeof(float));
		SingleU5BU5D_t2905636975* L_5 = V_1;
		int32_t L_6 = V_2;
		serializableSHC_t2973324561 * L_7 = ___spc0;
		NullCheck(L_7);
		ByteU5BU5D_t3287329517* L_8 = L_7->get_shcData_0();
		int32_t L_9 = V_2;
		// shcFloats [i] = BitConverter.ToSingle (spc.shcData, i * sizeof(float));
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1229281639_il2cpp_TypeInfo_var);
		float L_10 = BitConverter_ToSingle_m3546845596(NULL /*static, unused*/, L_8, ((int32_t)((int32_t)L_9*(int32_t)4)), /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_10);
		// for (int i = 0; i < numFloats; i++)
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003d:
	{
		// for (int i = 0; i < numFloats; i++)
		int32_t L_12 = V_2;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// return shcFloats;
		SingleU5BU5D_t2905636975* L_14 = V_1;
		V_3 = L_14;
		goto IL_0053;
	}

IL_004b:
	{
		// return null;
		V_3 = (SingleU5BU5D_t2905636975*)NULL;
		goto IL_0053;
	}

IL_0053:
	{
		// }
		SingleU5BU5D_t2905636975* L_15 = V_3;
		return L_15;
	}
}
// System.Void Utils.serializableUnityARCamera::.ctor(Utils.serializableUnityARMatrix4x4,Utils.serializableUnityARMatrix4x4,UnityEngine.XR.iOS.ARTrackingState,UnityEngine.XR.iOS.ARTrackingStateReason,UnityEngine.XR.iOS.UnityVideoParams,UnityEngine.XR.iOS.UnityARLightData,Utils.serializableUnityARMatrix4x4,Utils.serializablePointCloud)
extern "C"  void serializableUnityARCamera__ctor_m1894601043 (serializableUnityARCamera_t3181320657 * __this, serializableUnityARMatrix4x4_t3548294260 * ___wt0, serializableUnityARMatrix4x4_t3548294260 * ___pm1, int32_t ___ats2, int32_t ___atsr3, UnityVideoParams_t909694111  ___uvp4, UnityARLightData_t3404656299  ___lightDat5, serializableUnityARMatrix4x4_t3548294260 * ___dt6, serializablePointCloud_t2991476747 * ___spc7, const RuntimeMethod* method)
{
	{
		// public serializableUnityARCamera( serializableUnityARMatrix4x4 wt, serializableUnityARMatrix4x4 pm, ARTrackingState ats, ARTrackingStateReason atsr, UnityVideoParams uvp, UnityARLightData lightDat, serializableUnityARMatrix4x4 dt, serializablePointCloud spc)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// worldTransform = wt;
		serializableUnityARMatrix4x4_t3548294260 * L_0 = ___wt0;
		__this->set_worldTransform_0(L_0);
		// projectionMatrix = pm;
		serializableUnityARMatrix4x4_t3548294260 * L_1 = ___pm1;
		__this->set_projectionMatrix_1(L_1);
		// trackingState = ats;
		int32_t L_2 = ___ats2;
		__this->set_trackingState_2(L_2);
		// trackingReason = atsr;
		int32_t L_3 = ___atsr3;
		__this->set_trackingReason_3(L_3);
		// videoParams = uvp;
		UnityVideoParams_t909694111  L_4 = ___uvp4;
		__this->set_videoParams_4(L_4);
		// lightData = lightDat;
		UnityARLightData_t3404656299  L_5 = ___lightDat5;
		// lightData = lightDat;
		serializableUnityARLightData_t1864664250 * L_6 = serializableUnityARLightData_op_Implicit_m1903111810(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_lightData_5(L_6);
		// displayTransform = dt;
		serializableUnityARMatrix4x4_t3548294260 * L_7 = ___dt6;
		__this->set_displayTransform_7(L_7);
		// pointCloud = spc;
		serializablePointCloud_t2991476747 * L_8 = ___spc7;
		__this->set_pointCloud_6(L_8);
		// }
		return;
	}
}
// Utils.serializableUnityARCamera Utils.serializableUnityARCamera::op_Implicit(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  serializableUnityARCamera_t3181320657 * serializableUnityARCamera_op_Implicit_m3148128646 (RuntimeObject * __this /* static, unused */, UnityARCamera_t3270530332  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARCamera_op_Implicit_m3148128646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableUnityARCamera_t3181320657 * V_0 = NULL;
	{
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		UnityARMatrix4x4_t758723042  L_0 = (&___rValue0)->get_worldTransform_0();
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		serializableUnityARMatrix4x4_t3548294260 * L_1 = serializableUnityARMatrix4x4_op_Implicit_m1426572080(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityARMatrix4x4_t758723042  L_2 = (&___rValue0)->get_projectionMatrix_1();
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		serializableUnityARMatrix4x4_t3548294260 * L_3 = serializableUnityARMatrix4x4_op_Implicit_m1426572080(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = (&___rValue0)->get_trackingState_2();
		int32_t L_5 = (&___rValue0)->get_trackingReason_3();
		UnityVideoParams_t909694111  L_6 = (&___rValue0)->get_videoParams_4();
		UnityARLightData_t3404656299  L_7 = (&___rValue0)->get_lightData_5();
		UnityARMatrix4x4_t758723042  L_8 = (&___rValue0)->get_displayTransform_6();
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		serializableUnityARMatrix4x4_t3548294260 * L_9 = serializableUnityARMatrix4x4_op_Implicit_m1426572080(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Vector3U5BU5D_t974944492* L_10 = (&___rValue0)->get_pointCloudData_7();
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		serializablePointCloud_t2991476747 * L_11 = serializablePointCloud_op_Implicit_m4280869743(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		// return new serializableUnityARCamera(rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloudData);
		serializableUnityARCamera_t3181320657 * L_12 = (serializableUnityARCamera_t3181320657 *)il2cpp_codegen_object_new(serializableUnityARCamera_t3181320657_il2cpp_TypeInfo_var);
		serializableUnityARCamera__ctor_m1894601043(L_12, L_1, L_3, L_4, L_5, L_6, L_7, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0058;
	}

IL_0058:
	{
		// }
		serializableUnityARCamera_t3181320657 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.XR.iOS.UnityARCamera Utils.serializableUnityARCamera::op_Implicit(Utils.serializableUnityARCamera)
extern "C"  UnityARCamera_t3270530332  serializableUnityARCamera_op_Implicit_m1548072201 (RuntimeObject * __this /* static, unused */, serializableUnityARCamera_t3181320657 * ___rValue0, const RuntimeMethod* method)
{
	UnityARCamera_t3270530332  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		serializableUnityARCamera_t3181320657 * L_0 = ___rValue0;
		NullCheck(L_0);
		serializableUnityARMatrix4x4_t3548294260 * L_1 = L_0->get_worldTransform_0();
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		UnityARMatrix4x4_t758723042  L_2 = serializableUnityARMatrix4x4_op_Implicit_m2448964739(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		serializableUnityARCamera_t3181320657 * L_3 = ___rValue0;
		NullCheck(L_3);
		serializableUnityARMatrix4x4_t3548294260 * L_4 = L_3->get_projectionMatrix_1();
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		UnityARMatrix4x4_t758723042  L_5 = serializableUnityARMatrix4x4_op_Implicit_m2448964739(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		serializableUnityARCamera_t3181320657 * L_6 = ___rValue0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_trackingState_2();
		serializableUnityARCamera_t3181320657 * L_8 = ___rValue0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_trackingReason_3();
		serializableUnityARCamera_t3181320657 * L_10 = ___rValue0;
		NullCheck(L_10);
		UnityVideoParams_t909694111  L_11 = L_10->get_videoParams_4();
		serializableUnityARCamera_t3181320657 * L_12 = ___rValue0;
		NullCheck(L_12);
		serializableUnityARLightData_t1864664250 * L_13 = L_12->get_lightData_5();
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		UnityARLightData_t3404656299  L_14 = serializableUnityARLightData_op_Implicit_m322329394(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		serializableUnityARCamera_t3181320657 * L_15 = ___rValue0;
		NullCheck(L_15);
		serializableUnityARMatrix4x4_t3548294260 * L_16 = L_15->get_displayTransform_7();
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		UnityARMatrix4x4_t758723042  L_17 = serializableUnityARMatrix4x4_op_Implicit_m2448964739(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		serializableUnityARCamera_t3181320657 * L_18 = ___rValue0;
		NullCheck(L_18);
		serializablePointCloud_t2991476747 * L_19 = L_18->get_pointCloud_6();
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		Vector3U5BU5D_t974944492* L_20 = serializablePointCloud_op_Implicit_m3149335011(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		// return new UnityARCamera (rValue.worldTransform, rValue.projectionMatrix, rValue.trackingState, rValue.trackingReason, rValue.videoParams, rValue.lightData, rValue.displayTransform, rValue.pointCloud);
		UnityARCamera_t3270530332  L_21;
		memset(&L_21, 0, sizeof(L_21));
		UnityARCamera__ctor_m1980968502((&L_21), L_2, L_5, L_7, L_9, L_11, L_14, L_17, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_0055;
	}

IL_0055:
	{
		// }
		UnityARCamera_t3270530332  L_22 = V_0;
		return L_22;
	}
}
// System.Void Utils.serializableUnityARLightData::.ctor(UnityEngine.XR.iOS.UnityARLightData)
extern "C"  void serializableUnityARLightData__ctor_m2325970984 (serializableUnityARLightData_t1864664250 * __this, UnityARLightData_t3404656299  ___lightData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARLightData__ctor_m2325970984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// serializableUnityARLightData(UnityARLightData lightData)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// whichLight = lightData.arLightingType;
		int32_t L_0 = (&___lightData0)->get_arLightingType_0();
		__this->set_whichLight_0(L_0);
		// if (whichLight == LightDataType.DirectionalLightEstimate) {
		int32_t L_1 = __this->get_whichLight_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0079;
		}
	}
	{
		// lightSHC = lightData.arDirectonalLightEstimate.sphericalHarmonicsCoefficients;
		UnityARDirectionalLightEstimate_t3433403765 * L_2 = (&___lightData0)->get_arDirectonalLightEstimate_2();
		NullCheck(L_2);
		SingleU5BU5D_t2905636975* L_3 = L_2->get_sphericalHarmonicsCoefficients_2();
		// lightSHC = lightData.arDirectonalLightEstimate.sphericalHarmonicsCoefficients;
		serializableSHC_t2973324561 * L_4 = serializableSHC_op_Implicit_m2702198786(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_lightSHC_1(L_4);
		// Vector3 lightDir = lightData.arDirectonalLightEstimate.primaryLightDirection;
		UnityARDirectionalLightEstimate_t3433403765 * L_5 = (&___lightData0)->get_arDirectonalLightEstimate_2();
		NullCheck(L_5);
		Vector3_t329709361  L_6 = L_5->get_primaryLightDirection_0();
		V_0 = L_6;
		// float lightIntensity = lightData.arDirectonalLightEstimate.primaryLightIntensity;
		UnityARDirectionalLightEstimate_t3433403765 * L_7 = (&___lightData0)->get_arDirectonalLightEstimate_2();
		NullCheck(L_7);
		float L_8 = L_7->get_primaryLightIntensity_1();
		V_1 = L_8;
		// primaryLightDirAndIntensity = new SerializableVector4 (lightDir.x, lightDir.y, lightDir.z, lightIntensity);
		float L_9 = (&V_0)->get_x_1();
		float L_10 = (&V_0)->get_y_2();
		float L_11 = (&V_0)->get_z_3();
		float L_12 = V_1;
		// primaryLightDirAndIntensity = new SerializableVector4 (lightDir.x, lightDir.y, lightDir.z, lightIntensity);
		SerializableVector4_t3177127415 * L_13 = (SerializableVector4_t3177127415 *)il2cpp_codegen_object_new(SerializableVector4_t3177127415_il2cpp_TypeInfo_var);
		SerializableVector4__ctor_m1389529421(L_13, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		__this->set_primaryLightDirAndIntensity_2(L_13);
		goto IL_009f;
	}

IL_0079:
	{
		// ambientIntensity = lightData.arLightEstimate.ambientIntensity;
		UnityARLightEstimate_t2392533559 * L_14 = (&___lightData0)->get_address_of_arLightEstimate_1();
		float L_15 = L_14->get_ambientIntensity_0();
		__this->set_ambientIntensity_3(L_15);
		// ambientColorTemperature = lightData.arLightEstimate.ambientColorTemperature;
		UnityARLightEstimate_t2392533559 * L_16 = (&___lightData0)->get_address_of_arLightEstimate_1();
		float L_17 = L_16->get_ambientColorTemperature_1();
		__this->set_ambientColorTemperature_4(L_17);
	}

IL_009f:
	{
		// }
		return;
	}
}
// Utils.serializableUnityARLightData Utils.serializableUnityARLightData::op_Implicit(UnityEngine.XR.iOS.UnityARLightData)
extern "C"  serializableUnityARLightData_t1864664250 * serializableUnityARLightData_op_Implicit_m1903111810 (RuntimeObject * __this /* static, unused */, UnityARLightData_t3404656299  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARLightData_op_Implicit_m1903111810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableUnityARLightData_t1864664250 * V_0 = NULL;
	{
		// return new serializableUnityARLightData(rValue);
		UnityARLightData_t3404656299  L_0 = ___rValue0;
		// return new serializableUnityARLightData(rValue);
		serializableUnityARLightData_t1864664250 * L_1 = (serializableUnityARLightData_t1864664250 *)il2cpp_codegen_object_new(serializableUnityARLightData_t1864664250_il2cpp_TypeInfo_var);
		serializableUnityARLightData__ctor_m2325970984(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		// }
		serializableUnityARLightData_t1864664250 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.XR.iOS.UnityARLightData Utils.serializableUnityARLightData::op_Implicit(Utils.serializableUnityARLightData)
extern "C"  UnityARLightData_t3404656299  serializableUnityARLightData_op_Implicit_m322329394 (RuntimeObject * __this /* static, unused */, serializableUnityARLightData_t1864664250 * ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARLightData_op_Implicit_m322329394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARDirectionalLightEstimate_t3433403765 * V_0 = NULL;
	UnityARLightEstimate_t2392533559  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t329709361  V_2;
	memset(&V_2, 0, sizeof(V_2));
	UnityARLightData_t3404656299  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		// UnityARDirectionalLightEstimate udle = null;
		V_0 = (UnityARDirectionalLightEstimate_t3433403765 *)NULL;
		// UnityARLightEstimate ule = new UnityARLightEstimate (rValue.ambientIntensity, rValue.ambientColorTemperature);
		serializableUnityARLightData_t1864664250 * L_0 = ___rValue0;
		NullCheck(L_0);
		float L_1 = L_0->get_ambientIntensity_3();
		serializableUnityARLightData_t1864664250 * L_2 = ___rValue0;
		NullCheck(L_2);
		float L_3 = L_2->get_ambientColorTemperature_4();
		// UnityARLightEstimate ule = new UnityARLightEstimate (rValue.ambientIntensity, rValue.ambientColorTemperature);
		UnityARLightEstimate__ctor_m302426687((&V_1), L_1, L_3, /*hidden argument*/NULL);
		// if (rValue.whichLight == LightDataType.DirectionalLightEstimate) {
		serializableUnityARLightData_t1864664250 * L_4 = ___rValue0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_whichLight_0();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0069;
		}
	}
	{
		// Vector3 lightDir = new Vector3 (rValue.primaryLightDirAndIntensity.x, rValue.primaryLightDirAndIntensity.y, rValue.primaryLightDirAndIntensity.z);
		serializableUnityARLightData_t1864664250 * L_6 = ___rValue0;
		NullCheck(L_6);
		SerializableVector4_t3177127415 * L_7 = L_6->get_primaryLightDirAndIntensity_2();
		NullCheck(L_7);
		float L_8 = L_7->get_x_0();
		serializableUnityARLightData_t1864664250 * L_9 = ___rValue0;
		NullCheck(L_9);
		SerializableVector4_t3177127415 * L_10 = L_9->get_primaryLightDirAndIntensity_2();
		NullCheck(L_10);
		float L_11 = L_10->get_y_1();
		serializableUnityARLightData_t1864664250 * L_12 = ___rValue0;
		NullCheck(L_12);
		SerializableVector4_t3177127415 * L_13 = L_12->get_primaryLightDirAndIntensity_2();
		NullCheck(L_13);
		float L_14 = L_13->get_z_2();
		// Vector3 lightDir = new Vector3 (rValue.primaryLightDirAndIntensity.x, rValue.primaryLightDirAndIntensity.y, rValue.primaryLightDirAndIntensity.z);
		Vector3__ctor_m3984700005((&V_2), L_8, L_11, L_14, /*hidden argument*/NULL);
		// udle = new UnityARDirectionalLightEstimate (rValue.lightSHC, lightDir, rValue.primaryLightDirAndIntensity.w);
		serializableUnityARLightData_t1864664250 * L_15 = ___rValue0;
		NullCheck(L_15);
		serializableSHC_t2973324561 * L_16 = L_15->get_lightSHC_1();
		// udle = new UnityARDirectionalLightEstimate (rValue.lightSHC, lightDir, rValue.primaryLightDirAndIntensity.w);
		SingleU5BU5D_t2905636975* L_17 = serializableSHC_op_Implicit_m4253342705(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Vector3_t329709361  L_18 = V_2;
		serializableUnityARLightData_t1864664250 * L_19 = ___rValue0;
		NullCheck(L_19);
		SerializableVector4_t3177127415 * L_20 = L_19->get_primaryLightDirAndIntensity_2();
		NullCheck(L_20);
		float L_21 = L_20->get_w_3();
		// udle = new UnityARDirectionalLightEstimate (rValue.lightSHC, lightDir, rValue.primaryLightDirAndIntensity.w);
		UnityARDirectionalLightEstimate_t3433403765 * L_22 = (UnityARDirectionalLightEstimate_t3433403765 *)il2cpp_codegen_object_new(UnityARDirectionalLightEstimate_t3433403765_il2cpp_TypeInfo_var);
		UnityARDirectionalLightEstimate__ctor_m3195205346(L_22, L_17, L_18, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
	}

IL_0069:
	{
		// return new UnityARLightData(rValue.whichLight, ule, udle);
		serializableUnityARLightData_t1864664250 * L_23 = ___rValue0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_whichLight_0();
		UnityARLightEstimate_t2392533559  L_25 = V_1;
		UnityARDirectionalLightEstimate_t3433403765 * L_26 = V_0;
		// return new UnityARLightData(rValue.whichLight, ule, udle);
		UnityARLightData_t3404656299  L_27;
		memset(&L_27, 0, sizeof(L_27));
		UnityARLightData__ctor_m2108807113((&L_27), L_24, L_25, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		goto IL_007c;
	}

IL_007c:
	{
		// }
		UnityARLightData_t3404656299  L_28 = V_3;
		return L_28;
	}
}
// System.Void Utils.serializableUnityARMatrix4x4::.ctor(Utils.SerializableVector4,Utils.SerializableVector4,Utils.SerializableVector4,Utils.SerializableVector4)
extern "C"  void serializableUnityARMatrix4x4__ctor_m2539109597 (serializableUnityARMatrix4x4_t3548294260 * __this, SerializableVector4_t3177127415 * ___v00, SerializableVector4_t3177127415 * ___v11, SerializableVector4_t3177127415 * ___v22, SerializableVector4_t3177127415 * ___v33, const RuntimeMethod* method)
{
	{
		// public serializableUnityARMatrix4x4(SerializableVector4 v0, SerializableVector4 v1, SerializableVector4 v2, SerializableVector4 v3)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// column0 = v0;
		SerializableVector4_t3177127415 * L_0 = ___v00;
		__this->set_column0_0(L_0);
		// column1 = v1;
		SerializableVector4_t3177127415 * L_1 = ___v11;
		__this->set_column1_1(L_1);
		// column2 = v2;
		SerializableVector4_t3177127415 * L_2 = ___v22;
		__this->set_column2_2(L_2);
		// column3 = v3;
		SerializableVector4_t3177127415 * L_3 = ___v33;
		__this->set_column3_3(L_3);
		// }
		return;
	}
}
// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(UnityEngine.XR.iOS.UnityARMatrix4x4)
extern "C"  serializableUnityARMatrix4x4_t3548294260 * serializableUnityARMatrix4x4_op_Implicit_m1426572080 (RuntimeObject * __this /* static, unused */, UnityARMatrix4x4_t758723042  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARMatrix4x4_op_Implicit_m1426572080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableUnityARMatrix4x4_t3548294260 * V_0 = NULL;
	{
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_0 = (&___rValue0)->get_column0_0();
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		SerializableVector4_t3177127415 * L_1 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Vector4_t380635127  L_2 = (&___rValue0)->get_column1_1();
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		SerializableVector4_t3177127415 * L_3 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Vector4_t380635127  L_4 = (&___rValue0)->get_column2_2();
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		SerializableVector4_t3177127415 * L_5 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector4_t380635127  L_6 = (&___rValue0)->get_column3_3();
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		SerializableVector4_t3177127415 * L_7 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		serializableUnityARMatrix4x4_t3548294260 * L_8 = (serializableUnityARMatrix4x4_t3548294260 *)il2cpp_codegen_object_new(serializableUnityARMatrix4x4_t3548294260_il2cpp_TypeInfo_var);
		serializableUnityARMatrix4x4__ctor_m2539109597(L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_003c;
	}

IL_003c:
	{
		// }
		serializableUnityARMatrix4x4_t3548294260 * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.XR.iOS.UnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(Utils.serializableUnityARMatrix4x4)
extern "C"  UnityARMatrix4x4_t758723042  serializableUnityARMatrix4x4_op_Implicit_m2448964739 (RuntimeObject * __this /* static, unused */, serializableUnityARMatrix4x4_t3548294260 * ___rValue0, const RuntimeMethod* method)
{
	UnityARMatrix4x4_t758723042  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		serializableUnityARMatrix4x4_t3548294260 * L_0 = ___rValue0;
		NullCheck(L_0);
		SerializableVector4_t3177127415 * L_1 = L_0->get_column0_0();
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_2 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_3 = ___rValue0;
		NullCheck(L_3);
		SerializableVector4_t3177127415 * L_4 = L_3->get_column1_1();
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_5 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_6 = ___rValue0;
		NullCheck(L_6);
		SerializableVector4_t3177127415 * L_7 = L_6->get_column2_2();
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_8 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_9 = ___rValue0;
		NullCheck(L_9);
		SerializableVector4_t3177127415 * L_10 = L_9->get_column3_3();
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_11 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		// return new UnityARMatrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		UnityARMatrix4x4_t758723042  L_12;
		memset(&L_12, 0, sizeof(L_12));
		UnityARMatrix4x4__ctor_m2409038275((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		UnityARMatrix4x4_t758723042  L_13 = V_0;
		return L_13;
	}
}
// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(UnityEngine.Matrix4x4)
extern "C"  serializableUnityARMatrix4x4_t3548294260 * serializableUnityARMatrix4x4_op_Implicit_m319493325 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2375577114  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARMatrix4x4_op_Implicit_m319493325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableUnityARMatrix4x4_t3548294260 * V_0 = NULL;
	{
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		Vector4_t380635127  L_0 = Matrix4x4_GetColumn_m1167652957((&___rValue0), 0, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		SerializableVector4_t3177127415 * L_1 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		Vector4_t380635127  L_2 = Matrix4x4_GetColumn_m1167652957((&___rValue0), 1, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		SerializableVector4_t3177127415 * L_3 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		Vector4_t380635127  L_4 = Matrix4x4_GetColumn_m1167652957((&___rValue0), 2, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		SerializableVector4_t3177127415 * L_5 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		Vector4_t380635127  L_6 = Matrix4x4_GetColumn_m1167652957((&___rValue0), 3, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		SerializableVector4_t3177127415 * L_7 = SerializableVector4_op_Implicit_m2125629402(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// return new serializableUnityARMatrix4x4(rValue.GetColumn(0), rValue.GetColumn(1), rValue.GetColumn(2), rValue.GetColumn(3));
		serializableUnityARMatrix4x4_t3548294260 * L_8 = (serializableUnityARMatrix4x4_t3548294260 *)il2cpp_codegen_object_new(serializableUnityARMatrix4x4_t3548294260_il2cpp_TypeInfo_var);
		serializableUnityARMatrix4x4__ctor_m2539109597(L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0040;
	}

IL_0040:
	{
		// }
		serializableUnityARMatrix4x4_t3548294260 * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Matrix4x4 Utils.serializableUnityARMatrix4x4::op_Implicit(Utils.serializableUnityARMatrix4x4)
extern "C"  Matrix4x4_t2375577114  serializableUnityARMatrix4x4_op_Implicit_m615038945 (RuntimeObject * __this /* static, unused */, serializableUnityARMatrix4x4_t3548294260 * ___rValue0, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		serializableUnityARMatrix4x4_t3548294260 * L_0 = ___rValue0;
		NullCheck(L_0);
		SerializableVector4_t3177127415 * L_1 = L_0->get_column0_0();
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_2 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_3 = ___rValue0;
		NullCheck(L_3);
		SerializableVector4_t3177127415 * L_4 = L_3->get_column1_1();
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_5 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_6 = ___rValue0;
		NullCheck(L_6);
		SerializableVector4_t3177127415 * L_7 = L_6->get_column2_2();
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_8 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		serializableUnityARMatrix4x4_t3548294260 * L_9 = ___rValue0;
		NullCheck(L_9);
		SerializableVector4_t3177127415 * L_10 = L_9->get_column3_3();
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Vector4_t380635127  L_11 = SerializableVector4_op_Implicit_m946317734(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		// return new Matrix4x4(rValue.column0, rValue.column1, rValue.column2, rValue.column3);
		Matrix4x4_t2375577114  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Matrix4x4__ctor_m3113358119((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		Matrix4x4_t2375577114  L_13 = V_0;
		return L_13;
	}
}
// System.Void Utils.serializableUnityARPlaneAnchor::.ctor(Utils.serializableUnityARMatrix4x4,Utils.SerializableVector4,Utils.SerializableVector4,UnityEngine.XR.iOS.ARPlaneAnchorAlignment,System.Byte[])
extern "C"  void serializableUnityARPlaneAnchor__ctor_m1264816476 (serializableUnityARPlaneAnchor_t3395010059 * __this, serializableUnityARMatrix4x4_t3548294260 * ___wt0, SerializableVector4_t3177127415 * ___ctr1, SerializableVector4_t3177127415 * ___ext2, int64_t ___apaa3, ByteU5BU5D_t3287329517* ___idstr4, const RuntimeMethod* method)
{
	{
		// public serializableUnityARPlaneAnchor( serializableUnityARMatrix4x4 wt, SerializableVector4 ctr, SerializableVector4 ext, ARPlaneAnchorAlignment apaa,
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// worldTransform = wt;
		serializableUnityARMatrix4x4_t3548294260 * L_0 = ___wt0;
		__this->set_worldTransform_0(L_0);
		// center = ctr;
		SerializableVector4_t3177127415 * L_1 = ___ctr1;
		__this->set_center_1(L_1);
		// extent = ext;
		SerializableVector4_t3177127415 * L_2 = ___ext2;
		__this->set_extent_2(L_2);
		// planeAlignment = apaa;
		int64_t L_3 = ___apaa3;
		__this->set_planeAlignment_3(L_3);
		// identifierStr = idstr;
		ByteU5BU5D_t3287329517* L_4 = ___idstr4;
		__this->set_identifierStr_4(L_4);
		// }
		return;
	}
}
// Utils.serializableUnityARPlaneAnchor Utils.serializableUnityARPlaneAnchor::op_Implicit(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  serializableUnityARPlaneAnchor_t3395010059 * serializableUnityARPlaneAnchor_op_Implicit_m66415631 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t2525223154  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARPlaneAnchor_op_Implicit_m66415631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	serializableUnityARMatrix4x4_t3548294260 * V_0 = NULL;
	SerializableVector4_t3177127415 * V_1 = NULL;
	SerializableVector4_t3177127415 * V_2 = NULL;
	ByteU5BU5D_t3287329517* V_3 = NULL;
	serializableUnityARPlaneAnchor_t3395010059 * V_4 = NULL;
	{
		// serializableUnityARMatrix4x4 wt = rValue.transform;
		Matrix4x4_t2375577114  L_0 = (&___rValue0)->get_transform_1();
		// serializableUnityARMatrix4x4 wt = rValue.transform;
		serializableUnityARMatrix4x4_t3548294260 * L_1 = serializableUnityARMatrix4x4_op_Implicit_m319493325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// SerializableVector4 ctr = new SerializableVector4 (rValue.center.x, rValue.center.y, rValue.center.z, 1.0f);
		Vector3_t329709361 * L_2 = (&___rValue0)->get_address_of_center_3();
		float L_3 = L_2->get_x_1();
		Vector3_t329709361 * L_4 = (&___rValue0)->get_address_of_center_3();
		float L_5 = L_4->get_y_2();
		Vector3_t329709361 * L_6 = (&___rValue0)->get_address_of_center_3();
		float L_7 = L_6->get_z_3();
		// SerializableVector4 ctr = new SerializableVector4 (rValue.center.x, rValue.center.y, rValue.center.z, 1.0f);
		SerializableVector4_t3177127415 * L_8 = (SerializableVector4_t3177127415 *)il2cpp_codegen_object_new(SerializableVector4_t3177127415_il2cpp_TypeInfo_var);
		SerializableVector4__ctor_m1389529421(L_8, L_3, L_5, L_7, (1.0f), /*hidden argument*/NULL);
		V_1 = L_8;
		// SerializableVector4 ext = new SerializableVector4 (rValue.extent.x, rValue.extent.y, rValue.extent.z, 1.0f);
		Vector3_t329709361 * L_9 = (&___rValue0)->get_address_of_extent_4();
		float L_10 = L_9->get_x_1();
		Vector3_t329709361 * L_11 = (&___rValue0)->get_address_of_extent_4();
		float L_12 = L_11->get_y_2();
		Vector3_t329709361 * L_13 = (&___rValue0)->get_address_of_extent_4();
		float L_14 = L_13->get_z_3();
		// SerializableVector4 ext = new SerializableVector4 (rValue.extent.x, rValue.extent.y, rValue.extent.z, 1.0f);
		SerializableVector4_t3177127415 * L_15 = (SerializableVector4_t3177127415 *)il2cpp_codegen_object_new(SerializableVector4_t3177127415_il2cpp_TypeInfo_var);
		SerializableVector4__ctor_m1389529421(L_15, L_10, L_12, L_14, (1.0f), /*hidden argument*/NULL);
		V_2 = L_15;
		// byte[] idstr = Encoding.UTF8.GetBytes (rValue.identifier);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t3638904762_il2cpp_TypeInfo_var);
		Encoding_t3638904762 * L_16 = Encoding_get_UTF8_m3605462633(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = (&___rValue0)->get_identifier_0();
		// byte[] idstr = Encoding.UTF8.GetBytes (rValue.identifier);
		NullCheck(L_16);
		ByteU5BU5D_t3287329517* L_18 = VirtFuncInvoker1< ByteU5BU5D_t3287329517*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_16, L_17);
		V_3 = L_18;
		// return new serializableUnityARPlaneAnchor(wt, ctr, ext, rValue.alignment, idstr);
		serializableUnityARMatrix4x4_t3548294260 * L_19 = V_0;
		SerializableVector4_t3177127415 * L_20 = V_1;
		SerializableVector4_t3177127415 * L_21 = V_2;
		int64_t L_22 = (&___rValue0)->get_alignment_2();
		ByteU5BU5D_t3287329517* L_23 = V_3;
		// return new serializableUnityARPlaneAnchor(wt, ctr, ext, rValue.alignment, idstr);
		serializableUnityARPlaneAnchor_t3395010059 * L_24 = (serializableUnityARPlaneAnchor_t3395010059 *)il2cpp_codegen_object_new(serializableUnityARPlaneAnchor_t3395010059_il2cpp_TypeInfo_var);
		serializableUnityARPlaneAnchor__ctor_m1264816476(L_24, L_19, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		goto IL_0095;
	}

IL_0095:
	{
		// }
		serializableUnityARPlaneAnchor_t3395010059 * L_25 = V_4;
		return L_25;
	}
}
// UnityEngine.XR.iOS.ARPlaneAnchor Utils.serializableUnityARPlaneAnchor::op_Implicit(Utils.serializableUnityARPlaneAnchor)
extern "C"  ARPlaneAnchor_t2525223154  serializableUnityARPlaneAnchor_op_Implicit_m3608928610 (RuntimeObject * __this /* static, unused */, serializableUnityARPlaneAnchor_t3395010059 * ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (serializableUnityARPlaneAnchor_op_Implicit_m3608928610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t2525223154  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ARPlaneAnchor_t2525223154  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// retValue.identifier = Encoding.UTF8.GetString (rValue.identifierStr);
		// retValue.identifier = Encoding.UTF8.GetString (rValue.identifierStr);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t3638904762_il2cpp_TypeInfo_var);
		Encoding_t3638904762 * L_0 = Encoding_get_UTF8_m3605462633(NULL /*static, unused*/, /*hidden argument*/NULL);
		serializableUnityARPlaneAnchor_t3395010059 * L_1 = ___rValue0;
		NullCheck(L_1);
		ByteU5BU5D_t3287329517* L_2 = L_1->get_identifierStr_4();
		// retValue.identifier = Encoding.UTF8.GetString (rValue.identifierStr);
		NullCheck(L_0);
		String_t* L_3 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3287329517* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_0, L_2);
		(&V_0)->set_identifier_0(L_3);
		// retValue.center = new Vector3 (rValue.center.x, rValue.center.y, rValue.center.z);
		serializableUnityARPlaneAnchor_t3395010059 * L_4 = ___rValue0;
		NullCheck(L_4);
		SerializableVector4_t3177127415 * L_5 = L_4->get_center_1();
		NullCheck(L_5);
		float L_6 = L_5->get_x_0();
		serializableUnityARPlaneAnchor_t3395010059 * L_7 = ___rValue0;
		NullCheck(L_7);
		SerializableVector4_t3177127415 * L_8 = L_7->get_center_1();
		NullCheck(L_8);
		float L_9 = L_8->get_y_1();
		serializableUnityARPlaneAnchor_t3395010059 * L_10 = ___rValue0;
		NullCheck(L_10);
		SerializableVector4_t3177127415 * L_11 = L_10->get_center_1();
		NullCheck(L_11);
		float L_12 = L_11->get_z_2();
		// retValue.center = new Vector3 (rValue.center.x, rValue.center.y, rValue.center.z);
		Vector3_t329709361  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m3984700005((&L_13), L_6, L_9, L_12, /*hidden argument*/NULL);
		(&V_0)->set_center_3(L_13);
		// retValue.extent = new Vector3 (rValue.extent.x, rValue.extent.y, rValue.extent.z);
		serializableUnityARPlaneAnchor_t3395010059 * L_14 = ___rValue0;
		NullCheck(L_14);
		SerializableVector4_t3177127415 * L_15 = L_14->get_extent_2();
		NullCheck(L_15);
		float L_16 = L_15->get_x_0();
		serializableUnityARPlaneAnchor_t3395010059 * L_17 = ___rValue0;
		NullCheck(L_17);
		SerializableVector4_t3177127415 * L_18 = L_17->get_extent_2();
		NullCheck(L_18);
		float L_19 = L_18->get_y_1();
		serializableUnityARPlaneAnchor_t3395010059 * L_20 = ___rValue0;
		NullCheck(L_20);
		SerializableVector4_t3177127415 * L_21 = L_20->get_extent_2();
		NullCheck(L_21);
		float L_22 = L_21->get_z_2();
		// retValue.extent = new Vector3 (rValue.extent.x, rValue.extent.y, rValue.extent.z);
		Vector3_t329709361  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m3984700005((&L_23), L_16, L_19, L_22, /*hidden argument*/NULL);
		(&V_0)->set_extent_4(L_23);
		// retValue.alignment = rValue.planeAlignment;
		serializableUnityARPlaneAnchor_t3395010059 * L_24 = ___rValue0;
		NullCheck(L_24);
		int64_t L_25 = L_24->get_planeAlignment_3();
		(&V_0)->set_alignment_2(L_25);
		// retValue.transform = rValue.worldTransform;
		serializableUnityARPlaneAnchor_t3395010059 * L_26 = ___rValue0;
		NullCheck(L_26);
		serializableUnityARMatrix4x4_t3548294260 * L_27 = L_26->get_worldTransform_0();
		// retValue.transform = rValue.worldTransform;
		Matrix4x4_t2375577114  L_28 = serializableUnityARMatrix4x4_op_Implicit_m615038945(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		(&V_0)->set_transform_1(L_28);
		// return retValue;
		ARPlaneAnchor_t2525223154  L_29 = V_0;
		V_1 = L_29;
		goto IL_0098;
	}

IL_0098:
	{
		// }
		ARPlaneAnchor_t2525223154  L_30 = V_1;
		return L_30;
	}
}
// System.Void Utils.SerializableVector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SerializableVector4__ctor_m1389529421 (SerializableVector4_t3177127415 * __this, float ___rX0, float ___rY1, float ___rZ2, float ___rW3, const RuntimeMethod* method)
{
	{
		// public SerializableVector4(float rX, float rY, float rZ, float rW)
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		// x = rX;
		float L_0 = ___rX0;
		__this->set_x_0(L_0);
		// y = rY;
		float L_1 = ___rY1;
		__this->set_y_1(L_1);
		// z = rZ;
		float L_2 = ___rZ2;
		__this->set_z_2(L_2);
		// w = rW;
		float L_3 = ___rW3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
// System.String Utils.SerializableVector4::ToString()
extern "C"  String_t* SerializableVector4_ToString_m2464175451 (SerializableVector4_t3177127415 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector4_ToString_m2464175451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		// return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1554370993(NULL /*static, unused*/, _stringLiteral4079274232, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		// }
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Vector4 Utils.SerializableVector4::op_Implicit(Utils.SerializableVector4)
extern "C"  Vector4_t380635127  SerializableVector4_op_Implicit_m946317734 (RuntimeObject * __this /* static, unused */, SerializableVector4_t3177127415 * ___rValue0, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return new Vector4(rValue.x, rValue.y, rValue.z, rValue.w);
		SerializableVector4_t3177127415 * L_0 = ___rValue0;
		NullCheck(L_0);
		float L_1 = L_0->get_x_0();
		SerializableVector4_t3177127415 * L_2 = ___rValue0;
		NullCheck(L_2);
		float L_3 = L_2->get_y_1();
		SerializableVector4_t3177127415 * L_4 = ___rValue0;
		NullCheck(L_4);
		float L_5 = L_4->get_z_2();
		SerializableVector4_t3177127415 * L_6 = ___rValue0;
		NullCheck(L_6);
		float L_7 = L_6->get_w_3();
		// return new Vector4(rValue.x, rValue.y, rValue.z, rValue.w);
		Vector4_t380635127  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m3770092030((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0024;
	}

IL_0024:
	{
		// }
		Vector4_t380635127  L_9 = V_0;
		return L_9;
	}
}
// Utils.SerializableVector4 Utils.SerializableVector4::op_Implicit(UnityEngine.Vector4)
extern "C"  SerializableVector4_t3177127415 * SerializableVector4_op_Implicit_m2125629402 (RuntimeObject * __this /* static, unused */, Vector4_t380635127  ___rValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializableVector4_op_Implicit_m2125629402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SerializableVector4_t3177127415 * V_0 = NULL;
	{
		// return new SerializableVector4(rValue.x, rValue.y, rValue.z, rValue.w);
		float L_0 = (&___rValue0)->get_x_1();
		float L_1 = (&___rValue0)->get_y_2();
		float L_2 = (&___rValue0)->get_z_3();
		float L_3 = (&___rValue0)->get_w_4();
		// return new SerializableVector4(rValue.x, rValue.y, rValue.z, rValue.w);
		SerializableVector4_t3177127415 * L_4 = (SerializableVector4_t3177127415 *)il2cpp_codegen_object_new(SerializableVector4_t3177127415_il2cpp_TypeInfo_var);
		SerializableVector4__ctor_m1389529421(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		SerializableVector4_t3177127415 * L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
