﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t4188729888;
// System.String
struct String_t;
// System.Attribute
struct Attribute_t3852256153;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t2086846075;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t781534341;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t3406496741;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t309455265;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2962125979;
// UnityEngine.Object
struct Object_t1970767703;
// UnityEngine.IPlayerEditorConnectionNative
struct IPlayerEditorConnectionNative_t1924324047;
// UnityEngine.PlayerConnectionInternal
struct PlayerConnectionInternal_t984574678;
// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityAction_1_t4230376260;
// System.ArgumentException
struct ArgumentException_t3637419113;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t3611986306;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t701569646;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityEvent_1_t1359409152;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3942628331;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2518628143;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t2642535036;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t4066535224;
// System.Byte[]
struct ByteU5BU5D_t3287329517;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t3095965032;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t185548372;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t1857494072;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t4252092203;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t347937039;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1
struct U3CAddAndCreateU3Ec__AnonStorey1_t2037887867;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct MessageTypeSubscribers_t3285514618;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct MessageEvent_t3155234160;
// UnityEngine.Object[]
struct ObjectU5BU5D_t3487290734;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1568665923;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t2447076843;
// UnityEngine.GameObject
struct GameObject_t1318052361;
// UnityEngine.Playables.PlayableBehaviour
struct PlayableBehaviour_t2742200540;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t69305138;
// UnityEngine.RangeAttribute
struct RangeAttribute_t2570355765;
// UnityEngine.RectOffset
struct RectOffset_t83369214;
// UnityEngine.RectTransform
struct RectTransform_t15861704;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3527255582;
// System.Delegate
struct Delegate_t1563516729;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492;
// UnityEngine.Component
struct Component_t789413749;
// UnityEngine.Transform
struct Transform_t532597831;
// System.IAsyncResult
struct IAsyncResult_t614244269;
// System.AsyncCallback
struct AsyncCallback_t869574496;
// UnityEngine.Renderer
struct Renderer_t265431926;
// UnityEngine.Material
struct Material_t2712136762;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3417007309;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2673047760;
// UnityEngine.Texture
struct Texture_t2838694469;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3921978895;
// UnityEngine.RenderTexture
struct RenderTexture_t3361574884;
// UnityEngine.RequireComponent
struct RequireComponent_t4152270991;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2777655240;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t2691974747;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t562236193;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t1698167202;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t917163987;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3769384580;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t1772378422;
// UnityEngine.Camera
struct Camera_t989002943;
// UnityEngine.GUILayer
struct GUILayer_t2694382279;
// UnityEngine.GUIElement
struct GUIElement_t1520723180;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3897890150;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t55868289;
// UnityEngine.SerializeField
struct SerializeField_t1434498030;
// System.Collections.IEnumerator
struct IEnumerator_t1842762036;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t4105298238;
// UnityEngine.Sprite
struct Sprite_t3012664695;
// UnityEngine.Texture2D
struct Texture2D_t878840578;
// System.Diagnostics.StackTrace
struct StackTrace_t1080444557;
// System.Text.StringBuilder
struct StringBuilder_t2355629516;
// System.Exception
struct Exception_t2123675094;
// System.String[]
struct StringU5BU5D_t369357837;
// System.Char[]
struct CharU5BU5D_t83643201;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2372523953;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t1594486658;
// UnityEngine.Color32[]
struct Color32U5BU5D_t1505762612;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t46731272;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t2327649817;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t3991761301;
// UnityEngine.TrackedReference
struct TrackedReference_t774033239;
// UnityEngine.Transform/Enumerator
struct Enumerator_t1378995202;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t1977832595;
// System.Action`1<System.Object>
struct Action_1_t3509626261;
// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct RequestAtlasCallback_t634135197;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t3138271588;
// System.AppDomain
struct AppDomain_t3006601831;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t2340223587;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t4227701455;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1216529257;
// System.Globalization.CultureInfo
struct CultureInfo_t1870276872;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1937290703;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t3472450545;
// System.Globalization.TextInfo
struct TextInfo_t4217374101;
// System.Globalization.CompareInfo
struct CompareInfo_t4218043007;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t4092062009;
// System.Globalization.Calendar
struct Calendar_t2104464680;
// System.Collections.Hashtable
struct Hashtable_t3677817270;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3491703694;
// System.Int32
struct Int32_t499004851;
// System.Void
struct Void_t2642135423;
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t948874216;
// System.IntPtr[]
struct IntPtrU5BU5D_t2297631236;
// System.Collections.IDictionary
struct IDictionary_t2298573854;
// System.Reflection.MethodBase
struct MethodBase_t3535115348;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1231629294;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1952737969;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2184608466;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers[]
struct MessageTypeSubscribersU5BU5D_t1177853791;
// System.Int32[]
struct Int32U5BU5D_t3565237794;
// System.Boolean[]
struct BooleanU5BU5D_t2577425931;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2116050852;
// System.Byte
struct Byte_t2815932036;
// System.Double
struct Double_t2078998952;
// System.UInt16
struct UInt16_t3440013504;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t975501551;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t67027751;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t3010105947;
// System.Type[]
struct TypeU5BU5D_t1460120061;
// System.Reflection.MemberFilter
struct MemberFilter_t3962658540;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1960139725;
// System.Security.Policy.Evidence
struct Evidence_t3393713150;
// System.Security.PermissionSet
struct PermissionSet_t2345569226;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1517141768;
// System.AppDomainManager
struct AppDomainManager_t4290337414;
// System.ActivationContext
struct ActivationContext_t4234663815;
// System.ApplicationIdentity
struct ApplicationIdentity_t3327377620;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t1180749029;
// System.ResolveEventHandler
struct ResolveEventHandler_t3861315061;
// System.EventHandler
struct EventHandler_t1794010283;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t2620733104;

extern RuntimeClass* PlayerEditorConnectionEvents_t3406496741_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t309455265_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3981019775_RuntimeMethod_var;
extern const uint32_t PlayerConnection__ctor_m2316667390_MetadataUsageId;
extern RuntimeClass* PlayerConnection_t781534341_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t1970767703_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_instance_m1185993489_MetadataUsageId;
extern RuntimeClass* IPlayerEditorConnectionNative_t1924324047_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_isConnected_m1595077683_MetadataUsageId;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisPlayerConnection_t781534341_m1156802871_RuntimeMethod_var;
extern const uint32_t PlayerConnection_CreateInstance_m3407819880_MetadataUsageId;
extern RuntimeClass* PlayerConnectionInternal_t984574678_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_GetConnectionNativeApi_m3971624049_MetadataUsageId;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t3637419113_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m508826909_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral200924526;
extern Il2CppCodeGenString* _stringLiteral1786669557;
extern const uint32_t PlayerConnection_Register_m3298957340_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m2158044649_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m991032171_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1_Invoke_m3292288955_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3878557316_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4143828380_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m1699996981_RuntimeMethod_var;
extern const uint32_t PlayerConnection_RegisterConnection_m2917454692_MetadataUsageId;
extern const uint32_t PlayerConnection_RegisterDisconnection_m3823650919_MetadataUsageId;
extern const uint32_t PlayerConnection_Send_m941112358_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectAll_m1534374562_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var;
extern RuntimeClass* Marshal_t436595762_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_MessageCallbackInternal_m41337847_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m2519407053_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m2806368434_RuntimeMethod_var;
extern const uint32_t PlayerConnection_ConnectedCallbackInternal_m3339679254_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectedCallback_m1992762101_MetadataUsageId;
extern RuntimeClass* List_1_t3095965032_il2cpp_TypeInfo_var;
extern RuntimeClass* ConnectionChangeEvent_t1857494072_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2158277014_RuntimeMethod_var;
extern const uint32_t PlayerEditorConnectionEvents__ctor_m1071995445_MetadataUsageId;
extern RuntimeClass* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t4252092203_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t114115908_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageEventArgs_t2086846075_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t3611986306_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t174001016_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1842762036_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3363289168_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m3681874696_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m2720484915_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Where_TisMessageTypeSubscribers_t3285514618_m2269018702_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m1430365874_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1391582540;
extern const uint32_t PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4252710989_MetadataUsageId;
extern RuntimeClass* U3CAddAndCreateU3Ec__AnonStorey1_t2037887867_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageTypeSubscribers_t3285514618_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageEvent_t3155234160_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m2574083174_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t3285514618_m1777054811_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1776051518_RuntimeMethod_var;
extern const uint32_t PlayerEditorConnectionEvents_AddAndCreate_m3897452280_MetadataUsageId;
extern const uint32_t U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m2574083174_MetadataUsageId;
extern const uint32_t U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m3681874696_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m15913427_RuntimeMethod_var;
extern const uint32_t ConnectionChangeEvent__ctor_m537474357_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m2017699965_RuntimeMethod_var;
extern const uint32_t MessageEvent__ctor_m4280913397_MetadataUsageId;
extern const uint32_t MessageTypeSubscribers__ctor_m2582810082_MetadataUsageId;
extern const uint32_t Object_Internal_InstantiateSingle_m3669590211_MetadataUsageId;
extern const uint32_t Object_Destroy_m935298371_MetadataUsageId;
extern const uint32_t Object_DestroyImmediate_m3678630788_MetadataUsageId;
extern const uint32_t Object_Equals_m828978334_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m1890640795_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m4209539787_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m2316845610_MetadataUsageId;
extern RuntimeClass* ScriptableObject_t2962125979_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral568845039;
extern Il2CppCodeGenString* _stringLiteral3499848769;
extern const uint32_t Object_Instantiate_m709640951_MetadataUsageId;
extern const uint32_t Object_CheckNullArgument_m426474767_MetadataUsageId;
extern const uint32_t Object_op_Equality_m1910042615_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m4170278078_MetadataUsageId;
extern const uint32_t Object__cctor_m3981639895_MetadataUsageId;
extern RuntimeClass* Vector3_t329709361_il2cpp_TypeInfo_var;
extern const uint32_t Plane__ctor_m2699116517_MetadataUsageId;
extern RuntimeClass* Mathf_t2081007568_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m995144852_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1863352746_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral815514077;
extern const uint32_t Plane_ToString_m2969686206_MetadataUsageId;
extern RuntimeClass* Playable_t348555058_il2cpp_TypeInfo_var;
extern const uint32_t Playable_get_Null_m799873240_MetadataUsageId;
extern const uint32_t Playable__cctor_m2813807852_MetadataUsageId;
extern RuntimeClass* PlayableBinding_t181640494_il2cpp_TypeInfo_var;
extern const uint32_t PlayableAsset_get_duration_m1385497653_MetadataUsageId;
extern const uint32_t PlayableAsset_Internal_CreatePlayable_m1139410774_MetadataUsageId;
struct Object_t1970767703_marshaled_pinvoke;
struct Object_t1970767703;;
struct Object_t1970767703_marshaled_pinvoke;;
struct Object_t1970767703_marshaled_com;
struct Object_t1970767703_marshaled_com;;
extern RuntimeClass* PlayableBindingU5BU5D_t3010105947_il2cpp_TypeInfo_var;
extern const uint32_t PlayableBinding__cctor_m513300806_MetadataUsageId;
extern RuntimeClass* PlayableHandle_t3963382032_il2cpp_TypeInfo_var;
extern const uint32_t PlayableHandle_get_Null_m428254764_MetadataUsageId;
extern const uint32_t PlayableHandle_Equals_m2260768344_MetadataUsageId;
extern RuntimeClass* PlayableOutput_t1141860262_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutput__cctor_m2104493696_MetadataUsageId;
extern RuntimeClass* PlayableOutputHandle_t1112988996_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutputHandle_get_Null_m373902066_MetadataUsageId;
extern const uint32_t PlayableOutputHandle_Equals_m4139373885_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2334848428;
extern Il2CppCodeGenString* _stringLiteral4027178674;
extern const uint32_t PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m1332611929_MetadataUsageId;
extern const uint32_t PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m3972857555_MetadataUsageId;
extern RuntimeClass* PropertyName_t3644065956_il2cpp_TypeInfo_var;
extern const uint32_t PropertyName_Equals_m1268943811_MetadataUsageId;
extern RuntimeClass* Int32_t499004851_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral984751784;
extern const uint32_t PropertyName_ToString_m1247093862_MetadataUsageId;
extern RuntimeClass* Quaternion_t2761156409_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_LookRotation_m358999601_MetadataUsageId;
extern const uint32_t Quaternion_Inverse_m1565765131_MetadataUsageId;
extern const uint32_t Quaternion_Euler_m3622738752_MetadataUsageId;
extern const uint32_t Quaternion_Internal_FromEulerRad_m2232655408_MetadataUsageId;
extern const uint32_t Quaternion_get_identity_m2232226055_MetadataUsageId;
extern const uint32_t Quaternion_op_Equality_m3822093777_MetadataUsageId;
extern const uint32_t Quaternion_op_Inequality_m2565209256_MetadataUsageId;
extern const uint32_t Quaternion_Equals_m1356692124_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral409661081;
extern const uint32_t Quaternion_ToString_m1462711549_MetadataUsageId;
extern const uint32_t Quaternion__cctor_m3320953330_MetadataUsageId;
extern const uint32_t Ray_GetPoint_m635666389_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1413257799;
extern const uint32_t Ray_ToString_m1475619137_MetadataUsageId;
extern RuntimeClass* Rect_t1344834245_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m1339189050_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1740198039;
extern const uint32_t Rect_ToString_m4042206769_MetadataUsageId;
extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t RectOffset_t83369214_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t RectOffset_t83369214_com_FromNativeMethodDefinition_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral324546219;
extern const uint32_t RectOffset_ToString_m767491965_MetadataUsageId;
extern RuntimeClass* RectTransform_t15861704_il2cpp_TypeInfo_var;
extern RuntimeClass* ReapplyDrivenProperties_t3527255582_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1630831955_MetadataUsageId;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m2621577569_MetadataUsageId;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m1998353017_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3056984367;
extern const uint32_t RectTransform_GetLocalCorners_m4198635943_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2324639295;
extern const uint32_t RectTransform_GetWorldCorners_m2569558680_MetadataUsageId;
extern RuntimeClass* Vector2_t3057062568_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m1866663977_MetadataUsageId;
extern const uint32_t CommandBuffer__ctor_m125783349_MetadataUsageId;
extern const uint32_t CommandBuffer_Dispose_m1676858108_MetadataUsageId;
extern const uint32_t RenderTargetIdentifier__ctor_m1757717066_MetadataUsageId;
extern RuntimeClass* BuiltinRenderTextureType_t3831699340_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2767891638;
extern const uint32_t RenderTargetIdentifier_ToString_m2533785366_MetadataUsageId;
extern const uint32_t RenderTargetIdentifier_GetHashCode_m1270711389_MetadataUsageId;
extern RuntimeClass* RenderTargetIdentifier_t3642434912_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetIdentifier_Equals_m2199170207_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3191453988;
extern const uint32_t SphericalHarmonicsL2_set_Item_m967009223_MetadataUsageId;
extern RuntimeClass* SphericalHarmonicsL2_t298540216_il2cpp_TypeInfo_var;
extern const uint32_t SphericalHarmonicsL2_Equals_m391510961_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral547391191;
extern const uint32_t Resolution_ToString_m85524303_MetadataUsageId;
extern RuntimeClass* Scene_t548444562_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m4035306960_MetadataUsageId;
extern RuntimeClass* SceneManager_t1963070651_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityAction_2_Invoke_m3981208854_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m1765525885_MetadataUsageId;
extern const RuntimeMethod* UnityAction_1_Invoke_m2617017530_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m2727365971_MetadataUsageId;
extern const RuntimeMethod* UnityAction_2_Invoke_m1895819670_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m3720678395_MetadataUsageId;
extern const uint32_t ScriptableObject__ctor_m1532608883_MetadataUsageId;
extern RuntimeClass* SendMouseEvents_t926685678_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m3953679151_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGUILayer_t2694382279_m835163552_RuntimeMethod_var;
extern const uint32_t SendMouseEvents_HitTestLegacyGUI_m2363379606_MetadataUsageId;
extern RuntimeClass* Input_t2963215744_il2cpp_TypeInfo_var;
extern RuntimeClass* CameraU5BU5D_t3897890150_il2cpp_TypeInfo_var;
extern RuntimeClass* HitInfo_t214434488_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m2020259991_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2933606041;
extern Il2CppCodeGenString* _stringLiteral1252746111;
extern Il2CppCodeGenString* _stringLiteral2948729405;
extern Il2CppCodeGenString* _stringLiteral3326067302;
extern Il2CppCodeGenString* _stringLiteral2495155086;
extern Il2CppCodeGenString* _stringLiteral719642620;
extern Il2CppCodeGenString* _stringLiteral4238796895;
extern const uint32_t SendMouseEvents_SendEvents_m4819307_MetadataUsageId;
extern RuntimeClass* HitInfoU5BU5D_t1216529257_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m1195496091_MetadataUsageId;
extern const uint32_t HitInfo_op_Implicit_m4245295910_MetadataUsageId;
extern const uint32_t HitInfo_Compare_m2657684222_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4070250858;
extern Il2CppCodeGenString* _stringLiteral768326453;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m84037963_MetadataUsageId;
extern const uint32_t SetupCoroutine_InvokeMember_m477548565_MetadataUsageId;
extern RuntimeClass* StackTraceUtility_t142931386_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047796672;
extern Il2CppCodeGenString* _stringLiteral618654864;
extern const uint32_t StackTraceUtility_SetProjectFolder_m2901744821_MetadataUsageId;
extern RuntimeClass* StackTrace_t1080444557_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m3776908625_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2718138177;
extern Il2CppCodeGenString* _stringLiteral3270038836;
extern Il2CppCodeGenString* _stringLiteral1161391446;
extern Il2CppCodeGenString* _stringLiteral3758298780;
extern Il2CppCodeGenString* _stringLiteral3366573739;
extern Il2CppCodeGenString* _stringLiteral2996873391;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m2557993735_MetadataUsageId;
extern RuntimeClass* Exception_t2123675094_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t2355629516_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2280038672;
extern Il2CppCodeGenString* _stringLiteral376254785;
extern Il2CppCodeGenString* _stringLiteral1772440553;
extern Il2CppCodeGenString* _stringLiteral572800344;
extern Il2CppCodeGenString* _stringLiteral948631903;
extern Il2CppCodeGenString* _stringLiteral1714897927;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m3565903846_MetadataUsageId;
extern RuntimeClass* CharU5BU5D_t83643201_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral801117164;
extern Il2CppCodeGenString* _stringLiteral165326431;
extern Il2CppCodeGenString* _stringLiteral1038419906;
extern Il2CppCodeGenString* _stringLiteral4277981398;
extern Il2CppCodeGenString* _stringLiteral283101400;
extern Il2CppCodeGenString* _stringLiteral3165837002;
extern Il2CppCodeGenString* _stringLiteral3589461666;
extern Il2CppCodeGenString* _stringLiteral2358762624;
extern Il2CppCodeGenString* _stringLiteral869691469;
extern Il2CppCodeGenString* _stringLiteral898477964;
extern Il2CppCodeGenString* _stringLiteral1035361912;
extern Il2CppCodeGenString* _stringLiteral3711086256;
extern Il2CppCodeGenString* _stringLiteral2088073012;
extern Il2CppCodeGenString* _stringLiteral272318210;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m4079690477_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2072793447;
extern Il2CppCodeGenString* _stringLiteral60326768;
extern Il2CppCodeGenString* _stringLiteral3968012355;
extern Il2CppCodeGenString* _stringLiteral268940233;
extern Il2CppCodeGenString* _stringLiteral1236921323;
extern Il2CppCodeGenString* _stringLiteral2253610359;
extern Il2CppCodeGenString* _stringLiteral2644163874;
extern Il2CppCodeGenString* _stringLiteral1576533761;
extern Il2CppCodeGenString* _stringLiteral1772141335;
extern Il2CppCodeGenString* _stringLiteral4250600190;
extern Il2CppCodeGenString* _stringLiteral3976536586;
extern Il2CppCodeGenString* _stringLiteral3428737307;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m2461990048_MetadataUsageId;
extern const uint32_t StackTraceUtility__cctor_m3261477867_MetadataUsageId;
extern const uint32_t Texture__ctor_m3010839343_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m4114474571_MetadataUsageId;
extern RuntimeClass* Texture2D_t878840578_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral175949560;
extern const uint32_t Texture2D_CreateExternalTexture_m4069785793_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchScreenKeyboardType_t2115689981_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2545527069_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m2368604044_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m3989449005_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m2781171978_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_t3991761301_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m2497313546_MetadataUsageId;
extern const uint32_t TrackedReference_op_Equality_m3187773712_MetadataUsageId;
extern RuntimeClass* TrackedReference_t774033239_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m3554410169_MetadataUsageId;
extern const uint32_t Transform_get_forward_m3951380002_MetadataUsageId;
extern RuntimeClass* Enumerator_t1378995202_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m1659425715_MetadataUsageId;
extern RuntimeClass* SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t1977832595_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SpriteAtlasManager_Register_m3782966546_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m4209780603_RuntimeMethod_var;
extern const uint32_t SpriteAtlasManager_RequestAtlas_m1392089085_MetadataUsageId;
extern const uint32_t SpriteAtlasManager__cctor_m2760532170_MetadataUsageId;
extern RuntimeClass* UnhandledExceptionHandler_t1659926578_il2cpp_TypeInfo_var;
extern RuntimeClass* UnhandledExceptionEventHandler_t2340223587_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnhandledExceptionHandler_HandleUnhandledException_m517656032_RuntimeMethod_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m1434751384_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2415579295;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m517656032_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3822892827;
extern const uint32_t UnhandledExceptionHandler_PrintException_m1818247004_MetadataUsageId;

struct ByteU5BU5D_t3287329517;
struct ObjectU5BU5D_t3487290734;
struct ObjectU5BU5D_t1568665923;
struct PlayableBindingU5BU5D_t3010105947;
struct Vector3U5BU5D_t974944492;
struct CameraU5BU5D_t3897890150;
struct HitInfoU5BU5D_t1216529257;
struct ParameterModifierU5BU5D_t3541603165;
struct StringU5BU5D_t369357837;
struct CharU5BU5D_t83643201;
struct ParameterInfoU5BU5D_t4074997356;
struct Color32U5BU5D_t1505762612;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef PROPERTYNAMEUTILS_T4142570144_H
#define PROPERTYNAMEUTILS_T4142570144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyNameUtils
struct  PropertyNameUtils_t4142570144  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMEUTILS_T4142570144_H
#ifndef RANDOM_T2010332965_H
#define RANDOM_T2010332965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Random
struct  Random_t2010332965  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T2010332965_H
#ifndef EVENTARGS_T3796752705_H
#define EVENTARGS_T3796752705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3796752705  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3796752705_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3796752705 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3796752705_StaticFields, ___Empty_0)); }
	inline EventArgs_t3796752705 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3796752705 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3796752705 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3796752705_H
#ifndef RESOURCES_T3437456709_H
#define RESOURCES_T3437456709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resources
struct  Resources_t3437456709  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCES_T3437456709_H
#ifndef SCENEMANAGER_T1963070651_H
#define SCENEMANAGER_T1963070651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.SceneManager
struct  SceneManager_t1963070651  : public RuntimeObject
{
public:

public:
};

struct SceneManager_t1963070651_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> UnityEngine.SceneManagement.SceneManager::sceneLoaded
	UnityAction_2_t2777655240 * ___sceneLoaded_0;
	// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::sceneUnloaded
	UnityAction_1_t2691974747 * ___sceneUnloaded_1;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::activeSceneChanged
	UnityAction_2_t562236193 * ___activeSceneChanged_2;

public:
	inline static int32_t get_offset_of_sceneLoaded_0() { return static_cast<int32_t>(offsetof(SceneManager_t1963070651_StaticFields, ___sceneLoaded_0)); }
	inline UnityAction_2_t2777655240 * get_sceneLoaded_0() const { return ___sceneLoaded_0; }
	inline UnityAction_2_t2777655240 ** get_address_of_sceneLoaded_0() { return &___sceneLoaded_0; }
	inline void set_sceneLoaded_0(UnityAction_2_t2777655240 * value)
	{
		___sceneLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___sceneLoaded_0), value);
	}

	inline static int32_t get_offset_of_sceneUnloaded_1() { return static_cast<int32_t>(offsetof(SceneManager_t1963070651_StaticFields, ___sceneUnloaded_1)); }
	inline UnityAction_1_t2691974747 * get_sceneUnloaded_1() const { return ___sceneUnloaded_1; }
	inline UnityAction_1_t2691974747 ** get_address_of_sceneUnloaded_1() { return &___sceneUnloaded_1; }
	inline void set_sceneUnloaded_1(UnityAction_1_t2691974747 * value)
	{
		___sceneUnloaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___sceneUnloaded_1), value);
	}

	inline static int32_t get_offset_of_activeSceneChanged_2() { return static_cast<int32_t>(offsetof(SceneManager_t1963070651_StaticFields, ___activeSceneChanged_2)); }
	inline UnityAction_2_t562236193 * get_activeSceneChanged_2() const { return ___activeSceneChanged_2; }
	inline UnityAction_2_t562236193 ** get_address_of_activeSceneChanged_2() { return &___activeSceneChanged_2; }
	inline void set_activeSceneChanged_2(UnityAction_2_t562236193 * value)
	{
		___activeSceneChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeSceneChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGER_T1963070651_H
#ifndef SCREEN_T1210784387_H
#define SCREEN_T1210784387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Screen
struct  Screen_t1210784387  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREEN_T1210784387_H
#ifndef SENDMOUSEEVENTS_T926685678_H
#define SENDMOUSEEVENTS_T926685678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents
struct  SendMouseEvents_t926685678  : public RuntimeObject
{
public:

public:
};

struct SendMouseEvents_t926685678_StaticFields
{
public:
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_t1216529257* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_t1216529257* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_t1216529257* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t3897890150* ___m_Cameras_4;

public:
	inline static int32_t get_offset_of_s_MouseUsed_0() { return static_cast<int32_t>(offsetof(SendMouseEvents_t926685678_StaticFields, ___s_MouseUsed_0)); }
	inline bool get_s_MouseUsed_0() const { return ___s_MouseUsed_0; }
	inline bool* get_address_of_s_MouseUsed_0() { return &___s_MouseUsed_0; }
	inline void set_s_MouseUsed_0(bool value)
	{
		___s_MouseUsed_0 = value;
	}

	inline static int32_t get_offset_of_m_LastHit_1() { return static_cast<int32_t>(offsetof(SendMouseEvents_t926685678_StaticFields, ___m_LastHit_1)); }
	inline HitInfoU5BU5D_t1216529257* get_m_LastHit_1() const { return ___m_LastHit_1; }
	inline HitInfoU5BU5D_t1216529257** get_address_of_m_LastHit_1() { return &___m_LastHit_1; }
	inline void set_m_LastHit_1(HitInfoU5BU5D_t1216529257* value)
	{
		___m_LastHit_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastHit_1), value);
	}

	inline static int32_t get_offset_of_m_MouseDownHit_2() { return static_cast<int32_t>(offsetof(SendMouseEvents_t926685678_StaticFields, ___m_MouseDownHit_2)); }
	inline HitInfoU5BU5D_t1216529257* get_m_MouseDownHit_2() const { return ___m_MouseDownHit_2; }
	inline HitInfoU5BU5D_t1216529257** get_address_of_m_MouseDownHit_2() { return &___m_MouseDownHit_2; }
	inline void set_m_MouseDownHit_2(HitInfoU5BU5D_t1216529257* value)
	{
		___m_MouseDownHit_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseDownHit_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentHit_3() { return static_cast<int32_t>(offsetof(SendMouseEvents_t926685678_StaticFields, ___m_CurrentHit_3)); }
	inline HitInfoU5BU5D_t1216529257* get_m_CurrentHit_3() const { return ___m_CurrentHit_3; }
	inline HitInfoU5BU5D_t1216529257** get_address_of_m_CurrentHit_3() { return &___m_CurrentHit_3; }
	inline void set_m_CurrentHit_3(HitInfoU5BU5D_t1216529257* value)
	{
		___m_CurrentHit_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentHit_3), value);
	}

	inline static int32_t get_offset_of_m_Cameras_4() { return static_cast<int32_t>(offsetof(SendMouseEvents_t926685678_StaticFields, ___m_Cameras_4)); }
	inline CameraU5BU5D_t3897890150* get_m_Cameras_4() const { return ___m_Cameras_4; }
	inline CameraU5BU5D_t3897890150** get_address_of_m_Cameras_4() { return &___m_Cameras_4; }
	inline void set_m_Cameras_4(CameraU5BU5D_t3897890150* value)
	{
		___m_Cameras_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cameras_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMOUSEEVENTS_T926685678_H
#ifndef SETUPCOROUTINE_T2806836092_H
#define SETUPCOROUTINE_T2806836092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SetupCoroutine
struct  SetupCoroutine_t2806836092  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPCOROUTINE_T2806836092_H
#ifndef BINDER_T3602727638_H
#define BINDER_T3602727638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t3602727638  : public RuntimeObject
{
public:

public:
};

struct Binder_t3602727638_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t3602727638 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t3602727638_StaticFields, ___default_binder_0)); }
	inline Binder_t3602727638 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t3602727638 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t3602727638 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T3602727638_H
#ifndef CULTUREINFO_T1870276872_H
#define CULTUREINFO_T1870276872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t1870276872  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t1937290703 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t3472450545 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t4217374101 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t4218043007 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t4092062009* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t1870276872 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t2104464680 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t3287329517* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___numInfo_14)); }
	inline NumberFormatInfo_t1937290703 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t1937290703 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t1937290703 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t3472450545 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t3472450545 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t3472450545 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___textInfo_16)); }
	inline TextInfo_t4217374101 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t4217374101 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t4217374101 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___compareInfo_26)); }
	inline CompareInfo_t4218043007 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t4218043007 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t4218043007 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t4092062009* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t4092062009** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t4092062009* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___parent_culture_30)); }
	inline CultureInfo_t1870276872 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t1870276872 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t1870276872 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___calendar_32)); }
	inline Calendar_t2104464680 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t2104464680 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t2104464680 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t3287329517* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t3287329517** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t3287329517* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t1870276872_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t1870276872 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t3677817270 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t3677817270 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t3491703694 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t1870276872 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t1870276872 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t1870276872 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t3677817270 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t3677817270 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t3677817270 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t3677817270 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t3677817270 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t3677817270 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t1870276872_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t3491703694 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t3491703694 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t3491703694 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T1870276872_H
#ifndef DATAUTILITY_T2599945596_H
#define DATAUTILITY_T2599945596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprites.DataUtility
struct  DataUtility_t2599945596  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAUTILITY_T2599945596_H
#ifndef STACKTRACEUTILITY_T142931386_H
#define STACKTRACEUTILITY_T142931386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StackTraceUtility
struct  StackTraceUtility_t142931386  : public RuntimeObject
{
public:

public:
};

struct StackTraceUtility_t142931386_StaticFields
{
public:
	// System.String UnityEngine.StackTraceUtility::projectFolder
	String_t* ___projectFolder_0;

public:
	inline static int32_t get_offset_of_projectFolder_0() { return static_cast<int32_t>(offsetof(StackTraceUtility_t142931386_StaticFields, ___projectFolder_0)); }
	inline String_t* get_projectFolder_0() const { return ___projectFolder_0; }
	inline String_t** get_address_of_projectFolder_0() { return &___projectFolder_0; }
	inline void set_projectFolder_0(String_t* value)
	{
		___projectFolder_0 = value;
		Il2CppCodeGenWriteBarrier((&___projectFolder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACEUTILITY_T142931386_H
#ifndef STACKTRACE_T1080444557_H
#define STACKTRACE_T1080444557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackTrace
struct  StackTrace_t1080444557  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::frames
	StackFrameU5BU5D_t948874216* ___frames_1;
	// System.Boolean System.Diagnostics.StackTrace::debug_info
	bool ___debug_info_2;

public:
	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(StackTrace_t1080444557, ___frames_1)); }
	inline StackFrameU5BU5D_t948874216* get_frames_1() const { return ___frames_1; }
	inline StackFrameU5BU5D_t948874216** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(StackFrameU5BU5D_t948874216* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_debug_info_2() { return static_cast<int32_t>(offsetof(StackTrace_t1080444557, ___debug_info_2)); }
	inline bool get_debug_info_2() const { return ___debug_info_2; }
	inline bool* get_address_of_debug_info_2() { return &___debug_info_2; }
	inline void set_debug_info_2(bool value)
	{
		___debug_info_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACE_T1080444557_H
#ifndef EXCEPTION_T2123675094_H
#define EXCEPTION_T2123675094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t2123675094  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t2297631236* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t2123675094 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t2297631236* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t2297631236** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t2297631236* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___inner_exception_1)); }
	inline Exception_t2123675094 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t2123675094 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t2123675094 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T2123675094_H
#ifndef STRINGBUILDER_T2355629516_H
#define STRINGBUILDER_T2355629516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t2355629516  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t2355629516, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t2355629516, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t2355629516, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t2355629516, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T2355629516_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STACKFRAME_T795761381_H
#define STACKFRAME_T795761381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackFrame
struct  StackFrame_t795761381  : public RuntimeObject
{
public:
	// System.Int32 System.Diagnostics.StackFrame::ilOffset
	int32_t ___ilOffset_1;
	// System.Int32 System.Diagnostics.StackFrame::nativeOffset
	int32_t ___nativeOffset_2;
	// System.Reflection.MethodBase System.Diagnostics.StackFrame::methodBase
	MethodBase_t3535115348 * ___methodBase_3;
	// System.String System.Diagnostics.StackFrame::fileName
	String_t* ___fileName_4;
	// System.Int32 System.Diagnostics.StackFrame::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 System.Diagnostics.StackFrame::columnNumber
	int32_t ___columnNumber_6;
	// System.String System.Diagnostics.StackFrame::internalMethodName
	String_t* ___internalMethodName_7;

public:
	inline static int32_t get_offset_of_ilOffset_1() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___ilOffset_1)); }
	inline int32_t get_ilOffset_1() const { return ___ilOffset_1; }
	inline int32_t* get_address_of_ilOffset_1() { return &___ilOffset_1; }
	inline void set_ilOffset_1(int32_t value)
	{
		___ilOffset_1 = value;
	}

	inline static int32_t get_offset_of_nativeOffset_2() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___nativeOffset_2)); }
	inline int32_t get_nativeOffset_2() const { return ___nativeOffset_2; }
	inline int32_t* get_address_of_nativeOffset_2() { return &___nativeOffset_2; }
	inline void set_nativeOffset_2(int32_t value)
	{
		___nativeOffset_2 = value;
	}

	inline static int32_t get_offset_of_methodBase_3() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___methodBase_3)); }
	inline MethodBase_t3535115348 * get_methodBase_3() const { return ___methodBase_3; }
	inline MethodBase_t3535115348 ** get_address_of_methodBase_3() { return &___methodBase_3; }
	inline void set_methodBase_3(MethodBase_t3535115348 * value)
	{
		___methodBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___methodBase_3), value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_4), value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_columnNumber_6() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___columnNumber_6)); }
	inline int32_t get_columnNumber_6() const { return ___columnNumber_6; }
	inline int32_t* get_address_of_columnNumber_6() { return &___columnNumber_6; }
	inline void set_columnNumber_6(int32_t value)
	{
		___columnNumber_6 = value;
	}

	inline static int32_t get_offset_of_internalMethodName_7() { return static_cast<int32_t>(offsetof(StackFrame_t795761381, ___internalMethodName_7)); }
	inline String_t* get_internalMethodName_7() const { return ___internalMethodName_7; }
	inline String_t** get_address_of_internalMethodName_7() { return &___internalMethodName_7; }
	inline void set_internalMethodName_7(String_t* value)
	{
		___internalMethodName_7 = value;
		Il2CppCodeGenWriteBarrier((&___internalMethodName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKFRAME_T795761381_H
#ifndef SYSTEMINFO_T1900786052_H
#define SYSTEMINFO_T1900786052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemInfo
struct  SystemInfo_t1900786052  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMINFO_T1900786052_H
#ifndef TIME_T3366507110_H
#define TIME_T3366507110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Time
struct  Time_t3366507110  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T3366507110_H
#ifndef ENUMERATOR_T1378995202_H
#define ENUMERATOR_T1378995202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform/Enumerator
struct  Enumerator_t1378995202  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityEngine.Transform/Enumerator::outer
	Transform_t532597831 * ___outer_0;
	// System.Int32 UnityEngine.Transform/Enumerator::currentIndex
	int32_t ___currentIndex_1;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t1378995202, ___outer_0)); }
	inline Transform_t532597831 * get_outer_0() const { return ___outer_0; }
	inline Transform_t532597831 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Transform_t532597831 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t1378995202, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1378995202_H
#ifndef SPRITEATLASMANAGER_T524371920_H
#define SPRITEATLASMANAGER_T524371920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager
struct  SpriteAtlasManager_t524371920  : public RuntimeObject
{
public:

public:
};

struct SpriteAtlasManager_t524371920_StaticFields
{
public:
	// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback UnityEngine.U2D.SpriteAtlasManager::atlasRequested
	RequestAtlasCallback_t634135197 * ___atlasRequested_0;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.U2D.SpriteAtlasManager::<>f__mg$cache0
	Action_1_t1977832595 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_atlasRequested_0() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t524371920_StaticFields, ___atlasRequested_0)); }
	inline RequestAtlasCallback_t634135197 * get_atlasRequested_0() const { return ___atlasRequested_0; }
	inline RequestAtlasCallback_t634135197 ** get_address_of_atlasRequested_0() { return &___atlasRequested_0; }
	inline void set_atlasRequested_0(RequestAtlasCallback_t634135197 * value)
	{
		___atlasRequested_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasRequested_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t524371920_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Action_1_t1977832595 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Action_1_t1977832595 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Action_1_t1977832595 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLASMANAGER_T524371920_H
#ifndef UNHANDLEDEXCEPTIONHANDLER_T1659926578_H
#define UNHANDLEDEXCEPTIONHANDLER_T1659926578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnhandledExceptionHandler
struct  UnhandledExceptionHandler_t1659926578  : public RuntimeObject
{
public:

public:
};

struct UnhandledExceptionHandler_t1659926578_StaticFields
{
public:
	// System.UnhandledExceptionEventHandler UnityEngine.UnhandledExceptionHandler::<>f__mg$cache0
	UnhandledExceptionEventHandler_t2340223587 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(UnhandledExceptionHandler_t1659926578_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline UnhandledExceptionEventHandler_t2340223587 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline UnhandledExceptionEventHandler_t2340223587 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(UnhandledExceptionEventHandler_t2340223587 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONHANDLER_T1659926578_H
#ifndef VALUETYPE_T1364887298_H
#define VALUETYPE_T1364887298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1364887298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_com
{
};
#endif // VALUETYPE_T1364887298_H
#ifndef UNITYEVENTBASE_T2204896975_H
#define UNITYEVENTBASE_T2204896975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t2204896975  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t1231629294 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t1952737969 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_Calls_0)); }
	inline InvokableCallList_t1231629294 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t1231629294 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t1231629294 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t1952737969 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t1952737969 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t1952737969 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T2204896975_H
#ifndef MARSHALBYREFOBJECT_T532690895_H
#define MARSHALBYREFOBJECT_T532690895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t532690895  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2184608466 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t532690895, ____identity_0)); }
	inline ServerIdentity_t2184608466 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2184608466 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2184608466 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T532690895_H
#ifndef PLAYABLEBEHAVIOUR_T2742200540_H
#define PLAYABLEBEHAVIOUR_T2742200540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t2742200540  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T2742200540_H
#ifndef MESSAGETYPESUBSCRIBERS_T3285514618_H
#define MESSAGETYPESUBSCRIBERS_T3285514618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct  MessageTypeSubscribers_t3285514618  : public RuntimeObject
{
public:
	// System.String UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::m_messageTypeId
	String_t* ___m_messageTypeId_0;
	// System.Int32 UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::subscriberCount
	int32_t ___subscriberCount_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::messageCallback
	MessageEvent_t3155234160 * ___messageCallback_2;

public:
	inline static int32_t get_offset_of_m_messageTypeId_0() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t3285514618, ___m_messageTypeId_0)); }
	inline String_t* get_m_messageTypeId_0() const { return ___m_messageTypeId_0; }
	inline String_t** get_address_of_m_messageTypeId_0() { return &___m_messageTypeId_0; }
	inline void set_m_messageTypeId_0(String_t* value)
	{
		___m_messageTypeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_messageTypeId_0), value);
	}

	inline static int32_t get_offset_of_subscriberCount_1() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t3285514618, ___subscriberCount_1)); }
	inline int32_t get_subscriberCount_1() const { return ___subscriberCount_1; }
	inline int32_t* get_address_of_subscriberCount_1() { return &___subscriberCount_1; }
	inline void set_subscriberCount_1(int32_t value)
	{
		___subscriberCount_1 = value;
	}

	inline static int32_t get_offset_of_messageCallback_2() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t3285514618, ___messageCallback_2)); }
	inline MessageEvent_t3155234160 * get_messageCallback_2() const { return ___messageCallback_2; }
	inline MessageEvent_t3155234160 ** get_address_of_messageCallback_2() { return &___messageCallback_2; }
	inline void set_messageCallback_2(MessageEvent_t3155234160 * value)
	{
		___messageCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPESUBSCRIBERS_T3285514618_H
#ifndef YIELDINSTRUCTION_T3362187334_H
#define YIELDINSTRUCTION_T3362187334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3362187334  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3362187334_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3362187334_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3362187334_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t83643201* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t83643201* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t83643201** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t83643201* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T3095965032_H
#define LIST_1_T3095965032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  List_1_t3095965032  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MessageTypeSubscribersU5BU5D_t1177853791* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3095965032, ____items_1)); }
	inline MessageTypeSubscribersU5BU5D_t1177853791* get__items_1() const { return ____items_1; }
	inline MessageTypeSubscribersU5BU5D_t1177853791** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MessageTypeSubscribersU5BU5D_t1177853791* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3095965032, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3095965032, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3095965032_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MessageTypeSubscribersU5BU5D_t1177853791* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3095965032_StaticFields, ___EmptyArray_4)); }
	inline MessageTypeSubscribersU5BU5D_t1177853791* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MessageTypeSubscribersU5BU5D_t1177853791** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MessageTypeSubscribersU5BU5D_t1177853791* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3095965032_H
#ifndef LIST_1_T309455265_H
#define LIST_1_T309455265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t309455265  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t3565237794* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____items_1)); }
	inline Int32U5BU5D_t3565237794* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t3565237794** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t3565237794* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t309455265_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t3565237794* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t309455265_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t3565237794* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t3565237794** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t3565237794* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T309455265_H
#ifndef ATTRIBUTE_T3852256153_H
#define ATTRIBUTE_T3852256153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t3852256153  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T3852256153_H
#ifndef MESSAGEEVENTARGS_T2086846075_H
#define MESSAGEEVENTARGS_T2086846075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t2086846075  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t3287329517* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2086846075, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2086846075, ___data_1)); }
	inline ByteU5BU5D_t3287329517* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3287329517** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3287329517* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2086846075_H
#ifndef PLAYERCONNECTIONINTERNAL_T984574678_H
#define PLAYERCONNECTIONINTERNAL_T984574678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayerConnectionInternal
struct  PlayerConnectionInternal_t984574678  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTIONINTERNAL_T984574678_H
#ifndef PLAYEREDITORCONNECTIONEVENTS_T3406496741_H
#define PLAYEREDITORCONNECTIONEVENTS_T3406496741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct  PlayerEditorConnectionEvents_t3406496741  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::messageTypeSubscribers
	List_1_t3095965032 * ___messageTypeSubscribers_0;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::connectionEvent
	ConnectionChangeEvent_t1857494072 * ___connectionEvent_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::disconnectionEvent
	ConnectionChangeEvent_t1857494072 * ___disconnectionEvent_2;

public:
	inline static int32_t get_offset_of_messageTypeSubscribers_0() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t3406496741, ___messageTypeSubscribers_0)); }
	inline List_1_t3095965032 * get_messageTypeSubscribers_0() const { return ___messageTypeSubscribers_0; }
	inline List_1_t3095965032 ** get_address_of_messageTypeSubscribers_0() { return &___messageTypeSubscribers_0; }
	inline void set_messageTypeSubscribers_0(List_1_t3095965032 * value)
	{
		___messageTypeSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageTypeSubscribers_0), value);
	}

	inline static int32_t get_offset_of_connectionEvent_1() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t3406496741, ___connectionEvent_1)); }
	inline ConnectionChangeEvent_t1857494072 * get_connectionEvent_1() const { return ___connectionEvent_1; }
	inline ConnectionChangeEvent_t1857494072 ** get_address_of_connectionEvent_1() { return &___connectionEvent_1; }
	inline void set_connectionEvent_1(ConnectionChangeEvent_t1857494072 * value)
	{
		___connectionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEvent_1), value);
	}

	inline static int32_t get_offset_of_disconnectionEvent_2() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t3406496741, ___disconnectionEvent_2)); }
	inline ConnectionChangeEvent_t1857494072 * get_disconnectionEvent_2() const { return ___disconnectionEvent_2; }
	inline ConnectionChangeEvent_t1857494072 ** get_address_of_disconnectionEvent_2() { return &___disconnectionEvent_2; }
	inline void set_disconnectionEvent_2(ConnectionChangeEvent_t1857494072 * value)
	{
		___disconnectionEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectionEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREDITORCONNECTIONEVENTS_T3406496741_H
#ifndef SELECTIONBASEATTRIBUTE_T1772378422_H
#define SELECTIONBASEATTRIBUTE_T1772378422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SelectionBaseAttribute
struct  SelectionBaseAttribute_t1772378422  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBASEATTRIBUTE_T1772378422_H
#ifndef PARAMETERMODIFIER_T1946944212_H
#define PARAMETERMODIFIER_T1946944212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t1946944212 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t2577425931* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t1946944212, ____byref_0)); }
	inline BooleanU5BU5D_t2577425931* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t2577425931** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t2577425931* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1946944212_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1946944212_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T1946944212_H
#ifndef UNITYEVENT_1_T1359409152_H
#define UNITYEVENT_1_T1359409152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityEvent_1_t1359409152  : public UnityEventBase_t2204896975
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t1568665923* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1359409152, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t1568665923* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t1568665923** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t1568665923* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1359409152_H
#ifndef SERIALIZEPRIVATEVARIABLES_T2401626249_H
#define SERIALIZEPRIVATEVARIABLES_T2401626249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializePrivateVariables
struct  SerializePrivateVariables_t2401626249  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEPRIVATEVARIABLES_T2401626249_H
#ifndef SERIALIZEFIELD_T1434498030_H
#define SERIALIZEFIELD_T1434498030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializeField
struct  SerializeField_t1434498030  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEFIELD_T1434498030_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T55868289_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T55868289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t55868289  : public Attribute_t3852256153
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t55868289, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T55868289_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T3769384580_H
#define USEDBYNATIVECODEATTRIBUTE_T3769384580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t3769384580  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T3769384580_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T917163987_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T917163987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t917163987  : public Attribute_t3852256153
{
public:
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t917163987, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t917163987, ___U3COptionalU3Ek__BackingField_1)); }
	inline bool get_U3COptionalU3Ek__BackingField_1() const { return ___U3COptionalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_1() { return &___U3COptionalU3Ek__BackingField_1; }
	inline void set_U3COptionalU3Ek__BackingField_1(bool value)
	{
		___U3COptionalU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T917163987_H
#ifndef GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1698167202_H
#define GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1698167202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct  GeneratedByOldBindingsGeneratorAttribute_t1698167202  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1698167202_H
#ifndef ENUMERATOR_T1909216310_H
#define ENUMERATOR_T1909216310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t1909216310 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t309455265 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1909216310, ___l_0)); }
	inline List_1_t309455265 * get_l_0() const { return ___l_0; }
	inline List_1_t309455265 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t309455265 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1909216310, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1909216310, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1909216310, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1909216310_H
#ifndef INT32_T499004851_H
#define INT32_T499004851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t499004851 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t499004851, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T499004851_H
#ifndef SCENE_T548444562_H
#define SCENE_T548444562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t548444562 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t548444562, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T548444562_H
#ifndef UNITYEVENT_1_T4066535224_H
#define UNITYEVENT_1_T4066535224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t4066535224  : public UnityEventBase_t2204896975
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t1568665923* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4066535224, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t1568665923* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t1568665923** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t1568665923* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4066535224_H
#ifndef HITINFO_T214434488_H
#define HITINFO_T214434488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t214434488 
{
public:
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t1318052361 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t989002943 * ___camera_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(HitInfo_t214434488, ___target_0)); }
	inline GameObject_t1318052361 * get_target_0() const { return ___target_0; }
	inline GameObject_t1318052361 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1318052361 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(HitInfo_t214434488, ___camera_1)); }
	inline Camera_t989002943 * get_camera_1() const { return ___camera_1; }
	inline Camera_t989002943 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t989002943 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t214434488_marshaled_pinvoke
{
	GameObject_t1318052361 * ___target_0;
	Camera_t989002943 * ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t214434488_marshaled_com
{
	GameObject_t1318052361 * ___target_0;
	Camera_t989002943 * ___camera_1;
};
#endif // HITINFO_T214434488_H
#ifndef SORTINGLAYER_T3699874318_H
#define SORTINGLAYER_T3699874318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SortingLayer
struct  SortingLayer_t3699874318 
{
public:
	// System.Int32 UnityEngine.SortingLayer::m_Id
	int32_t ___m_Id_0;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(SortingLayer_t3699874318, ___m_Id_0)); }
	inline int32_t get_m_Id_0() const { return ___m_Id_0; }
	inline int32_t* get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(int32_t value)
	{
		___m_Id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYER_T3699874318_H
#ifndef BOOLEAN_T569405246_H
#define BOOLEAN_T569405246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t569405246 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t569405246, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t569405246_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T569405246_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2116050852 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2116050852 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2116050852 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2116050852 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2116050852 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2116050852 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2116050852 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2116050852 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef NATIVECLASSATTRIBUTE_T4188729888_H
#define NATIVECLASSATTRIBUTE_T4188729888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NativeClassAttribute
struct  NativeClassAttribute_t4188729888  : public Attribute_t3852256153
{
public:
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeClassAttribute_t4188729888, ___U3CQualifiedNativeNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CQualifiedNativeNameU3Ek__BackingField_0() const { return ___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return &___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline void set_U3CQualifiedNativeNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CQualifiedNativeNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQualifiedNativeNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECLASSATTRIBUTE_T4188729888_H
#ifndef SYSTEMEXCEPTION_T4228135144_H
#define SYSTEMEXCEPTION_T4228135144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t4228135144  : public Exception_t2123675094
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T4228135144_H
#ifndef ENUM_T3173835468_H
#define ENUM_T3173835468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t3173835468  : public ValueType_t1364887298
{
public:

public:
};

struct Enum_t3173835468_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t83643201* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t3173835468_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t83643201* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t83643201** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t83643201* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t3173835468_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t3173835468_marshaled_com
{
};
#endif // ENUM_T3173835468_H
#ifndef VOID_T2642135423_H
#define VOID_T2642135423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2642135423 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2642135423_H
#ifndef UNHANDLEDEXCEPTIONEVENTARGS_T4227701455_H
#define UNHANDLEDEXCEPTIONEVENTARGS_T4227701455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventArgs
struct  UnhandledExceptionEventArgs_t4227701455  : public EventArgs_t3796752705
{
public:
	// System.Object System.UnhandledExceptionEventArgs::exception
	RuntimeObject * ___exception_1;
	// System.Boolean System.UnhandledExceptionEventArgs::m_isTerminating
	bool ___m_isTerminating_2;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t4227701455, ___exception_1)); }
	inline RuntimeObject * get_exception_1() const { return ___exception_1; }
	inline RuntimeObject ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(RuntimeObject * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_m_isTerminating_2() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t4227701455, ___m_isTerminating_2)); }
	inline bool get_m_isTerminating_2() const { return ___m_isTerminating_2; }
	inline bool* get_address_of_m_isTerminating_2() { return &___m_isTerminating_2; }
	inline void set_m_isTerminating_2(bool value)
	{
		___m_isTerminating_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTARGS_T4227701455_H
#ifndef MATRIX4X4_T2375577114_H
#define MATRIX4X4_T2375577114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2375577114 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2375577114_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2375577114  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2375577114  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2375577114  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2375577114 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2375577114  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2375577114  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2375577114 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2375577114  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2375577114_H
#ifndef VECTOR4_T380635127_H
#define VECTOR4_T380635127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t380635127 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t380635127_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t380635127  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t380635127  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t380635127  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t380635127  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___zeroVector_5)); }
	inline Vector4_t380635127  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t380635127 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t380635127  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___oneVector_6)); }
	inline Vector4_t380635127  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t380635127 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t380635127  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t380635127  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t380635127 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t380635127  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t380635127  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t380635127 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t380635127  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T380635127_H
#ifndef UINT32_T3311932136_H
#define UINT32_T3311932136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3311932136 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3311932136, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3311932136_H
#ifndef THREADANDSERIALIZATIONSAFEATTRIBUTE_T46731272_H
#define THREADANDSERIALIZATIONSAFEATTRIBUTE_T46731272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadAndSerializationSafeAttribute
struct  ThreadAndSerializationSafeAttribute_t46731272  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADANDSERIALIZATIONSAFEATTRIBUTE_T46731272_H
#ifndef COLOR32_T2788147849_H
#define COLOR32_T2788147849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2788147849 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2788147849_H
#ifndef COLOR_T460381780_H
#define COLOR_T460381780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t460381780 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t460381780, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t460381780, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t460381780, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t460381780, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T460381780_H
#ifndef METHODBASE_T3535115348_H
#define METHODBASE_T3535115348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3535115348  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3535115348_H
#ifndef CHAR_T2157028800_H
#define CHAR_T2157028800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t2157028800 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t2157028800, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t2157028800_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t2157028800_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T2157028800_H
#ifndef REQUIRECOMPONENT_T4152270991_H
#define REQUIRECOMPONENT_T4152270991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t4152270991  : public Attribute_t3852256153
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T4152270991_H
#ifndef TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T934812271_H
#define TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T934812271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
struct  TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271 
{
public:
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::keyboardType
	uint32_t ___keyboardType_0;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::autocorrection
	uint32_t ___autocorrection_1;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::multiline
	uint32_t ___multiline_2;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::secure
	uint32_t ___secure_3;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::alert
	uint32_t ___alert_4;

public:
	inline static int32_t get_offset_of_keyboardType_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271, ___keyboardType_0)); }
	inline uint32_t get_keyboardType_0() const { return ___keyboardType_0; }
	inline uint32_t* get_address_of_keyboardType_0() { return &___keyboardType_0; }
	inline void set_keyboardType_0(uint32_t value)
	{
		___keyboardType_0 = value;
	}

	inline static int32_t get_offset_of_autocorrection_1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271, ___autocorrection_1)); }
	inline uint32_t get_autocorrection_1() const { return ___autocorrection_1; }
	inline uint32_t* get_address_of_autocorrection_1() { return &___autocorrection_1; }
	inline void set_autocorrection_1(uint32_t value)
	{
		___autocorrection_1 = value;
	}

	inline static int32_t get_offset_of_multiline_2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271, ___multiline_2)); }
	inline uint32_t get_multiline_2() const { return ___multiline_2; }
	inline uint32_t* get_address_of_multiline_2() { return &___multiline_2; }
	inline void set_multiline_2(uint32_t value)
	{
		___multiline_2 = value;
	}

	inline static int32_t get_offset_of_secure_3() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271, ___secure_3)); }
	inline uint32_t get_secure_3() const { return ___secure_3; }
	inline uint32_t* get_address_of_secure_3() { return &___secure_3; }
	inline void set_secure_3(uint32_t value)
	{
		___secure_3 = value;
	}

	inline static int32_t get_offset_of_alert_4() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271, ___alert_4)); }
	inline uint32_t get_alert_4() const { return ___alert_4; }
	inline uint32_t* get_address_of_alert_4() { return &___alert_4; }
	inline void set_alert_4(uint32_t value)
	{
		___alert_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T934812271_H
#ifndef SPHERICALHARMONICSL2_T298540216_H
#define SPHERICALHARMONICSL2_T298540216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.SphericalHarmonicsL2
struct  SphericalHarmonicsL2_t298540216 
{
public:
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr0
	float ___shr0_0;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr1
	float ___shr1_1;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr2
	float ___shr2_2;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr3
	float ___shr3_3;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr4
	float ___shr4_4;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr5
	float ___shr5_5;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr6
	float ___shr6_6;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr7
	float ___shr7_7;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr8
	float ___shr8_8;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg0
	float ___shg0_9;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg1
	float ___shg1_10;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg2
	float ___shg2_11;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg3
	float ___shg3_12;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg4
	float ___shg4_13;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg5
	float ___shg5_14;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg6
	float ___shg6_15;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg7
	float ___shg7_16;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg8
	float ___shg8_17;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb0
	float ___shb0_18;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb1
	float ___shb1_19;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb2
	float ___shb2_20;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb3
	float ___shb3_21;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb4
	float ___shb4_22;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb5
	float ___shb5_23;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb6
	float ___shb6_24;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb7
	float ___shb7_25;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb8
	float ___shb8_26;

public:
	inline static int32_t get_offset_of_shr0_0() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr0_0)); }
	inline float get_shr0_0() const { return ___shr0_0; }
	inline float* get_address_of_shr0_0() { return &___shr0_0; }
	inline void set_shr0_0(float value)
	{
		___shr0_0 = value;
	}

	inline static int32_t get_offset_of_shr1_1() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr1_1)); }
	inline float get_shr1_1() const { return ___shr1_1; }
	inline float* get_address_of_shr1_1() { return &___shr1_1; }
	inline void set_shr1_1(float value)
	{
		___shr1_1 = value;
	}

	inline static int32_t get_offset_of_shr2_2() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr2_2)); }
	inline float get_shr2_2() const { return ___shr2_2; }
	inline float* get_address_of_shr2_2() { return &___shr2_2; }
	inline void set_shr2_2(float value)
	{
		___shr2_2 = value;
	}

	inline static int32_t get_offset_of_shr3_3() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr3_3)); }
	inline float get_shr3_3() const { return ___shr3_3; }
	inline float* get_address_of_shr3_3() { return &___shr3_3; }
	inline void set_shr3_3(float value)
	{
		___shr3_3 = value;
	}

	inline static int32_t get_offset_of_shr4_4() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr4_4)); }
	inline float get_shr4_4() const { return ___shr4_4; }
	inline float* get_address_of_shr4_4() { return &___shr4_4; }
	inline void set_shr4_4(float value)
	{
		___shr4_4 = value;
	}

	inline static int32_t get_offset_of_shr5_5() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr5_5)); }
	inline float get_shr5_5() const { return ___shr5_5; }
	inline float* get_address_of_shr5_5() { return &___shr5_5; }
	inline void set_shr5_5(float value)
	{
		___shr5_5 = value;
	}

	inline static int32_t get_offset_of_shr6_6() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr6_6)); }
	inline float get_shr6_6() const { return ___shr6_6; }
	inline float* get_address_of_shr6_6() { return &___shr6_6; }
	inline void set_shr6_6(float value)
	{
		___shr6_6 = value;
	}

	inline static int32_t get_offset_of_shr7_7() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr7_7)); }
	inline float get_shr7_7() const { return ___shr7_7; }
	inline float* get_address_of_shr7_7() { return &___shr7_7; }
	inline void set_shr7_7(float value)
	{
		___shr7_7 = value;
	}

	inline static int32_t get_offset_of_shr8_8() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr8_8)); }
	inline float get_shr8_8() const { return ___shr8_8; }
	inline float* get_address_of_shr8_8() { return &___shr8_8; }
	inline void set_shr8_8(float value)
	{
		___shr8_8 = value;
	}

	inline static int32_t get_offset_of_shg0_9() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg0_9)); }
	inline float get_shg0_9() const { return ___shg0_9; }
	inline float* get_address_of_shg0_9() { return &___shg0_9; }
	inline void set_shg0_9(float value)
	{
		___shg0_9 = value;
	}

	inline static int32_t get_offset_of_shg1_10() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg1_10)); }
	inline float get_shg1_10() const { return ___shg1_10; }
	inline float* get_address_of_shg1_10() { return &___shg1_10; }
	inline void set_shg1_10(float value)
	{
		___shg1_10 = value;
	}

	inline static int32_t get_offset_of_shg2_11() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg2_11)); }
	inline float get_shg2_11() const { return ___shg2_11; }
	inline float* get_address_of_shg2_11() { return &___shg2_11; }
	inline void set_shg2_11(float value)
	{
		___shg2_11 = value;
	}

	inline static int32_t get_offset_of_shg3_12() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg3_12)); }
	inline float get_shg3_12() const { return ___shg3_12; }
	inline float* get_address_of_shg3_12() { return &___shg3_12; }
	inline void set_shg3_12(float value)
	{
		___shg3_12 = value;
	}

	inline static int32_t get_offset_of_shg4_13() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg4_13)); }
	inline float get_shg4_13() const { return ___shg4_13; }
	inline float* get_address_of_shg4_13() { return &___shg4_13; }
	inline void set_shg4_13(float value)
	{
		___shg4_13 = value;
	}

	inline static int32_t get_offset_of_shg5_14() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg5_14)); }
	inline float get_shg5_14() const { return ___shg5_14; }
	inline float* get_address_of_shg5_14() { return &___shg5_14; }
	inline void set_shg5_14(float value)
	{
		___shg5_14 = value;
	}

	inline static int32_t get_offset_of_shg6_15() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg6_15)); }
	inline float get_shg6_15() const { return ___shg6_15; }
	inline float* get_address_of_shg6_15() { return &___shg6_15; }
	inline void set_shg6_15(float value)
	{
		___shg6_15 = value;
	}

	inline static int32_t get_offset_of_shg7_16() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg7_16)); }
	inline float get_shg7_16() const { return ___shg7_16; }
	inline float* get_address_of_shg7_16() { return &___shg7_16; }
	inline void set_shg7_16(float value)
	{
		___shg7_16 = value;
	}

	inline static int32_t get_offset_of_shg8_17() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg8_17)); }
	inline float get_shg8_17() const { return ___shg8_17; }
	inline float* get_address_of_shg8_17() { return &___shg8_17; }
	inline void set_shg8_17(float value)
	{
		___shg8_17 = value;
	}

	inline static int32_t get_offset_of_shb0_18() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb0_18)); }
	inline float get_shb0_18() const { return ___shb0_18; }
	inline float* get_address_of_shb0_18() { return &___shb0_18; }
	inline void set_shb0_18(float value)
	{
		___shb0_18 = value;
	}

	inline static int32_t get_offset_of_shb1_19() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb1_19)); }
	inline float get_shb1_19() const { return ___shb1_19; }
	inline float* get_address_of_shb1_19() { return &___shb1_19; }
	inline void set_shb1_19(float value)
	{
		___shb1_19 = value;
	}

	inline static int32_t get_offset_of_shb2_20() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb2_20)); }
	inline float get_shb2_20() const { return ___shb2_20; }
	inline float* get_address_of_shb2_20() { return &___shb2_20; }
	inline void set_shb2_20(float value)
	{
		___shb2_20 = value;
	}

	inline static int32_t get_offset_of_shb3_21() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb3_21)); }
	inline float get_shb3_21() const { return ___shb3_21; }
	inline float* get_address_of_shb3_21() { return &___shb3_21; }
	inline void set_shb3_21(float value)
	{
		___shb3_21 = value;
	}

	inline static int32_t get_offset_of_shb4_22() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb4_22)); }
	inline float get_shb4_22() const { return ___shb4_22; }
	inline float* get_address_of_shb4_22() { return &___shb4_22; }
	inline void set_shb4_22(float value)
	{
		___shb4_22 = value;
	}

	inline static int32_t get_offset_of_shb5_23() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb5_23)); }
	inline float get_shb5_23() const { return ___shb5_23; }
	inline float* get_address_of_shb5_23() { return &___shb5_23; }
	inline void set_shb5_23(float value)
	{
		___shb5_23 = value;
	}

	inline static int32_t get_offset_of_shb6_24() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb6_24)); }
	inline float get_shb6_24() const { return ___shb6_24; }
	inline float* get_address_of_shb6_24() { return &___shb6_24; }
	inline void set_shb6_24(float value)
	{
		___shb6_24 = value;
	}

	inline static int32_t get_offset_of_shb7_25() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb7_25)); }
	inline float get_shb7_25() const { return ___shb7_25; }
	inline float* get_address_of_shb7_25() { return &___shb7_25; }
	inline void set_shb7_25(float value)
	{
		___shb7_25 = value;
	}

	inline static int32_t get_offset_of_shb8_26() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb8_26)); }
	inline float get_shb8_26() const { return ___shb8_26; }
	inline float* get_address_of_shb8_26() { return &___shb8_26; }
	inline void set_shb8_26(float value)
	{
		___shb8_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERICALHARMONICSL2_T298540216_H
#ifndef RESOLUTION_T1041838152_H
#define RESOLUTION_T1041838152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resolution
struct  Resolution_t1041838152 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t1041838152, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T1041838152_H
#ifndef RECT_T1344834245_H
#define RECT_T1344834245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1344834245 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1344834245_H
#ifndef VECTOR3_T329709361_H
#define VECTOR3_T329709361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t329709361 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t329709361_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t329709361  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t329709361  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t329709361  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t329709361  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t329709361  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t329709361  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t329709361  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t329709361  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t329709361  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t329709361  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___zeroVector_4)); }
	inline Vector3_t329709361  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t329709361 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t329709361  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___oneVector_5)); }
	inline Vector3_t329709361  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t329709361 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t329709361  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___upVector_6)); }
	inline Vector3_t329709361  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t329709361 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t329709361  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___downVector_7)); }
	inline Vector3_t329709361  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t329709361 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t329709361  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___leftVector_8)); }
	inline Vector3_t329709361  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t329709361 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t329709361  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___rightVector_9)); }
	inline Vector3_t329709361  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t329709361 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t329709361  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___forwardVector_10)); }
	inline Vector3_t329709361  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t329709361 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t329709361  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___backVector_11)); }
	inline Vector3_t329709361  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t329709361 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t329709361  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t329709361  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t329709361 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t329709361  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t329709361  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t329709361 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t329709361  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T329709361_H
#ifndef BYTE_T2815932036_H
#define BYTE_T2815932036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t2815932036 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t2815932036, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T2815932036_H
#ifndef PROPERTYATTRIBUTE_T69305138_H
#define PROPERTYATTRIBUTE_T69305138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t69305138  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T69305138_H
#ifndef RANGEINT_T3767345128_H
#define RANGEINT_T3767345128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeInt
struct  RangeInt_t3767345128 
{
public:
	// System.Int32 UnityEngine.RangeInt::start
	int32_t ___start_0;
	// System.Int32 UnityEngine.RangeInt::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RangeInt_t3767345128, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RangeInt_t3767345128, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEINT_T3767345128_H
#ifndef DOUBLE_T2078998952_H
#define DOUBLE_T2078998952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t2078998952 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t2078998952, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T2078998952_H
#ifndef PREFERBINARYSERIALIZATION_T1732391879_H
#define PREFERBINARYSERIALIZATION_T1732391879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PreferBinarySerialization
struct  PreferBinarySerialization_t1732391879  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERBINARYSERIALIZATION_T1732391879_H
#ifndef PROPERTYNAME_T3644065956_H
#define PROPERTYNAME_T3644065956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3644065956 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3644065956, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3644065956_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T3057062568_H
#define VECTOR2_T3057062568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3057062568 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3057062568, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3057062568, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3057062568_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3057062568  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3057062568  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3057062568  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3057062568  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3057062568  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3057062568  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3057062568  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3057062568  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3057062568  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3057062568 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3057062568  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___oneVector_3)); }
	inline Vector2_t3057062568  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3057062568 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3057062568  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___upVector_4)); }
	inline Vector2_t3057062568  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3057062568 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3057062568  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___downVector_5)); }
	inline Vector2_t3057062568  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3057062568 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3057062568  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___leftVector_6)); }
	inline Vector2_t3057062568  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3057062568 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3057062568  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___rightVector_7)); }
	inline Vector2_t3057062568  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3057062568 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3057062568  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3057062568  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3057062568 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3057062568  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3057062568  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3057062568 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3057062568  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3057062568_H
#ifndef SINGLE_T1863352746_H
#define SINGLE_T1863352746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1863352746 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1863352746, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1863352746_H
#ifndef QUATERNION_T2761156409_H
#define QUATERNION_T2761156409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2761156409 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2761156409_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2761156409  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2761156409  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2761156409 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2761156409  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2761156409_H
#ifndef UINT64_T96233451_H
#define UINT64_T96233451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t96233451 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t96233451, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T96233451_H
#ifndef TOUCHPHASE_T1888277914_H
#define TOUCHPHASE_T1888277914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t1888277914 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t1888277914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T1888277914_H
#ifndef PLANE_T1350213727_H
#define PLANE_T1350213727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1350213727 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t329709361  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1350213727, ___m_Normal_0)); }
	inline Vector3_t329709361  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t329709361 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t329709361  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1350213727, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1350213727_H
#ifndef SENDMESSAGEOPTIONS_T2857403360_H
#define SENDMESSAGEOPTIONS_T2857403360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t2857403360 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t2857403360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T2857403360_H
#ifndef CAMERACLEARFLAGS_T1635293836_H
#define CAMERACLEARFLAGS_T1635293836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t1635293836 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t1635293836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T1635293836_H
#ifndef RANGEATTRIBUTE_T2570355765_H
#define RANGEATTRIBUTE_T2570355765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeAttribute
struct  RangeAttribute_t2570355765  : public PropertyAttribute_t69305138
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t2570355765, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t2570355765, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEATTRIBUTE_T2570355765_H
#ifndef ARGUMENTEXCEPTION_T3637419113_H
#define ARGUMENTEXCEPTION_T3637419113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3637419113  : public SystemException_t4228135144
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3637419113, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3637419113_H
#ifndef TOOLTIPATTRIBUTE_T2327649817_H
#define TOOLTIPATTRIBUTE_T2327649817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t2327649817  : public PropertyAttribute_t69305138
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t2327649817, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T2327649817_H
#ifndef SPACEATTRIBUTE_T4105298238_H
#define SPACEATTRIBUTE_T4105298238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t4105298238  : public PropertyAttribute_t69305138
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t4105298238, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEATTRIBUTE_T4105298238_H
#ifndef PLAYABLEHANDLE_T3963382032_H
#define PLAYABLEHANDLE_T3963382032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t3963382032 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t3963382032, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t3963382032, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T3963382032_H
#ifndef OPERATINGSYSTEMFAMILY_T3477500013_H
#define OPERATINGSYSTEMFAMILY_T3477500013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_t3477500013 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_t3477500013, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_T3477500013_H
#ifndef TEXTUREFORMAT_T2369697549_H
#define TEXTUREFORMAT_T2369697549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2369697549 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2369697549, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2369697549_H
#ifndef PLAYABLEOUTPUTHANDLE_T1112988996_H
#define PLAYABLEOUTPUTHANDLE_T1112988996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t1112988996 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t1112988996, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t1112988996, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T1112988996_H
#ifndef HIDEFLAGS_T3447093715_H
#define HIDEFLAGS_T3447093715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t3447093715 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t3447093715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T3447093715_H
#ifndef TEXTUREWRAPMODE_T287651694_H
#define TEXTUREWRAPMODE_T287651694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t287651694 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t287651694, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T287651694_H
#ifndef OBJECT_T1970767703_H
#define OBJECT_T1970767703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1970767703  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1970767703, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1970767703_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1970767703_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1970767703_H
#ifndef FILTERMODE_T4175348091_H
#define FILTERMODE_T4175348091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t4175348091 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t4175348091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T4175348091_H
#ifndef BINDINGFLAGS_T4292999562_H
#define BINDINGFLAGS_T4292999562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t4292999562 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t4292999562, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T4292999562_H
#ifndef TEXTAREAATTRIBUTE_T1594486658_H
#define TEXTAREAATTRIBUTE_T1594486658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAreaAttribute
struct  TextAreaAttribute_t1594486658  : public PropertyAttribute_t69305138
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t1594486658, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t1594486658, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAATTRIBUTE_T1594486658_H
#ifndef U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T545998035_H
#define U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T545998035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct  U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T545998035_H
#ifndef TOUCHTYPE_T3597616217_H
#define TOUCHTYPE_T3597616217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t3597616217 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t3597616217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T3597616217_H
#ifndef MATERIALPROPERTYBLOCK_T3417007309_H
#define MATERIALPROPERTYBLOCK_T3417007309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t3417007309  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t3417007309, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T3417007309_H
#ifndef AMBIENTMODE_T825416228_H
#define AMBIENTMODE_T825416228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.AmbientMode
struct  AmbientMode_t825416228 
{
public:
	// System.Int32 UnityEngine.Rendering.AmbientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AmbientMode_t825416228, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTMODE_T825416228_H
#ifndef BUILTINRENDERTEXTURETYPE_T3831699340_H
#define BUILTINRENDERTEXTURETYPE_T3831699340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t3831699340 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t3831699340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T3831699340_H
#ifndef CAMERAEVENT_T2770737965_H
#define CAMERAEVENT_T2770737965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t2770737965 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t2770737965, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T2770737965_H
#ifndef COLORWRITEMASK_T3990226935_H
#define COLORWRITEMASK_T3990226935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t3990226935 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWriteMask_t3990226935, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWRITEMASK_T3990226935_H
#ifndef COMMANDBUFFER_T2673047760_H
#define COMMANDBUFFER_T2673047760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t2673047760  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t2673047760, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T2673047760_H
#ifndef RAY_T1448970001_H
#define RAY_T1448970001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1448970001 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t329709361  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t329709361  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1448970001, ___m_Origin_0)); }
	inline Vector3_t329709361  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t329709361 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t329709361  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1448970001, ___m_Direction_1)); }
	inline Vector3_t329709361  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t329709361 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t329709361  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1448970001_H
#ifndef TRACKEDREFERENCE_T774033239_H
#define TRACKEDREFERENCE_T774033239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t774033239  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t774033239, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t774033239_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t774033239_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T774033239_H
#ifndef COMPAREFUNCTION_T1054283604_H
#define COMPAREFUNCTION_T1054283604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t1054283604 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareFunction_t1054283604, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREFUNCTION_T1054283604_H
#ifndef CUBEMAPFACE_T4057844954_H
#define CUBEMAPFACE_T4057844954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t4057844954 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t4057844954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T4057844954_H
#ifndef PRINCIPALPOLICY_T2115956893_H
#define PRINCIPALPOLICY_T2115956893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.PrincipalPolicy
struct  PrincipalPolicy_t2115956893 
{
public:
	// System.Int32 System.Security.Principal.PrincipalPolicy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrincipalPolicy_t2115956893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINCIPALPOLICY_T2115956893_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#define INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t3921978895  : public SystemException_t4228135144
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#ifndef STENCILOP_T1413144161_H
#define STENCILOP_T1413144161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.StencilOp
struct  StencilOp_t1413144161 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StencilOp_t1413144161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILOP_T1413144161_H
#ifndef RECTOFFSET_T83369214_H
#define RECTOFFSET_T83369214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t83369214  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t83369214, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t83369214, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t83369214_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t83369214_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T83369214_H
#ifndef DATASTREAMTYPE_T2115096334_H
#define DATASTREAMTYPE_T2115096334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t2115096334 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t2115096334, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T2115096334_H
#ifndef AXIS_T842534531_H
#define AXIS_T842534531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Axis
struct  Axis_t842534531 
{
public:
	// System.Int32 UnityEngine.RectTransform/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t842534531, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T842534531_H
#ifndef DELEGATE_T1563516729_H
#define DELEGATE_T1563516729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1563516729  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t975501551 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___data_8)); }
	inline DelegateData_t975501551 * get_data_8() const { return ___data_8; }
	inline DelegateData_t975501551 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t975501551 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1563516729_H
#ifndef U3CADDANDCREATEU3EC__ANONSTOREY1_T2037887867_H
#define U3CADDANDCREATEU3EC__ANONSTOREY1_T2037887867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1
struct  U3CAddAndCreateU3Ec__AnonStorey1_t2037887867  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CAddAndCreateU3Ec__AnonStorey1_t2037887867, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDANDCREATEU3EC__ANONSTOREY1_T2037887867_H
#ifndef RUNTIMETYPEHANDLE_T3762232676_H
#define RUNTIMETYPEHANDLE_T3762232676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3762232676 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3762232676, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3762232676_H
#ifndef ASYNCOPERATION_T1227466744_H
#define ASYNCOPERATION_T1227466744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1227466744  : public YieldInstruction_t3362187334
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t67027751 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1227466744, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1227466744, ___m_completeCallback_1)); }
	inline Action_1_t67027751 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t67027751 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t67027751 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744_marshaled_pinvoke : public YieldInstruction_t3362187334_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744_marshaled_com : public YieldInstruction_t3362187334_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1227466744_H
#ifndef TOUCHSCREENKEYBOARD_T3991761301_H
#define TOUCHSCREENKEYBOARD_T3991761301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard
struct  TouchScreenKeyboard_t3991761301  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TouchScreenKeyboard::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t3991761301, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_T3991761301_H
#ifndef MESSAGEEVENT_T3155234160_H
#define MESSAGEEVENT_T3155234160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct  MessageEvent_t3155234160  : public UnityEvent_1_t1359409152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENT_T3155234160_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T2115689981_H
#define TOUCHSCREENKEYBOARDTYPE_T2115689981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t2115689981 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t2115689981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T2115689981_H
#ifndef LOADSCENEMODE_T2763863609_H
#define LOADSCENEMODE_T2763863609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t2763863609 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t2763863609, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T2763863609_H
#ifndef RUNTIMEPLATFORM_T3135153872_H
#define RUNTIMEPLATFORM_T3135153872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t3135153872 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t3135153872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T3135153872_H
#ifndef CONNECTIONCHANGEEVENT_T1857494072_H
#define CONNECTIONCHANGEEVENT_T1857494072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct  ConnectionChangeEvent_t1857494072  : public UnityEvent_1_t4066535224
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCHANGEEVENT_T1857494072_H
#ifndef PARAMETERATTRIBUTES_T448949219_H
#define PARAMETERATTRIBUTES_T448949219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t448949219 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t448949219, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T448949219_H
#ifndef EDGE_T264151043_H
#define EDGE_T264151043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Edge
struct  Edge_t264151043 
{
public:
	// System.Int32 UnityEngine.RectTransform/Edge::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Edge_t264151043, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T264151043_H
#ifndef PLAYABLEGRAPH_T2451555590_H
#define PLAYABLEGRAPH_T2451555590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t2451555590 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t2451555590, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t2451555590, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T2451555590_H
#ifndef TOUCH_T1822905180_H
#define TOUCH_T1822905180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1822905180 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t3057062568  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t3057062568  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t3057062568  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Position_1)); }
	inline Vector2_t3057062568  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t3057062568 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t3057062568  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_RawPosition_2)); }
	inline Vector2_t3057062568  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t3057062568 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t3057062568  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_PositionDelta_3)); }
	inline Vector2_t3057062568  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t3057062568 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t3057062568  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1822905180_H
#ifndef PLAYABLE_T348555058_H
#define PLAYABLE_T348555058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t348555058 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t3963382032  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t348555058, ___m_Handle_0)); }
	inline PlayableHandle_t3963382032  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3963382032 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3963382032  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t348555058_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t348555058  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t348555058_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t348555058  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t348555058 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t348555058  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T348555058_H
#ifndef PLAYABLEBINDING_T181640494_H
#define PLAYABLEBINDING_T181640494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t181640494 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t1970767703 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t181640494__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t1970767703 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t1970767703 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t1970767703 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t181640494_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t3010105947* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t3010105947* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t3010105947** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t3010105947* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t181640494_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t181640494_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t1970767703_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t181640494__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t181640494_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t1970767703_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t181640494__padding[1];
	};
};
#endif // PLAYABLEBINDING_T181640494_H
#ifndef MATERIAL_T2712136762_H
#define MATERIAL_T2712136762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2712136762  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2712136762_H
#ifndef GAMEOBJECT_T1318052361_H
#define GAMEOBJECT_T1318052361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1318052361  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1318052361_H
#ifndef TEXTURE_T2838694469_H
#define TEXTURE_T2838694469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2838694469  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2838694469_H
#ifndef RENDERTARGETIDENTIFIER_T3642434912_H
#define RENDERTARGETIDENTIFIER_T3642434912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t3642434912 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t3642434912, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T3642434912_H
#ifndef RENDERSETTINGS_T2447079404_H
#define RENDERSETTINGS_T2447079404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderSettings
struct  RenderSettings_t2447079404  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERSETTINGS_T2447079404_H
#ifndef RESOURCEREQUEST_T230212484_H
#define RESOURCEREQUEST_T230212484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ResourceRequest
struct  ResourceRequest_t230212484  : public AsyncOperation_t1227466744
{
public:
	// System.String UnityEngine.ResourceRequest::m_Path
	String_t* ___m_Path_2;
	// System.Type UnityEngine.ResourceRequest::m_Type
	Type_t * ___m_Type_3;

public:
	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(ResourceRequest_t230212484, ___m_Path_2)); }
	inline String_t* get_m_Path_2() const { return ___m_Path_2; }
	inline String_t** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(String_t* value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(ResourceRequest_t230212484, ___m_Type_3)); }
	inline Type_t * get_m_Type_3() const { return ___m_Type_3; }
	inline Type_t ** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Type_t * value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t230212484_marshaled_pinvoke : public AsyncOperation_t1227466744_marshaled_pinvoke
{
	char* ___m_Path_2;
	Type_t * ___m_Type_3;
};
// Native definition for COM marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t230212484_marshaled_com : public AsyncOperation_t1227466744_marshaled_com
{
	Il2CppChar* ___m_Path_2;
	Type_t * ___m_Type_3;
};
#endif // RESOURCEREQUEST_T230212484_H
#ifndef COMPONENT_T789413749_H
#define COMPONENT_T789413749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t789413749  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T789413749_H
#ifndef MULTICASTDELEGATE_T1280656641_H
#define MULTICASTDELEGATE_T1280656641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1280656641  : public Delegate_t1563516729
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1280656641 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1280656641 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___prev_9)); }
	inline MulticastDelegate_t1280656641 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1280656641 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1280656641 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___kpm_next_10)); }
	inline MulticastDelegate_t1280656641 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1280656641 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1280656641 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1280656641_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3762232676  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3762232676  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3762232676 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3762232676  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1460120061* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3962658540 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3962658540 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3962658540 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1460120061* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1460120061** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1460120061* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3962658540 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3962658540 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3962658540 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3962658540 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3962658540 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3962658540 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef PARAMETERINFO_T2372523953_H
#define PARAMETERINFO_T2372523953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t2372523953  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t1960139725 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___marshalAs_6)); }
	inline UnmanagedMarshal_t1960139725 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t1960139725 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t1960139725 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T2372523953_H
#ifndef APPDOMAIN_T3006601831_H
#define APPDOMAIN_T3006601831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppDomain
struct  AppDomain_t3006601831  : public MarshalByRefObject_t532690895
{
public:
	// System.IntPtr System.AppDomain::_mono_app_domain
	intptr_t ____mono_app_domain_1;
	// System.Security.Policy.Evidence System.AppDomain::_evidence
	Evidence_t3393713150 * ____evidence_6;
	// System.Security.PermissionSet System.AppDomain::_granted
	PermissionSet_t2345569226 * ____granted_7;
	// System.Security.Principal.PrincipalPolicy System.AppDomain::_principalPolicy
	int32_t ____principalPolicy_8;
	// System.AppDomainManager System.AppDomain::_domain_manager
	AppDomainManager_t4290337414 * ____domain_manager_11;
	// System.ActivationContext System.AppDomain::_activation
	ActivationContext_t4234663815 * ____activation_12;
	// System.ApplicationIdentity System.AppDomain::_applicationIdentity
	ApplicationIdentity_t3327377620 * ____applicationIdentity_13;
	// System.AssemblyLoadEventHandler System.AppDomain::AssemblyLoad
	AssemblyLoadEventHandler_t1180749029 * ___AssemblyLoad_14;
	// System.ResolveEventHandler System.AppDomain::AssemblyResolve
	ResolveEventHandler_t3861315061 * ___AssemblyResolve_15;
	// System.EventHandler System.AppDomain::DomainUnload
	EventHandler_t1794010283 * ___DomainUnload_16;
	// System.EventHandler System.AppDomain::ProcessExit
	EventHandler_t1794010283 * ___ProcessExit_17;
	// System.ResolveEventHandler System.AppDomain::ResourceResolve
	ResolveEventHandler_t3861315061 * ___ResourceResolve_18;
	// System.ResolveEventHandler System.AppDomain::TypeResolve
	ResolveEventHandler_t3861315061 * ___TypeResolve_19;
	// System.UnhandledExceptionEventHandler System.AppDomain::UnhandledException
	UnhandledExceptionEventHandler_t2340223587 * ___UnhandledException_20;
	// System.ResolveEventHandler System.AppDomain::ReflectionOnlyAssemblyResolve
	ResolveEventHandler_t3861315061 * ___ReflectionOnlyAssemblyResolve_21;

public:
	inline static int32_t get_offset_of__mono_app_domain_1() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____mono_app_domain_1)); }
	inline intptr_t get__mono_app_domain_1() const { return ____mono_app_domain_1; }
	inline intptr_t* get_address_of__mono_app_domain_1() { return &____mono_app_domain_1; }
	inline void set__mono_app_domain_1(intptr_t value)
	{
		____mono_app_domain_1 = value;
	}

	inline static int32_t get_offset_of__evidence_6() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____evidence_6)); }
	inline Evidence_t3393713150 * get__evidence_6() const { return ____evidence_6; }
	inline Evidence_t3393713150 ** get_address_of__evidence_6() { return &____evidence_6; }
	inline void set__evidence_6(Evidence_t3393713150 * value)
	{
		____evidence_6 = value;
		Il2CppCodeGenWriteBarrier((&____evidence_6), value);
	}

	inline static int32_t get_offset_of__granted_7() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____granted_7)); }
	inline PermissionSet_t2345569226 * get__granted_7() const { return ____granted_7; }
	inline PermissionSet_t2345569226 ** get_address_of__granted_7() { return &____granted_7; }
	inline void set__granted_7(PermissionSet_t2345569226 * value)
	{
		____granted_7 = value;
		Il2CppCodeGenWriteBarrier((&____granted_7), value);
	}

	inline static int32_t get_offset_of__principalPolicy_8() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____principalPolicy_8)); }
	inline int32_t get__principalPolicy_8() const { return ____principalPolicy_8; }
	inline int32_t* get_address_of__principalPolicy_8() { return &____principalPolicy_8; }
	inline void set__principalPolicy_8(int32_t value)
	{
		____principalPolicy_8 = value;
	}

	inline static int32_t get_offset_of__domain_manager_11() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____domain_manager_11)); }
	inline AppDomainManager_t4290337414 * get__domain_manager_11() const { return ____domain_manager_11; }
	inline AppDomainManager_t4290337414 ** get_address_of__domain_manager_11() { return &____domain_manager_11; }
	inline void set__domain_manager_11(AppDomainManager_t4290337414 * value)
	{
		____domain_manager_11 = value;
		Il2CppCodeGenWriteBarrier((&____domain_manager_11), value);
	}

	inline static int32_t get_offset_of__activation_12() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____activation_12)); }
	inline ActivationContext_t4234663815 * get__activation_12() const { return ____activation_12; }
	inline ActivationContext_t4234663815 ** get_address_of__activation_12() { return &____activation_12; }
	inline void set__activation_12(ActivationContext_t4234663815 * value)
	{
		____activation_12 = value;
		Il2CppCodeGenWriteBarrier((&____activation_12), value);
	}

	inline static int32_t get_offset_of__applicationIdentity_13() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ____applicationIdentity_13)); }
	inline ApplicationIdentity_t3327377620 * get__applicationIdentity_13() const { return ____applicationIdentity_13; }
	inline ApplicationIdentity_t3327377620 ** get_address_of__applicationIdentity_13() { return &____applicationIdentity_13; }
	inline void set__applicationIdentity_13(ApplicationIdentity_t3327377620 * value)
	{
		____applicationIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&____applicationIdentity_13), value);
	}

	inline static int32_t get_offset_of_AssemblyLoad_14() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___AssemblyLoad_14)); }
	inline AssemblyLoadEventHandler_t1180749029 * get_AssemblyLoad_14() const { return ___AssemblyLoad_14; }
	inline AssemblyLoadEventHandler_t1180749029 ** get_address_of_AssemblyLoad_14() { return &___AssemblyLoad_14; }
	inline void set_AssemblyLoad_14(AssemblyLoadEventHandler_t1180749029 * value)
	{
		___AssemblyLoad_14 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyLoad_14), value);
	}

	inline static int32_t get_offset_of_AssemblyResolve_15() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___AssemblyResolve_15)); }
	inline ResolveEventHandler_t3861315061 * get_AssemblyResolve_15() const { return ___AssemblyResolve_15; }
	inline ResolveEventHandler_t3861315061 ** get_address_of_AssemblyResolve_15() { return &___AssemblyResolve_15; }
	inline void set_AssemblyResolve_15(ResolveEventHandler_t3861315061 * value)
	{
		___AssemblyResolve_15 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyResolve_15), value);
	}

	inline static int32_t get_offset_of_DomainUnload_16() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___DomainUnload_16)); }
	inline EventHandler_t1794010283 * get_DomainUnload_16() const { return ___DomainUnload_16; }
	inline EventHandler_t1794010283 ** get_address_of_DomainUnload_16() { return &___DomainUnload_16; }
	inline void set_DomainUnload_16(EventHandler_t1794010283 * value)
	{
		___DomainUnload_16 = value;
		Il2CppCodeGenWriteBarrier((&___DomainUnload_16), value);
	}

	inline static int32_t get_offset_of_ProcessExit_17() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___ProcessExit_17)); }
	inline EventHandler_t1794010283 * get_ProcessExit_17() const { return ___ProcessExit_17; }
	inline EventHandler_t1794010283 ** get_address_of_ProcessExit_17() { return &___ProcessExit_17; }
	inline void set_ProcessExit_17(EventHandler_t1794010283 * value)
	{
		___ProcessExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___ProcessExit_17), value);
	}

	inline static int32_t get_offset_of_ResourceResolve_18() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___ResourceResolve_18)); }
	inline ResolveEventHandler_t3861315061 * get_ResourceResolve_18() const { return ___ResourceResolve_18; }
	inline ResolveEventHandler_t3861315061 ** get_address_of_ResourceResolve_18() { return &___ResourceResolve_18; }
	inline void set_ResourceResolve_18(ResolveEventHandler_t3861315061 * value)
	{
		___ResourceResolve_18 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceResolve_18), value);
	}

	inline static int32_t get_offset_of_TypeResolve_19() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___TypeResolve_19)); }
	inline ResolveEventHandler_t3861315061 * get_TypeResolve_19() const { return ___TypeResolve_19; }
	inline ResolveEventHandler_t3861315061 ** get_address_of_TypeResolve_19() { return &___TypeResolve_19; }
	inline void set_TypeResolve_19(ResolveEventHandler_t3861315061 * value)
	{
		___TypeResolve_19 = value;
		Il2CppCodeGenWriteBarrier((&___TypeResolve_19), value);
	}

	inline static int32_t get_offset_of_UnhandledException_20() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___UnhandledException_20)); }
	inline UnhandledExceptionEventHandler_t2340223587 * get_UnhandledException_20() const { return ___UnhandledException_20; }
	inline UnhandledExceptionEventHandler_t2340223587 ** get_address_of_UnhandledException_20() { return &___UnhandledException_20; }
	inline void set_UnhandledException_20(UnhandledExceptionEventHandler_t2340223587 * value)
	{
		___UnhandledException_20 = value;
		Il2CppCodeGenWriteBarrier((&___UnhandledException_20), value);
	}

	inline static int32_t get_offset_of_ReflectionOnlyAssemblyResolve_21() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831, ___ReflectionOnlyAssemblyResolve_21)); }
	inline ResolveEventHandler_t3861315061 * get_ReflectionOnlyAssemblyResolve_21() const { return ___ReflectionOnlyAssemblyResolve_21; }
	inline ResolveEventHandler_t3861315061 ** get_address_of_ReflectionOnlyAssemblyResolve_21() { return &___ReflectionOnlyAssemblyResolve_21; }
	inline void set_ReflectionOnlyAssemblyResolve_21(ResolveEventHandler_t3861315061 * value)
	{
		___ReflectionOnlyAssemblyResolve_21 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionOnlyAssemblyResolve_21), value);
	}
};

struct AppDomain_t3006601831_StaticFields
{
public:
	// System.String System.AppDomain::_process_guid
	String_t* ____process_guid_2;
	// System.AppDomain System.AppDomain::default_domain
	AppDomain_t3006601831 * ___default_domain_10;

public:
	inline static int32_t get_offset_of__process_guid_2() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_StaticFields, ____process_guid_2)); }
	inline String_t* get__process_guid_2() const { return ____process_guid_2; }
	inline String_t** get_address_of__process_guid_2() { return &____process_guid_2; }
	inline void set__process_guid_2(String_t* value)
	{
		____process_guid_2 = value;
		Il2CppCodeGenWriteBarrier((&____process_guid_2), value);
	}

	inline static int32_t get_offset_of_default_domain_10() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_StaticFields, ___default_domain_10)); }
	inline AppDomain_t3006601831 * get_default_domain_10() const { return ___default_domain_10; }
	inline AppDomain_t3006601831 ** get_address_of_default_domain_10() { return &___default_domain_10; }
	inline void set_default_domain_10(AppDomain_t3006601831 * value)
	{
		___default_domain_10 = value;
		Il2CppCodeGenWriteBarrier((&___default_domain_10), value);
	}
};

struct AppDomain_t3006601831_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.AppDomain::type_resolve_in_progress
	Hashtable_t3677817270 * ___type_resolve_in_progress_3;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress
	Hashtable_t3677817270 * ___assembly_resolve_in_progress_4;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress_refonly
	Hashtable_t3677817270 * ___assembly_resolve_in_progress_refonly_5;
	// System.Security.Principal.IPrincipal System.AppDomain::_principal
	RuntimeObject* ____principal_9;

public:
	inline static int32_t get_offset_of_type_resolve_in_progress_3() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_ThreadStaticFields, ___type_resolve_in_progress_3)); }
	inline Hashtable_t3677817270 * get_type_resolve_in_progress_3() const { return ___type_resolve_in_progress_3; }
	inline Hashtable_t3677817270 ** get_address_of_type_resolve_in_progress_3() { return &___type_resolve_in_progress_3; }
	inline void set_type_resolve_in_progress_3(Hashtable_t3677817270 * value)
	{
		___type_resolve_in_progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_resolve_in_progress_3), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_4() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_ThreadStaticFields, ___assembly_resolve_in_progress_4)); }
	inline Hashtable_t3677817270 * get_assembly_resolve_in_progress_4() const { return ___assembly_resolve_in_progress_4; }
	inline Hashtable_t3677817270 ** get_address_of_assembly_resolve_in_progress_4() { return &___assembly_resolve_in_progress_4; }
	inline void set_assembly_resolve_in_progress_4(Hashtable_t3677817270 * value)
	{
		___assembly_resolve_in_progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_4), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_refonly_5() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_ThreadStaticFields, ___assembly_resolve_in_progress_refonly_5)); }
	inline Hashtable_t3677817270 * get_assembly_resolve_in_progress_refonly_5() const { return ___assembly_resolve_in_progress_refonly_5; }
	inline Hashtable_t3677817270 ** get_address_of_assembly_resolve_in_progress_refonly_5() { return &___assembly_resolve_in_progress_refonly_5; }
	inline void set_assembly_resolve_in_progress_refonly_5(Hashtable_t3677817270 * value)
	{
		___assembly_resolve_in_progress_refonly_5 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_refonly_5), value);
	}

	inline static int32_t get_offset_of__principal_9() { return static_cast<int32_t>(offsetof(AppDomain_t3006601831_ThreadStaticFields, ____principal_9)); }
	inline RuntimeObject* get__principal_9() const { return ____principal_9; }
	inline RuntimeObject** get_address_of__principal_9() { return &____principal_9; }
	inline void set__principal_9(RuntimeObject* value)
	{
		____principal_9 = value;
		Il2CppCodeGenWriteBarrier((&____principal_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPDOMAIN_T3006601831_H
#ifndef SPRITE_T3012664695_H
#define SPRITE_T3012664695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t3012664695  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T3012664695_H
#ifndef SCRIPTPLAYABLEOUTPUT_T3953448488_H
#define SCRIPTPLAYABLEOUTPUT_T3953448488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.ScriptPlayableOutput
struct  ScriptPlayableOutput_t3953448488 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::m_Handle
	PlayableOutputHandle_t1112988996  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(ScriptPlayableOutput_t3953448488, ___m_Handle_0)); }
	inline PlayableOutputHandle_t1112988996  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t1112988996 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t1112988996  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPLAYABLEOUTPUT_T3953448488_H
#ifndef PLAYABLEOUTPUT_T1141860262_H
#define PLAYABLEOUTPUT_T1141860262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t1141860262 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t1112988996  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t1141860262, ___m_Handle_0)); }
	inline PlayableOutputHandle_t1112988996  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t1112988996 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t1112988996  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t1141860262_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t1141860262  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t1141860262_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t1141860262  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t1141860262 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t1141860262  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T1141860262_H
#ifndef SCRIPTABLEOBJECT_T2962125979_H
#define SCRIPTABLEOBJECT_T2962125979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2962125979  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2962125979_marshaled_pinvoke : public Object_t1970767703_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2962125979_marshaled_com : public Object_t1970767703_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2962125979_H
#ifndef SHADER_T3339661152_H
#define SHADER_T3339661152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t3339661152  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T3339661152_H
#ifndef SPRITEATLAS_T3138271588_H
#define SPRITEATLAS_T3138271588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlas
struct  SpriteAtlas_t3138271588  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLAS_T3138271588_H
#ifndef REQUESTATLASCALLBACK_T634135197_H
#define REQUESTATLASCALLBACK_T634135197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct  RequestAtlasCallback_t634135197  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTATLASCALLBACK_T634135197_H
#ifndef BEHAVIOUR_T2441856611_H
#define BEHAVIOUR_T2441856611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2441856611  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2441856611_H
#ifndef UNHANDLEDEXCEPTIONEVENTHANDLER_T2340223587_H
#define UNHANDLEDEXCEPTIONEVENTHANDLER_T2340223587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t2340223587  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTHANDLER_T2340223587_H
#ifndef ACTION_1_T1977832595_H
#define ACTION_1_T1977832595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct  Action_1_t1977832595  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1977832595_H
#ifndef TEXTURE2D_T878840578_H
#define TEXTURE2D_T878840578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t878840578  : public Texture_t2838694469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T878840578_H
#ifndef PLAYABLEASSET_T2447076843_H
#define PLAYABLEASSET_T2447076843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t2447076843  : public ScriptableObject_t2962125979
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T2447076843_H
#ifndef ASYNCCALLBACK_T869574496_H
#define ASYNCCALLBACK_T869574496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t869574496  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T869574496_H
#ifndef RENDERTEXTURE_T3361574884_H
#define RENDERTEXTURE_T3361574884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t3361574884  : public Texture_t2838694469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T3361574884_H
#ifndef ACTION_1_T67027751_H
#define ACTION_1_T67027751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t67027751  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T67027751_H
#ifndef TRANSFORM_T532597831_H
#define TRANSFORM_T532597831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t532597831  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T532597831_H
#ifndef UNITYACTION_2_T2777655240_H
#define UNITYACTION_2_T2777655240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_t2777655240  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T2777655240_H
#ifndef UNITYACTION_1_T2691974747_H
#define UNITYACTION_1_T2691974747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct  UnityAction_1_t2691974747  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2691974747_H
#ifndef UNITYACTION_2_T562236193_H
#define UNITYACTION_2_T562236193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityAction_2_t562236193  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T562236193_H
#ifndef PLAYERCONNECTION_T781534341_H
#define PLAYERCONNECTION_T781534341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct  PlayerConnection_t781534341  : public ScriptableObject_t2962125979
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents UnityEngine.Networking.PlayerConnection.PlayerConnection::m_PlayerEditorConnectionEvents
	PlayerEditorConnectionEvents_t3406496741 * ___m_PlayerEditorConnectionEvents_3;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Networking.PlayerConnection.PlayerConnection::m_connectedPlayers
	List_1_t309455265 * ___m_connectedPlayers_4;
	// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::m_IsInitilized
	bool ___m_IsInitilized_5;

public:
	inline static int32_t get_offset_of_m_PlayerEditorConnectionEvents_3() { return static_cast<int32_t>(offsetof(PlayerConnection_t781534341, ___m_PlayerEditorConnectionEvents_3)); }
	inline PlayerEditorConnectionEvents_t3406496741 * get_m_PlayerEditorConnectionEvents_3() const { return ___m_PlayerEditorConnectionEvents_3; }
	inline PlayerEditorConnectionEvents_t3406496741 ** get_address_of_m_PlayerEditorConnectionEvents_3() { return &___m_PlayerEditorConnectionEvents_3; }
	inline void set_m_PlayerEditorConnectionEvents_3(PlayerEditorConnectionEvents_t3406496741 * value)
	{
		___m_PlayerEditorConnectionEvents_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerEditorConnectionEvents_3), value);
	}

	inline static int32_t get_offset_of_m_connectedPlayers_4() { return static_cast<int32_t>(offsetof(PlayerConnection_t781534341, ___m_connectedPlayers_4)); }
	inline List_1_t309455265 * get_m_connectedPlayers_4() const { return ___m_connectedPlayers_4; }
	inline List_1_t309455265 ** get_address_of_m_connectedPlayers_4() { return &___m_connectedPlayers_4; }
	inline void set_m_connectedPlayers_4(List_1_t309455265 * value)
	{
		___m_connectedPlayers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectedPlayers_4), value);
	}

	inline static int32_t get_offset_of_m_IsInitilized_5() { return static_cast<int32_t>(offsetof(PlayerConnection_t781534341, ___m_IsInitilized_5)); }
	inline bool get_m_IsInitilized_5() const { return ___m_IsInitilized_5; }
	inline bool* get_address_of_m_IsInitilized_5() { return &___m_IsInitilized_5; }
	inline void set_m_IsInitilized_5(bool value)
	{
		___m_IsInitilized_5 = value;
	}
};

struct PlayerConnection_t781534341_StaticFields
{
public:
	// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::connectionNative
	RuntimeObject* ___connectionNative_2;
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::s_Instance
	PlayerConnection_t781534341 * ___s_Instance_6;

public:
	inline static int32_t get_offset_of_connectionNative_2() { return static_cast<int32_t>(offsetof(PlayerConnection_t781534341_StaticFields, ___connectionNative_2)); }
	inline RuntimeObject* get_connectionNative_2() const { return ___connectionNative_2; }
	inline RuntimeObject** get_address_of_connectionNative_2() { return &___connectionNative_2; }
	inline void set_connectionNative_2(RuntimeObject* value)
	{
		___connectionNative_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectionNative_2), value);
	}

	inline static int32_t get_offset_of_s_Instance_6() { return static_cast<int32_t>(offsetof(PlayerConnection_t781534341_StaticFields, ___s_Instance_6)); }
	inline PlayerConnection_t781534341 * get_s_Instance_6() const { return ___s_Instance_6; }
	inline PlayerConnection_t781534341 ** get_address_of_s_Instance_6() { return &___s_Instance_6; }
	inline void set_s_Instance_6(PlayerConnection_t781534341 * value)
	{
		___s_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTION_T781534341_H
#ifndef REAPPLYDRIVENPROPERTIES_T3527255582_H
#define REAPPLYDRIVENPROPERTIES_T3527255582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t3527255582  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REAPPLYDRIVENPROPERTIES_T3527255582_H
#ifndef UNITYACTION_1_T2642535036_H
#define UNITYACTION_1_T2642535036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Int32>
struct  UnityAction_1_t2642535036  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2642535036_H
#ifndef RENDERER_T265431926_H
#define RENDERER_T265431926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t265431926  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T265431926_H
#ifndef UNITYACTION_1_T4230376260_H
#define UNITYACTION_1_T4230376260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityAction_1_t4230376260  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T4230376260_H
#ifndef FUNC_2_T4252092203_H
#define FUNC_2_T4252092203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct  Func_2_t4252092203  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T4252092203_H
#ifndef CAMERA_T989002943_H
#define CAMERA_T989002943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t989002943  : public Behaviour_t2441856611
{
public:

public:
};

struct Camera_t989002943_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t2620733104 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t2620733104 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t2620733104 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t2620733104 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t2620733104 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t2620733104 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t2620733104 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t2620733104 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t2620733104 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t2620733104 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T989002943_H
#ifndef GUILAYER_T2694382279_H
#define GUILAYER_T2694382279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t2694382279  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T2694382279_H
#ifndef GUIELEMENT_T1520723180_H
#define GUIELEMENT_T1520723180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t1520723180  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T1520723180_H
#ifndef SPRITERENDERER_T3043448750_H
#define SPRITERENDERER_T3043448750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3043448750  : public Renderer_t265431926
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3043448750_H
#ifndef RECTTRANSFORM_T15861704_H
#define RECTTRANSFORM_T15861704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t15861704  : public Transform_t532597831
{
public:

public:
};

struct RectTransform_t15861704_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3527255582 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t15861704_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t3527255582 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t3527255582 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t3527255582 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T15861704_H
// System.Byte[]
struct ByteU5BU5D_t3287329517  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t3487290734  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_t1970767703 * m_Items[1];

public:
	inline Object_t1970767703 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t1970767703 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t1970767703 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t1970767703 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t1970767703 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t1970767703 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t1568665923  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t3010105947  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PlayableBinding_t181640494  m_Items[1];

public:
	inline PlayableBinding_t181640494  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayableBinding_t181640494 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayableBinding_t181640494  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlayableBinding_t181640494  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayableBinding_t181640494 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayableBinding_t181640494  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t329709361  m_Items[1];

public:
	inline Vector3_t329709361  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t329709361  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t3897890150  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t989002943 * m_Items[1];

public:
	inline Camera_t989002943 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t989002943 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t989002943 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t989002943 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t989002943 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t989002943 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1216529257  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HitInfo_t214434488  m_Items[1];

public:
	inline HitInfo_t214434488  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HitInfo_t214434488 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HitInfo_t214434488  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HitInfo_t214434488  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HitInfo_t214434488 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HitInfo_t214434488  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3541603165  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t1946944212  m_Items[1];

public:
	inline ParameterModifier_t1946944212  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t1946944212 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t1946944212  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t1946944212  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t1946944212 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t1946944212  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t369357837  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t83643201  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t4074997356  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t2372523953 * m_Items[1];

public:
	inline ParameterInfo_t2372523953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t2372523953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t2372523953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t2372523953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t2372523953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t2372523953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t1505762612  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t2788147849  m_Items[1];

public:
	inline Color32_t2788147849  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t2788147849 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t2788147849  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t2788147849  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t2788147849 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t2788147849  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void Object_t1970767703_marshal_pinvoke(const Object_t1970767703& unmarshaled, Object_t1970767703_marshaled_pinvoke& marshaled);
extern "C" void Object_t1970767703_marshal_pinvoke_back(const Object_t1970767703_marshaled_pinvoke& marshaled, Object_t1970767703& unmarshaled);
extern "C" void Object_t1970767703_marshal_pinvoke_cleanup(Object_t1970767703_marshaled_pinvoke& marshaled);
extern "C" void Object_t1970767703_marshal_com(const Object_t1970767703& unmarshaled, Object_t1970767703_marshaled_com& marshaled);
extern "C" void Object_t1970767703_marshal_com_back(const Object_t1970767703_marshaled_com& marshaled, Object_t1970767703& unmarshaled);
extern "C" void Object_t1970767703_marshal_com_cleanup(Object_t1970767703_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m3981019775_gshared (List_1_t309455265 * __this, const RuntimeMethod* method);
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m2865889793_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisRuntimeObject_m2545763864_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2657312599_gshared (UnityEvent_1_t3942628331 * __this, UnityAction_1_t2518628143 * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1909216310  List_1_GetEnumerator_m2158044649_gshared (List_1_t309455265 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m991032171_gshared (Enumerator_t1909216310 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3292288955_gshared (UnityAction_1_t2642535036 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3878557316_gshared (Enumerator_t1909216310 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m4143828380_gshared (Enumerator_t1909216310 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1699996981_gshared (UnityEvent_1_t4066535224 * __this, UnityAction_1_t2642535036 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m2519407053_gshared (List_1_t309455265 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m2806368434_gshared (UnityEvent_1_t4066535224 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2233273281_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2181542045_gshared (Func_2_t347937039 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject* Enumerable_Where_TisRuntimeObject_m4270751971_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t347937039 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4158201835_gshared (UnityEvent_1_t3942628331 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::SingleOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject * Enumerable_SingleOrDefault_TisRuntimeObject_m3416927023_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t347937039 * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4064363414_gshared (List_1_t185548372 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m15913427_gshared (UnityEvent_1_t4066535224 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m4222880068_gshared (UnityEvent_1_t3942628331 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3981208854_gshared (UnityAction_2_t2777655240 * __this, Scene_t548444562  p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2617017530_gshared (UnityAction_1_t2691974747 * __this, Scene_t548444562  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1895819670_gshared (UnityAction_2_t562236193 * __this, Scene_t548444562  p0, Scene_t548444562  p1, const RuntimeMethod* method);
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2674875975_gshared (Component_t789413749 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3238157784_gshared (Action_1_t3509626261 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m4233958761 (Attribute_t3852256153 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2590354428 (NativeClassAttribute_t4188729888 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2095069727 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m1071995445 (PlayerEditorConnectionEvents_t3406496741 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m3981019775(__this, method) ((  void (*) (List_1_t309455265 *, const RuntimeMethod*))List_1__ctor_m3981019775_gshared)(__this, method)
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1532608883 (ScriptableObject_t2962125979 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1910042615 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___x0, Object_t1970767703 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t781534341 * PlayerConnection_CreateInstance_m3407819880 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern "C"  RuntimeObject* PlayerConnection_GetConnectionNativeApi_m3971624049 (PlayerConnection_t781534341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.ScriptableObject::CreateInstance<UnityEngine.Networking.PlayerConnection.PlayerConnection>()
#define ScriptableObject_CreateInstance_TisPlayerConnection_t781534341_m1156802871(__this /* static, unused */, method) ((  PlayerConnection_t781534341 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m2865889793_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2630637173 (Object_t1970767703 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern "C"  void PlayerConnectionInternal__ctor_m2836034707 (PlayerConnectionInternal_t984574678 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m1475836212 (RuntimeObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m3645081522 (ArgumentException_t3637419113 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230(__this /* static, unused */, p0, method) ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m2545763864_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern "C"  UnityEvent_1_t1359409152 * PlayerEditorConnectionEvents_AddAndCreate_m3897452280 (PlayerEditorConnectionEvents_t3406496741 * __this, Guid_t  ___messageId0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m508826909(__this, p0, method) ((  void (*) (UnityEvent_1_t1359409152 *, UnityAction_1_t4230376260 *, const RuntimeMethod*))UnityEvent_1_AddListener_m2657312599_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
#define List_1_GetEnumerator_m2158044649(__this, method) ((  Enumerator_t1909216310  (*) (List_1_t309455265 *, const RuntimeMethod*))List_1_GetEnumerator_m2158044649_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m991032171(__this, method) ((  int32_t (*) (Enumerator_t1909216310 *, const RuntimeMethod*))Enumerator_get_Current_m991032171_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
#define UnityAction_1_Invoke_m3292288955(__this, p0, method) ((  void (*) (UnityAction_1_t2642535036 *, int32_t, const RuntimeMethod*))UnityAction_1_Invoke_m3292288955_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3878557316(__this, method) ((  bool (*) (Enumerator_t1909216310 *, const RuntimeMethod*))Enumerator_MoveNext_m3878557316_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m4143828380(__this, method) ((  void (*) (Enumerator_t1909216310 *, const RuntimeMethod*))Enumerator_Dispose_m4143828380_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m1699996981(__this, p0, method) ((  void (*) (UnityEvent_1_t4066535224 *, UnityAction_1_t2642535036 *, const RuntimeMethod*))UnityEvent_1_AddListener_m1699996981_gshared)(__this, p0, method)
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m148013006 (RuntimeObject * __this /* static, unused */, intptr_t p0, ByteU5BU5D_t3287329517* p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t781534341 * PlayerConnection_get_instance_m1185993489 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m2921421088 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4252710989 (PlayerEditorConnectionEvents_t3406496741 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3287329517* ___data1, int32_t ___playerId2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m2519407053(__this, p0, method) ((  void (*) (List_1_t309455265 *, int32_t, const RuntimeMethod*))List_1_Add_m2519407053_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
#define UnityEvent_1_Invoke_m2806368434(__this, p0, method) ((  void (*) (UnityEvent_1_t4066535224 *, int32_t, const RuntimeMethod*))UnityEvent_1_Invoke_m2806368434_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::.ctor()
#define List_1__ctor_m2158277014(__this, method) ((  void (*) (List_1_t3095965032 *, const RuntimeMethod*))List_1__ctor_m2233273281_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m537474357 (ConnectionChangeEvent_t1857494072 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m937319274 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2720484915(__this, p0, p1, method) ((  void (*) (Func_2_t4252092203 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m2181542045_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMessageTypeSubscribers_t3285514618_m2269018702(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t4252092203 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m4270751971_gshared)(__this /* static, unused */, p0, p1, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m1420639942 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m627890074 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m2179581038 (MessageEventArgs_t2086846075 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::Invoke(T0)
#define UnityEvent_1_Invoke_m1430365874(__this, p0, method) ((  void (*) (UnityEvent_1_t1359409152 *, MessageEventArgs_t2086846075 *, const RuntimeMethod*))UnityEvent_1_Invoke_m4158201835_gshared)(__this, p0, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::.ctor()
extern "C"  void U3CAddAndCreateU3Ec__AnonStorey1__ctor_m782456925 (U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 System.Linq.Enumerable::SingleOrDefault<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t3285514618_m1777054811(__this /* static, unused */, p0, p1, method) ((  MessageTypeSubscribers_t3285514618 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t4252092203 *, const RuntimeMethod*))Enumerable_SingleOrDefault_TisRuntimeObject_m3416927023_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m2582810082 (MessageTypeSubscribers_t3285514618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern "C"  void MessageTypeSubscribers_set_MessageTypeId_m3146332482 (MessageTypeSubscribers_t3285514618 * __this, Guid_t  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m4280913397 (MessageEvent_t3155234160 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::Add(!0)
#define List_1_Add_m1776051518(__this, p0, method) ((  void (*) (List_1_t3095965032 *, MessageTypeSubscribers_t3285514618 *, const RuntimeMethod*))List_1_Add_m4064363414_gshared)(__this, p0, method)
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m416571748 (MessageTypeSubscribers_t3285514618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
#define UnityEvent_1__ctor_m15913427(__this, method) ((  void (*) (UnityEvent_1_t4066535224 *, const RuntimeMethod*))UnityEvent_1__ctor_m15913427_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::.ctor()
#define UnityEvent_1__ctor_m2017699965(__this, method) ((  void (*) (UnityEvent_1_t1359409152 *, const RuntimeMethod*))UnityEvent_1__ctor_m4222880068_gshared)(__this, method)
// System.String System.Guid::ToString()
extern "C"  String_t* Guid_ToString_m659365756 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1970767703 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___data0, Vector3_t329709361 * ___pos1, Quaternion_t2761156409 * ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m1310479296 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, float ___t1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3659857569 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m2809416424 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m4209539787 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___lhs0, Object_t1970767703 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m2316845610 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m1116264796 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m434724044 (Object_t1970767703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m4173155962 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m426474767 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1422478095 (ArgumentException_t3637419113 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1970767703 * Object_Internal_InstantiateSingle_m3669590211 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___data0, Vector3_t329709361  ___pos1, Quaternion_t2761156409  ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_Normalize_m2417808733 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m3905706415 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___lhs0, Vector3_t329709361  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2699116517 (Plane_t1350213727 * __this, Vector3_t329709361  ___inNormal0, Vector3_t329709361  ___inPoint1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t329709361  Ray_get_direction_m777501070 (Ray_t1448970001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t329709361  Ray_get_origin_m3562529566 (Ray_t1448970001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1485428512 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m995144852 (Plane_t1350213727 * __this, Ray_t1448970001  ___ray0, float* ___enter1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m1584616489 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1568665923* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m2969686206 (Plane_t1350213727 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m2275210789 (Playable_t348555058 * __this, PlayableHandle_t3963382032  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t3963382032  Playable_GetHandle_m1535596094 (Playable_t348555058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m447576049 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032  ___x0, PlayableHandle_t3963382032  ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m1514203323 (Playable_t348555058 * __this, Playable_t348555058  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t3963382032  PlayableHandle_get_Null_m428254764 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t348555058  Playable_get_Null_m799873240 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m2850170309 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Object::MemberwiseClone()
extern "C"  RuntimeObject * Object_MemberwiseClone_m38416816 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m3883546329 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2989374346 (PlayableHandle_t3963382032 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m1964937316 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032  ___lhs0, PlayableHandle_t3963382032  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m2260768344 (PlayableHandle_t3963382032 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::GetHashCode()
extern "C"  int32_t IntPtr_GetHashCode_m2693920867 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m3782351173 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m1373097937 (PlayableHandle_t3963382032 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3008674881 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2581237927 (PlayableOutput_t1141860262 * __this, PlayableOutputHandle_t1112988996  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t1112988996  PlayableOutput_GetHandle_m3642617554 (PlayableOutput_t1141860262 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m3380206387 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1112988996  ___lhs0, PlayableOutputHandle_t1112988996  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m1397146541 (PlayableOutput_t1141860262 * __this, PlayableOutput_t1141860262  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t1112988996  PlayableOutputHandle_get_Null_m373902066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m3692544427 (PlayableOutputHandle_t1112988996 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3807837470 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1112988996  ___lhs0, PlayableOutputHandle_t1112988996  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m4139373885 (PlayableOutputHandle_t1112988996 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Guid::ToString(System.String)
extern "C"  String_t* Guid_ToString_m1281603392 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_SendMessage_m2860749805 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, ByteU5BU5D_t3287329517* ___data1, int32_t ___playerId2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern "C"  void PlayerConnectionInternal_RegisterInternal_m1526953951 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern "C"  bool PlayerConnectionInternal_IsConnected_m3995228764 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern "C"  void PlayerConnectionInternal_DisconnectAll_m2331867028 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t3644065956  PropertyNameUtils_PropertyNameFromString_m353937208 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m780200493 (PropertyName_t3644065956 * __this, PropertyName_t3644065956  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m3929666074 (PropertyName_t3644065956 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m247674800 (PropertyName_t3644065956 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m2665292733 (PropertyName_t3644065956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m3142464868 (RuntimeObject * __this /* static, unused */, PropertyName_t3644065956  ___lhs0, PropertyName_t3644065956  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m1268943811 (PropertyName_t3644065956 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2126999818 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1247093862 (PropertyName_t3644065956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m1655027246 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t3644065956 * ___ret1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3784845077 (Quaternion_t2761156409 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3131108156 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___forward0, Vector3_t329709361 * ___upwards1, Quaternion_t2761156409 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m3061492299 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409 * ___rotation0, Quaternion_t2761156409 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3984700005 (Vector3_t329709361 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t329709361  Vector3_op_Multiply_m3838130503 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t2761156409  Quaternion_Internal_FromEulerRad_m2232655408 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___euler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___euler0, Quaternion_t2761156409 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m342780875 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___a0, Quaternion_t2761156409  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m3822093777 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___lhs0, Quaternion_t2761156409  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3104521605 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m1455437325 (Quaternion_t2761156409 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m54068117 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m1356692124 (Quaternion_t2761156409 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m1462711549 (Quaternion_t2761156409 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3365316696 (PropertyAttribute_t69305138 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2731283770 (RangeInt_t3767345128 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t329709361  Vector3_get_normalized_m2341245559 (Vector3_t329709361 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3929653519 (Ray_t1448970001 * __this, Vector3_t329709361  ___origin0, Vector3_t329709361  ___direction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_op_Addition_m2206773101 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___a0, Vector3_t329709361  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t329709361  Ray_GetPoint_m635666389 (Ray_t1448970001 * __this, float ___distance0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m1475619137 (Ray_t1448970001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m4143151633 (Rect_t1344834245 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m2491470360 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m2837260125 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3128128304 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m840657376 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m1371796108 (Vector2_t3057062568 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t3057062568  Rect_get_position_m205947143 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t3057062568  Rect_get_center_m3649125928 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m2002404286 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m3125052285 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t3057062568  Rect_get_min_m1464947635 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2060108737 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m3717006025 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t3057062568  Rect_get_max_m2236455262 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1015428014 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m642665222 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m4024308933 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3801339137 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t3057062568  Rect_get_size_m1943186915 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2530956691 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m2705338653 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1652106490 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m738832795 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3306500271 (Rect_t1344834245 * __this, Vector2_t3057062568  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3476213010 (Rect_t1344834245 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m1139021315 (Rect_t1344834245 * __this, Rect_t1344834245  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t1344834245  Rect_OrderMinMax_m3781845787 (RuntimeObject * __this /* static, unused */, Rect_t1344834245  ___rect0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4001495534 (Rect_t1344834245 * __this, Rect_t1344834245  ___other0, bool ___allowInverse1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2563054195 (RuntimeObject * __this /* static, unused */, Rect_t1344834245  ___lhs0, Rect_t1344834245  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m2627567217 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m1339189050 (Rect_t1344834245 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m4042206769 (Rect_t1344834245 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4183761430 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m456239651 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2450496781 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m2136847356 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m241919892 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3343256683 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m396615172 (RectOffset_t83369214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1193447114 (RectTransform_t15861704 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m2850584098 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m854179797 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m429367384 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m728187047 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3509514529 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m245688481 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m1138808073 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m2182524852 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m2489252632 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m3372425696 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1563516729 * Delegate_Combine_m645997588 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * p0, Delegate_t1563516729 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1563516729 * Delegate_Remove_m2405839985 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * p0, Delegate_t1563516729 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3181565260 (ReapplyDrivenProperties_t3527255582 * __this, RectTransform_t15861704 * ___driven0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t1344834245  RectTransform_get_rect_m4288011661 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m4198635943 (RectTransform_t15861704 * __this, Vector3U5BU5D_t974944492* ___fourCornersArray0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t532597831 * Component_get_transform_m2645782843 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Transform_TransformPoint_m642078408 (Transform_t532597831 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t3057062568  RectTransform_get_anchorMin_m3173730716 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1285091505 (Vector2_t3057062568 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m2066112249 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t3057062568  RectTransform_get_anchorMax_m1145375430 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m3662733738 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t3057062568  RectTransform_get_sizeDelta_m3670515036 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2990296297 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t3057062568  RectTransform_get_anchoredPosition_m1623341961 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t3057062568  RectTransform_get_pivot_m2251648063 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m2598596005 (Vector2_t3057062568 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2221025746 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t3057062568  RectTransform_GetParentSize_m1866663977 (RectTransform_t15861704 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t532597831 * Transform_get_parent_m1093437403 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1890640795 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t3057062568  Vector2_get_zero_m111055298 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m1369565639 (RuntimeObject * __this /* static, unused */, CommandBuffer_t2673047760 * ___buf0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m1676858108 (CommandBuffer_t2673047760 * __this, bool ___disposing0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1666552611 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m1352738611 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CommandBuffer_Blit_Texture_m4259355444 (CommandBuffer_t2673047760 * __this, Texture_t2838694469 * ___source0, RenderTargetIdentifier_t3642434912 * ___dest1, Material_t2712136762 * ___mat2, int32_t ___pass3, Vector2_t3057062568  ___scale4, Vector2_t3057062568  ___offset5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527 (RuntimeObject * __this /* static, unused */, CommandBuffer_t2673047760 * ___self0, Texture_t2838694469 * ___source1, RenderTargetIdentifier_t3642434912 * ___dest2, Material_t2712136762 * ___mat3, int32_t ___pass4, Vector2_t3057062568 * ___scale5, Vector2_t3057062568 * ___offset6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m1757717066 (RenderTargetIdentifier_t3642434912 * __this, int32_t ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m2533785366 (RenderTargetIdentifier_t3642434912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1270711389 (RenderTargetIdentifier_t3642434912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  bool RenderTargetIdentifier_Equals_m1382118731 (RenderTargetIdentifier_t3642434912 * __this, RenderTargetIdentifier_t3642434912  ___rhs0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2199170207 (RenderTargetIdentifier_t3642434912 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m3132318046 (IndexOutOfRangeException_t3921978895 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void SphericalHarmonicsL2_set_Item_m967009223 (SphericalHarmonicsL2_t298540216 * __this, int32_t ___rgb0, int32_t ___coefficient1, float ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2623158889 (SphericalHarmonicsL2_t298540216 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3387909627 (RuntimeObject * __this /* static, unused */, SphericalHarmonicsL2_t298540216  ___lhs0, SphericalHarmonicsL2_t298540216  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern "C"  bool SphericalHarmonicsL2_Equals_m391510961 (SphericalHarmonicsL2_t298540216 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_set_ambientProbe_m1324021335 (RuntimeObject * __this /* static, unused */, SphericalHarmonicsL2_t298540216 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m566807488 (RuntimeObject * __this /* static, unused */, RenderTexture_t3361574884 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m4075470559 (RuntimeObject * __this /* static, unused */, RenderTexture_t3361574884 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m2175655275 (Resolution_t1041838152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m3498799763 (Resolution_t1041838152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m85524303 (Resolution_t1041838152 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m2202351169 (Scene_t548444562 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m2236524741 (Scene_t548444562 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m4035306960 (Scene_t548444562 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m3981208854(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2777655240 *, Scene_t548444562 , int32_t, const RuntimeMethod*))UnityAction_2_Invoke_m3981208854_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m2617017530(__this, p0, method) ((  void (*) (UnityAction_1_t2691974747 *, Scene_t548444562 , const RuntimeMethod*))UnityAction_1_Invoke_m2617017530_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m1895819670(__this, p0, p1, method) ((  void (*) (UnityAction_2_t562236193 *, Scene_t548444562 , Scene_t548444562 , const RuntimeMethod*))UnityAction_2_Invoke_m1895819670_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)
extern "C"  void Screen_INTERNAL_get_currentResolution_m1709057242 (RuntimeObject * __this /* static, unused */, Resolution_t1041838152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m132342697 (Object_t1970767703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m3676008935 (RuntimeObject * __this /* static, unused */, ScriptableObject_t2962125979 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2962125979 * ScriptableObject_CreateInstanceFromType_m1618254869 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m1613284443 (RequiredByNativeCodeAttribute_t917163987 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t2694382279_m835163552(__this, method) ((  GUILayer_t2694382279 * (*) (Component_t789413749 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2674875975_gshared)(__this, method)
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t1520723180 * GUILayer_HitTest_m1709809635 (GUILayer_t2694382279 * __this, Vector3_t329709361  ___screenPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1318052361 * Component_get_gameObject_m2399756717 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t329709361  Input_get_mousePosition_m354171928 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1239575985 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m2864360343 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t3897890150* ___cameras0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t3361574884 * Camera_get_targetTexture_m902197149 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4170278078 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___x0, Object_t1970767703 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t1344834245  Camera_get_pixelRect_m2697375247 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m2363379606 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___camera0, Vector3_t329709361  ___mousePosition1, HitInfo_t214434488 * ___hitInfo2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3847497448 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1448970001  Camera_ScreenPointToRay_m1775774407 (Camera_t989002943 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m3100038649 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m2174207718 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m2989971275 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_RaycastTry_m413943321 (Camera_t989002943 * __this, Ray_t1448970001  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1432164003 (Camera_t989002943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_RaycastTry2D_m2680111107 (Camera_t989002943 * __this, Ray_t1448970001  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m4819307 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t214434488  ___hit1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2439334524 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m2038728439 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m4245295910 (RuntimeObject * __this /* static, unused */, HitInfo_t214434488  ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m695071726 (HitInfo_t214434488 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m2657684222 (RuntimeObject * __this /* static, unused */, HitInfo_t214434488  ___lhs0, HitInfo_t214434488  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m2681843329 (GameObject_t1318052361 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m1639871891 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m3688535302 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m1110526867 (Sprite_t3012664695 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m477698307 (Sprite_t3012664695 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m3647917258 (Sprite_t3012664695 * __this, Vector4_t380635127 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m2332767386 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m40337108 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector2_t3057062568 * ___output1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1530107849 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Int32,System.Boolean)
extern "C"  void StackTrace__ctor_m2668716623 (StackTrace_t1080444557 * __this, int32_t p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m2461990048 (RuntimeObject * __this /* static, unused */, StackTrace_t1080444557 * ___stackTrace0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m2784677750 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m992845221 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m2246994706 (StringBuilder_t2355629516 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m414952842 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Exception::GetType()
extern "C"  Type_t * Exception_GetType_m200229271 (Exception_t2123675094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim()
extern "C"  String_t* String_Trim_m710734237 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m1227504536 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::get_InnerException()
extern "C"  Exception_t2123675094 * Exception_get_InnerException_m4268342947 (Exception_t2123675094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1756273763 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t2355629516 * StringBuilder_Append_m346941624 (StringBuilder_t2355629516 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t369357837* String_Split_m2076658130 (String_t* __this, CharU5BU5D_t83643201* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m1323985989 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m2557993735 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m170176446 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1618205500 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1375684648 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Remove(System.Int32,System.Int32)
extern "C"  String_t* String_Remove_m1510850277 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String,System.Int32)
extern "C"  int32_t String_IndexOf_m3924003364 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.Char,System.Char)
extern "C"  String_t* String_Replace_m2145106676 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.String)
extern "C"  int32_t String_LastIndexOf_m3640043114 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Insert(System.Int32,System.String)
extern "C"  String_t* String_Insert_m1416384255 (String_t* __this, int32_t p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m3268564145 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2515392467 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m115646220 (RuntimeObject * __this /* static, unused */, Texture_t2838694469 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m1514583028 (RuntimeObject * __this /* static, unused */, Texture_t2838694469 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m1813954261 (Texture_t2838694469 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3010839343 (Texture_t2838694469 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m3845050557 (RuntimeObject * __this /* static, unused */, Texture2D_t878840578 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D__ctor_m3327994032 (Texture2D_t878840578 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352 (RuntimeObject * __this /* static, unused */, Texture2D_t878840578 * ___self0, float ___u1, float ___v2, Color_t460381780 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m1443084076 (Texture2D_t878840578 * __this, Color32U5BU5D_t1505762612* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2104342217 (Texture2D_t878840578 * __this, Color32U5BU5D_t1505762612* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m1536952667 (Texture2D_t878840578 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t1505762612* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetBlockOfPixels32_m2327681220 (Texture2D_t878840578 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t1505762612* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m3107428142 (Texture2D_t878840578 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m4188294198 (Touch_t1822905180 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t3057062568  Touch_get_position_m884931166 (Touch_t1822905180 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m4210103310 (Touch_t1822905180 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3315114897 (Touch_t1822905180 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Object)
extern "C"  uint32_t Convert_ToUInt32_m602550190 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Boolean)
extern "C"  uint32_t Convert_ToUInt32_m1444157391 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270 (TouchScreenKeyboard_t3991761301 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m2816688376 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m4196840056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t3991761301 * TouchScreenKeyboard_Open_m2497313546 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m2368604044 (TouchScreenKeyboard_t3991761301 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m3891609934 (TouchScreenKeyboard_t3991761301 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m3187773712 (RuntimeObject * __this /* static, unused */, TrackedReference_t774033239 * ___x0, TrackedReference_t774033239 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  int32_t IntPtr_op_Explicit_m3542367511 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3701326763 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3294911734 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1243462975 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m2892246933 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m2449196661 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2761156409  Transform_get_rotation_m1089223433 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t329709361  Vector3_get_forward_m3509871422 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Quaternion_op_Multiply_m1160487389 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___rotation0, Vector3_t329709361  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m3828045280 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m3852742363 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m376748379 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m1591061908 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m3187990364 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m573024179 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t532597831 * Transform_get_parentInternal_m1711913369 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m3755172863 (Transform_t532597831 * __this, Transform_t532597831 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m276834655 (Transform_t532597831 * __this, Matrix4x4_t2375577114 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m3946760557 (RuntimeObject * __this /* static, unused */, Transform_t532597831 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069 (RuntimeObject * __this /* static, unused */, Transform_t532597831 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m2582217870 (Enumerator_t1378995202 * __this, Transform_t532597831 * ___outer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t532597831 * Transform_GetChild_m685835979 (Transform_t532597831 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2366507616 (Transform_t532597831 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.U2D.SpriteAtlas>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m4209780603(__this, p0, p1, method) ((  void (*) (Action_1_t1977832595 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m3238157784_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m186315368 (RequestAtlasCallback_t634135197 * __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C"  AppDomain_t3006601831 * AppDomain_get_CurrentDomain_m4287540733 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UnhandledExceptionEventHandler__ctor_m4261131196 (UnhandledExceptionEventHandler_t2340223587 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C"  void AppDomain_add_UnhandledException_m529960550 (AppDomain_t3006601831 * __this, UnhandledExceptionEventHandler_t2340223587 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern "C"  RuntimeObject * UnhandledExceptionEventArgs_get_ExceptionObject_m2966859680 (UnhandledExceptionEventArgs_t4227701455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m1818247004 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t2123675094 * ___e1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1234897647 (RuntimeObject * __this /* static, unused */, Exception_t2123675094 * ___exception0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.NativeClassAttribute::.ctor(System.String)
extern "C"  void NativeClassAttribute__ctor_m3709105044 (NativeClassAttribute_t4188729888 * __this, String_t* ___qualifiedCppName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___qualifiedCppName0;
		NativeClassAttribute_set_QualifiedNativeName_m2590354428(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2590354428 (NativeClassAttribute_t4188729888 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQualifiedNativeNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m2179581038 (MessageEventArgs_t2086846075 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern "C"  void PlayerConnection__ctor_m2316667390 (PlayerConnection_t781534341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection__ctor_m2316667390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t3406496741 * L_0 = (PlayerEditorConnectionEvents_t3406496741 *)il2cpp_codegen_object_new(PlayerEditorConnectionEvents_t3406496741_il2cpp_TypeInfo_var);
		PlayerEditorConnectionEvents__ctor_m1071995445(L_0, /*hidden argument*/NULL);
		__this->set_m_PlayerEditorConnectionEvents_3(L_0);
		List_1_t309455265 * L_1 = (List_1_t309455265 *)il2cpp_codegen_object_new(List_1_t309455265_il2cpp_TypeInfo_var);
		List_1__ctor_m3981019775(L_1, /*hidden argument*/List_1__ctor_m3981019775_RuntimeMethod_var);
		__this->set_m_connectedPlayers_4(L_1);
		ScriptableObject__ctor_m1532608883(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t781534341 * PlayerConnection_get_instance_m1185993489 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_instance_m1185993489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t781534341 * V_0 = NULL;
	{
		PlayerConnection_t781534341 * L_0 = ((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->get_s_Instance_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		PlayerConnection_t781534341 * L_2 = PlayerConnection_CreateInstance_m3407819880(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0028;
	}

IL_001d:
	{
		PlayerConnection_t781534341 * L_3 = ((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->get_s_Instance_6();
		V_0 = L_3;
		goto IL_0028;
	}

IL_0028:
	{
		PlayerConnection_t781534341 * L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern "C"  bool PlayerConnection_get_isConnected_m1595077683 (PlayerConnection_t781534341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_isConnected_m1595077683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject* L_0 = PlayerConnection_GetConnectionNativeApi_m3971624049(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean UnityEngine.IPlayerEditorConnectionNative::IsConnected() */, IPlayerEditorConnectionNative_t1924324047_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t781534341 * PlayerConnection_CreateInstance_m3407819880 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_CreateInstance_m3407819880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t781534341 * V_0 = NULL;
	{
		PlayerConnection_t781534341 * L_0 = ScriptableObject_CreateInstance_TisPlayerConnection_t781534341_m1156802871(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayerConnection_t781534341_m1156802871_RuntimeMethod_var);
		((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->set_s_Instance_6(L_0);
		PlayerConnection_t781534341 * L_1 = ((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->get_s_Instance_6();
		NullCheck(L_1);
		Object_set_hideFlags_m2630637173(L_1, ((int32_t)61), /*hidden argument*/NULL);
		PlayerConnection_t781534341 * L_2 = ((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->get_s_Instance_6();
		V_0 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		PlayerConnection_t781534341 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern "C"  RuntimeObject* PlayerConnection_GetConnectionNativeApi_m3971624049 (PlayerConnection_t781534341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_GetConnectionNativeApi_m3971624049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* G_B2_0 = NULL;
	RuntimeObject* G_B1_0 = NULL;
	{
		RuntimeObject* L_0 = ((PlayerConnection_t781534341_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t781534341_il2cpp_TypeInfo_var))->get_connectionNative_2();
		RuntimeObject* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		PlayerConnectionInternal_t984574678 * L_2 = (PlayerConnectionInternal_t984574678 *)il2cpp_codegen_object_new(PlayerConnectionInternal_t984574678_il2cpp_TypeInfo_var);
		PlayerConnectionInternal__ctor_m2836034707(L_2, /*hidden argument*/NULL);
		G_B2_0 = ((RuntimeObject*)(L_2));
	}

IL_0012:
	{
		V_0 = G_B2_0;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern "C"  void PlayerConnection_Register_m3298957340 (PlayerConnection_t781534341 * __this, Guid_t  ___messageId0, UnityAction_1_t4230376260 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_Register_m3298957340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m1475836212(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3637419113 * L_3 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_3, _stringLiteral200924526, _stringLiteral1786669557, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		PlayerEditorConnectionEvents_t3406496741 * L_4 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_4);
		List_1_t3095965032 * L_5 = L_4->get_messageTypeSubscribers_0();
		bool L_6 = Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0045;
		}
	}
	{
		RuntimeObject* L_7 = PlayerConnection_GetConnectionNativeApi_m3971624049(__this, /*hidden argument*/NULL);
		Guid_t  L_8 = ___messageId0;
		NullCheck(L_7);
		InterfaceActionInvoker1< Guid_t  >::Invoke(2 /* System.Void UnityEngine.IPlayerEditorConnectionNative::RegisterInternal(System.Guid) */, IPlayerEditorConnectionNative_t1924324047_il2cpp_TypeInfo_var, L_7, L_8);
	}

IL_0045:
	{
		PlayerEditorConnectionEvents_t3406496741 * L_9 = __this->get_m_PlayerEditorConnectionEvents_3();
		Guid_t  L_10 = ___messageId0;
		NullCheck(L_9);
		UnityEvent_1_t1359409152 * L_11 = PlayerEditorConnectionEvents_AddAndCreate_m3897452280(L_9, L_10, /*hidden argument*/NULL);
		UnityAction_1_t4230376260 * L_12 = ___callback1;
		NullCheck(L_11);
		UnityEvent_1_AddListener_m508826909(L_11, L_12, /*hidden argument*/UnityEvent_1_AddListener_m508826909_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterConnection_m2917454692 (PlayerConnection_t781534341 * __this, UnityAction_1_t2642535036 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_RegisterConnection_m2917454692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t1909216310  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t309455265 * L_0 = __this->get_m_connectedPlayers_4();
		NullCheck(L_0);
		Enumerator_t1909216310  L_1 = List_1_GetEnumerator_m2158044649(L_0, /*hidden argument*/List_1_GetEnumerator_m2158044649_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0013:
		{
			int32_t L_2 = Enumerator_get_Current_m991032171((&V_1), /*hidden argument*/Enumerator_get_Current_m991032171_RuntimeMethod_var);
			V_0 = L_2;
			UnityAction_1_t2642535036 * L_3 = ___callback0;
			int32_t L_4 = V_0;
			NullCheck(L_3);
			UnityAction_1_Invoke_m3292288955(L_3, L_4, /*hidden argument*/UnityAction_1_Invoke_m3292288955_RuntimeMethod_var);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m3878557316((&V_1), /*hidden argument*/Enumerator_MoveNext_m3878557316_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0013;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4143828380((&V_1), /*hidden argument*/Enumerator_Dispose_m4143828380_RuntimeMethod_var);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0043:
	{
		PlayerEditorConnectionEvents_t3406496741 * L_6 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_6);
		ConnectionChangeEvent_t1857494072 * L_7 = L_6->get_connectionEvent_1();
		UnityAction_1_t2642535036 * L_8 = ___callback0;
		NullCheck(L_7);
		UnityEvent_1_AddListener_m1699996981(L_7, L_8, /*hidden argument*/UnityEvent_1_AddListener_m1699996981_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterDisconnection_m3823650919 (PlayerConnection_t781534341 * __this, UnityAction_1_t2642535036 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_RegisterDisconnection_m3823650919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t3406496741 * L_0 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_0);
		ConnectionChangeEvent_t1857494072 * L_1 = L_0->get_disconnectionEvent_2();
		UnityAction_1_t2642535036 * L_2 = ___callback0;
		NullCheck(L_1);
		UnityEvent_1_AddListener_m1699996981(L_1, L_2, /*hidden argument*/UnityEvent_1_AddListener_m1699996981_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern "C"  void PlayerConnection_Send_m941112358 (PlayerConnection_t781534341 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3287329517* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_Send_m941112358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m1475836212(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3637419113 * L_3 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_3, _stringLiteral200924526, _stringLiteral1786669557, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		RuntimeObject* L_4 = PlayerConnection_GetConnectionNativeApi_m3971624049(__this, /*hidden argument*/NULL);
		Guid_t  L_5 = ___messageId0;
		ByteU5BU5D_t3287329517* L_6 = ___data1;
		NullCheck(L_4);
		InterfaceActionInvoker3< Guid_t , ByteU5BU5D_t3287329517*, int32_t >::Invoke(1 /* System.Void UnityEngine.IPlayerEditorConnectionNative::SendMessage(System.Guid,System.Byte[],System.Int32) */, IPlayerEditorConnectionNative_t1924324047_il2cpp_TypeInfo_var, L_4, L_5, L_6, 0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectAll()
extern "C"  void PlayerConnection_DisconnectAll_m1534374562 (PlayerConnection_t781534341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectAll_m1534374562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = PlayerConnection_GetConnectionNativeApi_m3971624049(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.IPlayerEditorConnectionNative::DisconnectAll() */, IPlayerEditorConnectionNative_t1924324047_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern "C"  void PlayerConnection_MessageCallbackInternal_m41337847 (RuntimeObject * __this /* static, unused */, intptr_t ___data0, uint64_t ___size1, uint64_t ___guid2, String_t* ___messageId3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_MessageCallbackInternal_m41337847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3287329517* V_0 = NULL;
	{
		V_0 = (ByteU5BU5D_t3287329517*)NULL;
		uint64_t L_0 = ___size1;
		if ((!(((uint64_t)L_0) > ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001f;
		}
	}
	{
		uint64_t L_1 = ___size1;
		if ((uint64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t3287329517*)SZArrayNew(ByteU5BU5D_t3287329517_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_1))));
		intptr_t L_2 = ___data0;
		ByteU5BU5D_t3287329517* L_3 = V_0;
		uint64_t L_4 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t436595762_il2cpp_TypeInfo_var);
		Marshal_Copy_m148013006(NULL /*static, unused*/, L_2, L_3, 0, (((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
	}

IL_001f:
	{
		PlayerConnection_t781534341 * L_5 = PlayerConnection_get_instance_m1185993489(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		PlayerEditorConnectionEvents_t3406496741 * L_6 = L_5->get_m_PlayerEditorConnectionEvents_3();
		String_t* L_7 = ___messageId3;
		Guid_t  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Guid__ctor_m2921421088((&L_8), L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_9 = V_0;
		uint64_t L_10 = ___guid2;
		NullCheck(L_6);
		PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4252710989(L_6, L_8, L_9, (((int32_t)((int32_t)L_10))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern "C"  void PlayerConnection_ConnectedCallbackInternal_m3339679254 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_ConnectedCallbackInternal_m3339679254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t781534341 * L_0 = PlayerConnection_get_instance_m1185993489(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t309455265 * L_1 = L_0->get_m_connectedPlayers_4();
		int32_t L_2 = ___playerId0;
		NullCheck(L_1);
		List_1_Add_m2519407053(L_1, L_2, /*hidden argument*/List_1_Add_m2519407053_RuntimeMethod_var);
		PlayerConnection_t781534341 * L_3 = PlayerConnection_get_instance_m1185993489(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerEditorConnectionEvents_t3406496741 * L_4 = L_3->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_4);
		ConnectionChangeEvent_t1857494072 * L_5 = L_4->get_connectionEvent_1();
		int32_t L_6 = ___playerId0;
		NullCheck(L_5);
		UnityEvent_1_Invoke_m2806368434(L_5, L_6, /*hidden argument*/UnityEvent_1_Invoke_m2806368434_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern "C"  void PlayerConnection_DisconnectedCallback_m1992762101 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectedCallback_m1992762101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t781534341 * L_0 = PlayerConnection_get_instance_m1185993489(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayerEditorConnectionEvents_t3406496741 * L_1 = L_0->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_1);
		ConnectionChangeEvent_t1857494072 * L_2 = L_1->get_disconnectionEvent_2();
		int32_t L_3 = ___playerId0;
		NullCheck(L_2);
		UnityEvent_1_Invoke_m2806368434(L_2, L_3, /*hidden argument*/UnityEvent_1_Invoke_m2806368434_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m1071995445 (PlayerEditorConnectionEvents_t3406496741 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents__ctor_m1071995445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3095965032 * L_0 = (List_1_t3095965032 *)il2cpp_codegen_object_new(List_1_t3095965032_il2cpp_TypeInfo_var);
		List_1__ctor_m2158277014(L_0, /*hidden argument*/List_1__ctor_m2158277014_RuntimeMethod_var);
		__this->set_messageTypeSubscribers_0(L_0);
		ConnectionChangeEvent_t1857494072 * L_1 = (ConnectionChangeEvent_t1857494072 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t1857494072_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m537474357(L_1, /*hidden argument*/NULL);
		__this->set_connectionEvent_1(L_1);
		ConnectionChangeEvent_t1857494072 * L_2 = (ConnectionChangeEvent_t1857494072 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t1857494072_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m537474357(L_2, /*hidden argument*/NULL);
		__this->set_disconnectionEvent_2(L_2);
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4252710989 (PlayerEditorConnectionEvents_t3406496741 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3287329517* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4252710989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	MessageEventArgs_t2086846075 * V_2 = NULL;
	MessageEventArgs_t2086846075 * V_3 = NULL;
	MessageTypeSubscribers_t3285514618 * V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * L_0 = (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 *)il2cpp_codegen_object_new(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035_il2cpp_TypeInfo_var);
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m937319274(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t3095965032 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m3681874696_RuntimeMethod_var;
		Func_2_t4252092203 * L_6 = (Func_2_t4252092203 *)il2cpp_codegen_object_new(Func_2_t4252092203_il2cpp_TypeInfo_var);
		Func_2__ctor_m2720484915(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m2720484915_RuntimeMethod_var);
		RuntimeObject* L_7 = Enumerable_Where_TisMessageTypeSubscribers_t3285514618_m2269018702(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_Where_TisMessageTypeSubscribers_t3285514618_m2269018702_RuntimeMethod_var);
		V_1 = L_7;
		RuntimeObject* L_8 = V_1;
		bool L_9 = Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t3285514618_m237661230_RuntimeMethod_var);
		if (L_9)
		{
			goto IL_0051;
		}
	}
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * L_10 = V_0;
		NullCheck(L_10);
		Guid_t  L_11 = L_10->get_messageId_0();
		Guid_t  L_12 = L_11;
		RuntimeObject * L_13 = Box(Guid_t_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1420639942(NULL /*static, unused*/, _stringLiteral1391582540, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_0051:
	{
		MessageEventArgs_t2086846075 * L_15 = (MessageEventArgs_t2086846075 *)il2cpp_codegen_object_new(MessageEventArgs_t2086846075_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2179581038(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		MessageEventArgs_t2086846075 * L_16 = V_3;
		int32_t L_17 = ___playerId2;
		NullCheck(L_16);
		L_16->set_playerId_0(L_17);
		MessageEventArgs_t2086846075 * L_18 = V_3;
		ByteU5BU5D_t3287329517* L_19 = ___data1;
		NullCheck(L_18);
		L_18->set_data_1(L_19);
		MessageEventArgs_t2086846075 * L_20 = V_3;
		V_2 = L_20;
		RuntimeObject* L_21 = V_1;
		NullCheck(L_21);
		RuntimeObject* L_22 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::GetEnumerator() */, IEnumerable_1_t3611986306_il2cpp_TypeInfo_var, L_21);
		V_5 = L_22;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008d;
		}

IL_0075:
		{
			RuntimeObject* L_23 = V_5;
			NullCheck(L_23);
			MessageTypeSubscribers_t3285514618 * L_24 = InterfaceFuncInvoker0< MessageTypeSubscribers_t3285514618 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::get_Current() */, IEnumerator_1_t174001016_il2cpp_TypeInfo_var, L_23);
			V_4 = L_24;
			MessageTypeSubscribers_t3285514618 * L_25 = V_4;
			NullCheck(L_25);
			MessageEvent_t3155234160 * L_26 = L_25->get_messageCallback_2();
			MessageEventArgs_t2086846075 * L_27 = V_2;
			NullCheck(L_26);
			UnityEvent_1_Invoke_m1430365874(L_26, L_27, /*hidden argument*/UnityEvent_1_Invoke_m1430365874_RuntimeMethod_var);
		}

IL_008d:
		{
			RuntimeObject* L_28 = V_5;
			NullCheck(L_28);
			bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1842762036_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0075;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_30 = V_5;
			if (!L_30)
			{
				goto IL_00ac;
			}
		}

IL_00a5:
		{
			RuntimeObject* L_31 = V_5;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3363289168_il2cpp_TypeInfo_var, L_31);
		}

IL_00ac:
		{
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_00ad:
	{
		return;
	}
}
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern "C"  UnityEvent_1_t1359409152 * PlayerEditorConnectionEvents_AddAndCreate_m3897452280 (PlayerEditorConnectionEvents_t3406496741 * __this, Guid_t  ___messageId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_AddAndCreate_m3897452280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * V_0 = NULL;
	MessageTypeSubscribers_t3285514618 * V_1 = NULL;
	MessageTypeSubscribers_t3285514618 * V_2 = NULL;
	UnityEvent_1_t1359409152 * V_3 = NULL;
	{
		U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * L_0 = (U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 *)il2cpp_codegen_object_new(U3CAddAndCreateU3Ec__AnonStorey1_t2037887867_il2cpp_TypeInfo_var);
		U3CAddAndCreateU3Ec__AnonStorey1__ctor_m782456925(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t3095965032 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m2574083174_RuntimeMethod_var;
		Func_2_t4252092203 * L_6 = (Func_2_t4252092203 *)il2cpp_codegen_object_new(Func_2_t4252092203_il2cpp_TypeInfo_var);
		Func_2__ctor_m2720484915(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m2720484915_RuntimeMethod_var);
		MessageTypeSubscribers_t3285514618 * L_7 = Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t3285514618_m1777054811(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t3285514618_m1777054811_RuntimeMethod_var);
		V_1 = L_7;
		MessageTypeSubscribers_t3285514618 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0059;
		}
	}
	{
		MessageTypeSubscribers_t3285514618 * L_9 = (MessageTypeSubscribers_t3285514618 *)il2cpp_codegen_object_new(MessageTypeSubscribers_t3285514618_il2cpp_TypeInfo_var);
		MessageTypeSubscribers__ctor_m2582810082(L_9, /*hidden argument*/NULL);
		V_2 = L_9;
		MessageTypeSubscribers_t3285514618 * L_10 = V_2;
		U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * L_11 = V_0;
		NullCheck(L_11);
		Guid_t  L_12 = L_11->get_messageId_0();
		NullCheck(L_10);
		MessageTypeSubscribers_set_MessageTypeId_m3146332482(L_10, L_12, /*hidden argument*/NULL);
		MessageTypeSubscribers_t3285514618 * L_13 = V_2;
		MessageEvent_t3155234160 * L_14 = (MessageEvent_t3155234160 *)il2cpp_codegen_object_new(MessageEvent_t3155234160_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m4280913397(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_messageCallback_2(L_14);
		MessageTypeSubscribers_t3285514618 * L_15 = V_2;
		V_1 = L_15;
		List_1_t3095965032 * L_16 = __this->get_messageTypeSubscribers_0();
		MessageTypeSubscribers_t3285514618 * L_17 = V_1;
		NullCheck(L_16);
		List_1_Add_m1776051518(L_16, L_17, /*hidden argument*/List_1_Add_m1776051518_RuntimeMethod_var);
	}

IL_0059:
	{
		MessageTypeSubscribers_t3285514618 * L_18 = V_1;
		MessageTypeSubscribers_t3285514618 * L_19 = L_18;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_subscriberCount_1();
		NullCheck(L_19);
		L_19->set_subscriberCount_1(((int32_t)((int32_t)L_20+(int32_t)1)));
		MessageTypeSubscribers_t3285514618 * L_21 = V_1;
		NullCheck(L_21);
		MessageEvent_t3155234160 * L_22 = L_21->get_messageCallback_2();
		V_3 = L_22;
		goto IL_0073;
	}

IL_0073:
	{
		UnityEvent_1_t1359409152 * L_23 = V_3;
		return L_23;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::.ctor()
extern "C"  void U3CAddAndCreateU3Ec__AnonStorey1__ctor_m782456925 (U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m2574083174 (U3CAddAndCreateU3Ec__AnonStorey1_t2037887867 * __this, MessageTypeSubscribers_t3285514618 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m2574083174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t3285514618 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m416571748(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m1475836212(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m937319274 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m3681874696 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t545998035 * __this, MessageTypeSubscribers_t3285514618 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m3681874696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t3285514618 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m416571748(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m1475836212(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m537474357 (ConnectionChangeEvent_t1857494072 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionChangeEvent__ctor_m537474357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m15913427(__this, /*hidden argument*/UnityEvent_1__ctor_m15913427_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m4280913397 (MessageEvent_t3155234160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEvent__ctor_m4280913397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m2017699965(__this, /*hidden argument*/UnityEvent_1__ctor_m2017699965_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m2582810082 (MessageTypeSubscribers_t3285514618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageTypeSubscribers__ctor_m2582810082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_subscriberCount_1(0);
		MessageEvent_t3155234160 * L_0 = (MessageEvent_t3155234160 *)il2cpp_codegen_object_new(MessageEvent_t3155234160_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m4280913397(L_0, /*hidden argument*/NULL);
		__this->set_messageCallback_2(L_0);
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m416571748 (MessageTypeSubscribers_t3285514618 * __this, const RuntimeMethod* method)
{
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_messageTypeId_0();
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m2921421088((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Guid_t  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern "C"  void MessageTypeSubscribers_set_MessageTypeId_m3146332482 (MessageTypeSubscribers_t3285514618 * __this, Guid_t  ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Guid_ToString_m659365756((&___value0), /*hidden argument*/NULL);
		__this->set_m_messageTypeId_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1970767703_marshal_pinvoke(const Object_t1970767703& unmarshaled, Object_t1970767703_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t1970767703_marshal_pinvoke_back(const Object_t1970767703_marshaled_pinvoke& marshaled, Object_t1970767703& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1970767703_marshal_pinvoke_cleanup(Object_t1970767703_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1970767703_marshal_com(const Object_t1970767703& unmarshaled, Object_t1970767703_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t1970767703_marshal_com_back(const Object_t1970767703_marshaled_com& marshaled, Object_t1970767703& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1970767703_marshal_com_cleanup(Object_t1970767703_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m132342697 (Object_t1970767703 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1970767703 * Object_Internal_CloneSingle_m272411890 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___data0, const RuntimeMethod* method)
{
	typedef Object_t1970767703 * (*Object_Internal_CloneSingle_m272411890_ftn) (Object_t1970767703 *);
	static Object_Internal_CloneSingle_m272411890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m272411890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	Object_t1970767703 * retVal = _il2cpp_icall_func(___data0);
	return retVal;
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1970767703 * Object_Internal_InstantiateSingle_m3669590211 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___data0, Vector3_t329709361  ___pos1, Quaternion_t2761156409  ___rot2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m3669590211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1970767703 * V_0 = NULL;
	{
		Object_t1970767703 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_t1970767703 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t1970767703 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1970767703 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___data0, Vector3_t329709361 * ___pos1, Quaternion_t2761156409 * ___rot2, const RuntimeMethod* method)
{
	typedef Object_t1970767703 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147_ftn) (Object_t1970767703 *, Vector3_t329709361 *, Quaternion_t2761156409 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m2163512147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	Object_t1970767703 * retVal = _il2cpp_icall_func(___data0, ___pos1, ___rot2);
	return retVal;
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m1310479296 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, float ___t1, const RuntimeMethod* method)
{
	typedef void (*Object_Destroy_m1310479296_ftn) (Object_t1970767703 *, float);
	static Object_Destroy_m1310479296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m1310479296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m935298371 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m935298371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1970767703 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_Destroy_m1310479296(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3659857569 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method)
{
	typedef void (*Object_DestroyImmediate_m3659857569_ftn) (Object_t1970767703 *, bool);
	static Object_DestroyImmediate_m3659857569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3659857569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m3678630788 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m3678630788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1970767703 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3659857569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t3487290734* Object_FindObjectsOfType_m2534233342 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ObjectU5BU5D_t3487290734* (*Object_FindObjectsOfType_m2534233342_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2534233342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2534233342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	ObjectU5BU5D_t3487290734* retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m461114069 (Object_t1970767703 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_get_name_m461114069_ftn) (Object_t1970767703 *);
	static Object_get_name_m461114069_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m461114069_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m2629735859 (Object_t1970767703 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_name_m2629735859_ftn) (Object_t1970767703 *, String_t*);
	static Object_set_name_m2629735859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m2629735859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m1854791291 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___target0, const RuntimeMethod* method)
{
	typedef void (*Object_DontDestroyOnLoad_m1854791291_ftn) (Object_t1970767703 *);
	static Object_DontDestroyOnLoad_m1854791291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m1854791291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2630637173 (Object_t1970767703 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_hideFlags_m2630637173_ftn) (Object_t1970767703 *, int32_t);
	static Object_set_hideFlags_m2630637173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2630637173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m535622928 (Object_t1970767703 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_ToString_m535622928_ftn) (Object_t1970767703 *);
	static Object_ToString_m535622928_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m535622928_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m464675639 (Object_t1970767703 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m2809416424(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m828978334 (Object_t1970767703 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m828978334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1970767703 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___other0;
		V_0 = ((Object_t1970767703 *)IsInstClass((RuntimeObject*)L_0, Object_t1970767703_il2cpp_TypeInfo_var));
		Object_t1970767703 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_1, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_4 = ___other0;
		if (((Object_t1970767703 *)IsInstClass((RuntimeObject*)L_4, Object_t1970767703_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t1970767703 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m4209539787(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1890640795 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m1890640795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1970767703 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m4209539787(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m4209539787 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___lhs0, Object_t1970767703 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m4209539787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t1970767703 * L_0 = ___lhs0;
		V_0 = (bool)((((RuntimeObject*)(Object_t1970767703 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		Object_t1970767703 * L_1 = ___rhs1;
		V_1 = (bool)((((RuntimeObject*)(Object_t1970767703 *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t1970767703 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m2316845610(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t1970767703 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m2316845610(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t1970767703 * L_10 = ___lhs0;
		Object_t1970767703 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m1116264796(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m2316845610 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m2316845610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1970767703 * L_0 = ___o0;
		NullCheck(L_0);
		intptr_t L_1 = Object_GetCachedPtr_m434724044(L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Inequality_m4173155962(NULL /*static, unused*/, L_1, (intptr_t)(0), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m434724044 (Object_t1970767703 * __this, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		intptr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		intptr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1970767703 * Object_Instantiate_m709640951 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___original0, Vector3_t329709361  ___position1, Quaternion_t2761156409  ___rotation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m709640951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1970767703 * V_0 = NULL;
	{
		Object_t1970767703 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m426474767(NULL /*static, unused*/, L_0, _stringLiteral568845039, /*hidden argument*/NULL);
		Object_t1970767703 * L_1 = ___original0;
		if (!((ScriptableObject_t2962125979 *)IsInstClass((RuntimeObject*)L_1, ScriptableObject_t2962125979_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_2, _stringLiteral3499848769, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t1970767703 * L_3 = ___original0;
		Vector3_t329709361  L_4 = ___position1;
		Quaternion_t2761156409  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object_t1970767703 * L_6 = Object_Internal_InstantiateSingle_m3669590211(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t1970767703 * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m426474767 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m426474767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1910042615 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___x0, Object_t1970767703 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m1910042615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1970767703 * L_0 = ___x0;
		Object_t1970767703 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m4209539787(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4170278078 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___x0, Object_t1970767703 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m4170278078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1970767703 * L_0 = ___x0;
		Object_t1970767703 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m4209539787(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m3981639895 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m3981639895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t1970767703_StaticFields*)il2cpp_codegen_static_fields_for(Object_t1970767703_il2cpp_TypeInfo_var))->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2699116517 (Plane_t1350213727 * __this, Vector3_t329709361  ___inNormal0, Vector3_t329709361  ___inPoint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane__ctor_m2699116517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t329709361  L_0 = ___inNormal0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_1 = Vector3_Normalize_m2417808733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t329709361  L_2 = __this->get_m_Normal_0();
		Vector3_t329709361  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m3905706415(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2699116517_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___inNormal0, Vector3_t329709361  ___inPoint1, const RuntimeMethod* method)
{
	Plane_t1350213727 * _thisAdjusted = reinterpret_cast<Plane_t1350213727 *>(__this + 1);
	Plane__ctor_m2699116517(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m995144852 (Plane_t1350213727 * __this, Ray_t1448970001  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m995144852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t329709361  L_0 = Ray_get_direction_m777501070((&___ray0), /*hidden argument*/NULL);
		Vector3_t329709361  L_1 = __this->get_m_Normal_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m3905706415(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t329709361  L_3 = Ray_get_origin_m3562529566((&___ray0), /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = __this->get_m_Normal_0();
		float L_5 = Vector3_Dot_m3905706415(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_m_Distance_1();
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1485428512(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m995144852_AdjustorThunk (RuntimeObject * __this, Ray_t1448970001  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	Plane_t1350213727 * _thisAdjusted = reinterpret_cast<Plane_t1350213727 *>(__this + 1);
	return Plane_Raycast_m995144852(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m2969686206 (Plane_t1350213727 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_ToString_m2969686206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		Vector3_t329709361 * L_1 = __this->get_address_of_m_Normal_0();
		float L_2 = L_1->get_x_1();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_4);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t1568665923* L_5 = L_0;
		Vector3_t329709361 * L_6 = __this->get_address_of_m_Normal_0();
		float L_7 = L_6->get_y_2();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		ObjectU5BU5D_t1568665923* L_10 = L_5;
		Vector3_t329709361 * L_11 = __this->get_address_of_m_Normal_0();
		float L_12 = L_11->get_z_3();
		float L_13 = L_12;
		RuntimeObject * L_14 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_14);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_14);
		ObjectU5BU5D_t1568665923* L_15 = L_10;
		float L_16 = __this->get_m_Distance_1();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_18);
		String_t* L_19 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral815514077, L_15, /*hidden argument*/NULL);
		V_0 = L_19;
		goto IL_005e;
	}

IL_005e:
	{
		String_t* L_20 = V_0;
		return L_20;
	}
}
extern "C"  String_t* Plane_ToString_m2969686206_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Plane_t1350213727 * _thisAdjusted = reinterpret_cast<Plane_t1350213727 *>(__this + 1);
	return Plane_ToString_m2969686206(_thisAdjusted, method);
}
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m2275210789 (Playable_t348555058 * __this, PlayableHandle_t3963382032  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t3963382032  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void Playable__ctor_m2275210789_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t3963382032  ___handle0, const RuntimeMethod* method)
{
	Playable_t348555058 * _thisAdjusted = reinterpret_cast<Playable_t348555058 *>(__this + 1);
	Playable__ctor_m2275210789(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t348555058  Playable_get_Null_m799873240 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_get_Null_m799873240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t348555058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t348555058_il2cpp_TypeInfo_var);
		Playable_t348555058  L_0 = ((Playable_t348555058_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t348555058_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Playable_t348555058  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t3963382032  Playable_GetHandle_m1535596094 (Playable_t348555058 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t3963382032  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t3963382032  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t3963382032  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t3963382032  Playable_GetHandle_m1535596094_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Playable_t348555058 * _thisAdjusted = reinterpret_cast<Playable_t348555058 *>(__this + 1);
	return Playable_GetHandle_m1535596094(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m1514203323 (Playable_t348555058 * __this, Playable_t348555058  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t3963382032  L_0 = Playable_GetHandle_m1535596094(__this, /*hidden argument*/NULL);
		PlayableHandle_t3963382032  L_1 = Playable_GetHandle_m1535596094((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m447576049(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool Playable_Equals_m1514203323_AdjustorThunk (RuntimeObject * __this, Playable_t348555058  ___other0, const RuntimeMethod* method)
{
	Playable_t348555058 * _thisAdjusted = reinterpret_cast<Playable_t348555058 *>(__this + 1);
	return Playable_Equals_m1514203323(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.Playable::.cctor()
extern "C"  void Playable__cctor_m2813807852 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable__cctor_m2813807852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t3963382032  L_0 = PlayableHandle_get_Null_m428254764(NULL /*static, unused*/, /*hidden argument*/NULL);
		Playable_t348555058  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m2275210789((&L_1), L_0, /*hidden argument*/NULL);
		((Playable_t348555058_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t348555058_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern "C"  void PlayableAsset__ctor_m191177745 (PlayableAsset_t2447076843 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m1532608883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern "C"  double PlayableAsset_get_duration_m1385497653 (PlayableAsset_t2447076843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_get_duration_m1385497653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayableBinding_t181640494_il2cpp_TypeInfo_var);
		double L_0 = ((PlayableBinding_t181640494_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t181640494_il2cpp_TypeInfo_var))->get_DefaultDuration_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern "C"  void PlayableAsset_Internal_CreatePlayable_m1139410774 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2447076843 * ___asset0, PlayableGraph_t2451555590  ___graph1, GameObject_t1318052361 * ___go2, intptr_t ___ptr3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_Internal_CreatePlayable_m1139410774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t348555058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t348555058 * V_1 = NULL;
	{
		PlayableAsset_t2447076843 * L_0 = ___asset0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t348555058_il2cpp_TypeInfo_var);
		Playable_t348555058  L_2 = Playable_get_Null_m799873240(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0018:
	{
		PlayableAsset_t2447076843 * L_3 = ___asset0;
		PlayableGraph_t2451555590  L_4 = ___graph1;
		GameObject_t1318052361 * L_5 = ___go2;
		NullCheck(L_3);
		Playable_t348555058  L_6 = VirtFuncInvoker2< Playable_t348555058 , PlayableGraph_t2451555590 , GameObject_t1318052361 * >::Invoke(4 /* UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject) */, L_3, L_4, L_5);
		V_0 = L_6;
	}

IL_0021:
	{
		void* L_7 = IntPtr_ToPointer_m2850170309((&___ptr3), /*hidden argument*/NULL);
		V_1 = (Playable_t348555058 *)L_7;
		Playable_t348555058 * L_8 = V_1;
		Playable_t348555058  L_9 = V_0;
		*(Playable_t348555058 *)L_8 = L_9;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern "C"  void PlayableAsset_Internal_GetPlayableAssetDuration_m1748060470 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2447076843 * ___asset0, intptr_t ___ptrToDouble1, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double* V_1 = NULL;
	{
		PlayableAsset_t2447076843 * L_0 = ___asset0;
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(5 /* System.Double UnityEngine.Playables.PlayableAsset::get_duration() */, L_0);
		V_0 = L_1;
		void* L_2 = IntPtr_ToPointer_m2850170309((&___ptrToDouble1), /*hidden argument*/NULL);
		V_1 = (double*)L_2;
		double* L_3 = V_1;
		double L_4 = V_0;
		*((double*)(L_3)) = (double)L_4;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern "C"  void PlayableBehaviour__ctor_m1295706842 (PlayableBehaviour_t2742200540 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern "C"  RuntimeObject * PlayableBehaviour_Clone_m3866932923 (PlayableBehaviour_t2742200540 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = Object_MemberwiseClone_m38416816(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t181640494_marshal_pinvoke(const PlayableBinding_t181640494& unmarshaled, PlayableBinding_t181640494_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t181640494_marshal_pinvoke_back(const PlayableBinding_t181640494_marshaled_pinvoke& marshaled, PlayableBinding_t181640494& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t181640494_marshal_pinvoke_cleanup(PlayableBinding_t181640494_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t181640494_marshal_com(const PlayableBinding_t181640494& unmarshaled, PlayableBinding_t181640494_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t181640494_marshal_com_back(const PlayableBinding_t181640494_marshaled_com& marshaled, PlayableBinding_t181640494& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t181640494_marshal_com_cleanup(PlayableBinding_t181640494_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern "C"  void PlayableBinding__cctor_m513300806 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableBinding__cctor_m513300806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayableBinding_t181640494_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t181640494_il2cpp_TypeInfo_var))->set_None_0(((PlayableBindingU5BU5D_t3010105947*)SZArrayNew(PlayableBindingU5BU5D_t3010105947_il2cpp_TypeInfo_var, (uint32_t)0)));
		((PlayableBinding_t181640494_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t181640494_il2cpp_TypeInfo_var))->set_DefaultDuration_1((std::numeric_limits<double>::infinity()));
		return;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2989374346 (PlayableHandle_t3963382032 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayableHandle_IsValidInternal_m3883546329(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool PlayableHandle_IsValid_m2989374346_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t3963382032 * _thisAdjusted = reinterpret_cast<PlayableHandle_t3963382032 *>(__this + 1);
	return PlayableHandle_IsValid_m2989374346(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m3883546329 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t3963382032 * L_0 = ___playable0;
		bool L_1 = PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method)
{
	typedef bool (*PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567_ftn) (PlayableHandle_t3963382032 *);
	static PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_IsValidInternal_m881722567_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_GetPlayableTypeOf_m2899014050 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		PlayableHandle_t3963382032 * L_0 = ___playable0;
		Type_t * L_1 = PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032 * ___playable0, const RuntimeMethod* method)
{
	typedef Type_t * (*PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490_ftn) (PlayableHandle_t3963382032 *);
	static PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2768541490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)");
	Type_t * retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t3963382032  PlayableHandle_get_Null_m428254764 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_get_Null_m428254764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t3963382032  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t3963382032  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableHandle_t3963382032_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)10));
		PlayableHandle_t3963382032  L_0 = V_0;
		V_1 = L_0;
		goto IL_0019;
	}

IL_0019:
	{
		PlayableHandle_t3963382032  L_1 = V_1;
		return L_1;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m447576049 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032  ___x0, PlayableHandle_t3963382032  ___y1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t3963382032  L_0 = ___x0;
		PlayableHandle_t3963382032  L_1 = ___y1;
		bool L_2 = PlayableHandle_CompareVersion_m1964937316(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m2260768344 (PlayableHandle_t3963382032 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_Equals_m2260768344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___p0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableHandle_t3963382032_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableHandle_CompareVersion_m1964937316(NULL /*static, unused*/, (*(PlayableHandle_t3963382032 *)__this), ((*(PlayableHandle_t3963382032 *)((PlayableHandle_t3963382032 *)UnBox(L_1, PlayableHandle_t3963382032_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002a;
	}

IL_002a:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableHandle_Equals_m2260768344_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableHandle_t3963382032 * _thisAdjusted = reinterpret_cast<PlayableHandle_t3963382032 *>(__this + 1);
	return PlayableHandle_Equals_m2260768344(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m1373097937 (PlayableHandle_t3963382032 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m2693920867(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m3782351173(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableHandle_GetHashCode_m1373097937_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t3963382032 * _thisAdjusted = reinterpret_cast<PlayableHandle_t3963382032 *>(__this + 1);
	return PlayableHandle_GetHashCode_m1373097937(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m1964937316 (RuntimeObject * __this /* static, unused */, PlayableHandle_t3963382032  ___lhs0, PlayableHandle_t3963382032  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2581237927 (PlayableOutput_t1141860262 * __this, PlayableOutputHandle_t1112988996  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t1112988996  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void PlayableOutput__ctor_m2581237927_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t1112988996  ___handle0, const RuntimeMethod* method)
{
	PlayableOutput_t1141860262 * _thisAdjusted = reinterpret_cast<PlayableOutput_t1141860262 *>(__this + 1);
	PlayableOutput__ctor_m2581237927(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t1112988996  PlayableOutput_GetHandle_m3642617554 (PlayableOutput_t1141860262 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1112988996  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t1112988996  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t1112988996  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t1112988996  PlayableOutput_GetHandle_m3642617554_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutput_t1141860262 * _thisAdjusted = reinterpret_cast<PlayableOutput_t1141860262 *>(__this + 1);
	return PlayableOutput_GetHandle_m3642617554(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m1397146541 (PlayableOutput_t1141860262 * __this, PlayableOutput_t1141860262  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t1112988996  L_0 = PlayableOutput_GetHandle_m3642617554(__this, /*hidden argument*/NULL);
		PlayableOutputHandle_t1112988996  L_1 = PlayableOutput_GetHandle_m3642617554((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableOutputHandle_op_Equality_m3380206387(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutput_Equals_m1397146541_AdjustorThunk (RuntimeObject * __this, PlayableOutput_t1141860262  ___other0, const RuntimeMethod* method)
{
	PlayableOutput_t1141860262 * _thisAdjusted = reinterpret_cast<PlayableOutput_t1141860262 *>(__this + 1);
	return PlayableOutput_Equals_m1397146541(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern "C"  void PlayableOutput__cctor_m2104493696 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutput__cctor_m2104493696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableOutputHandle_t1112988996  L_0 = PlayableOutputHandle_get_Null_m373902066(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayableOutput_t1141860262  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m2581237927((&L_1), L_0, /*hidden argument*/NULL);
		((PlayableOutput_t1141860262_StaticFields*)il2cpp_codegen_static_fields_for(PlayableOutput_t1141860262_il2cpp_TypeInfo_var))->set_m_NullPlayableOutput_1(L_1);
		return;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t1112988996  PlayableOutputHandle_get_Null_m373902066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_get_Null_m373902066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableOutputHandle_t1112988996  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableOutputHandle_t1112988996  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableOutputHandle_t1112988996_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)2147483647LL));
		PlayableOutputHandle_t1112988996  L_0 = V_0;
		V_1 = L_0;
		goto IL_001c;
	}

IL_001c:
	{
		PlayableOutputHandle_t1112988996  L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m3692544427 (PlayableOutputHandle_t1112988996 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m2693920867(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m3782351173(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m3692544427_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1112988996 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t1112988996 *>(__this + 1);
	return PlayableOutputHandle_GetHashCode_m3692544427(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m3380206387 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1112988996  ___lhs0, PlayableOutputHandle_t1112988996  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t1112988996  L_0 = ___lhs0;
		PlayableOutputHandle_t1112988996  L_1 = ___rhs1;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3807837470(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m4139373885 (PlayableOutputHandle_t1112988996 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_Equals_m4139373885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___p0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableOutputHandle_t1112988996_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3807837470(NULL /*static, unused*/, (*(PlayableOutputHandle_t1112988996 *)__this), ((*(PlayableOutputHandle_t1112988996 *)((PlayableOutputHandle_t1112988996 *)UnBox(L_1, PlayableOutputHandle_t1112988996_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutputHandle_Equals_m4139373885_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1112988996 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t1112988996 *>(__this + 1);
	return PlayableOutputHandle_Equals_m4139373885(_thisAdjusted, ___p0, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3807837470 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1112988996  ___lhs0, PlayableOutputHandle_t1112988996  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern "C"  void PlayerConnectionInternal__ctor_m2836034707 (PlayerConnectionInternal_t984574678 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.SendMessage(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m1332611929 (PlayerConnectionInternal_t984574678 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3287329517* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m1332611929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m1475836212(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t3637419113 * L_3 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_3, _stringLiteral2334848428, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		String_t* L_4 = Guid_ToString_m1281603392((&___messageId0), _stringLiteral4027178674, /*hidden argument*/NULL);
		ByteU5BU5D_t3287329517* L_5 = ___data1;
		int32_t L_6 = ___playerId2;
		PlayerConnectionInternal_SendMessage_m2860749805(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.RegisterInternal(System.Guid)
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m3972857555 (PlayerConnectionInternal_t984574678 * __this, Guid_t  ___messageId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m3972857555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Guid_ToString_m1281603392((&___messageId0), _stringLiteral4027178674, /*hidden argument*/NULL);
		PlayerConnectionInternal_RegisterInternal_m1526953951(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.IsConnected()
extern "C"  bool PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_m399242399 (PlayerConnectionInternal_t984574678 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayerConnectionInternal_IsConnected_m3995228764(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.DisconnectAll()
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m658434311 (PlayerConnectionInternal_t984574678 * __this, const RuntimeMethod* method)
{
	{
		PlayerConnectionInternal_DisconnectAll_m2331867028(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern "C"  bool PlayerConnectionInternal_IsConnected_m3995228764 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*PlayerConnectionInternal_IsConnected_m3995228764_ftn) ();
	static PlayerConnectionInternal_IsConnected_m3995228764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_IsConnected_m3995228764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::IsConnected()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern "C"  void PlayerConnectionInternal_RegisterInternal_m1526953951 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_RegisterInternal_m1526953951_ftn) (String_t*);
	static PlayerConnectionInternal_RegisterInternal_m1526953951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_RegisterInternal_m1526953951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)");
	_il2cpp_icall_func(___messageId0);
}
// System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_SendMessage_m2860749805 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, ByteU5BU5D_t3287329517* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_SendMessage_m2860749805_ftn) (String_t*, ByteU5BU5D_t3287329517*, int32_t);
	static PlayerConnectionInternal_SendMessage_m2860749805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_SendMessage_m2860749805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)");
	_il2cpp_icall_func(___messageId0, ___data1, ___playerId2);
}
// System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern "C"  void PlayerConnectionInternal_DisconnectAll_m2331867028 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_DisconnectAll_m2331867028_ftn) ();
	static PlayerConnectionInternal_DisconnectAll_m2331867028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_DisconnectAll_m2331867028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::DisconnectAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3365316696 (PropertyAttribute_t69305138 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m3929666074 (PropertyName_t3644065956 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		PropertyName_t3644065956  L_1 = PropertyNameUtils_PropertyNameFromString_m353937208(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		PropertyName__ctor_m780200493(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PropertyName__ctor_m3929666074_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	PropertyName__ctor_m3929666074(_thisAdjusted, ___name0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m780200493 (PropertyName_t3644065956 * __this, PropertyName_t3644065956  ___other0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (&___other0)->get_id_0();
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m780200493_AdjustorThunk (RuntimeObject * __this, PropertyName_t3644065956  ___other0, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	PropertyName__ctor_m780200493(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m247674800 (PropertyName_t3644065956 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m247674800_AdjustorThunk (RuntimeObject * __this, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	PropertyName__ctor_m247674800(_thisAdjusted, ___id0, method);
}
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m3142464868 (RuntimeObject * __this /* static, unused */, PropertyName_t3644065956  ___lhs0, PropertyName_t3644065956  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.PropertyName::op_Inequality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Inequality_m1792458410 (RuntimeObject * __this /* static, unused */, PropertyName_t3644065956  ___lhs0, PropertyName_t3644065956  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m2665292733 (PropertyName_t3644065956 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_id_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t PropertyName_GetHashCode_m2665292733_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	return PropertyName_GetHashCode_m2665292733(_thisAdjusted, method);
}
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m1268943811 (PropertyName_t3644065956 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_Equals_m1268943811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PropertyName_t3644065956_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___other0;
		bool L_2 = PropertyName_op_Equality_m3142464868(NULL /*static, unused*/, (*(PropertyName_t3644065956 *)__this), ((*(PropertyName_t3644065956 *)((PropertyName_t3644065956 *)UnBox(L_1, PropertyName_t3644065956_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PropertyName_Equals_m1268943811_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	return PropertyName_Equals_m1268943811(_thisAdjusted, ___other0, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.String)
extern "C"  PropertyName_t3644065956  PropertyName_op_Implicit_m4261179411 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3644065956  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyName_t3644065956  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m3929666074((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t3644065956  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.Int32)
extern "C"  PropertyName_t3644065956  PropertyName_op_Implicit_m3617053647 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t3644065956  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___id0;
		PropertyName_t3644065956  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m247674800((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t3644065956  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1247093862 (PropertyName_t3644065956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_ToString_m1247093862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_id_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2126999818(NULL /*static, unused*/, _stringLiteral984751784, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
extern "C"  String_t* PropertyName_ToString_m1247093862_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t3644065956 * _thisAdjusted = reinterpret_cast<PropertyName_t3644065956 *>(__this + 1);
	return PropertyName_ToString_m1247093862(_thisAdjusted, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t3644065956  PropertyNameUtils_PropertyNameFromString_m353937208 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3644065956  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyNameUtils_PropertyNameFromString_Injected_m1655027246(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t3644065956  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m1655027246 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t3644065956 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*PropertyNameUtils_PropertyNameFromString_Injected_m1655027246_ftn) (String_t*, PropertyName_t3644065956 *);
	static PropertyNameUtils_PropertyNameFromString_Injected_m1655027246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PropertyNameUtils_PropertyNameFromString_Injected_m1655027246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___name0, ___ret1);
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3784845077 (Quaternion_t2761156409 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m3784845077_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	Quaternion_t2761156409 * _thisAdjusted = reinterpret_cast<Quaternion_t2761156409 *>(__this + 1);
	Quaternion__ctor_m3784845077(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2761156409  Quaternion_LookRotation_m358999601 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___forward0, Vector3_t329709361  ___upwards1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_LookRotation_m358999601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_LookRotation_m3131108156(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2761156409  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t2761156409  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3131108156 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___forward0, Vector3_t329709361 * ___upwards1, Quaternion_t2761156409 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m3131108156_ftn) (Vector3_t329709361 *, Vector3_t329709361 *, Quaternion_t2761156409 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m3131108156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m3131108156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t2761156409  Quaternion_Inverse_m1565765131 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___rotation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Inverse_m1565765131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Inverse_m3061492299(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2761156409  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t2761156409  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m3061492299 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409 * ___rotation0, Quaternion_t2761156409 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m3061492299_ftn) (Quaternion_t2761156409 *, Quaternion_t2761156409 *);
	static Quaternion_INTERNAL_CALL_Inverse_m3061492299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m3061492299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t2761156409  Quaternion_Euler_m3622738752 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Euler_m3622738752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t329709361  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3984700005((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_4 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Quaternion_t2761156409  L_5 = Quaternion_Internal_FromEulerRad_m2232655408(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t2761156409  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t2761156409  Quaternion_Internal_FromEulerRad_m2232655408 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___euler0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Internal_FromEulerRad_m2232655408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2761156409  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t2761156409  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___euler0, Quaternion_t2761156409 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470_ftn) (Vector3_t329709361 *, Quaternion_t2761156409 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m3685480470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2761156409  Quaternion_get_identity_m2232226055 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_get_identity_m2232226055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Quaternion_t2761156409  L_0 = ((Quaternion_t2761156409_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t2761156409_il2cpp_TypeInfo_var))->get_identityQuaternion_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Quaternion_t2761156409  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2761156409  Quaternion_op_Multiply_m1390148054 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___lhs0, Quaternion_t2761156409  ___rhs1, const RuntimeMethod* method)
{
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t2761156409  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m3784845077((&L_32), ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t2761156409  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Quaternion_op_Multiply_m1160487389 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___rotation0, Vector3_t329709361  ___point1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t329709361  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t329709361  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t329709361  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t329709361  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m3822093777 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___lhs0, Quaternion_t2761156409  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Equality_m3822093777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t2761156409  L_0 = ___lhs0;
		Quaternion_t2761156409  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Dot_m342780875(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m2565209256 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___lhs0, Quaternion_t2761156409  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Inequality_m2565209256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t2761156409  L_0 = ___lhs0;
		Quaternion_t2761156409  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		bool L_2 = Quaternion_op_Equality_m3822093777(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m342780875 (RuntimeObject * __this /* static, unused */, Quaternion_t2761156409  ___a0, Quaternion_t2761156409  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m1455437325 (Quaternion_t2761156409 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3104521605(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3104521605(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3104521605(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3104521605(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m1455437325_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t2761156409 * _thisAdjusted = reinterpret_cast<Quaternion_t2761156409 *>(__this + 1);
	return Quaternion_GetHashCode_m1455437325(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m1356692124 (Quaternion_t2761156409 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m1356692124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Quaternion_t2761156409_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t2761156409 *)((Quaternion_t2761156409 *)UnBox(L_1, Quaternion_t2761156409_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m54068117(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m54068117(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m54068117(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m54068117(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m1356692124_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Quaternion_t2761156409 * _thisAdjusted = reinterpret_cast<Quaternion_t2761156409 *>(__this + 1);
	return Quaternion_Equals_m1356692124(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m1462711549 (Quaternion_t2761156409 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m1462711549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral409661081, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m1462711549_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t2761156409 * _thisAdjusted = reinterpret_cast<Quaternion_t2761156409 *>(__this + 1);
	return Quaternion_ToString_m1462711549(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::.cctor()
extern "C"  void Quaternion__cctor_m3320953330 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion__cctor_m3320953330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2761156409  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m3784845077((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Quaternion_t2761156409_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t2761156409_il2cpp_TypeInfo_var))->set_identityQuaternion_4(L_0);
		return;
	}
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2497685183 (RuntimeObject * __this /* static, unused */, float ___min0, float ___max1, const RuntimeMethod* method)
{
	typedef float (*Random_Range_m2497685183_ftn) (float, float);
	static Random_Range_m2497685183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m2497685183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	float retVal = _il2cpp_icall_func(___min0, ___max1);
	return retVal;
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1461339684 (RangeAttribute_t2570355765 * __this, float ___min0, float ___max1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2731283770 (RangeInt_t3767345128 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m2731283770_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RangeInt_t3767345128 * _thisAdjusted = reinterpret_cast<RangeInt_t3767345128 *>(__this + 1);
	return RangeInt_get_end_m2731283770(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3929653519 (Ray_t1448970001 * __this, Vector3_t329709361  ___origin0, Vector3_t329709361  ___direction1, const RuntimeMethod* method)
{
	{
		Vector3_t329709361  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t329709361  L_1 = Vector3_get_normalized_m2341245559((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m3929653519_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___origin0, Vector3_t329709361  ___direction1, const RuntimeMethod* method)
{
	Ray_t1448970001 * _thisAdjusted = reinterpret_cast<Ray_t1448970001 *>(__this + 1);
	Ray__ctor_m3929653519(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t329709361  Ray_get_origin_m3562529566 (Ray_t1448970001 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t329709361  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t329709361  Ray_get_origin_m3562529566_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1448970001 * _thisAdjusted = reinterpret_cast<Ray_t1448970001 *>(__this + 1);
	return Ray_get_origin_m3562529566(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t329709361  Ray_get_direction_m777501070 (Ray_t1448970001 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t329709361  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t329709361  Ray_get_direction_m777501070_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1448970001 * _thisAdjusted = reinterpret_cast<Ray_t1448970001 *>(__this + 1);
	return Ray_get_direction_m777501070(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t329709361  Ray_GetPoint_m635666389 (Ray_t1448970001 * __this, float ___distance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_GetPoint_m635666389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Origin_0();
		Vector3_t329709361  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_3 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = Vector3_op_Addition_m2206773101(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t329709361  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t329709361  Ray_GetPoint_m635666389_AdjustorThunk (RuntimeObject * __this, float ___distance0, const RuntimeMethod* method)
{
	Ray_t1448970001 * _thisAdjusted = reinterpret_cast<Ray_t1448970001 *>(__this + 1);
	return Ray_GetPoint_m635666389(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m1475619137 (Ray_t1448970001 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1475619137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t329709361  L_1 = __this->get_m_Origin_0();
		Vector3_t329709361  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		Vector3_t329709361  L_5 = __this->get_m_Direction_1();
		Vector3_t329709361  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral1413257799, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m1475619137_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1448970001 * _thisAdjusted = reinterpret_cast<Ray_t1448970001 *>(__this + 1);
	return Ray_ToString_m1475619137(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m4143151633 (Rect_t1344834245 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m4143151633_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect__ctor_m4143151633(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m2491470360 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m2491470360_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_x_m2491470360(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m2837260125 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m2837260125_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_x_m2837260125(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3128128304 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m3128128304_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_y_m3128128304(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m840657376 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m840657376_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_y_m840657376(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t3057062568  Rect_get_position_m205947143 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t3057062568  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1371796108((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3057062568  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3057062568  Rect_get_position_m205947143_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_position_m205947143(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t3057062568  Rect_get_center_m3649125928 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m2491470360(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m3128128304(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t3057062568  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1371796108((&L_4), ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t3057062568  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t3057062568  Rect_get_center_m3649125928_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_center_m3649125928(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t3057062568  Rect_get_min_m1464947635 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m2002404286(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m3125052285(__this, /*hidden argument*/NULL);
		Vector2_t3057062568  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1371796108((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3057062568  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3057062568  Rect_get_min_m1464947635_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_min_m1464947635(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t3057062568  Rect_get_max_m2236455262 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m2060108737(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m3717006025(__this, /*hidden argument*/NULL);
		Vector2_t3057062568  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1371796108((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3057062568  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3057062568  Rect_get_max_m2236455262_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_max_m2236455262(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1015428014 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m1015428014_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_width_m1015428014(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m642665222 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m642665222_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_width_m642665222(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m4024308933 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m4024308933_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_height_m4024308933(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3801339137 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3801339137_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_height_m3801339137(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t3057062568  Rect_get_size_m1943186915 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t3057062568  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1371796108((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3057062568  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3057062568  Rect_get_size_m1943186915_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_size_m1943186915(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m2002404286 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m2002404286_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_xMin_m2002404286(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2530956691 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m2060108737(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m2530956691_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_xMin_m2530956691(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m3125052285 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m3125052285_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_yMin_m3125052285(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m2705338653 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m3717006025(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m2705338653_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_yMin_m2705338653(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2060108737 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m2060108737_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_xMax_m2060108737(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1652106490 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m1652106490_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_xMax_m1652106490(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m3717006025 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m3717006025_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_get_yMax_m3717006025(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m738832795 (Rect_t1344834245 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m738832795_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	Rect_set_yMax_m738832795(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3306500271 (Rect_t1344834245 * __this, Vector2_t3057062568  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m2002404286(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m2060108737(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m3125052285(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m3717006025(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m3306500271_AdjustorThunk (RuntimeObject * __this, Vector2_t3057062568  ___point0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_Contains_m3306500271(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3476213010 (Rect_t1344834245 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m2002404286(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2060108737(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m3125052285(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m3717006025(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m3476213010_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_Contains_m3476213010(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t1344834245  Rect_OrderMinMax_m3781845787 (RuntimeObject * __this /* static, unused */, Rect_t1344834245  ___rect0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t1344834245  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m2002404286((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m2060108737((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m2002404286((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m2060108737((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m2530956691((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m1652106490((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m3125052285((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m3717006025((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m3125052285((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m3717006025((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m2705338653((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m738832795((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t1344834245  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t1344834245  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m1139021315 (Rect_t1344834245 * __this, Rect_t1344834245  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m2060108737((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m2002404286(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m2002404286((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m2060108737(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m3717006025((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m3125052285(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m3125052285((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m3717006025(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m1139021315_AdjustorThunk (RuntimeObject * __this, Rect_t1344834245  ___other0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_Overlaps_m1139021315(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4001495534 (Rect_t1344834245 * __this, Rect_t1344834245  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t1344834245 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t1344834245  L_1 = V_0;
		Rect_t1344834245  L_2 = Rect_OrderMinMax_m3781845787(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t1344834245  L_3 = ___other0;
		Rect_t1344834245  L_4 = Rect_OrderMinMax_m3781845787(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t1344834245  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m1139021315((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m4001495534_AdjustorThunk (RuntimeObject * __this, Rect_t1344834245  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_Overlaps_m4001495534(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m1463733727 (RuntimeObject * __this /* static, unused */, Rect_t1344834245  ___lhs0, Rect_t1344834245  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Rect_t1344834245  L_0 = ___lhs0;
		Rect_t1344834245  L_1 = ___rhs1;
		bool L_2 = Rect_op_Equality_m2563054195(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2563054195 (RuntimeObject * __this /* static, unused */, Rect_t1344834245  ___lhs0, Rect_t1344834245  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m2491470360((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m2491470360((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m3128128304((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m3128128304((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m1015428014((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1015428014((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m4024308933((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m4024308933((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m2627567217 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m2491470360(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3104521605((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m1015428014(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3104521605((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m3128128304(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3104521605((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m4024308933(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3104521605((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m2627567217_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_GetHashCode_m2627567217(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m1339189050 (Rect_t1344834245 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m1339189050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t1344834245  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Rect_t1344834245_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Rect_t1344834245 *)((Rect_t1344834245 *)UnBox(L_1, Rect_t1344834245_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m2491470360(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m2491470360((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m54068117((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m3128128304(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m3128128304((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m54068117((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m1015428014(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m1015428014((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m54068117((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m4024308933(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m4024308933((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m54068117((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m1339189050_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_Equals_m1339189050(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m4042206769 (Rect_t1344834245 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m4042206769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m2491470360(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		float L_5 = Rect_get_y_m3128128304(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		float L_9 = Rect_get_width_m1015428014(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		float L_13 = Rect_get_height_m4024308933(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral1740198039, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m4042206769_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t1344834245 * _thisAdjusted = reinterpret_cast<Rect_t1344834245 *>(__this + 1);
	return Rect_ToString_m4042206769(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t83369214_marshal_pinvoke(const RectOffset_t83369214& unmarshaled, RectOffset_t83369214_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t83369214_marshal_pinvoke_back(const RectOffset_t83369214_marshaled_pinvoke& marshaled, RectOffset_t83369214& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t83369214_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t83369214_marshal_pinvoke_cleanup(RectOffset_t83369214_marshaled_pinvoke& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t83369214_marshal_com(const RectOffset_t83369214& unmarshaled, RectOffset_t83369214_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t83369214_marshal_com_back(const RectOffset_t83369214_marshaled_com& marshaled, RectOffset_t83369214& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t83369214_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t83369214_marshal_com_cleanup(RectOffset_t83369214_marshaled_com& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m1935065804 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4183761430(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern "C"  void RectOffset__ctor_m3045166434 (RectOffset_t83369214 * __this, RuntimeObject * ___sourceStyle0, intptr_t ___source1, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		intptr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4183761430 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Init_m4183761430_ftn) (RectOffset_t83369214 *);
	static RectOffset_Init_m4183761430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m4183761430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m456239651 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Cleanup_m456239651_ftn) (RectOffset_t83369214 *);
	static RectOffset_Cleanup_m456239651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m456239651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m2136847356 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_left_m2136847356_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_left_m2136847356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m2136847356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m2440322792 (RectOffset_t83369214 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_left_m2440322792_ftn) (RectOffset_t83369214 *, int32_t);
	static RectOffset_set_left_m2440322792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m2440322792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m241919892 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_right_m241919892_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_right_m241919892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m241919892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1573602077 (RectOffset_t83369214 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_right_m1573602077_ftn) (RectOffset_t83369214 *, int32_t);
	static RectOffset_set_right_m1573602077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m1573602077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3343256683 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_top_m3343256683_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_top_m3343256683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m3343256683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m729351304 (RectOffset_t83369214 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_top_m729351304_ftn) (RectOffset_t83369214 *, int32_t);
	static RectOffset_set_top_m729351304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m729351304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m396615172 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_bottom_m396615172_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_bottom_m396615172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m396615172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m1949314507 (RectOffset_t83369214 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_bottom_m1949314507_ftn) (RectOffset_t83369214 *, int32_t);
	static RectOffset_set_bottom_m1949314507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m1949314507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1735141533 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1735141533_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_horizontal_m1735141533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1735141533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m559753670 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_vertical_m559753670_ftn) (RectOffset_t83369214 *);
	static RectOffset_get_vertical_m559753670_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m559753670_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m1178082265 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m456239651(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern "C"  String_t* RectOffset_ToString_m767491965 (RectOffset_t83369214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m767491965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m2136847356(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m241919892(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m3343256683(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m396615172(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral324546219, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t1344834245  RectTransform_get_rect_m4288011661 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1344834245  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m1193447114(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t1344834245  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t1344834245  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1193447114 (RectTransform_t15861704 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1193447114_ftn) (RectTransform_t15861704 *, Rect_t1344834245 *);
	static RectTransform_INTERNAL_get_rect_m1193447114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1193447114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t3057062568  RectTransform_get_anchorMin_m3173730716 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m2850584098(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m2066112249 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m854179797(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m2850584098 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m2850584098_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_get_anchorMin_m2850584098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m2850584098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m854179797 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m854179797_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_set_anchorMin_m854179797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m854179797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t3057062568  RectTransform_get_anchorMax_m1145375430 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m429367384(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m3662733738 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m728187047(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m429367384 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m429367384_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_get_anchorMax_m429367384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m429367384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m728187047 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m728187047_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_set_anchorMax_m728187047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m728187047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t3057062568  RectTransform_get_anchoredPosition_m1623341961 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m3509514529(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2221025746 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m245688481(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3509514529 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m3509514529_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m3509514529_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m3509514529_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m245688481 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m245688481_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m245688481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m245688481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t3057062568  RectTransform_get_sizeDelta_m3670515036 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m1138808073(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2990296297 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m2182524852(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m1138808073 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m1138808073_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_get_sizeDelta_m1138808073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m1138808073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m2182524852 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m2182524852_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_set_sizeDelta_m2182524852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m2182524852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t3057062568  RectTransform_get_pivot_m2251648063 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m2489252632(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m1929838473 (RectTransform_t15861704 * __this, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m3372425696(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m2489252632 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m2489252632_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_get_pivot_m2489252632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m2489252632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m3372425696 (RectTransform_t15861704 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m3372425696_ftn) (RectTransform_t15861704 *, Vector2_t3057062568 *);
	static RectTransform_INTERNAL_set_pivot_m3372425696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m3372425696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1630831955 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t3527255582 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1630831955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t3527255582 * V_0 = NULL;
	ReapplyDrivenProperties_t3527255582 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t3527255582 * L_0 = ((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t3527255582 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t3527255582 * L_2 = V_1;
		ReapplyDrivenProperties_t3527255582 * L_3 = ___value0;
		Delegate_t1563516729 * L_4 = Delegate_Combine_m645997588(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t3527255582 * L_5 = V_0;
		ReapplyDrivenProperties_t3527255582 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t3527255582 *>((((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t3527255582 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t3527255582_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t3527255582 * L_7 = V_0;
		ReapplyDrivenProperties_t3527255582 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t3527255582 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t3527255582 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m2621577569 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t3527255582 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m2621577569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t3527255582 * V_0 = NULL;
	ReapplyDrivenProperties_t3527255582 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t3527255582 * L_0 = ((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t3527255582 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t3527255582 * L_2 = V_1;
		ReapplyDrivenProperties_t3527255582 * L_3 = ___value0;
		Delegate_t1563516729 * L_4 = Delegate_Remove_m2405839985(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t3527255582 * L_5 = V_0;
		ReapplyDrivenProperties_t3527255582 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t3527255582 *>((((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t3527255582 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t3527255582_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t3527255582 * L_7 = V_0;
		ReapplyDrivenProperties_t3527255582 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t3527255582 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t3527255582 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m1998353017 (RuntimeObject * __this /* static, unused */, RectTransform_t15861704 * ___driven0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m1998353017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t3527255582 * L_0 = ((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t3527255582 * L_1 = ((RectTransform_t15861704_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t15861704_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		RectTransform_t15861704 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3181565260(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m4198635943 (RectTransform_t15861704 * __this, Vector3U5BU5D_t974944492* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m4198635943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t974944492* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t974944492* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, _stringLiteral3056984367, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t1344834245  L_2 = RectTransform_get_rect_m4288011661(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m2491470360((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m3128128304((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m2060108737((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m3717006025((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t974944492* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t329709361  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m3984700005((&L_10), L_8, L_9, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t329709361 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_10;
		Vector3U5BU5D_t974944492* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t329709361  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3984700005((&L_14), L_12, L_13, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t329709361 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_14;
		Vector3U5BU5D_t974944492* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t329709361  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m3984700005((&L_18), L_16, L_17, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t329709361 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_18;
		Vector3U5BU5D_t974944492* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t329709361  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m3984700005((&L_22), L_20, L_21, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t329709361 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m2569558680 (RectTransform_t15861704 * __this, Vector3U5BU5D_t974944492* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m2569558680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t532597831 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t974944492* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t974944492* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, _stringLiteral2324639295, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t974944492* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m4198635943(__this, L_2, /*hidden argument*/NULL);
		Transform_t532597831 * L_3 = Component_get_transform_m2645782843(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t974944492* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t532597831 * L_6 = V_0;
		Vector3U5BU5D_t974944492* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t329709361  L_9 = Transform_TransformPoint_m642078408(L_6, (*(Vector3_t329709361 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		*(Vector3_t329709361 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m909240712 (RectTransform_t15861704 * __this, int32_t ___edge0, float ___inset1, float ___size2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t3057062568  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3057062568  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t3057062568  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t3057062568  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t3057062568  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t3057062568 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t3057062568 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t3057062568 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t3057062568  L_5 = RectTransform_get_anchorMin_m3173730716(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m1285091505((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t3057062568  L_8 = V_3;
		RectTransform_set_anchorMin_m2066112249(__this, L_8, /*hidden argument*/NULL);
		Vector2_t3057062568  L_9 = RectTransform_get_anchorMax_m1145375430(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m1285091505((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t3057062568  L_12 = V_3;
		RectTransform_set_anchorMax_m3662733738(__this, L_12, /*hidden argument*/NULL);
		Vector2_t3057062568  L_13 = RectTransform_get_sizeDelta_m3670515036(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m1285091505((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t3057062568  L_16 = V_4;
		RectTransform_set_sizeDelta_m2990296297(__this, L_16, /*hidden argument*/NULL);
		Vector2_t3057062568  L_17 = RectTransform_get_anchoredPosition_m1623341961(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t3057062568  L_22 = RectTransform_get_pivot_m2251648063(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2598596005((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t3057062568  L_27 = RectTransform_get_pivot_m2251648063(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2598596005((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m1285091505(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t3057062568  L_30 = V_5;
		RectTransform_set_anchoredPosition_m2221025746(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m2794938979 (RectTransform_t15861704 * __this, int32_t ___axis0, float ___size1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t3057062568  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t3057062568  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3057062568  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t3057062568  L_1 = RectTransform_get_sizeDelta_m3670515036(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t3057062568  L_4 = RectTransform_GetParentSize_m1866663977(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2598596005((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t3057062568  L_7 = RectTransform_get_anchorMax_m1145375430(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2598596005((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t3057062568  L_10 = RectTransform_get_anchorMin_m3173730716(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2598596005((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m1285091505((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t3057062568  L_13 = V_1;
		RectTransform_set_sizeDelta_m2990296297(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t3057062568  RectTransform_GetParentSize_m1866663977 (RectTransform_t15861704 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m1866663977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t15861704 * V_0 = NULL;
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t1344834245  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t532597831 * L_0 = Transform_get_parent_m1093437403(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t15861704 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t15861704_il2cpp_TypeInfo_var));
		RectTransform_t15861704 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m1890640795(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3057062568_il2cpp_TypeInfo_var);
		Vector2_t3057062568  L_3 = Vector2_get_zero_m111055298(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t15861704 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t1344834245  L_5 = RectTransform_get_rect_m4288011661(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t3057062568  L_6 = Rect_get_size_m1943186915((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t3057062568  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m2466375476 (ReapplyDrivenProperties_t3527255582 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3181565260 (ReapplyDrivenProperties_t3527255582 * __this, RectTransform_t15861704 * ___driven0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3181565260((ReapplyDrivenProperties_t3527255582 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RectTransform_t15861704 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t15861704 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ReapplyDrivenProperties_BeginInvoke_m1594928305 (ReapplyDrivenProperties_t3527255582 * __this, RectTransform_t15861704 * ___driven0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m132819072 (ReapplyDrivenProperties_t3527255582 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t2712136762 * Renderer_get_material_m620483634 (Renderer_t265431926 * __this, const RuntimeMethod* method)
{
	typedef Material_t2712136762 * (*Renderer_get_material_m620483634_ftn) (Renderer_t265431926 *);
	static Renderer_get_material_m620483634_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m620483634_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	Material_t2712136762 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m873316028 (Renderer_t265431926 * __this, MaterialPropertyBlock_t3417007309 * ___properties0, const RuntimeMethod* method)
{
	typedef void (*Renderer_SetPropertyBlock_m873316028_ftn) (Renderer_t265431926 *, MaterialPropertyBlock_t3417007309 *);
	static Renderer_SetPropertyBlock_m873316028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetPropertyBlock_m873316028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___properties0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m4137932513 (Renderer_t265431926 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m4137932513_ftn) (Renderer_t265431926 *);
	static Renderer_get_sortingLayerID_m4137932513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m4137932513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m2851771941 (Renderer_t265431926 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m2851771941_ftn) (Renderer_t265431926 *);
	static Renderer_get_sortingOrder_m2851771941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m2851771941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m125783349 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer__ctor_m125783349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		__this->set_m_Ptr_0((intptr_t)(0));
		CommandBuffer_InitBuffer_m1369565639(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Finalize()
extern "C"  void CommandBuffer_Finalize_m1770844256 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		CommandBuffer_Dispose_m1676858108(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000d;
	}

FINALLY_000d:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(13)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(13)
	{
		IL2CPP_JUMP_TBL(0x14, IL_0014)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern "C"  void CommandBuffer_Dispose_m3338956772 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method)
{
	{
		CommandBuffer_Dispose_m1676858108(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1666552611(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m1676858108 (CommandBuffer_t2673047760 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_Dispose_m1676858108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CommandBuffer_ReleaseBuffer_m1352738611(__this, /*hidden argument*/NULL);
		__this->set_m_Ptr_0((intptr_t)(0));
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m1369565639 (RuntimeObject * __this /* static, unused */, CommandBuffer_t2673047760 * ___buf0, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_InitBuffer_m1369565639_ftn) (CommandBuffer_t2673047760 *);
	static CommandBuffer_InitBuffer_m1369565639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_InitBuffer_m1369565639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(___buf0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m1352738611 (CommandBuffer_t2673047760 * __this, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_ReleaseBuffer_m1352738611_ftn) (CommandBuffer_t2673047760 *);
	static CommandBuffer_ReleaseBuffer_m1352738611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_ReleaseBuffer_m1352738611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m3942920201 (CommandBuffer_t2673047760 * __this, Texture_t2838694469 * ___source0, RenderTargetIdentifier_t3642434912  ___dest1, Material_t2712136762 * ___mat2, const RuntimeMethod* method)
{
	{
		Texture_t2838694469 * L_0 = ___source0;
		Material_t2712136762 * L_1 = ___mat2;
		Vector2_t3057062568  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1371796108((&L_2), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_t3057062568  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m1371796108((&L_3), (0.0f), (0.0f), /*hidden argument*/NULL);
		CommandBuffer_Blit_Texture_m4259355444(__this, L_0, (&___dest1), L_1, (-1), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CommandBuffer_Blit_Texture_m4259355444 (CommandBuffer_t2673047760 * __this, Texture_t2838694469 * ___source0, RenderTargetIdentifier_t3642434912 * ___dest1, Material_t2712136762 * ___mat2, int32_t ___pass3, Vector2_t3057062568  ___scale4, Vector2_t3057062568  ___offset5, const RuntimeMethod* method)
{
	{
		Texture_t2838694469 * L_0 = ___source0;
		RenderTargetIdentifier_t3642434912 * L_1 = ___dest1;
		Material_t2712136762 * L_2 = ___mat2;
		int32_t L_3 = ___pass3;
		CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (&___scale4), (&___offset5), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527 (RuntimeObject * __this /* static, unused */, CommandBuffer_t2673047760 * ___self0, Texture_t2838694469 * ___source1, RenderTargetIdentifier_t3642434912 * ___dest2, Material_t2712136762 * ___mat3, int32_t ___pass4, Vector2_t3057062568 * ___scale5, Vector2_t3057062568 * ___offset6, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527_ftn) (CommandBuffer_t2673047760 *, Texture_t2838694469 *, RenderTargetIdentifier_t3642434912 *, Material_t2712136762 *, int32_t, Vector2_t3057062568 *, Vector2_t3057062568 *);
	static CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_INTERNAL_CALL_Blit_Texture_m2929934527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___source1, ___dest2, ___mat3, ___pass4, ___scale5, ___offset6);
}
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m1757717066 (RenderTargetIdentifier_t3642434912 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier__ctor_m1757717066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		__this->set_m_Type_0(L_0);
		__this->set_m_NameID_1((-1));
		__this->set_m_InstanceID_2(0);
		__this->set_m_BufferPointer_3((intptr_t)(0));
		__this->set_m_MipLevel_4(0);
		__this->set_m_CubeFace_5((-1));
		__this->set_m_DepthSlice_6(0);
		return;
	}
}
extern "C"  void RenderTargetIdentifier__ctor_m1757717066_AdjustorThunk (RuntimeObject * __this, int32_t ___type0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t3642434912 *>(__this + 1);
	RenderTargetIdentifier__ctor_m1757717066(_thisAdjusted, ___type0, method);
}
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t3642434912  RenderTargetIdentifier_op_Implicit_m2181182592 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___type0;
		RenderTargetIdentifier_t3642434912  L_1;
		memset(&L_1, 0, sizeof(L_1));
		RenderTargetIdentifier__ctor_m1757717066((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		RenderTargetIdentifier_t3642434912  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m2533785366 (RenderTargetIdentifier_t3642434912 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_ToString_m2533785366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Type_0();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(BuiltinRenderTextureType_t3831699340_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		int32_t L_5 = __this->get_m_NameID_1();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		int32_t L_9 = __this->get_m_InstanceID_2();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral2767891638, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* RenderTargetIdentifier_ToString_m2533785366_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t3642434912 *>(__this + 1);
	return RenderTargetIdentifier_ToString_m2533785366(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1270711389 (RenderTargetIdentifier_t3642434912 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_GetHashCode_m1270711389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t* L_0 = __this->get_address_of_m_Type_0();
		RuntimeObject * L_1 = Box(BuiltinRenderTextureType_t3831699340_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		*L_0 = *(int32_t*)UnBox(L_1);
		int32_t* L_3 = __this->get_address_of_m_NameID_1();
		int32_t L_4 = Int32_GetHashCode_m3782351173(L_3, /*hidden argument*/NULL);
		int32_t* L_5 = __this->get_address_of_m_InstanceID_2();
		int32_t L_6 = Int32_GetHashCode_m3782351173(L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)23)))+(int32_t)L_4))*(int32_t)((int32_t)23)))+(int32_t)L_6));
		goto IL_0042;
	}

IL_0042:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1270711389_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t3642434912 *>(__this + 1);
	return RenderTargetIdentifier_GetHashCode_m1270711389(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  bool RenderTargetIdentifier_Equals_m1382118731 (RenderTargetIdentifier_t3642434912 * __this, RenderTargetIdentifier_t3642434912  ___rhs0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_0();
		int32_t L_1 = (&___rhs0)->get_m_Type_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_2 = __this->get_m_NameID_1();
		int32_t L_3 = (&___rhs0)->get_m_NameID_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_4 = __this->get_m_InstanceID_2();
		int32_t L_5 = (&___rhs0)->get_m_InstanceID_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0083;
		}
	}
	{
		intptr_t L_6 = __this->get_m_BufferPointer_3();
		intptr_t L_7 = (&___rhs0)->get_m_BufferPointer_3();
		bool L_8 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_9 = __this->get_m_MipLevel_4();
		int32_t L_10 = (&___rhs0)->get_m_MipLevel_4();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_11 = __this->get_m_CubeFace_5();
		int32_t L_12 = (&___rhs0)->get_m_CubeFace_5();
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_13 = __this->get_m_DepthSlice_6();
		int32_t L_14 = (&___rhs0)->get_m_DepthSlice_6();
		G_B8_0 = ((((int32_t)L_13) == ((int32_t)L_14))? 1 : 0);
		goto IL_0084;
	}

IL_0083:
	{
		G_B8_0 = 0;
	}

IL_0084:
	{
		V_0 = (bool)G_B8_0;
		goto IL_008a;
	}

IL_008a:
	{
		bool L_15 = V_0;
		return L_15;
	}
}
extern "C"  bool RenderTargetIdentifier_Equals_m1382118731_AdjustorThunk (RuntimeObject * __this, RenderTargetIdentifier_t3642434912  ___rhs0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t3642434912 *>(__this + 1);
	return RenderTargetIdentifier_Equals_m1382118731(_thisAdjusted, ___rhs0, method);
}
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2199170207 (RenderTargetIdentifier_t3642434912 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_Equals_m2199170207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RenderTargetIdentifier_t3642434912  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, RenderTargetIdentifier_t3642434912_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0027;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___obj0;
		V_1 = ((*(RenderTargetIdentifier_t3642434912 *)((RenderTargetIdentifier_t3642434912 *)UnBox(L_1, RenderTargetIdentifier_t3642434912_il2cpp_TypeInfo_var))));
		RenderTargetIdentifier_t3642434912  L_2 = V_1;
		bool L_3 = RenderTargetIdentifier_Equals_m1382118731(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0027;
	}

IL_0027:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool RenderTargetIdentifier_Equals_m2199170207_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t3642434912 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t3642434912 *>(__this + 1);
	return RenderTargetIdentifier_Equals_m2199170207(_thisAdjusted, ___obj0, method);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void SphericalHarmonicsL2_set_Item_m967009223 (SphericalHarmonicsL2_t298540216 * __this, int32_t ___rgb0, int32_t ___coefficient1, float ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SphericalHarmonicsL2_set_Item_m967009223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___rgb0;
		int32_t L_1 = ___coefficient1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_007f;
			}
			case 1:
			{
				goto IL_008b;
			}
			case 2:
			{
				goto IL_0097;
			}
			case 3:
			{
				goto IL_00a3;
			}
			case 4:
			{
				goto IL_00af;
			}
			case 5:
			{
				goto IL_00bb;
			}
			case 6:
			{
				goto IL_00c7;
			}
			case 7:
			{
				goto IL_00d3;
			}
			case 8:
			{
				goto IL_00df;
			}
			case 9:
			{
				goto IL_00eb;
			}
			case 10:
			{
				goto IL_00f7;
			}
			case 11:
			{
				goto IL_0103;
			}
			case 12:
			{
				goto IL_010f;
			}
			case 13:
			{
				goto IL_011b;
			}
			case 14:
			{
				goto IL_0127;
			}
			case 15:
			{
				goto IL_0133;
			}
			case 16:
			{
				goto IL_013f;
			}
			case 17:
			{
				goto IL_014b;
			}
			case 18:
			{
				goto IL_0157;
			}
			case 19:
			{
				goto IL_0163;
			}
			case 20:
			{
				goto IL_016f;
			}
			case 21:
			{
				goto IL_017b;
			}
			case 22:
			{
				goto IL_0187;
			}
			case 23:
			{
				goto IL_0193;
			}
			case 24:
			{
				goto IL_019f;
			}
			case 25:
			{
				goto IL_01ab;
			}
			case 26:
			{
				goto IL_01b7;
			}
		}
	}
	{
		goto IL_01c3;
	}

IL_007f:
	{
		float L_3 = ___value2;
		__this->set_shr0_0(L_3);
		goto IL_01ce;
	}

IL_008b:
	{
		float L_4 = ___value2;
		__this->set_shr1_1(L_4);
		goto IL_01ce;
	}

IL_0097:
	{
		float L_5 = ___value2;
		__this->set_shr2_2(L_5);
		goto IL_01ce;
	}

IL_00a3:
	{
		float L_6 = ___value2;
		__this->set_shr3_3(L_6);
		goto IL_01ce;
	}

IL_00af:
	{
		float L_7 = ___value2;
		__this->set_shr4_4(L_7);
		goto IL_01ce;
	}

IL_00bb:
	{
		float L_8 = ___value2;
		__this->set_shr5_5(L_8);
		goto IL_01ce;
	}

IL_00c7:
	{
		float L_9 = ___value2;
		__this->set_shr6_6(L_9);
		goto IL_01ce;
	}

IL_00d3:
	{
		float L_10 = ___value2;
		__this->set_shr7_7(L_10);
		goto IL_01ce;
	}

IL_00df:
	{
		float L_11 = ___value2;
		__this->set_shr8_8(L_11);
		goto IL_01ce;
	}

IL_00eb:
	{
		float L_12 = ___value2;
		__this->set_shg0_9(L_12);
		goto IL_01ce;
	}

IL_00f7:
	{
		float L_13 = ___value2;
		__this->set_shg1_10(L_13);
		goto IL_01ce;
	}

IL_0103:
	{
		float L_14 = ___value2;
		__this->set_shg2_11(L_14);
		goto IL_01ce;
	}

IL_010f:
	{
		float L_15 = ___value2;
		__this->set_shg3_12(L_15);
		goto IL_01ce;
	}

IL_011b:
	{
		float L_16 = ___value2;
		__this->set_shg4_13(L_16);
		goto IL_01ce;
	}

IL_0127:
	{
		float L_17 = ___value2;
		__this->set_shg5_14(L_17);
		goto IL_01ce;
	}

IL_0133:
	{
		float L_18 = ___value2;
		__this->set_shg6_15(L_18);
		goto IL_01ce;
	}

IL_013f:
	{
		float L_19 = ___value2;
		__this->set_shg7_16(L_19);
		goto IL_01ce;
	}

IL_014b:
	{
		float L_20 = ___value2;
		__this->set_shg8_17(L_20);
		goto IL_01ce;
	}

IL_0157:
	{
		float L_21 = ___value2;
		__this->set_shb0_18(L_21);
		goto IL_01ce;
	}

IL_0163:
	{
		float L_22 = ___value2;
		__this->set_shb1_19(L_22);
		goto IL_01ce;
	}

IL_016f:
	{
		float L_23 = ___value2;
		__this->set_shb2_20(L_23);
		goto IL_01ce;
	}

IL_017b:
	{
		float L_24 = ___value2;
		__this->set_shb3_21(L_24);
		goto IL_01ce;
	}

IL_0187:
	{
		float L_25 = ___value2;
		__this->set_shb4_22(L_25);
		goto IL_01ce;
	}

IL_0193:
	{
		float L_26 = ___value2;
		__this->set_shb5_23(L_26);
		goto IL_01ce;
	}

IL_019f:
	{
		float L_27 = ___value2;
		__this->set_shb6_24(L_27);
		goto IL_01ce;
	}

IL_01ab:
	{
		float L_28 = ___value2;
		__this->set_shb7_25(L_28);
		goto IL_01ce;
	}

IL_01b7:
	{
		float L_29 = ___value2;
		__this->set_shb8_26(L_29);
		goto IL_01ce;
	}

IL_01c3:
	{
		IndexOutOfRangeException_t3921978895 * L_30 = (IndexOutOfRangeException_t3921978895 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3132318046(L_30, _stringLiteral3191453988, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_01ce:
	{
		return;
	}
}
extern "C"  void SphericalHarmonicsL2_set_Item_m967009223_AdjustorThunk (RuntimeObject * __this, int32_t ___rgb0, int32_t ___coefficient1, float ___value2, const RuntimeMethod* method)
{
	SphericalHarmonicsL2_t298540216 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t298540216 *>(__this + 1);
	SphericalHarmonicsL2_set_Item_m967009223(_thisAdjusted, ___rgb0, ___coefficient1, ___value2, method);
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2623158889 (SphericalHarmonicsL2_t298540216 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = __this->get_address_of_shr0_0();
		int32_t L_2 = Single_GetHashCode_m3104521605(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = __this->get_address_of_shr1_1();
		int32_t L_5 = Single_GetHashCode_m3104521605(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = __this->get_address_of_shr2_2();
		int32_t L_8 = Single_GetHashCode_m3104521605(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = __this->get_address_of_shr3_3();
		int32_t L_11 = Single_GetHashCode_m3104521605(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = __this->get_address_of_shr4_4();
		int32_t L_14 = Single_GetHashCode_m3104521605(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_shr5_5();
		int32_t L_17 = Single_GetHashCode_m3104521605(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = __this->get_address_of_shr6_6();
		int32_t L_20 = Single_GetHashCode_m3104521605(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = __this->get_address_of_shr7_7();
		int32_t L_23 = Single_GetHashCode_m3104521605(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = __this->get_address_of_shr8_8();
		int32_t L_26 = Single_GetHashCode_m3104521605(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = __this->get_address_of_shg0_9();
		int32_t L_29 = Single_GetHashCode_m3104521605(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = __this->get_address_of_shg1_10();
		int32_t L_32 = Single_GetHashCode_m3104521605(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = __this->get_address_of_shg2_11();
		int32_t L_35 = Single_GetHashCode_m3104521605(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = __this->get_address_of_shg3_12();
		int32_t L_38 = Single_GetHashCode_m3104521605(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = __this->get_address_of_shg4_13();
		int32_t L_41 = Single_GetHashCode_m3104521605(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = __this->get_address_of_shg5_14();
		int32_t L_44 = Single_GetHashCode_m3104521605(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = __this->get_address_of_shg6_15();
		int32_t L_47 = Single_GetHashCode_m3104521605(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = __this->get_address_of_shg7_16();
		int32_t L_50 = Single_GetHashCode_m3104521605(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = __this->get_address_of_shg8_17();
		int32_t L_53 = Single_GetHashCode_m3104521605(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = __this->get_address_of_shb0_18();
		int32_t L_56 = Single_GetHashCode_m3104521605(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = __this->get_address_of_shb1_19();
		int32_t L_59 = Single_GetHashCode_m3104521605(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = __this->get_address_of_shb2_20();
		int32_t L_62 = Single_GetHashCode_m3104521605(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = __this->get_address_of_shb3_21();
		int32_t L_65 = Single_GetHashCode_m3104521605(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = __this->get_address_of_shb4_22();
		int32_t L_68 = Single_GetHashCode_m3104521605(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = __this->get_address_of_shb5_23();
		int32_t L_71 = Single_GetHashCode_m3104521605(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = __this->get_address_of_shb6_24();
		int32_t L_74 = Single_GetHashCode_m3104521605(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = __this->get_address_of_shb7_25();
		int32_t L_77 = Single_GetHashCode_m3104521605(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = __this->get_address_of_shb8_26();
		int32_t L_80 = Single_GetHashCode_m3104521605(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		V_1 = L_81;
		goto IL_0279;
	}

IL_0279:
	{
		int32_t L_82 = V_1;
		return L_82;
	}
}
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2623158889_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SphericalHarmonicsL2_t298540216 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t298540216 *>(__this + 1);
	return SphericalHarmonicsL2_GetHashCode_m2623158889(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern "C"  bool SphericalHarmonicsL2_Equals_m391510961 (SphericalHarmonicsL2_t298540216 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SphericalHarmonicsL2_Equals_m391510961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	SphericalHarmonicsL2_t298540216  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, SphericalHarmonicsL2_t298540216_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(SphericalHarmonicsL2_t298540216 *)((SphericalHarmonicsL2_t298540216 *)UnBox(L_1, SphericalHarmonicsL2_t298540216_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t298540216  L_2 = V_1;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m3387909627(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t298540216 *)__this), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool SphericalHarmonicsL2_Equals_m391510961_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	SphericalHarmonicsL2_t298540216 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t298540216 *>(__this + 1);
	return SphericalHarmonicsL2_Equals_m391510961(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3387909627 (RuntimeObject * __this /* static, unused */, SphericalHarmonicsL2_t298540216  ___lhs0, SphericalHarmonicsL2_t298540216  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B28_0 = 0;
	{
		float L_0 = (&___lhs0)->get_shr0_0();
		float L_1 = (&___rhs1)->get_shr0_0();
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0201;
		}
	}
	{
		float L_2 = (&___lhs0)->get_shr1_1();
		float L_3 = (&___rhs1)->get_shr1_1();
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0201;
		}
	}
	{
		float L_4 = (&___lhs0)->get_shr2_2();
		float L_5 = (&___rhs1)->get_shr2_2();
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0201;
		}
	}
	{
		float L_6 = (&___lhs0)->get_shr3_3();
		float L_7 = (&___rhs1)->get_shr3_3();
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0201;
		}
	}
	{
		float L_8 = (&___lhs0)->get_shr4_4();
		float L_9 = (&___rhs1)->get_shr4_4();
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0201;
		}
	}
	{
		float L_10 = (&___lhs0)->get_shr5_5();
		float L_11 = (&___rhs1)->get_shr5_5();
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0201;
		}
	}
	{
		float L_12 = (&___lhs0)->get_shr6_6();
		float L_13 = (&___rhs1)->get_shr6_6();
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0201;
		}
	}
	{
		float L_14 = (&___lhs0)->get_shr7_7();
		float L_15 = (&___rhs1)->get_shr7_7();
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0201;
		}
	}
	{
		float L_16 = (&___lhs0)->get_shr8_8();
		float L_17 = (&___rhs1)->get_shr8_8();
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0201;
		}
	}
	{
		float L_18 = (&___lhs0)->get_shg0_9();
		float L_19 = (&___rhs1)->get_shg0_9();
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0201;
		}
	}
	{
		float L_20 = (&___lhs0)->get_shg1_10();
		float L_21 = (&___rhs1)->get_shg1_10();
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0201;
		}
	}
	{
		float L_22 = (&___lhs0)->get_shg2_11();
		float L_23 = (&___rhs1)->get_shg2_11();
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0201;
		}
	}
	{
		float L_24 = (&___lhs0)->get_shg3_12();
		float L_25 = (&___rhs1)->get_shg3_12();
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0201;
		}
	}
	{
		float L_26 = (&___lhs0)->get_shg4_13();
		float L_27 = (&___rhs1)->get_shg4_13();
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0201;
		}
	}
	{
		float L_28 = (&___lhs0)->get_shg5_14();
		float L_29 = (&___rhs1)->get_shg5_14();
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0201;
		}
	}
	{
		float L_30 = (&___lhs0)->get_shg6_15();
		float L_31 = (&___rhs1)->get_shg6_15();
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0201;
		}
	}
	{
		float L_32 = (&___lhs0)->get_shg7_16();
		float L_33 = (&___rhs1)->get_shg7_16();
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0201;
		}
	}
	{
		float L_34 = (&___lhs0)->get_shg8_17();
		float L_35 = (&___rhs1)->get_shg8_17();
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0201;
		}
	}
	{
		float L_36 = (&___lhs0)->get_shb0_18();
		float L_37 = (&___rhs1)->get_shb0_18();
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0201;
		}
	}
	{
		float L_38 = (&___lhs0)->get_shb1_19();
		float L_39 = (&___rhs1)->get_shb1_19();
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0201;
		}
	}
	{
		float L_40 = (&___lhs0)->get_shb2_20();
		float L_41 = (&___rhs1)->get_shb2_20();
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0201;
		}
	}
	{
		float L_42 = (&___lhs0)->get_shb3_21();
		float L_43 = (&___rhs1)->get_shb3_21();
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0201;
		}
	}
	{
		float L_44 = (&___lhs0)->get_shb4_22();
		float L_45 = (&___rhs1)->get_shb4_22();
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0201;
		}
	}
	{
		float L_46 = (&___lhs0)->get_shb5_23();
		float L_47 = (&___rhs1)->get_shb5_23();
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0201;
		}
	}
	{
		float L_48 = (&___lhs0)->get_shb6_24();
		float L_49 = (&___rhs1)->get_shb6_24();
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0201;
		}
	}
	{
		float L_50 = (&___lhs0)->get_shb7_25();
		float L_51 = (&___rhs1)->get_shb7_25();
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0201;
		}
	}
	{
		float L_52 = (&___lhs0)->get_shb8_26();
		float L_53 = (&___rhs1)->get_shb8_26();
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0202;
	}

IL_0201:
	{
		G_B28_0 = 0;
	}

IL_0202:
	{
		V_0 = (bool)G_B28_0;
		goto IL_0208;
	}

IL_0208:
	{
		bool L_54 = V_0;
		return L_54;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientMode(UnityEngine.Rendering.AmbientMode)
extern "C"  void RenderSettings_set_ambientMode_m1165476808 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderSettings_set_ambientMode_m1165476808_ftn) (int32_t);
	static RenderSettings_set_ambientMode_m1165476808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_ambientMode_m1165476808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_ambientMode(UnityEngine.Rendering.AmbientMode)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  void RenderSettings_set_ambientProbe_m2137014923 (RuntimeObject * __this /* static, unused */, SphericalHarmonicsL2_t298540216  ___value0, const RuntimeMethod* method)
{
	{
		RenderSettings_INTERNAL_set_ambientProbe_m1324021335(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_set_ambientProbe_m1324021335 (RuntimeObject * __this /* static, unused */, SphericalHarmonicsL2_t298540216 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientProbe_m1324021335_ftn) (SphericalHarmonicsL2_t298540216 *);
	static RenderSettings_INTERNAL_set_ambientProbe_m1324021335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientProbe_m1324021335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m566807488 (RuntimeObject * __this /* static, unused */, RenderTexture_t3361574884 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m566807488_ftn) (RenderTexture_t3361574884 *);
	static RenderTexture_Internal_GetWidth_m566807488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m566807488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m4075470559 (RuntimeObject * __this /* static, unused */, RenderTexture_t3361574884 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m4075470559_ftn) (RenderTexture_t3361574884 *);
	static RenderTexture_Internal_GetHeight_m4075470559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m4075470559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m804045951 (RenderTexture_t3361574884 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m566807488(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4196707290 (RenderTexture_t3361574884 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m4075470559(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m1295948690 (RequireComponent_t4152270991 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
extern "C"  void RequireComponent__ctor_m1655823196 (RequireComponent_t4152270991 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		Type_t * L_1 = ___requiredComponent21;
		__this->set_m_Type1_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m2175655275 (Resolution_t1041838152 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Width_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_width_m2175655275_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t1041838152 * _thisAdjusted = reinterpret_cast<Resolution_t1041838152 *>(__this + 1);
	return Resolution_get_width_m2175655275(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m3498799763 (Resolution_t1041838152 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Height_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_height_m3498799763_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t1041838152 * _thisAdjusted = reinterpret_cast<Resolution_t1041838152 *>(__this + 1);
	return Resolution_get_height_m3498799763(_thisAdjusted, method);
}
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m85524303 (Resolution_t1041838152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m85524303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t499004851_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral547391191, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Resolution_ToString_m85524303_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t1041838152 * _thisAdjusted = reinterpret_cast<Resolution_t1041838152 *>(__this + 1);
	return Resolution_ToString_m85524303(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t230212484_marshal_pinvoke(const ResourceRequest_t230212484& unmarshaled, ResourceRequest_t230212484_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
extern "C" void ResourceRequest_t230212484_marshal_pinvoke_back(const ResourceRequest_t230212484_marshaled_pinvoke& marshaled, ResourceRequest_t230212484& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t230212484_marshal_pinvoke_cleanup(ResourceRequest_t230212484_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t230212484_marshal_com(const ResourceRequest_t230212484& unmarshaled, ResourceRequest_t230212484_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
extern "C" void ResourceRequest_t230212484_marshal_com_back(const ResourceRequest_t230212484_marshaled_com& marshaled, ResourceRequest_t230212484& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t230212484_marshal_com_cleanup(ResourceRequest_t230212484_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t1970767703 * Resources_GetBuiltinResource_m118742424 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const RuntimeMethod* method)
{
	typedef Object_t1970767703 * (*Resources_GetBuiltinResource_m118742424_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m118742424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m118742424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	Object_t1970767703 * retVal = _il2cpp_icall_func(___type0, ___path1);
	return retVal;
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m2202351169 (Scene_t548444562 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m2202351169_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t548444562 * _thisAdjusted = reinterpret_cast<Scene_t548444562 *>(__this + 1);
	return Scene_get_handle_m2202351169(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m2236524741 (Scene_t548444562 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m2236524741_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t548444562 * _thisAdjusted = reinterpret_cast<Scene_t548444562 *>(__this + 1);
	return Scene_GetHashCode_m2236524741(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m4035306960 (Scene_t548444562 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m4035306960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t548444562  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Scene_t548444562_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Scene_t548444562 *)((Scene_t548444562 *)UnBox(L_1, Scene_t548444562_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m2202351169(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m2202351169((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m4035306960_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Scene_t548444562 * _thisAdjusted = reinterpret_cast<Scene_t548444562 *>(__this + 1);
	return Scene_Equals_m4035306960(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_Internal_SceneLoaded_m1765525885 (RuntimeObject * __this /* static, unused */, Scene_t548444562  ___scene0, int32_t ___mode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m1765525885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t2777655240 * L_0 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t2777655240 * L_1 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		Scene_t548444562  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m3981208854(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m3981208854_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_SceneUnloaded_m2727365971 (RuntimeObject * __this /* static, unused */, Scene_t548444562  ___scene0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m2727365971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t2691974747 * L_0 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t2691974747 * L_1 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		Scene_t548444562  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m2617017530(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m2617017530_RuntimeMethod_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m3720678395 (RuntimeObject * __this /* static, unused */, Scene_t548444562  ___previousActiveScene0, Scene_t548444562  ___newActiveScene1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m3720678395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t562236193 * L_0 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t562236193 * L_1 = ((SceneManager_t1963070651_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1963070651_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		Scene_t548444562  L_2 = ___previousActiveScene0;
		Scene_t548444562  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1895819670(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1895819670_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t1041838152  Screen_get_currentResolution_m205929278 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Resolution_t1041838152  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t1041838152  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Screen_INTERNAL_get_currentResolution_m1709057242(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Resolution_t1041838152  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Resolution_t1041838152  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)
extern "C"  void Screen_INTERNAL_get_currentResolution_m1709057242 (RuntimeObject * __this /* static, unused */, Resolution_t1041838152 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_INTERNAL_get_currentResolution_m1709057242_ftn) (Resolution_t1041838152 *);
	static Screen_INTERNAL_get_currentResolution_m1709057242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_INTERNAL_get_currentResolution_m1709057242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m537998339 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_width_m537998339_ftn) ();
	static Screen_get_width_m537998339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m537998339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m3303665209 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_height_m3303665209_ftn) ();
	static Screen_get_height_m3303665209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m3303665209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m975933010 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Screen_get_dpi_m975933010_ftn) ();
	static Screen_get_dpi_m975933010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m975933010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke(const ScriptableObject_t2962125979& unmarshaled, ScriptableObject_t2962125979_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke_back(const ScriptableObject_t2962125979_marshaled_pinvoke& marshaled, ScriptableObject_t2962125979& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2962125979_marshal_pinvoke_cleanup(ScriptableObject_t2962125979_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2962125979_marshal_com(const ScriptableObject_t2962125979& unmarshaled, ScriptableObject_t2962125979_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void ScriptableObject_t2962125979_marshal_com_back(const ScriptableObject_t2962125979_marshaled_com& marshaled, ScriptableObject_t2962125979& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2962125979_marshal_com_cleanup(ScriptableObject_t2962125979_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1532608883 (ScriptableObject_t2962125979 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m1532608883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m3676008935(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m3676008935 (RuntimeObject * __this /* static, unused */, ScriptableObject_t2962125979 * ___self0, const RuntimeMethod* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m3676008935_ftn) (ScriptableObject_t2962125979 *);
	static ScriptableObject_Internal_CreateScriptableObject_m3676008935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m3676008935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2962125979 * ScriptableObject_CreateInstance_m1733741624 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	ScriptableObject_t2962125979 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2962125979 * L_1 = ScriptableObject_CreateInstanceFromType_m1618254869(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t2962125979 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2962125979 * ScriptableObject_CreateInstanceFromType_m1618254869 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ScriptableObject_t2962125979 * (*ScriptableObject_CreateInstanceFromType_m1618254869_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m1618254869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m1618254869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	ScriptableObject_t2962125979 * retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m2137585850 (GeneratedByOldBindingsGeneratorAttribute_t1698167202 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m1017941165 (RequiredByNativeCodeAttribute_t917163987 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor(System.String)
extern "C"  void RequiredByNativeCodeAttribute__ctor_m4224717411 (RequiredByNativeCodeAttribute_t917163987 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		RequiredByNativeCodeAttribute_set_Name_m1613284443(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m1613284443 (RequiredByNativeCodeAttribute_t917163987 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Optional(System.Boolean)
extern "C"  void RequiredByNativeCodeAttribute_set_Optional_m3958107600 (RequiredByNativeCodeAttribute_t917163987 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m1481344688 (UsedByNativeCodeAttribute_t3769384580 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m1505746355 (SelectionBaseAttribute_t1772378422 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern "C"  void SendMouseEvents_SetMouseMoved_m3953679151 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m3953679151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m2363379606 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___camera0, Vector3_t329709361  ___mousePosition1, HitInfo_t214434488 * ___hitInfo2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_HitTestLegacyGUI_m2363379606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUILayer_t2694382279 * V_0 = NULL;
	GUIElement_t1520723180 * V_1 = NULL;
	{
		Camera_t989002943 * L_0 = ___camera0;
		NullCheck(L_0);
		GUILayer_t2694382279 * L_1 = Component_GetComponent_TisGUILayer_t2694382279_m835163552(L_0, /*hidden argument*/Component_GetComponent_TisGUILayer_t2694382279_m835163552_RuntimeMethod_var);
		V_0 = L_1;
		GUILayer_t2694382279 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m1890640795(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		GUILayer_t2694382279 * L_4 = V_0;
		Vector3_t329709361  L_5 = ___mousePosition1;
		NullCheck(L_4);
		GUIElement_t1520723180 * L_6 = GUILayer_HitTest_m1709809635(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GUIElement_t1520723180 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m1890640795(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		HitInfo_t214434488 * L_9 = ___hitInfo2;
		GUIElement_t1520723180 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t1318052361 * L_11 = Component_get_gameObject_m2399756717(L_10, /*hidden argument*/NULL);
		L_9->set_target_0(L_11);
		HitInfo_t214434488 * L_12 = ___hitInfo2;
		Camera_t989002943 * L_13 = ___camera0;
		L_12->set_camera_1(L_13);
		goto IL_0051;
	}

IL_0041:
	{
		HitInfo_t214434488 * L_14 = ___hitInfo2;
		L_14->set_target_0((GameObject_t1318052361 *)NULL);
		HitInfo_t214434488 * L_15 = ___hitInfo2;
		L_15->set_camera_1((Camera_t989002943 *)NULL);
	}

IL_0051:
	{
	}

IL_0052:
	{
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern "C"  void SendMouseEvents_DoSendMouseEvents_m2020259991 (RuntimeObject * __this /* static, unused */, int32_t ___skipRTCameras0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m2020259991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	HitInfo_t214434488  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Camera_t989002943 * V_4 = NULL;
	CameraU5BU5D_t3897890150* V_5 = NULL;
	int32_t V_6 = 0;
	Rect_t1344834245  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Ray_t1448970001  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector3_t329709361  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	GameObject_t1318052361 * V_12 = NULL;
	GameObject_t1318052361 * V_13 = NULL;
	int32_t V_14 = 0;
	float G_B19_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_0 = Input_get_mousePosition_m354171928(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m1239575985(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3897890150* L_2 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3897890150* L_3 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002f;
		}
	}

IL_0024:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_m_Cameras_4(((CameraU5BU5D_t3897890150*)SZArrayNew(CameraU5BU5D_t3897890150_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3897890150* L_6 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		Camera_GetAllCameras_m2864360343(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_7 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t214434488  L_9 = V_3;
		*(HitInfo_t214434488 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_12 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_027e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3897890150* L_14 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		V_5 = L_14;
		V_6 = 0;
		goto IL_0272;
	}

IL_0086:
	{
		CameraU5BU5D_t3897890150* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Camera_t989002943 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		Camera_t989002943 * L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_19, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b8;
		}
	}
	{
		Camera_t989002943 * L_22 = V_4;
		NullCheck(L_22);
		RenderTexture_t3361574884 * L_23 = Camera_get_targetTexture_m902197149(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_23, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b8;
		}
	}

IL_00b3:
	{
		goto IL_026c;
	}

IL_00b8:
	{
		Camera_t989002943 * L_25 = V_4;
		NullCheck(L_25);
		Rect_t1344834245  L_26 = Camera_get_pixelRect_m2697375247(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		Vector3_t329709361  L_27 = V_0;
		bool L_28 = Rect_Contains_m3476213010((&V_7), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_026c;
	}

IL_00d3:
	{
		Camera_t989002943 * L_29 = V_4;
		Vector3_t329709361  L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_31 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_31);
		SendMouseEvents_HitTestLegacyGUI_m2363379606(NULL /*static, unused*/, L_29, L_30, ((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		Camera_t989002943 * L_32 = V_4;
		NullCheck(L_32);
		int32_t L_33 = Camera_get_eventMask_m3847497448(L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_026c;
	}

IL_00f7:
	{
		Camera_t989002943 * L_34 = V_4;
		Vector3_t329709361  L_35 = V_0;
		NullCheck(L_34);
		Ray_t1448970001  L_36 = Camera_ScreenPointToRay_m1775774407(L_34, L_35, /*hidden argument*/NULL);
		V_8 = L_36;
		Vector3_t329709361  L_37 = Ray_get_direction_m777501070((&V_8), /*hidden argument*/NULL);
		V_10 = L_37;
		float L_38 = (&V_10)->get_z_3();
		V_9 = L_38;
		float L_39 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		bool L_40 = Mathf_Approximately_m1485428512(NULL /*static, unused*/, (0.0f), L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_012e;
		}
	}
	{
		G_B19_0 = (std::numeric_limits<float>::infinity());
		goto IL_0145;
	}

IL_012e:
	{
		Camera_t989002943 * L_41 = V_4;
		NullCheck(L_41);
		float L_42 = Camera_get_farClipPlane_m3100038649(L_41, /*hidden argument*/NULL);
		Camera_t989002943 * L_43 = V_4;
		NullCheck(L_43);
		float L_44 = Camera_get_nearClipPlane_m2174207718(L_43, /*hidden argument*/NULL);
		float L_45 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_46 = fabsf(((float)((float)((float)((float)L_42-(float)L_44))/(float)L_45)));
		G_B19_0 = L_46;
	}

IL_0145:
	{
		V_11 = G_B19_0;
		Camera_t989002943 * L_47 = V_4;
		Ray_t1448970001  L_48 = V_8;
		float L_49 = V_11;
		Camera_t989002943 * L_50 = V_4;
		NullCheck(L_50);
		int32_t L_51 = Camera_get_cullingMask_m2989971275(L_50, /*hidden argument*/NULL);
		Camera_t989002943 * L_52 = V_4;
		NullCheck(L_52);
		int32_t L_53 = Camera_get_eventMask_m3847497448(L_52, /*hidden argument*/NULL);
		NullCheck(L_47);
		GameObject_t1318052361 * L_54 = Camera_RaycastTry_m413943321(L_47, L_48, L_49, ((int32_t)((int32_t)L_51&(int32_t)L_53)), /*hidden argument*/NULL);
		V_12 = L_54;
		GameObject_t1318052361 * L_55 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_55, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_019b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_57 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_57);
		GameObject_t1318052361 * L_58 = V_12;
		((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_58);
		HitInfoU5BU5D_t1216529257* L_59 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_59);
		Camera_t989002943 * L_60 = V_4;
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_60);
		goto IL_01d9;
	}

IL_019b:
	{
		Camera_t989002943 * L_61 = V_4;
		NullCheck(L_61);
		int32_t L_62 = Camera_get_clearFlags_m1432164003(L_61, /*hidden argument*/NULL);
		if ((((int32_t)L_62) == ((int32_t)1)))
		{
			goto IL_01b5;
		}
	}
	{
		Camera_t989002943 * L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_clearFlags_m1432164003(L_63, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_64) == ((uint32_t)2))))
		{
			goto IL_01d9;
		}
	}

IL_01b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_65 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_65);
		((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t1318052361 *)NULL);
		HitInfoU5BU5D_t1216529257* L_66 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_66);
		((L_66)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t989002943 *)NULL);
	}

IL_01d9:
	{
		Camera_t989002943 * L_67 = V_4;
		Ray_t1448970001  L_68 = V_8;
		float L_69 = V_11;
		Camera_t989002943 * L_70 = V_4;
		NullCheck(L_70);
		int32_t L_71 = Camera_get_cullingMask_m2989971275(L_70, /*hidden argument*/NULL);
		Camera_t989002943 * L_72 = V_4;
		NullCheck(L_72);
		int32_t L_73 = Camera_get_eventMask_m3847497448(L_72, /*hidden argument*/NULL);
		NullCheck(L_67);
		GameObject_t1318052361 * L_74 = Camera_RaycastTry2D_m2680111107(L_67, L_68, L_69, ((int32_t)((int32_t)L_71&(int32_t)L_73)), /*hidden argument*/NULL);
		V_13 = L_74;
		GameObject_t1318052361 * L_75 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_76 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_75, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_022d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_77 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_77);
		GameObject_t1318052361 * L_78 = V_13;
		((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_78);
		HitInfoU5BU5D_t1216529257* L_79 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_79);
		Camera_t989002943 * L_80 = V_4;
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_80);
		goto IL_026b;
	}

IL_022d:
	{
		Camera_t989002943 * L_81 = V_4;
		NullCheck(L_81);
		int32_t L_82 = Camera_get_clearFlags_m1432164003(L_81, /*hidden argument*/NULL);
		if ((((int32_t)L_82) == ((int32_t)1)))
		{
			goto IL_0247;
		}
	}
	{
		Camera_t989002943 * L_83 = V_4;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_clearFlags_m1432164003(L_83, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_84) == ((uint32_t)2))))
		{
			goto IL_026b;
		}
	}

IL_0247:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_85 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_85);
		((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t1318052361 *)NULL);
		HitInfoU5BU5D_t1216529257* L_86 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_86);
		((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t989002943 *)NULL);
	}

IL_026b:
	{
	}

IL_026c:
	{
		int32_t L_87 = V_6;
		V_6 = ((int32_t)((int32_t)L_87+(int32_t)1));
	}

IL_0272:
	{
		int32_t L_88 = V_6;
		CameraU5BU5D_t3897890150* L_89 = V_5;
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_89)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
	}

IL_027e:
	{
		V_14 = 0;
		goto IL_02a4;
	}

IL_0286:
	{
		int32_t L_90 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_91 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_92 = V_14;
		NullCheck(L_91);
		SendMouseEvents_SendEvents_m4819307(NULL /*static, unused*/, L_90, (*(HitInfo_t214434488 *)((L_91)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_92)))), /*hidden argument*/NULL);
		int32_t L_93 = V_14;
		V_14 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_02a4:
	{
		int32_t L_94 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_95 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_95);
		if ((((int32_t)L_94) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_95)->max_length)))))))
		{
			goto IL_0286;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m4819307 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t214434488  ___hit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m4819307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t214434488  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2439334524(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m2038728439(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		HitInfo_t214434488  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_5 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		HitInfo_t214434488  L_7 = ___hit1;
		*(HitInfo_t214434488 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))) = L_7;
		HitInfoU5BU5D_t1216529257* L_8 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		HitInfo_SendMessage_m695071726(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral2933606041, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0107;
	}

IL_004f:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_11 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		bool L_13 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, (*(HitInfo_t214434488 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00d0;
		}
	}
	{
		HitInfo_t214434488  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_15 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		bool L_17 = HitInfo_Compare_m2657684222(NULL /*static, unused*/, L_14, (*(HitInfo_t214434488 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_18 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		HitInfo_SendMessage_m695071726(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral1252746111, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_20 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		HitInfo_SendMessage_m695071726(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral2948729405, /*hidden argument*/NULL);
		HitInfoU5BU5D_t1216529257* L_22 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t214434488  L_24 = V_2;
		*(HitInfo_t214434488 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23))) = L_24;
	}

IL_00d0:
	{
		goto IL_0107;
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_25 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		bool L_27 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, (*(HitInfo_t214434488 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_28 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		HitInfo_SendMessage_m695071726(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral3326067302, /*hidden argument*/NULL);
	}

IL_0107:
	{
		HitInfo_t214434488  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_31 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		bool L_33 = HitInfo_Compare_m2657684222(NULL /*static, unused*/, L_30, (*(HitInfo_t214434488 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0140;
		}
	}
	{
		HitInfo_t214434488  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_013a;
		}
	}
	{
		HitInfo_SendMessage_m695071726((&___hit1), _stringLiteral2495155086, /*hidden argument*/NULL);
	}

IL_013a:
	{
		goto IL_0198;
	}

IL_0140:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_36 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		bool L_38 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, (*(HitInfo_t214434488 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0172;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_39 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		HitInfo_SendMessage_m695071726(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral719642620, /*hidden argument*/NULL);
	}

IL_0172:
	{
		HitInfo_t214434488  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m4245295910(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0197;
		}
	}
	{
		HitInfo_SendMessage_m695071726((&___hit1), _stringLiteral4238796895, /*hidden argument*/NULL);
		HitInfo_SendMessage_m695071726((&___hit1), _stringLiteral2495155086, /*hidden argument*/NULL);
	}

IL_0197:
	{
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t926685678_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1216529257* L_43 = ((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		HitInfo_t214434488  L_45 = ___hit1;
		*(HitInfo_t214434488 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern "C"  void SendMouseEvents__cctor_m1195496091 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m1195496091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t214434488  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t214434488  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t214434488  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t214434488  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t214434488  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t214434488  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t214434488  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t214434488  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t214434488  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t1216529257* L_0 = ((HitInfoU5BU5D_t1216529257*)SZArrayNew(HitInfoU5BU5D_t1216529257_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t214434488  L_1 = V_0;
		*(HitInfo_t214434488 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		HitInfoU5BU5D_t1216529257* L_2 = L_0;
		NullCheck(L_2);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t214434488  L_3 = V_1;
		*(HitInfo_t214434488 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		HitInfoU5BU5D_t1216529257* L_4 = L_2;
		NullCheck(L_4);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t214434488  L_5 = V_2;
		*(HitInfo_t214434488 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_5;
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t1216529257* L_6 = ((HitInfoU5BU5D_t1216529257*)SZArrayNew(HitInfoU5BU5D_t1216529257_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t214434488  L_7 = V_3;
		*(HitInfo_t214434488 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_7;
		HitInfoU5BU5D_t1216529257* L_8 = L_6;
		NullCheck(L_8);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t214434488  L_9 = V_4;
		*(HitInfo_t214434488 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_9;
		HitInfoU5BU5D_t1216529257* L_10 = L_8;
		NullCheck(L_10);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t214434488  L_11 = V_5;
		*(HitInfo_t214434488 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_11;
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t1216529257* L_12 = ((HitInfoU5BU5D_t1216529257*)SZArrayNew(HitInfoU5BU5D_t1216529257_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t214434488  L_13 = V_6;
		*(HitInfo_t214434488 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_13;
		HitInfoU5BU5D_t1216529257* L_14 = L_12;
		NullCheck(L_14);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t214434488  L_15 = V_7;
		*(HitInfo_t214434488 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_15;
		HitInfoU5BU5D_t1216529257* L_16 = L_14;
		NullCheck(L_16);
		Initobj (HitInfo_t214434488_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t214434488  L_17 = V_8;
		*(HitInfo_t214434488 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_17;
		((SendMouseEvents_t926685678_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t926685678_il2cpp_TypeInfo_var))->set_m_CurrentHit_3(L_16);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t214434488_marshal_pinvoke(const HitInfo_t214434488& unmarshaled, HitInfo_t214434488_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t214434488_marshal_pinvoke_back(const HitInfo_t214434488_marshaled_pinvoke& marshaled, HitInfo_t214434488& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t214434488_marshal_pinvoke_cleanup(HitInfo_t214434488_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t214434488_marshal_com(const HitInfo_t214434488& unmarshaled, HitInfo_t214434488_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t214434488_marshal_com_back(const HitInfo_t214434488_marshaled_com& marshaled, HitInfo_t214434488& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t214434488_marshal_com_cleanup(HitInfo_t214434488_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m695071726 (HitInfo_t214434488 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		GameObject_t1318052361 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m2681843329(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m695071726_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	HitInfo_t214434488 * _thisAdjusted = reinterpret_cast<HitInfo_t214434488 *>(__this + 1);
	HitInfo_SendMessage_m695071726(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m4245295910 (RuntimeObject * __this /* static, unused */, HitInfo_t214434488  ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m4245295910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1318052361 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Camera_t989002943 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_2, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m2657684222 (RuntimeObject * __this /* static, unused */, HitInfo_t214434488  ___lhs0, HitInfo_t214434488  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m2657684222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1318052361 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t1318052361 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Camera_t989002943 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t989002943 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1910042615(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m743611948 (FormerlySerializedAsAttribute_t55868289 * __this, String_t* ___oldName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m3632773511 (SerializeField_t1434498030 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern "C"  void SetupCoroutine_InvokeMoveNext_m84037963 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___enumerator0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m84037963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_2, _stringLiteral4070250858, _stringLiteral768326453, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		intptr_t L_3 = ___returnValueAddress1;
		void* L_4 = IntPtr_op_Explicit_m1639871891(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		RuntimeObject* L_5 = ___enumerator0;
		NullCheck(L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1842762036_il2cpp_TypeInfo_var, L_5);
		*((int8_t*)(L_4)) = (int8_t)L_6;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern "C"  RuntimeObject * SetupCoroutine_InvokeMember_m477548565 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___behaviour0, String_t* ___name1, RuntimeObject * ___variable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m477548565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t1568665923* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		V_0 = (ObjectU5BU5D_t1568665923*)NULL;
		RuntimeObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1568665923* L_1 = V_0;
		RuntimeObject * L_2 = ___variable2;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
	}

IL_0016:
	{
		RuntimeObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m3688535302(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		RuntimeObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t1568665923* L_7 = V_0;
		NullCheck(L_4);
		RuntimeObject * L_8 = VirtFuncInvoker8< RuntimeObject *, String_t*, int32_t, Binder_t3602727638 *, RuntimeObject *, ObjectU5BU5D_t1568665923*, ParameterModifierU5BU5D_t3541603165*, CultureInfo_t1870276872 *, StringU5BU5D_t369357837* >::Invoke(75 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t3602727638 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3541603165*)(ParameterModifierU5BU5D_t3541603165*)NULL, (CultureInfo_t1870276872 *)NULL, (StringU5BU5D_t369357837*)(StringU5BU5D_t369357837*)NULL);
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		RuntimeObject * L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3888350337 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Shader_PropertyToID_m3888350337_ftn) (String_t*);
	static Shader_PropertyToID_m3888350337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3888350337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C"  int32_t SortingLayer_GetLayerValueFromID_m576195267 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m576195267_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m576195267_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m576195267_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	int32_t retVal = _il2cpp_icall_func(___id0);
	return retVal;
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C"  void SpaceAttribute__ctor_m3876988278 (SpaceAttribute_t4105298238 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		__this->set_height_0((8.0f));
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C"  void SpaceAttribute__ctor_m1394015468 (SpaceAttribute_t4105298238 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_height_0(L_0);
		return;
	}
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t1344834245  Sprite_get_rect_m1731132080 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1344834245  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_rect_m1110526867(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t1344834245  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t1344834245  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m1110526867 (Sprite_t3012664695 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m1110526867_ftn) (Sprite_t3012664695 *, Rect_t1344834245 *);
	static Sprite_INTERNAL_get_rect_m1110526867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m1110526867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m569774748 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m569774748_ftn) (Sprite_t3012664695 *);
	static Sprite_get_pixelsPerUnit_m569774748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m569774748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t878840578 * Sprite_get_texture_m3301895456 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t878840578 * (*Sprite_get_texture_m3301895456_ftn) (Sprite_t3012664695 *);
	static Sprite_get_texture_m3301895456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m3301895456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	Texture2D_t878840578 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t878840578 * Sprite_get_associatedAlphaSplitTexture_m251464071 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t878840578 * (*Sprite_get_associatedAlphaSplitTexture_m251464071_ftn) (Sprite_t3012664695 *);
	static Sprite_get_associatedAlphaSplitTexture_m251464071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_associatedAlphaSplitTexture_m251464071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_associatedAlphaSplitTexture()");
	Texture2D_t878840578 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t1344834245  Sprite_get_textureRect_m2683085873 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1344834245  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_textureRect_m477698307(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t1344834245  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t1344834245  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m477698307 (Sprite_t3012664695 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m477698307_ftn) (Sprite_t3012664695 *, Rect_t1344834245 *);
	static Sprite_INTERNAL_get_textureRect_m477698307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m477698307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Sprite::get_packed()
extern "C"  bool Sprite_get_packed_m3326896168 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	typedef bool (*Sprite_get_packed_m3326896168_ftn) (Sprite_t3012664695 *);
	static Sprite_get_packed_m3326896168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_packed_m3326896168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_packed()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t380635127  Sprite_get_border_m3407652672 (Sprite_t3012664695 * __this, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_border_m3647917258(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t380635127  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t380635127  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m3647917258 (Sprite_t3012664695 * __this, Vector4_t380635127 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m3647917258_ftn) (Sprite_t3012664695 *, Vector4_t380635127 *);
	static Sprite_INTERNAL_get_border_m3647917258_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m3647917258_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C"  Vector4_t380635127  DataUtility_GetInnerUV_m1986177378 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t3012664695 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t380635127  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t380635127  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144_ftn) (Sprite_t3012664695 *, Vector4_t380635127 *);
	static DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetInnerUV_m2313045144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C"  Vector4_t380635127  DataUtility_GetOuterUV_m3910060428 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t3012664695 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t380635127  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t380635127  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919_ftn) (Sprite_t3012664695 *, Vector4_t380635127 *);
	static DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetOuterUV_m3448283919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C"  Vector4_t380635127  DataUtility_GetPadding_m1299531973 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t3012664695 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetPadding_m2332767386(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t380635127  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t380635127  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m2332767386 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector4_t380635127 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetPadding_m2332767386_ftn) (Sprite_t3012664695 *, Vector4_t380635127 *);
	static DataUtility_INTERNAL_CALL_GetPadding_m2332767386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetPadding_m2332767386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C"  Vector2_t3057062568  DataUtility_GetMinSize_m3150030788 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t3012664695 * L_0 = ___sprite0;
		DataUtility_Internal_GetMinSize_m40337108(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m40337108 (RuntimeObject * __this /* static, unused */, Sprite_t3012664695 * ___sprite0, Vector2_t3057062568 * ___output1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m40337108_ftn) (Sprite_t3012664695 *, Vector2_t3057062568 *);
	static DataUtility_Internal_GetMinSize_m40337108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m40337108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite0, ___output1);
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern "C"  void StackTraceUtility_SetProjectFolder_m2901744821 (RuntimeObject * __this /* static, unused */, String_t* ___folder0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m2901744821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m1530107849(L_0, _stringLiteral1047796672, _stringLiteral618654864, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->set_projectFolder_0(L_1);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m3776908625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m3776908625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StackTrace_t1080444557 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		StackTrace_t1080444557 * L_0 = (StackTrace_t1080444557 *)il2cpp_codegen_object_new(StackTrace_t1080444557_il2cpp_TypeInfo_var);
		StackTrace__ctor_m2668716623(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1080444557 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m2461990048(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_1 = L_3;
		String_t* L_4 = V_1;
		V_2 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m2557993735 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m2557993735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m2784677750(L_1, _stringLiteral2718138177, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m2784677750(L_3, _stringLiteral3270038836, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m2784677750(L_5, _stringLiteral1161391446, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m2784677750(L_7, _stringLiteral3758298780, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m2784677750(L_9, _stringLiteral3366573739, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m2784677750(L_11, _stringLiteral2996873391, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0066;
	}

IL_0065:
	{
		G_B7_0 = 1;
	}

IL_0066:
	{
		V_1 = (bool)G_B7_0;
		goto IL_006c;
	}

IL_006c:
	{
		bool L_13 = V_1;
		return L_13;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m3565903846 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m3565903846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t2123675094 * V_0 = NULL;
	StringBuilder_t2355629516 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t1080444557 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentException_t3637419113 * L_1 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_1, _stringLiteral2280038672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t2123675094 *)IsInstClass((RuntimeObject*)L_2, Exception_t2123675094_il2cpp_TypeInfo_var));
		Exception_t2123675094 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentException_t3637419113 * L_4 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_4, _stringLiteral376254785, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002a:
	{
		Exception_t2123675094 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004c;
	}

IL_003f:
	{
		Exception_t2123675094 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m992845221(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004c:
	{
		StringBuilder_t2355629516 * L_10 = (StringBuilder_t2355629516 *)il2cpp_codegen_object_new(StringBuilder_t2355629516_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2246994706(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		*((RuntimeObject **)(L_11)) = (RuntimeObject *)_stringLiteral1772440553;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_11), (RuntimeObject *)_stringLiteral1772440553);
		V_2 = _stringLiteral1772440553;
		goto IL_0106;
	}

IL_0064:
	{
		String_t* L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m992845221(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		Exception_t2123675094 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_14);
		V_2 = L_15;
		goto IL_008e;
	}

IL_007c:
	{
		Exception_t2123675094 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		String_t* L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m414952842(NULL /*static, unused*/, L_17, _stringLiteral572800344, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
	}

IL_008e:
	{
		Exception_t2123675094 * L_20 = V_0;
		NullCheck(L_20);
		Type_t * L_21 = Exception_GetType_m200229271(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		V_3 = L_22;
		V_4 = _stringLiteral1772440553;
		Exception_t2123675094 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
		if (!L_24)
		{
			goto IL_00b4;
		}
	}
	{
		Exception_t2123675094 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_25);
		V_4 = L_26;
	}

IL_00b4:
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		String_t* L_28 = String_Trim_m710734237(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m992845221(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00dc;
		}
	}
	{
		String_t* L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m1227504536(NULL /*static, unused*/, L_30, _stringLiteral948631903, /*hidden argument*/NULL);
		V_3 = L_31;
		String_t* L_32 = V_3;
		String_t* L_33 = V_4;
		String_t* L_34 = String_Concat_m1227504536(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		V_3 = L_34;
	}

IL_00dc:
	{
		String_t** L_35 = ___message1;
		String_t* L_36 = V_3;
		*((RuntimeObject **)(L_35)) = (RuntimeObject *)L_36;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_35), (RuntimeObject *)L_36);
		Exception_t2123675094 * L_37 = V_0;
		NullCheck(L_37);
		Exception_t2123675094 * L_38 = Exception_get_InnerException_m4268342947(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00fe;
		}
	}
	{
		String_t* L_39 = V_3;
		String_t* L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1756273763(NULL /*static, unused*/, _stringLiteral1714897927, L_39, _stringLiteral572800344, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
	}

IL_00fe:
	{
		Exception_t2123675094 * L_42 = V_0;
		NullCheck(L_42);
		Exception_t2123675094 * L_43 = Exception_get_InnerException_m4268342947(L_42, /*hidden argument*/NULL);
		V_0 = L_43;
	}

IL_0106:
	{
		Exception_t2123675094 * L_44 = V_0;
		if (L_44)
		{
			goto IL_0064;
		}
	}
	{
		StringBuilder_t2355629516 * L_45 = V_1;
		String_t* L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m1227504536(NULL /*static, unused*/, L_46, _stringLiteral572800344, /*hidden argument*/NULL);
		NullCheck(L_45);
		StringBuilder_Append_m346941624(L_45, L_47, /*hidden argument*/NULL);
		StackTrace_t1080444557 * L_48 = (StackTrace_t1080444557 *)il2cpp_codegen_object_new(StackTrace_t1080444557_il2cpp_TypeInfo_var);
		StackTrace__ctor_m2668716623(L_48, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_48;
		StringBuilder_t2355629516 * L_49 = V_1;
		StackTrace_t1080444557 * L_50 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		String_t* L_51 = StackTraceUtility_ExtractFormattedStackTrace_m2461990048(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		StringBuilder_Append_m346941624(L_49, L_51, /*hidden argument*/NULL);
		String_t** L_52 = ___stackTrace2;
		StringBuilder_t2355629516 * L_53 = V_1;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_53);
		*((RuntimeObject **)(L_52)) = (RuntimeObject *)L_54;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_52), (RuntimeObject *)L_54);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m4079690477 (RuntimeObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m4079690477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t369357837* V_1 = NULL;
	StringBuilder_t2355629516 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_1;
		goto IL_02a4;
	}

IL_0012:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t83643201* L_3 = ((CharU5BU5D_t83643201*)SZArrayNew(CharU5BU5D_t83643201_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t369357837* L_4 = String_Split_m2076658130(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m992845221(L_5, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_7 = (StringBuilder_t2355629516 *)il2cpp_codegen_object_new(StringBuilder_t2355629516_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2246994706(L_7, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_0046;
	}

IL_0037:
	{
		StringU5BU5D_t369357837* L_8 = V_1;
		int32_t L_9 = V_3;
		StringU5BU5D_t369357837* L_10 = V_1;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m710734237(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_16 = V_3;
		StringU5BU5D_t369357837* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		V_4 = 0;
		goto IL_028e;
	}

IL_0057:
	{
		StringU5BU5D_t369357837* L_18 = V_1;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_5 = L_21;
		String_t* L_22 = V_5;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m992845221(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_24 = V_5;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m1323985989(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_007e;
		}
	}

IL_0079:
	{
		goto IL_0288;
	}

IL_007e:
	{
		String_t* L_26 = V_5;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m2784677750(L_26, _stringLiteral801117164, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_0288;
	}

IL_0094:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_29 = V_5;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m2784677750(L_29, _stringLiteral165326431, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00b0;
		}
	}
	{
		goto IL_0298;
	}

IL_00b0:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_32 = V_4;
		StringU5BU5D_t369357837* L_33 = V_1;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_0107;
		}
	}
	{
		String_t* L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m2557993735(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0107;
		}
	}
	{
		StringU5BU5D_t369357837* L_36 = V_1;
		int32_t L_37 = V_4;
		NullCheck(L_36);
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m2557993735(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00e4;
		}
	}
	{
		goto IL_0288;
	}

IL_00e4:
	{
		String_t* L_41 = V_5;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m170176446(L_41, _stringLiteral1038419906, /*hidden argument*/NULL);
		V_6 = L_42;
		int32_t L_43 = V_6;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_0106;
		}
	}
	{
		String_t* L_44 = V_5;
		int32_t L_45 = V_6;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m1618205500(L_44, 0, L_45, /*hidden argument*/NULL);
		V_5 = L_46;
	}

IL_0106:
	{
	}

IL_0107:
	{
		String_t* L_47 = V_5;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m170176446(L_47, _stringLiteral4277981398, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_011e;
		}
	}
	{
		goto IL_0288;
	}

IL_011e:
	{
		String_t* L_49 = V_5;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m170176446(L_49, _stringLiteral283101400, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0288;
	}

IL_0135:
	{
		String_t* L_51 = V_5;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m170176446(L_51, _stringLiteral3165837002, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_014c;
		}
	}
	{
		goto IL_0288;
	}

IL_014c:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_54 = V_5;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m2784677750(L_54, _stringLiteral3589461666, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_56 = V_5;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m1375684648(L_56, _stringLiteral2358762624, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0179;
		}
	}
	{
		goto IL_0288;
	}

IL_0179:
	{
		String_t* L_58 = V_5;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m2784677750(L_58, _stringLiteral869691469, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0197;
		}
	}
	{
		String_t* L_60 = V_5;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m1510850277(L_60, 0, 3, /*hidden argument*/NULL);
		V_5 = L_61;
	}

IL_0197:
	{
		String_t* L_62 = V_5;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m170176446(L_62, _stringLiteral898477964, /*hidden argument*/NULL);
		V_7 = L_63;
		V_8 = (-1);
		int32_t L_64 = V_7;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01c0;
		}
	}
	{
		String_t* L_65 = V_5;
		int32_t L_66 = V_7;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m3924003364(L_65, _stringLiteral2358762624, L_66, /*hidden argument*/NULL);
		V_8 = L_67;
	}

IL_01c0:
	{
		int32_t L_68 = V_7;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_69 = V_8;
		int32_t L_70 = V_7;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01e5;
		}
	}
	{
		String_t* L_71 = V_5;
		int32_t L_72 = V_7;
		int32_t L_73 = V_8;
		int32_t L_74 = V_7;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m1510850277(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_5 = L_75;
	}

IL_01e5:
	{
		String_t* L_76 = V_5;
		NullCheck(L_76);
		String_t* L_77 = String_Replace_m1530107849(L_76, _stringLiteral1035361912, _stringLiteral1772440553, /*hidden argument*/NULL);
		V_5 = L_77;
		String_t* L_78 = V_5;
		NullCheck(L_78);
		String_t* L_79 = String_Replace_m1530107849(L_78, _stringLiteral1047796672, _stringLiteral618654864, /*hidden argument*/NULL);
		V_5 = L_79;
		String_t* L_80 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		String_t* L_82 = String_Replace_m1530107849(L_80, L_81, _stringLiteral1772440553, /*hidden argument*/NULL);
		V_5 = L_82;
		String_t* L_83 = V_5;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m2145106676(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_5 = L_84;
		String_t* L_85 = V_5;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m3640043114(L_85, _stringLiteral3711086256, /*hidden argument*/NULL);
		V_9 = L_86;
		int32_t L_87 = V_9;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_0274;
		}
	}
	{
		String_t* L_88 = V_5;
		int32_t L_89 = V_9;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m1510850277(L_88, L_89, 5, /*hidden argument*/NULL);
		V_5 = L_90;
		String_t* L_91 = V_5;
		int32_t L_92 = V_9;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m1416384255(L_91, L_92, _stringLiteral2088073012, /*hidden argument*/NULL);
		V_5 = L_93;
		String_t* L_94 = V_5;
		String_t* L_95 = V_5;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m992845221(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m1416384255(L_94, L_96, _stringLiteral272318210, /*hidden argument*/NULL);
		V_5 = L_97;
	}

IL_0274:
	{
		StringBuilder_t2355629516 * L_98 = V_2;
		String_t* L_99 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m1227504536(NULL /*static, unused*/, L_99, _stringLiteral572800344, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m346941624(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0288:
	{
		int32_t L_101 = V_4;
		V_4 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_028e:
	{
		int32_t L_102 = V_4;
		StringU5BU5D_t369357837* L_103 = V_1;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_103)->max_length)))))))
		{
			goto IL_0057;
		}
	}

IL_0298:
	{
		StringBuilder_t2355629516 * L_104 = V_2;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_0 = L_105;
		goto IL_02a4;
	}

IL_02a4:
	{
		String_t* L_106 = V_0;
		return L_106;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m2461990048 (RuntimeObject * __this /* static, unused */, StackTrace_t1080444557 * ___stackTrace0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m2461990048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t2355629516 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t795761381 * V_2 = NULL;
	MethodBase_t3535115348 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t4074997356* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	String_t* V_12 = NULL;
	int32_t G_B27_0 = 0;
	int32_t G_B29_0 = 0;
	{
		StringBuilder_t2355629516 * L_0 = (StringBuilder_t2355629516 *)il2cpp_codegen_object_new(StringBuilder_t2355629516_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2246994706(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_02ba;
	}

IL_0013:
	{
		StackTrace_t1080444557 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t795761381 * L_3 = VirtFuncInvoker1< StackFrame_t795761381 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t795761381 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t3535115348 * L_5 = VirtFuncInvoker0< MethodBase_t3535115348 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t3535115348 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_02b6;
	}

IL_002e:
	{
		MethodBase_t3535115348 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_02b6;
	}

IL_0042:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m992845221(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		StringBuilder_t2355629516 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m346941624(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m346941624(L_17, _stringLiteral2072793447, /*hidden argument*/NULL);
	}

IL_0075:
	{
		StringBuilder_t2355629516 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m346941624(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m346941624(L_21, _stringLiteral60326768, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_22 = V_0;
		MethodBase_t3535115348 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m346941624(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m346941624(L_25, _stringLiteral3968012355, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t3535115348 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t4074997356* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t4074997356* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00f4;
	}

IL_00bb:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00d4;
		}
	}
	{
		StringBuilder_t2355629516 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m346941624(L_29, _stringLiteral268940233, /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_00d4:
	{
		V_8 = (bool)0;
	}

IL_00d7:
	{
		StringBuilder_t2355629516 * L_30 = V_0;
		ParameterInfoU5BU5D_t4074997356* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		ParameterInfo_t2372523953 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m346941624(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t4074997356* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length)))))))
		{
			goto IL_00bb;
		}
	}
	{
		StringBuilder_t2355629516 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m346941624(L_40, _stringLiteral272318210, /*hidden argument*/NULL);
		StackFrame_t795761381 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_02a9;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_45, _stringLiteral1236921323, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0147;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_48, _stringLiteral2253610359, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_020c;
		}
	}

IL_0147:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_51, _stringLiteral2644163874, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0173;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_54, _stringLiteral2253610359, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_020c;
		}
	}

IL_0173:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_57, _stringLiteral1576533761, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_019f;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_60, _stringLiteral2253610359, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_020c;
		}
	}

IL_019f:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_63, _stringLiteral1772141335, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01cb;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_66, _stringLiteral4250600190, /*hidden argument*/NULL);
		if (L_67)
		{
			goto IL_020c;
		}
	}

IL_01cb:
	{
		MethodBase_t3535115348 * L_68 = V_3;
		NullCheck(L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_68);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_70 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_69, _stringLiteral3976536586, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_71 = V_4;
		NullCheck(L_71);
		String_t* L_72 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_73 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_72, _stringLiteral3428737307, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_74 = V_4;
		NullCheck(L_74);
		String_t* L_75 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_74);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_76 = String_op_Equality_m3268564145(NULL /*static, unused*/, L_75, _stringLiteral2253610359, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_76));
		goto IL_020a;
	}

IL_0209:
	{
		G_B27_0 = 0;
	}

IL_020a:
	{
		G_B29_0 = G_B27_0;
		goto IL_020d;
	}

IL_020c:
	{
		G_B29_0 = 1;
	}

IL_020d:
	{
		V_10 = (bool)G_B29_0;
		bool L_77 = V_10;
		if (L_77)
		{
			goto IL_02a8;
		}
	}
	{
		StringBuilder_t2355629516 * L_78 = V_0;
		NullCheck(L_78);
		StringBuilder_Append_m346941624(L_78, _stringLiteral2088073012, /*hidden argument*/NULL);
		String_t* L_79 = V_9;
		NullCheck(L_79);
		String_t* L_80 = String_Replace_m1530107849(L_79, _stringLiteral1047796672, _stringLiteral618654864, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		bool L_82 = String_StartsWith_m2784677750(L_80, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_026a;
		}
	}
	{
		String_t* L_83 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t142931386_il2cpp_TypeInfo_var);
		String_t* L_84 = ((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_84);
		int32_t L_85 = String_get_Length_m992845221(L_84, /*hidden argument*/NULL);
		String_t* L_86 = V_9;
		NullCheck(L_86);
		int32_t L_87 = String_get_Length_m992845221(L_86, /*hidden argument*/NULL);
		String_t* L_88 = ((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_88);
		int32_t L_89 = String_get_Length_m992845221(L_88, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_90 = String_Substring_m1618205500(L_83, L_85, ((int32_t)((int32_t)L_87-(int32_t)L_89)), /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_026a:
	{
		StringBuilder_t2355629516 * L_91 = V_0;
		String_t* L_92 = V_9;
		NullCheck(L_91);
		StringBuilder_Append_m346941624(L_91, L_92, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_93 = V_0;
		NullCheck(L_93);
		StringBuilder_Append_m346941624(L_93, _stringLiteral60326768, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_94 = V_0;
		StackFrame_t795761381 * L_95 = V_2;
		NullCheck(L_95);
		int32_t L_96 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_95);
		V_11 = L_96;
		String_t* L_97 = Int32_ToString_m2515392467((&V_11), /*hidden argument*/NULL);
		NullCheck(L_94);
		StringBuilder_Append_m346941624(L_94, L_97, /*hidden argument*/NULL);
		StringBuilder_t2355629516 * L_98 = V_0;
		NullCheck(L_98);
		StringBuilder_Append_m346941624(L_98, _stringLiteral272318210, /*hidden argument*/NULL);
	}

IL_02a8:
	{
	}

IL_02a9:
	{
		StringBuilder_t2355629516 * L_99 = V_0;
		NullCheck(L_99);
		StringBuilder_Append_m346941624(L_99, _stringLiteral572800344, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		int32_t L_100 = V_1;
		V_1 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02ba:
	{
		int32_t L_101 = V_1;
		StackTrace_t1080444557 * L_102 = ___stackTrace0;
		NullCheck(L_102);
		int32_t L_103 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_102);
		if ((((int32_t)L_101) < ((int32_t)L_103)))
		{
			goto IL_0013;
		}
	}
	{
		StringBuilder_t2355629516 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_12 = L_105;
		goto IL_02d3;
	}

IL_02d3:
	{
		String_t* L_106 = V_12;
		return L_106;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern "C"  void StackTraceUtility__cctor_m3261477867 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m3261477867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((StackTraceUtility_t142931386_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t142931386_il2cpp_TypeInfo_var))->set_projectFolder_0(_stringLiteral1772440553);
		return;
	}
}
// UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern "C"  int32_t SystemInfo_get_operatingSystemFamily_m3895795971 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_operatingSystemFamily_m3895795971_ftn) ();
	static SystemInfo_get_operatingSystemFamily_m3895795971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystemFamily_m3895795971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystemFamily()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m367412388 (TextAreaAttribute_t1594486658 * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3010839343 (Texture_t2838694469 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture__ctor_m3010839343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m115646220 (RuntimeObject * __this /* static, unused */, Texture_t2838694469 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m115646220_ftn) (Texture_t2838694469 *);
	static Texture_Internal_GetWidth_m115646220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m115646220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m1514583028 (RuntimeObject * __this /* static, unused */, Texture_t2838694469 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m1514583028_ftn) (Texture_t2838694469 *);
	static Texture_Internal_GetHeight_m1514583028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m1514583028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m255171677 (Texture_t2838694469 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetWidth_m115646220(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m1838936065 (Texture_t2838694469 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetHeight_m1514583028(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m1565425947 (Texture_t2838694469 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_filterMode_m1565425947_ftn) (Texture_t2838694469 *, int32_t);
	static Texture_set_filterMode_m1565425947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m1565425947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C"  int32_t Texture_get_wrapMode_m3862319941 (Texture_t2838694469 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_wrapMode_m3862319941_ftn) (Texture_t2838694469 *);
	static Texture_get_wrapMode_m3862319941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_wrapMode_m3862319941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_wrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m2103720371 (Texture_t2838694469 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_wrapMode_m2103720371_ftn) (Texture_t2838694469 *, int32_t);
	static Texture_set_wrapMode_m2103720371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m2103720371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern "C"  Vector2_t3057062568  Texture_get_texelSize_m4072577456 (Texture_t2838694469 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Texture_INTERNAL_get_texelSize_m1813954261(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m1813954261 (Texture_t2838694469 * __this, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_INTERNAL_get_texelSize_m1813954261_ftn) (Texture_t2838694469 *, Vector2_t3057062568 *);
	static Texture_INTERNAL_get_texelSize_m1813954261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_INTERNAL_get_texelSize_m1813954261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m4114474571 (Texture2D_t878840578 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m4114474571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m3010839343(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		Texture2D_Internal_Create_m3845050557(NULL /*static, unused*/, __this, L_0, L_1, 4, (bool)1, (bool)0, (intptr_t)(0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D__ctor_m3327994032 (Texture2D_t878840578 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method)
{
	{
		Texture__ctor_m3010839343(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		bool L_4 = ___linear4;
		intptr_t L_5 = ___nativeTex5;
		Texture2D_Internal_Create_m3845050557(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m3845050557 (RuntimeObject * __this /* static, unused */, Texture2D_t878840578 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Internal_Create_m3845050557_ftn) (Texture2D_t878840578 *, int32_t, int32_t, int32_t, bool, bool, intptr_t);
	static Texture2D_Internal_Create_m3845050557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m3845050557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  Texture2D_t878840578 * Texture2D_CreateExternalTexture_m4069785793 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D_CreateExternalTexture_m4069785793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t878840578 * V_0 = NULL;
	{
		intptr_t L_0 = ___nativeTex5;
		bool L_1 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1422478095(L_2, _stringLiteral175949560, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		int32_t L_3 = ___width0;
		int32_t L_4 = ___height1;
		int32_t L_5 = ___format2;
		bool L_6 = ___mipmap3;
		bool L_7 = ___linear4;
		intptr_t L_8 = ___nativeTex5;
		Texture2D_t878840578 * L_9 = (Texture2D_t878840578 *)il2cpp_codegen_object_new(Texture2D_t878840578_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3327994032(L_9, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0030;
	}

IL_0030:
	{
		Texture2D_t878840578 * L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern "C"  void Texture2D_UpdateExternalTexture_m4242697313 (Texture2D_t878840578 * __this, intptr_t ___nativeTex0, const RuntimeMethod* method)
{
	typedef void (*Texture2D_UpdateExternalTexture_m4242697313_ftn) (Texture2D_t878840578 *, intptr_t);
	static Texture2D_UpdateExternalTexture_m4242697313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_UpdateExternalTexture_m4242697313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)");
	_il2cpp_icall_func(__this, ___nativeTex0);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t878840578 * Texture2D_get_whiteTexture_m850840723 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Texture2D_t878840578 * (*Texture2D_get_whiteTexture_m850840723_ftn) ();
	static Texture2D_get_whiteTexture_m850840723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m850840723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	Texture2D_t878840578 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t460381780  Texture2D_GetPixelBilinear_m2853817766 (Texture2D_t878840578 * __this, float ___u0, float ___v1, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t460381780  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t460381780  L_2 = V_0;
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Color_t460381780  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352 (RuntimeObject * __this /* static, unused */, Texture2D_t878840578 * ___self0, float ___u1, float ___v2, Color_t460381780 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352_ftn) (Texture2D_t878840578 *, float, float, Color_t460381780 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m4178462352_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2104342217 (Texture2D_t878840578 * __this, Color32U5BU5D_t1505762612* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetAllPixels32_m2104342217_ftn) (Texture2D_t878840578 *, Color32U5BU5D_t1505762612*, int32_t);
	static Texture2D_SetAllPixels32_m2104342217_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m2104342217_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetBlockOfPixels32_m2327681220 (Texture2D_t878840578 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t1505762612* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetBlockOfPixels32_m2327681220_ftn) (Texture2D_t878840578 *, int32_t, int32_t, int32_t, int32_t, Color32U5BU5D_t1505762612*, int32_t);
	static Texture2D_SetBlockOfPixels32_m2327681220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetBlockOfPixels32_m2327681220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m3079630859 (Texture2D_t878840578 * __this, Color32U5BU5D_t1505762612* ___colors0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t1505762612* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m1443084076(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m1443084076 (Texture2D_t878840578 * __this, Color32U5BU5D_t1505762612* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	{
		Color32U5BU5D_t1505762612* L_0 = ___colors0;
		int32_t L_1 = ___miplevel1;
		Texture2D_SetAllPixels32_m2104342217(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m2824360293 (Texture2D_t878840578 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t1505762612* ___colors4, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___blockWidth2;
		int32_t L_3 = ___blockHeight3;
		Color32U5BU5D_t1505762612* L_4 = ___colors4;
		int32_t L_5 = V_0;
		Texture2D_SetPixels32_m1536952667(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m1536952667 (Texture2D_t878840578 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t1505762612* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___blockWidth2;
		int32_t L_3 = ___blockHeight3;
		Color32U5BU5D_t1505762612* L_4 = ___colors4;
		int32_t L_5 = ___miplevel5;
		Texture2D_SetBlockOfPixels32_m2327681220(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m3107428142 (Texture2D_t878840578 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Apply_m3107428142_ftn) (Texture2D_t878840578 *, bool, bool);
	static Texture2D_Apply_m3107428142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m3107428142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m3335581688 (Texture2D_t878840578 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m3107428142(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m2883899274 (ThreadAndSerializationSafeAttribute_t46731272 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m4280845921 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_deltaTime_m4280845921_ftn) ();
	static Time_get_deltaTime_m4280845921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m4280845921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m2357498356 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledTime_m2357498356_ftn) ();
	static Time_get_unscaledTime_m2357498356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m2357498356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m3163136816 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m3163136816_ftn) ();
	static Time_get_unscaledDeltaTime_m3163136816_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m3163136816_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m4074464187 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m4074464187_ftn) ();
	static Time_get_realtimeSinceStartup_m4074464187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m4074464187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m160799429 (TooltipAttribute_t2327649817 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m4188294198 (Touch_t1822905180 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_fingerId_m4188294198_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1822905180 * _thisAdjusted = reinterpret_cast<Touch_t1822905180 *>(__this + 1);
	return Touch_get_fingerId_m4188294198(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t3057062568  Touch_get_position_m884931166 (Touch_t1822905180 * __this, const RuntimeMethod* method)
{
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t3057062568  L_0 = __this->get_m_Position_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t3057062568  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t3057062568  Touch_get_position_m884931166_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1822905180 * _thisAdjusted = reinterpret_cast<Touch_t1822905180 *>(__this + 1);
	return Touch_get_position_m884931166(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m4210103310 (Touch_t1822905180 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Phase_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_phase_m4210103310_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1822905180 * _thisAdjusted = reinterpret_cast<Touch_t1822905180 *>(__this + 1);
	return Touch_get_phase_m4210103310(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3315114897 (Touch_t1822905180 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_type_m3315114897_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t1822905180 * _thisAdjusted = reinterpret_cast<Touch_t1822905180 *>(__this + 1);
	return Touch_get_type_m3315114897(_thisAdjusted, method);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m2368604044 (TouchScreenKeyboard_t3991761301 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m2368604044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(TouchScreenKeyboardType_t2115689981_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2545527069_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m602550190(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m1444157391(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m1444157391(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m1444157391(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m1444157391(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m2816688376 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m2816688376_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_Destroy_m2816688376_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m2816688376_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m115316175 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m2816688376(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270 (TouchScreenKeyboard_t3991761301 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270_ftn) (TouchScreenKeyboard_t3991761301 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t934812271 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1491987270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m751800886 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = Application_get_platform_m4196840056(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))))
		{
			case 0:
			{
				goto IL_006d;
			}
			case 1:
			{
				goto IL_006d;
			}
			case 2:
			{
				goto IL_006d;
			}
			case 3:
			{
				goto IL_0034;
			}
			case 4:
			{
				goto IL_0034;
			}
			case 5:
			{
				goto IL_0066;
			}
			case 6:
			{
				goto IL_0034;
			}
			case 7:
			{
				goto IL_0034;
			}
			case 8:
			{
				goto IL_0066;
			}
		}
	}

IL_0034:
	{
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)((int32_t)30))))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0066;
			}
		}
	}
	{
		int32_t L_3 = V_0;
		switch (((int32_t)((int32_t)L_3-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0074;
			}
			case 2:
			{
				goto IL_0074;
			}
			case 3:
			{
				goto IL_0066;
			}
		}
	}
	{
		goto IL_0074;
	}

IL_0066:
	{
		V_1 = (bool)1;
		goto IL_007b;
	}

IL_006d:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_0074:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_007b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t3991761301 * TouchScreenKeyboard_Open_m3989449005 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m3989449005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	TouchScreenKeyboard_t3991761301 * V_2 = NULL;
	{
		V_0 = _stringLiteral1772440553;
		V_1 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t3991761301 * L_7 = TouchScreenKeyboard_Open_m2497313546(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_001c;
	}

IL_001c:
	{
		TouchScreenKeyboard_t3991761301 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t3991761301 * TouchScreenKeyboard_Open_m2781171978 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2781171978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	TouchScreenKeyboard_t3991761301 * V_3 = NULL;
	{
		V_0 = _stringLiteral1772440553;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = V_2;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t3991761301 * L_7 = TouchScreenKeyboard_Open_m2497313546(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		goto IL_001d;
	}

IL_001d:
	{
		TouchScreenKeyboard_t3991761301 * L_8 = V_3;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t3991761301 * TouchScreenKeyboard_Open_m2497313546 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2497313546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_t3991761301 * V_0 = NULL;
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t3991761301 * L_7 = (TouchScreenKeyboard_t3991761301 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t3991761301_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m2368604044(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0016;
	}

IL_0016:
	{
		TouchScreenKeyboard_t3991761301 * L_8 = V_0;
		return L_8;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m3810642818 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m3810642818_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_get_text_m3810642818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m3810642818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m2683350814 (TouchScreenKeyboard_t3991761301 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m2683350814_ftn) (TouchScreenKeyboard_t3991761301 *, String_t*);
	static TouchScreenKeyboard_set_text_m2683350814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m2683350814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m3252342509 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m3252342509_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m3252342509_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m3252342509_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m3470643509 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m3470643509_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_get_active_m3470643509_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m3470643509_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m2717951573 (TouchScreenKeyboard_t3991761301 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m2717951573_ftn) (TouchScreenKeyboard_t3991761301 *, bool);
	static TouchScreenKeyboard_set_active_m2717951573_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m2717951573_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m3826572977 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m3826572977_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_get_done_m3826572977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m3826572977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m3980351409 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m3980351409_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_get_wasCanceled_m3980351409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m3980351409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern "C"  bool TouchScreenKeyboard_get_canGetSelection_m749651394 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_canGetSelection_m749651394_ftn) (TouchScreenKeyboard_t3991761301 *);
	static TouchScreenKeyboard_get_canGetSelection_m749651394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_canGetSelection_m749651394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_canGetSelection()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern "C"  RangeInt_t3767345128  TouchScreenKeyboard_get_selection_m1874451608 (TouchScreenKeyboard_t3991761301 * __this, const RuntimeMethod* method)
{
	RangeInt_t3767345128  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RangeInt_t3767345128  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t* L_0 = (&V_0)->get_address_of_start_0();
		int32_t* L_1 = (&V_0)->get_address_of_length_1();
		TouchScreenKeyboard_GetSelectionInternal_m3891609934(__this, L_0, L_1, /*hidden argument*/NULL);
		RangeInt_t3767345128  L_2 = V_0;
		V_1 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		RangeInt_t3767345128  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m3891609934 (TouchScreenKeyboard_t3991761301 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_GetSelectionInternal_m3891609934_ftn) (TouchScreenKeyboard_t3991761301 *, int32_t*, int32_t*);
	static TouchScreenKeyboard_GetSelectionInternal_m3891609934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_GetSelectionInternal_m3891609934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)");
	_il2cpp_icall_func(__this, ___start0, ___length1);
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t774033239_marshal_pinvoke(const TrackedReference_t774033239& unmarshaled, TrackedReference_t774033239_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t774033239_marshal_pinvoke_back(const TrackedReference_t774033239_marshaled_pinvoke& marshaled, TrackedReference_t774033239& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t774033239_marshal_pinvoke_cleanup(TrackedReference_t774033239_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t774033239_marshal_com(const TrackedReference_t774033239& unmarshaled, TrackedReference_t774033239_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t774033239_marshal_com_back(const TrackedReference_t774033239_marshaled_com& marshaled, TrackedReference_t774033239& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t774033239_marshal_com_cleanup(TrackedReference_t774033239_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m3187773712 (RuntimeObject * __this /* static, unused */, TrackedReference_t774033239 * ___x0, TrackedReference_t774033239 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m3187773712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	{
		TrackedReference_t774033239 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t774033239 * L_1 = ___y1;
		V_1 = L_1;
		RuntimeObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0067;
	}

IL_0018:
	{
		RuntimeObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		TrackedReference_t774033239 * L_5 = ___x0;
		NullCheck(L_5);
		intptr_t L_6 = L_5->get_m_Ptr_0();
		bool L_7 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_6, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0067;
	}

IL_0034:
	{
		RuntimeObject * L_8 = V_0;
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		TrackedReference_t774033239 * L_9 = ___y1;
		NullCheck(L_9);
		intptr_t L_10 = L_9->get_m_Ptr_0();
		bool L_11 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_10, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_11;
		goto IL_0067;
	}

IL_0050:
	{
		TrackedReference_t774033239 * L_12 = ___x0;
		NullCheck(L_12);
		intptr_t L_13 = L_12->get_m_Ptr_0();
		TrackedReference_t774033239 * L_14 = ___y1;
		NullCheck(L_14);
		intptr_t L_15 = L_14->get_m_Ptr_0();
		bool L_16 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		goto IL_0067;
	}

IL_0067:
	{
		bool L_17 = V_2;
		return L_17;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern "C"  bool TrackedReference_Equals_m3554410169 (TrackedReference_t774033239 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m3554410169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m3187773712(NULL /*static, unused*/, ((TrackedReference_t774033239 *)IsInstClass((RuntimeObject*)L_0, TrackedReference_t774033239_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m146576582 (TrackedReference_t774033239 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m3542367511(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Transform::.ctor()
extern "C"  void Transform__ctor_m2171111032 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m3701326763(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t329709361  Transform_get_position_m609417876 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_position_m3294911734(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m1768136472 (Transform_t532597831 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_position_m1243462975(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3294911734 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_position_m3294911734_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_get_position_m3294911734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m3294911734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1243462975 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_position_m1243462975_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_set_position_m1243462975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m1243462975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t329709361  Transform_get_localPosition_m3038103500 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localPosition_m2892246933(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3967901824 (Transform_t532597831 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localPosition_m2449196661(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m2892246933 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m2892246933_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_get_localPosition_m2892246933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m2892246933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m2449196661 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m2449196661_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_set_localPosition_m2449196661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m2449196661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t329709361  Transform_get_forward_m3951380002 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_forward_m3951380002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t2761156409  L_0 = Transform_get_rotation_m1089223433(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_1 = Vector3_get_forward_m3509871422(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2761156409_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Quaternion_op_Multiply_m1160487389(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t329709361  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2761156409  Transform_get_rotation_m1089223433 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_rotation_m3828045280(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t2761156409  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t2761156409  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m4058359888 (Transform_t532597831 * __this, Quaternion_t2761156409  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_rotation_m3852742363(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m3828045280 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m3828045280_ftn) (Transform_t532597831 *, Quaternion_t2761156409 *);
	static Transform_INTERNAL_get_rotation_m3828045280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m3828045280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m3852742363 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m3852742363_ftn) (Transform_t532597831 *, Quaternion_t2761156409 *);
	static Transform_INTERNAL_set_rotation_m3852742363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m3852742363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t2761156409  Transform_get_localRotation_m2630156502 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Quaternion_t2761156409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2761156409  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localRotation_m376748379(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t2761156409  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t2761156409  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m83703377 (Transform_t532597831 * __this, Quaternion_t2761156409  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localRotation_m1591061908(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m376748379 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m376748379_ftn) (Transform_t532597831 *, Quaternion_t2761156409 *);
	static Transform_INTERNAL_get_localRotation_m376748379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m376748379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m1591061908 (Transform_t532597831 * __this, Quaternion_t2761156409 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m1591061908_ftn) (Transform_t532597831 *, Quaternion_t2761156409 *);
	static Transform_INTERNAL_set_localRotation_m1591061908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m1591061908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t329709361  Transform_get_localScale_m1865232606 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localScale_m3187990364(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3356822685 (Transform_t532597831 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localScale_m573024179(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m3187990364 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m3187990364_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_get_localScale_m3187990364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m3187990364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m573024179 (Transform_t532597831 * __this, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m573024179_ftn) (Transform_t532597831 *, Vector3_t329709361 *);
	static Transform_INTERNAL_set_localScale_m573024179_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m573024179_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t532597831 * Transform_get_parent_m1093437403 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Transform_t532597831 * V_0 = NULL;
	{
		Transform_t532597831 * L_0 = Transform_get_parentInternal_m1711913369(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Transform_t532597831 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t532597831 * Transform_get_parentInternal_m1711913369 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	typedef Transform_t532597831 * (*Transform_get_parentInternal_m1711913369_ftn) (Transform_t532597831 *);
	static Transform_get_parentInternal_m1711913369_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m1711913369_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	Transform_t532597831 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m1334336276 (Transform_t532597831 * __this, Transform_t532597831 * ___parent0, const RuntimeMethod* method)
{
	{
		Transform_t532597831 * L_0 = ___parent0;
		Transform_SetParent_m3755172863(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m3755172863 (Transform_t532597831 * __this, Transform_t532597831 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method)
{
	typedef void (*Transform_SetParent_m3755172863_ftn) (Transform_t532597831 *, Transform_t532597831 *, bool);
	static Transform_SetParent_m3755172863_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m3755172863_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t2375577114  Transform_get_worldToLocalMatrix_m3198838962 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2375577114  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m276834655(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t2375577114  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m276834655 (Transform_t532597831 * __this, Matrix4x4_t2375577114 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m276834655_ftn) (Transform_t532597831 *, Matrix4x4_t2375577114 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m276834655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m276834655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Transform_TransformPoint_m642078408 (Transform_t532597831 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformPoint_m3946760557(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m3946760557 (RuntimeObject * __this /* static, unused */, Transform_t532597831 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m3946760557_ftn) (Transform_t532597831 *, Vector3_t329709361 *, Vector3_t329709361 *);
	static Transform_INTERNAL_CALL_TransformPoint_m3946760557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m3946760557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Transform_InverseTransformPoint_m3184192574 (Transform_t532597831 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069 (RuntimeObject * __this /* static, unused */, Transform_t532597831 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069_ftn) (Transform_t532597831 *, Vector3_t329709361 *, Vector3_t329709361 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m3051766069_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2366507616 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Transform_get_childCount_m2366507616_ftn) (Transform_t532597831 *);
	static Transform_get_childCount_m2366507616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m2366507616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m3456631360 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	typedef void (*Transform_SetAsFirstSibling_m3456631360_ftn) (Transform_t532597831 *);
	static Transform_SetAsFirstSibling_m3456631360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m3456631360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m2999578724 (Transform_t532597831 * __this, Transform_t532597831 * ___parent0, const RuntimeMethod* method)
{
	typedef bool (*Transform_IsChildOf_m2999578724_ftn) (Transform_t532597831 *, Transform_t532597831 *);
	static Transform_IsChildOf_m2999578724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m2999578724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	bool retVal = _il2cpp_icall_func(__this, ___parent0);
	return retVal;
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  RuntimeObject* Transform_GetEnumerator_m1659425715 (Transform_t532597831 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m1659425715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t1378995202 * L_0 = (Enumerator_t1378995202 *)il2cpp_codegen_object_new(Enumerator_t1378995202_il2cpp_TypeInfo_var);
		Enumerator__ctor_m2582217870(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t532597831 * Transform_GetChild_m685835979 (Transform_t532597831 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef Transform_t532597831 * (*Transform_GetChild_m685835979_ftn) (Transform_t532597831 *, int32_t);
	static Transform_GetChild_m685835979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m685835979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	Transform_t532597831 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m2582217870 (Enumerator_t1378995202 * __this, Transform_t532597831 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		Transform_t532597831 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2284404590 (Enumerator_t1378995202 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Transform_t532597831 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t532597831 * L_2 = Transform_GetChild_m685835979(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2879026930 (Enumerator_t1378995202 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Transform_t532597831 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2366507616(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		goto IL_0027;
	}

IL_0027:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m2556373659 (Enumerator_t1378995202 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern "C"  bool SpriteAtlasManager_RequestAtlas_m1392089085 (RuntimeObject * __this /* static, unused */, String_t* ___tag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager_RequestAtlas_m1392089085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B3_0 = NULL;
	RequestAtlasCallback_t634135197 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	RequestAtlasCallback_t634135197 * G_B2_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t634135197 * L_0 = ((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t634135197 * L_1 = ((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		String_t* L_2 = ___tag0;
		Action_1_t1977832595 * L_3 = ((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_002a;
		}
	}
	{
		intptr_t L_4 = (intptr_t)SpriteAtlasManager_Register_m3782966546_RuntimeMethod_var;
		Action_1_t1977832595 * L_5 = (Action_1_t1977832595 *)il2cpp_codegen_object_new(Action_1_t1977832595_il2cpp_TypeInfo_var);
		Action_1__ctor_m4209780603(L_5, NULL, L_4, /*hidden argument*/Action_1__ctor_m4209780603_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var);
		((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_1(L_5);
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var);
		Action_1_t1977832595 * L_6 = ((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		NullCheck(G_B3_1);
		RequestAtlasCallback_Invoke_m186315368(G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0042;
	}

IL_003b:
	{
		V_0 = (bool)0;
		goto IL_0042;
	}

IL_0042:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern "C"  void SpriteAtlasManager_Register_m3782966546 (RuntimeObject * __this /* static, unused */, SpriteAtlas_t3138271588 * ___spriteAtlas0, const RuntimeMethod* method)
{
	typedef void (*SpriteAtlasManager_Register_m3782966546_ftn) (SpriteAtlas_t3138271588 *);
	static SpriteAtlasManager_Register_m3782966546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteAtlasManager_Register_m3782966546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)");
	_il2cpp_icall_func(___spriteAtlas0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern "C"  void SpriteAtlasManager__cctor_m2760532170 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager__cctor_m2760532170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SpriteAtlasManager_t524371920_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t524371920_il2cpp_TypeInfo_var))->set_atlasRequested_0((RequestAtlasCallback_t634135197 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestAtlasCallback_t634135197 (RequestAtlasCallback_t634135197 * __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, Il2CppMethodPointer);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___tag0' to native representation
	char* ____tag0_marshaled = NULL;
	____tag0_marshaled = il2cpp_codegen_marshal_string(___tag0);

	// Marshaling of parameter '___action1' to native representation
	Il2CppMethodPointer ____action1_marshaled = NULL;
	____action1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___action1));

	// Native function invocation
	il2cppPInvokeFunc(____tag0_marshaled, ____action1_marshaled);

	// Marshaling cleanup of parameter '___tag0' native representation
	il2cpp_codegen_marshal_free(____tag0_marshaled);
	____tag0_marshaled = NULL;

}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestAtlasCallback__ctor_m1110990128 (RequestAtlasCallback_t634135197 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m186315368 (RequestAtlasCallback_t634135197 * __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestAtlasCallback_Invoke_m186315368((RequestAtlasCallback_t634135197 *)__this->get_prev_9(),___tag0, ___action1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Action_1_t1977832595 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::BeginInvoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* RequestAtlasCallback_BeginInvoke_m4034554441 (RequestAtlasCallback_t634135197 * __this, String_t* ___tag0, Action_1_t1977832595 * ___action1, AsyncCallback_t869574496 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___tag0;
	__d_args[1] = ___action1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RequestAtlasCallback_EndInvoke_m3000351456 (RequestAtlasCallback_t634135197 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m1434751384 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m1434751384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppDomain_t3006601831 * G_B2_0 = NULL;
	AppDomain_t3006601831 * G_B1_0 = NULL;
	{
		AppDomain_t3006601831 * L_0 = AppDomain_get_CurrentDomain_m4287540733(NULL /*static, unused*/, /*hidden argument*/NULL);
		UnhandledExceptionEventHandler_t2340223587 * L_1 = ((UnhandledExceptionHandler_t1659926578_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t1659926578_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		intptr_t L_2 = (intptr_t)UnhandledExceptionHandler_HandleUnhandledException_m517656032_RuntimeMethod_var;
		UnhandledExceptionEventHandler_t2340223587 * L_3 = (UnhandledExceptionEventHandler_t2340223587 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t2340223587_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m4261131196(L_3, NULL, L_2, /*hidden argument*/NULL);
		((UnhandledExceptionHandler_t1659926578_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t1659926578_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		UnhandledExceptionEventHandler_t2340223587 * L_4 = ((UnhandledExceptionHandler_t1659926578_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t1659926578_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		NullCheck(G_B2_0);
		AppDomain_add_UnhandledException_m529960550(G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m517656032 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___sender0, UnhandledExceptionEventArgs_t4227701455 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m517656032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t2123675094 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t4227701455 * L_0 = ___args1;
		NullCheck(L_0);
		RuntimeObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m2966859680(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t2123675094 *)IsInstClass((RuntimeObject*)L_1, Exception_t2123675094_il2cpp_TypeInfo_var));
		Exception_t2123675094 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Exception_t2123675094 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m1818247004(NULL /*static, unused*/, _stringLiteral2415579295, L_3, /*hidden argument*/NULL);
		Exception_t2123675094 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Exception_GetType_m200229271(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		Exception_t2123675094 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_7);
		Exception_t2123675094 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_9);
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		goto IL_004b;
	}

IL_0041:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213(NULL /*static, unused*/, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m1818247004 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t2123675094 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m1818247004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t2123675094 * L_0 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogException_m1234897647(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t2123675094 * L_1 = ___e1;
		NullCheck(L_1);
		Exception_t2123675094 * L_2 = Exception_get_InnerException_m4268342947(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Exception_t2123675094 * L_3 = ___e1;
		NullCheck(L_3);
		Exception_t2123675094 * L_4 = Exception_get_InnerException_m4268342947(L_3, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m1818247004(NULL /*static, unused*/, _stringLiteral3822892827, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213_ftn) (String_t*, String_t*, String_t*);
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3380826213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)");
	_il2cpp_icall_func(___managedExceptionType0, ___managedExceptionMessage1, ___managedExceptionStack2);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
