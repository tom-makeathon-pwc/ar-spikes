﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t2444501850;
// System.Type
struct Type_t;
// System.Attribute
struct Attribute_t3852256153;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t67651563;
// System.String
struct String_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2429328822;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t2576915489;
// UnityEngine.Application/LowMemoryCallback
struct LowMemoryCallback_t2129875537;
// UnityEngine.Application/LogCallback
struct LogCallback_t657888911;
// UnityEngine.Events.UnityAction
struct UnityAction_t1298730314;
// System.IAsyncResult
struct IAsyncResult_t614244269;
// System.AsyncCallback
struct AsyncCallback_t869574496;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t67027751;
// System.Action`1<System.Object>
struct Action_1_t3509626261;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t3308188911;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3164505761;
// System.Type[]
struct TypeU5BU5D_t1460120061;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t329231522;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t185548372;
// System.Object[]
struct ObjectU5BU5D_t1568665923;
// UnityEngine.RequireComponent
struct RequireComponent_t4152270991;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t2771548650;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t1081793821;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t814488931;
// UnityEngine.Behaviour
struct Behaviour_t2441856611;
// UnityEngine.Component
struct Component_t789413749;
// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t1659112717;
// UnityEngine.Camera
struct Camera_t989002943;
// UnityEngine.RenderTexture
struct RenderTexture_t3361574884;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3897890150;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t2620733104;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2673047760;
// UnityEngine.GameObject
struct GameObject_t1318052361;
// UnityEngine.Object
struct Object_t1970767703;
// UnityEngine.Transform
struct Transform_t532597831;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t599864163;
// UnityEngine.Coroutine
struct Coroutine_t4212313979;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3362187334;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t1456955310;
// UnityEngine.CSSLayout.CSSMeasureFunc
struct CSSMeasureFunc_t2617999110;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct Dictionary_2_t1695300448;
// System.WeakReference
struct WeakReference_t1069295502;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t1001102904;
// UnityEngine.CullingGroup
struct CullingGroup_t635124431;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t3421641282;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t1639495614;
// UnityEngine.ILogger
struct ILogger_t589268963;
// System.Exception
struct Exception_t2123675094;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t2136690766;
// UnityEngine.Logger
struct Logger_t2034979157;
// UnityEngine.ILogHandler
struct ILogHandler_t2015281511;
// UnityEngine.Display
struct Display_t882507400;
// System.IntPtr[]
struct IntPtrU5BU5D_t2297631236;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t850082040;
// UnityEngine.RectTransform
struct RectTransform_t15861704;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t3186847956;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t3782853021;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.ArgumentNullException
struct ArgumentNullException_t970256261;
// System.Delegate
struct Delegate_t1563516729;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t1532986675;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1231629294;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t3593303435;
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t2877392878;
// System.Predicate`1<System.Object>
struct Predicate_1_t3764605111;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t4109324709;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t701569646;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1257714207;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t2204896975;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1392551900;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t28204005;
// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t3765088790;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t4199264408;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t98604400;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1575920019;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1952737969;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t1068164621;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t3617968793;
// System.Reflection.Binder
struct Binder_t3602727638;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3541603165;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2372523953;
// UnityEngine.Experimental.Rendering.IRenderPipeline
struct IRenderPipeline_t1567407966;
// UnityEngine.Experimental.Rendering.IRenderPipelineAsset
struct IRenderPipelineAsset_t735150870;
// UnityEngine.Gradient
struct Gradient_t1395232743;
// UnityEngine.GUIElement
struct GUIElement_t1520723180;
// UnityEngine.GUILayer
struct GUILayer_t2694382279;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t30125744;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t69305138;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t2686620401;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t3593402498;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t3465613458;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t2999868044;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t163380436;
// UnityEngine.Light
struct Light_t3883945566;
// UnityEngine.LightProbes
struct LightProbes_t380083648;
// UnityEngine.Rendering.SphericalHarmonicsL2[]
struct SphericalHarmonicsL2U5BU5D_t2505359721;
// System.IO.Stream
struct Stream_t3046340190;
// System.ArgumentException
struct ArgumentException_t3637419113;
// System.Byte[]
struct ByteU5BU5D_t3287329517;
// UnityEngine.Material
struct Material_t2712136762;
// UnityEngine.Texture
struct Texture_t2838694469;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3417007309;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3921978895;
// UnityEngine.Mesh
struct Mesh_t1621212487;
// System.Int32[]
struct Int32U5BU5D_t3565237794;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t309455265;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t2564234830;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1392199097;
// UnityEngine.Color32[]
struct Color32U5BU5D_t1505762612;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t140159775;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t191085541;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t2598598263;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t2867512982;
// UnityEngine.MeshFilter
struct MeshFilter_t1334808991;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3829899482;
// System.Collections.IEnumerator
struct IEnumerator_t1842762036;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1250540745;
// System.WeakReference[]
struct WeakReferenceU5BU5D_t3215569531;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t1193243513;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1922660610;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.WeakReference,System.Collections.DictionaryEntry>
struct Transform_1_t3476416000;
// System.Collections.IDictionary
struct IDictionary_t2298573854;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t788527952;
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t3849682566;
// UnityEngine.Gyroscope
struct Gyroscope_t416198001;
// UnityEngine.Component[]
struct ComponentU5BU5D_t259396376;
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t56197584;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t3106130354;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1251203158;
// System.Char[]
struct CharU5BU5D_t83643201;
// System.Boolean[]
struct BooleanU5BU5D_t2577425931;
// System.Void
struct Void_t2642135423;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t4006882931;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t2642535036;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t2084452525;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t2712935431;
// UnityEngine.Display[]
struct DisplayU5BU5D_t3818835545;
// System.DelegateData
struct DelegateData_t975501551;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1960139725;
// System.Reflection.MemberFilter
struct MemberFilter_t3962658540;
// System.String[]
struct StringU5BU5D_t369357837;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3527255582;

extern RuntimeClass* Application_t1423836661_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLowMemory_m2711201767_MetadataUsageId;
extern const uint32_t Application_CallLogCallback_m2863196351_MetadataUsageId;
extern const uint32_t Application_InvokeOnBeforeRender_m2639803043_MetadataUsageId;
extern RuntimeClass* LogType_t3958793426_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m601874442_MetadataUsageId;
extern RuntimeClass* Action_1_t67027751_il2cpp_TypeInfo_var;
extern const uint32_t AssetBundleCreateRequest_t1484565475_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleCreateRequest_t1484565475_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t4006347538_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t4006347538_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t1227466744_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t1227466744_com_FromNativeMethodDefinition_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m3655672843_RuntimeMethod_var;
extern const uint32_t AsyncOperation_InvokeCompletionEvent_m15538063_MetadataUsageId;
extern const RuntimeType* MonoBehaviour_t3829899482_0_0_0_var;
extern const RuntimeType* DisallowMultipleComponent_t1081793821_0_0_0_var;
extern RuntimeClass* Stack_1_t3308188911_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Stack_1__ctor_m1150277029_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m1339590981_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m2893246736_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m41169841_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3962214426_MetadataUsageId;
extern const RuntimeType* RequireComponent_t4152270991_0_0_0_var;
extern RuntimeClass* RequireComponentU5BU5D_t1251203158_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t329231522_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3649843648_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2325233572_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m2346909423_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m1719255508_MetadataUsageId;
extern const RuntimeType* ExecuteInEditMode_t814488931_0_0_0_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m989208379_MetadataUsageId;
extern RuntimeClass* AttributeHelperEngine_t1954971056_il2cpp_TypeInfo_var;
extern const RuntimeMethod* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2771548650_m2442461925_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m76839880_MetadataUsageId;
extern RuntimeClass* DisallowMultipleComponentU5BU5D_t56197584_il2cpp_TypeInfo_var;
extern RuntimeClass* ExecuteInEditModeU5BU5D_t3106130354_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m3903595114_MetadataUsageId;
extern RuntimeClass* Vector3_t329709361_il2cpp_TypeInfo_var;
extern const uint32_t Bounds__ctor_m656227705_MetadataUsageId;
extern RuntimeClass* Bounds_t197583170_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m2379144132_MetadataUsageId;
extern const uint32_t Bounds_get_size_m4147319483_MetadataUsageId;
extern const uint32_t Bounds_set_size_m4242979136_MetadataUsageId;
extern const uint32_t Bounds_get_min_m3422049026_MetadataUsageId;
extern const uint32_t Bounds_get_max_m2087102622_MetadataUsageId;
extern const uint32_t Bounds_op_Equality_m3048700048_MetadataUsageId;
extern const uint32_t Bounds_SetMinMax_m261817242_MetadataUsageId;
extern const uint32_t Bounds_Encapsulate_m2628864493_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1971303795;
extern const uint32_t Bounds_ToString_m1643044461_MetadataUsageId;
extern RuntimeClass* Camera_t989002943_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m1310166745_MetadataUsageId;
extern const uint32_t Camera_FireOnPreRender_m965332065_MetadataUsageId;
extern const uint32_t Camera_FireOnPostRender_m1302122984_MetadataUsageId;
extern RuntimeClass* Single_t1863352746_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3336280268;
extern const uint32_t Color_ToString_m2177171216_MetadataUsageId;
extern RuntimeClass* Color_t460381780_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m1644359303_MetadataUsageId;
extern RuntimeClass* Vector4_t380635127_il2cpp_TypeInfo_var;
extern const uint32_t Color_op_Equality_m3738865736_MetadataUsageId;
extern RuntimeClass* Mathf_t2081007568_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m544019243_MetadataUsageId;
extern const uint32_t Color32_op_Implicit_m308041032_MetadataUsageId;
extern RuntimeClass* Byte_t2815932036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral141853172;
extern const uint32_t Color32_ToString_m2492342775_MetadataUsageId;
extern RuntimeClass* Object_t1970767703_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m3701326763_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureMode_t483571173_il2cpp_TypeInfo_var;
extern const uint32_t CSSMeasureFunc_BeginInvoke_m1450775633_MetadataUsageId;
extern RuntimeClass* Native_t4163284866_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureFunc_t2617999110_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1067134071_RuntimeMethod_var;
extern const uint32_t Native_CSSNodeGetMeasureFunc_m1865278247_MetadataUsageId;
extern const uint32_t Native_CSSNodeMeasureInvoke_m2427172143_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1695300448_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m499092701_RuntimeMethod_var;
extern const uint32_t Native__cctor_m3474275384_MetadataUsageId;
extern RuntimeClass* StateChanged_t3421641282_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t635124431_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_t635124431_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_Finalize_m915509337_MetadataUsageId;
extern RuntimeClass* CullingGroupEvent_t2298849179_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_SendEvents_m4154025933_MetadataUsageId;
extern const uint32_t StateChanged_BeginInvoke_m381182532_MetadataUsageId;
extern RuntimeClass* Debug_t114115908_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_unityLogger_m1064451884_MetadataUsageId;
extern RuntimeClass* ILogger_t589268963_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m1645010884_MetadataUsageId;
extern const uint32_t Debug_LogError_m627890074_MetadataUsageId;
extern const uint32_t Debug_LogError_m1993805588_MetadataUsageId;
extern RuntimeClass* ILogHandler_t2015281511_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogErrorFormat_m2541379551_MetadataUsageId;
extern const uint32_t Debug_LogException_m1234897647_MetadataUsageId;
extern const uint32_t Debug_LogException_m455398078_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m3875502493_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m2821516339_MetadataUsageId;
extern const uint32_t Debug_LogWarningFormat_m107336790_MetadataUsageId;
extern RuntimeClass* DebugLogHandler_t2136690766_il2cpp_TypeInfo_var;
extern RuntimeClass* Logger_t2034979157_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m3310780854_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m2978450909_MetadataUsageId;
extern RuntimeClass* Display_t882507400_il2cpp_TypeInfo_var;
extern const uint32_t Display_get_renderingWidth_m2172863445_MetadataUsageId;
extern const uint32_t Display_get_renderingHeight_m2111578961_MetadataUsageId;
extern const uint32_t Display_get_systemWidth_m2156533738_MetadataUsageId;
extern const uint32_t Display_get_systemHeight_m1848489818_MetadataUsageId;
extern const uint32_t Display_RelativeMouseAt_m4179181397_MetadataUsageId;
extern RuntimeClass* DisplayU5BU5D_t3818835545_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m2778731890_MetadataUsageId;
extern const uint32_t Display_FireDisplaysUpdated_m1903554954_MetadataUsageId;
extern const uint32_t Display__cctor_m3674733810_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1862748563;
extern Il2CppCodeGenString* _stringLiteral1500987161;
extern Il2CppCodeGenString* _stringLiteral1642176685;
extern Il2CppCodeGenString* _stringLiteral1188916203;
extern Il2CppCodeGenString* _stringLiteral4225161693;
extern Il2CppCodeGenString* _stringLiteral3777595284;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m2499549447_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t970256261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3934963589;
extern Il2CppCodeGenString* _stringLiteral1116345114;
extern const uint32_t BaseInvokableCall__ctor_m2684183300_MetadataUsageId;
extern const uint32_t BaseInvokableCall_AllowInvoke_m3200287249_MetadataUsageId;
extern const RuntimeType* UnityAction_t1298730314_0_0_0_var;
extern RuntimeClass* UnityAction_t1298730314_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m3813870726_MetadataUsageId;
extern const uint32_t InvokableCall_add_Delegate_m3264530938_MetadataUsageId;
extern const uint32_t InvokableCall_remove_Delegate_m2316392508_MetadataUsageId;
extern RuntimeClass* List_1_t3593303435_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3136659381_RuntimeMethod_var;
extern const uint32_t InvokableCallList__ctor_m3375537592_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m166741303_RuntimeMethod_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m1525842468_MetadataUsageId;
extern const uint32_t InvokableCallList_AddListener_m1729791600_MetadataUsageId;
extern RuntimeClass* Predicate_1_t2877392878_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m532699242_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m4034068867_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m2599341048_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m2108898755_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m3245126275_RuntimeMethod_var;
extern const uint32_t InvokableCallList_RemoveListener_m2959324581_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m2582543520_RuntimeMethod_var;
extern const uint32_t InvokableCallList_ClearPersistent_m3370527310_MetadataUsageId;
extern const RuntimeMethod* List_1_AddRange_m1581173574_RuntimeMethod_var;
extern const uint32_t InvokableCallList_PrepareInvoke_m3324959179_MetadataUsageId;
extern RuntimeClass* ArgumentCache_t3186847956_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m1335226983_MetadataUsageId;
extern const uint32_t PersistentCall_IsValid_m3600404012_MetadataUsageId;
extern RuntimeClass* CachedInvokableCall_1_t1392551900_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t28204005_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t3765088790_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t98604400_il2cpp_TypeInfo_var;
extern RuntimeClass* InvokableCall_t1532986675_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m1503881081_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m1923180212_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m3388188837_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m3785938189_RuntimeMethod_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m1034035855_MetadataUsageId;
extern const RuntimeType* Object_t1970767703_0_0_0_var;
extern const RuntimeType* CachedInvokableCall_1_t269288053_0_0_0_var;
extern const RuntimeType* MethodInfo_t_0_0_0_var;
extern RuntimeClass* BaseInvokableCall_t3782853021_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m2309820747_MetadataUsageId;
extern RuntimeClass* List_1_t1068164621_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m852503502_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup__ctor_m3543885354_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m2460221054_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2730847451_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4259834011_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2021464298_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup_Initialize_m3520909384_MetadataUsageId;
extern const uint32_t UnityEvent_FindMethod_Impl_m2170766220_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m1503760066_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m3771139711_MetadataUsageId;
extern const uint32_t UnityEvent_Invoke_m279815371_MetadataUsageId;
extern RuntimeClass* InvokableCallList_t1231629294_il2cpp_TypeInfo_var;
extern RuntimeClass* PersistentCallGroup_t1952737969_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m3145387590_MetadataUsageId;
extern const uint32_t UnityEventBase_FindMethod_m2595158952_MetadataUsageId;
extern const RuntimeType* Single_t1863352746_0_0_0_var;
extern const RuntimeType* Int32_t499004851_0_0_0_var;
extern const RuntimeType* Boolean_t569405246_0_0_0_var;
extern const RuntimeType* String_t_0_0_0_var;
extern const uint32_t UnityEventBase_FindMethod_m1231245736_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2998835488;
extern const uint32_t UnityEventBase_ToString_m702212230_MetadataUsageId;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const uint32_t UnityEventBase_GetValidMethodInfo_m3893544593_MetadataUsageId;
extern RuntimeClass* RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_get_currentPipeline_m2019186464_MetadataUsageId;
extern const uint32_t RenderPipelineManager_set_currentPipeline_m1978623761_MetadataUsageId;
extern RuntimeClass* IRenderPipelineAsset_t735150870_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_CleanupRenderPipeline_m185655714_MetadataUsageId;
extern RuntimeClass* IRenderPipeline_t1567407966_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_DoRenderLoop_Internal_m1110218575_MetadataUsageId;
extern const uint32_t RenderPipelineManager_PrepareRenderPipeline_m3314607428_MetadataUsageId;
extern const uint32_t GameObject__ctor_m407434703_MetadataUsageId;
extern const uint32_t GameObject__ctor_m2351111389_MetadataUsageId;
extern const uint32_t GameObject__ctor_m694740885_MetadataUsageId;
extern RuntimeClass* Input_t2963215744_il2cpp_TypeInfo_var;
extern const uint32_t Input_get_mousePosition_m354171928_MetadataUsageId;
extern const uint32_t Input_get_mouseScrollDelta_m1557855344_MetadataUsageId;
extern const uint32_t Input_GetTouch_m2440883173_MetadataUsageId;
extern const uint32_t Input_get_compositionCursorPos_m3427021554_MetadataUsageId;
extern const uint32_t Input_set_compositionCursorPos_m2684519623_MetadataUsageId;
extern const uint32_t Input__cctor_m3780596079_MetadataUsageId;
extern RuntimeClass* DefaultValueAttribute_t3593402498_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m62841503_MetadataUsageId;
extern RuntimeClass* LocalNotification_t2999868044_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m4222515013_MetadataUsageId;
extern const uint32_t LightProbes__ctor_m1000816120_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral652294219;
extern const uint32_t Logger_GetString_m1753770121_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral843812391;
extern const uint32_t Logger_Log_m3920909206_MetadataUsageId;
extern const uint32_t Logger_Log_m591675774_MetadataUsageId;
extern const uint32_t Logger_LogFormat_m3050159729_MetadataUsageId;
extern const uint32_t Logger_LogException_m106933759_MetadataUsageId;
extern RuntimeClass* ArgumentException_t3637419113_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2945614662;
extern Il2CppCodeGenString* _stringLiteral1443217705;
extern Il2CppCodeGenString* _stringLiteral2025743269;
extern Il2CppCodeGenString* _stringLiteral4259981313;
extern const uint32_t ManagedStreamHelpers_ValidateLoadFromStream_m730854658_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4070250858;
extern Il2CppCodeGenString* _stringLiteral768326453;
extern const uint32_t ManagedStreamHelpers_ManagedStreamRead_m877885100_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamSeek_m1677488971_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamLength_m2106065483_MetadataUsageId;
extern const uint32_t Material__ctor_m1876270996_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1603114518;
extern const uint32_t Material_set_color_m825231558_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2338579479;
extern const uint32_t Material_get_mainTexture_m1492267632_MetadataUsageId;
extern const uint32_t Mathf_Lerp_m1308516861_MetadataUsageId;
extern const uint32_t Mathf_Approximately_m1485428512_MetadataUsageId;
extern const uint32_t Mathf_SmoothDamp_m724208596_MetadataUsageId;
extern const uint32_t Mathf_Repeat_m2357693841_MetadataUsageId;
extern const uint32_t Mathf_InverseLerp_m2415857371_MetadataUsageId;
extern RuntimeClass* MathfInternal_t1363288871_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m1374701236_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t2375577114_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_TRS_m2667050262_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1189015226;
extern const uint32_t Matrix4x4_get_Item_m592394008_MetadataUsageId;
extern const uint32_t Matrix4x4_set_Item_m942837760_MetadataUsageId;
extern const uint32_t Matrix4x4_Equals_m2605048655_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2723610844;
extern const uint32_t Matrix4x4_GetColumn_m1167652957_MetadataUsageId;
extern const uint32_t Matrix4x4_get_identity_m386701337_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4045066072;
extern const uint32_t Matrix4x4_ToString_m782477224_MetadataUsageId;
extern const uint32_t Matrix4x4__cctor_m590356860_MetadataUsageId;
extern const uint32_t Mesh__ctor_m1598271010_MetadataUsageId;
extern const RuntimeMethod* Mesh_SafeLength_TisInt32_t499004851_m2496825826_RuntimeMethod_var;
extern const uint32_t Mesh_SetTriangles_m1728167629_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1098436485;
extern Il2CppCodeGenString* _stringLiteral3386726826;
extern const uint32_t Mesh_GetUVChannel_m414321819_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3224309003;
extern Il2CppCodeGenString* _stringLiteral1925967687;
extern const uint32_t Mesh_DefaultDimensionForChannel_m3431474595_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429_RuntimeMethod_var;
extern const uint32_t Mesh_get_vertices_m4085462919_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940_RuntimeMethod_var;
extern const uint32_t Mesh_set_vertices_m1491965711_MetadataUsageId;
extern const uint32_t Mesh_get_normals_m435100974_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490_RuntimeMethod_var;
extern const uint32_t Mesh_get_tangents_m1380468074_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_RuntimeMethod_var;
extern const uint32_t Mesh_get_uv_m1614926589_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259_RuntimeMethod_var;
extern const uint32_t Mesh_set_uv_m8989046_MetadataUsageId;
extern const uint32_t Mesh_get_uv2_m1108875579_MetadataUsageId;
extern const uint32_t Mesh_get_uv3_m3194937612_MetadataUsageId;
extern const uint32_t Mesh_get_uv4_m2850090743_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690_RuntimeMethod_var;
extern const uint32_t Mesh_get_colors32_m1184769428_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisVector3_t329709361_m1783659180_RuntimeMethod_var;
extern const uint32_t Mesh_SetVertices_m669673273_MetadataUsageId;
extern const uint32_t Mesh_SetNormals_m800993536_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisVector4_t380635127_m429250604_RuntimeMethod_var;
extern const uint32_t Mesh_SetTangents_m2522372090_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440_RuntimeMethod_var;
extern const uint32_t Mesh_SetColors_m2158139618_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797_RuntimeMethod_var;
extern const uint32_t Mesh_SetUVs_m926587831_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1639121846;
extern const uint32_t Mesh_PrintErrorCantAccessIndices_m23294446_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral71053757;
extern Il2CppCodeGenString* _stringLiteral1342767652;
extern Il2CppCodeGenString* _stringLiteral2391538392;
extern const uint32_t Mesh_CheckCanAccessSubmesh_m3483679341_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t3565237794_il2cpp_TypeInfo_var;
extern const uint32_t Mesh_GetIndices_m1110104194_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4263497495;
extern const uint32_t Mesh_RecalculateBounds_m1630268163_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2318156272;
extern const uint32_t Mesh_RecalculateNormals_m1089037679_MetadataUsageId;

struct KeyframeU5BU5D_t2576915489;
struct ObjectU5BU5D_t1568665923;
struct TypeU5BU5D_t1460120061;
struct RequireComponentU5BU5D_t1251203158;
struct DisallowMultipleComponentU5BU5D_t56197584;
struct ExecuteInEditModeU5BU5D_t3106130354;
struct CameraU5BU5D_t3897890150;
struct IntPtrU5BU5D_t2297631236;
struct DisplayU5BU5D_t3818835545;
struct ParameterInfoU5BU5D_t4074997356;
struct ParameterModifierU5BU5D_t3541603165;
struct SphericalHarmonicsL2U5BU5D_t2505359721;
struct ByteU5BU5D_t3287329517;
struct Int32U5BU5D_t3565237794;
struct Vector3U5BU5D_t974944492;
struct Vector4U5BU5D_t2564234830;
struct Vector2U5BU5D_t1392199097;
struct Color32U5BU5D_t1505762612;


#ifndef U3CMODULEU3E_T1094827702_H
#define U3CMODULEU3E_T1094827702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1094827702 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1094827702_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef YIELDINSTRUCTION_T3362187334_H
#define YIELDINSTRUCTION_T3362187334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3362187334  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3362187334_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3362187334_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3362187334_H
#ifndef NATIVE_T4163284866_H
#define NATIVE_T4163284866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.Native
struct  Native_t4163284866  : public RuntimeObject
{
public:

public:
};

struct Native_t4163284866_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference> UnityEngine.CSSLayout.Native::s_MeasureFunctions
	Dictionary_2_t1695300448 * ___s_MeasureFunctions_0;

public:
	inline static int32_t get_offset_of_s_MeasureFunctions_0() { return static_cast<int32_t>(offsetof(Native_t4163284866_StaticFields, ___s_MeasureFunctions_0)); }
	inline Dictionary_2_t1695300448 * get_s_MeasureFunctions_0() const { return ___s_MeasureFunctions_0; }
	inline Dictionary_2_t1695300448 ** get_address_of_s_MeasureFunctions_0() { return &___s_MeasureFunctions_0; }
	inline void set_s_MeasureFunctions_0(Dictionary_2_t1695300448 * value)
	{
		___s_MeasureFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MeasureFunctions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVE_T4163284866_H
#ifndef DICTIONARY_2_T1695300448_H
#define DICTIONARY_2_T1695300448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct  Dictionary_2_t1695300448  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3565237794* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t1250540745* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	IntPtrU5BU5D_t2297631236* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	WeakReferenceU5BU5D_t3215569531* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t1922660610 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___table_4)); }
	inline Int32U5BU5D_t3565237794* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3565237794** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3565237794* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___linkSlots_5)); }
	inline LinkU5BU5D_t1250540745* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t1250540745** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t1250540745* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___keySlots_6)); }
	inline IntPtrU5BU5D_t2297631236* get_keySlots_6() const { return ___keySlots_6; }
	inline IntPtrU5BU5D_t2297631236** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(IntPtrU5BU5D_t2297631236* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___valueSlots_7)); }
	inline WeakReferenceU5BU5D_t3215569531* get_valueSlots_7() const { return ___valueSlots_7; }
	inline WeakReferenceU5BU5D_t3215569531** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(WeakReferenceU5BU5D_t3215569531* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___serialization_info_13)); }
	inline SerializationInfo_t1922660610 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t1922660610 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t1922660610 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1695300448_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3476416000 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1695300448_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3476416000 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3476416000 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3476416000 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1695300448_H
#ifndef CURSOR_T2094791969_H
#define CURSOR_T2094791969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Cursor
struct  Cursor_t2094791969  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSOR_T2094791969_H
#ifndef CUSTOMYIELDINSTRUCTION_T1639495614_H
#define CUSTOMYIELDINSTRUCTION_T1639495614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1639495614  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1639495614_H
#ifndef DEBUG_T114115908_H
#define DEBUG_T114115908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Debug
struct  Debug_t114115908  : public RuntimeObject
{
public:

public:
};

struct Debug_t114115908_StaticFields
{
public:
	// UnityEngine.ILogger UnityEngine.Debug::s_Logger
	RuntimeObject* ___s_Logger_0;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Debug_t114115908_StaticFields, ___s_Logger_0)); }
	inline RuntimeObject* get_s_Logger_0() const { return ___s_Logger_0; }
	inline RuntimeObject** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(RuntimeObject* value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T114115908_H
#ifndef EXCEPTION_T2123675094_H
#define EXCEPTION_T2123675094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t2123675094  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t2297631236* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t2123675094 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t2297631236* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t2297631236** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t2297631236* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___inner_exception_1)); }
	inline Exception_t2123675094 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t2123675094 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t2123675094 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t2123675094, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T2123675094_H
#ifndef DEBUGLOGHANDLER_T2136690766_H
#define DEBUGLOGHANDLER_T2136690766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DebugLogHandler
struct  DebugLogHandler_t2136690766  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGHANDLER_T2136690766_H
#ifndef ARGUMENTCACHE_T3186847956_H
#define ARGUMENTCACHE_T3186847956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.ArgumentCache
struct  ArgumentCache_t3186847956  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.ArgumentCache::m_ObjectArgument
	Object_t1970767703 * ___m_ObjectArgument_0;
	// System.String UnityEngine.Events.ArgumentCache::m_ObjectArgumentAssemblyTypeName
	String_t* ___m_ObjectArgumentAssemblyTypeName_1;
	// System.Int32 UnityEngine.Events.ArgumentCache::m_IntArgument
	int32_t ___m_IntArgument_2;
	// System.Single UnityEngine.Events.ArgumentCache::m_FloatArgument
	float ___m_FloatArgument_3;
	// System.String UnityEngine.Events.ArgumentCache::m_StringArgument
	String_t* ___m_StringArgument_4;
	// System.Boolean UnityEngine.Events.ArgumentCache::m_BoolArgument
	bool ___m_BoolArgument_5;

public:
	inline static int32_t get_offset_of_m_ObjectArgument_0() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_ObjectArgument_0)); }
	inline Object_t1970767703 * get_m_ObjectArgument_0() const { return ___m_ObjectArgument_0; }
	inline Object_t1970767703 ** get_address_of_m_ObjectArgument_0() { return &___m_ObjectArgument_0; }
	inline void set_m_ObjectArgument_0(Object_t1970767703 * value)
	{
		___m_ObjectArgument_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgument_0), value);
	}

	inline static int32_t get_offset_of_m_ObjectArgumentAssemblyTypeName_1() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_ObjectArgumentAssemblyTypeName_1)); }
	inline String_t* get_m_ObjectArgumentAssemblyTypeName_1() const { return ___m_ObjectArgumentAssemblyTypeName_1; }
	inline String_t** get_address_of_m_ObjectArgumentAssemblyTypeName_1() { return &___m_ObjectArgumentAssemblyTypeName_1; }
	inline void set_m_ObjectArgumentAssemblyTypeName_1(String_t* value)
	{
		___m_ObjectArgumentAssemblyTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgumentAssemblyTypeName_1), value);
	}

	inline static int32_t get_offset_of_m_IntArgument_2() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_IntArgument_2)); }
	inline int32_t get_m_IntArgument_2() const { return ___m_IntArgument_2; }
	inline int32_t* get_address_of_m_IntArgument_2() { return &___m_IntArgument_2; }
	inline void set_m_IntArgument_2(int32_t value)
	{
		___m_IntArgument_2 = value;
	}

	inline static int32_t get_offset_of_m_FloatArgument_3() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_FloatArgument_3)); }
	inline float get_m_FloatArgument_3() const { return ___m_FloatArgument_3; }
	inline float* get_address_of_m_FloatArgument_3() { return &___m_FloatArgument_3; }
	inline void set_m_FloatArgument_3(float value)
	{
		___m_FloatArgument_3 = value;
	}

	inline static int32_t get_offset_of_m_StringArgument_4() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_StringArgument_4)); }
	inline String_t* get_m_StringArgument_4() const { return ___m_StringArgument_4; }
	inline String_t** get_address_of_m_StringArgument_4() { return &___m_StringArgument_4; }
	inline void set_m_StringArgument_4(String_t* value)
	{
		___m_StringArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringArgument_4), value);
	}

	inline static int32_t get_offset_of_m_BoolArgument_5() { return static_cast<int32_t>(offsetof(ArgumentCache_t3186847956, ___m_BoolArgument_5)); }
	inline bool get_m_BoolArgument_5() const { return ___m_BoolArgument_5; }
	inline bool* get_address_of_m_BoolArgument_5() { return &___m_BoolArgument_5; }
	inline void set_m_BoolArgument_5(bool value)
	{
		___m_BoolArgument_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTCACHE_T3186847956_H
#ifndef INVOKABLECALLLIST_T1231629294_H
#define INVOKABLECALLLIST_T1231629294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCallList
struct  InvokableCallList_t1231629294  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_PersistentCalls
	List_1_t3593303435 * ___m_PersistentCalls_0;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_RuntimeCalls
	List_1_t3593303435 * ___m_RuntimeCalls_1;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_ExecutingCalls
	List_1_t3593303435 * ___m_ExecutingCalls_2;
	// System.Boolean UnityEngine.Events.InvokableCallList::m_NeedsUpdate
	bool ___m_NeedsUpdate_3;

public:
	inline static int32_t get_offset_of_m_PersistentCalls_0() { return static_cast<int32_t>(offsetof(InvokableCallList_t1231629294, ___m_PersistentCalls_0)); }
	inline List_1_t3593303435 * get_m_PersistentCalls_0() const { return ___m_PersistentCalls_0; }
	inline List_1_t3593303435 ** get_address_of_m_PersistentCalls_0() { return &___m_PersistentCalls_0; }
	inline void set_m_PersistentCalls_0(List_1_t3593303435 * value)
	{
		___m_PersistentCalls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_0), value);
	}

	inline static int32_t get_offset_of_m_RuntimeCalls_1() { return static_cast<int32_t>(offsetof(InvokableCallList_t1231629294, ___m_RuntimeCalls_1)); }
	inline List_1_t3593303435 * get_m_RuntimeCalls_1() const { return ___m_RuntimeCalls_1; }
	inline List_1_t3593303435 ** get_address_of_m_RuntimeCalls_1() { return &___m_RuntimeCalls_1; }
	inline void set_m_RuntimeCalls_1(List_1_t3593303435 * value)
	{
		___m_RuntimeCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_RuntimeCalls_1), value);
	}

	inline static int32_t get_offset_of_m_ExecutingCalls_2() { return static_cast<int32_t>(offsetof(InvokableCallList_t1231629294, ___m_ExecutingCalls_2)); }
	inline List_1_t3593303435 * get_m_ExecutingCalls_2() const { return ___m_ExecutingCalls_2; }
	inline List_1_t3593303435 ** get_address_of_m_ExecutingCalls_2() { return &___m_ExecutingCalls_2; }
	inline void set_m_ExecutingCalls_2(List_1_t3593303435 * value)
	{
		___m_ExecutingCalls_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutingCalls_2), value);
	}

	inline static int32_t get_offset_of_m_NeedsUpdate_3() { return static_cast<int32_t>(offsetof(InvokableCallList_t1231629294, ___m_NeedsUpdate_3)); }
	inline bool get_m_NeedsUpdate_3() const { return ___m_NeedsUpdate_3; }
	inline bool* get_address_of_m_NeedsUpdate_3() { return &___m_NeedsUpdate_3; }
	inline void set_m_NeedsUpdate_3(bool value)
	{
		___m_NeedsUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALLLIST_T1231629294_H
#ifndef LIST_1_T3593303435_H
#define LIST_1_T3593303435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct  List_1_t3593303435  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BaseInvokableCallU5BU5D_t788527952* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3593303435, ____items_1)); }
	inline BaseInvokableCallU5BU5D_t788527952* get__items_1() const { return ____items_1; }
	inline BaseInvokableCallU5BU5D_t788527952** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BaseInvokableCallU5BU5D_t788527952* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3593303435, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3593303435, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3593303435_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	BaseInvokableCallU5BU5D_t788527952* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3593303435_StaticFields, ___EmptyArray_4)); }
	inline BaseInvokableCallU5BU5D_t788527952* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline BaseInvokableCallU5BU5D_t788527952** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(BaseInvokableCallU5BU5D_t788527952* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3593303435_H
#ifndef UNITYEVENTBASE_T2204896975_H
#define UNITYEVENTBASE_T2204896975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t2204896975  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t1231629294 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t1952737969 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_Calls_0)); }
	inline InvokableCallList_t1231629294 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t1231629294 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t1231629294 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t1952737969 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t1952737969 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t1952737969 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t2204896975, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T2204896975_H
#ifndef PERSISTENTCALLGROUP_T1952737969_H
#define PERSISTENTCALLGROUP_T1952737969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCallGroup
struct  PersistentCallGroup_t1952737969  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall> UnityEngine.Events.PersistentCallGroup::m_Calls
	List_1_t1068164621 * ___m_Calls_0;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(PersistentCallGroup_t1952737969, ___m_Calls_0)); }
	inline List_1_t1068164621 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline List_1_t1068164621 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(List_1_t1068164621 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALLGROUP_T1952737969_H
#ifndef LIST_1_T1068164621_H
#define LIST_1_T1068164621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct  List_1_t1068164621  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PersistentCallU5BU5D_t3849682566* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1068164621, ____items_1)); }
	inline PersistentCallU5BU5D_t3849682566* get__items_1() const { return ____items_1; }
	inline PersistentCallU5BU5D_t3849682566** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PersistentCallU5BU5D_t3849682566* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1068164621, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1068164621, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1068164621_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PersistentCallU5BU5D_t3849682566* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1068164621_StaticFields, ___EmptyArray_4)); }
	inline PersistentCallU5BU5D_t3849682566* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PersistentCallU5BU5D_t3849682566** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PersistentCallU5BU5D_t3849682566* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1068164621_H
#ifndef BINDER_T3602727638_H
#define BINDER_T3602727638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t3602727638  : public RuntimeObject
{
public:

public:
};

struct Binder_t3602727638_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t3602727638 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t3602727638_StaticFields, ___default_binder_0)); }
	inline Binder_t3602727638 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t3602727638 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t3602727638 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T3602727638_H
#ifndef RENDERPIPELINEMANAGER_T2128300851_H
#define RENDERPIPELINEMANAGER_T2128300851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineManager
struct  RenderPipelineManager_t2128300851  : public RuntimeObject
{
public:

public:
};

struct RenderPipelineManager_t2128300851_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.IRenderPipelineAsset UnityEngine.Experimental.Rendering.RenderPipelineManager::s_CurrentPipelineAsset
	RuntimeObject* ___s_CurrentPipelineAsset_0;
	// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::<currentPipeline>k__BackingField
	RuntimeObject* ___U3CcurrentPipelineU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_s_CurrentPipelineAsset_0() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t2128300851_StaticFields, ___s_CurrentPipelineAsset_0)); }
	inline RuntimeObject* get_s_CurrentPipelineAsset_0() const { return ___s_CurrentPipelineAsset_0; }
	inline RuntimeObject** get_address_of_s_CurrentPipelineAsset_0() { return &___s_CurrentPipelineAsset_0; }
	inline void set_s_CurrentPipelineAsset_0(RuntimeObject* value)
	{
		___s_CurrentPipelineAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentPipelineAsset_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t2128300851_StaticFields, ___U3CcurrentPipelineU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CcurrentPipelineU3Ek__BackingField_1() const { return ___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CcurrentPipelineU3Ek__BackingField_1() { return &___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline void set_U3CcurrentPipelineU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CcurrentPipelineU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPipelineU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEMANAGER_T2128300851_H
#ifndef GYROSCOPE_T416198001_H
#define GYROSCOPE_T416198001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gyroscope
struct  Gyroscope_t416198001  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPE_T416198001_H
#ifndef INPUT_T2963215744_H
#define INPUT_T2963215744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Input
struct  Input_t2963215744  : public RuntimeObject
{
public:

public:
};

struct Input_t2963215744_StaticFields
{
public:
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t416198001 * ___m_MainGyro_0;

public:
	inline static int32_t get_offset_of_m_MainGyro_0() { return static_cast<int32_t>(offsetof(Input_t2963215744_StaticFields, ___m_MainGyro_0)); }
	inline Gyroscope_t416198001 * get_m_MainGyro_0() const { return ___m_MainGyro_0; }
	inline Gyroscope_t416198001 ** get_address_of_m_MainGyro_0() { return &___m_MainGyro_0; }
	inline void set_m_MainGyro_0(Gyroscope_t416198001 * value)
	{
		___m_MainGyro_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainGyro_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T2963215744_H
#ifndef MANAGEDSTREAMHELPERS_T2966138683_H
#define MANAGEDSTREAMHELPERS_T2966138683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ManagedStreamHelpers
struct  ManagedStreamHelpers_t2966138683  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGEDSTREAMHELPERS_T2966138683_H
#ifndef STREAM_T3046340190_H
#define STREAM_T3046340190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3046340190  : public RuntimeObject
{
public:

public:
};

struct Stream_t3046340190_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3046340190 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3046340190_StaticFields, ___Null_0)); }
	inline Stream_t3046340190 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3046340190 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3046340190 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3046340190_H
#ifndef LIST_1_T309455265_H
#define LIST_1_T309455265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t309455265  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t3565237794* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____items_1)); }
	inline Int32U5BU5D_t3565237794* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t3565237794** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t3565237794* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t309455265, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t309455265_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t3565237794* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t309455265_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t3565237794* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t3565237794** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t3565237794* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T309455265_H
#ifndef LIST_1_T140159775_H
#define LIST_1_T140159775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t140159775  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t974944492* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t140159775, ____items_1)); }
	inline Vector3U5BU5D_t974944492* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t974944492** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t974944492* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t140159775, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t140159775, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t140159775_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t974944492* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t140159775_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t974944492* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t974944492** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t974944492* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T140159775_H
#ifndef LIST_1_T191085541_H
#define LIST_1_T191085541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t191085541  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t2564234830* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t191085541, ____items_1)); }
	inline Vector4U5BU5D_t2564234830* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t2564234830** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t2564234830* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t191085541, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t191085541, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t191085541_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t2564234830* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t191085541_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t2564234830* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t2564234830** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t2564234830* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T191085541_H
#ifndef LIST_1_T2598598263_H
#define LIST_1_T2598598263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t2598598263  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_t1505762612* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2598598263, ____items_1)); }
	inline Color32U5BU5D_t1505762612* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_t1505762612** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_t1505762612* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2598598263, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2598598263, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2598598263_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Color32U5BU5D_t1505762612* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2598598263_StaticFields, ___EmptyArray_4)); }
	inline Color32U5BU5D_t1505762612* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Color32U5BU5D_t1505762612** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Color32U5BU5D_t1505762612* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2598598263_H
#ifndef LIST_1_T2867512982_H
#define LIST_1_T2867512982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t2867512982  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t1392199097* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2867512982, ____items_1)); }
	inline Vector2U5BU5D_t1392199097* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t1392199097** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t1392199097* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2867512982, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2867512982, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2867512982_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t1392199097* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2867512982_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t1392199097* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t1392199097** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t1392199097* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2867512982_H
#ifndef VALUETYPE_T1364887298_H
#define VALUETYPE_T1364887298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1364887298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1364887298_marshaled_com
{
};
#endif // VALUETYPE_T1364887298_H
#ifndef LIST_1_T599864163_H
#define LIST_1_T599864163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_t599864163  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ComponentU5BU5D_t259396376* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t599864163, ____items_1)); }
	inline ComponentU5BU5D_t259396376* get__items_1() const { return ____items_1; }
	inline ComponentU5BU5D_t259396376** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ComponentU5BU5D_t259396376* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t599864163, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t599864163, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t599864163_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ComponentU5BU5D_t259396376* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t599864163_StaticFields, ___EmptyArray_4)); }
	inline ComponentU5BU5D_t259396376* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ComponentU5BU5D_t259396376** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ComponentU5BU5D_t259396376* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T599864163_H
#ifndef CLASSLIBRARYINITIALIZER_T3487816611_H
#define CLASSLIBRARYINITIALIZER_T3487816611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ClassLibraryInitializer
struct  ClassLibraryInitializer_t3487816611  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSLIBRARYINITIALIZER_T3487816611_H
#ifndef BASEINVOKABLECALL_T3782853021_H
#define BASEINVOKABLECALL_T3782853021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.BaseInvokableCall
struct  BaseInvokableCall_t3782853021  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINVOKABLECALL_T3782853021_H
#ifndef ATTRIBUTEHELPERENGINE_T1954971056_H
#define ATTRIBUTEHELPERENGINE_T1954971056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t1954971056  : public RuntimeObject
{
public:

public:
};

struct AttributeHelperEngine_t1954971056_StaticFields
{
public:
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t56197584* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t3106130354* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t1251203158* ____requireComponentArray_2;

public:
	inline static int32_t get_offset_of__disallowMultipleComponentArray_0() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t1954971056_StaticFields, ____disallowMultipleComponentArray_0)); }
	inline DisallowMultipleComponentU5BU5D_t56197584* get__disallowMultipleComponentArray_0() const { return ____disallowMultipleComponentArray_0; }
	inline DisallowMultipleComponentU5BU5D_t56197584** get_address_of__disallowMultipleComponentArray_0() { return &____disallowMultipleComponentArray_0; }
	inline void set__disallowMultipleComponentArray_0(DisallowMultipleComponentU5BU5D_t56197584* value)
	{
		____disallowMultipleComponentArray_0 = value;
		Il2CppCodeGenWriteBarrier((&____disallowMultipleComponentArray_0), value);
	}

	inline static int32_t get_offset_of__executeInEditModeArray_1() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t1954971056_StaticFields, ____executeInEditModeArray_1)); }
	inline ExecuteInEditModeU5BU5D_t3106130354* get__executeInEditModeArray_1() const { return ____executeInEditModeArray_1; }
	inline ExecuteInEditModeU5BU5D_t3106130354** get_address_of__executeInEditModeArray_1() { return &____executeInEditModeArray_1; }
	inline void set__executeInEditModeArray_1(ExecuteInEditModeU5BU5D_t3106130354* value)
	{
		____executeInEditModeArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeInEditModeArray_1), value);
	}

	inline static int32_t get_offset_of__requireComponentArray_2() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t1954971056_StaticFields, ____requireComponentArray_2)); }
	inline RequireComponentU5BU5D_t1251203158* get__requireComponentArray_2() const { return ____requireComponentArray_2; }
	inline RequireComponentU5BU5D_t1251203158** get_address_of__requireComponentArray_2() { return &____requireComponentArray_2; }
	inline void set__requireComponentArray_2(RequireComponentU5BU5D_t1251203158* value)
	{
		____requireComponentArray_2 = value;
		Il2CppCodeGenWriteBarrier((&____requireComponentArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEHELPERENGINE_T1954971056_H
#ifndef ATTRIBUTE_T3852256153_H
#define ATTRIBUTE_T3852256153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t3852256153  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T3852256153_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t83643201* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t83643201* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t83643201** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t83643201* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T329231522_H
#define LIST_1_T329231522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Type>
struct  List_1_t329231522  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeU5BU5D_t1460120061* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t329231522, ____items_1)); }
	inline TypeU5BU5D_t1460120061* get__items_1() const { return ____items_1; }
	inline TypeU5BU5D_t1460120061** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeU5BU5D_t1460120061* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t329231522, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t329231522, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t329231522_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TypeU5BU5D_t1460120061* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t329231522_StaticFields, ___EmptyArray_4)); }
	inline TypeU5BU5D_t1460120061* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TypeU5BU5D_t1460120061** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TypeU5BU5D_t1460120061* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T329231522_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef APPLICATION_T1423836661_H
#define APPLICATION_T1423836661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application
struct  Application_t1423836661  : public RuntimeObject
{
public:

public:
};

struct Application_t1423836661_StaticFields
{
public:
	// UnityEngine.Application/LowMemoryCallback UnityEngine.Application::lowMemory
	LowMemoryCallback_t2129875537 * ___lowMemory_0;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandler
	LogCallback_t657888911 * ___s_LogCallbackHandler_1;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandlerThreaded
	LogCallback_t657888911 * ___s_LogCallbackHandlerThreaded_2;
	// UnityEngine.Events.UnityAction UnityEngine.Application::onBeforeRender
	UnityAction_t1298730314 * ___onBeforeRender_3;

public:
	inline static int32_t get_offset_of_lowMemory_0() { return static_cast<int32_t>(offsetof(Application_t1423836661_StaticFields, ___lowMemory_0)); }
	inline LowMemoryCallback_t2129875537 * get_lowMemory_0() const { return ___lowMemory_0; }
	inline LowMemoryCallback_t2129875537 ** get_address_of_lowMemory_0() { return &___lowMemory_0; }
	inline void set_lowMemory_0(LowMemoryCallback_t2129875537 * value)
	{
		___lowMemory_0 = value;
		Il2CppCodeGenWriteBarrier((&___lowMemory_0), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandler_1() { return static_cast<int32_t>(offsetof(Application_t1423836661_StaticFields, ___s_LogCallbackHandler_1)); }
	inline LogCallback_t657888911 * get_s_LogCallbackHandler_1() const { return ___s_LogCallbackHandler_1; }
	inline LogCallback_t657888911 ** get_address_of_s_LogCallbackHandler_1() { return &___s_LogCallbackHandler_1; }
	inline void set_s_LogCallbackHandler_1(LogCallback_t657888911 * value)
	{
		___s_LogCallbackHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandler_1), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandlerThreaded_2() { return static_cast<int32_t>(offsetof(Application_t1423836661_StaticFields, ___s_LogCallbackHandlerThreaded_2)); }
	inline LogCallback_t657888911 * get_s_LogCallbackHandlerThreaded_2() const { return ___s_LogCallbackHandlerThreaded_2; }
	inline LogCallback_t657888911 ** get_address_of_s_LogCallbackHandlerThreaded_2() { return &___s_LogCallbackHandlerThreaded_2; }
	inline void set_s_LogCallbackHandlerThreaded_2(LogCallback_t657888911 * value)
	{
		___s_LogCallbackHandlerThreaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandlerThreaded_2), value);
	}

	inline static int32_t get_offset_of_onBeforeRender_3() { return static_cast<int32_t>(offsetof(Application_t1423836661_StaticFields, ___onBeforeRender_3)); }
	inline UnityAction_t1298730314 * get_onBeforeRender_3() const { return ___onBeforeRender_3; }
	inline UnityAction_t1298730314 ** get_address_of_onBeforeRender_3() { return &___onBeforeRender_3; }
	inline void set_onBeforeRender_3(UnityAction_t1298730314 * value)
	{
		___onBeforeRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeRender_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATION_T1423836661_H
#ifndef STACK_1_T3308188911_H
#define STACK_1_T3308188911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Type>
struct  Stack_1_t3308188911  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	TypeU5BU5D_t1460120061* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t3308188911, ____array_0)); }
	inline TypeU5BU5D_t1460120061* get__array_0() const { return ____array_0; }
	inline TypeU5BU5D_t1460120061** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(TypeU5BU5D_t1460120061* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t3308188911, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t3308188911, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T3308188911_H
#ifndef PROPERTYATTRIBUTE_T69305138_H
#define PROPERTYATTRIBUTE_T69305138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t69305138  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T69305138_H
#ifndef BOOLEAN_T569405246_H
#define BOOLEAN_T569405246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t569405246 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t569405246, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t569405246_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t569405246_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T569405246_H
#ifndef CREATEASSETMENUATTRIBUTE_T1456955310_H
#define CREATEASSETMENUATTRIBUTE_T1456955310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CreateAssetMenuAttribute
struct  CreateAssetMenuAttribute_t1456955310  : public Attribute_t3852256153
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t1456955310, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmenuNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t1456955310, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t1456955310, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEASSETMENUATTRIBUTE_T1456955310_H
#ifndef UNITYEVENT_T3617968793_H
#define UNITYEVENT_T3617968793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t3617968793  : public UnityEventBase_t2204896975
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t1568665923* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t3617968793, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t1568665923* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t1568665923** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t1568665923* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T3617968793_H
#ifndef CSSSIZE_T3542252173_H
#define CSSSIZE_T3542252173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t3542252173 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t3542252173, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t3542252173, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T3542252173_H
#ifndef PARAMETERMODIFIER_T1946944212_H
#define PARAMETERMODIFIER_T1946944212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t1946944212 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t2577425931* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t1946944212, ____byref_0)); }
	inline BooleanU5BU5D_t2577425931* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t2577425931** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t2577425931* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1946944212_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1946944212_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T1946944212_H
#ifndef QUATERNION_T2761156409_H
#define QUATERNION_T2761156409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2761156409 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2761156409_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2761156409  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2761156409_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2761156409  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2761156409 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2761156409  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2761156409_H
#ifndef DOUBLE_T2078998952_H
#define DOUBLE_T2078998952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t2078998952 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t2078998952, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T2078998952_H
#ifndef CULLINGGROUPEVENT_T2298849179_H
#define CULLINGGROUPEVENT_T2298849179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroupEvent
struct  CullingGroupEvent_t2298849179 
{
public:
	// System.Int32 UnityEngine.CullingGroupEvent::m_Index
	int32_t ___m_Index_0;
	// System.Byte UnityEngine.CullingGroupEvent::m_PrevState
	uint8_t ___m_PrevState_1;
	// System.Byte UnityEngine.CullingGroupEvent::m_ThisState
	uint8_t ___m_ThisState_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t2298849179, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_1() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t2298849179, ___m_PrevState_1)); }
	inline uint8_t get_m_PrevState_1() const { return ___m_PrevState_1; }
	inline uint8_t* get_address_of_m_PrevState_1() { return &___m_PrevState_1; }
	inline void set_m_PrevState_1(uint8_t value)
	{
		___m_PrevState_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisState_2() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t2298849179, ___m_ThisState_2)); }
	inline uint8_t get_m_ThisState_2() const { return ___m_ThisState_2; }
	inline uint8_t* get_address_of_m_ThisState_2() { return &___m_ThisState_2; }
	inline void set_m_ThisState_2(uint8_t value)
	{
		___m_ThisState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGGROUPEVENT_T2298849179_H
#ifndef MATHF_T2081007568_H
#define MATHF_T2081007568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mathf
struct  Mathf_t2081007568 
{
public:

public:
};

struct Mathf_t2081007568_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_t2081007568_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHF_T2081007568_H
#ifndef UINT32_T3311932136_H
#define UINT32_T3311932136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3311932136 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3311932136, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3311932136_H
#ifndef ASSEMBLYISEDITORASSEMBLY_T2773218126_H
#define ASSEMBLYISEDITORASSEMBLY_T2773218126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssemblyIsEditorAssembly
struct  AssemblyIsEditorAssembly_t2773218126  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYISEDITORASSEMBLY_T2773218126_H
#ifndef INT32_T499004851_H
#define INT32_T499004851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t499004851 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t499004851, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T499004851_H
#ifndef SPHERICALHARMONICSL2_T298540216_H
#define SPHERICALHARMONICSL2_T298540216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.SphericalHarmonicsL2
struct  SphericalHarmonicsL2_t298540216 
{
public:
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr0
	float ___shr0_0;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr1
	float ___shr1_1;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr2
	float ___shr2_2;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr3
	float ___shr3_3;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr4
	float ___shr4_4;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr5
	float ___shr5_5;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr6
	float ___shr6_6;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr7
	float ___shr7_7;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr8
	float ___shr8_8;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg0
	float ___shg0_9;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg1
	float ___shg1_10;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg2
	float ___shg2_11;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg3
	float ___shg3_12;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg4
	float ___shg4_13;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg5
	float ___shg5_14;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg6
	float ___shg6_15;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg7
	float ___shg7_16;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg8
	float ___shg8_17;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb0
	float ___shb0_18;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb1
	float ___shb1_19;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb2
	float ___shb2_20;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb3
	float ___shb3_21;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb4
	float ___shb4_22;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb5
	float ___shb5_23;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb6
	float ___shb6_24;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb7
	float ___shb7_25;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb8
	float ___shb8_26;

public:
	inline static int32_t get_offset_of_shr0_0() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr0_0)); }
	inline float get_shr0_0() const { return ___shr0_0; }
	inline float* get_address_of_shr0_0() { return &___shr0_0; }
	inline void set_shr0_0(float value)
	{
		___shr0_0 = value;
	}

	inline static int32_t get_offset_of_shr1_1() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr1_1)); }
	inline float get_shr1_1() const { return ___shr1_1; }
	inline float* get_address_of_shr1_1() { return &___shr1_1; }
	inline void set_shr1_1(float value)
	{
		___shr1_1 = value;
	}

	inline static int32_t get_offset_of_shr2_2() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr2_2)); }
	inline float get_shr2_2() const { return ___shr2_2; }
	inline float* get_address_of_shr2_2() { return &___shr2_2; }
	inline void set_shr2_2(float value)
	{
		___shr2_2 = value;
	}

	inline static int32_t get_offset_of_shr3_3() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr3_3)); }
	inline float get_shr3_3() const { return ___shr3_3; }
	inline float* get_address_of_shr3_3() { return &___shr3_3; }
	inline void set_shr3_3(float value)
	{
		___shr3_3 = value;
	}

	inline static int32_t get_offset_of_shr4_4() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr4_4)); }
	inline float get_shr4_4() const { return ___shr4_4; }
	inline float* get_address_of_shr4_4() { return &___shr4_4; }
	inline void set_shr4_4(float value)
	{
		___shr4_4 = value;
	}

	inline static int32_t get_offset_of_shr5_5() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr5_5)); }
	inline float get_shr5_5() const { return ___shr5_5; }
	inline float* get_address_of_shr5_5() { return &___shr5_5; }
	inline void set_shr5_5(float value)
	{
		___shr5_5 = value;
	}

	inline static int32_t get_offset_of_shr6_6() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr6_6)); }
	inline float get_shr6_6() const { return ___shr6_6; }
	inline float* get_address_of_shr6_6() { return &___shr6_6; }
	inline void set_shr6_6(float value)
	{
		___shr6_6 = value;
	}

	inline static int32_t get_offset_of_shr7_7() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr7_7)); }
	inline float get_shr7_7() const { return ___shr7_7; }
	inline float* get_address_of_shr7_7() { return &___shr7_7; }
	inline void set_shr7_7(float value)
	{
		___shr7_7 = value;
	}

	inline static int32_t get_offset_of_shr8_8() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shr8_8)); }
	inline float get_shr8_8() const { return ___shr8_8; }
	inline float* get_address_of_shr8_8() { return &___shr8_8; }
	inline void set_shr8_8(float value)
	{
		___shr8_8 = value;
	}

	inline static int32_t get_offset_of_shg0_9() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg0_9)); }
	inline float get_shg0_9() const { return ___shg0_9; }
	inline float* get_address_of_shg0_9() { return &___shg0_9; }
	inline void set_shg0_9(float value)
	{
		___shg0_9 = value;
	}

	inline static int32_t get_offset_of_shg1_10() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg1_10)); }
	inline float get_shg1_10() const { return ___shg1_10; }
	inline float* get_address_of_shg1_10() { return &___shg1_10; }
	inline void set_shg1_10(float value)
	{
		___shg1_10 = value;
	}

	inline static int32_t get_offset_of_shg2_11() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg2_11)); }
	inline float get_shg2_11() const { return ___shg2_11; }
	inline float* get_address_of_shg2_11() { return &___shg2_11; }
	inline void set_shg2_11(float value)
	{
		___shg2_11 = value;
	}

	inline static int32_t get_offset_of_shg3_12() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg3_12)); }
	inline float get_shg3_12() const { return ___shg3_12; }
	inline float* get_address_of_shg3_12() { return &___shg3_12; }
	inline void set_shg3_12(float value)
	{
		___shg3_12 = value;
	}

	inline static int32_t get_offset_of_shg4_13() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg4_13)); }
	inline float get_shg4_13() const { return ___shg4_13; }
	inline float* get_address_of_shg4_13() { return &___shg4_13; }
	inline void set_shg4_13(float value)
	{
		___shg4_13 = value;
	}

	inline static int32_t get_offset_of_shg5_14() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg5_14)); }
	inline float get_shg5_14() const { return ___shg5_14; }
	inline float* get_address_of_shg5_14() { return &___shg5_14; }
	inline void set_shg5_14(float value)
	{
		___shg5_14 = value;
	}

	inline static int32_t get_offset_of_shg6_15() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg6_15)); }
	inline float get_shg6_15() const { return ___shg6_15; }
	inline float* get_address_of_shg6_15() { return &___shg6_15; }
	inline void set_shg6_15(float value)
	{
		___shg6_15 = value;
	}

	inline static int32_t get_offset_of_shg7_16() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg7_16)); }
	inline float get_shg7_16() const { return ___shg7_16; }
	inline float* get_address_of_shg7_16() { return &___shg7_16; }
	inline void set_shg7_16(float value)
	{
		___shg7_16 = value;
	}

	inline static int32_t get_offset_of_shg8_17() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shg8_17)); }
	inline float get_shg8_17() const { return ___shg8_17; }
	inline float* get_address_of_shg8_17() { return &___shg8_17; }
	inline void set_shg8_17(float value)
	{
		___shg8_17 = value;
	}

	inline static int32_t get_offset_of_shb0_18() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb0_18)); }
	inline float get_shb0_18() const { return ___shb0_18; }
	inline float* get_address_of_shb0_18() { return &___shb0_18; }
	inline void set_shb0_18(float value)
	{
		___shb0_18 = value;
	}

	inline static int32_t get_offset_of_shb1_19() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb1_19)); }
	inline float get_shb1_19() const { return ___shb1_19; }
	inline float* get_address_of_shb1_19() { return &___shb1_19; }
	inline void set_shb1_19(float value)
	{
		___shb1_19 = value;
	}

	inline static int32_t get_offset_of_shb2_20() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb2_20)); }
	inline float get_shb2_20() const { return ___shb2_20; }
	inline float* get_address_of_shb2_20() { return &___shb2_20; }
	inline void set_shb2_20(float value)
	{
		___shb2_20 = value;
	}

	inline static int32_t get_offset_of_shb3_21() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb3_21)); }
	inline float get_shb3_21() const { return ___shb3_21; }
	inline float* get_address_of_shb3_21() { return &___shb3_21; }
	inline void set_shb3_21(float value)
	{
		___shb3_21 = value;
	}

	inline static int32_t get_offset_of_shb4_22() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb4_22)); }
	inline float get_shb4_22() const { return ___shb4_22; }
	inline float* get_address_of_shb4_22() { return &___shb4_22; }
	inline void set_shb4_22(float value)
	{
		___shb4_22 = value;
	}

	inline static int32_t get_offset_of_shb5_23() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb5_23)); }
	inline float get_shb5_23() const { return ___shb5_23; }
	inline float* get_address_of_shb5_23() { return &___shb5_23; }
	inline void set_shb5_23(float value)
	{
		___shb5_23 = value;
	}

	inline static int32_t get_offset_of_shb6_24() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb6_24)); }
	inline float get_shb6_24() const { return ___shb6_24; }
	inline float* get_address_of_shb6_24() { return &___shb6_24; }
	inline void set_shb6_24(float value)
	{
		___shb6_24 = value;
	}

	inline static int32_t get_offset_of_shb7_25() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb7_25)); }
	inline float get_shb7_25() const { return ___shb7_25; }
	inline float* get_address_of_shb7_25() { return &___shb7_25; }
	inline void set_shb7_25(float value)
	{
		___shb7_25 = value;
	}

	inline static int32_t get_offset_of_shb8_26() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t298540216, ___shb8_26)); }
	inline float get_shb8_26() const { return ___shb8_26; }
	inline float* get_address_of_shb8_26() { return &___shb8_26; }
	inline void set_shb8_26(float value)
	{
		___shb8_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERICALHARMONICSL2_T298540216_H
#ifndef ENUMERATOR_T1785309417_H
#define ENUMERATOR_T1785309417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t1785309417 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t185548372 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1785309417, ___l_0)); }
	inline List_1_t185548372 * get_l_0() const { return ___l_0; }
	inline List_1_t185548372 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t185548372 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1785309417, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1785309417, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1785309417, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1785309417_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T4212122797_H
#define DRIVENRECTTRANSFORMTRACKER_T4212122797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t4212122797 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T4212122797_H
#ifndef ENUMERATOR_T2667925666_H
#define ENUMERATOR_T2667925666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct  Enumerator_t2667925666 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1068164621 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PersistentCall_t1257714207 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2667925666, ___l_0)); }
	inline List_1_t1068164621 * get_l_0() const { return ___l_0; }
	inline List_1_t1068164621 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1068164621 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2667925666, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2667925666, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2667925666, ___current_3)); }
	inline PersistentCall_t1257714207 * get_current_3() const { return ___current_3; }
	inline PersistentCall_t1257714207 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PersistentCall_t1257714207 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2667925666_H
#ifndef SYSTEMEXCEPTION_T4228135144_H
#define SYSTEMEXCEPTION_T4228135144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t4228135144  : public Exception_t2123675094
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T4228135144_H
#ifndef INT64_T3070791913_H
#define INT64_T3070791913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3070791913 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3070791913, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3070791913_H
#ifndef INVOKABLECALL_T1532986675_H
#define INVOKABLECALL_T1532986675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1532986675  : public BaseInvokableCall_t3782853021
{
public:
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t1298730314 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_t1532986675, ___Delegate_0)); }
	inline UnityAction_t1298730314 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_t1298730314 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_t1298730314 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_T1532986675_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef EXCLUDEFROMDOCSATTRIBUTE_T3465613458_H
#define EXCLUDEFROMDOCSATTRIBUTE_T3465613458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct  ExcludeFromDocsAttribute_t3465613458  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMDOCSATTRIBUTE_T3465613458_H
#ifndef DEFAULTVALUEATTRIBUTE_T3593402498_H
#define DEFAULTVALUEATTRIBUTE_T3593402498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t3593402498  : public Attribute_t3852256153
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t3593402498, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T3593402498_H
#ifndef VECTOR2_T3057062568_H
#define VECTOR2_T3057062568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3057062568 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3057062568, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3057062568, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3057062568_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3057062568  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3057062568  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3057062568  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3057062568  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3057062568  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3057062568  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3057062568  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3057062568  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3057062568  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3057062568 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3057062568  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___oneVector_3)); }
	inline Vector2_t3057062568  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3057062568 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3057062568  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___upVector_4)); }
	inline Vector2_t3057062568  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3057062568 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3057062568  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___downVector_5)); }
	inline Vector2_t3057062568  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3057062568 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3057062568  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___leftVector_6)); }
	inline Vector2_t3057062568  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3057062568 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3057062568  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___rightVector_7)); }
	inline Vector2_t3057062568  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3057062568 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3057062568  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3057062568  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3057062568 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3057062568  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3057062568_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3057062568  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3057062568 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3057062568  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3057062568_H
#ifndef KEYFRAME_T1682423392_H
#define KEYFRAME_T1682423392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t1682423392 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t1682423392, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t1682423392, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t1682423392, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t1682423392, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T1682423392_H
#ifndef IL2CPPSTRUCTALIGNMENTATTRIBUTE_T2686620401_H
#define IL2CPPSTRUCTALIGNMENTATTRIBUTE_T2686620401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IL2CPPStructAlignmentAttribute
struct  IL2CPPStructAlignmentAttribute_t2686620401  : public Attribute_t3852256153
{
public:
	// System.Int32 UnityEngine.IL2CPPStructAlignmentAttribute::Align
	int32_t ___Align_0;

public:
	inline static int32_t get_offset_of_Align_0() { return static_cast<int32_t>(offsetof(IL2CPPStructAlignmentAttribute_t2686620401, ___Align_0)); }
	inline int32_t get_Align_0() const { return ___Align_0; }
	inline int32_t* get_address_of_Align_0() { return &___Align_0; }
	inline void set_Align_0(int32_t value)
	{
		___Align_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPSTRUCTALIGNMENTATTRIBUTE_T2686620401_H
#ifndef REQUIRECOMPONENT_T4152270991_H
#define REQUIRECOMPONENT_T4152270991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t4152270991  : public Attribute_t3852256153
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t4152270991, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T4152270991_H
#ifndef LAYERMASK_T1788260229_H
#define LAYERMASK_T1788260229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t1788260229 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t1788260229, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T1788260229_H
#ifndef DEFAULTEXECUTIONORDER_T2771548650_H
#define DEFAULTEXECUTIONORDER_T2771548650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DefaultExecutionOrder
struct  DefaultExecutionOrder_t2771548650  : public Attribute_t3852256153
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t2771548650, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXECUTIONORDER_T2771548650_H
#ifndef CONTEXTMENU_T1633623427_H
#define CONTEXTMENU_T1633623427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContextMenu
struct  ContextMenu_t1633623427  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTMENU_T1633623427_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T2444501850_H
#define MONOPINVOKECALLBACKATTRIBUTE_T2444501850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AOT.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t2444501850  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T2444501850_H
#ifndef ENUM_T3173835468_H
#define ENUM_T3173835468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t3173835468  : public ValueType_t1364887298
{
public:

public:
};

struct Enum_t3173835468_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t83643201* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t3173835468_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t83643201* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t83643201** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t83643201* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t3173835468_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t3173835468_marshaled_com
{
};
#endif // ENUM_T3173835468_H
#ifndef ADDCOMPONENTMENU_T67651563_H
#define ADDCOMPONENTMENU_T67651563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t67651563  : public Attribute_t3852256153
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t67651563, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AddComponentMenu_0), value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t67651563, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENTMENU_T67651563_H
#ifndef INVOKABLECALL_1_T1041587107_H
#define INVOKABLECALL_1_T1041587107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t1041587107  : public BaseInvokableCall_t3782853021
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t4006882931 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t1041587107, ___Delegate_0)); }
	inline UnityAction_1_t4006882931 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t4006882931 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t4006882931 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T1041587107_H
#ifndef INVOKABLECALL_1_T3972206508_H
#define INVOKABLECALL_1_T3972206508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t3972206508  : public BaseInvokableCall_t3782853021
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2642535036 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t3972206508, ___Delegate_0)); }
	inline UnityAction_1_t2642535036 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2642535036 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2642535036 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T3972206508_H
#ifndef INVOKABLECALL_1_T3414123997_H
#define INVOKABLECALL_1_T3414123997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t3414123997  : public BaseInvokableCall_t3782853021
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2084452525 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t3414123997, ___Delegate_0)); }
	inline UnityAction_1_t2084452525 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2084452525 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2084452525 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T3414123997_H
#ifndef MATRIX4X4_T2375577114_H
#define MATRIX4X4_T2375577114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2375577114 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2375577114_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2375577114  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2375577114  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2375577114  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2375577114 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2375577114  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2375577114_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2375577114  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2375577114 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2375577114  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2375577114_H
#ifndef INVOKABLECALL_1_T4042606903_H
#define INVOKABLECALL_1_T4042606903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct  InvokableCall_1_t4042606903  : public BaseInvokableCall_t3782853021
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2712935431 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t4042606903, ___Delegate_0)); }
	inline UnityAction_1_t2712935431 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2712935431 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2712935431 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T4042606903_H
#ifndef RECT_T1344834245_H
#define RECT_T1344834245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1344834245 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1344834245, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1344834245_H
#ifndef SINGLE_T1863352746_H
#define SINGLE_T1863352746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1863352746 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1863352746, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1863352746_H
#ifndef VECTOR3_T329709361_H
#define VECTOR3_T329709361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t329709361 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t329709361, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t329709361_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t329709361  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t329709361  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t329709361  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t329709361  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t329709361  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t329709361  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t329709361  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t329709361  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t329709361  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t329709361  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___zeroVector_4)); }
	inline Vector3_t329709361  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t329709361 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t329709361  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___oneVector_5)); }
	inline Vector3_t329709361  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t329709361 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t329709361  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___upVector_6)); }
	inline Vector3_t329709361  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t329709361 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t329709361  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___downVector_7)); }
	inline Vector3_t329709361  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t329709361 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t329709361  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___leftVector_8)); }
	inline Vector3_t329709361  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t329709361 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t329709361  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___rightVector_9)); }
	inline Vector3_t329709361  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t329709361 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t329709361  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___forwardVector_10)); }
	inline Vector3_t329709361  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t329709361 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t329709361  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___backVector_11)); }
	inline Vector3_t329709361  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t329709361 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t329709361  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t329709361  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t329709361 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t329709361  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t329709361_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t329709361  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t329709361 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t329709361  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T329709361_H
#ifndef UNMARSHALLEDATTRIBUTE_T1659112717_H
#define UNMARSHALLEDATTRIBUTE_T1659112717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bindings.UnmarshalledAttribute
struct  UnmarshalledAttribute_t1659112717  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEDATTRIBUTE_T1659112717_H
#ifndef TIMESPAN_T457147580_H
#define TIMESPAN_T457147580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t457147580 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t457147580_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t457147580  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t457147580  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t457147580  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t457147580  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t457147580 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t457147580  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t457147580  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t457147580 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t457147580  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t457147580_StaticFields, ___Zero_2)); }
	inline TimeSpan_t457147580  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t457147580 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t457147580  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T457147580_H
#ifndef EXECUTEINEDITMODE_T814488931_H
#define EXECUTEINEDITMODE_T814488931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExecuteInEditMode
struct  ExecuteInEditMode_t814488931  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEINEDITMODE_T814488931_H
#ifndef DISALLOWMULTIPLECOMPONENT_T1081793821_H
#define DISALLOWMULTIPLECOMPONENT_T1081793821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DisallowMultipleComponent
struct  DisallowMultipleComponent_t1081793821  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISALLOWMULTIPLECOMPONENT_T1081793821_H
#ifndef DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T809005988_H
#define DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T809005988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.DeallocateOnJobCompletionAttribute
struct  DeallocateOnJobCompletionAttribute_t809005988  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T809005988_H
#ifndef NATIVECONTAINERATTRIBUTE_T2622005047_H
#define NATIVECONTAINERATTRIBUTE_T2622005047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerAttribute
struct  NativeContainerAttribute_t2622005047  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERATTRIBUTE_T2622005047_H
#ifndef GCHANDLE_T714486722_H
#define GCHANDLE_T714486722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t714486722 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t714486722, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T714486722_H
#ifndef METHODBASE_T3535115348_H
#define METHODBASE_T3535115348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3535115348  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3535115348_H
#ifndef NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T2553578233_H
#define NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T2553578233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute
struct  NativeContainerSupportsAtomicWriteAttribute_t2553578233  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T2553578233_H
#ifndef NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3071186153_H
#define NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3071186153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute
struct  NativeContainerSupportsMinMaxWriteRestrictionAttribute_t3071186153  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T3071186153_H
#ifndef VOID_T2642135423_H
#define VOID_T2642135423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2642135423 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2642135423_H
#ifndef COLOR32_T2788147849_H
#define COLOR32_T2788147849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2788147849 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2788147849, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2788147849_H
#ifndef BYTE_T2815932036_H
#define BYTE_T2815932036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t2815932036 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t2815932036, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T2815932036_H
#ifndef VECTOR4_T380635127_H
#define VECTOR4_T380635127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t380635127 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t380635127, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t380635127_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t380635127  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t380635127  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t380635127  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t380635127  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___zeroVector_5)); }
	inline Vector4_t380635127  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t380635127 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t380635127  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___oneVector_6)); }
	inline Vector4_t380635127  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t380635127 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t380635127  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t380635127  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t380635127 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t380635127  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t380635127_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t380635127  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t380635127 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t380635127  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T380635127_H
#ifndef READONLYATTRIBUTE_T4072044580_H
#define READONLYATTRIBUTE_T4072044580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadOnlyAttribute
struct  ReadOnlyAttribute_t4072044580  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T4072044580_H
#ifndef COLOR_T460381780_H
#define COLOR_T460381780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t460381780 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t460381780, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t460381780, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t460381780, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t460381780, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T460381780_H
#ifndef READWRITEATTRIBUTE_T2014446059_H
#define READWRITEATTRIBUTE_T2014446059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadWriteAttribute
struct  ReadWriteAttribute_t2014446059  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READWRITEATTRIBUTE_T2014446059_H
#ifndef WRITEONLYATTRIBUTE_T2789400261_H
#define WRITEONLYATTRIBUTE_T2789400261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.WriteOnlyAttribute
struct  WriteOnlyAttribute_t2789400261  : public Attribute_t3852256153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEONLYATTRIBUTE_T2789400261_H
#ifndef GRADIENT_T1395232743_H
#define GRADIENT_T1395232743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t1395232743  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t1395232743, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t1395232743_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t1395232743_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T1395232743_H
#ifndef SENDMESSAGEOPTIONS_T2857403360_H
#define SENDMESSAGEOPTIONS_T2857403360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t2857403360 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t2857403360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T2857403360_H
#ifndef TOUCHTYPE_T3597616217_H
#define TOUCHTYPE_T3597616217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t3597616217 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t3597616217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T3597616217_H
#ifndef INTERNALVERTEXCHANNELTYPE_T1320944330_H
#define INTERNALVERTEXCHANNELTYPE_T1320944330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh/InternalVertexChannelType
struct  InternalVertexChannelType_t1320944330 
{
public:
	// System.Int32 UnityEngine.Mesh/InternalVertexChannelType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InternalVertexChannelType_t1320944330, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALVERTEXCHANNELTYPE_T1320944330_H
#ifndef FILTERMODE_T4175348091_H
#define FILTERMODE_T4175348091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t4175348091 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t4175348091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T4175348091_H
#ifndef SCRIPTABLERENDERCONTEXT_T186008635_H
#define SCRIPTABLERENDERCONTEXT_T186008635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ScriptableRenderContext
struct  ScriptableRenderContext_t186008635 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ScriptableRenderContext_t186008635, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERCONTEXT_T186008635_H
#ifndef TOUCHPHASE_T1888277914_H
#define TOUCHPHASE_T1888277914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t1888277914 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t1888277914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T1888277914_H
#ifndef HEADERATTRIBUTE_T30125744_H
#define HEADERATTRIBUTE_T30125744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t30125744  : public PropertyAttribute_t69305138
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t30125744, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERATTRIBUTE_T30125744_H
#ifndef IMECOMPOSITIONMODE_T1236926590_H
#define IMECOMPOSITIONMODE_T1236926590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IMECompositionMode
struct  IMECompositionMode_t1236926590 
{
public:
	// System.Int32 UnityEngine.IMECompositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IMECompositionMode_t1236926590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMECOMPOSITIONMODE_T1236926590_H
#ifndef HIDEFLAGS_T3447093715_H
#define HIDEFLAGS_T3447093715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t3447093715 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t3447093715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T3447093715_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#define INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t3921978895  : public SystemException_t4228135144
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T3921978895_H
#ifndef PARAMETERATTRIBUTES_T448949219_H
#define PARAMETERATTRIBUTES_T448949219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t448949219 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t448949219, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T448949219_H
#ifndef MATHFINTERNAL_T1363288871_H
#define MATHFINTERNAL_T1363288871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.MathfInternal
struct  MathfInternal_t1363288871 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MathfInternal_t1363288871__padding[1];
	};

public:
};

struct MathfInternal_t1363288871_StaticFields
{
public:
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;

public:
	inline static int32_t get_offset_of_FloatMinNormal_0() { return static_cast<int32_t>(offsetof(MathfInternal_t1363288871_StaticFields, ___FloatMinNormal_0)); }
	inline float get_FloatMinNormal_0() const { return ___FloatMinNormal_0; }
	inline float* get_address_of_FloatMinNormal_0() { return &___FloatMinNormal_0; }
	inline void set_FloatMinNormal_0(float value)
	{
		___FloatMinNormal_0 = value;
	}

	inline static int32_t get_offset_of_FloatMinDenormal_1() { return static_cast<int32_t>(offsetof(MathfInternal_t1363288871_StaticFields, ___FloatMinDenormal_1)); }
	inline float get_FloatMinDenormal_1() const { return ___FloatMinDenormal_1; }
	inline float* get_address_of_FloatMinDenormal_1() { return &___FloatMinDenormal_1; }
	inline void set_FloatMinDenormal_1(float value)
	{
		___FloatMinDenormal_1 = value;
	}

	inline static int32_t get_offset_of_IsFlushToZeroEnabled_2() { return static_cast<int32_t>(offsetof(MathfInternal_t1363288871_StaticFields, ___IsFlushToZeroEnabled_2)); }
	inline bool get_IsFlushToZeroEnabled_2() const { return ___IsFlushToZeroEnabled_2; }
	inline bool* get_address_of_IsFlushToZeroEnabled_2() { return &___IsFlushToZeroEnabled_2; }
	inline void set_IsFlushToZeroEnabled_2(bool value)
	{
		___IsFlushToZeroEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHFINTERNAL_T1363288871_H
#ifndef LOCALNOTIFICATION_T2999868044_H
#define LOCALNOTIFICATION_T2999868044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.LocalNotification
struct  LocalNotification_t2999868044  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.LocalNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(LocalNotification_t2999868044, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

struct LocalNotification_t2999868044_StaticFields
{
public:
	// System.Int64 UnityEngine.iOS.LocalNotification::m_NSReferenceDateTicks
	int64_t ___m_NSReferenceDateTicks_1;

public:
	inline static int32_t get_offset_of_m_NSReferenceDateTicks_1() { return static_cast<int32_t>(offsetof(LocalNotification_t2999868044_StaticFields, ___m_NSReferenceDateTicks_1)); }
	inline int64_t get_m_NSReferenceDateTicks_1() const { return ___m_NSReferenceDateTicks_1; }
	inline int64_t* get_address_of_m_NSReferenceDateTicks_1() { return &___m_NSReferenceDateTicks_1; }
	inline void set_m_NSReferenceDateTicks_1(int64_t value)
	{
		___m_NSReferenceDateTicks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALNOTIFICATION_T2999868044_H
#ifndef DATETIMEKIND_T508394208_H
#define DATETIMEKIND_T508394208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t508394208 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t508394208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T508394208_H
#ifndef MATERIALPROPERTYBLOCK_T3417007309_H
#define MATERIALPROPERTYBLOCK_T3417007309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t3417007309  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t3417007309, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T3417007309_H
#ifndef REMOTENOTIFICATION_T163380436_H
#define REMOTENOTIFICATION_T163380436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.RemoteNotification
struct  RemoteNotification_t163380436  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.RemoteNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(RemoteNotification_t163380436, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTENOTIFICATION_T163380436_H
#ifndef KEYCODE_T3979415068_H
#define KEYCODE_T3979415068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t3979415068 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t3979415068, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T3979415068_H
#ifndef SEEKORIGIN_T3931563262_H
#define SEEKORIGIN_T3931563262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SeekOrigin
struct  SeekOrigin_t3931563262 
{
public:
	// System.Int32 System.IO.SeekOrigin::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeekOrigin_t3931563262, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEKORIGIN_T3931563262_H
#ifndef ARGUMENTEXCEPTION_T3637419113_H
#define ARGUMENTEXCEPTION_T3637419113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3637419113  : public SystemException_t4228135144
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3637419113, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3637419113_H
#ifndef INTERNALSHADERCHANNEL_T245505398_H
#define INTERNALSHADERCHANNEL_T245505398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh/InternalShaderChannel
struct  InternalShaderChannel_t245505398 
{
public:
	// System.Int32 UnityEngine.Mesh/InternalShaderChannel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InternalShaderChannel_t245505398, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALSHADERCHANNEL_T245505398_H
#ifndef ANIMATIONCURVE_T2429328822_H
#define ANIMATIONCURVE_T2429328822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t2429328822  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2429328822, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2429328822_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2429328822_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T2429328822_H
#ifndef CONSTRUCTORINFO_T1575920019_H
#define CONSTRUCTORINFO_T1575920019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t1575920019  : public MethodBase_t3535115348
{
public:

public:
};

struct ConstructorInfo_t1575920019_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t1575920019_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t1575920019_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T1575920019_H
#ifndef PERSISTENTLISTENERMODE_T3727755410_H
#define PERSISTENTLISTENERMODE_T3727755410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentListenerMode
struct  PersistentListenerMode_t3727755410 
{
public:
	// System.Int32 UnityEngine.Events.PersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PersistentListenerMode_t3727755410, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTLISTENERMODE_T3727755410_H
#ifndef RUNTIMETYPEHANDLE_T3762232676_H
#define RUNTIMETYPEHANDLE_T3762232676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3762232676 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3762232676, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3762232676_H
#ifndef CSSMEASUREMODE_T483571173_H
#define CSSMEASUREMODE_T483571173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureMode
struct  CSSMeasureMode_t483571173 
{
public:
	// System.Int32 UnityEngine.CSSLayout.CSSMeasureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CSSMeasureMode_t483571173, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREMODE_T483571173_H
#ifndef DRIVENTRANSFORMPROPERTIES_T2238090006_H
#define DRIVENTRANSFORMPROPERTIES_T2238090006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t2238090006 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t2238090006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T2238090006_H
#ifndef COROUTINE_T4212313979_H
#define COROUTINE_T4212313979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t4212313979  : public YieldInstruction_t3362187334
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t4212313979, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t4212313979_marshaled_pinvoke : public YieldInstruction_t3362187334_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t4212313979_marshaled_com : public YieldInstruction_t3362187334_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T4212313979_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t3535115348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef OBJECT_T1970767703_H
#define OBJECT_T1970767703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1970767703  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1970767703, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1970767703_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1970767703_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1970767703_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1970767703_H
#ifndef DISPLAY_T882507400_H
#define DISPLAY_T882507400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display
struct  Display_t882507400  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Display::nativeDisplay
	intptr_t ___nativeDisplay_0;

public:
	inline static int32_t get_offset_of_nativeDisplay_0() { return static_cast<int32_t>(offsetof(Display_t882507400, ___nativeDisplay_0)); }
	inline intptr_t get_nativeDisplay_0() const { return ___nativeDisplay_0; }
	inline intptr_t* get_address_of_nativeDisplay_0() { return &___nativeDisplay_0; }
	inline void set_nativeDisplay_0(intptr_t value)
	{
		___nativeDisplay_0 = value;
	}
};

struct Display_t882507400_StaticFields
{
public:
	// UnityEngine.Display[] UnityEngine.Display::displays
	DisplayU5BU5D_t3818835545* ___displays_1;
	// UnityEngine.Display UnityEngine.Display::_mainDisplay
	Display_t882507400 * ____mainDisplay_2;
	// UnityEngine.Display/DisplaysUpdatedDelegate UnityEngine.Display::onDisplaysUpdated
	DisplaysUpdatedDelegate_t850082040 * ___onDisplaysUpdated_3;

public:
	inline static int32_t get_offset_of_displays_1() { return static_cast<int32_t>(offsetof(Display_t882507400_StaticFields, ___displays_1)); }
	inline DisplayU5BU5D_t3818835545* get_displays_1() const { return ___displays_1; }
	inline DisplayU5BU5D_t3818835545** get_address_of_displays_1() { return &___displays_1; }
	inline void set_displays_1(DisplayU5BU5D_t3818835545* value)
	{
		___displays_1 = value;
		Il2CppCodeGenWriteBarrier((&___displays_1), value);
	}

	inline static int32_t get_offset_of__mainDisplay_2() { return static_cast<int32_t>(offsetof(Display_t882507400_StaticFields, ____mainDisplay_2)); }
	inline Display_t882507400 * get__mainDisplay_2() const { return ____mainDisplay_2; }
	inline Display_t882507400 ** get_address_of__mainDisplay_2() { return &____mainDisplay_2; }
	inline void set__mainDisplay_2(Display_t882507400 * value)
	{
		____mainDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&____mainDisplay_2), value);
	}

	inline static int32_t get_offset_of_onDisplaysUpdated_3() { return static_cast<int32_t>(offsetof(Display_t882507400_StaticFields, ___onDisplaysUpdated_3)); }
	inline DisplaysUpdatedDelegate_t850082040 * get_onDisplaysUpdated_3() const { return ___onDisplaysUpdated_3; }
	inline DisplaysUpdatedDelegate_t850082040 ** get_address_of_onDisplaysUpdated_3() { return &___onDisplaysUpdated_3; }
	inline void set_onDisplaysUpdated_3(DisplaysUpdatedDelegate_t850082040 * value)
	{
		___onDisplaysUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDisplaysUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAY_T882507400_H
#ifndef DELEGATE_T1563516729_H
#define DELEGATE_T1563516729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1563516729  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t975501551 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1563516729, ___data_8)); }
	inline DelegateData_t975501551 * get_data_8() const { return ___data_8; }
	inline DelegateData_t975501551 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t975501551 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1563516729_H
#ifndef CULLINGGROUP_T635124431_H
#define CULLINGGROUP_T635124431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup
struct  CullingGroup_t635124431  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.CullingGroup::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.CullingGroup/StateChanged UnityEngine.CullingGroup::m_OnStateChanged
	StateChanged_t3421641282 * ___m_OnStateChanged_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CullingGroup_t635124431, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_OnStateChanged_1() { return static_cast<int32_t>(offsetof(CullingGroup_t635124431, ___m_OnStateChanged_1)); }
	inline StateChanged_t3421641282 * get_m_OnStateChanged_1() const { return ___m_OnStateChanged_1; }
	inline StateChanged_t3421641282 ** get_address_of_m_OnStateChanged_1() { return &___m_OnStateChanged_1; }
	inline void set_m_OnStateChanged_1(StateChanged_t3421641282 * value)
	{
		___m_OnStateChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnStateChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CullingGroup
struct CullingGroup_t635124431_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
// Native definition for COM marshalling of UnityEngine.CullingGroup
struct CullingGroup_t635124431_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
#endif // CULLINGGROUP_T635124431_H
#ifndef CURSORLOCKMODE_T1684718576_H
#define CURSORLOCKMODE_T1684718576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_t1684718576 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_t1684718576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_T1684718576_H
#ifndef COMMANDBUFFER_T2673047760_H
#define COMMANDBUFFER_T2673047760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t2673047760  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t2673047760, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T2673047760_H
#ifndef CAMERAEVENT_T2770737965_H
#define CAMERAEVENT_T2770737965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t2770737965 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t2770737965, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T2770737965_H
#ifndef WEAKREFERENCE_T1069295502_H
#define WEAKREFERENCE_T1069295502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WeakReference
struct  WeakReference_t1069295502  : public RuntimeObject
{
public:
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_t714486722  ___gcHandle_1;

public:
	inline static int32_t get_offset_of_isLongReference_0() { return static_cast<int32_t>(offsetof(WeakReference_t1069295502, ___isLongReference_0)); }
	inline bool get_isLongReference_0() const { return ___isLongReference_0; }
	inline bool* get_address_of_isLongReference_0() { return &___isLongReference_0; }
	inline void set_isLongReference_0(bool value)
	{
		___isLongReference_0 = value;
	}

	inline static int32_t get_offset_of_gcHandle_1() { return static_cast<int32_t>(offsetof(WeakReference_t1069295502, ___gcHandle_1)); }
	inline GCHandle_t714486722  get_gcHandle_1() const { return ___gcHandle_1; }
	inline GCHandle_t714486722 * get_address_of_gcHandle_1() { return &___gcHandle_1; }
	inline void set_gcHandle_1(GCHandle_t714486722  value)
	{
		___gcHandle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKREFERENCE_T1069295502_H
#ifndef UNITYEVENTCALLSTATE_T3379341283_H
#define UNITYEVENTCALLSTATE_T3379341283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventCallState
struct  UnityEventCallState_t3379341283 
{
public:
	// System.Int32 UnityEngine.Events.UnityEventCallState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityEventCallState_t3379341283, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTCALLSTATE_T3379341283_H
#ifndef RAY_T1448970001_H
#define RAY_T1448970001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1448970001 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t329709361  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t329709361  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1448970001, ___m_Origin_0)); }
	inline Vector3_t329709361  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t329709361 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t329709361  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1448970001, ___m_Direction_1)); }
	inline Vector3_t329709361  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t329709361 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t329709361  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1448970001_H
#ifndef CACHEDINVOKABLECALL_1_T1392551900_H
#define CACHEDINVOKABLECALL_1_T1392551900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t1392551900  : public InvokableCall_1_t1041587107
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	float ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t1392551900, ___m_Arg1_1)); }
	inline float get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline float* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(float value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T1392551900_H
#ifndef CACHEDINVOKABLECALL_1_T28204005_H
#define CACHEDINVOKABLECALL_1_T28204005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t28204005  : public InvokableCall_1_t3972206508
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	int32_t ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t28204005, ___m_Arg1_1)); }
	inline int32_t get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline int32_t* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(int32_t value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T28204005_H
#ifndef CACHEDINVOKABLECALL_1_T3765088790_H
#define CACHEDINVOKABLECALL_1_T3765088790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct  CachedInvokableCall_1_t3765088790  : public InvokableCall_1_t3414123997
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	String_t* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3765088790, ___m_Arg1_1)); }
	inline String_t* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline String_t** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(String_t* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3765088790_H
#ifndef CACHEDINVOKABLECALL_1_T98604400_H
#define CACHEDINVOKABLECALL_1_T98604400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct  CachedInvokableCall_1_t98604400  : public InvokableCall_1_t4042606903
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	bool ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t98604400, ___m_Arg1_1)); }
	inline bool get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline bool* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(bool value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T98604400_H
#ifndef CAMERACLEARFLAGS_T1635293836_H
#define CAMERACLEARFLAGS_T1635293836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t1635293836 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t1635293836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T1635293836_H
#ifndef BOUNDS_T197583170_H
#define BOUNDS_T197583170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t197583170 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t329709361  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t329709361  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t197583170, ___m_Center_0)); }
	inline Vector3_t329709361  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t329709361 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t329709361  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t197583170, ___m_Extents_1)); }
	inline Vector3_t329709361  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t329709361 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t329709361  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T197583170_H
#ifndef LOGTYPE_T3958793426_H
#define LOGTYPE_T3958793426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t3958793426 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t3958793426, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T3958793426_H
#ifndef RUNTIMEPLATFORM_T3135153872_H
#define RUNTIMEPLATFORM_T3135153872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t3135153872 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t3135153872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T3135153872_H
#ifndef BINDINGFLAGS_T4292999562_H
#define BINDINGFLAGS_T4292999562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t4292999562 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t4292999562, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T4292999562_H
#ifndef ASYNCOPERATION_T1227466744_H
#define ASYNCOPERATION_T1227466744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1227466744  : public YieldInstruction_t3362187334
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t67027751 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1227466744, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1227466744, ___m_completeCallback_1)); }
	inline Action_1_t67027751 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t67027751 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t67027751 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744_marshaled_pinvoke : public YieldInstruction_t3362187334_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1227466744_marshaled_com : public YieldInstruction_t3362187334_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1227466744_H
#ifndef CUBEMAPFACE_T4057844954_H
#define CUBEMAPFACE_T4057844954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t4057844954 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t4057844954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T4057844954_H
#ifndef PARAMETERINFO_T2372523953_H
#define PARAMETERINFO_T2372523953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t2372523953  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t1960139725 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t2372523953, ___marshalAs_6)); }
	inline UnmanagedMarshal_t1960139725 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t1960139725 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t1960139725 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T2372523953_H
#ifndef MULTICASTDELEGATE_T1280656641_H
#define MULTICASTDELEGATE_T1280656641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1280656641  : public Delegate_t1563516729
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1280656641 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1280656641 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___prev_9)); }
	inline MulticastDelegate_t1280656641 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1280656641 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1280656641 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1280656641, ___kpm_next_10)); }
	inline MulticastDelegate_t1280656641 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1280656641 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1280656641 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1280656641_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3762232676  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3762232676  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3762232676 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3762232676  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1460120061* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3962658540 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3962658540 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3962658540 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1460120061* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1460120061** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1460120061* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3962658540 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3962658540 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3962658540 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3962658540 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3962658540 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3962658540 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3962658540 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T1318052361_H
#define GAMEOBJECT_T1318052361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1318052361  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1318052361_H
#ifndef COMPONENT_T789413749_H
#define COMPONENT_T789413749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t789413749  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T789413749_H
#ifndef MESH_T1621212487_H
#define MESH_T1621212487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t1621212487  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T1621212487_H
#ifndef TEXTURE_T2838694469_H
#define TEXTURE_T2838694469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2838694469  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2838694469_H
#ifndef ASSETBUNDLEREQUEST_T4006347538_H
#define ASSETBUNDLEREQUEST_T4006347538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleRequest
struct  AssetBundleRequest_t4006347538  : public AsyncOperation_t1227466744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t4006347538_marshaled_pinvoke : public AsyncOperation_t1227466744_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t4006347538_marshaled_com : public AsyncOperation_t1227466744_marshaled_com
{
};
#endif // ASSETBUNDLEREQUEST_T4006347538_H
#ifndef DATETIME_T972933412_H
#define DATETIME_T972933412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t972933412 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t457147580  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t972933412, ___ticks_0)); }
	inline TimeSpan_t457147580  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t457147580 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t457147580  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t972933412, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t972933412_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t972933412  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t972933412  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t369357837* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t369357837* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t369357837* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t369357837* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t369357837* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t369357837* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t369357837* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3565237794* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3565237794* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MaxValue_2)); }
	inline DateTime_t972933412  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t972933412 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t972933412  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MinValue_3)); }
	inline DateTime_t972933412  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t972933412 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t972933412  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t369357837* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t369357837** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t369357837* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t369357837* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t369357837** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t369357837* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t369357837* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t369357837** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t369357837* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t369357837* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t369357837** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t369357837* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t369357837* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t369357837** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t369357837* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t369357837* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t369357837** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t369357837* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t369357837* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t369357837** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t369357837* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t3565237794* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t3565237794** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t3565237794* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t3565237794* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t3565237794** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t3565237794* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t972933412_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T972933412_H
#ifndef ASSETBUNDLECREATEREQUEST_T1484565475_H
#define ASSETBUNDLECREATEREQUEST_T1484565475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleCreateRequest
struct  AssetBundleCreateRequest_t1484565475  : public AsyncOperation_t1227466744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1484565475_marshaled_pinvoke : public AsyncOperation_t1227466744_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1484565475_marshaled_com : public AsyncOperation_t1227466744_marshaled_com
{
};
#endif // ASSETBUNDLECREATEREQUEST_T1484565475_H
#ifndef FAILEDTOLOADSCRIPTOBJECT_T227884036_H
#define FAILEDTOLOADSCRIPTOBJECT_T227884036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FailedToLoadScriptObject
struct  FailedToLoadScriptObject_t227884036  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t227884036_marshaled_pinvoke : public Object_t1970767703_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t227884036_marshaled_com : public Object_t1970767703_marshaled_com
{
};
#endif // FAILEDTOLOADSCRIPTOBJECT_T227884036_H
#ifndef LOGGER_T2034979157_H
#define LOGGER_T2034979157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Logger
struct  Logger_t2034979157  : public RuntimeObject
{
public:
	// UnityEngine.ILogHandler UnityEngine.Logger::<logHandler>k__BackingField
	RuntimeObject* ___U3ClogHandlerU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Logger::<logEnabled>k__BackingField
	bool ___U3ClogEnabledU3Ek__BackingField_1;
	// UnityEngine.LogType UnityEngine.Logger::<filterLogType>k__BackingField
	int32_t ___U3CfilterLogTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClogHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Logger_t2034979157, ___U3ClogHandlerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3ClogHandlerU3Ek__BackingField_0() const { return ___U3ClogHandlerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3ClogHandlerU3Ek__BackingField_0() { return &___U3ClogHandlerU3Ek__BackingField_0; }
	inline void set_U3ClogHandlerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3ClogHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClogHandlerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClogEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Logger_t2034979157, ___U3ClogEnabledU3Ek__BackingField_1)); }
	inline bool get_U3ClogEnabledU3Ek__BackingField_1() const { return ___U3ClogEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3ClogEnabledU3Ek__BackingField_1() { return &___U3ClogEnabledU3Ek__BackingField_1; }
	inline void set_U3ClogEnabledU3Ek__BackingField_1(bool value)
	{
		___U3ClogEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t2034979157, ___U3CfilterLogTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CfilterLogTypeU3Ek__BackingField_2() const { return ___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfilterLogTypeU3Ek__BackingField_2() { return &___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline void set_U3CfilterLogTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CfilterLogTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T2034979157_H
#ifndef ARGUMENTNULLEXCEPTION_T970256261_H
#define ARGUMENTNULLEXCEPTION_T970256261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t970256261  : public ArgumentException_t3637419113
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T970256261_H
#ifndef LIGHTPROBES_T380083648_H
#define LIGHTPROBES_T380083648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LightProbes
struct  LightProbes_t380083648  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTPROBES_T380083648_H
#ifndef LIGHTMAPSETTINGS_T1227893130_H
#define LIGHTMAPSETTINGS_T1227893130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LightmapSettings
struct  LightmapSettings_t1227893130  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTMAPSETTINGS_T1227893130_H
#ifndef MATERIAL_T2712136762_H
#define MATERIAL_T2712136762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2712136762  : public Object_t1970767703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2712136762_H
#ifndef PERSISTENTCALL_T1257714207_H
#define PERSISTENTCALL_T1257714207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCall
struct  PersistentCall_t1257714207  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.PersistentCall::m_Target
	Object_t1970767703 * ___m_Target_0;
	// System.String UnityEngine.Events.PersistentCall::m_MethodName
	String_t* ___m_MethodName_1;
	// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::m_Mode
	int32_t ___m_Mode_2;
	// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::m_Arguments
	ArgumentCache_t3186847956 * ___m_Arguments_3;
	// UnityEngine.Events.UnityEventCallState UnityEngine.Events.PersistentCall::m_CallState
	int32_t ___m_CallState_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(PersistentCall_t1257714207, ___m_Target_0)); }
	inline Object_t1970767703 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t1970767703 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t1970767703 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodName_1() { return static_cast<int32_t>(offsetof(PersistentCall_t1257714207, ___m_MethodName_1)); }
	inline String_t* get_m_MethodName_1() const { return ___m_MethodName_1; }
	inline String_t** get_address_of_m_MethodName_1() { return &___m_MethodName_1; }
	inline void set_m_MethodName_1(String_t* value)
	{
		___m_MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodName_1), value);
	}

	inline static int32_t get_offset_of_m_Mode_2() { return static_cast<int32_t>(offsetof(PersistentCall_t1257714207, ___m_Mode_2)); }
	inline int32_t get_m_Mode_2() const { return ___m_Mode_2; }
	inline int32_t* get_address_of_m_Mode_2() { return &___m_Mode_2; }
	inline void set_m_Mode_2(int32_t value)
	{
		___m_Mode_2 = value;
	}

	inline static int32_t get_offset_of_m_Arguments_3() { return static_cast<int32_t>(offsetof(PersistentCall_t1257714207, ___m_Arguments_3)); }
	inline ArgumentCache_t3186847956 * get_m_Arguments_3() const { return ___m_Arguments_3; }
	inline ArgumentCache_t3186847956 ** get_address_of_m_Arguments_3() { return &___m_Arguments_3; }
	inline void set_m_Arguments_3(ArgumentCache_t3186847956 * value)
	{
		___m_Arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arguments_3), value);
	}

	inline static int32_t get_offset_of_m_CallState_4() { return static_cast<int32_t>(offsetof(PersistentCall_t1257714207, ___m_CallState_4)); }
	inline int32_t get_m_CallState_4() const { return ___m_CallState_4; }
	inline int32_t* get_address_of_m_CallState_4() { return &___m_CallState_4; }
	inline void set_m_CallState_4(int32_t value)
	{
		___m_CallState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALL_T1257714207_H
#ifndef TOUCH_T1822905180_H
#define TOUCH_T1822905180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1822905180 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t3057062568  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t3057062568  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t3057062568  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Position_1)); }
	inline Vector2_t3057062568  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t3057062568 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t3057062568  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_RawPosition_2)); }
	inline Vector2_t3057062568  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t3057062568 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t3057062568  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_PositionDelta_3)); }
	inline Vector2_t3057062568  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t3057062568 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t3057062568  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1822905180, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1822905180_H
#ifndef LOWMEMORYCALLBACK_T2129875537_H
#define LOWMEMORYCALLBACK_T2129875537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LowMemoryCallback
struct  LowMemoryCallback_t2129875537  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWMEMORYCALLBACK_T2129875537_H
#ifndef PREDICATE_1_T2877392878_H
#define PREDICATE_1_T2877392878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct  Predicate_1_t2877392878  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2877392878_H
#ifndef LOGCALLBACK_T657888911_H
#define LOGCALLBACK_T657888911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LogCallback
struct  LogCallback_t657888911  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T657888911_H
#ifndef RENDERTEXTURE_T3361574884_H
#define RENDERTEXTURE_T3361574884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t3361574884  : public Texture_t2838694469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T3361574884_H
#ifndef BEHAVIOUR_T2441856611_H
#define BEHAVIOUR_T2441856611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2441856611  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2441856611_H
#ifndef CAMERACALLBACK_T2620733104_H
#define CAMERACALLBACK_T2620733104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t2620733104  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACALLBACK_T2620733104_H
#ifndef UNITYACTION_T1298730314_H
#define UNITYACTION_T1298730314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t1298730314  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T1298730314_H
#ifndef MESHFILTER_T1334808991_H
#define MESHFILTER_T1334808991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t1334808991  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T1334808991_H
#ifndef TRANSFORM_T532597831_H
#define TRANSFORM_T532597831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t532597831  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T532597831_H
#ifndef ASYNCCALLBACK_T869574496_H
#define ASYNCCALLBACK_T869574496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t869574496  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T869574496_H
#ifndef CSSMEASUREFUNC_T2617999110_H
#define CSSMEASUREFUNC_T2617999110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureFunc
struct  CSSMeasureFunc_t2617999110  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREFUNC_T2617999110_H
#ifndef DISPLAYSUPDATEDDELEGATE_T850082040_H
#define DISPLAYSUPDATEDDELEGATE_T850082040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display/DisplaysUpdatedDelegate
struct  DisplaysUpdatedDelegate_t850082040  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYSUPDATEDDELEGATE_T850082040_H
#ifndef STATECHANGED_T3421641282_H
#define STATECHANGED_T3421641282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup/StateChanged
struct  StateChanged_t3421641282  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATECHANGED_T3421641282_H
#ifndef ACTION_1_T67027751_H
#define ACTION_1_T67027751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t67027751  : public MulticastDelegate_t1280656641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T67027751_H
#ifndef RENDERER_T265431926_H
#define RENDERER_T265431926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t265431926  : public Component_t789413749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T265431926_H
#ifndef MESHRENDERER_T2414658676_H
#define MESHRENDERER_T2414658676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t2414658676  : public Renderer_t265431926
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T2414658676_H
#ifndef RECTTRANSFORM_T15861704_H
#define RECTTRANSFORM_T15861704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t15861704  : public Transform_t532597831
{
public:

public:
};

struct RectTransform_t15861704_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3527255582 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t15861704_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t3527255582 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t3527255582 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t3527255582 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T15861704_H
#ifndef LIGHT_T3883945566_H
#define LIGHT_T3883945566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Light
struct  Light_t3883945566  : public Behaviour_t2441856611
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_2;

public:
	inline static int32_t get_offset_of_m_BakedIndex_2() { return static_cast<int32_t>(offsetof(Light_t3883945566, ___m_BakedIndex_2)); }
	inline int32_t get_m_BakedIndex_2() const { return ___m_BakedIndex_2; }
	inline int32_t* get_address_of_m_BakedIndex_2() { return &___m_BakedIndex_2; }
	inline void set_m_BakedIndex_2(int32_t value)
	{
		___m_BakedIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHT_T3883945566_H
#ifndef GUILAYER_T2694382279_H
#define GUILAYER_T2694382279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t2694382279  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T2694382279_H
#ifndef CAMERA_T989002943_H
#define CAMERA_T989002943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t989002943  : public Behaviour_t2441856611
{
public:

public:
};

struct Camera_t989002943_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t2620733104 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t2620733104 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t2620733104 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t2620733104 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t2620733104 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t2620733104 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t2620733104 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t2620733104 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t989002943_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t2620733104 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t2620733104 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t2620733104 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T989002943_H
#ifndef GUIELEMENT_T1520723180_H
#define GUIELEMENT_T1520723180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t1520723180  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T1520723180_H
#ifndef MONOBEHAVIOUR_T3829899482_H
#define MONOBEHAVIOUR_T3829899482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3829899482  : public Behaviour_t2441856611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3829899482_H
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t2576915489  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t1682423392  m_Items[1];

public:
	inline Keyframe_t1682423392  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t1682423392 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t1682423392  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t1682423392  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t1682423392 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t1682423392  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t1568665923  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t1460120061  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1251203158  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t4152270991 * m_Items[1];

public:
	inline RequireComponent_t4152270991 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RequireComponent_t4152270991 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t4152270991 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RequireComponent_t4152270991 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RequireComponent_t4152270991 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RequireComponent_t4152270991 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t56197584  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t1081793821 * m_Items[1];

public:
	inline DisallowMultipleComponent_t1081793821 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t1081793821 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t1081793821 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisallowMultipleComponent_t1081793821 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t1081793821 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisallowMultipleComponent_t1081793821 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t3106130354  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t814488931 * m_Items[1];

public:
	inline ExecuteInEditMode_t814488931 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExecuteInEditMode_t814488931 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t814488931 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExecuteInEditMode_t814488931 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExecuteInEditMode_t814488931 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExecuteInEditMode_t814488931 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t3897890150  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t989002943 * m_Items[1];

public:
	inline Camera_t989002943 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t989002943 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t989002943 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t989002943 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t989002943 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t989002943 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t2297631236  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) intptr_t m_Items[1];

public:
	inline intptr_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline intptr_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, intptr_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline intptr_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline intptr_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, intptr_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t3818835545  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Display_t882507400 * m_Items[1];

public:
	inline Display_t882507400 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Display_t882507400 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Display_t882507400 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Display_t882507400 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Display_t882507400 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Display_t882507400 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t4074997356  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t2372523953 * m_Items[1];

public:
	inline ParameterInfo_t2372523953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t2372523953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t2372523953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t2372523953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t2372523953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t2372523953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3541603165  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t1946944212  m_Items[1];

public:
	inline ParameterModifier_t1946944212  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t1946944212 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t1946944212  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t1946944212  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t1946944212 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t1946944212  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rendering.SphericalHarmonicsL2[]
struct SphericalHarmonicsL2U5BU5D_t2505359721  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SphericalHarmonicsL2_t298540216  m_Items[1];

public:
	inline SphericalHarmonicsL2_t298540216  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SphericalHarmonicsL2_t298540216 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SphericalHarmonicsL2_t298540216  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline SphericalHarmonicsL2_t298540216  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SphericalHarmonicsL2_t298540216 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SphericalHarmonicsL2_t298540216  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3287329517  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3565237794  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t974944492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t329709361  m_Items[1];

public:
	inline Vector3_t329709361  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t329709361  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t329709361 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t329709361  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t2564234830  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_t380635127  m_Items[1];

public:
	inline Vector4_t380635127  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_t380635127 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_t380635127  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_t380635127  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_t380635127 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_t380635127  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1392199097  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t3057062568  m_Items[1];

public:
	inline Vector2_t3057062568  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t3057062568 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t3057062568  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t3057062568  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t3057062568 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t3057062568  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t1505762612  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t2788147849  m_Items[1];

public:
	inline Color32_t2788147849  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t2788147849 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t2788147849  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t2788147849  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t2788147849 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t2788147849  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m153633395_gshared (Action_1_t3509626261 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m4153389746_gshared (Stack_1_t3164505761 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1290809190_gshared (Stack_1_t3164505761 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m3857779619_gshared (Stack_1_t3164505761 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1963645566_gshared (Stack_1_t3164505761 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2233273281_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4064363414_gshared (List_1_t185548372 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t1568665923* List_1_ToArray_m2293037125_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  RuntimeObject * AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3341331714_gshared (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m4211633611_gshared (Dictionary_2_t1001102904 * __this, intptr_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3087788102_gshared (Dictionary_2_t1001102904 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2669777438_gshared (List_1_t185548372 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2861140108_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1439624903_gshared (Predicate_1_t3764605111 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C"  int32_t List_1_RemoveAll_m1507527317_gshared (List_1_t185548372 * __this, Predicate_1_t3764605111 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m2457597973_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m2912834934_gshared (List_1_t185548372 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1503881081_gshared (CachedInvokableCall_1_t1392551900 * __this, Object_t1970767703 * p0, MethodInfo_t * p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1923180212_gshared (CachedInvokableCall_1_t28204005 * __this, Object_t1970767703 * p0, MethodInfo_t * p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1470639350_gshared (CachedInvokableCall_1_t4199264408 * __this, Object_t1970767703 * p0, MethodInfo_t * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3785938189_gshared (CachedInvokableCall_1_t98604400 * __this, Object_t1970767703 * p0, MethodInfo_t * p1, bool p2, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1785309417  List_1_GetEnumerator_m827570881_gshared (List_1_t185548372 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m4118870276_gshared (Enumerator_t1785309417 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m455850545_gshared (Enumerator_t1785309417 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m112614151_gshared (Enumerator_t1785309417 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::SafeLength<System.Int32>(System.Collections.Generic.List`1<T>)
extern "C"  int32_t Mesh_SafeLength_TisInt32_t499004851_m2496825826_gshared (Mesh_t1621212487 * __this, List_1_t309455265 * ___values0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t974944492* Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, Vector3U5BU5D_t974944492* ___values1, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t2564234830* Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t1392199097* Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, Vector2U5BU5D_t1392199097* ___values1, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t1505762612* Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector3_t329709361_m1783659180_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, List_1_t140159775 * ___values1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector4_t380635127_m429250604_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, List_1_t191085541 * ___values1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440_gshared (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, List_1_t2598598263 * ___values3, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797_gshared (Mesh_t1621212487 * __this, int32_t ___uvIndex0, int32_t ___dim1, List_1_t2867512982 * ___uvs2, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m4233958761 (Attribute_t3852256153 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2095069727 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m2839859195 (AnimationCurve_t2429328822 * __this, KeyframeU5BU5D_t2576915489* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2672989456 (AnimationCurve_t2429328822 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2450496781 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m2854184373 (LowMemoryCallback_t2129875537 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m1340267840 (LogCallback_t657888911 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m3074312817 (UnityAction_t1298730314 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m872028291 (AsyncOperation_t1227466744 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.AsyncOperation>::Invoke(!0)
#define Action_1_Invoke_m3655672843(__this, p0, method) ((  void (*) (Action_1_t67027751 *, AsyncOperation_t1227466744 *, const RuntimeMethod*))Action_1_Invoke_m153633395_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m1150277029(__this, method) ((  void (*) (Stack_1_t3308188911 *, const RuntimeMethod*))Stack_1__ctor_m4153389746_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
#define Stack_1_Push_m1339590981(__this, p0, method) ((  void (*) (Stack_1_t3308188911 *, Type_t *, const RuntimeMethod*))Stack_1_Push_m1290809190_gshared)(__this, p0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1777045776 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3762232676  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m2893246736(__this, method) ((  Type_t * (*) (Stack_1_t3308188911 *, const RuntimeMethod*))Stack_1_Pop_m3857779619_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m41169841(__this, method) ((  int32_t (*) (Stack_1_t3308188911 *, const RuntimeMethod*))Stack_1_get_Count_m1963645566_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
#define List_1__ctor_m3649843648(__this, method) ((  void (*) (List_1_t329231522 *, const RuntimeMethod*))List_1__ctor_m2233273281_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
#define List_1_Add_m2325233572(__this, p0, method) ((  void (*) (List_1_t329231522 *, Type_t *, const RuntimeMethod*))List_1_Add_m4064363414_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m2346909423(__this, method) ((  TypeU5BU5D_t1460120061* (*) (List_1_t329231522 *, const RuntimeMethod*))List_1_ToArray_m2293037125_gshared)(__this, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2771548650_m2442461925(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t2771548650 * (*) (RuntimeObject * /* static, unused */, Type_t *, const RuntimeMethod*))AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m3341331714_gshared)(__this /* static, unused */, ___klass0, method)
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3190858536 (DefaultExecutionOrder_t2771548650 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3701326763 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t329709361  Vector3_op_Multiply_m3838130503 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m656227705 (Bounds_t197583170 * __this, Vector3_t329709361  ___center0, Vector3_t329709361  ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t329709361  Bounds_get_center_m1502788110 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m1545354540 (Vector3_t329709361 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t329709361  Bounds_get_extents_m756540168 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m2760763607 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m3669777023 (Vector3_t329709361 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m2379144132 (Bounds_t197583170 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1606194673 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t329709361  Bounds_get_size_m4147319483 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m4242979136 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2505371482 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_op_Subtraction_m1019738807 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___a0, Vector3_t329709361  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t329709361  Bounds_get_min_m3422049026 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_op_Addition_m2206773101 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___a0, Vector3_t329709361  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t329709361  Bounds_get_max_m2087102622 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m471342955 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___lhs0, Vector3_t329709361  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3048700048 (RuntimeObject * __this /* static, unused */, Bounds_t197583170  ___lhs0, Bounds_t197583170  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m261817242 (Bounds_t197583170 * __this, Vector3_t329709361  ___min0, Vector3_t329709361  ___max1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_Min_m950630171 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___lhs0, Vector3_t329709361  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Vector3_Max_m3644089585 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___lhs0, Vector3_t329709361  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m2628864493 (Bounds_t197583170 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m1584616489 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1568665923* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m1643044461 (Bounds_t197583170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2015992041 (Behaviour_t2441856611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m681291272 (Camera_t989002943 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m2719568275 (Camera_t989002943 * __this, Matrix4x4_t2375577114 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Ray_t1448970001 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m1414690239 (CameraCallback_t2620733104 * __this, Camera_t989002943 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_INTERNAL_CALL_RaycastTry_m1214413461 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Ray_t1448970001 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_INTERNAL_CALL_RaycastTry2D_m1667450469 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Ray_t1448970001 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m328945537 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m551863488 (Color_t460381780 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3833972762 (Color_t460381780 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m2177171216 (Color_t460381780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t380635127  Color_op_Implicit_m174303393 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3672563191 (Vector4_t380635127 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m44703584 (Color_t460381780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m54068117 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m1644359303 (Color_t460381780 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m2775808155 (RuntimeObject * __this /* static, unused */, Vector4_t380635127  ___lhs0, Vector4_t380635127  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2122253184 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m3770092030 (Vector4_t380635127 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m662029646 (Color32_t2788147849 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m2492342775 (Color32_t2788147849 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m132342697 (Object_t1970767703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1318052361 * Component_get_gameObject_m2399756717 (Component_t789413749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t789413749 * GameObject_GetComponent_m802188666 (GameObject_t1318052361 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t789413749 * GameObject_GetComponentInChildren_m36051739 (GameObject_t1318052361 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t789413749 * GameObject_GetComponentInParent_m3498674876 (GameObject_t1318052361 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m1131678872 (Component_t789413749 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m3635218774 (YieldInstruction_t3362187334 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m4209904503 (Coroutine_t4212313979 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t3542252173  CSSMeasureFunc_Invoke_m890182314 (CSSMeasureFunc_t2617999110 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1067134071(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1695300448 *, intptr_t, WeakReference_t1069295502 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m4211633611_gshared)(__this, p0, p1, method)
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2617999110 * Native_CSSNodeGetMeasureFunc_m1865278247 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m1639871891 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::.ctor()
#define Dictionary_2__ctor_m499092701(__this, method) ((  void (*) (Dictionary_2_t1695300448 *, const RuntimeMethod*))Dictionary_2__ctor_m3087788102_gshared)(__this, method)
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m4173155962 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m634800622 (CullingGroup_t635124431 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m2850170309 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m962163523 (StateChanged_t3421641282 * __this, CullingGroupEvent_t2298849179  ___sphere0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m1064451884 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m2911506489 (DebugLogHandler_t2136690766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m4124516248 (Logger_t2034979157 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1554370993 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t1568665923* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m514723521 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t1970767703 * ___obj2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m2210859879 (RuntimeObject * __this /* static, unused */, Exception_t2123675094 * ___exception0, Object_t1970767703 * ___obj1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int32)
extern "C"  void IntPtr__ctor_m757086868 (intptr_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m1851363389 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m3500243592 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m4048331529 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m911212561 (Display_t882507400 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m2818451955 (DisplaysUpdatedDelegate_t850082040 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m85349829 (Display_t882507400 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m2840871663 (DrivenRectTransformTracker_t4212122797 * __this, Object_t1970767703 * ___driver0, RectTransform_t15861704 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m1182552784 (DrivenRectTransformTracker_t4212122797 * __this, bool ___revertValues0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m2252913897 (DrivenRectTransformTracker_t4212122797 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m1440728790 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m170176446 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m2267115291 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1618205500 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1375684648 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m1227504536 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m2499549447 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m996870465 (ArgumentNullException_t970256261 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m2270113079 (Delegate_t1563516729 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m1116264796 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4170278078 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___x0, Object_t1970767703 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2684183300 (BaseInvokableCall_t3782853021 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t1563516729 * NetFxCoreExtensions_CreateDelegate_m3294389970 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m3264530938 (InvokableCall_t1532986675 * __this, UnityAction_t1298730314 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m3308847879 (BaseInvokableCall_t3782853021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1563516729 * Delegate_Combine_m645997588 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * p0, Delegate_t1563516729 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1563516729 * Delegate_Remove_m2405839985 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * p0, Delegate_t1563516729 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m3200287249 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * ___delegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m2563659778 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m3136659381(__this, method) ((  void (*) (List_1_t3593303435 *, const RuntimeMethod*))List_1__ctor_m2233273281_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0)
#define List_1_Add_m166741303(__this, p0, method) ((  void (*) (List_1_t3593303435 *, BaseInvokableCall_t3782853021 *, const RuntimeMethod*))List_1_Add_m4064363414_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m532699242(__this, p0, method) ((  BaseInvokableCall_t3782853021 * (*) (List_1_t3593303435 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2669777438_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m4034068867(__this, method) ((  int32_t (*) (List_1_t3593303435 *, const RuntimeMethod*))List_1_get_Count_m2861140108_gshared)(__this, method)
// System.Void System.Predicate`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2108898755(__this, p0, p1, method) ((  void (*) (Predicate_1_t2877392878 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m1439624903_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<!0>)
#define List_1_RemoveAll_m3245126275(__this, p0, method) ((  int32_t (*) (List_1_t3593303435 *, Predicate_1_t2877392878 *, const RuntimeMethod*))List_1_RemoveAll_m1507527317_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m2582543520(__this, method) ((  void (*) (List_1_t3593303435 *, const RuntimeMethod*))List_1_Clear_m2457597973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m1581173574(__this, p0, method) ((  void (*) (List_1_t3593303435 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m2912834934_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m27259610 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t1970767703 * PersistentCall_get_target_m3928746700 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m180845067 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m2595158952 (UnityEventBase_t2204896975 * __this, PersistentCall_t1257714207 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t3782853021 * PersistentCall_GetObjectCall_m2309820747 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t3186847956 * ___arguments2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m1195387480 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m1503881081(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t1392551900 *, Object_t1970767703 *, MethodInfo_t *, float, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1503881081_gshared)(__this, p0, p1, p2, method)
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2990531235 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m1923180212(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t28204005 *, Object_t1970767703 *, MethodInfo_t *, int32_t, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1923180212_gshared)(__this, p0, p1, p2, method)
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m2954742800 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m3388188837(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3765088790 *, Object_t1970767703 *, MethodInfo_t *, String_t*, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1470639350_gshared)(__this, p0, p1, p2, method)
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m611899758 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m3785938189(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t98604400 *, Object_t1970767703 *, MethodInfo_t *, bool, const RuntimeMethod*))CachedInvokableCall_1__ctor_m3785938189_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m3813870726 (InvokableCall_t1532986675 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m1216471199 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C"  ConstructorInfo_t1575920019 * Type_GetConstructor_m3152678330 (Type_t * __this, TypeU5BU5D_t1460120061* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t1970767703 * ArgumentCache_get_unityObjectArgument_m2115528201 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m3688535302 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C"  RuntimeObject * ConstructorInfo_Invoke_m3601249132 (ConstructorInfo_t1575920019 * __this, ObjectU5BU5D_t1568665923* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m852503502(__this, method) ((  void (*) (List_1_t1068164621 *, const RuntimeMethod*))List_1__ctor_m2233273281_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m2460221054(__this, method) ((  Enumerator_t2667925666  (*) (List_1_t1068164621 *, const RuntimeMethod*))List_1_GetEnumerator_m827570881_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m2730847451(__this, method) ((  PersistentCall_t1257714207 * (*) (Enumerator_t2667925666 *, const RuntimeMethod*))Enumerator_get_Current_m4118870276_gshared)(__this, method)
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m3600404012 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t3782853021 * PersistentCall_GetRuntimeCall_m1034035855 (PersistentCall_t1257714207 * __this, UnityEventBase_t2204896975 * ___theEvent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m1525842468 (InvokableCallList_t1231629294 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m4259834011(__this, method) ((  bool (*) (Enumerator_t2667925666 *, const RuntimeMethod*))Enumerator_MoveNext_m455850545_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m2021464298(__this, method) ((  void (*) (Enumerator_t2667925666 *, const RuntimeMethod*))Enumerator_Dispose_m112614151_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m3145387590 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t3782853021 * UnityEvent_GetDelegate_m3771139711 (RuntimeObject * __this /* static, unused */, UnityAction_t1298730314 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3089987405 (UnityEventBase_t2204896975 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m3893544593 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1460120061* ___argumentTypes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m2759934649 (InvokableCall_t1532986675 * __this, UnityAction_t1298730314 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern "C"  List_1_t3593303435 * UnityEventBase_PrepareInvoke_m665515233 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::Invoke()
extern "C"  void InvokableCall_Invoke_m2051501297 (InvokableCall_t1532986675 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m3375537592 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m3543885354 (PersistentCallGroup_t1952737969 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m4212958525 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t3186847956 * PersistentCall_get_arguments_m974501651 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m3685097626 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1231245736 (UnityEventBase_t2204896975 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m3370527310 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m3520909384 (PersistentCallGroup_t1952737969 * __this, InvokableCallList_t1231629294 * ___invokableList0, UnityEventBase_t2204896975 * ___unityEventBase1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m1729791600 (InvokableCallList_t1231629294 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m2959324581 (InvokableCallList_t1231629294 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m834582307 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t3593303435 * InvokableCallList_PrepareInvoke_m3324959179 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Object::ToString()
extern "C"  String_t* Object_ToString_m4205420029 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m414952842 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * Type_GetMethod_m3858208996 (Type_t * __this, String_t* p0, int32_t p1, Binder_t3602727638 * p2, TypeU5BU5D_t1460120061* p3, ParameterModifierU5BU5D_t3541603165* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m2375556458 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m1978623761 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m3314607428 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m2019186464 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m1547560917 (ScriptableRenderContext_t186008635 * __this, intptr_t ___ptr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m185655714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m2219017391 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___mono0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t789413749 * GameObject_AddComponent_m1275919783 (GameObject_t1318052361 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t789413749 * GameObject_Internal_AddComponentWithType_m4164311814 (GameObject_t1318052361 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m1552560054 (Gradient_t1395232743 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m117154722 (Gradient_t1395232743 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t1520723180 * GUILayer_INTERNAL_CALL_HitTest_m3932656170 (RuntimeObject * __this /* static, unused */, GUILayer_t2694382279 * ___self0, Vector3_t329709361 * ___screenPosition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3365316696 (PropertyAttribute_t69305138 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m2041535329 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m531055500 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m850486359 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t1822905180 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m3760724342 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m2255230934 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m1703762465 (DefaultValueAttribute_t3593402498 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Attribute::GetHashCode()
extern "C"  int32_t Attribute_GetHashCode_m3297037235 (Attribute_t3852256153 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m2536903317 (LocalNotification_t2999868044 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.DateTimeKind)
extern "C"  void DateTime__ctor_m2981095106 (DateTime_t972933412 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m1939839433 (DateTime_t972933412 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m3109502285 (RemoteNotification_t163380436 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m1411151799 (Logger_t2034979157 * __this, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m3126018738 (Logger_t2034979157 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m1146460364 (Logger_t2034979157 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2598788189 (Logger_t2034979157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m866625208 (Logger_t2034979157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m2476994406 (Logger_t2034979157 * __this, int32_t ___logType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1737771075 (Logger_t2034979157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m1753770121 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m1624032228 (ArgumentNullException_t970256261 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m3645081522 (ArgumentException_t3637419113 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3008674881 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m730854658 (RuntimeObject * __this /* static, unused */, Stream_t3046340190 * ___stream0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m2981100349 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___mono0, Material_t2712136762 * ___source1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m2365962888 (Material_t2712136762 * __this, String_t* ___name0, Color_t460381780  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t2838694469 * Material_GetTexture_m344891307 (Material_t2712136762 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColorImpl_m1843334451 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___self0, int32_t ___nameID1, Color_t460381780 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Material_INTERNAL_CALL_SetMatrixImpl_m3700682037 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___self0, int32_t ___nameID1, Matrix4x4_t2375577114 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3888350337 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m2991897582 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C"  void Material_SetInt_m1768491861 (Material_t2712136762 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
extern "C"  void Material_SetIntImpl_m3742874915 (Material_t2712136762 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m2173280049 (Material_t2712136762 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColorImpl_m2122336602 (Material_t2712136762 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m3498861241 (Material_t2712136762 * __this, int32_t ___nameID0, Matrix4x4_t2375577114  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrixImpl_m113484655 (Material_t2712136762 * __this, int32_t ___nameID0, Matrix4x4_t2375577114  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m2280997817 (Material_t2712136762 * __this, int32_t ___nameID0, Texture_t2838694469 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTextureImpl_m3302679637 (Material_t2712136762 * __this, int32_t ___nameID0, Texture_t2838694469 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t2838694469 * Material_GetTexture_m256980722 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern "C"  Texture_t2838694469 * Material_GetTextureImpl_m1718302891 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
extern "C"  void MaterialPropertyBlock_InitBlock_m3286158498 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
extern "C"  void MaterialPropertyBlock_DestroyBlock_m1450478 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241 (RuntimeObject * __this /* static, unused */, MaterialPropertyBlock_t3417007309 * ___self0, int32_t ___nameID1, Color_t460381780 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m292668400 (MaterialPropertyBlock_t3417007309 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColorImpl_m4025622658 (MaterialPropertyBlock_t3417007309 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
extern "C"  double Math_Log_m395471811 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m1074792940 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1749346724 (RuntimeObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m3113358119 (Matrix4x4_t2375577114 * __this, Vector4_t380635127  ___column00, Vector4_t380635127  ___column11, Vector4_t380635127  ___column22, Vector4_t380635127  ___column33, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m1078286175 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___pos0, Quaternion_t2761156409 * ___q1, Vector3_t329709361 * ___s2, Matrix4x4_t2375577114 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m592394008 (Matrix4x4_t2375577114 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m1739517094 (Matrix4x4_t2375577114 * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m942837760 (Matrix4x4_t2375577114 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1674402704 (Matrix4x4_t2375577114 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m3132318046 (IndexOutOfRangeException_t3921978895 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t380635127  Matrix4x4_GetColumn_m1167652957 (Matrix4x4_t2375577114 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m2403490898 (Matrix4x4_t2375577114 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m2175095089 (Vector4_t380635127 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m2605048655 (Matrix4x4_t2375577114 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m3548282205 (Matrix4x4_t2375577114 * __this, int32_t ___index0, Vector4_t380635127  ___column1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Matrix4x4_MultiplyPoint3x4_m2159497023 (Matrix4x4_t2375577114 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m782477224 (Matrix4x4_t2375577114 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m852979627 (RuntimeObject * __this /* static, unused */, Mesh_t1621212487 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m164793241 (Mesh_t1621212487 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTriangles_m1728167629 (Mesh_t1621212487 * __this, List_1_t309455265 * ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshTriangles_m3893594630 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  RuntimeArray * Mesh_ExtractArrayFromList_m3063324901 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___list0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::SafeLength<System.Int32>(System.Collections.Generic.List`1<T>)
#define Mesh_SafeLength_TisInt32_t499004851_m2496825826(__this, ___values0, method) ((  int32_t (*) (Mesh_t1621212487 *, List_1_t309455265 *, const RuntimeMethod*))Mesh_SafeLength_TisInt32_t499004851_m2496825826_gshared)(__this, ___values0, method)
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m2009775185 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m284991026 (Mesh_t1621212487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m1183388312 (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___arraySize4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessChannel_m4229761159 (Mesh_t1621212487 * __this, int32_t ___ch0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429(__this, ___channel0, method) ((  Vector3U5BU5D_t974944492* (*) (Mesh_t1621212487 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429_gshared)(__this, ___channel0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,T[])
#define Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1621212487 *, int32_t, Vector3U5BU5D_t974944492*, const RuntimeMethod*))Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940_gshared)(__this, ___channel0, ___values1, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490(__this, ___channel0, method) ((  Vector4U5BU5D_t2564234830* (*) (Mesh_t1621212487 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393(__this, ___channel0, method) ((  Vector2U5BU5D_t1392199097* (*) (Mesh_t1621212487 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_gshared)(__this, ___channel0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,T[])
#define Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1621212487 *, int32_t, Vector2U5BU5D_t1392199097*, const RuntimeMethod*))Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259_gshared)(__this, ___channel0, ___values1, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
#define Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690(__this, ___channel0, ___format1, ___dim2, method) ((  Color32U5BU5D_t1505762612* (*) (Mesh_t1621212487 *, int32_t, int32_t, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisVector3_t329709361_m1783659180(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1621212487 *, int32_t, List_1_t140159775 *, const RuntimeMethod*))Mesh_SetListForChannel_TisVector3_t329709361_m1783659180_gshared)(__this, ___channel0, ___values1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisVector4_t380635127_m429250604(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1621212487 *, int32_t, List_1_t191085541 *, const RuntimeMethod*))Mesh_SetListForChannel_TisVector4_t380635127_m429250604_gshared)(__this, ___channel0, ___values1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440(__this, ___channel0, ___format1, ___dim2, ___values3, method) ((  void (*) (Mesh_t1621212487 *, int32_t, int32_t, int32_t, List_1_t2598598263 *, const RuntimeMethod*))Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440_gshared)(__this, ___channel0, ___format1, ___dim2, ___values3, method)
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
#define Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797(__this, ___uvIndex0, ___dim1, ___uvs2, method) ((  void (*) (Mesh_t1621212487 *, int32_t, int32_t, List_1_t2867512982 *, const RuntimeMethod*))Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797_gshared)(__this, ___uvIndex0, ___dim1, ___uvs2, method)
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m461114069 (Object_t1970767703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2126999818 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m627890074 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern "C"  void Mesh_PrintErrorCantAccessIndices_m23294446 (Mesh_t1621212487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m1718999019 (Mesh_t1621212487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m1993805588 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t1970767703 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern "C"  bool Mesh_CheckCanAccessSubmesh_m3483679341 (Mesh_t1621212487 * __this, int32_t ___submesh0, bool ___errorAboutTriangles1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::SafeLength(System.Array)
extern "C"  int32_t Mesh_SafeLength_m930608131 (Mesh_t1621212487 * __this, RuntimeArray * ___values0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetTrianglesImpl_m3619734739 (Mesh_t1621212487 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshIndices_m1492168043 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3565237794* Mesh_GetIndicesImpl_m3232595394 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern "C"  void Mesh_ClearImpl_m1737913970 (Mesh_t1621212487 * __this, bool ___keepVertexLayout0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
extern "C"  void Mesh_RecalculateBoundsImpl_m4129815417 (Mesh_t1621212487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateNormalsImpl()
extern "C"  void Mesh_RecalculateNormalsImpl_m3938918416 (Mesh_t1621212487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t4212313979 * MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m813737111 (MonoBehaviour_t3829899482 * __this, Coroutine_t4212313979 * ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m2980223260 (MonoPInvokeCallbackAttribute_t2444501850 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m2831714886 (AddComponentMenu_t67651563 * __this, String_t* ___menuName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m1747650321 (AddComponentMenu_t67651563 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke(const AnimationCurve_t2429328822& unmarshaled, AnimationCurve_t2429328822_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke_back(const AnimationCurve_t2429328822_marshaled_pinvoke& marshaled, AnimationCurve_t2429328822& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2429328822_marshal_pinvoke_cleanup(AnimationCurve_t2429328822_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2429328822_marshal_com(const AnimationCurve_t2429328822& unmarshaled, AnimationCurve_t2429328822_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2429328822_marshal_com_back(const AnimationCurve_t2429328822_marshaled_com& marshaled, AnimationCurve_t2429328822& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2429328822_marshal_com_cleanup(AnimationCurve_t2429328822_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m192856955 (AnimationCurve_t2429328822 * __this, KeyframeU5BU5D_t2576915489* ___keys0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t2576915489* L_0 = ___keys0;
		AnimationCurve_Init_m2839859195(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3703872163 (AnimationCurve_t2429328822 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m2839859195(__this, (KeyframeU5BU5D_t2576915489*)(KeyframeU5BU5D_t2576915489*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2672989456 (AnimationCurve_t2429328822 * __this, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Cleanup_m2672989456_ftn) (AnimationCurve_t2429328822 *);
	static AnimationCurve_Cleanup_m2672989456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m2672989456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m309822655 (AnimationCurve_t2429328822 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m2672989456(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m2839859195 (AnimationCurve_t2429328822 * __this, KeyframeU5BU5D_t2576915489* ___keys0, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Init_m2839859195_ftn) (AnimationCurve_t2429328822 *, KeyframeU5BU5D_t2576915489*);
	static AnimationCurve_Init_m2839859195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m2839859195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// System.Void UnityEngine.Application::CallLowMemory()
extern "C"  void Application_CallLowMemory_m2711201767 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLowMemory_m2711201767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LowMemoryCallback_t2129875537 * V_0 = NULL;
	{
		LowMemoryCallback_t2129875537 * L_0 = ((Application_t1423836661_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1423836661_il2cpp_TypeInfo_var))->get_lowMemory_0();
		V_0 = L_0;
		LowMemoryCallback_t2129875537 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		LowMemoryCallback_t2129875537 * L_2 = V_0;
		NullCheck(L_2);
		LowMemoryCallback_Invoke_m2854184373(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m1479912797 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isPlaying_m1479912797_ftn) ();
	static Application_get_isPlaying_m1479912797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m1479912797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m4196020230 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isEditor_m4196020230_ftn) ();
	static Application_get_isEditor_m4196020230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m4196020230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m4196840056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Application_get_platform_m4196840056_ftn) ();
	static Application_get_platform_m4196840056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m4196840056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m1044111484 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Application_set_targetFrameRate_m1044111484_ftn) (int32_t);
	static Application_set_targetFrameRate_m1044111484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m1044111484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern "C"  void Application_CallLogCallback_m2863196351 (RuntimeObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m2863196351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t657888911 * V_0 = NULL;
	LogCallback_t657888911 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t657888911 * L_1 = ((Application_t1423836661_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1423836661_il2cpp_TypeInfo_var))->get_s_LogCallbackHandler_1();
		V_0 = L_1;
		LogCallback_t657888911 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t657888911 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m1340267840(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t657888911 * L_7 = ((Application_t1423836661_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1423836661_il2cpp_TypeInfo_var))->get_s_LogCallbackHandlerThreaded_2();
		V_1 = L_7;
		LogCallback_t657888911 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t657888911 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m1340267840(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern "C"  void Application_InvokeOnBeforeRender_m2639803043 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_InvokeOnBeforeRender_m2639803043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t1298730314 * V_0 = NULL;
	{
		UnityAction_t1298730314 * L_0 = ((Application_t1423836661_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1423836661_il2cpp_TypeInfo_var))->get_onBeforeRender_3();
		V_0 = L_0;
		UnityAction_t1298730314 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UnityAction_t1298730314 * L_2 = V_0;
		NullCheck(L_2);
		UnityAction_Invoke_m3074312817(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t657888911 (LogCallback_t657888911 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m1190468250 (LogCallback_t657888911 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m1340267840 (LogCallback_t657888911 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m1340267840((LogCallback_t657888911 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LogCallback_BeginInvoke_m601874442 (LogCallback_t657888911 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t869574496 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m601874442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t3958793426_il2cpp_TypeInfo_var, &___type2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m27028846 (LogCallback_t657888911 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_LowMemoryCallback_t2129875537 (LowMemoryCallback_t2129875537 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LowMemoryCallback__ctor_m3547351986 (LowMemoryCallback_t2129875537 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m2854184373 (LowMemoryCallback_t2129875537 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LowMemoryCallback_Invoke_m2854184373((LowMemoryCallback_t2129875537 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LowMemoryCallback_BeginInvoke_m42994289 (LowMemoryCallback_t2129875537 * __this, AsyncCallback_t869574496 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LowMemoryCallback_EndInvoke_m1255439379 (LowMemoryCallback_t2129875537 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke(const AssetBundleCreateRequest_t1484565475& unmarshaled, AssetBundleCreateRequest_t1484565475_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke_back(const AssetBundleCreateRequest_t1484565475_marshaled_pinvoke& marshaled, AssetBundleCreateRequest_t1484565475& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t1484565475_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_pinvoke_cleanup(AssetBundleCreateRequest_t1484565475_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_com(const AssetBundleCreateRequest_t1484565475& unmarshaled, AssetBundleCreateRequest_t1484565475_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_com_back(const AssetBundleCreateRequest_t1484565475_marshaled_com& marshaled, AssetBundleCreateRequest_t1484565475& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t1484565475_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t1484565475_marshal_com_cleanup(AssetBundleCreateRequest_t1484565475_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke(const AssetBundleRequest_t4006347538& unmarshaled, AssetBundleRequest_t4006347538_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke_back(const AssetBundleRequest_t4006347538_marshaled_pinvoke& marshaled, AssetBundleRequest_t4006347538& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t4006347538_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t4006347538_marshal_pinvoke_cleanup(AssetBundleRequest_t4006347538_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t4006347538_marshal_com(const AssetBundleRequest_t4006347538& unmarshaled, AssetBundleRequest_t4006347538_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t4006347538_marshal_com_back(const AssetBundleRequest_t4006347538_marshaled_com& marshaled, AssetBundleRequest_t4006347538& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t4006347538_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t4006347538_marshal_com_cleanup(AssetBundleRequest_t4006347538_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke(const AsyncOperation_t1227466744& unmarshaled, AsyncOperation_t1227466744_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke_back(const AsyncOperation_t1227466744_marshaled_pinvoke& marshaled, AsyncOperation_t1227466744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t1227466744_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1227466744_marshal_pinvoke_cleanup(AsyncOperation_t1227466744_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1227466744_marshal_com(const AsyncOperation_t1227466744& unmarshaled, AsyncOperation_t1227466744_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t1227466744_marshal_com_back(const AsyncOperation_t1227466744_marshaled_com& marshaled, AsyncOperation_t1227466744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t1227466744_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t67027751>(marshaled.___m_completeCallback_1, Action_1_t67027751_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1227466744_marshal_com_cleanup(AsyncOperation_t1227466744_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m872028291 (AsyncOperation_t1227466744 * __this, const RuntimeMethod* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m872028291_ftn) (AsyncOperation_t1227466744 *);
	static AsyncOperation_InternalDestroy_m872028291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m872028291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m3265287030 (AsyncOperation_t1227466744 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m872028291(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern "C"  void AsyncOperation_InvokeCompletionEvent_m15538063 (AsyncOperation_t1227466744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_InvokeCompletionEvent_m15538063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t67027751 * L_0 = __this->get_m_completeCallback_1();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t67027751 * L_1 = __this->get_m_completeCallback_1();
		NullCheck(L_1);
		Action_1_Invoke_m3655672843(L_1, __this, /*hidden argument*/Action_1_Invoke_m3655672843_RuntimeMethod_var);
		__this->set_m_completeCallback_1((Action_1_t67027751 *)NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3962214426 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3962214426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t3308188911 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t1568665923* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t3308188911 * L_0 = (Stack_1_t3308188911 *)il2cpp_codegen_object_new(Stack_1_t3308188911_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1150277029(L_0, /*hidden argument*/Stack_1__ctor_m1150277029_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t3308188911 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m1339590981(L_1, L_2, /*hidden argument*/Stack_1_Push_m1339590981_RuntimeMethod_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3829899482_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_6) == ((RuntimeObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t3308188911 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m2893246736(L_8, /*hidden argument*/Stack_1_Pop_m2893246736_RuntimeMethod_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t1081793821_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t1568665923* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t1568665923*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t1568665923* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t3308188911 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m41169841(L_16, /*hidden argument*/Stack_1_get_Count_m41169841_RuntimeMethod_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern "C"  TypeU5BU5D_t1460120061* AttributeHelperEngine_GetRequiredComponents_m1719255508 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m1719255508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t329231522 * V_0 = NULL;
	RequireComponentU5BU5D_t1251203158* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t4152270991 * V_3 = NULL;
	RequireComponentU5BU5D_t1251203158* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t1460120061* V_6 = NULL;
	TypeU5BU5D_t1460120061* V_7 = NULL;
	{
		V_0 = (List_1_t329231522 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t4152270991_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1568665923* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t1568665923*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t1251203158*)Castclass((RuntimeObject*)L_2, RequireComponentU5BU5D_t1251203158_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t1251203158* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t1251203158* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t4152270991 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t329231522 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t1251203158* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3829899482_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_12) == ((RuntimeObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t1460120061* L_14 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t4152270991 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1460120061* L_17 = L_14;
		RequireComponent_t4152270991 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t1460120061* L_20 = L_17;
		RequireComponent_t4152270991 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t1460120061* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t329231522 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t329231522 * L_25 = (List_1_t329231522 *)il2cpp_codegen_object_new(List_1_t329231522_il2cpp_TypeInfo_var);
		List_1__ctor_m3649843648(L_25, /*hidden argument*/List_1__ctor_m3649843648_RuntimeMethod_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t4152270991 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t329231522 * L_28 = V_0;
		RequireComponent_t4152270991 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m2325233572(L_28, L_30, /*hidden argument*/List_1_Add_m2325233572_RuntimeMethod_var);
	}

IL_00aa:
	{
		RequireComponent_t4152270991 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t329231522 * L_33 = V_0;
		RequireComponent_t4152270991 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m2325233572(L_33, L_35, /*hidden argument*/List_1_Add_m2325233572_RuntimeMethod_var);
	}

IL_00c1:
	{
		RequireComponent_t4152270991 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t329231522 * L_38 = V_0;
		RequireComponent_t4152270991 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m2325233572(L_38, L_40, /*hidden argument*/List_1_Add_m2325233572_RuntimeMethod_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t1251203158* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3829899482_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_46) == ((RuntimeObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t329231522 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t1460120061*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t329231522 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t1460120061* L_50 = List_1_ToArray_m2346909423(L_49, /*hidden argument*/List_1_ToArray_m2346909423_RuntimeMethod_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t1460120061* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m989208379 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m989208379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t1568665923* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t814488931_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1568665923* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t1568665923*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t1568665923* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3829899482_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m76839880 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m76839880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t2771548650 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t1954971056_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t2771548650 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2771548650_m2442461925(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2771548650_m2442461925_RuntimeMethod_var);
		V_0 = L_1;
		DefaultExecutionOrder_t2771548650 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t2771548650 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m3190858536(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern "C"  void AttributeHelperEngine__cctor_m3903595114 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m3903595114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t1954971056_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t1954971056_il2cpp_TypeInfo_var))->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t56197584*)SZArrayNew(DisallowMultipleComponentU5BU5D_t56197584_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t1954971056_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t1954971056_il2cpp_TypeInfo_var))->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t3106130354*)SZArrayNew(ExecuteInEditModeU5BU5D_t3106130354_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t1954971056_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t1954971056_il2cpp_TypeInfo_var))->set__requireComponentArray_2(((RequireComponentU5BU5D_t1251203158*)SZArrayNew(RequireComponentU5BU5D_t1251203158_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2015992041 (Behaviour_t2441856611 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m3701326763(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m103239282 (Behaviour_t2441856611 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_enabled_m103239282_ftn) (Behaviour_t2441856611 *);
	static Behaviour_get_enabled_m103239282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m103239282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1685040593 (Behaviour_t2441856611 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Behaviour_set_enabled_m1685040593_ftn) (Behaviour_t2441856611 *, bool);
	static Behaviour_set_enabled_m1685040593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m1685040593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m1965684228 (Behaviour_t2441856611 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m1965684228_ftn) (Behaviour_t2441856611 *);
	static Behaviour_get_isActiveAndEnabled_m1965684228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m1965684228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Bindings.UnmarshalledAttribute::.ctor()
extern "C"  void UnmarshalledAttribute__ctor_m2048290248 (UnmarshalledAttribute_t1659112717 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m656227705 (Bounds_t197583170 * __this, Vector3_t329709361  ___center0, Vector3_t329709361  ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds__ctor_m656227705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t329709361  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t329709361  L_1 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m656227705_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___center0, Vector3_t329709361  ___size1, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds__ctor_m656227705(_thisAdjusted, ___center0, ___size1, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m2760763607 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Vector3_t329709361  L_0 = Bounds_get_center_m1502788110(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m1545354540((&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_2 = Bounds_get_extents_m756540168(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m1545354540((&V_1), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_0032;
	}

IL_0032:
	{
		int32_t L_4 = V_2;
		return L_4;
	}
}
extern "C"  int32_t Bounds_GetHashCode_m2760763607_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_GetHashCode_m2760763607(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m2379144132 (Bounds_t197583170 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m2379144132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Bounds_t197583170  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t329709361  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t329709361  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Bounds_t197583170_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0068;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Bounds_t197583170 *)((Bounds_t197583170 *)UnBox(L_1, Bounds_t197583170_il2cpp_TypeInfo_var))));
		Vector3_t329709361  L_2 = Bounds_get_center_m1502788110(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t329709361  L_3 = Bounds_get_center_m1502788110((&V_1), /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m3669777023((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t329709361  L_7 = Bounds_get_extents_m756540168(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector3_t329709361  L_8 = Bounds_get_extents_m756540168((&V_1), /*hidden argument*/NULL);
		Vector3_t329709361  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m3669777023((&V_3), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0062;
	}

IL_0061:
	{
		G_B5_0 = 0;
	}

IL_0062:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0068;
	}

IL_0068:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
extern "C"  bool Bounds_Equals_m2379144132_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_Equals_m2379144132(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t329709361  Bounds_get_center_m1502788110 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Center_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t329709361  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t329709361  Bounds_get_center_m1502788110_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_get_center_m1502788110(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1606194673 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t329709361  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m1606194673_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds_set_center_m1606194673(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t329709361  Bounds_get_size_m4147319483 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_size_m4147319483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Extents_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_1 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t329709361  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t329709361  Bounds_get_size_m4147319483_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_get_size_m4147319483(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m4242979136 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_set_size_m4242979136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t329709361  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_1 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m4242979136_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds_set_size_m4242979136(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t329709361  Bounds_get_extents_m756540168 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = __this->get_m_Extents_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t329709361  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t329709361  Bounds_get_extents_m756540168_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_get_extents_m756540168(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2505371482 (Bounds_t197583170 * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t329709361  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m2505371482_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___value0, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds_set_extents_m2505371482(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t329709361  Bounds_get_min_m3422049026 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_min_m3422049026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = Bounds_get_center_m1502788110(__this, /*hidden argument*/NULL);
		Vector3_t329709361  L_1 = Bounds_get_extents_m756540168(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Vector3_op_Subtraction_m1019738807(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t329709361  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t329709361  Bounds_get_min_m3422049026_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_get_min_m3422049026(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t329709361  Bounds_get_max_m2087102622 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_max_m2087102622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t329709361  L_0 = Bounds_get_center_m1502788110(__this, /*hidden argument*/NULL);
		Vector3_t329709361  L_1 = Bounds_get_extents_m756540168(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Vector3_op_Addition_m2206773101(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t329709361  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t329709361  Bounds_get_max_m2087102622_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_get_max_m2087102622(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3048700048 (RuntimeObject * __this /* static, unused */, Bounds_t197583170  ___lhs0, Bounds_t197583170  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_op_Equality_m3048700048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Vector3_t329709361  L_0 = Bounds_get_center_m1502788110((&___lhs0), /*hidden argument*/NULL);
		Vector3_t329709361  L_1 = Bounds_get_center_m1502788110((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		bool L_2 = Vector3_op_Equality_m471342955(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Vector3_t329709361  L_3 = Bounds_get_extents_m756540168((&___lhs0), /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = Bounds_get_extents_m756540168((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		bool L_5 = Vector3_op_Equality_m471342955(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m2972442665 (RuntimeObject * __this /* static, unused */, Bounds_t197583170  ___lhs0, Bounds_t197583170  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Bounds_t197583170  L_0 = ___lhs0;
		Bounds_t197583170  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m3048700048(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m261817242 (Bounds_t197583170 * __this, Vector3_t329709361  ___min0, Vector3_t329709361  ___max1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_SetMinMax_m261817242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t329709361  L_0 = ___max1;
		Vector3_t329709361  L_1 = ___min0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Vector3_op_Subtraction_m1019738807(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t329709361  L_3 = Vector3_op_Multiply_m3838130503(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m2505371482(__this, L_3, /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = ___min0;
		Vector3_t329709361  L_5 = Bounds_get_extents_m756540168(__this, /*hidden argument*/NULL);
		Vector3_t329709361  L_6 = Vector3_op_Addition_m2206773101(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m1606194673(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m261817242_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___min0, Vector3_t329709361  ___max1, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds_SetMinMax_m261817242(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m2628864493 (Bounds_t197583170 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Encapsulate_m2628864493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t329709361  L_0 = Bounds_get_min_m3422049026(__this, /*hidden argument*/NULL);
		Vector3_t329709361  L_1 = ___point0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t329709361_il2cpp_TypeInfo_var);
		Vector3_t329709361  L_2 = Vector3_Min_m950630171(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t329709361  L_3 = Bounds_get_max_m2087102622(__this, /*hidden argument*/NULL);
		Vector3_t329709361  L_4 = ___point0;
		Vector3_t329709361  L_5 = Vector3_Max_m3644089585(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m261817242(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m2628864493_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	Bounds_Encapsulate_m2628864493(_thisAdjusted, ___point0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m1643044461 (Bounds_t197583170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m1643044461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t329709361  L_1 = __this->get_m_Center_0();
		Vector3_t329709361  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		Vector3_t329709361  L_5 = __this->get_m_Extents_1();
		Vector3_t329709361  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t329709361_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral1971303795, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Bounds_ToString_m1643044461_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t197583170 * _thisAdjusted = reinterpret_cast<Bounds_t197583170 *>(__this + 1);
	return Bounds_ToString_m1643044461(_thisAdjusted, method);
}
// System.Void UnityEngine.Camera::.ctor()
extern "C"  void Camera__ctor_m2542522450 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2015992041(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m2174207718 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_nearClipPlane_m2174207718_ftn) (Camera_t989002943 *);
	static Camera_get_nearClipPlane_m2174207718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m2174207718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m3100038649 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_farClipPlane_m3100038649_ftn) (Camera_t989002943 *);
	static Camera_get_farClipPlane_m3100038649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m3100038649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m431273507 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_depth_m431273507_ftn) (Camera_t989002943 *);
	static Camera_get_depth_m431273507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m431273507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m2989971275 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_cullingMask_m2989971275_ftn) (Camera_t989002943 *);
	static Camera_get_cullingMask_m2989971275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m2989971275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3847497448 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_eventMask_m3847497448_ftn) (Camera_t989002943 *);
	static Camera_get_eventMask_m3847497448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m3847497448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t1344834245  Camera_get_pixelRect_m2697375247 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	Rect_t1344834245  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1344834245  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m681291272(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t1344834245  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t1344834245  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m681291272 (Camera_t989002943 * __this, Rect_t1344834245 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m681291272_ftn) (Camera_t989002943 *, Rect_t1344834245 *);
	static Camera_INTERNAL_get_pixelRect_m681291272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m681291272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t3361574884 * Camera_get_targetTexture_m902197149 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef RenderTexture_t3361574884 * (*Camera_get_targetTexture_m902197149_ftn) (Camera_t989002943 *);
	static Camera_get_targetTexture_m902197149_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m902197149_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	RenderTexture_t3361574884 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m457620729 (Camera_t989002943 * __this, Matrix4x4_t2375577114  ___value0, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m2719568275(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m2719568275 (Camera_t989002943 * __this, Matrix4x4_t2375577114 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m2719568275_ftn) (Camera_t989002943 *, Matrix4x4_t2375577114 *);
	static Camera_INTERNAL_set_projectionMatrix_m2719568275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m2719568275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1432164003 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_clearFlags_m1432164003_ftn) (Camera_t989002943 *);
	static Camera_get_clearFlags_m1432164003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m1432164003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_targetDisplay()
extern "C"  int32_t Camera_get_targetDisplay_m2463712576 (Camera_t989002943 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_targetDisplay_m2463712576_ftn) (Camera_t989002943 *);
	static Camera_get_targetDisplay_m2463712576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetDisplay_m2463712576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetDisplay()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Camera_ScreenToWorldPoint_m410285743 (Camera_t989002943 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476_ftn) (Camera_t989002943 *, Vector3_t329709361 *, Vector3_t329709361 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m1857820476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Camera_ScreenToViewportPoint_m2977521633 (Camera_t989002943 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Vector3_t329709361 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698_ftn) (Camera_t989002943 *, Vector3_t329709361 *, Vector3_t329709361 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m969700698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1448970001  Camera_ScreenPointToRay_m1775774407 (Camera_t989002943 * __this, Vector3_t329709361  ___position0, const RuntimeMethod* method)
{
	Ray_t1448970001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t1448970001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t1448970001  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t1448970001  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Vector3_t329709361 * ___position1, Ray_t1448970001 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544_ftn) (Camera_t989002943 *, Vector3_t329709361 *, Ray_t1448970001 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m3009749544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t989002943 * Camera_get_main_m3663886479 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Camera_t989002943 * (*Camera_get_main_m3663886479_ftn) ();
	static Camera_get_main_m3663886479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m3663886479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	Camera_t989002943 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1239575985 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m1239575985_ftn) ();
	static Camera_get_allCamerasCount_m1239575985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m1239575985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m2864360343 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t3897890150* ___cameras0, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_GetAllCameras_m2864360343_ftn) (CameraU5BU5D_t3897890150*);
	static Camera_GetAllCameras_m2864360343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m2864360343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	int32_t retVal = _il2cpp_icall_func(___cameras0);
	return retVal;
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreCull_m1310166745 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m1310166745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2620733104 * L_0 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2620733104 * L_1 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPreCull_2();
		Camera_t989002943 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1414690239(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreRender_m965332065 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m965332065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2620733104 * L_0 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2620733104 * L_1 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPreRender_3();
		Camera_t989002943 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1414690239(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPostRender_m1302122984 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m1302122984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t2620733104 * L_0 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t2620733104 * L_1 = ((Camera_t989002943_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t989002943_il2cpp_TypeInfo_var))->get_onPostRender_4();
		Camera_t989002943 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m1414690239(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m3104925877 (Camera_t989002943 * __this, int32_t ___evt0, CommandBuffer_t2673047760 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_AddCommandBuffer_m3104925877_ftn) (Camera_t989002943 *, int32_t, CommandBuffer_t2673047760 *);
	static Camera_AddCommandBuffer_m3104925877_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_AddCommandBuffer_m3104925877_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m1713736922 (Camera_t989002943 * __this, int32_t ___evt0, CommandBuffer_t2673047760 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_RemoveCommandBuffer_m1713736922_ftn) (Camera_t989002943 *, int32_t, CommandBuffer_t2673047760 *);
	static Camera_RemoveCommandBuffer_m1713736922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RemoveCommandBuffer_m1713736922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_RaycastTry_m413943321 (Camera_t989002943 * __this, Ray_t1448970001  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t1318052361 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1318052361 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m1214413461(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t1318052361 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_INTERNAL_CALL_RaycastTry_m1214413461 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Ray_t1448970001 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t1318052361 * (*Camera_INTERNAL_CALL_RaycastTry_m1214413461_ftn) (Camera_t989002943 *, Ray_t1448970001 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m1214413461_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m1214413461_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t1318052361 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_RaycastTry2D_m2680111107 (Camera_t989002943 * __this, Ray_t1448970001  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t1318052361 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1318052361 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m1667450469(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t1318052361 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1318052361 * Camera_INTERNAL_CALL_RaycastTry2D_m1667450469 (RuntimeObject * __this /* static, unused */, Camera_t989002943 * ___self0, Ray_t1448970001 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t1318052361 * (*Camera_INTERNAL_CALL_RaycastTry2D_m1667450469_ftn) (Camera_t989002943 *, Ray_t1448970001 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m1667450469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m1667450469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t1318052361 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m1144352177 (CameraCallback_t2620733104 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m1414690239 (CameraCallback_t2620733104 * __this, Camera_t989002943 * ___cam0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m1414690239((CameraCallback_t2620733104 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Camera_t989002943 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t989002943 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CameraCallback_BeginInvoke_m1027549246 (CameraCallback_t2620733104 * __this, Camera_t989002943 * ___cam0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m1153401090 (CameraCallback_t2620733104 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.ClassLibraryInitializer::Init()
extern "C"  void ClassLibraryInitializer_Init_m2596500571 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		UnityLogWriter_Init_m328945537(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m551863488 (Color_t460381780 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m551863488_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	Color_t460381780 * _thisAdjusted = reinterpret_cast<Color_t460381780 *>(__this + 1);
	Color__ctor_m551863488(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3833972762 (Color_t460381780 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m3833972762_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	Color_t460381780 * _thisAdjusted = reinterpret_cast<Color_t460381780 *>(__this + 1);
	Color__ctor_m3833972762(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m2177171216 (Color_t460381780 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m2177171216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral3336280268, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color_ToString_m2177171216_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t460381780 * _thisAdjusted = reinterpret_cast<Color_t460381780 *>(__this + 1);
	return Color_ToString_m2177171216(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m44703584 (Color_t460381780 * __this, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector4_t380635127  L_0 = Color_op_Implicit_m174303393(NULL /*static, unused*/, (*(Color_t460381780 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3672563191((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0020;
	}

IL_0020:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t Color_GetHashCode_m44703584_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t460381780 * _thisAdjusted = reinterpret_cast<Color_t460381780 *>(__this + 1);
	return Color_GetHashCode_m44703584(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m1644359303 (Color_t460381780 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m1644359303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Color_t460381780  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Color_t460381780_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Color_t460381780 *)((Color_t460381780 *)UnBox(L_1, Color_t460381780_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_1)->get_r_0();
		bool L_4 = Single_Equals_m54068117(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_1)->get_g_1();
		bool L_7 = Single_Equals_m54068117(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_1)->get_b_2();
		bool L_10 = Single_Equals_m54068117(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_1)->get_a_3();
		bool L_13 = Single_Equals_m54068117(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Color_Equals_m1644359303_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Color_t460381780 * _thisAdjusted = reinterpret_cast<Color_t460381780 *>(__this + 1);
	return Color_Equals_m1644359303(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t460381780  Color_op_Multiply_m220941899 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___a0, float ___b1, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t460381780  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m551863488((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Color_t460381780  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m3738865736 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___lhs0, Color_t460381780  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_op_Equality_m3738865736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Color_t460381780  L_0 = ___lhs0;
		Vector4_t380635127  L_1 = Color_op_Implicit_m174303393(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t460381780  L_2 = ___rhs1;
		Vector4_t380635127  L_3 = Color_op_Implicit_m174303393(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t380635127_il2cpp_TypeInfo_var);
		bool L_4 = Vector4_op_Equality_m2775808155(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t460381780  Color_Lerp_m544019243 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___a0, Color_t460381780  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m544019243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t460381780  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m551863488((&L_18), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_0078;
	}

IL_0078:
	{
		Color_t460381780  L_19 = V_0;
		return L_19;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t460381780  Color_get_white_m158943222 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t460381780  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m551863488((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t460381780  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t460381780  Color_get_black_m1248688383 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t460381780  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m551863488((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t460381780  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t460381780  Color_get_clear_m3915984280 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t460381780  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m551863488((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t460381780  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t380635127  Color_op_Implicit_m174303393 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___c0, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t380635127  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m3770092030((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		Vector4_t380635127  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m662029646 (Color32_t2788147849 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m662029646_AdjustorThunk (RuntimeObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	Color32_t2788147849 * _thisAdjusted = reinterpret_cast<Color32_t2788147849 *>(__this + 1);
	Color32__ctor_m662029646(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t2788147849  Color32_op_Implicit_m308041032 (RuntimeObject * __this /* static, unused */, Color_t460381780  ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m308041032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2788147849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t2788147849  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m662029646((&L_8), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0058;
	}

IL_0058:
	{
		Color32_t2788147849  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t460381780  Color32_op_Implicit_m130658301 (RuntimeObject * __this /* static, unused */, Color32_t2788147849  ___c0, const RuntimeMethod* method)
{
	Color_t460381780  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t460381780  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m551863488((&L_4), ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0044;
	}

IL_0044:
	{
		Color_t460381780  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m2492342775 (Color32_t2788147849 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m2492342775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Byte_t2815932036_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Byte_t2815932036_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Byte_t2815932036_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Byte_t2815932036_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral141853172, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color32_ToString_m2492342775_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color32_t2788147849 * _thisAdjusted = reinterpret_cast<Color32_t2788147849 *>(__this + 1);
	return Color32_ToString_m2492342775(_thisAdjusted, method);
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3701326763 (Component_t789413749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m3701326763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t532597831 * Component_get_transform_m2645782843 (Component_t789413749 * __this, const RuntimeMethod* method)
{
	typedef Transform_t532597831 * (*Component_get_transform_m2645782843_ftn) (Component_t789413749 *);
	static Component_get_transform_m2645782843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m2645782843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	Transform_t532597831 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1318052361 * Component_get_gameObject_m2399756717 (Component_t789413749 * __this, const RuntimeMethod* method)
{
	typedef GameObject_t1318052361 * (*Component_get_gameObject_m2399756717_ftn) (Component_t789413749 *);
	static Component_get_gameObject_m2399756717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m2399756717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	GameObject_t1318052361 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t789413749 * Component_GetComponent_m41488779 (Component_t789413749 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	Component_t789413749 * V_0 = NULL;
	{
		GameObject_t1318052361 * L_0 = Component_get_gameObject_m2399756717(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t789413749 * L_2 = GameObject_GetComponent_m802188666(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t789413749 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m51010340 (Component_t789413749 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentFastPath_m51010340_ftn) (Component_t789413749 *, Type_t *, intptr_t);
	static Component_GetComponentFastPath_m51010340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m51010340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t789413749 * Component_GetComponentInChildren_m1196124058 (Component_t789413749 * __this, Type_t * ___t0, bool ___includeInactive1, const RuntimeMethod* method)
{
	Component_t789413749 * V_0 = NULL;
	{
		GameObject_t1318052361 * L_0 = Component_get_gameObject_m2399756717(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t789413749 * L_3 = GameObject_GetComponentInChildren_m36051739(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Component_t789413749 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t789413749 * Component_GetComponentInParent_m3686707876 (Component_t789413749 * __this, Type_t * ___t0, const RuntimeMethod* method)
{
	Component_t789413749 * V_0 = NULL;
	{
		GameObject_t1318052361 * L_0 = Component_get_gameObject_m2399756717(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t789413749 * L_2 = GameObject_GetComponentInParent_m3498674876(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t789413749 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m1131678872 (Component_t789413749 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentsForListInternal_m1131678872_ftn) (Component_t789413749 *, Type_t *, RuntimeObject *);
	static Component_GetComponentsForListInternal_m1131678872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m1131678872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m3953310928 (Component_t789413749 * __this, Type_t * ___type0, List_1_t599864163 * ___results1, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t599864163 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m1131678872(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t4212313979_marshal_pinvoke(const Coroutine_t4212313979& unmarshaled, Coroutine_t4212313979_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t4212313979_marshal_pinvoke_back(const Coroutine_t4212313979_marshaled_pinvoke& marshaled, Coroutine_t4212313979& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t4212313979_marshal_pinvoke_cleanup(Coroutine_t4212313979_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t4212313979_marshal_com(const Coroutine_t4212313979& unmarshaled, Coroutine_t4212313979_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t4212313979_marshal_com_back(const Coroutine_t4212313979_marshaled_com& marshaled, Coroutine_t4212313979& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t4212313979_marshal_com_cleanup(Coroutine_t4212313979_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m2666577742 (Coroutine_t4212313979 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m3635218774(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m4209904503 (Coroutine_t4212313979 * __this, const RuntimeMethod* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m4209904503_ftn) (Coroutine_t4212313979 *);
	static Coroutine_ReleaseCoroutine_m4209904503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m4209904503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m4126187435 (Coroutine_t4212313979 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m4209904503(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
extern "C"  void CreateAssetMenuAttribute__ctor_m2374655793 (CreateAssetMenuAttribute_t1456955310 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
extern "C"  void CreateAssetMenuAttribute_set_menuName_m3040641700 (CreateAssetMenuAttribute_t1456955310 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
extern "C"  void CreateAssetMenuAttribute_set_fileName_m1666535574 (CreateAssetMenuAttribute_t1456955310 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
extern "C"  void CreateAssetMenuAttribute_set_order_m4131765685 (CreateAssetMenuAttribute_t1456955310 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
extern "C"  CSSSize_t3542252173  DelegatePInvokeWrapper_CSSMeasureFunc_t2617999110 (CSSMeasureFunc_t2617999110 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	typedef CSSSize_t3542252173  (STDCALL *PInvokeFunc)(intptr_t, float, int32_t, float, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	CSSSize_t3542252173  returnValue = il2cppPInvokeFunc(___node0, ___width1, ___widthMode2, ___height3, ___heightMode4);

	return returnValue;
}
// System.Void UnityEngine.CSSLayout.CSSMeasureFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void CSSMeasureFunc__ctor_m3514902745 (CSSMeasureFunc_t2617999110 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t3542252173  CSSMeasureFunc_Invoke_m890182314 (CSSMeasureFunc_t2617999110 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CSSMeasureFunc_Invoke_m890182314((CSSMeasureFunc_t2617999110 *)__this->get_prev_9(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef CSSSize_t3542252173  (*FunctionPointerType) (RuntimeObject *, void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef CSSSize_t3542252173  (*FunctionPointerType) (void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CSSLayout.CSSMeasureFunc::BeginInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CSSMeasureFunc_BeginInvoke_m1450775633 (CSSMeasureFunc_t2617999110 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, AsyncCallback_t869574496 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSSMeasureFunc_BeginInvoke_m1450775633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___node0);
	__d_args[1] = Box(Single_t1863352746_il2cpp_TypeInfo_var, &___width1);
	__d_args[2] = Box(CSSMeasureMode_t483571173_il2cpp_TypeInfo_var, &___widthMode2);
	__d_args[3] = Box(Single_t1863352746_il2cpp_TypeInfo_var, &___height3);
	__d_args[4] = Box(CSSMeasureMode_t483571173_il2cpp_TypeInfo_var, &___heightMode4);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::EndInvoke(System.IAsyncResult)
extern "C"  CSSSize_t3542252173  CSSMeasureFunc_EndInvoke_m1368078637 (CSSMeasureFunc_t2617999110 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(CSSSize_t3542252173 *)UnBox ((RuntimeObject*)__result);
}
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2617999110 * Native_CSSNodeGetMeasureFunc_m1865278247 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeGetMeasureFunc_m1865278247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WeakReference_t1069295502 * V_0 = NULL;
	CSSMeasureFunc_t2617999110 * V_1 = NULL;
	{
		V_0 = (WeakReference_t1069295502 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4163284866_il2cpp_TypeInfo_var);
		Dictionary_2_t1695300448 * L_0 = ((Native_t4163284866_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4163284866_il2cpp_TypeInfo_var))->get_s_MeasureFunctions_0();
		intptr_t L_1 = ___node0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1067134071(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1067134071_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t1069295502 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.WeakReference::get_IsAlive() */, L_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t1069295502 * L_5 = V_0;
		NullCheck(L_5);
		RuntimeObject * L_6 = VirtFuncInvoker0< RuntimeObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, L_5);
		V_1 = ((CSSMeasureFunc_t2617999110 *)IsInstSealed((RuntimeObject*)L_6, CSSMeasureFunc_t2617999110_il2cpp_TypeInfo_var));
		goto IL_003a;
	}

IL_0032:
	{
		V_1 = (CSSMeasureFunc_t2617999110 *)NULL;
		goto IL_003a;
	}

IL_003a:
	{
		CSSMeasureFunc_t2617999110 * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.CSSLayout.Native::CSSNodeMeasureInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.IntPtr)
extern "C"  void Native_CSSNodeMeasureInvoke_m2427172143 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, intptr_t ___returnValueAddress5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeMeasureInvoke_m2427172143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CSSMeasureFunc_t2617999110 * V_0 = NULL;
	{
		intptr_t L_0 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t4163284866_il2cpp_TypeInfo_var);
		CSSMeasureFunc_t2617999110 * L_1 = Native_CSSNodeGetMeasureFunc_m1865278247(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CSSMeasureFunc_t2617999110 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		intptr_t L_3 = ___returnValueAddress5;
		void* L_4 = IntPtr_op_Explicit_m1639871891(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		CSSMeasureFunc_t2617999110 * L_5 = V_0;
		intptr_t L_6 = ___node0;
		float L_7 = ___width1;
		int32_t L_8 = ___widthMode2;
		float L_9 = ___height3;
		int32_t L_10 = ___heightMode4;
		NullCheck(L_5);
		CSSSize_t3542252173  L_11 = CSSMeasureFunc_Invoke_m890182314(L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		*(CSSSize_t3542252173 *)L_4 = L_11;
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.CSSLayout.Native::.cctor()
extern "C"  void Native__cctor_m3474275384 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native__cctor_m3474275384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1695300448 * L_0 = (Dictionary_2_t1695300448 *)il2cpp_codegen_object_new(Dictionary_2_t1695300448_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m499092701(L_0, /*hidden argument*/Dictionary_2__ctor_m499092701_RuntimeMethod_var);
		((Native_t4163284866_StaticFields*)il2cpp_codegen_static_fields_for(Native_t4163284866_il2cpp_TypeInfo_var))->set_s_MeasureFunctions_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t635124431_marshal_pinvoke(const CullingGroup_t635124431& unmarshaled, CullingGroup_t635124431_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t635124431_marshal_pinvoke_back(const CullingGroup_t635124431_marshaled_pinvoke& marshaled, CullingGroup_t635124431& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t635124431_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t3421641282>(marshaled.___m_OnStateChanged_1, StateChanged_t3421641282_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t635124431_marshal_pinvoke_cleanup(CullingGroup_t635124431_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t635124431_marshal_com(const CullingGroup_t635124431& unmarshaled, CullingGroup_t635124431_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t635124431_marshal_com_back(const CullingGroup_t635124431_marshaled_com& marshaled, CullingGroup_t635124431& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t635124431_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t3421641282>(marshaled.___m_OnStateChanged_1, StateChanged_t3421641282_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t635124431_marshal_com_cleanup(CullingGroup_t635124431_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern "C"  void CullingGroup_Finalize_m915509337 (CullingGroup_t635124431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m915509337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			intptr_t L_0 = __this->get_m_Ptr_0();
			bool L_1 = IntPtr_op_Inequality_m4173155962(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m634800622(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m64590221 (CullingGroup_t635124431 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_Dispose_m64590221_ftn) (CullingGroup_t635124431 *);
	static CullingGroup_Dispose_m64590221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m64590221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m4154025933 (RuntimeObject * __this /* static, unused */, CullingGroup_t635124431 * ___cullingGroup0, intptr_t ___eventsPtr1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_SendEvents_m4154025933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CullingGroupEvent_t2298849179 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m2850170309((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t2298849179 *)L_0;
		CullingGroup_t635124431 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t3421641282 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t635124431 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t3421641282 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t2298849179 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = il2cpp_codegen_sizeof(CullingGroupEvent_t2298849179_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StateChanged_Invoke_m962163523(L_4, (*(CullingGroupEvent_t2298849179 *)((CullingGroupEvent_t2298849179 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m634800622 (CullingGroup_t635124431 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m634800622_ftn) (CullingGroup_t635124431 *);
	static CullingGroup_FinalizerFailure_m634800622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m634800622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t3421641282 (StateChanged_t3421641282 * __this, CullingGroupEvent_t2298849179  ___sphere0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t2298849179 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m3775630999 (StateChanged_t3421641282 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m962163523 (StateChanged_t3421641282 * __this, CullingGroupEvent_t2298849179  ___sphere0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m962163523((StateChanged_t3421641282 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, CullingGroupEvent_t2298849179  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t2298849179  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* StateChanged_BeginInvoke_m381182532 (StateChanged_t3421641282 * __this, CullingGroupEvent_t2298849179  ___sphere0, AsyncCallback_t869574496 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m381182532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t2298849179_il2cpp_TypeInfo_var, &___sphere0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m970293609 (StateChanged_t3421641282 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m356172783 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Cursor_get_lockState_m356172783_ftn) ();
	static Cursor_get_lockState_m356172783_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m356172783_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m2763301555 (CustomYieldInstruction_t1639495614 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  RuntimeObject * CustomYieldInstruction_get_Current_m2347914062 (CustomYieldInstruction_t1639495614 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		V_0 = NULL;
		goto IL_0008;
	}

IL_0008:
	{
		RuntimeObject * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m4190681055 (CustomYieldInstruction_t1639495614 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m991052510 (CustomYieldInstruction_t1639495614 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m1064451884 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_unityLogger_m1064451884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Debug_t114115908_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t114115908_il2cpp_TypeInfo_var))->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1645010884 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m1645010884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t589268963_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m627890074 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m627890074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t589268963_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m1993805588 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t1970767703 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m1993805588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t1970767703 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t1970767703 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t589268963_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogErrorFormat_m2541379551 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___context0, String_t* ___format1, ObjectU5BU5D_t1568665923* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogErrorFormat_m2541379551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t1970767703 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t1568665923* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t1970767703 *, String_t*, ObjectU5BU5D_t1568665923* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1234897647 (RuntimeObject * __this /* static, unused */, Exception_t2123675094 * ___exception0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m1234897647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t2123675094 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t2123675094 *, Object_t1970767703 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_0, L_1, (Object_t1970767703 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Debug_LogException_m455398078 (RuntimeObject * __this /* static, unused */, Exception_t2123675094 * ___exception0, Object_t1970767703 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m455398078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t2123675094 * L_1 = ___exception0;
		Object_t1970767703 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t2123675094 *, Object_t1970767703 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3875502493 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m3875502493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t589268963_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m2821516339 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t1970767703 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m2821516339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t1970767703 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t1970767703 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t589268963_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m107336790 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___context0, String_t* ___format1, ObjectU5BU5D_t1568665923* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m107336790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m1064451884(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t1970767703 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t1568665923* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t1970767703 *, String_t*, ObjectU5BU5D_t1568665923* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern "C"  void Debug__cctor_m3310780854 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m3310780854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t2136690766 * L_0 = (DebugLogHandler_t2136690766 *)il2cpp_codegen_object_new(DebugLogHandler_t2136690766_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m2911506489(L_0, /*hidden argument*/NULL);
		Logger_t2034979157 * L_1 = (Logger_t2034979157 *)il2cpp_codegen_object_new(Logger_t2034979157_il2cpp_TypeInfo_var);
		Logger__ctor_m4124516248(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t114115908_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t114115908_il2cpp_TypeInfo_var))->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m2911506489 (DebugLogHandler_t2136690766 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m514723521 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t1970767703 * ___obj2, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m514723521_ftn) (int32_t, String_t*, Object_t1970767703 *);
	static DebugLogHandler_Internal_Log_m514723521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m514723521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m2210859879 (RuntimeObject * __this /* static, unused */, Exception_t2123675094 * ___exception0, Object_t1970767703 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m2210859879_ftn) (Exception_t2123675094 *, Object_t1970767703 *);
	static DebugLogHandler_Internal_LogException_m2210859879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m2210859879_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void DebugLogHandler_LogFormat_m2978450909 (DebugLogHandler_t2136690766 * __this, int32_t ___logType0, Object_t1970767703 * ___context1, String_t* ___format2, ObjectU5BU5D_t1568665923* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m2978450909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t1568665923* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1554370993(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t1970767703 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m514723521(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m2159782674 (DebugLogHandler_t2136690766 * __this, Exception_t2123675094 * ___exception0, Object_t1970767703 * ___context1, const RuntimeMethod* method)
{
	{
		Exception_t2123675094 * L_0 = ___exception0;
		Object_t1970767703 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m2210859879(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3190858536 (DefaultExecutionOrder_t2771548650 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m372865937 (DisallowMultipleComponent_t1081793821 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m85349829 (Display_t882507400 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		intptr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m757086868((&L_0), 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m911212561 (Display_t882507400 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		intptr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern "C"  int32_t Display_get_renderingWidth_m2172863445 (Display_t882507400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingWidth_m2172863445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m1851363389(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern "C"  int32_t Display_get_renderingHeight_m2111578961 (Display_t882507400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingHeight_m2111578961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m1851363389(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern "C"  int32_t Display_get_systemWidth_m2156533738 (Display_t882507400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemWidth_m2156533738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m3500243592(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern "C"  int32_t Display_get_systemHeight_m1848489818 (Display_t882507400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemHeight_m1848489818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m3500243592(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Display_RelativeMouseAt_m4179181397 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___inputMouseCoordinates0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RelativeMouseAt_m4179181397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t329709361  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = (&___inputMouseCoordinates0)->get_x_1();
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&___inputMouseCoordinates0)->get_y_2();
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m4048331529(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->set_z_3((((float)((float)L_4))));
		int32_t L_5 = V_1;
		(&V_0)->set_x_1((((float)((float)L_5))));
		int32_t L_6 = V_2;
		(&V_0)->set_y_2((((float)((float)L_6))));
		Vector3_t329709361  L_7 = V_0;
		V_5 = L_7;
		goto IL_0046;
	}

IL_0046:
	{
		Vector3_t329709361  L_8 = V_5;
		return L_8;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern "C"  void Display_RecreateDisplayList_m2778731890 (RuntimeObject * __this /* static, unused */, IntPtrU5BU5D_t2297631236* ___nativeDisplay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m2778731890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t2297631236* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->set_displays_1(((DisplayU5BU5D_t3818835545*)SZArrayNew(DisplayU5BU5D_t3818835545_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t3818835545* L_1 = ((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t2297631236* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		intptr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t882507400 * L_7 = (Display_t882507400 *)il2cpp_codegen_object_new(Display_t882507400_il2cpp_TypeInfo_var);
		Display__ctor_m911212561(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t882507400 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t2297631236* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t3818835545* L_11 = ((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t882507400 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern "C"  void Display_FireDisplaysUpdated_m1903554954 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m1903554954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t850082040 * L_0 = ((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t882507400_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t850082040 * L_1 = ((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m2818451955(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m3500243592 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetSystemExtImpl_m3500243592_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m3500243592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m3500243592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m1851363389 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetRenderingExtImpl_m1851363389_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m1851363389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m1851363389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m4048331529 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m4048331529_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m4048331529_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m4048331529_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	int32_t retVal = _il2cpp_icall_func(___x0, ___y1, ___rx2, ___ry3);
	return retVal;
}
// System.Void UnityEngine.Display::.cctor()
extern "C"  void Display__cctor_m3674733810 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m3674733810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t3818835545* L_0 = ((DisplayU5BU5D_t3818835545*)SZArrayNew(DisplayU5BU5D_t3818835545_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t882507400 * L_1 = (Display_t882507400 *)il2cpp_codegen_object_new(Display_t882507400_il2cpp_TypeInfo_var);
		Display__ctor_m85349829(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t882507400 *)L_1);
		((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->set_displays_1(L_0);
		DisplayU5BU5D_t3818835545* L_2 = ((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t882507400 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_4);
		((Display_t882507400_StaticFields*)il2cpp_codegen_static_fields_for(Display_t882507400_il2cpp_TypeInfo_var))->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t850082040 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t850082040 (DisplaysUpdatedDelegate_t850082040 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m2621983460 (DisplaysUpdatedDelegate_t850082040 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m2818451955 (DisplaysUpdatedDelegate_t850082040 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m2818451955((DisplaysUpdatedDelegate_t850082040 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* DisplaysUpdatedDelegate_BeginInvoke_m4291745309 (DisplaysUpdatedDelegate_t850082040 * __this, AsyncCallback_t869574496 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m2886756350 (DisplaysUpdatedDelegate_t850082040 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m2840871663 (DrivenRectTransformTracker_t4212122797 * __this, Object_t1970767703 * ___driver0, RectTransform_t15861704 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m2840871663_AdjustorThunk (RuntimeObject * __this, Object_t1970767703 * ___driver0, RectTransform_t15861704 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t4212122797 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t4212122797 *>(__this + 1);
	DrivenRectTransformTracker_Add_m2840871663(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m2252913897 (DrivenRectTransformTracker_t4212122797 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		DrivenRectTransformTracker_Clear_m1182552784(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m2252913897_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t4212122797 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t4212122797 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m2252913897(_thisAdjusted, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m1182552784 (DrivenRectTransformTracker_t4212122797 * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m1182552784_AdjustorThunk (RuntimeObject * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t4212122797 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t4212122797 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m1182552784(_thisAdjusted, ___revertValues0, method);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m27259610 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t1970767703 * ArgumentCache_get_unityObjectArgument_m2115528201 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	Object_t1970767703 * V_0 = NULL;
	{
		Object_t1970767703 * L_0 = __this->get_m_ObjectArgument_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t1970767703 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2990531235 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m1195387480 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatArgument_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m2954742800 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m611899758 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m2499549447 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m2499549447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1440728790(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00e4;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m170176446(L_2, _stringLiteral1862748563, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m2267115291(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m170176446(L_8, _stringLiteral1500987161, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m2267115291(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m170176446(L_14, _stringLiteral1642176685, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m2267115291(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1618205500(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
	}

IL_009a:
	{
		String_t* L_24 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_24);
		int32_t L_25 = String_IndexOf_m170176446(L_24, _stringLiteral1188916203, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) == ((int32_t)(-1))))
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_27 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_27);
		bool L_28 = String_EndsWith_m1375684648(L_27, _stringLiteral4225161693, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_29 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = String_Substring_m1618205500(L_29, 0, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m1227504536(NULL /*static, unused*/, L_31, _stringLiteral3777595284, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_32);
	}

IL_00e4:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m1938425222 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m2499549447(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m2246489200 (ArgumentCache_t3186847956 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m2499549447(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m3308847879 (BaseInvokableCall_t3782853021 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2684183300 (BaseInvokableCall_t3782853021 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m2684183300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t970256261 * L_1 = (ArgumentNullException_t970256261 *)il2cpp_codegen_object_new(ArgumentNullException_t970256261_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m996870465(L_1, _stringLiteral3934963589, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t970256261 * L_3 = (ArgumentNullException_t970256261 *)il2cpp_codegen_object_new(ArgumentNullException_t970256261_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m996870465(L_3, _stringLiteral1116345114, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m3200287249 (RuntimeObject * __this /* static, unused */, Delegate_t1563516729 * ___delegate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m3200287249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Object_t1970767703 * V_2 = NULL;
	{
		Delegate_t1563516729 * L_0 = ___delegate0;
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2270113079(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0015:
	{
		RuntimeObject * L_3 = V_0;
		V_2 = ((Object_t1970767703 *)IsInstClass((RuntimeObject*)L_3, Object_t1970767703_il2cpp_TypeInfo_var));
		Object_t1970767703 * L_4 = V_2;
		bool L_5 = Object_ReferenceEquals_m1116264796(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Object_t1970767703 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_6, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m3813870726 (InvokableCall_t1532986675 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m3813870726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m2684183300(__this, L_0, L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(UnityAction_t1298730314_0_0_0_var), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t1563516729 * L_5 = NetFxCoreExtensions_CreateDelegate_m3294389970(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		InvokableCall_add_Delegate_m3264530938(__this, ((UnityAction_t1298730314 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t1298730314_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m2759934649 (InvokableCall_t1532986675 * __this, UnityAction_t1298730314 * ___action0, const RuntimeMethod* method)
{
	{
		BaseInvokableCall__ctor_m3308847879(__this, /*hidden argument*/NULL);
		UnityAction_t1298730314 * L_0 = ___action0;
		InvokableCall_add_Delegate_m3264530938(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m3264530938 (InvokableCall_t1532986675 * __this, UnityAction_t1298730314 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_add_Delegate_m3264530938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t1298730314 * V_0 = NULL;
	UnityAction_t1298730314 * V_1 = NULL;
	{
		UnityAction_t1298730314 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t1298730314 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t1298730314 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t1298730314 * L_3 = V_1;
		UnityAction_t1298730314 * L_4 = ___value0;
		Delegate_t1563516729 * L_5 = Delegate_Combine_m645997588(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t1298730314 * L_6 = V_0;
		UnityAction_t1298730314 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t1298730314 *>(L_2, ((UnityAction_t1298730314 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t1298730314_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t1298730314 * L_8 = V_0;
		UnityAction_t1298730314 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t1298730314 *)L_8) == ((RuntimeObject*)(UnityAction_t1298730314 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_remove_Delegate_m2316392508 (InvokableCall_t1532986675 * __this, UnityAction_t1298730314 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_remove_Delegate_m2316392508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t1298730314 * V_0 = NULL;
	UnityAction_t1298730314 * V_1 = NULL;
	{
		UnityAction_t1298730314 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t1298730314 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t1298730314 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t1298730314 * L_3 = V_1;
		UnityAction_t1298730314 * L_4 = ___value0;
		Delegate_t1563516729 * L_5 = Delegate_Remove_m2405839985(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t1298730314 * L_6 = V_0;
		UnityAction_t1298730314 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t1298730314 *>(L_2, ((UnityAction_t1298730314 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t1298730314_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t1298730314 * L_8 = V_0;
		UnityAction_t1298730314 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t1298730314 *)L_8) == ((RuntimeObject*)(UnityAction_t1298730314 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m788008172 (InvokableCall_t1532986675 * __this, ObjectU5BU5D_t1568665923* ___args0, const RuntimeMethod* method)
{
	{
		UnityAction_t1298730314 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m3200287249(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t1298730314 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m3074312817(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke()
extern "C"  void InvokableCall_Invoke_m2051501297 (InvokableCall_t1532986675 * __this, const RuntimeMethod* method)
{
	{
		UnityAction_t1298730314 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m3200287249(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t1298730314 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m3074312817(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m487001134 (InvokableCall_t1532986675 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_t1298730314 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2270113079(L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_t1298730314 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2563659778(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m3375537592 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m3375537592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3593303435 * L_0 = (List_1_t3593303435 *)il2cpp_codegen_object_new(List_1_t3593303435_il2cpp_TypeInfo_var);
		List_1__ctor_m3136659381(L_0, /*hidden argument*/List_1__ctor_m3136659381_RuntimeMethod_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t3593303435 * L_1 = (List_1_t3593303435 *)il2cpp_codegen_object_new(List_1_t3593303435_il2cpp_TypeInfo_var);
		List_1__ctor_m3136659381(L_1, /*hidden argument*/List_1__ctor_m3136659381_RuntimeMethod_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t3593303435 * L_2 = (List_1_t3593303435 *)il2cpp_codegen_object_new(List_1_t3593303435_il2cpp_TypeInfo_var);
		List_1__ctor_m3136659381(L_2, /*hidden argument*/List_1__ctor_m3136659381_RuntimeMethod_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m1525842468 (InvokableCallList_t1231629294 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m1525842468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3593303435 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t3782853021 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m166741303(L_0, L_1, /*hidden argument*/List_1_Add_m166741303_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m1729791600 (InvokableCallList_t1231629294 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m1729791600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3593303435 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t3782853021 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m166741303(L_0, L_1, /*hidden argument*/List_1_Add_m166741303_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m2959324581 (InvokableCallList_t1231629294 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m2959324581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3593303435 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t3593303435 * L_0 = (List_1_t3593303435 *)il2cpp_codegen_object_new(List_1_t3593303435_il2cpp_TypeInfo_var);
		List_1__ctor_m3136659381(L_0, /*hidden argument*/List_1__ctor_m3136659381_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003e;
	}

IL_000e:
	{
		List_1_t3593303435 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t3782853021 * L_3 = List_1_get_Item_m532699242(L_1, L_2, /*hidden argument*/List_1_get_Item_m532699242_RuntimeMethod_var);
		RuntimeObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, RuntimeObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		List_1_t3593303435 * L_7 = V_0;
		List_1_t3593303435 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t3782853021 * L_10 = List_1_get_Item_m532699242(L_8, L_9, /*hidden argument*/List_1_get_Item_m532699242_RuntimeMethod_var);
		NullCheck(L_7);
		List_1_Add_m166741303(L_7, L_10, /*hidden argument*/List_1_Add_m166741303_RuntimeMethod_var);
	}

IL_0039:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		List_1_t3593303435 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m4034068867(L_13, /*hidden argument*/List_1_get_Count_m4034068867_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t3593303435 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t3593303435 * L_16 = V_0;
		intptr_t L_17 = (intptr_t)List_1_Contains_m2599341048_RuntimeMethod_var;
		Predicate_1_t2877392878 * L_18 = (Predicate_1_t2877392878 *)il2cpp_codegen_object_new(Predicate_1_t2877392878_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2108898755(L_18, L_16, L_17, /*hidden argument*/Predicate_1__ctor_m2108898755_RuntimeMethod_var);
		NullCheck(L_15);
		List_1_RemoveAll_m3245126275(L_15, L_18, /*hidden argument*/List_1_RemoveAll_m3245126275_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m3370527310 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m3370527310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3593303435 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m2582543520(L_0, /*hidden argument*/List_1_Clear_m2582543520_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t3593303435 * InvokableCallList_PrepareInvoke_m3324959179 (InvokableCallList_t1231629294 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_PrepareInvoke_m3324959179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3593303435 * V_0 = NULL;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t3593303435 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m2582543520(L_1, /*hidden argument*/List_1_Clear_m2582543520_RuntimeMethod_var);
		List_1_t3593303435 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t3593303435 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m1581173574(L_2, L_3, /*hidden argument*/List_1_AddRange_m1581173574_RuntimeMethod_var);
		List_1_t3593303435 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t3593303435 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m1581173574(L_4, L_5, /*hidden argument*/List_1_AddRange_m1581173574_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_0042:
	{
		List_1_t3593303435 * L_6 = __this->get_m_ExecutingCalls_2();
		V_0 = L_6;
		goto IL_004e;
	}

IL_004e:
	{
		List_1_t3593303435 * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern "C"  void PersistentCall__ctor_m1335226983 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m1335226983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_2(0);
		ArgumentCache_t3186847956 * L_0 = (ArgumentCache_t3186847956 *)il2cpp_codegen_object_new(ArgumentCache_t3186847956_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m27259610(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t1970767703 * PersistentCall_get_target_m3928746700 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	Object_t1970767703 * V_0 = NULL;
	{
		Object_t1970767703 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t1970767703 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m180845067 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m3685097626 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Mode_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t3186847956 * PersistentCall_get_arguments_m974501651 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	ArgumentCache_t3186847956 * V_0 = NULL;
	{
		ArgumentCache_t3186847956 * L_0 = __this->get_m_Arguments_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ArgumentCache_t3186847956 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m3600404012 (PersistentCall_t1257714207 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m3600404012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Object_t1970767703 * L_0 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_0, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m180845067(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1440728790(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t3782853021 * PersistentCall_GetRuntimeCall_m1034035855 (PersistentCall_t1257714207 * __this, UnityEventBase_t2204896975 * ___theEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m1034035855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3782853021 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		UnityEventBase_t2204896975 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0012:
	{
		V_0 = (BaseInvokableCall_t3782853021 *)NULL;
		goto IL_0114;
	}

IL_0019:
	{
		UnityEventBase_t2204896975 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m2595158952(L_2, __this, /*hidden argument*/NULL);
		V_1 = L_3;
		MethodInfo_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (BaseInvokableCall_t3782853021 *)NULL;
		goto IL_0114;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_2 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_005c;
			}
			case 1:
			{
				goto IL_00fb;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_00a4;
			}
			case 4:
			{
				goto IL_0087;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00de;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_005c:
	{
		UnityEventBase_t2204896975 * L_7 = ___theEvent0;
		Object_t1970767703 * L_8 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_1;
		NullCheck(L_7);
		BaseInvokableCall_t3782853021 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t3782853021 *, RuntimeObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		V_0 = L_10;
		goto IL_0114;
	}

IL_006f:
	{
		Object_t1970767703 * L_11 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_1;
		ArgumentCache_t3186847956 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t3782853021 * L_14 = PersistentCall_GetObjectCall_m2309820747(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0114;
	}

IL_0087:
	{
		Object_t1970767703 * L_15 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_1;
		ArgumentCache_t3186847956 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m1195387480(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1392551900 * L_19 = (CachedInvokableCall_1_t1392551900 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t1392551900_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m1503881081(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m1503881081_RuntimeMethod_var);
		V_0 = L_19;
		goto IL_0114;
	}

IL_00a4:
	{
		Object_t1970767703 * L_20 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_1;
		ArgumentCache_t3186847956 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m2990531235(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t28204005 * L_24 = (CachedInvokableCall_1_t28204005 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t28204005_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m1923180212(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m1923180212_RuntimeMethod_var);
		V_0 = L_24;
		goto IL_0114;
	}

IL_00c1:
	{
		Object_t1970767703 * L_25 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_1;
		ArgumentCache_t3186847956 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m2954742800(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3765088790 * L_29 = (CachedInvokableCall_1_t3765088790 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3765088790_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3388188837(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m3388188837_RuntimeMethod_var);
		V_0 = L_29;
		goto IL_0114;
	}

IL_00de:
	{
		Object_t1970767703 * L_30 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_1;
		ArgumentCache_t3186847956 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m611899758(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t98604400 * L_34 = (CachedInvokableCall_1_t98604400 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t98604400_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3785938189(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m3785938189_RuntimeMethod_var);
		V_0 = L_34;
		goto IL_0114;
	}

IL_00fb:
	{
		Object_t1970767703 * L_35 = PersistentCall_get_target_m3928746700(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_1;
		InvokableCall_t1532986675 * L_37 = (InvokableCall_t1532986675 *)il2cpp_codegen_object_new(InvokableCall_t1532986675_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m3813870726(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0114;
	}

IL_010d:
	{
		V_0 = (BaseInvokableCall_t3782853021 *)NULL;
		goto IL_0114;
	}

IL_0114:
	{
		BaseInvokableCall_t3782853021 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t3782853021 * PersistentCall_GetObjectCall_m2309820747 (RuntimeObject * __this /* static, unused */, Object_t1970767703 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t3186847956 * ___arguments2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m2309820747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t1575920019 * V_3 = NULL;
	Object_t1970767703 * V_4 = NULL;
	BaseInvokableCall_t3782853021 * V_5 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t3186847956 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1440728790(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentCache_t3186847956 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m1216471199, L_5, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0039:
	{
		V_0 = G_B3_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t269288053_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1460120061* L_11 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1460120061* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1460120061* L_15 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1460120061* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t1460120061* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t1575920019 * L_21 = Type_GetConstructor_m3152678330(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t3186847956 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t1970767703 * L_23 = ArgumentCache_get_unityObjectArgument_m2115528201(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t1970767703 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m4170278078(NULL /*static, unused*/, L_24, (Object_t1970767703 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t1970767703 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m3688535302(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00ab;
		}
	}
	{
		V_4 = (Object_t1970767703 *)NULL;
	}

IL_00ab:
	{
		ConstructorInfo_t1575920019 * L_30 = V_3;
		ObjectU5BU5D_t1568665923* L_31 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t1970767703 * L_32 = ___target0;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_32);
		ObjectU5BU5D_t1568665923* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_34);
		ObjectU5BU5D_t1568665923* L_35 = L_33;
		Object_t1970767703 * L_36 = V_4;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_36);
		NullCheck(L_30);
		RuntimeObject * L_37 = ConstructorInfo_Invoke_m3601249132(L_30, L_35, /*hidden argument*/NULL);
		V_5 = ((BaseInvokableCall_t3782853021 *)IsInstClass((RuntimeObject*)L_37, BaseInvokableCall_t3782853021_il2cpp_TypeInfo_var));
		goto IL_00d0;
	}

IL_00d0:
	{
		BaseInvokableCall_t3782853021 * L_38 = V_5;
		return L_38;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m3543885354 (PersistentCallGroup_t1952737969 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m3543885354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		List_1_t1068164621 * L_0 = (List_1_t1068164621 *)il2cpp_codegen_object_new(List_1_t1068164621_il2cpp_TypeInfo_var);
		List_1__ctor_m852503502(L_0, /*hidden argument*/List_1__ctor_m852503502_RuntimeMethod_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m3520909384 (PersistentCallGroup_t1952737969 * __this, InvokableCallList_t1231629294 * ___invokableList0, UnityEventBase_t2204896975 * ___unityEventBase1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m3520909384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentCall_t1257714207 * V_0 = NULL;
	Enumerator_t2667925666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t3782853021 * V_2 = NULL;
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1068164621 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t2667925666  L_1 = List_1_GetEnumerator_m2460221054(L_0, /*hidden argument*/List_1_GetEnumerator_m2460221054_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			PersistentCall_t1257714207 * L_2 = Enumerator_get_Current_m2730847451((&V_1), /*hidden argument*/Enumerator_get_Current_m2730847451_RuntimeMethod_var);
			V_0 = L_2;
			PersistentCall_t1257714207 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m3600404012(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			goto IL_0042;
		}

IL_002c:
		{
			PersistentCall_t1257714207 * L_5 = V_0;
			UnityEventBase_t2204896975 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t3782853021 * L_7 = PersistentCall_GetRuntimeCall_m1034035855(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t3782853021 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			InvokableCallList_t1231629294 * L_9 = ___invokableList0;
			BaseInvokableCall_t3782853021 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m1525842468(L_9, L_10, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m4259834011((&V_1), /*hidden argument*/Enumerator_MoveNext_m4259834011_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2021464298((&V_1), /*hidden argument*/Enumerator_Dispose_m2021464298_RuntimeMethod_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t1298730314 (UnityAction_t1298730314 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2209828363 (UnityAction_t1298730314 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m3074312817 (UnityAction_t1298730314 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m3074312817((UnityAction_t1298730314 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_BeginInvoke_m1364653977 (UnityAction_t1298730314 * __this, AsyncCallback_t869574496 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m4091436289 (UnityAction_t1298730314 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m1293152508 (UnityEvent_t3617968793 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_InvokeArray_4((ObjectU5BU5D_t1568665923*)NULL);
		UnityEventBase__ctor_m3145387590(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m2506419180 (UnityEvent_t3617968793 * __this, UnityAction_t1298730314 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_t1298730314 * L_0 = ___call0;
		BaseInvokableCall_t3782853021 * L_1 = UnityEvent_GetDelegate_m3771139711(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m3089987405(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m2170766220 (UnityEvent_t3617968793 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m2170766220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		MethodInfo_t * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t3782853021 * UnityEvent_GetDelegate_m1503760066 (UnityEvent_t3617968793 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m1503760066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3782853021 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t1532986675 * L_2 = (InvokableCall_t1532986675 *)il2cpp_codegen_object_new(InvokableCall_t1532986675_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m3813870726(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t3782853021 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t3782853021 * UnityEvent_GetDelegate_m3771139711 (RuntimeObject * __this /* static, unused */, UnityAction_t1298730314 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m3771139711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3782853021 * V_0 = NULL;
	{
		UnityAction_t1298730314 * L_0 = ___action0;
		InvokableCall_t1532986675 * L_1 = (InvokableCall_t1532986675 *)il2cpp_codegen_object_new(InvokableCall_t1532986675_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m2759934649(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t3782853021 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m279815371 (UnityEvent_t3617968793 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_Invoke_m279815371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3593303435 * V_0 = NULL;
	int32_t V_1 = 0;
	InvokableCall_t1532986675 * V_2 = NULL;
	BaseInvokableCall_t3782853021 * V_3 = NULL;
	{
		List_1_t3593303435 * L_0 = UnityEventBase_PrepareInvoke_m665515233(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0060;
	}

IL_000f:
	{
		List_1_t3593303435 * L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t3782853021 * L_3 = List_1_get_Item_m532699242(L_1, L_2, /*hidden argument*/List_1_get_Item_m532699242_RuntimeMethod_var);
		V_2 = ((InvokableCall_t1532986675 *)IsInstClass((RuntimeObject*)L_3, InvokableCall_t1532986675_il2cpp_TypeInfo_var));
		InvokableCall_t1532986675 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		InvokableCall_t1532986675 * L_5 = V_2;
		NullCheck(L_5);
		InvokableCall_Invoke_m2051501297(L_5, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_002e:
	{
		List_1_t3593303435 * L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		BaseInvokableCall_t3782853021 * L_8 = List_1_get_Item_m532699242(L_6, L_7, /*hidden argument*/List_1_get_Item_m532699242_RuntimeMethod_var);
		V_3 = L_8;
		ObjectU5BU5D_t1568665923* L_9 = __this->get_m_InvokeArray_4();
		if (L_9)
		{
			goto IL_004e;
		}
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)0)));
	}

IL_004e:
	{
		BaseInvokableCall_t3782853021 * L_10 = V_3;
		ObjectU5BU5D_t1568665923* L_11 = __this->get_m_InvokeArray_4();
		NullCheck(L_10);
		VirtActionInvoker1< ObjectU5BU5D_t1568665923* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_10, L_11);
	}

IL_005b:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_13 = V_1;
		List_1_t3593303435 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m4034068867(L_14, /*hidden argument*/List_1_get_Count_m4034068867_RuntimeMethod_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m3145387590 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m3145387590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		InvokableCallList_t1231629294 * L_0 = (InvokableCallList_t1231629294 *)il2cpp_codegen_object_new(InvokableCallList_t1231629294_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m3375537592(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t1952737969 * L_1 = (PersistentCallGroup_t1952737969 *)il2cpp_codegen_object_new(PersistentCallGroup_t1952737969_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m3543885354(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m3688535302(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3992232046 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3944798016 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m4212958525(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m3688535302(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m2595158952 (UnityEventBase_t2204896975 * __this, PersistentCall_t1257714207 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m2595158952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t1257714207 * L_1 = ___call0;
		NullCheck(L_1);
		ArgumentCache_t3186847956 * L_2 = PersistentCall_get_arguments_m974501651(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1440728790(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		PersistentCall_t1257714207 * L_5 = ___call0;
		NullCheck(L_5);
		ArgumentCache_t3186847956 * L_6 = PersistentCall_get_arguments_m974501651(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2134455673(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m1216471199, L_7, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B3_0;
	}

IL_0044:
	{
		PersistentCall_t1257714207 * L_11 = ___call0;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m180845067(L_11, /*hidden argument*/NULL);
		PersistentCall_t1257714207 * L_13 = ___call0;
		NullCheck(L_13);
		Object_t1970767703 * L_14 = PersistentCall_get_target_m3928746700(L_13, /*hidden argument*/NULL);
		PersistentCall_t1257714207 * L_15 = ___call0;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m3685097626(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m1231245736(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		MethodInfo_t * L_19 = V_1;
		return L_19;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1231245736 (UnityEventBase_t2204896975 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m1231245736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t1460120061* G_B10_2 = NULL;
	TypeU5BU5D_t1460120061* G_B10_3 = NULL;
	String_t* G_B10_4 = NULL;
	RuntimeObject * G_B10_5 = NULL;
	Type_t * G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t1460120061* G_B9_2 = NULL;
	TypeU5BU5D_t1460120061* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	RuntimeObject * G_B9_5 = NULL;
	{
		int32_t L_0 = ___mode2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_00c9;
			}
			case 3:
			{
				goto IL_0069;
			}
			case 4:
			{
				goto IL_0049;
			}
			case 5:
			{
				goto IL_00a9;
			}
			case 6:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_00f2;
	}

IL_0028:
	{
		String_t* L_1 = ___name0;
		RuntimeObject * L_2 = ___listener1;
		MethodInfo_t * L_3 = VirtFuncInvoker2< MethodInfo_t *, String_t*, RuntimeObject * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_1, L_2);
		V_0 = L_3;
		goto IL_00f9;
	}

IL_0036:
	{
		RuntimeObject * L_4 = ___listener1;
		String_t* L_5 = ___name0;
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_4, L_5, ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_00f9;
	}

IL_0049:
	{
		RuntimeObject * L_7 = ___listener1;
		String_t* L_8 = ___name0;
		TypeU5BU5D_t1460120061* L_9 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Single_t1863352746_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		MethodInfo_t * L_11 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_00f9;
	}

IL_0069:
	{
		RuntimeObject * L_12 = ___listener1;
		String_t* L_13 = ___name0;
		TypeU5BU5D_t1460120061* L_14 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Int32_t499004851_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_15);
		MethodInfo_t * L_16 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00f9;
	}

IL_0089:
	{
		RuntimeObject * L_17 = ___listener1;
		String_t* L_18 = ___name0;
		TypeU5BU5D_t1460120061* L_19 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Boolean_t569405246_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_20);
		MethodInfo_t * L_21 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00f9;
	}

IL_00a9:
	{
		RuntimeObject * L_22 = ___listener1;
		String_t* L_23 = ___name0;
		TypeU5BU5D_t1460120061* L_24 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_25);
		MethodInfo_t * L_26 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00f9;
	}

IL_00c9:
	{
		RuntimeObject * L_27 = ___listener1;
		String_t* L_28 = ___name0;
		TypeU5BU5D_t1460120061* L_29 = ((TypeU5BU5D_t1460120061*)SZArrayNew(TypeU5BU5D_t1460120061_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_30 = ___argumentType3;
		Type_t * L_31 = L_30;
		G_B9_0 = L_31;
		G_B9_1 = 0;
		G_B9_2 = L_29;
		G_B9_3 = L_29;
		G_B9_4 = L_28;
		G_B9_5 = L_27;
		if (L_31)
		{
			G_B10_0 = L_31;
			G_B10_1 = 0;
			G_B10_2 = L_29;
			G_B10_3 = L_29;
			G_B10_4 = L_28;
			G_B10_5 = L_27;
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(Object_t1970767703_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00e6:
	{
		NullCheck(G_B10_2);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (Type_t *)G_B10_0);
		MethodInfo_t * L_33 = UnityEventBase_GetValidMethodInfo_m3893544593(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00f9;
	}

IL_00f2:
	{
		V_0 = (MethodInfo_t *)NULL;
		goto IL_00f9;
	}

IL_00f9:
	{
		MethodInfo_t * L_34 = V_0;
		return L_34;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m4212958525 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1231629294 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m3370527310(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m834582307 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_CallsDirty_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		PersistentCallGroup_t1952737969 * L_1 = __this->get_m_PersistentCalls_1();
		InvokableCallList_t1231629294 * L_2 = __this->get_m_Calls_0();
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m3520909384(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)0);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3089987405 (UnityEventBase_t2204896975 * __this, BaseInvokableCall_t3782853021 * ___call0, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1231629294 * L_0 = __this->get_m_Calls_0();
		BaseInvokableCall_t3782853021 * L_1 = ___call0;
		NullCheck(L_0);
		InvokableCallList_AddListener_m1729791600(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m3350827987 (UnityEventBase_t2204896975 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	{
		InvokableCallList_t1231629294 * L_0 = __this->get_m_Calls_0();
		RuntimeObject * L_1 = ___targetObj0;
		MethodInfo_t * L_2 = ___method1;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m2959324581(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern "C"  List_1_t3593303435 * UnityEventBase_PrepareInvoke_m665515233 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	List_1_t3593303435 * V_0 = NULL;
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m834582307(__this, /*hidden argument*/NULL);
		InvokableCallList_t1231629294 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		List_1_t3593303435 * L_1 = InvokableCallList_PrepareInvoke_m3324959179(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		List_1_t3593303435 * L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern "C"  String_t* UnityEventBase_ToString_m702212230 (UnityEventBase_t2204896975 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m702212230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m4205420029(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m3688535302(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m414952842(NULL /*static, unused*/, L_0, _stringLiteral2998835488, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m3893544593 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1460120061* ___argumentTypes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_GetValidMethodInfo_m3893544593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	ParameterInfoU5BU5D_t4074997356* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t2372523953 * V_5 = NULL;
	ParameterInfoU5BU5D_t4074997356* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	MethodInfo_t * V_10 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m3688535302(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_009c;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName1;
		TypeU5BU5D_t1460120061* L_4 = ___argumentTypes2;
		NullCheck(L_2);
		MethodInfo_t * L_5 = Type_GetMethod_m3858208996(L_2, L_3, ((int32_t)52), (Binder_t3602727638 *)NULL, L_4, (ParameterModifierU5BU5D_t3541603165*)(ParameterModifierU5BU5D_t3541603165*)NULL, /*hidden argument*/NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t4074997356* L_8 = VirtFuncInvoker0< ParameterInfoU5BU5D_t4074997356* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = (bool)1;
		V_4 = 0;
		ParameterInfoU5BU5D_t4074997356* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_007a;
	}

IL_003a:
	{
		ParameterInfoU5BU5D_t4074997356* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t2372523953 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		TypeU5BU5D_t1460120061* L_14 = ___argumentTypes2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_8 = L_17;
		ParameterInfo_t2372523953 * L_18 = V_5;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		V_9 = L_19;
		Type_t * L_20 = V_8;
		NullCheck(L_20);
		bool L_21 = Type_get_IsPrimitive_m2375556458(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = V_9;
		NullCheck(L_22);
		bool L_23 = Type_get_IsPrimitive_m2375556458(L_22, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_21) == ((int32_t)L_23))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0085;
	}

IL_006d:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_7;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_7;
		ParameterInfoU5BU5D_t4074997356* L_28 = V_6;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0085:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_0093;
		}
	}
	{
		MethodInfo_t * L_30 = V_1;
		V_10 = L_30;
		goto IL_00ba;
	}

IL_0093:
	{
	}

IL_0094:
	{
		Type_t * L_31 = V_0;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_31);
		V_0 = L_32;
	}

IL_009c:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m1777045776(NULL /*static, unused*/, LoadTypeToken(RuntimeObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_33) == ((RuntimeObject*)(Type_t *)L_34)))
		{
			goto IL_00b2;
		}
	}
	{
		Type_t * L_35 = V_0;
		if (L_35)
		{
			goto IL_000d;
		}
	}

IL_00b2:
	{
		V_10 = (MethodInfo_t *)NULL;
		goto IL_00ba;
	}

IL_00ba:
	{
		MethodInfo_t * L_36 = V_10;
		return L_36;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C"  void ExecuteInEditMode__ctor_m3523051509 (ExecuteInEditMode_t814488931 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m2019186464 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_get_currentPipeline_m2019186464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_U3CcurrentPipelineU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m1978623761 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_set_currentPipeline_m1978623761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___value0;
		((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->set_U3CcurrentPipelineU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m185655714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_CleanupRenderPipeline_m185655714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject* L_1 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipelineAsset::DestroyCreatedInstances() */, IRenderPipelineAsset_t735150870_il2cpp_TypeInfo_var, L_1);
	}

IL_0015:
	{
		((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0((RuntimeObject*)NULL);
		RenderPipelineManager_set_currentPipeline_m1978623761(NULL /*static, unused*/, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Experimental.Rendering.IRenderPipelineAsset,UnityEngine.Camera[],System.IntPtr)
extern "C"  void RenderPipelineManager_DoRenderLoop_Internal_m1110218575 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, CameraU5BU5D_t3897890150* ___cameras1, intptr_t ___loopPtr2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_DoRenderLoop_Internal_m1110218575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScriptableRenderContext_t186008635  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = ___pipe0;
		RenderPipelineManager_PrepareRenderPipeline_m3314607428(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RuntimeObject* L_1 = RenderPipelineManager_get_currentPipeline_m2019186464(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_002a;
	}

IL_0016:
	{
		intptr_t L_2 = ___loopPtr2;
		ScriptableRenderContext__ctor_m1547560917((&V_0), L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = RenderPipelineManager_get_currentPipeline_m2019186464(NULL /*static, unused*/, /*hidden argument*/NULL);
		ScriptableRenderContext_t186008635  L_4 = V_0;
		CameraU5BU5D_t3897890150* L_5 = ___cameras1;
		NullCheck(L_3);
		InterfaceActionInvoker2< ScriptableRenderContext_t186008635 , CameraU5BU5D_t3897890150* >::Invoke(1 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipeline::Render(UnityEngine.Experimental.Rendering.ScriptableRenderContext,UnityEngine.Camera[]) */, IRenderPipeline_t1567407966_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m3314607428 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_PrepareRenderPipeline_m3314607428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		RuntimeObject* L_1 = ___pipe0;
		if ((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject*)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		RuntimeObject* L_2 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RenderPipelineManager_CleanupRenderPipeline_m185655714(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		RuntimeObject* L_3 = ___pipe0;
		((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0(L_3);
	}

IL_0025:
	{
		RuntimeObject* L_4 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_5 = RenderPipelineManager_get_currentPipeline_m2019186464(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		RuntimeObject* L_6 = RenderPipelineManager_get_currentPipeline_m2019186464(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.Experimental.Rendering.IRenderPipeline::get_disposed() */, IRenderPipeline_t1567407966_il2cpp_TypeInfo_var, L_6);
		if (!L_7)
		{
			goto IL_0057;
		}
	}

IL_0048:
	{
		RuntimeObject* L_8 = ((RenderPipelineManager_t2128300851_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t2128300851_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.IRenderPipelineAsset::CreatePipeline() */, IRenderPipelineAsset_t735150870_il2cpp_TypeInfo_var, L_8);
		RenderPipelineManager_set_currentPipeline_m1978623761(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m1547560917 (ScriptableRenderContext_t186008635 * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___ptr0;
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
extern "C"  void ScriptableRenderContext__ctor_m1547560917_AdjustorThunk (RuntimeObject * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	ScriptableRenderContext_t186008635 * _thisAdjusted = reinterpret_cast<ScriptableRenderContext_t186008635 *>(__this + 1);
	ScriptableRenderContext__ctor_m1547560917(_thisAdjusted, ___ptr0, method);
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke(const FailedToLoadScriptObject_t227884036& unmarshaled, FailedToLoadScriptObject_t227884036_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke_back(const FailedToLoadScriptObject_t227884036_marshaled_pinvoke& marshaled, FailedToLoadScriptObject_t227884036& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t227884036_marshal_pinvoke_cleanup(FailedToLoadScriptObject_t227884036_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t227884036_marshal_com(const FailedToLoadScriptObject_t227884036& unmarshaled, FailedToLoadScriptObject_t227884036_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t227884036_marshal_com_back(const FailedToLoadScriptObject_t227884036_marshaled_com& marshaled, FailedToLoadScriptObject_t227884036& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t227884036_marshal_com_cleanup(FailedToLoadScriptObject_t227884036_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m407434703 (GameObject_t1318052361 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m407434703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m2219017391(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m2351111389 (GameObject_t1318052361 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m2351111389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m2219017391(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m694740885 (GameObject_t1318052361 * __this, String_t* ___name0, TypeU5BU5D_t1460120061* ___components1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m694740885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t1460120061* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m2219017391(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t1460120061* L_1 = ___components1;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0028;
	}

IL_0018:
	{
		TypeU5BU5D_t1460120061* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Type_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		Type_t * L_6 = V_0;
		GameObject_AddComponent_m1275919783(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_8 = V_2;
		TypeU5BU5D_t1460120061* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t789413749 * GameObject_GetComponent_m802188666 (GameObject_t1318052361 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t789413749 * (*GameObject_GetComponent_m802188666_ftn) (GameObject_t1318052361 *, Type_t *);
	static GameObject_GetComponent_m802188666_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m802188666_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	Component_t789413749 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void GameObject_GetComponentFastPath_m3304262648 (GameObject_t1318052361 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*GameObject_GetComponentFastPath_m3304262648_ftn) (GameObject_t1318052361 *, Type_t *, intptr_t);
	static GameObject_GetComponentFastPath_m3304262648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m3304262648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t789413749 * GameObject_GetComponentInChildren_m36051739 (GameObject_t1318052361 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method)
{
	typedef Component_t789413749 * (*GameObject_GetComponentInChildren_m36051739_ftn) (GameObject_t1318052361 *, Type_t *, bool);
	static GameObject_GetComponentInChildren_m36051739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInChildren_m36051739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)");
	Component_t789413749 * retVal = _il2cpp_icall_func(__this, ___type0, ___includeInactive1);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t789413749 * GameObject_GetComponentInParent_m3498674876 (GameObject_t1318052361 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t789413749 * (*GameObject_GetComponentInParent_m3498674876_ftn) (GameObject_t1318052361 *, Type_t *);
	static GameObject_GetComponentInParent_m3498674876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInParent_m3498674876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInParent(System.Type)");
	Component_t789413749 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C"  RuntimeArray * GameObject_GetComponentsInternal_m1266835143 (GameObject_t1318052361 * __this, Type_t * ___type0, bool ___useSearchTypeAsArrayReturnType1, bool ___recursive2, bool ___includeInactive3, bool ___reverse4, RuntimeObject * ___resultList5, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*GameObject_GetComponentsInternal_m1266835143_ftn) (GameObject_t1318052361 *, Type_t *, bool, bool, bool, bool, RuntimeObject *);
	static GameObject_GetComponentsInternal_m1266835143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m1266835143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	RuntimeArray * retVal = _il2cpp_icall_func(__this, ___type0, ___useSearchTypeAsArrayReturnType1, ___recursive2, ___includeInactive3, ___reverse4, ___resultList5);
	return retVal;
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t532597831 * GameObject_get_transform_m1187966899 (GameObject_t1318052361 * __this, const RuntimeMethod* method)
{
	typedef Transform_t532597831 * (*GameObject_get_transform_m1187966899_ftn) (GameObject_t1318052361 *);
	static GameObject_get_transform_m1187966899_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m1187966899_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	Transform_t532597831 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m3002702318 (GameObject_t1318052361 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*GameObject_get_layer_m3002702318_ftn) (GameObject_t1318052361 *);
	static GameObject_get_layer_m3002702318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m3002702318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m4123344997 (GameObject_t1318052361 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_set_layer_m4123344997_ftn) (GameObject_t1318052361 *, int32_t);
	static GameObject_set_layer_m4123344997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m4123344997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2894453144 (GameObject_t1318052361 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_SetActive_m2894453144_ftn) (GameObject_t1318052361 *, bool);
	static GameObject_SetActive_m2894453144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m2894453144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C"  bool GameObject_get_activeSelf_m1206445787 (GameObject_t1318052361 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeSelf_m1206445787_ftn) (GameObject_t1318052361 *);
	static GameObject_get_activeSelf_m1206445787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeSelf_m1206445787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeSelf()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m1704601378 (GameObject_t1318052361 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m1704601378_ftn) (GameObject_t1318052361 *);
	static GameObject_get_activeInHierarchy_m1704601378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m1704601378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m2681843329 (GameObject_t1318052361 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*GameObject_SendMessage_m2681843329_ftn) (GameObject_t1318052361 *, String_t*, RuntimeObject *, int32_t);
	static GameObject_SendMessage_m2681843329_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m2681843329_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t789413749 * GameObject_Internal_AddComponentWithType_m4164311814 (GameObject_t1318052361 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	typedef Component_t789413749 * (*GameObject_Internal_AddComponentWithType_m4164311814_ftn) (GameObject_t1318052361 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m4164311814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m4164311814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	Component_t789413749 * retVal = _il2cpp_icall_func(__this, ___componentType0);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t789413749 * GameObject_AddComponent_m1275919783 (GameObject_t1318052361 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	Component_t789413749 * V_0 = NULL;
	{
		Type_t * L_0 = ___componentType0;
		Component_t789413749 * L_1 = GameObject_Internal_AddComponentWithType_m4164311814(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Component_t789413749 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m2219017391 (RuntimeObject * __this /* static, unused */, GameObject_t1318052361 * ___mono0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m2219017391_ftn) (GameObject_t1318052361 *, String_t*);
	static GameObject_Internal_CreateGameObject_m2219017391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m2219017391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono0, ___name1);
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1395232743_marshal_pinvoke(const Gradient_t1395232743& unmarshaled, Gradient_t1395232743_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t1395232743_marshal_pinvoke_back(const Gradient_t1395232743_marshaled_pinvoke& marshaled, Gradient_t1395232743& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1395232743_marshal_pinvoke_cleanup(Gradient_t1395232743_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1395232743_marshal_com(const Gradient_t1395232743& unmarshaled, Gradient_t1395232743_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t1395232743_marshal_com_back(const Gradient_t1395232743_marshaled_com& marshaled, Gradient_t1395232743& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1395232743_marshal_com_cleanup(Gradient_t1395232743_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m1310091561 (Gradient_t1395232743 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		Gradient_Init_m1552560054(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m1552560054 (Gradient_t1395232743 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Init_m1552560054_ftn) (Gradient_t1395232743 *);
	static Gradient_Init_m1552560054_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m1552560054_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m117154722 (Gradient_t1395232743 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Cleanup_m117154722_ftn) (Gradient_t1395232743 *);
	static Gradient_Cleanup_m117154722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m117154722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C"  void Gradient_Finalize_m654027066 (Gradient_t1395232743 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m117154722(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.GUIElement::.ctor()
extern "C"  void GUIElement__ctor_m1662183352 (GUIElement_t1520723180 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2015992041(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t1520723180 * GUILayer_HitTest_m1709809635 (GUILayer_t2694382279 * __this, Vector3_t329709361  ___screenPosition0, const RuntimeMethod* method)
{
	GUIElement_t1520723180 * V_0 = NULL;
	{
		GUIElement_t1520723180 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m3932656170(NULL /*static, unused*/, __this, (&___screenPosition0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		GUIElement_t1520723180 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t1520723180 * GUILayer_INTERNAL_CALL_HitTest_m3932656170 (RuntimeObject * __this /* static, unused */, GUILayer_t2694382279 * ___self0, Vector3_t329709361 * ___screenPosition1, const RuntimeMethod* method)
{
	typedef GUIElement_t1520723180 * (*GUILayer_INTERNAL_CALL_HitTest_m3932656170_ftn) (GUILayer_t2694382279 *, Vector3_t329709361 *);
	static GUILayer_INTERNAL_CALL_HitTest_m3932656170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m3932656170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	GUIElement_t1520723180 * retVal = _il2cpp_icall_func(___self0, ___screenPosition1);
	return retVal;
}
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
extern "C"  void HeaderAttribute__ctor_m3176828244 (HeaderAttribute_t30125744 * __this, String_t* ___header0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3365316696(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___header0;
		__this->set_header_0(L_0);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C"  void IL2CPPStructAlignmentAttribute__ctor_m2808873253 (IL2CPPStructAlignmentAttribute_t2686620401 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		__this->set_Align_0(1);
		return;
	}
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m3962599710 (RuntimeObject * __this /* static, unused */, String_t* ___axisName0, const RuntimeMethod* method)
{
	typedef float (*Input_GetAxisRaw_m3962599710_ftn) (String_t*);
	static Input_GetAxisRaw_m3962599710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m3962599710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	float retVal = _il2cpp_icall_func(___axisName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m1668438614 (RuntimeObject * __this /* static, unused */, String_t* ___buttonName0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetButtonDown_m1668438614_ftn) (String_t*);
	static Input_GetButtonDown_m1668438614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m1668438614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	bool retVal = _il2cpp_icall_func(___buttonName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m2038728439 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButton_m2038728439_ftn) (int32_t);
	static Input_GetMouseButton_m2038728439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m2038728439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2439334524 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonDown_m2439334524_ftn) (int32_t);
	static Input_GetMouseButtonDown_m2439334524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m2439334524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m930001757 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonUp_m930001757_ftn) (int32_t);
	static Input_GetMouseButtonUp_m930001757_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m930001757_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t329709361  Input_get_mousePosition_m354171928 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mousePosition_m354171928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m2041535329(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t329709361  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t329709361  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m2041535329 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m2041535329_ftn) (Vector3_t329709361 *);
	static Input_INTERNAL_get_mousePosition_m2041535329_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m2041535329_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern "C"  Vector2_t3057062568  Input_get_mouseScrollDelta_m1557855344 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mouseScrollDelta_m1557855344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m531055500(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m531055500 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m531055500_ftn) (Vector2_t3057062568 *);
	static Input_INTERNAL_get_mouseScrollDelta_m531055500_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m531055500_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C"  bool Input_get_mousePresent_m151235816 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_mousePresent_m151235816_ftn) ();
	static Input_get_mousePresent_m151235816_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m151235816_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1822905180  Input_GetTouch_m2440883173 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetTouch_m2440883173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1822905180  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t1822905180  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Input_INTERNAL_CALL_GetTouch_m850486359(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Touch_t1822905180  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Touch_t1822905180  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m850486359 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t1822905180 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_CALL_GetTouch_m850486359_ftn) (int32_t, Touch_t1822905180 *);
	static Input_INTERNAL_CALL_GetTouch_m850486359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_CALL_GetTouch_m850486359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m4024874449 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_touchCount_m4024874449_ftn) ();
	static Input_get_touchCount_m4024874449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m4024874449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C"  bool Input_get_touchSupported_m2897303851 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_touchSupported_m2897303851_ftn) ();
	static Input_get_touchSupported_m2897303851_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchSupported_m2897303851_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchSupported()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
extern "C"  int32_t Input_get_imeCompositionMode_m491668595 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_imeCompositionMode_m491668595_ftn) ();
	static Input_get_imeCompositionMode_m491668595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_imeCompositionMode_m491668595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_imeCompositionMode()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C"  void Input_set_imeCompositionMode_m2058933306 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_set_imeCompositionMode_m2058933306_ftn) (int32_t);
	static Input_set_imeCompositionMode_m2058933306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m2058933306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C"  String_t* Input_get_compositionString_m4276586557 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*Input_get_compositionString_m4276586557_ftn) ();
	static Input_get_compositionString_m4276586557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m4276586557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Vector2 UnityEngine.Input::get_compositionCursorPos()
extern "C"  Vector2_t3057062568  Input_get_compositionCursorPos_m3427021554 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_compositionCursorPos_m3427021554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t3057062568  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3057062568  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_compositionCursorPos_m3760724342(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t3057062568  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t3057062568  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern "C"  void Input_set_compositionCursorPos_m2684519623 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_set_compositionCursorPos_m2684519623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t2963215744_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m2255230934(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m3760724342 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_compositionCursorPos_m3760724342_ftn) (Vector2_t3057062568 *);
	static Input_INTERNAL_get_compositionCursorPos_m3760724342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_compositionCursorPos_m3760724342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m2255230934 (RuntimeObject * __this /* static, unused */, Vector2_t3057062568 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m2255230934_ftn) (Vector2_t3057062568 *);
	static Input_INTERNAL_set_compositionCursorPos_m2255230934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m2255230934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::.cctor()
extern "C"  void Input__cctor_m3780596079 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m3780596079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t2963215744_StaticFields*)il2cpp_codegen_static_fields_for(Input_t2963215744_il2cpp_TypeInfo_var))->set_m_MainGyro_0((Gyroscope_t416198001 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m3284556383 (DefaultValueAttribute_t3593402498 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m1703762465 (DefaultValueAttribute_t3593402498 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern "C"  bool DefaultValueAttribute_Equals_m62841503 (DefaultValueAttribute_t3593402498 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m62841503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t3593402498 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t3593402498 *)IsInstClass((RuntimeObject*)L_0, DefaultValueAttribute_t3593402498_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t3593402498 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0046;
	}

IL_0015:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		DefaultValueAttribute_t3593402498 * L_3 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_4 = DefaultValueAttribute_get_Value_m1703762465(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((RuntimeObject*)(RuntimeObject *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		goto IL_0046;
	}

IL_002f:
	{
		RuntimeObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t3593402498 * L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_7 = DefaultValueAttribute_get_Value_m1703762465(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		V_1 = L_8;
		goto IL_0046;
	}

IL_0046:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m1845346810 (DefaultValueAttribute_t3593402498 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m3297037235(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0029;
	}

IL_0018:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C"  void ExcludeFromDocsAttribute__ctor_m3764274013 (ExcludeFromDocsAttribute_t3465613458 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m4233958761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m2536903317 (LocalNotification_t2999868044 * __this, const RuntimeMethod* method)
{
	typedef void (*LocalNotification_Destroy_m2536903317_ftn) (LocalNotification_t2999868044 *);
	static LocalNotification_Destroy_m2536903317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m2536903317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m1555407255 (LocalNotification_t2999868044 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m2536903317(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern "C"  void LocalNotification__cctor_m4222515013 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m4222515013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t972933412  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m2981095106((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m1939839433((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t2999868044_StaticFields*)il2cpp_codegen_static_fields_for(LocalNotification_t2999868044_il2cpp_TypeInfo_var))->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m3109502285 (RemoteNotification_t163380436 * __this, const RuntimeMethod* method)
{
	typedef void (*RemoteNotification_Destroy_m3109502285_ftn) (RemoteNotification_t163380436 *);
	static RemoteNotification_Destroy_m3109502285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m3109502285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m2783840182 (RemoteNotification_t163380436 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m3109502285(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m844094926 (RuntimeObject * __this /* static, unused */, LayerMask_t1788260229  ___mask0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (&___mask0)->get_m_Mask_0();
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t1788260229  LayerMask_op_Implicit_m1801988922 (RuntimeObject * __this /* static, unused */, int32_t ___intVal0, const RuntimeMethod* method)
{
	LayerMask_t1788260229  V_0;
	memset(&V_0, 0, sizeof(V_0));
	LayerMask_t1788260229  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___intVal0;
		(&V_0)->set_m_Mask_0(L_0);
		LayerMask_t1788260229  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		LayerMask_t1788260229  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Light::set_colorTemperature(System.Single)
extern "C"  void Light_set_colorTemperature_m730358656 (Light_t3883945566 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Light_set_colorTemperature_m730358656_ftn) (Light_t3883945566 *, float);
	static Light_set_colorTemperature_m730358656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_colorTemperature_m730358656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_colorTemperature(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m2158977792 (Light_t3883945566 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Light_set_intensity_m2158977792_ftn) (Light_t3883945566 *, float);
	static Light_set_intensity_m2158977792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_intensity_m2158977792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_intensity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.LightProbes UnityEngine.LightmapSettings::get_lightProbes()
extern "C"  LightProbes_t380083648 * LightmapSettings_get_lightProbes_m2576896048 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef LightProbes_t380083648 * (*LightmapSettings_get_lightProbes_m2576896048_ftn) ();
	static LightmapSettings_get_lightProbes_m2576896048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LightmapSettings_get_lightProbes_m2576896048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LightmapSettings::get_lightProbes()");
	LightProbes_t380083648 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.LightProbes::.ctor()
extern "C"  void LightProbes__ctor_m1000816120 (LightProbes_t380083648 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LightProbes__ctor_m1000816120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2[] UnityEngine.LightProbes::get_bakedProbes()
extern "C"  SphericalHarmonicsL2U5BU5D_t2505359721* LightProbes_get_bakedProbes_m790038193 (LightProbes_t380083648 * __this, const RuntimeMethod* method)
{
	typedef SphericalHarmonicsL2U5BU5D_t2505359721* (*LightProbes_get_bakedProbes_m790038193_ftn) (LightProbes_t380083648 *);
	static LightProbes_get_bakedProbes_m790038193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LightProbes_get_bakedProbes_m790038193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LightProbes::get_bakedProbes()");
	SphericalHarmonicsL2U5BU5D_t2505359721* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.LightProbes::get_count()
extern "C"  int32_t LightProbes_get_count_m1291403856 (LightProbes_t380083648 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*LightProbes_get_count_m1291403856_ftn) (LightProbes_t380083648 *);
	static LightProbes_get_count_m1291403856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LightProbes_get_count_m1291403856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LightProbes::get_count()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m4124516248 (Logger_t2034979157 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___logHandler0;
		Logger_set_logHandler_m1411151799(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m3126018738(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m1146460364(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1737771075 (Logger_t2034979157 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m1411151799 (Logger_t2034979157 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2598788189 (Logger_t2034979157 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m3126018738 (Logger_t2034979157 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m866625208 (Logger_t2034979157 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m1146460364 (Logger_t2034979157 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m2476994406 (Logger_t2034979157 * __this, int32_t ___logType0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = Logger_get_logEnabled_m2598788189(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_1 = ___logType0;
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_001b;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0041;
	}

IL_001b:
	{
		int32_t L_2 = Logger_get_filterLogType_m866625208(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___logType0;
		int32_t L_4 = Logger_get_filterLogType_m866625208(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_3) > ((int32_t)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0041;
	}

IL_0039:
	{
	}

IL_003a:
	{
		V_0 = (bool)0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m1753770121 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m1753770121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		RuntimeObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral652294219;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001d;
	}

IL_001d:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern "C"  void Logger_Log_m3920909206 (Logger_t2034979157 * __this, int32_t ___logType0, RuntimeObject * ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m3920909206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m2476994406(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1737771075(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t1568665923* L_4 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m1753770121(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1970767703 *, String_t*, ObjectU5BU5D_t1568665923* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_2, L_3, (Object_t1970767703 *)NULL, _stringLiteral843812391, L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern "C"  void Logger_Log_m591675774 (Logger_t2034979157 * __this, int32_t ___logType0, RuntimeObject * ___message1, Object_t1970767703 * ___context2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m591675774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m2476994406(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1737771075(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t1970767703 * L_4 = ___context2;
		ObjectU5BU5D_t1568665923* L_5 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_6 = ___message1;
		String_t* L_7 = Logger_GetString_m1753770121(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_7);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1970767703 *, String_t*, ObjectU5BU5D_t1568665923* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_2, L_3, L_4, _stringLiteral843812391, L_5);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void Logger_LogFormat_m3050159729 (Logger_t2034979157 * __this, int32_t ___logType0, Object_t1970767703 * ___context1, String_t* ___format2, ObjectU5BU5D_t1568665923* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m3050159729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m2476994406(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1737771075(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t1970767703 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t1568665923* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1970767703 *, String_t*, ObjectU5BU5D_t1568665923* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Logger_LogException_m106933759 (Logger_t2034979157 * __this, Exception_t2123675094 * ___exception0, Object_t1970767703 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogException_m106933759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Logger_get_logEnabled_m2598788189(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_1 = Logger_get_logHandler_m1737771075(__this, /*hidden argument*/NULL);
		Exception_t2123675094 * L_2 = ___exception0;
		Object_t1970767703 * L_3 = ___context1;
		NullCheck(L_1);
		InterfaceActionInvoker2< Exception_t2123675094 *, Object_t1970767703 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2015281511_il2cpp_TypeInfo_var, L_1, L_2, L_3);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m730854658 (RuntimeObject * __this /* static, unused */, Stream_t3046340190 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ValidateLoadFromStream_m730854658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3046340190 * L_0 = ___stream0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t970256261 * L_1 = (ArgumentNullException_t970256261 *)il2cpp_codegen_object_new(ArgumentNullException_t970256261_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1624032228(L_1, _stringLiteral2945614662, _stringLiteral1443217705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Stream_t3046340190 * L_2 = ___stream0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_2);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		ArgumentException_t3637419113 * L_4 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_4, _stringLiteral2025743269, _stringLiteral1443217705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		Stream_t3046340190 * L_5 = ___stream0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_5);
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		ArgumentException_t3637419113 * L_7 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_7, _stringLiteral4259981313, _stringLiteral1443217705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamRead_m877885100 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3287329517* ___buffer0, int32_t ___offset1, int32_t ___count2, Stream_t3046340190 * ___stream3, intptr_t ___returnValueAddress4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamRead_m877885100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress4;
		bool L_1 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_2, _stringLiteral4070250858, _stringLiteral768326453, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Stream_t3046340190 * L_3 = ___stream3;
		ManagedStreamHelpers_ValidateLoadFromStream_m730854658(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress4;
		void* L_5 = IntPtr_op_Explicit_m1639871891(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t3046340190 * L_6 = ___stream3;
		ByteU5BU5D_t3287329517* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___count2;
		NullCheck(L_6);
		int32_t L_10 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3287329517*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, L_8, L_9);
		*((int32_t*)(L_5)) = (int32_t)L_10;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamSeek_m1677488971 (RuntimeObject * __this /* static, unused */, int64_t ___offset0, uint32_t ___origin1, Stream_t3046340190 * ___stream2, intptr_t ___returnValueAddress3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamSeek_m1677488971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress3;
		bool L_1 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_2, _stringLiteral4070250858, _stringLiteral768326453, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t3046340190 * L_3 = ___stream2;
		ManagedStreamHelpers_ValidateLoadFromStream_m730854658(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress3;
		void* L_5 = IntPtr_op_Explicit_m1639871891(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t3046340190 * L_6 = ___stream2;
		int64_t L_7 = ___offset0;
		uint32_t L_8 = ___origin1;
		NullCheck(L_6);
		int64_t L_9 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(16 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, L_7, L_8);
		*((int64_t*)(L_5)) = (int64_t)L_9;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamLength_m2106065483 (RuntimeObject * __this /* static, unused */, Stream_t3046340190 * ___stream0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamLength_m2106065483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m3008674881(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_2, _stringLiteral4070250858, _stringLiteral768326453, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t3046340190 * L_3 = ___stream0;
		ManagedStreamHelpers_ValidateLoadFromStream_m730854658(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m1639871891(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t3046340190 * L_6 = ___stream0;
		NullCheck(L_6);
		int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_6);
		*((int64_t*)(L_5)) = (int64_t)L_7;
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m1876270996 (Material_t2712136762 * __this, Material_t2712136762 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material__ctor_m1876270996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		Material_t2712136762 * L_0 = ___source0;
		Material_Internal_CreateWithMaterial_m2981100349(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m825231558 (Material_t2712136762 * __this, Color_t460381780  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material_set_color_m825231558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t460381780  L_0 = ___value0;
		Material_SetColor_m2365962888(__this, _stringLiteral1603114518, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern "C"  Texture_t2838694469 * Material_get_mainTexture_m1492267632 (Material_t2712136762 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTexture_m1492267632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture_t2838694469 * V_0 = NULL;
	{
		Texture_t2838694469 * L_0 = Material_GetTexture_m344891307(__this, _stringLiteral2338579479, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Texture_t2838694469 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
extern "C"  void Material_SetIntImpl_m3742874915 (Material_t2712136762 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*Material_SetIntImpl_m3742874915_ftn) (Material_t2712136762 *, int32_t, int32_t);
	static Material_SetIntImpl_m3742874915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetIntImpl_m3742874915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColorImpl_m2122336602 (Material_t2712136762 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetColorImpl_m1843334451(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColorImpl_m1843334451 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___self0, int32_t ___nameID1, Color_t460381780 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColorImpl_m1843334451_ftn) (Material_t2712136762 *, int32_t, Color_t460381780 *);
	static Material_INTERNAL_CALL_SetColorImpl_m1843334451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColorImpl_m1843334451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrixImpl_m113484655 (Material_t2712136762 * __this, int32_t ___nameID0, Matrix4x4_t2375577114  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetMatrixImpl_m3700682037(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Material_INTERNAL_CALL_SetMatrixImpl_m3700682037 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___self0, int32_t ___nameID1, Matrix4x4_t2375577114 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Material_INTERNAL_CALL_SetMatrixImpl_m3700682037_ftn) (Material_t2712136762 *, int32_t, Matrix4x4_t2375577114 *);
	static Material_INTERNAL_CALL_SetMatrixImpl_m3700682037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetMatrixImpl_m3700682037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTextureImpl_m3302679637 (Material_t2712136762 * __this, int32_t ___nameID0, Texture_t2838694469 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Material_SetTextureImpl_m3302679637_ftn) (Material_t2712136762 *, int32_t, Texture_t2838694469 *);
	static Material_SetTextureImpl_m3302679637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTextureImpl_m3302679637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern "C"  Texture_t2838694469 * Material_GetTextureImpl_m1718302891 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	typedef Texture_t2838694469 * (*Material_GetTextureImpl_m1718302891_ftn) (Material_t2712136762 *, int32_t);
	static Material_GetTextureImpl_m1718302891_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTextureImpl_m1718302891_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTextureImpl(System.Int32)");
	Texture_t2838694469 * retVal = _il2cpp_icall_func(__this, ___nameID0);
	return retVal;
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m3668621304 (Material_t2712136762 * __this, String_t* ___propertyName0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m2991897582(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m2991897582 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	typedef bool (*Material_HasProperty_m2991897582_ftn) (Material_t2712136762 *, int32_t);
	static Material_HasProperty_m2991897582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m2991897582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	bool retVal = _il2cpp_icall_func(__this, ___nameID0);
	return retVal;
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m2981100349 (RuntimeObject * __this /* static, unused */, Material_t2712136762 * ___mono0, Material_t2712136762 * ___source1, const RuntimeMethod* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m2981100349_ftn) (Material_t2712136762 *, Material_t2712136762 *);
	static Material_Internal_CreateWithMaterial_m2981100349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m2981100349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono0, ___source1);
}
// System.Void UnityEngine.Material::EnableKeyword(System.String)
extern "C"  void Material_EnableKeyword_m1768037422 (Material_t2712136762 * __this, String_t* ___keyword0, const RuntimeMethod* method)
{
	typedef void (*Material_EnableKeyword_m1768037422_ftn) (Material_t2712136762 *, String_t*);
	static Material_EnableKeyword_m1768037422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_EnableKeyword_m1768037422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::EnableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::DisableKeyword(System.String)
extern "C"  void Material_DisableKeyword_m3673342632 (Material_t2712136762 * __this, String_t* ___keyword0, const RuntimeMethod* method)
{
	typedef void (*Material_DisableKeyword_m3673342632_ftn) (Material_t2712136762 *, String_t*);
	static Material_DisableKeyword_m3673342632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_DisableKeyword_m3673342632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::DisableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m1286560417 (Material_t2712136762 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value1;
		Material_SetInt_m1768491861(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C"  void Material_SetInt_m1768491861 (Material_t2712136762 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		int32_t L_1 = ___value1;
		Material_SetIntImpl_m3742874915(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m2365962888 (Material_t2712136762 * __this, String_t* ___name0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t460381780  L_2 = ___value1;
		Material_SetColor_m2173280049(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m2173280049 (Material_t2712136762 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Color_t460381780  L_1 = ___value1;
		Material_SetColorImpl_m2122336602(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m2294933890 (Material_t2712136762 * __this, String_t* ___name0, Matrix4x4_t2375577114  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_2 = ___value1;
		Material_SetMatrix_m3498861241(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m3498861241 (Material_t2712136762 * __this, int32_t ___nameID0, Matrix4x4_t2375577114  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Matrix4x4_t2375577114  L_1 = ___value1;
		Material_SetMatrixImpl_m113484655(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m825494064 (Material_t2712136762 * __this, String_t* ___name0, Texture_t2838694469 * ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2838694469 * L_2 = ___value1;
		Material_SetTexture_m2280997817(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m2280997817 (Material_t2712136762 * __this, int32_t ___nameID0, Texture_t2838694469 * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Texture_t2838694469 * L_1 = ___value1;
		Material_SetTextureImpl_m3302679637(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t2838694469 * Material_GetTexture_m344891307 (Material_t2712136762 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	Texture_t2838694469 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2838694469 * L_2 = Material_GetTexture_m256980722(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Texture_t2838694469 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t2838694469 * Material_GetTexture_m256980722 (Material_t2712136762 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	Texture_t2838694469 * V_0 = NULL;
	{
		int32_t L_0 = ___nameID0;
		Texture_t2838694469 * L_1 = Material_GetTextureImpl_m1718302891(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Texture_t2838694469 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern "C"  void MaterialPropertyBlock__ctor_m2999207607 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2095069727(__this, /*hidden argument*/NULL);
		MaterialPropertyBlock_InitBlock_m3286158498(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
extern "C"  void MaterialPropertyBlock_InitBlock_m3286158498 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_InitBlock_m3286158498_ftn) (MaterialPropertyBlock_t3417007309 *);
	static MaterialPropertyBlock_InitBlock_m3286158498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_InitBlock_m3286158498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::InitBlock()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
extern "C"  void MaterialPropertyBlock_DestroyBlock_m1450478 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_DestroyBlock_m1450478_ftn) (MaterialPropertyBlock_t3417007309 *);
	static MaterialPropertyBlock_DestroyBlock_m1450478_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_DestroyBlock_m1450478_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::DestroyBlock()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MaterialPropertyBlock::Finalize()
extern "C"  void MaterialPropertyBlock_Finalize_m4293291726 (MaterialPropertyBlock_t3417007309 * __this, const RuntimeMethod* method)
{
	Exception_t2123675094 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2123675094 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		MaterialPropertyBlock_DestroyBlock_m1450478(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2123675094 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2450496781(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2123675094 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColorImpl_m4025622658 (MaterialPropertyBlock_t3417007309 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241 (RuntimeObject * __this /* static, unused */, MaterialPropertyBlock_t3417007309 * ___self0, int32_t ___nameID1, Color_t460381780 * ___value2, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241_ftn) (MaterialPropertyBlock_t3417007309 *, int32_t, Color_t460381780 *);
	static MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m272919241_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m1767458416 (MaterialPropertyBlock_t3417007309 * __this, String_t* ___name0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m3888350337(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t460381780  L_2 = ___value1;
		MaterialPropertyBlock_SetColor_m292668400(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m292668400 (MaterialPropertyBlock_t3417007309 * __this, int32_t ___nameID0, Color_t460381780  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Color_t460381780  L_1 = ___value1;
		MaterialPropertyBlock_SetColorImpl_m4025622658(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C"  float Mathf_Sin_m1977288281 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = sin((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C"  float Mathf_Cos_m1949046804 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = cos((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C"  float Mathf_Sqrt_m461732699 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = sqrt((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m3341836143 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		V_0 = (((float)((float)L_1)));
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m2514331926 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m590424322 (RuntimeObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m1074792940 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m4089987154 (RuntimeObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C"  float Mathf_Pow_m1416809500 (RuntimeObject * __this /* static, unused */, float ___f0, float ___p1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		V_0 = (((float)((float)L_2)));
		goto IL_0011;
	}

IL_0011:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C"  float Mathf_Log_m160720356 (RuntimeObject * __this /* static, unused */, float ___f0, float ___p1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = Math_Log_m395471811(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		V_0 = (((float)((float)L_2)));
		goto IL_0011;
	}

IL_0011:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C"  float Mathf_Floor_m1186113274 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C"  float Mathf_Round_m1952738248 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m2173476573 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m1142562325 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m1150531888 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m1322848001 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001b;
	}

IL_0016:
	{
		G_B3_0 = (-1.0f);
	}

IL_001b:
	{
		V_0 = G_B3_0;
		goto IL_0021;
	}

IL_0021:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1749346724 (RuntimeObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0010;
		}
	}
	{
		float L_2 = ___min1;
		___value0 = L_2;
		goto IL_001a;
	}

IL_0010:
	{
		float L_3 = ___value0;
		float L_4 = ___max2;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_001a;
		}
	}
	{
		float L_5 = ___max2;
		___value0 = L_5;
	}

IL_001a:
	{
		float L_6 = ___value0;
		V_0 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m1673146398 (RuntimeObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___min1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = ___min1;
		___value0 = L_2;
		goto IL_001a;
	}

IL_0010:
	{
		int32_t L_3 = ___value0;
		int32_t L_4 = ___max2;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_5 = ___max2;
		___value0 = L_5;
	}

IL_001a:
	{
		int32_t L_6 = ___value0;
		V_0 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2122253184 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		V_0 = (0.0f);
		goto IL_0034;
	}

IL_0017:
	{
		float L_1 = ___value0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		V_0 = (1.0f);
		goto IL_0034;
	}

IL_002d:
	{
		float L_2 = ___value0;
		V_0 = L_2;
		goto IL_0034;
	}

IL_0034:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1308516861 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Lerp_m1308516861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
		goto IL_0013;
	}

IL_0013:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1485428512 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1485428512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m1074792940(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t2081007568_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t2081007568_il2cpp_TypeInfo_var))->get_Epsilon_0();
		float L_9 = Mathf_Max_m1074792940(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
		goto IL_0038;
	}

IL_0038:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern "C"  float Mathf_SmoothDamp_m724208596 (RuntimeObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m724208596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		float L_0 = ___smoothTime3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m1074792940(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime3 = L_1;
		float L_2 = ___smoothTime3;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime5;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current0;
		float L_12 = ___target1;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target1;
		V_4 = L_13;
		float L_14 = ___maxSpeed4;
		float L_15 = ___smoothTime3;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m1749346724(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current0;
		float L_21 = V_3;
		___target1 = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity2;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime5;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity2;
		float* L_27 = ___currentVelocity2;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target1;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current0;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity2;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime5;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a3:
	{
		float L_44 = V_7;
		V_8 = L_44;
		goto IL_00ac;
	}

IL_00ac:
	{
		float L_45 = V_8;
		return L_45;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C"  float Mathf_Repeat_m2357693841 (RuntimeObject * __this /* static, unused */, float ___t0, float ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Repeat_m2357693841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___t0;
		float L_1 = ___t0;
		float L_2 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length1;
		float L_5 = ___length1;
		float L_6 = Mathf_Clamp_m1749346724(NULL /*static, unused*/, ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4)))), (0.0f), L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_InverseLerp_m2415857371 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, float ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_InverseLerp_m2415857371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		float L_2 = ___value2;
		float L_3 = ___a0;
		float L_4 = ___b1;
		float L_5 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2081007568_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m2122253184(NULL /*static, unused*/, ((float)((float)((float)((float)L_2-(float)L_3))/(float)((float)((float)L_4-(float)L_5)))), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0025;
	}

IL_001a:
	{
		V_0 = (0.0f);
		goto IL_0025;
	}

IL_0025:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern "C"  void Mathf__cctor_m1374701236 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m1374701236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1363288871_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t1363288871_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1363288871_il2cpp_TypeInfo_var))->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1363288871_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t1363288871_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1363288871_il2cpp_TypeInfo_var))->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1363288871_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t1363288871_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1363288871_il2cpp_TypeInfo_var))->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t2081007568_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t2081007568_il2cpp_TypeInfo_var))->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m3113358119 (Matrix4x4_t2375577114 * __this, Vector4_t380635127  ___column00, Vector4_t380635127  ___column11, Vector4_t380635127  ___column22, Vector4_t380635127  ___column33, const RuntimeMethod* method)
{
	{
		float L_0 = (&___column00)->get_x_1();
		__this->set_m00_0(L_0);
		float L_1 = (&___column11)->get_x_1();
		__this->set_m01_4(L_1);
		float L_2 = (&___column22)->get_x_1();
		__this->set_m02_8(L_2);
		float L_3 = (&___column33)->get_x_1();
		__this->set_m03_12(L_3);
		float L_4 = (&___column00)->get_y_2();
		__this->set_m10_1(L_4);
		float L_5 = (&___column11)->get_y_2();
		__this->set_m11_5(L_5);
		float L_6 = (&___column22)->get_y_2();
		__this->set_m12_9(L_6);
		float L_7 = (&___column33)->get_y_2();
		__this->set_m13_13(L_7);
		float L_8 = (&___column00)->get_z_3();
		__this->set_m20_2(L_8);
		float L_9 = (&___column11)->get_z_3();
		__this->set_m21_6(L_9);
		float L_10 = (&___column22)->get_z_3();
		__this->set_m22_10(L_10);
		float L_11 = (&___column33)->get_z_3();
		__this->set_m23_14(L_11);
		float L_12 = (&___column00)->get_w_4();
		__this->set_m30_3(L_12);
		float L_13 = (&___column11)->get_w_4();
		__this->set_m31_7(L_13);
		float L_14 = (&___column22)->get_w_4();
		__this->set_m32_11(L_14);
		float L_15 = (&___column33)->get_w_4();
		__this->set_m33_15(L_15);
		return;
	}
}
extern "C"  void Matrix4x4__ctor_m3113358119_AdjustorThunk (RuntimeObject * __this, Vector4_t380635127  ___column00, Vector4_t380635127  ___column11, Vector4_t380635127  ___column22, Vector4_t380635127  ___column33, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	Matrix4x4__ctor_m3113358119(_thisAdjusted, ___column00, ___column11, ___column22, ___column33, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2375577114  Matrix4x4_TRS_m2667050262 (RuntimeObject * __this /* static, unused */, Vector3_t329709361  ___pos0, Quaternion_t2761156409  ___q1, Vector3_t329709361  ___s2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_TRS_m2667050262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2375577114  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2375577114_il2cpp_TypeInfo_var);
		Matrix4x4_INTERNAL_CALL_TRS_m1078286175(NULL /*static, unused*/, (&___pos0), (&___q1), (&___s2), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_0 = V_0;
		V_1 = L_0;
		goto IL_0015;
	}

IL_0015:
	{
		Matrix4x4_t2375577114  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m1078286175 (RuntimeObject * __this /* static, unused */, Vector3_t329709361 * ___pos0, Quaternion_t2761156409 * ___q1, Vector3_t329709361 * ___s2, Matrix4x4_t2375577114 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_TRS_m1078286175_ftn) (Vector3_t329709361 *, Quaternion_t2761156409 *, Vector3_t329709361 *, Matrix4x4_t2375577114 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m1078286175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m1078286175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___pos0, ___q1, ___s2, ___value3);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m1739517094 (Matrix4x4_t2375577114 * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = Matrix4x4_get_Item_m592394008(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		float L_3 = V_0;
		return L_3;
	}
}
extern "C"  float Matrix4x4_get_Item_m1739517094_AdjustorThunk (RuntimeObject * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_get_Item_m1739517094(_thisAdjusted, ___row0, ___column1, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1674402704 (Matrix4x4_t2375577114 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = ___value2;
		Matrix4x4_set_Item_m942837760(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m1674402704_AdjustorThunk (RuntimeObject * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	Matrix4x4_set_Item_m1674402704(_thisAdjusted, ___row0, ___column1, ___value2, method);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m592394008 (Matrix4x4_t2375577114 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_Item_m592394008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_0064;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007c;
			}
			case 5:
			{
				goto IL_0088;
			}
			case 6:
			{
				goto IL_0094;
			}
			case 7:
			{
				goto IL_00a0;
			}
			case 8:
			{
				goto IL_00ac;
			}
			case 9:
			{
				goto IL_00b8;
			}
			case 10:
			{
				goto IL_00c4;
			}
			case 11:
			{
				goto IL_00d0;
			}
			case 12:
			{
				goto IL_00dc;
			}
			case 13:
			{
				goto IL_00e8;
			}
			case 14:
			{
				goto IL_00f4;
			}
			case 15:
			{
				goto IL_0100;
			}
		}
	}
	{
		goto IL_010c;
	}

IL_004c:
	{
		float L_1 = __this->get_m00_0();
		V_0 = L_1;
		goto IL_0117;
	}

IL_0058:
	{
		float L_2 = __this->get_m10_1();
		V_0 = L_2;
		goto IL_0117;
	}

IL_0064:
	{
		float L_3 = __this->get_m20_2();
		V_0 = L_3;
		goto IL_0117;
	}

IL_0070:
	{
		float L_4 = __this->get_m30_3();
		V_0 = L_4;
		goto IL_0117;
	}

IL_007c:
	{
		float L_5 = __this->get_m01_4();
		V_0 = L_5;
		goto IL_0117;
	}

IL_0088:
	{
		float L_6 = __this->get_m11_5();
		V_0 = L_6;
		goto IL_0117;
	}

IL_0094:
	{
		float L_7 = __this->get_m21_6();
		V_0 = L_7;
		goto IL_0117;
	}

IL_00a0:
	{
		float L_8 = __this->get_m31_7();
		V_0 = L_8;
		goto IL_0117;
	}

IL_00ac:
	{
		float L_9 = __this->get_m02_8();
		V_0 = L_9;
		goto IL_0117;
	}

IL_00b8:
	{
		float L_10 = __this->get_m12_9();
		V_0 = L_10;
		goto IL_0117;
	}

IL_00c4:
	{
		float L_11 = __this->get_m22_10();
		V_0 = L_11;
		goto IL_0117;
	}

IL_00d0:
	{
		float L_12 = __this->get_m32_11();
		V_0 = L_12;
		goto IL_0117;
	}

IL_00dc:
	{
		float L_13 = __this->get_m03_12();
		V_0 = L_13;
		goto IL_0117;
	}

IL_00e8:
	{
		float L_14 = __this->get_m13_13();
		V_0 = L_14;
		goto IL_0117;
	}

IL_00f4:
	{
		float L_15 = __this->get_m23_14();
		V_0 = L_15;
		goto IL_0117;
	}

IL_0100:
	{
		float L_16 = __this->get_m33_15();
		V_0 = L_16;
		goto IL_0117;
	}

IL_010c:
	{
		IndexOutOfRangeException_t3921978895 * L_17 = (IndexOutOfRangeException_t3921978895 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3132318046(L_17, _stringLiteral1189015226, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0117:
	{
		float L_18 = V_0;
		return L_18;
	}
}
extern "C"  float Matrix4x4_get_Item_m592394008_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_get_Item_m592394008(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m942837760 (Matrix4x4_t2375577114 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_set_Item_m942837760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_0064;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007c;
			}
			case 5:
			{
				goto IL_0088;
			}
			case 6:
			{
				goto IL_0094;
			}
			case 7:
			{
				goto IL_00a0;
			}
			case 8:
			{
				goto IL_00ac;
			}
			case 9:
			{
				goto IL_00b8;
			}
			case 10:
			{
				goto IL_00c4;
			}
			case 11:
			{
				goto IL_00d0;
			}
			case 12:
			{
				goto IL_00dc;
			}
			case 13:
			{
				goto IL_00e8;
			}
			case 14:
			{
				goto IL_00f4;
			}
			case 15:
			{
				goto IL_0100;
			}
		}
	}
	{
		goto IL_010c;
	}

IL_004c:
	{
		float L_1 = ___value1;
		__this->set_m00_0(L_1);
		goto IL_0117;
	}

IL_0058:
	{
		float L_2 = ___value1;
		__this->set_m10_1(L_2);
		goto IL_0117;
	}

IL_0064:
	{
		float L_3 = ___value1;
		__this->set_m20_2(L_3);
		goto IL_0117;
	}

IL_0070:
	{
		float L_4 = ___value1;
		__this->set_m30_3(L_4);
		goto IL_0117;
	}

IL_007c:
	{
		float L_5 = ___value1;
		__this->set_m01_4(L_5);
		goto IL_0117;
	}

IL_0088:
	{
		float L_6 = ___value1;
		__this->set_m11_5(L_6);
		goto IL_0117;
	}

IL_0094:
	{
		float L_7 = ___value1;
		__this->set_m21_6(L_7);
		goto IL_0117;
	}

IL_00a0:
	{
		float L_8 = ___value1;
		__this->set_m31_7(L_8);
		goto IL_0117;
	}

IL_00ac:
	{
		float L_9 = ___value1;
		__this->set_m02_8(L_9);
		goto IL_0117;
	}

IL_00b8:
	{
		float L_10 = ___value1;
		__this->set_m12_9(L_10);
		goto IL_0117;
	}

IL_00c4:
	{
		float L_11 = ___value1;
		__this->set_m22_10(L_11);
		goto IL_0117;
	}

IL_00d0:
	{
		float L_12 = ___value1;
		__this->set_m32_11(L_12);
		goto IL_0117;
	}

IL_00dc:
	{
		float L_13 = ___value1;
		__this->set_m03_12(L_13);
		goto IL_0117;
	}

IL_00e8:
	{
		float L_14 = ___value1;
		__this->set_m13_13(L_14);
		goto IL_0117;
	}

IL_00f4:
	{
		float L_15 = ___value1;
		__this->set_m23_14(L_15);
		goto IL_0117;
	}

IL_0100:
	{
		float L_16 = ___value1;
		__this->set_m33_15(L_16);
		goto IL_0117;
	}

IL_010c:
	{
		IndexOutOfRangeException_t3921978895 * L_17 = (IndexOutOfRangeException_t3921978895 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3132318046(L_17, _stringLiteral1189015226, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0117:
	{
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m942837760_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	Matrix4x4_set_Item_m942837760(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m2403490898 (Matrix4x4_t2375577114 * __this, const RuntimeMethod* method)
{
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t380635127  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t380635127  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t380635127  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	{
		Vector4_t380635127  L_0 = Matrix4x4_GetColumn_m1167652957(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3672563191((&V_0), /*hidden argument*/NULL);
		Vector4_t380635127  L_2 = Matrix4x4_GetColumn_m1167652957(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m3672563191((&V_1), /*hidden argument*/NULL);
		Vector4_t380635127  L_4 = Matrix4x4_GetColumn_m1167652957(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m3672563191((&V_2), /*hidden argument*/NULL);
		Vector4_t380635127  L_6 = Matrix4x4_GetColumn_m1167652957(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m3672563191((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0065;
	}

IL_0065:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m2403490898_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_GetHashCode_m2403490898(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m2605048655 (Matrix4x4_t2375577114 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m2605048655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Matrix4x4_t2375577114  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t380635127  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t380635127  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t380635127  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector4_t380635127  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Matrix4x4_t2375577114_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00bc;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Matrix4x4_t2375577114 *)((Matrix4x4_t2375577114 *)UnBox(L_1, Matrix4x4_t2375577114_il2cpp_TypeInfo_var))));
		Vector4_t380635127  L_2 = Matrix4x4_GetColumn_m1167652957(__this, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector4_t380635127  L_3 = Matrix4x4_GetColumn_m1167652957((&V_1), 0, /*hidden argument*/NULL);
		Vector4_t380635127  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector4_t380635127_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m2175095089((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t380635127  L_7 = Matrix4x4_GetColumn_m1167652957(__this, 1, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector4_t380635127  L_8 = Matrix4x4_GetColumn_m1167652957((&V_1), 1, /*hidden argument*/NULL);
		Vector4_t380635127  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector4_t380635127_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m2175095089((&V_3), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t380635127  L_12 = Matrix4x4_GetColumn_m1167652957(__this, 2, /*hidden argument*/NULL);
		V_4 = L_12;
		Vector4_t380635127  L_13 = Matrix4x4_GetColumn_m1167652957((&V_1), 2, /*hidden argument*/NULL);
		Vector4_t380635127  L_14 = L_13;
		RuntimeObject * L_15 = Box(Vector4_t380635127_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m2175095089((&V_4), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t380635127  L_17 = Matrix4x4_GetColumn_m1167652957(__this, 3, /*hidden argument*/NULL);
		V_5 = L_17;
		Vector4_t380635127  L_18 = Matrix4x4_GetColumn_m1167652957((&V_1), 3, /*hidden argument*/NULL);
		Vector4_t380635127  L_19 = L_18;
		RuntimeObject * L_20 = Box(Vector4_t380635127_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m2175095089((&V_5), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B7_0 = 0;
	}

IL_00b6:
	{
		V_0 = (bool)G_B7_0;
		goto IL_00bc;
	}

IL_00bc:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
extern "C"  bool Matrix4x4_Equals_m2605048655_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_Equals_m2605048655(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t380635127  Matrix4x4_GetColumn_m1167652957 (Matrix4x4_t2375577114 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_GetColumn_m1167652957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t380635127  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_0085;
			}
		}
	}
	{
		goto IL_00a8;
	}

IL_001c:
	{
		float L_1 = __this->get_m00_0();
		float L_2 = __this->get_m10_1();
		float L_3 = __this->get_m20_2();
		float L_4 = __this->get_m30_3();
		Vector4_t380635127  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m3770092030((&L_5), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_00b3;
	}

IL_003f:
	{
		float L_6 = __this->get_m01_4();
		float L_7 = __this->get_m11_5();
		float L_8 = __this->get_m21_6();
		float L_9 = __this->get_m31_7();
		Vector4_t380635127  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m3770092030((&L_10), L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_00b3;
	}

IL_0062:
	{
		float L_11 = __this->get_m02_8();
		float L_12 = __this->get_m12_9();
		float L_13 = __this->get_m22_10();
		float L_14 = __this->get_m32_11();
		Vector4_t380635127  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector4__ctor_m3770092030((&L_15), L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_00b3;
	}

IL_0085:
	{
		float L_16 = __this->get_m03_12();
		float L_17 = __this->get_m13_13();
		float L_18 = __this->get_m23_14();
		float L_19 = __this->get_m33_15();
		Vector4_t380635127  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m3770092030((&L_20), L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00b3;
	}

IL_00a8:
	{
		IndexOutOfRangeException_t3921978895 * L_21 = (IndexOutOfRangeException_t3921978895 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3921978895_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3132318046(L_21, _stringLiteral2723610844, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00b3:
	{
		Vector4_t380635127  L_22 = V_0;
		return L_22;
	}
}
extern "C"  Vector4_t380635127  Matrix4x4_GetColumn_m1167652957_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_GetColumn_m1167652957(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m3548282205 (Matrix4x4_t2375577114 * __this, int32_t ___index0, Vector4_t380635127  ___column1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		float L_1 = (&___column1)->get_x_1();
		Matrix4x4_set_Item_m1674402704(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___index0;
		float L_3 = (&___column1)->get_y_2();
		Matrix4x4_set_Item_m1674402704(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___index0;
		float L_5 = (&___column1)->get_z_3();
		Matrix4x4_set_Item_m1674402704(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___index0;
		float L_7 = (&___column1)->get_w_4();
		Matrix4x4_set_Item_m1674402704(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_SetColumn_m3548282205_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, Vector4_t380635127  ___column1, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	Matrix4x4_SetColumn_m3548282205(_thisAdjusted, ___index0, ___column1, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t329709361  Matrix4x4_MultiplyPoint3x4_m2159497023 (Matrix4x4_t2375577114 * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	Vector3_t329709361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t329709361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = __this->get_m00_0();
		float L_1 = (&___point0)->get_x_1();
		float L_2 = __this->get_m01_4();
		float L_3 = (&___point0)->get_y_2();
		float L_4 = __this->get_m02_8();
		float L_5 = (&___point0)->get_z_3();
		float L_6 = __this->get_m03_12();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6)));
		float L_7 = __this->get_m10_1();
		float L_8 = (&___point0)->get_x_1();
		float L_9 = __this->get_m11_5();
		float L_10 = (&___point0)->get_y_2();
		float L_11 = __this->get_m12_9();
		float L_12 = (&___point0)->get_z_3();
		float L_13 = __this->get_m13_13();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13)));
		float L_14 = __this->get_m20_2();
		float L_15 = (&___point0)->get_x_1();
		float L_16 = __this->get_m21_6();
		float L_17 = (&___point0)->get_y_2();
		float L_18 = __this->get_m22_10();
		float L_19 = (&___point0)->get_z_3();
		float L_20 = __this->get_m23_14();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20)));
		Vector3_t329709361  L_21 = V_0;
		V_1 = L_21;
		goto IL_00b6;
	}

IL_00b6:
	{
		Vector3_t329709361  L_22 = V_1;
		return L_22;
	}
}
extern "C"  Vector3_t329709361  Matrix4x4_MultiplyPoint3x4_m2159497023_AdjustorThunk (RuntimeObject * __this, Vector3_t329709361  ___point0, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_MultiplyPoint3x4_m2159497023(_thisAdjusted, ___point0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern "C"  Matrix4x4_t2375577114  Matrix4x4_get_identity_m386701337 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m386701337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2375577114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2375577114_il2cpp_TypeInfo_var);
		Matrix4x4_t2375577114  L_0 = ((Matrix4x4_t2375577114_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t2375577114_il2cpp_TypeInfo_var))->get_identityMatrix_17();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Matrix4x4_t2375577114  L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m782477224 (Matrix4x4_t2375577114 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m782477224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1568665923* L_0 = ((ObjectU5BU5D_t1568665923*)SZArrayNew(ObjectU5BU5D_t1568665923_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1568665923* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1568665923* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1568665923* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		ObjectU5BU5D_t1568665923* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		RuntimeObject * L_19 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_19);
		ObjectU5BU5D_t1568665923* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		RuntimeObject * L_23 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_23);
		ObjectU5BU5D_t1568665923* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		RuntimeObject * L_27 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_27);
		ObjectU5BU5D_t1568665923* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		RuntimeObject * L_31 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_31);
		ObjectU5BU5D_t1568665923* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_35);
		ObjectU5BU5D_t1568665923* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		RuntimeObject * L_39 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_39);
		ObjectU5BU5D_t1568665923* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		RuntimeObject * L_43 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_43);
		ObjectU5BU5D_t1568665923* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		RuntimeObject * L_47 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_47);
		ObjectU5BU5D_t1568665923* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		RuntimeObject * L_51 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_51);
		ObjectU5BU5D_t1568665923* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_55);
		ObjectU5BU5D_t1568665923* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		RuntimeObject * L_59 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_59);
		ObjectU5BU5D_t1568665923* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		RuntimeObject * L_63 = Box(Single_t1863352746_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_63);
		String_t* L_64 = UnityString_Format_m1584616489(NULL /*static, unused*/, _stringLiteral4045066072, L_60, /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_00ff;
	}

IL_00ff:
	{
		String_t* L_65 = V_0;
		return L_65;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m782477224_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t2375577114 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2375577114 *>(__this + 1);
	return Matrix4x4_ToString_m782477224(_thisAdjusted, method);
}
// System.Void UnityEngine.Matrix4x4::.cctor()
extern "C"  void Matrix4x4__cctor_m590356860 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4__cctor_m590356860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t380635127  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m3770092030((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m3770092030((&L_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m3770092030((&L_2), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m3770092030((&L_3), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Matrix4x4__ctor_m3113358119((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		((Matrix4x4_t2375577114_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t2375577114_il2cpp_TypeInfo_var))->set_zeroMatrix_16(L_4);
		Vector4_t380635127  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m3770092030((&L_5), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector4__ctor_m3770092030((&L_6), (0.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector4__ctor_m3770092030((&L_7), (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t380635127  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m3770092030((&L_8), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2375577114  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Matrix4x4__ctor_m3113358119((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		((Matrix4x4_t2375577114_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t2375577114_il2cpp_TypeInfo_var))->set_identityMatrix_17(L_9);
		return;
	}
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m1598271010 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh__ctor_m1598271010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1970767703_il2cpp_TypeInfo_var);
		Object__ctor_m132342697(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m852979627(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m852979627 (RuntimeObject * __this /* static, unused */, Mesh_t1621212487 * ___mono0, const RuntimeMethod* method)
{
	typedef void (*Mesh_Internal_Create_m852979627_ftn) (Mesh_t1621212487 *);
	static Mesh_Internal_Create_m852979627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m852979627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m1183388312 (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___arraySize4, const RuntimeMethod* method)
{
	typedef void (*Mesh_SetArrayForChannelImpl_m1183388312_ftn) (Mesh_t1621212487 *, int32_t, int32_t, int32_t, RuntimeArray *, int32_t);
	static Mesh_SetArrayForChannelImpl_m1183388312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetArrayForChannelImpl_m1183388312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)");
	_il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2, ___values3, ___arraySize4);
}
// System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  RuntimeArray * Mesh_GetAllocArrayFromChannelImpl_m804461491 (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*Mesh_GetAllocArrayFromChannelImpl_m804461491_ftn) (Mesh_t1621212487 *, int32_t, int32_t, int32_t);
	static Mesh_GetAllocArrayFromChannelImpl_m804461491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetAllocArrayFromChannelImpl_m804461491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)");
	RuntimeArray * retVal = _il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2);
	return retVal;
}
// System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  bool Mesh_HasChannel_m3789168033 (Mesh_t1621212487 * __this, int32_t ___channel0, const RuntimeMethod* method)
{
	typedef bool (*Mesh_HasChannel_m3789168033_ftn) (Mesh_t1621212487 *, int32_t);
	static Mesh_HasChannel_m3789168033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_HasChannel_m3789168033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)");
	bool retVal = _il2cpp_icall_func(__this, ___channel0);
	return retVal;
}
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  RuntimeArray * Mesh_ExtractArrayFromList_m3063324901 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___list0, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*Mesh_ExtractArrayFromList_m3063324901_ftn) (RuntimeObject *);
	static Mesh_ExtractArrayFromList_m3063324901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ExtractArrayFromList_m3063324901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ExtractArrayFromList(System.Object)");
	RuntimeArray * retVal = _il2cpp_icall_func(___list0);
	return retVal;
}
// System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3565237794* Mesh_GetIndicesImpl_m3232595394 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	typedef Int32U5BU5D_t3565237794* (*Mesh_GetIndicesImpl_m3232595394_ftn) (Mesh_t1621212487 *, int32_t);
	static Mesh_GetIndicesImpl_m3232595394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetIndicesImpl_m3232595394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetIndicesImpl(System.Int32)");
	Int32U5BU5D_t3565237794* retVal = _il2cpp_icall_func(__this, ___submesh0);
	return retVal;
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m164793241 (Mesh_t1621212487 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const RuntimeMethod* method)
{
	typedef void (*Mesh_SetTrianglesImpl_m164793241_ftn) (Mesh_t1621212487 *, int32_t, RuntimeArray *, int32_t, bool);
	static Mesh_SetTrianglesImpl_m164793241_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesImpl_m164793241_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___submesh0, ___triangles1, ___arraySize2, ___calculateBounds3);
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetTrianglesImpl_m3619734739 (Mesh_t1621212487 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___submesh0;
		RuntimeArray * L_1 = ___triangles1;
		int32_t L_2 = ___arraySize2;
		bool L_3 = V_0;
		Mesh_SetTrianglesImpl_m164793241(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void Mesh_SetTriangles_m351623095 (Mesh_t1621212487 * __this, List_1_t309455265 * ___triangles0, int32_t ___submesh1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		List_1_t309455265 * L_0 = ___triangles0;
		int32_t L_1 = ___submesh1;
		bool L_2 = V_0;
		Mesh_SetTriangles_m1728167629(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTriangles_m1728167629 (Mesh_t1621212487 * __this, List_1_t309455265 * ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTriangles_m1728167629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___submesh1;
		bool L_1 = Mesh_CheckCanAccessSubmeshTriangles_m3893594630(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = ___submesh1;
		List_1_t309455265 * L_3 = ___triangles0;
		RuntimeArray * L_4 = Mesh_ExtractArrayFromList_m3063324901(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		List_1_t309455265 * L_5 = ___triangles0;
		int32_t L_6 = Mesh_SafeLength_TisInt32_t499004851_m2496825826(__this, L_5, /*hidden argument*/Mesh_SafeLength_TisInt32_t499004851_m2496825826_RuntimeMethod_var);
		bool L_7 = ___calculateBounds2;
		Mesh_SetTrianglesImpl_m164793241(__this, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m284991026 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	typedef bool (*Mesh_get_canAccess_m284991026_ftn) (Mesh_t1621212487 *);
	static Mesh_get_canAccess_m284991026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_canAccess_m284991026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_canAccess()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m1718999019 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Mesh_get_subMeshCount_m1718999019_ftn) (Mesh_t1621212487 *);
	static Mesh_get_subMeshCount_m1718999019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_subMeshCount_m1718999019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_subMeshCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern "C"  void Mesh_ClearImpl_m1737913970 (Mesh_t1621212487 * __this, bool ___keepVertexLayout0, const RuntimeMethod* method)
{
	typedef void (*Mesh_ClearImpl_m1737913970_ftn) (Mesh_t1621212487 *, bool);
	static Mesh_ClearImpl_m1737913970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ClearImpl_m1737913970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ClearImpl(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
extern "C"  void Mesh_RecalculateBoundsImpl_m4129815417 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	typedef void (*Mesh_RecalculateBoundsImpl_m4129815417_ftn) (Mesh_t1621212487 *);
	static Mesh_RecalculateBoundsImpl_m4129815417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBoundsImpl_m4129815417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBoundsImpl()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::RecalculateNormalsImpl()
extern "C"  void Mesh_RecalculateNormalsImpl_m3938918416 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	typedef void (*Mesh_RecalculateNormalsImpl_m3938918416_ftn) (Mesh_t1621212487 *);
	static Mesh_RecalculateNormalsImpl_m3938918416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateNormalsImpl_m3938918416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateNormalsImpl()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessChannel_m4229761159 (Mesh_t1621212487 * __this, int32_t ___ch0, const RuntimeMethod* method)
{
	typedef void (*Mesh_PrintErrorCantAccessChannel_m4229761159_ftn) (Mesh_t1621212487 *, int32_t);
	static Mesh_PrintErrorCantAccessChannel_m4229761159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessChannel_m4229761159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)");
	_il2cpp_icall_func(__this, ___ch0);
}
// UnityEngine.Mesh/InternalShaderChannel UnityEngine.Mesh::GetUVChannel(System.Int32)
extern "C"  int32_t Mesh_GetUVChannel_m414321819 (Mesh_t1621212487 * __this, int32_t ___uvIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetUVChannel_m414321819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___uvIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___uvIndex0;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_001f;
		}
	}

IL_000f:
	{
		ArgumentException_t3637419113 * L_2 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_2, _stringLiteral1098436485, _stringLiteral3386726826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		int32_t L_3 = ___uvIndex0;
		V_0 = ((int32_t)((int32_t)3+(int32_t)L_3));
		goto IL_0028;
	}

IL_0028:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  int32_t Mesh_DefaultDimensionForChannel_m3431474595 (RuntimeObject * __this /* static, unused */, int32_t ___channel0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_DefaultDimensionForChannel_m3431474595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___channel0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}

IL_000e:
	{
		V_0 = 3;
		goto IL_004f;
	}

IL_0015:
	{
		int32_t L_2 = ___channel0;
		if ((((int32_t)L_2) < ((int32_t)3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = ___channel0;
		if ((((int32_t)L_3) > ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		V_0 = 2;
		goto IL_004f;
	}

IL_002a:
	{
		int32_t L_4 = ___channel0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = ___channel0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		V_0 = 4;
		goto IL_004f;
	}

IL_003f:
	{
		ArgumentException_t3637419113 * L_6 = (ArgumentException_t3637419113 *)il2cpp_codegen_object_new(ArgumentException_t3637419113_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3645081522(L_6, _stringLiteral3224309003, _stringLiteral1925967687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_004f:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Int32 UnityEngine.Mesh::SafeLength(System.Array)
extern "C"  int32_t Mesh_SafeLength_m930608131 (Mesh_t1621212487 * __this, RuntimeArray * ___values0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		RuntimeArray * L_0 = ___values0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeArray * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m2009775185(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		V_0 = G_B3_0;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetSizedArrayForChannel_m1431507522 (Mesh_t1621212487 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___valuesCount4, const RuntimeMethod* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m284991026(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		int32_t L_2 = ___format1;
		int32_t L_3 = ___dim2;
		RuntimeArray * L_4 = ___values3;
		int32_t L_5 = ___valuesCount4;
		Mesh_SetArrayForChannelImpl_m1183388312(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001e:
	{
		int32_t L_6 = ___channel0;
		Mesh_PrintErrorCantAccessChannel_m4229761159(__this, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t974944492* Mesh_get_vertices_m4085462919 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_vertices_m4085462919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t974944492* V_0 = NULL;
	{
		Vector3U5BU5D_t974944492* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429(__this, 0, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector3U5BU5D_t974944492* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m1491965711 (Mesh_t1621212487 * __this, Vector3U5BU5D_t974944492* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_vertices_m1491965711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3U5BU5D_t974944492* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940(__this, 0, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisVector3_t329709361_m545091940_RuntimeMethod_var);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t974944492* Mesh_get_normals_m435100974 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_normals_m435100974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t974944492* V_0 = NULL;
	{
		Vector3U5BU5D_t974944492* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429(__this, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t329709361_m2777471429_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector3U5BU5D_t974944492* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern "C"  Vector4U5BU5D_t2564234830* Mesh_get_tangents_m1380468074 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_tangents_m1380468074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4U5BU5D_t2564234830* V_0 = NULL;
	{
		Vector4U5BU5D_t2564234830* L_0 = Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490(__this, 7, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector4_t380635127_m195134490_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector4U5BU5D_t2564234830* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C"  Vector2U5BU5D_t1392199097* Mesh_get_uv_m1614926589 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv_m1614926589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t1392199097* V_0 = NULL;
	{
		Vector2U5BU5D_t1392199097* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393(__this, 3, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t1392199097* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv_m8989046 (Mesh_t1621212487 * __this, Vector2U5BU5D_t1392199097* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_uv_m8989046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2U5BU5D_t1392199097* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259(__this, 3, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisVector2_t3057062568_m2407136259_RuntimeMethod_var);
		return;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C"  Vector2U5BU5D_t1392199097* Mesh_get_uv2_m1108875579 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv2_m1108875579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t1392199097* V_0 = NULL;
	{
		Vector2U5BU5D_t1392199097* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393(__this, 4, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t1392199097* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
extern "C"  Vector2U5BU5D_t1392199097* Mesh_get_uv3_m3194937612 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv3_m3194937612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t1392199097* V_0 = NULL;
	{
		Vector2U5BU5D_t1392199097* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393(__this, 5, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t1392199097* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
extern "C"  Vector2U5BU5D_t1392199097* Mesh_get_uv4_m2850090743 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv4_m2850090743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t1392199097* V_0 = NULL;
	{
		Vector2U5BU5D_t1392199097* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393(__this, 6, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t3057062568_m3479613393_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t1392199097* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern "C"  Color32U5BU5D_t1505762612* Mesh_get_colors32_m1184769428 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_colors32_m1184769428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32U5BU5D_t1505762612* V_0 = NULL;
	{
		Color32U5BU5D_t1505762612* L_0 = Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690(__this, 2, 2, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisColor32_t2788147849_m2195353690_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Color32U5BU5D_t1505762612* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetVertices_m669673273 (Mesh_t1621212487 * __this, List_1_t140159775 * ___inVertices0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetVertices_m669673273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t140159775 * L_0 = ___inVertices0;
		Mesh_SetListForChannel_TisVector3_t329709361_m1783659180(__this, 0, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t329709361_m1783659180_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetNormals_m800993536 (Mesh_t1621212487 * __this, List_1_t140159775 * ___inNormals0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetNormals_m800993536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t140159775 * L_0 = ___inNormals0;
		Mesh_SetListForChannel_TisVector3_t329709361_m1783659180(__this, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t329709361_m1783659180_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetTangents_m2522372090 (Mesh_t1621212487 * __this, List_1_t191085541 * ___inTangents0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTangents_m2522372090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t191085541 * L_0 = ___inTangents0;
		Mesh_SetListForChannel_TisVector4_t380635127_m429250604(__this, 7, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector4_t380635127_m429250604_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C"  void Mesh_SetColors_m2158139618 (Mesh_t1621212487 * __this, List_1_t2598598263 * ___inColors0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetColors_m2158139618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2598598263 * L_0 = ___inColors0;
		Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440(__this, 2, 2, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisColor32_t2788147849_m1896332440_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void Mesh_SetUVs_m926587831 (Mesh_t1621212487 * __this, int32_t ___channel0, List_1_t2867512982 * ___uvs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUVs_m926587831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___channel0;
		List_1_t2867512982 * L_1 = ___uvs1;
		Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797(__this, L_0, 2, L_1, /*hidden argument*/Mesh_SetUvsImpl_TisVector2_t3057062568_m1729498797_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern "C"  void Mesh_PrintErrorCantAccessIndices_m23294446 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_PrintErrorCantAccessIndices_m23294446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Object_get_name_m461114069(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2126999818(NULL /*static, unused*/, _stringLiteral1639121846, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern "C"  bool Mesh_CheckCanAccessSubmesh_m3483679341 (Mesh_t1621212487 * __this, int32_t ___submesh0, bool ___errorAboutTriangles1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_CheckCanAccessSubmesh_m3483679341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	{
		bool L_0 = Mesh_get_canAccess_m284991026(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		Mesh_PrintErrorCantAccessIndices_m23294446(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0061;
	}

IL_001a:
	{
		int32_t L_1 = ___submesh0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		int32_t L_3 = Mesh_get_subMeshCount_m1718999019(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_005a;
		}
	}

IL_002d:
	{
		bool L_4 = ___errorAboutTriangles1;
		G_B5_0 = _stringLiteral71053757;
		if (!L_4)
		{
			G_B6_0 = _stringLiteral71053757;
			goto IL_0043;
		}
	}
	{
		G_B7_0 = _stringLiteral1342767652;
		G_B7_1 = G_B5_0;
		goto IL_0048;
	}

IL_0043:
	{
		G_B7_0 = _stringLiteral2391538392;
		G_B7_1 = G_B6_0;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2126999818(NULL /*static, unused*/, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m1993805588(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0061;
	}

IL_005a:
	{
		V_0 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshTriangles_m3893594630 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m3483679341(__this, L_0, (bool)1, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshIndices_m1492168043 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m3483679341(__this, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m505205204 (Mesh_t1621212487 * __this, Int32U5BU5D_t3565237794* ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m284991026(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Int32U5BU5D_t3565237794* L_1 = ___value0;
		Int32U5BU5D_t3565237794* L_2 = ___value0;
		int32_t L_3 = Mesh_SafeLength_m930608131(__this, (RuntimeArray *)(RuntimeArray *)L_2, /*hidden argument*/NULL);
		Mesh_SetTrianglesImpl_m3619734739(__this, (-1), (RuntimeArray *)(RuntimeArray *)L_1, L_3, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		Mesh_PrintErrorCantAccessIndices_m23294446(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern "C"  Int32U5BU5D_t3565237794* Mesh_GetIndices_m1110104194 (Mesh_t1621212487 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetIndices_m1110104194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3565237794* V_0 = NULL;
	Int32U5BU5D_t3565237794* G_B3_0 = NULL;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmeshIndices_m1492168043(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		Int32U5BU5D_t3565237794* L_3 = Mesh_GetIndicesImpl_m3232595394(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_001f;
	}

IL_0019:
	{
		G_B3_0 = ((Int32U5BU5D_t3565237794*)SZArrayNew(Int32U5BU5D_t3565237794_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_001f:
	{
		V_0 = G_B3_0;
		goto IL_0025;
	}

IL_0025:
	{
		Int32U5BU5D_t3565237794* L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m3302734538 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	{
		Mesh_ClearImpl_m1737913970(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m1630268163 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_RecalculateBounds_m1630268163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Mesh_get_canAccess_m284991026(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Mesh_RecalculateBoundsImpl_m4129815417(__this, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_0017:
	{
		String_t* L_1 = Object_get_name_m461114069(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2126999818(NULL /*static, unused*/, _stringLiteral4263497495, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1089037679 (Mesh_t1621212487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_RecalculateNormals_m1089037679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Mesh_get_canAccess_m284991026(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Mesh_RecalculateNormalsImpl_m3938918416(__this, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_0017:
	{
		String_t* L_1 = Object_get_name_m461114069(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2126999818(NULL /*static, unused*/, _stringLiteral2318156272, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t114115908_il2cpp_TypeInfo_var);
		Debug_LogError_m627890074(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_mesh_m1413123005 (MeshFilter_t1334808991 * __this, Mesh_t1621212487 * ___value0, const RuntimeMethod* method)
{
	typedef void (*MeshFilter_set_mesh_m1413123005_ftn) (MeshFilter_t1334808991 *, Mesh_t1621212487 *);
	static MeshFilter_set_mesh_m1413123005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m1413123005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2458272640 (MonoBehaviour_t3829899482 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m2015992041(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t4212313979 * MonoBehaviour_StartCoroutine_m2028966792 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	Coroutine_t4212313979 * V_0 = NULL;
	{
		RuntimeObject* L_0 = ___routine0;
		Coroutine_t4212313979 * L_1 = MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t4212313979 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t4212313979 * MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	typedef Coroutine_t4212313979 * (*MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963_ftn) (MonoBehaviour_t3829899482 *, RuntimeObject*);
	static MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_Internal_m1665851963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)");
	Coroutine_t4212313979 * retVal = _il2cpp_icall_func(__this, ___routine0);
	return retVal;
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m2404389362 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m3546707004 (MonoBehaviour_t3829899482 * __this, Coroutine_t4212313979 * ___routine0, const RuntimeMethod* method)
{
	{
		Coroutine_t4212313979 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m813737111(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326 (MonoBehaviour_t3829899482 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326_ftn) (MonoBehaviour_t3829899482 *, RuntimeObject*);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1393037326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m813737111 (MonoBehaviour_t3829899482 * __this, Coroutine_t4212313979 * ___routine0, const RuntimeMethod* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m813737111_ftn) (MonoBehaviour_t3829899482 *, Coroutine_t4212313979 *);
	static MonoBehaviour_StopCoroutine_Auto_m813737111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m813737111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
