﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t499004851_0_0_0;
extern const Il2CppType Char_t2157028800_0_0_0;
extern const Il2CppType Int64_t3070791913_0_0_0;
extern const Il2CppType UInt32_t3311932136_0_0_0;
extern const Il2CppType UInt64_t96233451_0_0_0;
extern const Il2CppType Byte_t2815932036_0_0_0;
extern const Il2CppType SByte_t1268219320_0_0_0;
extern const Il2CppType Int16_t508617419_0_0_0;
extern const Il2CppType UInt16_t3440013504_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t435918070_0_0_0;
extern const Il2CppType IComparable_t1276860510_0_0_0;
extern const Il2CppType IEnumerable_t2515817764_0_0_0;
extern const Il2CppType ICloneable_t94813995_0_0_0;
extern const Il2CppType IComparable_1_t3371938004_0_0_0;
extern const Il2CppType IEquatable_1_t1378251015_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t1617407174_0_0_0;
extern const Il2CppType _Type_t271421363_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t3385495939_0_0_0;
extern const Il2CppType _MemberInfo_t3153976712_0_0_0;
extern const Il2CppType Double_t2078998952_0_0_0;
extern const Il2CppType Single_t1863352746_0_0_0;
extern const Il2CppType Decimal_t3984207949_0_0_0;
extern const Il2CppType Boolean_t569405246_0_0_0;
extern const Il2CppType Delegate_t1563516729_0_0_0;
extern const Il2CppType ISerializable_t1949570287_0_0_0;
extern const Il2CppType ParameterInfo_t2372523953_0_0_0;
extern const Il2CppType _ParameterInfo_t3363811512_0_0_0;
extern const Il2CppType ParameterModifier_t1946944212_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t494648311_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t506103983_0_0_0;
extern const Il2CppType MethodBase_t3535115348_0_0_0;
extern const Il2CppType _MethodBase_t2512713776_0_0_0;
extern const Il2CppType ConstructorInfo_t1575920019_0_0_0;
extern const Il2CppType _ConstructorInfo_t598873286_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t550209432_0_0_0;
extern const Il2CppType TailoringInfo_t310506661_0_0_0;
extern const Il2CppType KeyValuePair_2_t4110271513_0_0_0;
extern const Il2CppType Link_t2671721688_0_0_0;
extern const Il2CppType DictionaryEntry_t2422360636_0_0_0;
extern const Il2CppType KeyValuePair_2_t2910963427_0_0_0;
extern const Il2CppType Contraction_t350025005_0_0_0;
extern const Il2CppType Level2Map_t3911096064_0_0_0;
extern const Il2CppType BigInteger_t3167099825_0_0_0;
extern const Il2CppType KeySizes_t652359446_0_0_0;
extern const Il2CppType KeyValuePair_2_t3986364620_0_0_0;
extern const Il2CppType Slot_t2203870133_0_0_0;
extern const Il2CppType Slot_t2314815295_0_0_0;
extern const Il2CppType StackFrame_t795761381_0_0_0;
extern const Il2CppType Calendar_t2104464680_0_0_0;
extern const Il2CppType ModuleBuilder_t353271061_0_0_0;
extern const Il2CppType _ModuleBuilder_t55528917_0_0_0;
extern const Il2CppType Module_t829555369_0_0_0;
extern const Il2CppType _Module_t4246123975_0_0_0;
extern const Il2CppType CustomAttributeBuilder_t2861075622_0_0_0;
extern const Il2CppType _CustomAttributeBuilder_t4202319907_0_0_0;
extern const Il2CppType MonoResource_t1625298336_0_0_0;
extern const Il2CppType MonoWin32Resource_t1452687774_0_0_0;
extern const Il2CppType RefEmitPermissionSet_t3060109815_0_0_0;
extern const Il2CppType ParameterBuilder_t1450031896_0_0_0;
extern const Il2CppType _ParameterBuilder_t1925184101_0_0_0;
extern const Il2CppType TypeU5BU5D_t1460120061_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t1903847002_0_0_0;
extern const Il2CppType IList_t2170747348_0_0_0;
extern const Il2CppType IList_1_t4171725588_0_0_0;
extern const Il2CppType ICollection_1_t1991481984_0_0_0;
extern const Il2CppType IEnumerable_1_t845252796_0_0_0;
extern const Il2CppType IList_1_t975384358_0_0_0;
extern const Il2CppType ICollection_1_t3090108050_0_0_0;
extern const Il2CppType IEnumerable_1_t1943878862_0_0_0;
extern const Il2CppType IList_1_t3924365843_0_0_0;
extern const Il2CppType ICollection_1_t1744122239_0_0_0;
extern const Il2CppType IEnumerable_1_t597893051_0_0_0;
extern const Il2CppType IList_1_t2241863952_0_0_0;
extern const Il2CppType ICollection_1_t61620348_0_0_0;
extern const Il2CppType IEnumerable_1_t3210358456_0_0_0;
extern const Il2CppType IList_1_t2743473123_0_0_0;
extern const Il2CppType ICollection_1_t563229519_0_0_0;
extern const Il2CppType IEnumerable_1_t3711967627_0_0_0;
extern const Il2CppType IList_1_t2511953896_0_0_0;
extern const Il2CppType ICollection_1_t331710292_0_0_0;
extern const Il2CppType IEnumerable_1_t3480448400_0_0_0;
extern const Il2CppType IList_1_t4028042438_0_0_0;
extern const Il2CppType ICollection_1_t1847798834_0_0_0;
extern const Il2CppType IEnumerable_1_t701569646_0_0_0;
extern const Il2CppType LocalBuilder_t2129824983_0_0_0;
extern const Il2CppType _LocalBuilder_t2362914606_0_0_0;
extern const Il2CppType LocalVariableInfo_t4058697059_0_0_0;
extern const Il2CppType ILTokenInfo_t1423368019_0_0_0;
extern const Il2CppType LabelData_t3746295612_0_0_0;
extern const Il2CppType LabelFixup_t362107953_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t1917656400_0_0_0;
extern const Il2CppType TypeBuilder_t4094040294_0_0_0;
extern const Il2CppType _TypeBuilder_t3514614725_0_0_0;
extern const Il2CppType MethodBuilder_t2052015890_0_0_0;
extern const Il2CppType _MethodBuilder_t3265230778_0_0_0;
extern const Il2CppType FieldBuilder_t375034881_0_0_0;
extern const Il2CppType _FieldBuilder_t956530906_0_0_0;
extern const Il2CppType ConstructorBuilder_t2019506662_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1009537191_0_0_0;
extern const Il2CppType PropertyBuilder_t2696548899_0_0_0;
extern const Il2CppType _PropertyBuilder_t131710825_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t4092521575_0_0_0;
extern const Il2CppType EventBuilder_t814990589_0_0_0;
extern const Il2CppType _EventBuilder_t2566635139_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t2525571267_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t3690676406_0_0_0;
extern const Il2CppType CustomAttributeData_t612787047_0_0_0;
extern const Il2CppType ResourceInfo_t3106642170_0_0_0;
extern const Il2CppType ResourceCacheItem_t3011921618_0_0_0;
extern const Il2CppType IContextProperty_t3459958298_0_0_0;
extern const Il2CppType Header_t3846250252_0_0_0;
extern const Il2CppType ITrackingHandler_t1663093004_0_0_0;
extern const Il2CppType IContextAttribute_t3303384032_0_0_0;
extern const Il2CppType DateTime_t972933412_0_0_0;
extern const Il2CppType TimeSpan_t457147580_0_0_0;
extern const Il2CppType TypeTag_t1982705342_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t2371975271_0_0_0;
extern const Il2CppType IBuiltInEvidence_t3244916114_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t574536121_0_0_0;
extern const Il2CppType WaitHandle_t697867384_0_0_0;
extern const Il2CppType IDisposable_t3363289168_0_0_0;
extern const Il2CppType MarshalByRefObject_t532690895_0_0_0;
extern const Il2CppType DateTimeOffset_t4244044333_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t1723234366_0_0_0;
extern const Il2CppType BigInteger_t3167099826_0_0_0;
extern const Il2CppType ByteU5BU5D_t3287329517_0_0_0;
extern const Il2CppType IList_1_t2173909220_0_0_0;
extern const Il2CppType ICollection_1_t4288632912_0_0_0;
extern const Il2CppType IEnumerable_1_t3142403724_0_0_0;
extern const Il2CppType X509Certificate_t934499855_0_0_0;
extern const Il2CppType IDeserializationCallback_t3407183497_0_0_0;
extern const Il2CppType ClientCertificateType_t1976286434_0_0_0;
extern const Il2CppType KeyValuePair_2_t4180671908_0_0_0;
extern const Il2CppType KeyValuePair_2_t2981363822_0_0_0;
extern const Il2CppType X509ChainStatus_t3509631940_0_0_0;
extern const Il2CppType Capture_t3723888528_0_0_0;
extern const Il2CppType Group_t4116754232_0_0_0;
extern const Il2CppType Mark_t3700292706_0_0_0;
extern const Il2CppType UriScheme_t1574639958_0_0_0;
extern const Il2CppType Link_t2401875721_0_0_0;
extern const Il2CppType AsyncOperation_t1227466744_0_0_0;
extern const Il2CppType Camera_t989002943_0_0_0;
extern const Il2CppType Behaviour_t2441856611_0_0_0;
extern const Il2CppType Component_t789413749_0_0_0;
extern const Il2CppType Object_t1970767703_0_0_0;
extern const Il2CppType Display_t882507400_0_0_0;
extern const Il2CppType SphericalHarmonicsL2_t298540216_0_0_0;
extern const Il2CppType Keyframe_t1682423392_0_0_0;
extern const Il2CppType Vector3_t329709361_0_0_0;
extern const Il2CppType Vector4_t380635127_0_0_0;
extern const Il2CppType Vector2_t3057062568_0_0_0;
extern const Il2CppType Color32_t2788147849_0_0_0;
extern const Il2CppType Playable_t348555058_0_0_0;
extern const Il2CppType PlayableOutput_t1141860262_0_0_0;
extern const Il2CppType Scene_t548444562_0_0_0;
extern const Il2CppType LoadSceneMode_t2763863609_0_0_0;
extern const Il2CppType SpriteAtlas_t3138271588_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t1081793821_0_0_0;
extern const Il2CppType Attribute_t3852256153_0_0_0;
extern const Il2CppType _Attribute_t2807569607_0_0_0;
extern const Il2CppType ExecuteInEditMode_t814488931_0_0_0;
extern const Il2CppType RequireComponent_t4152270991_0_0_0;
extern const Il2CppType HitInfo_t214434488_0_0_0;
extern const Il2CppType PersistentCall_t1257714207_0_0_0;
extern const Il2CppType BaseInvokableCall_t3782853021_0_0_0;
extern const Il2CppType WorkRequest_t4243437884_0_0_0;
extern const Il2CppType PlayableBinding_t181640494_0_0_0;
extern const Il2CppType MessageEventArgs_t2086846075_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t3285514618_0_0_0;
extern const Il2CppType WeakReference_t1069295502_0_0_0;
extern const Il2CppType KeyValuePair_2_t420362637_0_0_0;
extern const Il2CppType KeyValuePair_2_t1114560181_0_0_0;
extern const Il2CppType Rigidbody2D_t3506664974_0_0_0;
extern const Il2CppType Font_t801544967_0_0_0;
extern const Il2CppType UIVertex_t4115127231_0_0_0;
extern const Il2CppType UICharInfo_t854420848_0_0_0;
extern const Il2CppType UILineInfo_t285775551_0_0_0;
extern const Il2CppType AnimationClipPlayable_t3277800016_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t3926483291_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t1623951654_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t2049487209_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t2116071640_0_0_0;
extern const Il2CppType AudioSpatializerExtensionDefinition_t1514572524_0_0_0;
extern const Il2CppType AudioAmbisonicExtensionDefinition_t3557186147_0_0_0;
extern const Il2CppType AudioSourceExtension_t2588224584_0_0_0;
extern const Il2CppType ScriptableObject_t2962125979_0_0_0;
extern const Il2CppType AudioMixerPlayable_t1613331552_0_0_0;
extern const Il2CppType AudioClipPlayable_t765244045_0_0_0;
extern const Il2CppType AchievementDescription_t3476369140_0_0_0;
extern const Il2CppType IAchievementDescription_t2434259086_0_0_0;
extern const Il2CppType UserProfile_t3121054929_0_0_0;
extern const Il2CppType IUserProfile_t2190722809_0_0_0;
extern const Il2CppType GcLeaderboard_t2259580636_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t958143867_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t3241716440_0_0_0;
extern const Il2CppType IAchievement_t3151655605_0_0_0;
extern const Il2CppType GcAchievementData_t2938297544_0_0_0;
extern const Il2CppType Achievement_t3727036008_0_0_0;
extern const Il2CppType IScoreU5BU5D_t912747562_0_0_0;
extern const Il2CppType IScore_t3344695883_0_0_0;
extern const Il2CppType GcScoreData_t3653438230_0_0_0;
extern const Il2CppType Score_t2147121913_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t3279866116_0_0_0;
extern const Il2CppType GUILayoutOption_t3076073054_0_0_0;
extern const Il2CppType LayoutCache_t3924992466_0_0_0;
extern const Il2CppType KeyValuePair_2_t1687969195_0_0_0;
extern const Il2CppType KeyValuePair_2_t942896407_0_0_0;
extern const Il2CppType GUILayoutEntry_t2655444992_0_0_0;
extern const Il2CppType Exception_t2123675094_0_0_0;
extern const Il2CppType GUIStyle_t4058570518_0_0_0;
extern const Il2CppType KeyValuePair_2_t2175561798_0_0_0;
extern const Il2CppType Particle_t651781316_0_0_0;
extern const Il2CppType RaycastHit_t2851673566_0_0_0;
extern const Il2CppType ContactPoint_t2838489305_0_0_0;
extern const Il2CppType EventSystem_t1635553419_0_0_0;
extern const Il2CppType UIBehaviour_t2076195789_0_0_0;
extern const Il2CppType MonoBehaviour_t3829899482_0_0_0;
extern const Il2CppType BaseInputModule_t3513845355_0_0_0;
extern const Il2CppType RaycastResult_t463000792_0_0_0;
extern const Il2CppType IDeselectHandler_t1079439781_0_0_0;
extern const Il2CppType IEventSystemHandler_t2072879830_0_0_0;
extern const Il2CppType List_1_t1883330244_0_0_0;
extern const Il2CppType List_1_t185548372_0_0_0;
extern const Il2CppType List_1_t599864163_0_0_0;
extern const Il2CppType ISelectHandler_t287548082_0_0_0;
extern const Il2CppType BaseRaycaster_t2347538179_0_0_0;
extern const Il2CppType Entry_t4245925096_0_0_0;
extern const Il2CppType BaseEventData_t1619969158_0_0_0;
extern const Il2CppType IPointerEnterHandler_t2829503976_0_0_0;
extern const Il2CppType IPointerExitHandler_t2553928545_0_0_0;
extern const Il2CppType IPointerDownHandler_t3783019842_0_0_0;
extern const Il2CppType IPointerUpHandler_t2582372469_0_0_0;
extern const Il2CppType IPointerClickHandler_t3256793649_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t1510685835_0_0_0;
extern const Il2CppType IBeginDragHandler_t253960210_0_0_0;
extern const Il2CppType IDragHandler_t3031146797_0_0_0;
extern const Il2CppType IEndDragHandler_t2378561395_0_0_0;
extern const Il2CppType IDropHandler_t26677246_0_0_0;
extern const Il2CppType IScrollHandler_t2953857311_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t3805117774_0_0_0;
extern const Il2CppType IMoveHandler_t3674718662_0_0_0;
extern const Il2CppType ISubmitHandler_t160027500_0_0_0;
extern const Il2CppType ICancelHandler_t1737091460_0_0_0;
extern const Il2CppType Transform_t532597831_0_0_0;
extern const Il2CppType GameObject_t1318052361_0_0_0;
extern const Il2CppType BaseInput_t2641534846_0_0_0;
extern const Il2CppType PointerEventData_t2787773033_0_0_0;
extern const Il2CppType AbstractEventData_t40423571_0_0_0;
extern const Il2CppType KeyValuePair_2_t4100644270_0_0_0;
extern const Il2CppType ButtonState_t1610769254_0_0_0;
extern const Il2CppType RaycastHit2D_t266689378_0_0_0;
extern const Il2CppType Color_t460381780_0_0_0;
extern const Il2CppType ICanvasElement_t2902062777_0_0_0;
extern const Il2CppType ColorBlock_t266244752_0_0_0;
extern const Il2CppType OptionData_t1608351530_0_0_0;
extern const Il2CppType DropdownItem_t3503496286_0_0_0;
extern const Il2CppType FloatTween_t3352968507_0_0_0;
extern const Il2CppType Sprite_t3012664695_0_0_0;
extern const Il2CppType Canvas_t852644723_0_0_0;
extern const Il2CppType List_1_t663095137_0_0_0;
extern const Il2CppType HashSet_1_t2686032385_0_0_0;
extern const Il2CppType Text_t866332725_0_0_0;
extern const Il2CppType Link_t2893110488_0_0_0;
extern const Il2CppType ILayoutElement_t632884531_0_0_0;
extern const Il2CppType MaskableGraphic_t92248932_0_0_0;
extern const Il2CppType IClippable_t3700685589_0_0_0;
extern const Il2CppType IMaskable_t3175584775_0_0_0;
extern const Il2CppType IMaterialModifier_t2216105711_0_0_0;
extern const Il2CppType Graphic_t1165931317_0_0_0;
extern const Il2CppType KeyValuePair_2_t3484435490_0_0_0;
extern const Il2CppType ColorTween_t1361506897_0_0_0;
extern const Il2CppType IndexedSet_1_t2847635963_0_0_0;
extern const Il2CppType KeyValuePair_2_t3364176000_0_0_0;
extern const Il2CppType KeyValuePair_2_t101362478_0_0_0;
extern const Il2CppType KeyValuePair_2_t3961451802_0_0_0;
extern const Il2CppType Type_t74284121_0_0_0;
extern const Il2CppType FillMethod_t4202801712_0_0_0;
extern const Il2CppType ContentType_t59827793_0_0_0;
extern const Il2CppType LineType_t1398395467_0_0_0;
extern const Il2CppType InputType_t2454186138_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t2115689981_0_0_0;
extern const Il2CppType CharacterValidation_t2898689831_0_0_0;
extern const Il2CppType Mask_t4149322243_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t3610902803_0_0_0;
extern const Il2CppType List_1_t3959772657_0_0_0;
extern const Il2CppType RectMask2D_t1299097536_0_0_0;
extern const Il2CppType IClipper_t4258280777_0_0_0;
extern const Il2CppType List_1_t1109547950_0_0_0;
extern const Il2CppType Navigation_t4246164788_0_0_0;
extern const Il2CppType Link_t1432496056_0_0_0;
extern const Il2CppType Direction_t2752781413_0_0_0;
extern const Il2CppType Selectable_t199987819_0_0_0;
extern const Il2CppType Transition_t3336380680_0_0_0;
extern const Il2CppType SpriteState_t2305809087_0_0_0;
extern const Il2CppType CanvasGroup_t370922105_0_0_0;
extern const Il2CppType Direction_t571314014_0_0_0;
extern const Il2CppType MatEntry_t1253440912_0_0_0;
extern const Il2CppType Toggle_t3640703094_0_0_0;
extern const Il2CppType KeyValuePair_2_t2141035082_0_0_0;
extern const Il2CppType AspectMode_t2023416516_0_0_0;
extern const Il2CppType FitMode_t3728304995_0_0_0;
extern const Il2CppType RectTransform_t15861704_0_0_0;
extern const Il2CppType LayoutRebuilder_t2374704703_0_0_0;
extern const Il2CppType List_1_t140159775_0_0_0;
extern const Il2CppType List_1_t2598598263_0_0_0;
extern const Il2CppType List_1_t2867512982_0_0_0;
extern const Il2CppType List_1_t191085541_0_0_0;
extern const Il2CppType List_1_t309455265_0_0_0;
extern const Il2CppType List_1_t3925577645_0_0_0;
extern const Il2CppType Link_t1967700103_0_0_0;
extern const Il2CppType KeyValuePair_2_t1179652112_0_0_0;
extern const Il2CppType KeyValuePair_2_t4275311322_0_0_0;
extern const Il2CppType ARHitTestResult_t1183581528_0_0_0;
extern const Il2CppType ARHitTestResultType_t3376578133_0_0_0;
extern const Il2CppType ParticleSystem_t1097441456_0_0_0;
extern const Il2CppType ARPlaneAnchorGameObject_t2270868264_0_0_0;
extern const Il2CppType KeyValuePair_2_t387859544_0_0_0;
extern const Il2CppType UnityARSessionRunOption_t2827180039_0_0_0;
extern const Il2CppType UnityARAlignment_t2394317114_0_0_0;
extern const Il2CppType UnityARPlaneDetection_t4236545235_0_0_0;
extern const Il2CppType Light_t3883945566_0_0_0;
extern const Il2CppType IEnumerable_1_t536933947_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m734803740_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m787042007_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m787042007_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3596954745_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2095751586_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2095751586_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m925910360_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2097743790_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2097743790_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3972326822_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2038482742_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2038482742_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m4074792876_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1131242284_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m267516494_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m267516494_gp_1_0_0_0;
extern const Il2CppType Array_compare_m2330141893_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m1179209724_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m2794471509_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m2758149500_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m2910560811_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m3816593202_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m3816593202_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m4047168095_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2722629763_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1417763420_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3608062671_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3859049537_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1091091128_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3933600019_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m915595758_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3668675172_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3050703072_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m1079732850_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m3254707976_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m43705925_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m3752110915_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1647032180_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1323951861_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m1602332273_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m1616247547_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m443996534_gp_0_0_0_0;
extern const Il2CppType Array_Find_m2550374671_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m2838405935_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t4134619510_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0;
extern const Il2CppType IList_1_t555026515_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t1055842719_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t3261863354_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t4288707744_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t4214145415_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t1346033315_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t4241968728_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t4241968728_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t228248057_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3171201328_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3171201328_gp_1_0_0_0;
extern const Il2CppType Enumerator_t3672918140_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3672918140_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1695844585_0_0_0;
extern const Il2CppType ValueCollection_t2778072946_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2778072946_gp_1_0_0_0;
extern const Il2CppType Enumerator_t936271527_gp_0_0_0_0;
extern const Il2CppType Enumerator_t936271527_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t2843542286_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t4044621341_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2574266403_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t567457625_0_0_0;
extern const Il2CppType IDictionary_2_t3270403520_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3270403520_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t694182271_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t694182271_gp_1_0_0_0;
extern const Il2CppType List_1_t1825915537_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1634006062_gp_0_0_0_0;
extern const Il2CppType Collection_1_t2715885535_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t2737108554_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0;
extern const Il2CppType Queue_1_t1129464372_gp_0_0_0_0;
extern const Il2CppType Enumerator_t63438097_gp_0_0_0_0;
extern const Il2CppType Stack_1_t2792833190_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4232829344_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t1434642847_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3119472212_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t3200753757_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m2805250219_gp_0_0_0_0;
extern const Il2CppType Enumerable_Single_m1169226985_gp_0_0_0_0;
extern const Il2CppType Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToList_m2871797791_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m2312884896_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m1455327788_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m327293654_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m2020898487_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m4081353059_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m3335520185_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m3381760768_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m370193599_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m613625000_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0;
extern const Il2CppType Object_FindObjectsOfType_m895778613_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t2384605844_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t880801182_0_0_0;
extern const Il2CppType InvokableCall_2_t686556727_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t686556727_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t3720805410_0_0_0;
extern const Il2CppType InvokableCall_3_t1114138605_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1114138605_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1114138605_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t2206700587_0_0_0;
extern const Il2CppType InvokableCall_4_t3841286312_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3841286312_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3841286312_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3841286312_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t269288053_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t1144529152_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4235607108_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4235607108_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1018889849_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1018889849_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1018889849_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t963784060_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t963784060_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t963784060_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t963784060_gp_3_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m871388233_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t2880223937_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t1694099753_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t1470686486_gp_0_0_0_0;
extern const Il2CppType List_1_t1928769349_0_0_0;
extern const Il2CppType ObjectPool_1_t2798048145_gp_0_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t2771548650_0_0_0;
extern const Il2CppType PlayerConnection_t781534341_0_0_0;
extern const Il2CppType GUILayer_t2694382279_0_0_0;
extern const Il2CppType AxisEventData_t2438176915_0_0_0;
extern const Il2CppType SpriteRenderer_t3043448750_0_0_0;
extern const Il2CppType GraphicRaycaster_t3324891182_0_0_0;
extern const Il2CppType Image_t61690498_0_0_0;
extern const Il2CppType Button_t3188123007_0_0_0;
extern const Il2CppType Dropdown_t3370942616_0_0_0;
extern const Il2CppType CanvasRenderer_t3206453365_0_0_0;
extern const Il2CppType Corner_t3476260122_0_0_0;
extern const Il2CppType Axis_t3803614164_0_0_0;
extern const Il2CppType Constraint_t920180522_0_0_0;
extern const Il2CppType SubmitEvent_t1747690399_0_0_0;
extern const Il2CppType OnChangeEvent_t2451064907_0_0_0;
extern const Il2CppType OnValidateInput_t1213950634_0_0_0;
extern const Il2CppType LayoutElement_t2105357831_0_0_0;
extern const Il2CppType RectOffset_t83369214_0_0_0;
extern const Il2CppType TextAnchor_t3885169356_0_0_0;
extern const Il2CppType AnimationTriggers_t3813953280_0_0_0;
extern const Il2CppType Animator_t3267699442_0_0_0;
extern const Il2CppType UnityARVideo_t2486736449_0_0_0;
extern const Il2CppType MeshRenderer_t2414658676_0_0_0;
extern const Il2CppType Slider_t3224615444_0_0_0;
extern const Il2CppType RawImage_t4037952033_0_0_0;
extern const Il2CppType InputField_t4206798601_0_0_0;
extern const Il2CppType BoxSlider_t1119607061_0_0_0;
extern const Il2CppType UnityARUserAnchorComponent_t2848590843_0_0_0;
extern const Il2CppType serializableFromEditorMessage_t1383667999_0_0_0;
extern const Il2CppType DontDestroyOnLoad_t2332563878_0_0_0;
extern const Il2CppType MeshFilter_t1334808991_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_Types[] = { (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0 = { 1, GenInst_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_Char_t2157028800_0_0_0_Types[] = { (&Char_t2157028800_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t2157028800_0_0_0 = { 1, GenInst_Char_t2157028800_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t3070791913_0_0_0_Types[] = { (&Int64_t3070791913_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t3070791913_0_0_0 = { 1, GenInst_Int64_t3070791913_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t3311932136_0_0_0_Types[] = { (&UInt32_t3311932136_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t3311932136_0_0_0 = { 1, GenInst_UInt32_t3311932136_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t96233451_0_0_0_Types[] = { (&UInt64_t96233451_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t96233451_0_0_0 = { 1, GenInst_UInt64_t96233451_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t2815932036_0_0_0_Types[] = { (&Byte_t2815932036_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t2815932036_0_0_0 = { 1, GenInst_Byte_t2815932036_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t1268219320_0_0_0_Types[] = { (&SByte_t1268219320_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t1268219320_0_0_0 = { 1, GenInst_SByte_t1268219320_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t508617419_0_0_0_Types[] = { (&Int16_t508617419_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t508617419_0_0_0 = { 1, GenInst_Int16_t508617419_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t3440013504_0_0_0_Types[] = { (&UInt16_t3440013504_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t3440013504_0_0_0 = { 1, GenInst_UInt16_t3440013504_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t435918070_0_0_0_Types[] = { (&IConvertible_t435918070_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t435918070_0_0_0 = { 1, GenInst_IConvertible_t435918070_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t1276860510_0_0_0_Types[] = { (&IComparable_t1276860510_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t1276860510_0_0_0 = { 1, GenInst_IComparable_t1276860510_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t2515817764_0_0_0_Types[] = { (&IEnumerable_t2515817764_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t2515817764_0_0_0 = { 1, GenInst_IEnumerable_t2515817764_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t94813995_0_0_0_Types[] = { (&ICloneable_t94813995_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t94813995_0_0_0 = { 1, GenInst_ICloneable_t94813995_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t3371938004_0_0_0_Types[] = { (&IComparable_1_t3371938004_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t3371938004_0_0_0 = { 1, GenInst_IComparable_1_t3371938004_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t1378251015_0_0_0_Types[] = { (&IEquatable_1_t1378251015_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1378251015_0_0_0 = { 1, GenInst_IEquatable_1_t1378251015_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t1617407174_0_0_0_Types[] = { (&IReflect_t1617407174_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t1617407174_0_0_0 = { 1, GenInst_IReflect_t1617407174_0_0_0_Types };
static const RuntimeType* GenInst__Type_t271421363_0_0_0_Types[] = { (&_Type_t271421363_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t271421363_0_0_0 = { 1, GenInst__Type_t271421363_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t3385495939_0_0_0_Types[] = { (&ICustomAttributeProvider_t3385495939_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t3385495939_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t3385495939_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t3153976712_0_0_0_Types[] = { (&_MemberInfo_t3153976712_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t3153976712_0_0_0 = { 1, GenInst__MemberInfo_t3153976712_0_0_0_Types };
static const RuntimeType* GenInst_Double_t2078998952_0_0_0_Types[] = { (&Double_t2078998952_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t2078998952_0_0_0 = { 1, GenInst_Double_t2078998952_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1863352746_0_0_0_Types[] = { (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1863352746_0_0_0 = { 1, GenInst_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t3984207949_0_0_0_Types[] = { (&Decimal_t3984207949_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t3984207949_0_0_0 = { 1, GenInst_Decimal_t3984207949_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t569405246_0_0_0_Types[] = { (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t569405246_0_0_0 = { 1, GenInst_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t1563516729_0_0_0_Types[] = { (&Delegate_t1563516729_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t1563516729_0_0_0 = { 1, GenInst_Delegate_t1563516729_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1949570287_0_0_0_Types[] = { (&ISerializable_t1949570287_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1949570287_0_0_0 = { 1, GenInst_ISerializable_t1949570287_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t2372523953_0_0_0_Types[] = { (&ParameterInfo_t2372523953_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2372523953_0_0_0 = { 1, GenInst_ParameterInfo_t2372523953_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t3363811512_0_0_0_Types[] = { (&_ParameterInfo_t3363811512_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3363811512_0_0_0 = { 1, GenInst__ParameterInfo_t3363811512_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t1946944212_0_0_0_Types[] = { (&ParameterModifier_t1946944212_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1946944212_0_0_0 = { 1, GenInst_ParameterModifier_t1946944212_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t494648311_0_0_0_Types[] = { (&_FieldInfo_t494648311_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t494648311_0_0_0 = { 1, GenInst__FieldInfo_t494648311_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t506103983_0_0_0_Types[] = { (&_MethodInfo_t506103983_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t506103983_0_0_0 = { 1, GenInst__MethodInfo_t506103983_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t3535115348_0_0_0_Types[] = { (&MethodBase_t3535115348_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t3535115348_0_0_0 = { 1, GenInst_MethodBase_t3535115348_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t2512713776_0_0_0_Types[] = { (&_MethodBase_t2512713776_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t2512713776_0_0_0 = { 1, GenInst__MethodBase_t2512713776_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t1575920019_0_0_0_Types[] = { (&ConstructorInfo_t1575920019_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1575920019_0_0_0 = { 1, GenInst_ConstructorInfo_t1575920019_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t598873286_0_0_0_Types[] = { (&_ConstructorInfo_t598873286_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t598873286_0_0_0 = { 1, GenInst__ConstructorInfo_t598873286_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t550209432_0_0_0_Types[] = { (&TableRange_t550209432_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t550209432_0_0_0 = { 1, GenInst_TableRange_t550209432_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t310506661_0_0_0_Types[] = { (&TailoringInfo_t310506661_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t310506661_0_0_0 = { 1, GenInst_TailoringInfo_t310506661_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4110271513_0_0_0_Types[] = { (&KeyValuePair_2_t4110271513_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4110271513_0_0_0 = { 1, GenInst_KeyValuePair_2_t4110271513_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2671721688_0_0_0_Types[] = { (&Link_t2671721688_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2671721688_0_0_0 = { 1, GenInst_Link_t2671721688_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t499004851_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t499004851_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2422360636_0_0_0 = { 1, GenInst_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t4110271513_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t499004851_0_0_0), (&KeyValuePair_2_t4110271513_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t4110271513_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t4110271513_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t499004851_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2910963427_0_0_0_Types[] = { (&KeyValuePair_2_t2910963427_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2910963427_0_0_0 = { 1, GenInst_KeyValuePair_2_t2910963427_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2910963427_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t499004851_0_0_0), (&KeyValuePair_2_t2910963427_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2910963427_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2910963427_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t350025005_0_0_0_Types[] = { (&Contraction_t350025005_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t350025005_0_0_0 = { 1, GenInst_Contraction_t350025005_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t3911096064_0_0_0_Types[] = { (&Level2Map_t3911096064_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t3911096064_0_0_0 = { 1, GenInst_Level2Map_t3911096064_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t3167099825_0_0_0_Types[] = { (&BigInteger_t3167099825_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t3167099825_0_0_0 = { 1, GenInst_BigInteger_t3167099825_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t652359446_0_0_0_Types[] = { (&KeySizes_t652359446_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t652359446_0_0_0 = { 1, GenInst_KeySizes_t652359446_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3986364620_0_0_0_Types[] = { (&KeyValuePair_2_t3986364620_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3986364620_0_0_0 = { 1, GenInst_KeyValuePair_2_t3986364620_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3986364620_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3986364620_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3986364620_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3986364620_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2203870133_0_0_0_Types[] = { (&Slot_t2203870133_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2203870133_0_0_0 = { 1, GenInst_Slot_t2203870133_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2314815295_0_0_0_Types[] = { (&Slot_t2314815295_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2314815295_0_0_0 = { 1, GenInst_Slot_t2314815295_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t795761381_0_0_0_Types[] = { (&StackFrame_t795761381_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t795761381_0_0_0 = { 1, GenInst_StackFrame_t795761381_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t2104464680_0_0_0_Types[] = { (&Calendar_t2104464680_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t2104464680_0_0_0 = { 1, GenInst_Calendar_t2104464680_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t353271061_0_0_0_Types[] = { (&ModuleBuilder_t353271061_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t353271061_0_0_0 = { 1, GenInst_ModuleBuilder_t353271061_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t55528917_0_0_0_Types[] = { (&_ModuleBuilder_t55528917_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t55528917_0_0_0 = { 1, GenInst__ModuleBuilder_t55528917_0_0_0_Types };
static const RuntimeType* GenInst_Module_t829555369_0_0_0_Types[] = { (&Module_t829555369_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t829555369_0_0_0 = { 1, GenInst_Module_t829555369_0_0_0_Types };
static const RuntimeType* GenInst__Module_t4246123975_0_0_0_Types[] = { (&_Module_t4246123975_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t4246123975_0_0_0 = { 1, GenInst__Module_t4246123975_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeBuilder_t2861075622_0_0_0_Types[] = { (&CustomAttributeBuilder_t2861075622_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t2861075622_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t2861075622_0_0_0_Types };
static const RuntimeType* GenInst__CustomAttributeBuilder_t4202319907_0_0_0_Types[] = { (&_CustomAttributeBuilder_t4202319907_0_0_0) };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t4202319907_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t4202319907_0_0_0_Types };
static const RuntimeType* GenInst_MonoResource_t1625298336_0_0_0_Types[] = { (&MonoResource_t1625298336_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoResource_t1625298336_0_0_0 = { 1, GenInst_MonoResource_t1625298336_0_0_0_Types };
static const RuntimeType* GenInst_MonoWin32Resource_t1452687774_0_0_0_Types[] = { (&MonoWin32Resource_t1452687774_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoWin32Resource_t1452687774_0_0_0 = { 1, GenInst_MonoWin32Resource_t1452687774_0_0_0_Types };
static const RuntimeType* GenInst_RefEmitPermissionSet_t3060109815_0_0_0_Types[] = { (&RefEmitPermissionSet_t3060109815_0_0_0) };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t3060109815_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t3060109815_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t1450031896_0_0_0_Types[] = { (&ParameterBuilder_t1450031896_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1450031896_0_0_0 = { 1, GenInst_ParameterBuilder_t1450031896_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t1925184101_0_0_0_Types[] = { (&_ParameterBuilder_t1925184101_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t1925184101_0_0_0 = { 1, GenInst__ParameterBuilder_t1925184101_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t1460120061_0_0_0_Types[] = { (&TypeU5BU5D_t1460120061_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1460120061_0_0_0 = { 1, GenInst_TypeU5BU5D_t1460120061_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t1903847002_0_0_0_Types[] = { (&ICollection_t1903847002_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t1903847002_0_0_0 = { 1, GenInst_ICollection_t1903847002_0_0_0_Types };
static const RuntimeType* GenInst_IList_t2170747348_0_0_0_Types[] = { (&IList_t2170747348_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t2170747348_0_0_0 = { 1, GenInst_IList_t2170747348_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4171725588_0_0_0_Types[] = { (&IList_1_t4171725588_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4171725588_0_0_0 = { 1, GenInst_IList_1_t4171725588_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1991481984_0_0_0_Types[] = { (&ICollection_1_t1991481984_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1991481984_0_0_0 = { 1, GenInst_ICollection_1_t1991481984_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t845252796_0_0_0_Types[] = { (&IEnumerable_1_t845252796_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t845252796_0_0_0 = { 1, GenInst_IEnumerable_1_t845252796_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t975384358_0_0_0_Types[] = { (&IList_1_t975384358_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t975384358_0_0_0 = { 1, GenInst_IList_1_t975384358_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3090108050_0_0_0_Types[] = { (&ICollection_1_t3090108050_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3090108050_0_0_0 = { 1, GenInst_ICollection_1_t3090108050_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1943878862_0_0_0_Types[] = { (&IEnumerable_1_t1943878862_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1943878862_0_0_0 = { 1, GenInst_IEnumerable_1_t1943878862_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3924365843_0_0_0_Types[] = { (&IList_1_t3924365843_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3924365843_0_0_0 = { 1, GenInst_IList_1_t3924365843_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1744122239_0_0_0_Types[] = { (&ICollection_1_t1744122239_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1744122239_0_0_0 = { 1, GenInst_ICollection_1_t1744122239_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t597893051_0_0_0_Types[] = { (&IEnumerable_1_t597893051_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t597893051_0_0_0 = { 1, GenInst_IEnumerable_1_t597893051_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2241863952_0_0_0_Types[] = { (&IList_1_t2241863952_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2241863952_0_0_0 = { 1, GenInst_IList_1_t2241863952_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t61620348_0_0_0_Types[] = { (&ICollection_1_t61620348_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t61620348_0_0_0 = { 1, GenInst_ICollection_1_t61620348_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3210358456_0_0_0_Types[] = { (&IEnumerable_1_t3210358456_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3210358456_0_0_0 = { 1, GenInst_IEnumerable_1_t3210358456_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2743473123_0_0_0_Types[] = { (&IList_1_t2743473123_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2743473123_0_0_0 = { 1, GenInst_IList_1_t2743473123_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t563229519_0_0_0_Types[] = { (&ICollection_1_t563229519_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t563229519_0_0_0 = { 1, GenInst_ICollection_1_t563229519_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3711967627_0_0_0_Types[] = { (&IEnumerable_1_t3711967627_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3711967627_0_0_0 = { 1, GenInst_IEnumerable_1_t3711967627_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2511953896_0_0_0_Types[] = { (&IList_1_t2511953896_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2511953896_0_0_0 = { 1, GenInst_IList_1_t2511953896_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t331710292_0_0_0_Types[] = { (&ICollection_1_t331710292_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t331710292_0_0_0 = { 1, GenInst_ICollection_1_t331710292_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3480448400_0_0_0_Types[] = { (&IEnumerable_1_t3480448400_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3480448400_0_0_0 = { 1, GenInst_IEnumerable_1_t3480448400_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4028042438_0_0_0_Types[] = { (&IList_1_t4028042438_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4028042438_0_0_0 = { 1, GenInst_IList_1_t4028042438_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1847798834_0_0_0_Types[] = { (&ICollection_1_t1847798834_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1847798834_0_0_0 = { 1, GenInst_ICollection_1_t1847798834_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t701569646_0_0_0_Types[] = { (&IEnumerable_1_t701569646_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t701569646_0_0_0 = { 1, GenInst_IEnumerable_1_t701569646_0_0_0_Types };
static const RuntimeType* GenInst_LocalBuilder_t2129824983_0_0_0_Types[] = { (&LocalBuilder_t2129824983_0_0_0) };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2129824983_0_0_0 = { 1, GenInst_LocalBuilder_t2129824983_0_0_0_Types };
static const RuntimeType* GenInst__LocalBuilder_t2362914606_0_0_0_Types[] = { (&_LocalBuilder_t2362914606_0_0_0) };
extern const Il2CppGenericInst GenInst__LocalBuilder_t2362914606_0_0_0 = { 1, GenInst__LocalBuilder_t2362914606_0_0_0_Types };
static const RuntimeType* GenInst_LocalVariableInfo_t4058697059_0_0_0_Types[] = { (&LocalVariableInfo_t4058697059_0_0_0) };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t4058697059_0_0_0 = { 1, GenInst_LocalVariableInfo_t4058697059_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t1423368019_0_0_0_Types[] = { (&ILTokenInfo_t1423368019_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1423368019_0_0_0 = { 1, GenInst_ILTokenInfo_t1423368019_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t3746295612_0_0_0_Types[] = { (&LabelData_t3746295612_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t3746295612_0_0_0 = { 1, GenInst_LabelData_t3746295612_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t362107953_0_0_0_Types[] = { (&LabelFixup_t362107953_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t362107953_0_0_0 = { 1, GenInst_LabelFixup_t362107953_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t1917656400_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t1917656400_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1917656400_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1917656400_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t4094040294_0_0_0_Types[] = { (&TypeBuilder_t4094040294_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t4094040294_0_0_0 = { 1, GenInst_TypeBuilder_t4094040294_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t3514614725_0_0_0_Types[] = { (&_TypeBuilder_t3514614725_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3514614725_0_0_0 = { 1, GenInst__TypeBuilder_t3514614725_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t2052015890_0_0_0_Types[] = { (&MethodBuilder_t2052015890_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t2052015890_0_0_0 = { 1, GenInst_MethodBuilder_t2052015890_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3265230778_0_0_0_Types[] = { (&_MethodBuilder_t3265230778_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3265230778_0_0_0 = { 1, GenInst__MethodBuilder_t3265230778_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t375034881_0_0_0_Types[] = { (&FieldBuilder_t375034881_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t375034881_0_0_0 = { 1, GenInst_FieldBuilder_t375034881_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t956530906_0_0_0_Types[] = { (&_FieldBuilder_t956530906_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t956530906_0_0_0 = { 1, GenInst__FieldBuilder_t956530906_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t2019506662_0_0_0_Types[] = { (&ConstructorBuilder_t2019506662_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2019506662_0_0_0 = { 1, GenInst_ConstructorBuilder_t2019506662_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t1009537191_0_0_0_Types[] = { (&_ConstructorBuilder_t1009537191_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1009537191_0_0_0 = { 1, GenInst__ConstructorBuilder_t1009537191_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t2696548899_0_0_0_Types[] = { (&PropertyBuilder_t2696548899_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t2696548899_0_0_0 = { 1, GenInst_PropertyBuilder_t2696548899_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t131710825_0_0_0_Types[] = { (&_PropertyBuilder_t131710825_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t131710825_0_0_0 = { 1, GenInst__PropertyBuilder_t131710825_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t4092521575_0_0_0_Types[] = { (&_PropertyInfo_t4092521575_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t4092521575_0_0_0 = { 1, GenInst__PropertyInfo_t4092521575_0_0_0_Types };
static const RuntimeType* GenInst_EventBuilder_t814990589_0_0_0_Types[] = { (&EventBuilder_t814990589_0_0_0) };
extern const Il2CppGenericInst GenInst_EventBuilder_t814990589_0_0_0 = { 1, GenInst_EventBuilder_t814990589_0_0_0_Types };
static const RuntimeType* GenInst__EventBuilder_t2566635139_0_0_0_Types[] = { (&_EventBuilder_t2566635139_0_0_0) };
extern const Il2CppGenericInst GenInst__EventBuilder_t2566635139_0_0_0 = { 1, GenInst__EventBuilder_t2566635139_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t2525571267_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t3690676406_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t612787047_0_0_0_Types[] = { (&CustomAttributeData_t612787047_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t612787047_0_0_0 = { 1, GenInst_CustomAttributeData_t612787047_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3106642170_0_0_0_Types[] = { (&ResourceInfo_t3106642170_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3106642170_0_0_0 = { 1, GenInst_ResourceInfo_t3106642170_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t3011921618_0_0_0_Types[] = { (&ResourceCacheItem_t3011921618_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t3011921618_0_0_0 = { 1, GenInst_ResourceCacheItem_t3011921618_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t3459958298_0_0_0_Types[] = { (&IContextProperty_t3459958298_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t3459958298_0_0_0 = { 1, GenInst_IContextProperty_t3459958298_0_0_0_Types };
static const RuntimeType* GenInst_Header_t3846250252_0_0_0_Types[] = { (&Header_t3846250252_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t3846250252_0_0_0 = { 1, GenInst_Header_t3846250252_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t1663093004_0_0_0_Types[] = { (&ITrackingHandler_t1663093004_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1663093004_0_0_0 = { 1, GenInst_ITrackingHandler_t1663093004_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t3303384032_0_0_0_Types[] = { (&IContextAttribute_t3303384032_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t3303384032_0_0_0 = { 1, GenInst_IContextAttribute_t3303384032_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t972933412_0_0_0_Types[] = { (&DateTime_t972933412_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t972933412_0_0_0 = { 1, GenInst_DateTime_t972933412_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t457147580_0_0_0_Types[] = { (&TimeSpan_t457147580_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t457147580_0_0_0 = { 1, GenInst_TimeSpan_t457147580_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t1982705342_0_0_0_Types[] = { (&TypeTag_t1982705342_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t1982705342_0_0_0 = { 1, GenInst_TypeTag_t1982705342_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t2371975271_0_0_0_Types[] = { (&StrongName_t2371975271_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t2371975271_0_0_0 = { 1, GenInst_StrongName_t2371975271_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t3244916114_0_0_0_Types[] = { (&IBuiltInEvidence_t3244916114_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t3244916114_0_0_0 = { 1, GenInst_IBuiltInEvidence_t3244916114_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t574536121_0_0_0_Types[] = { (&IIdentityPermissionFactory_t574536121_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t574536121_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t574536121_0_0_0_Types };
static const RuntimeType* GenInst_WaitHandle_t697867384_0_0_0_Types[] = { (&WaitHandle_t697867384_0_0_0) };
extern const Il2CppGenericInst GenInst_WaitHandle_t697867384_0_0_0 = { 1, GenInst_WaitHandle_t697867384_0_0_0_Types };
static const RuntimeType* GenInst_IDisposable_t3363289168_0_0_0_Types[] = { (&IDisposable_t3363289168_0_0_0) };
extern const Il2CppGenericInst GenInst_IDisposable_t3363289168_0_0_0 = { 1, GenInst_IDisposable_t3363289168_0_0_0_Types };
static const RuntimeType* GenInst_MarshalByRefObject_t532690895_0_0_0_Types[] = { (&MarshalByRefObject_t532690895_0_0_0) };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t532690895_0_0_0 = { 1, GenInst_MarshalByRefObject_t532690895_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t4244044333_0_0_0_Types[] = { (&DateTimeOffset_t4244044333_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t4244044333_0_0_0 = { 1, GenInst_DateTimeOffset_t4244044333_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t1723234366_0_0_0_Types[] = { (&Version_t1723234366_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t1723234366_0_0_0 = { 1, GenInst_Version_t1723234366_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t3167099826_0_0_0_Types[] = { (&BigInteger_t3167099826_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t3167099826_0_0_0 = { 1, GenInst_BigInteger_t3167099826_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3287329517_0_0_0_Types[] = { (&ByteU5BU5D_t3287329517_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3287329517_0_0_0 = { 1, GenInst_ByteU5BU5D_t3287329517_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2173909220_0_0_0_Types[] = { (&IList_1_t2173909220_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2173909220_0_0_0 = { 1, GenInst_IList_1_t2173909220_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t4288632912_0_0_0_Types[] = { (&ICollection_1_t4288632912_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t4288632912_0_0_0 = { 1, GenInst_ICollection_1_t4288632912_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3142403724_0_0_0_Types[] = { (&IEnumerable_1_t3142403724_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3142403724_0_0_0 = { 1, GenInst_IEnumerable_1_t3142403724_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t934499855_0_0_0_Types[] = { (&X509Certificate_t934499855_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t934499855_0_0_0 = { 1, GenInst_X509Certificate_t934499855_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t3407183497_0_0_0_Types[] = { (&IDeserializationCallback_t3407183497_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t3407183497_0_0_0 = { 1, GenInst_IDeserializationCallback_t3407183497_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t1976286434_0_0_0_Types[] = { (&ClientCertificateType_t1976286434_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1976286434_0_0_0 = { 1, GenInst_ClientCertificateType_t1976286434_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4180671908_0_0_0_Types[] = { (&KeyValuePair_2_t4180671908_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180671908_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180671908_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t569405246_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t569405246_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t4180671908_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t569405246_0_0_0), (&KeyValuePair_2_t4180671908_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t4180671908_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t4180671908_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t569405246_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2981363822_0_0_0_Types[] = { (&KeyValuePair_2_t2981363822_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2981363822_0_0_0 = { 1, GenInst_KeyValuePair_2_t2981363822_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t2981363822_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t569405246_0_0_0), (&KeyValuePair_2_t2981363822_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t2981363822_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t2981363822_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t3509631940_0_0_0_Types[] = { (&X509ChainStatus_t3509631940_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t3509631940_0_0_0 = { 1, GenInst_X509ChainStatus_t3509631940_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t3723888528_0_0_0_Types[] = { (&Capture_t3723888528_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t3723888528_0_0_0 = { 1, GenInst_Capture_t3723888528_0_0_0_Types };
static const RuntimeType* GenInst_Group_t4116754232_0_0_0_Types[] = { (&Group_t4116754232_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t4116754232_0_0_0 = { 1, GenInst_Group_t4116754232_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t3700292706_0_0_0_Types[] = { (&Mark_t3700292706_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t3700292706_0_0_0 = { 1, GenInst_Mark_t3700292706_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t1574639958_0_0_0_Types[] = { (&UriScheme_t1574639958_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t1574639958_0_0_0 = { 1, GenInst_UriScheme_t1574639958_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2401875721_0_0_0_Types[] = { (&Link_t2401875721_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2401875721_0_0_0 = { 1, GenInst_Link_t2401875721_0_0_0_Types };
static const RuntimeType* GenInst_AsyncOperation_t1227466744_0_0_0_Types[] = { (&AsyncOperation_t1227466744_0_0_0) };
extern const Il2CppGenericInst GenInst_AsyncOperation_t1227466744_0_0_0 = { 1, GenInst_AsyncOperation_t1227466744_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t989002943_0_0_0_Types[] = { (&Camera_t989002943_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t989002943_0_0_0 = { 1, GenInst_Camera_t989002943_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t2441856611_0_0_0_Types[] = { (&Behaviour_t2441856611_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t2441856611_0_0_0 = { 1, GenInst_Behaviour_t2441856611_0_0_0_Types };
static const RuntimeType* GenInst_Component_t789413749_0_0_0_Types[] = { (&Component_t789413749_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t789413749_0_0_0 = { 1, GenInst_Component_t789413749_0_0_0_Types };
static const RuntimeType* GenInst_Object_t1970767703_0_0_0_Types[] = { (&Object_t1970767703_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t1970767703_0_0_0 = { 1, GenInst_Object_t1970767703_0_0_0_Types };
static const RuntimeType* GenInst_Display_t882507400_0_0_0_Types[] = { (&Display_t882507400_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t882507400_0_0_0 = { 1, GenInst_Display_t882507400_0_0_0_Types };
static const RuntimeType* GenInst_SphericalHarmonicsL2_t298540216_0_0_0_Types[] = { (&SphericalHarmonicsL2_t298540216_0_0_0) };
extern const Il2CppGenericInst GenInst_SphericalHarmonicsL2_t298540216_0_0_0 = { 1, GenInst_SphericalHarmonicsL2_t298540216_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1682423392_0_0_0_Types[] = { (&Keyframe_t1682423392_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1682423392_0_0_0 = { 1, GenInst_Keyframe_t1682423392_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t329709361_0_0_0_Types[] = { (&Vector3_t329709361_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t329709361_0_0_0 = { 1, GenInst_Vector3_t329709361_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t380635127_0_0_0_Types[] = { (&Vector4_t380635127_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t380635127_0_0_0 = { 1, GenInst_Vector4_t380635127_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t3057062568_0_0_0_Types[] = { (&Vector2_t3057062568_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t3057062568_0_0_0 = { 1, GenInst_Vector2_t3057062568_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2788147849_0_0_0_Types[] = { (&Color32_t2788147849_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2788147849_0_0_0 = { 1, GenInst_Color32_t2788147849_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t348555058_0_0_0_Types[] = { (&Playable_t348555058_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t348555058_0_0_0 = { 1, GenInst_Playable_t348555058_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t1141860262_0_0_0_Types[] = { (&PlayableOutput_t1141860262_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t1141860262_0_0_0 = { 1, GenInst_PlayableOutput_t1141860262_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t548444562_0_0_0_LoadSceneMode_t2763863609_0_0_0_Types[] = { (&Scene_t548444562_0_0_0), (&LoadSceneMode_t2763863609_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t548444562_0_0_0_LoadSceneMode_t2763863609_0_0_0 = { 2, GenInst_Scene_t548444562_0_0_0_LoadSceneMode_t2763863609_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t548444562_0_0_0_Types[] = { (&Scene_t548444562_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t548444562_0_0_0 = { 1, GenInst_Scene_t548444562_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t548444562_0_0_0_Scene_t548444562_0_0_0_Types[] = { (&Scene_t548444562_0_0_0), (&Scene_t548444562_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t548444562_0_0_0_Scene_t548444562_0_0_0 = { 2, GenInst_Scene_t548444562_0_0_0_Scene_t548444562_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t3138271588_0_0_0_Types[] = { (&SpriteAtlas_t3138271588_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t3138271588_0_0_0 = { 1, GenInst_SpriteAtlas_t3138271588_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t1081793821_0_0_0_Types[] = { (&DisallowMultipleComponent_t1081793821_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t1081793821_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t1081793821_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t3852256153_0_0_0_Types[] = { (&Attribute_t3852256153_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t3852256153_0_0_0 = { 1, GenInst_Attribute_t3852256153_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t2807569607_0_0_0_Types[] = { (&_Attribute_t2807569607_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t2807569607_0_0_0 = { 1, GenInst__Attribute_t2807569607_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t814488931_0_0_0_Types[] = { (&ExecuteInEditMode_t814488931_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t814488931_0_0_0 = { 1, GenInst_ExecuteInEditMode_t814488931_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t4152270991_0_0_0_Types[] = { (&RequireComponent_t4152270991_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t4152270991_0_0_0 = { 1, GenInst_RequireComponent_t4152270991_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t214434488_0_0_0_Types[] = { (&HitInfo_t214434488_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t214434488_0_0_0 = { 1, GenInst_HitInfo_t214434488_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t1257714207_0_0_0_Types[] = { (&PersistentCall_t1257714207_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t1257714207_0_0_0 = { 1, GenInst_PersistentCall_t1257714207_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t3782853021_0_0_0_Types[] = { (&BaseInvokableCall_t3782853021_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t3782853021_0_0_0 = { 1, GenInst_BaseInvokableCall_t3782853021_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t4243437884_0_0_0_Types[] = { (&WorkRequest_t4243437884_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t4243437884_0_0_0 = { 1, GenInst_WorkRequest_t4243437884_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t181640494_0_0_0_Types[] = { (&PlayableBinding_t181640494_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t181640494_0_0_0 = { 1, GenInst_PlayableBinding_t181640494_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t2086846075_0_0_0_Types[] = { (&MessageEventArgs_t2086846075_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t2086846075_0_0_0 = { 1, GenInst_MessageEventArgs_t2086846075_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Types[] = { (&MessageTypeSubscribers_t3285514618_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t3285514618_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&MessageTypeSubscribers_t3285514618_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1069295502_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t420362637_0_0_0_Types[] = { (&KeyValuePair_2_t420362637_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t420362637_0_0_0 = { 1, GenInst_KeyValuePair_2_t420362637_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t420362637_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t420362637_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t420362637_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t420362637_0_0_0_Types };
static const RuntimeType* GenInst_WeakReference_t1069295502_0_0_0_Types[] = { (&WeakReference_t1069295502_0_0_0) };
extern const Il2CppGenericInst GenInst_WeakReference_t1069295502_0_0_0 = { 1, GenInst_WeakReference_t1069295502_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1069295502_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1114560181_0_0_0_Types[] = { (&KeyValuePair_2_t1114560181_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1114560181_0_0_0 = { 1, GenInst_KeyValuePair_2_t1114560181_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_KeyValuePair_2_t1114560181_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1069295502_0_0_0), (&KeyValuePair_2_t1114560181_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_KeyValuePair_2_t1114560181_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_KeyValuePair_2_t1114560181_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t3506664974_0_0_0_Types[] = { (&Rigidbody2D_t3506664974_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t3506664974_0_0_0 = { 1, GenInst_Rigidbody2D_t3506664974_0_0_0_Types };
static const RuntimeType* GenInst_Font_t801544967_0_0_0_Types[] = { (&Font_t801544967_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t801544967_0_0_0 = { 1, GenInst_Font_t801544967_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t4115127231_0_0_0_Types[] = { (&UIVertex_t4115127231_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t4115127231_0_0_0 = { 1, GenInst_UIVertex_t4115127231_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t854420848_0_0_0_Types[] = { (&UICharInfo_t854420848_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t854420848_0_0_0 = { 1, GenInst_UICharInfo_t854420848_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t285775551_0_0_0_Types[] = { (&UILineInfo_t285775551_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t285775551_0_0_0 = { 1, GenInst_UILineInfo_t285775551_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t3277800016_0_0_0_Types[] = { (&AnimationClipPlayable_t3277800016_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t3277800016_0_0_0 = { 1, GenInst_AnimationClipPlayable_t3277800016_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t3926483291_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t3926483291_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t3926483291_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t3926483291_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t1623951654_0_0_0_Types[] = { (&AnimationMixerPlayable_t1623951654_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t1623951654_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t1623951654_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t2049487209_0_0_0_Types[] = { (&AnimationOffsetPlayable_t2049487209_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t2049487209_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t2049487209_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t2116071640_0_0_0_Types[] = { (&AnimatorControllerPlayable_t2116071640_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t2116071640_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t2116071640_0_0_0_Types };
static const RuntimeType* GenInst_AudioSpatializerExtensionDefinition_t1514572524_0_0_0_Types[] = { (&AudioSpatializerExtensionDefinition_t1514572524_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSpatializerExtensionDefinition_t1514572524_0_0_0 = { 1, GenInst_AudioSpatializerExtensionDefinition_t1514572524_0_0_0_Types };
static const RuntimeType* GenInst_AudioAmbisonicExtensionDefinition_t3557186147_0_0_0_Types[] = { (&AudioAmbisonicExtensionDefinition_t3557186147_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioAmbisonicExtensionDefinition_t3557186147_0_0_0 = { 1, GenInst_AudioAmbisonicExtensionDefinition_t3557186147_0_0_0_Types };
static const RuntimeType* GenInst_AudioSourceExtension_t2588224584_0_0_0_Types[] = { (&AudioSourceExtension_t2588224584_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSourceExtension_t2588224584_0_0_0 = { 1, GenInst_AudioSourceExtension_t2588224584_0_0_0_Types };
static const RuntimeType* GenInst_ScriptableObject_t2962125979_0_0_0_Types[] = { (&ScriptableObject_t2962125979_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptableObject_t2962125979_0_0_0 = { 1, GenInst_ScriptableObject_t2962125979_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t1613331552_0_0_0_Types[] = { (&AudioMixerPlayable_t1613331552_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t1613331552_0_0_0 = { 1, GenInst_AudioMixerPlayable_t1613331552_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t765244045_0_0_0_Types[] = { (&AudioClipPlayable_t765244045_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t765244045_0_0_0 = { 1, GenInst_AudioClipPlayable_t765244045_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t569405246_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t569405246_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t569405246_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t569405246_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t569405246_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t569405246_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t569405246_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t569405246_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t3476369140_0_0_0_Types[] = { (&AchievementDescription_t3476369140_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3476369140_0_0_0 = { 1, GenInst_AchievementDescription_t3476369140_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t2434259086_0_0_0_Types[] = { (&IAchievementDescription_t2434259086_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2434259086_0_0_0 = { 1, GenInst_IAchievementDescription_t2434259086_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t3121054929_0_0_0_Types[] = { (&UserProfile_t3121054929_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t3121054929_0_0_0 = { 1, GenInst_UserProfile_t3121054929_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t2190722809_0_0_0_Types[] = { (&IUserProfile_t2190722809_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t2190722809_0_0_0 = { 1, GenInst_IUserProfile_t2190722809_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t2259580636_0_0_0_Types[] = { (&GcLeaderboard_t2259580636_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t2259580636_0_0_0 = { 1, GenInst_GcLeaderboard_t2259580636_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t958143867_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t958143867_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t958143867_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t958143867_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t3241716440_0_0_0_Types[] = { (&IAchievementU5BU5D_t3241716440_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t3241716440_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t3241716440_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t3151655605_0_0_0_Types[] = { (&IAchievement_t3151655605_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t3151655605_0_0_0 = { 1, GenInst_IAchievement_t3151655605_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t2938297544_0_0_0_Types[] = { (&GcAchievementData_t2938297544_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t2938297544_0_0_0 = { 1, GenInst_GcAchievementData_t2938297544_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t3727036008_0_0_0_Types[] = { (&Achievement_t3727036008_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t3727036008_0_0_0 = { 1, GenInst_Achievement_t3727036008_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t912747562_0_0_0_Types[] = { (&IScoreU5BU5D_t912747562_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t912747562_0_0_0 = { 1, GenInst_IScoreU5BU5D_t912747562_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t3344695883_0_0_0_Types[] = { (&IScore_t3344695883_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t3344695883_0_0_0 = { 1, GenInst_IScore_t3344695883_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t3653438230_0_0_0_Types[] = { (&GcScoreData_t3653438230_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t3653438230_0_0_0 = { 1, GenInst_GcScoreData_t3653438230_0_0_0_Types };
static const RuntimeType* GenInst_Score_t2147121913_0_0_0_Types[] = { (&Score_t2147121913_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t2147121913_0_0_0 = { 1, GenInst_Score_t2147121913_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t3279866116_0_0_0_Types[] = { (&IUserProfileU5BU5D_t3279866116_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3279866116_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3279866116_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t3076073054_0_0_0_Types[] = { (&GUILayoutOption_t3076073054_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t3076073054_0_0_0 = { 1, GenInst_GUILayoutOption_t3076073054_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&LayoutCache_t3924992466_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0 = { 2, GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1687969195_0_0_0_Types[] = { (&KeyValuePair_2_t1687969195_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1687969195_0_0_0 = { 1, GenInst_KeyValuePair_2_t1687969195_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1687969195_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1687969195_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1687969195_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1687969195_0_0_0_Types };
static const RuntimeType* GenInst_LayoutCache_t3924992466_0_0_0_Types[] = { (&LayoutCache_t3924992466_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutCache_t3924992466_0_0_0 = { 1, GenInst_LayoutCache_t3924992466_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&LayoutCache_t3924992466_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t942896407_0_0_0_Types[] = { (&KeyValuePair_2_t942896407_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t942896407_0_0_0 = { 1, GenInst_KeyValuePair_2_t942896407_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_KeyValuePair_2_t942896407_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&LayoutCache_t3924992466_0_0_0), (&KeyValuePair_2_t942896407_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_KeyValuePair_2_t942896407_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_KeyValuePair_2_t942896407_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t2655444992_0_0_0_Types[] = { (&GUILayoutEntry_t2655444992_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t2655444992_0_0_0 = { 1, GenInst_GUILayoutEntry_t2655444992_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_IntPtr_t_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_IntPtr_t_0_0_0_Boolean_t569405246_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_IntPtr_t_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t2123675094_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Exception_t2123675094_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t2123675094_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Exception_t2123675094_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t4058570518_0_0_0_Types[] = { (&GUIStyle_t4058570518_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t4058570518_0_0_0 = { 1, GenInst_GUIStyle_t4058570518_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t4058570518_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t4058570518_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2175561798_0_0_0_Types[] = { (&KeyValuePair_2_t2175561798_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2175561798_0_0_0 = { 1, GenInst_KeyValuePair_2_t2175561798_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_KeyValuePair_2_t2175561798_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t4058570518_0_0_0), (&KeyValuePair_2_t2175561798_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_KeyValuePair_2_t2175561798_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_KeyValuePair_2_t2175561798_0_0_0_Types };
static const RuntimeType* GenInst_Particle_t651781316_0_0_0_Types[] = { (&Particle_t651781316_0_0_0) };
extern const Il2CppGenericInst GenInst_Particle_t651781316_0_0_0 = { 1, GenInst_Particle_t651781316_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t2851673566_0_0_0_Types[] = { (&RaycastHit_t2851673566_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t2851673566_0_0_0 = { 1, GenInst_RaycastHit_t2851673566_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t2838489305_0_0_0_Types[] = { (&ContactPoint_t2838489305_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t2838489305_0_0_0 = { 1, GenInst_ContactPoint_t2838489305_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t1635553419_0_0_0_Types[] = { (&EventSystem_t1635553419_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t1635553419_0_0_0 = { 1, GenInst_EventSystem_t1635553419_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t2076195789_0_0_0_Types[] = { (&UIBehaviour_t2076195789_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t2076195789_0_0_0 = { 1, GenInst_UIBehaviour_t2076195789_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t3829899482_0_0_0_Types[] = { (&MonoBehaviour_t3829899482_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3829899482_0_0_0 = { 1, GenInst_MonoBehaviour_t3829899482_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t3513845355_0_0_0_Types[] = { (&BaseInputModule_t3513845355_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t3513845355_0_0_0 = { 1, GenInst_BaseInputModule_t3513845355_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t463000792_0_0_0_Types[] = { (&RaycastResult_t463000792_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t463000792_0_0_0 = { 1, GenInst_RaycastResult_t463000792_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t1079439781_0_0_0_Types[] = { (&IDeselectHandler_t1079439781_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t1079439781_0_0_0 = { 1, GenInst_IDeselectHandler_t1079439781_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t2072879830_0_0_0_Types[] = { (&IEventSystemHandler_t2072879830_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2072879830_0_0_0 = { 1, GenInst_IEventSystemHandler_t2072879830_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1883330244_0_0_0_Types[] = { (&List_1_t1883330244_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1883330244_0_0_0 = { 1, GenInst_List_1_t1883330244_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t185548372_0_0_0_Types[] = { (&List_1_t185548372_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t185548372_0_0_0 = { 1, GenInst_List_1_t185548372_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t599864163_0_0_0_Types[] = { (&List_1_t599864163_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t599864163_0_0_0 = { 1, GenInst_List_1_t599864163_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t287548082_0_0_0_Types[] = { (&ISelectHandler_t287548082_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t287548082_0_0_0 = { 1, GenInst_ISelectHandler_t287548082_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t2347538179_0_0_0_Types[] = { (&BaseRaycaster_t2347538179_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2347538179_0_0_0 = { 1, GenInst_BaseRaycaster_t2347538179_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t4245925096_0_0_0_Types[] = { (&Entry_t4245925096_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t4245925096_0_0_0 = { 1, GenInst_Entry_t4245925096_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t1619969158_0_0_0_Types[] = { (&BaseEventData_t1619969158_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t1619969158_0_0_0 = { 1, GenInst_BaseEventData_t1619969158_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t2829503976_0_0_0_Types[] = { (&IPointerEnterHandler_t2829503976_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t2829503976_0_0_0 = { 1, GenInst_IPointerEnterHandler_t2829503976_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t2553928545_0_0_0_Types[] = { (&IPointerExitHandler_t2553928545_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t2553928545_0_0_0 = { 1, GenInst_IPointerExitHandler_t2553928545_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t3783019842_0_0_0_Types[] = { (&IPointerDownHandler_t3783019842_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3783019842_0_0_0 = { 1, GenInst_IPointerDownHandler_t3783019842_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t2582372469_0_0_0_Types[] = { (&IPointerUpHandler_t2582372469_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t2582372469_0_0_0 = { 1, GenInst_IPointerUpHandler_t2582372469_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t3256793649_0_0_0_Types[] = { (&IPointerClickHandler_t3256793649_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t3256793649_0_0_0 = { 1, GenInst_IPointerClickHandler_t3256793649_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t1510685835_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t1510685835_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t1510685835_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t1510685835_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t253960210_0_0_0_Types[] = { (&IBeginDragHandler_t253960210_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t253960210_0_0_0 = { 1, GenInst_IBeginDragHandler_t253960210_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t3031146797_0_0_0_Types[] = { (&IDragHandler_t3031146797_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t3031146797_0_0_0 = { 1, GenInst_IDragHandler_t3031146797_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t2378561395_0_0_0_Types[] = { (&IEndDragHandler_t2378561395_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t2378561395_0_0_0 = { 1, GenInst_IEndDragHandler_t2378561395_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t26677246_0_0_0_Types[] = { (&IDropHandler_t26677246_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t26677246_0_0_0 = { 1, GenInst_IDropHandler_t26677246_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t2953857311_0_0_0_Types[] = { (&IScrollHandler_t2953857311_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t2953857311_0_0_0 = { 1, GenInst_IScrollHandler_t2953857311_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t3805117774_0_0_0_Types[] = { (&IUpdateSelectedHandler_t3805117774_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3805117774_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3805117774_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t3674718662_0_0_0_Types[] = { (&IMoveHandler_t3674718662_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t3674718662_0_0_0 = { 1, GenInst_IMoveHandler_t3674718662_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t160027500_0_0_0_Types[] = { (&ISubmitHandler_t160027500_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t160027500_0_0_0 = { 1, GenInst_ISubmitHandler_t160027500_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t1737091460_0_0_0_Types[] = { (&ICancelHandler_t1737091460_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1737091460_0_0_0 = { 1, GenInst_ICancelHandler_t1737091460_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t532597831_0_0_0_Types[] = { (&Transform_t532597831_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t532597831_0_0_0 = { 1, GenInst_Transform_t532597831_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t1318052361_0_0_0_Types[] = { (&GameObject_t1318052361_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t1318052361_0_0_0 = { 1, GenInst_GameObject_t1318052361_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t2641534846_0_0_0_Types[] = { (&BaseInput_t2641534846_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t2641534846_0_0_0 = { 1, GenInst_BaseInput_t2641534846_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&PointerEventData_t2787773033_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0 = { 2, GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t2787773033_0_0_0_Types[] = { (&PointerEventData_t2787773033_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t2787773033_0_0_0 = { 1, GenInst_PointerEventData_t2787773033_0_0_0_Types };
static const RuntimeType* GenInst_AbstractEventData_t40423571_0_0_0_Types[] = { (&AbstractEventData_t40423571_0_0_0) };
extern const Il2CppGenericInst GenInst_AbstractEventData_t40423571_0_0_0 = { 1, GenInst_AbstractEventData_t40423571_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&PointerEventData_t2787773033_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4100644270_0_0_0_Types[] = { (&KeyValuePair_2_t4100644270_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4100644270_0_0_0 = { 1, GenInst_KeyValuePair_2_t4100644270_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_KeyValuePair_2_t4100644270_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&PointerEventData_t2787773033_0_0_0), (&KeyValuePair_2_t4100644270_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_KeyValuePair_2_t4100644270_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_KeyValuePair_2_t4100644270_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_PointerEventData_t2787773033_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&PointerEventData_t2787773033_0_0_0), (&PointerEventData_t2787773033_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_PointerEventData_t2787773033_0_0_0 = { 3, GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_PointerEventData_t2787773033_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t1610769254_0_0_0_Types[] = { (&ButtonState_t1610769254_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t1610769254_0_0_0 = { 1, GenInst_ButtonState_t1610769254_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t266689378_0_0_0_Types[] = { (&RaycastHit2D_t266689378_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t266689378_0_0_0 = { 1, GenInst_RaycastHit2D_t266689378_0_0_0_Types };
static const RuntimeType* GenInst_Color_t460381780_0_0_0_Types[] = { (&Color_t460381780_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t460381780_0_0_0 = { 1, GenInst_Color_t460381780_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2902062777_0_0_0_Types[] = { (&ICanvasElement_t2902062777_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2902062777_0_0_0 = { 1, GenInst_ICanvasElement_t2902062777_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&ICanvasElement_t2902062777_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&ICanvasElement_t2902062777_0_0_0), (&Int32_t499004851_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t266244752_0_0_0_Types[] = { (&ColorBlock_t266244752_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t266244752_0_0_0 = { 1, GenInst_ColorBlock_t266244752_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t1608351530_0_0_0_Types[] = { (&OptionData_t1608351530_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t1608351530_0_0_0 = { 1, GenInst_OptionData_t1608351530_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t3503496286_0_0_0_Types[] = { (&DropdownItem_t3503496286_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t3503496286_0_0_0 = { 1, GenInst_DropdownItem_t3503496286_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t3352968507_0_0_0_Types[] = { (&FloatTween_t3352968507_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t3352968507_0_0_0 = { 1, GenInst_FloatTween_t3352968507_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t3012664695_0_0_0_Types[] = { (&Sprite_t3012664695_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t3012664695_0_0_0 = { 1, GenInst_Sprite_t3012664695_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t852644723_0_0_0_Types[] = { (&Canvas_t852644723_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t852644723_0_0_0 = { 1, GenInst_Canvas_t852644723_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t663095137_0_0_0_Types[] = { (&List_1_t663095137_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t663095137_0_0_0 = { 1, GenInst_List_1_t663095137_0_0_0_Types };
static const RuntimeType* GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_Types[] = { (&Font_t801544967_0_0_0), (&HashSet_1_t2686032385_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0 = { 2, GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_Types };
static const RuntimeType* GenInst_Text_t866332725_0_0_0_Types[] = { (&Text_t866332725_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t866332725_0_0_0 = { 1, GenInst_Text_t866332725_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2893110488_0_0_0_Types[] = { (&Link_t2893110488_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2893110488_0_0_0 = { 1, GenInst_Link_t2893110488_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t632884531_0_0_0_Types[] = { (&ILayoutElement_t632884531_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t632884531_0_0_0 = { 1, GenInst_ILayoutElement_t632884531_0_0_0_Types };
static const RuntimeType* GenInst_MaskableGraphic_t92248932_0_0_0_Types[] = { (&MaskableGraphic_t92248932_0_0_0) };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t92248932_0_0_0 = { 1, GenInst_MaskableGraphic_t92248932_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t3700685589_0_0_0_Types[] = { (&IClippable_t3700685589_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t3700685589_0_0_0 = { 1, GenInst_IClippable_t3700685589_0_0_0_Types };
static const RuntimeType* GenInst_IMaskable_t3175584775_0_0_0_Types[] = { (&IMaskable_t3175584775_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaskable_t3175584775_0_0_0 = { 1, GenInst_IMaskable_t3175584775_0_0_0_Types };
static const RuntimeType* GenInst_IMaterialModifier_t2216105711_0_0_0_Types[] = { (&IMaterialModifier_t2216105711_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t2216105711_0_0_0 = { 1, GenInst_IMaterialModifier_t2216105711_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1165931317_0_0_0_Types[] = { (&Graphic_t1165931317_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1165931317_0_0_0 = { 1, GenInst_Graphic_t1165931317_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t2686032385_0_0_0_Types[] = { (&HashSet_1_t2686032385_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t2686032385_0_0_0 = { 1, GenInst_HashSet_1_t2686032385_0_0_0_Types };
static const RuntimeType* GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Font_t801544967_0_0_0), (&HashSet_1_t2686032385_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3484435490_0_0_0_Types[] = { (&KeyValuePair_2_t3484435490_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3484435490_0_0_0 = { 1, GenInst_KeyValuePair_2_t3484435490_0_0_0_Types };
static const RuntimeType* GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_KeyValuePair_2_t3484435490_0_0_0_Types[] = { (&Font_t801544967_0_0_0), (&HashSet_1_t2686032385_0_0_0), (&KeyValuePair_2_t3484435490_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_KeyValuePair_2_t3484435490_0_0_0 = { 3, GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_KeyValuePair_2_t3484435490_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t1361506897_0_0_0_Types[] = { (&ColorTween_t1361506897_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t1361506897_0_0_0 = { 1, GenInst_ColorTween_t1361506897_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_Types[] = { (&Canvas_t852644723_0_0_0), (&IndexedSet_1_t2847635963_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0 = { 2, GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&Graphic_t1165931317_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Graphic_t1165931317_0_0_0), (&Int32_t499004851_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t2847635963_0_0_0_Types[] = { (&IndexedSet_1_t2847635963_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2847635963_0_0_0 = { 1, GenInst_IndexedSet_1_t2847635963_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Canvas_t852644723_0_0_0), (&IndexedSet_1_t2847635963_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3364176000_0_0_0_Types[] = { (&KeyValuePair_2_t3364176000_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3364176000_0_0_0 = { 1, GenInst_KeyValuePair_2_t3364176000_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_KeyValuePair_2_t3364176000_0_0_0_Types[] = { (&Canvas_t852644723_0_0_0), (&IndexedSet_1_t2847635963_0_0_0), (&KeyValuePair_2_t3364176000_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_KeyValuePair_2_t3364176000_0_0_0 = { 3, GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_KeyValuePair_2_t3364176000_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t101362478_0_0_0_Types[] = { (&KeyValuePair_2_t101362478_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t101362478_0_0_0 = { 1, GenInst_KeyValuePair_2_t101362478_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t101362478_0_0_0_Types[] = { (&Graphic_t1165931317_0_0_0), (&Int32_t499004851_0_0_0), (&KeyValuePair_2_t101362478_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t101362478_0_0_0 = { 3, GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t101362478_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3961451802_0_0_0_Types[] = { (&KeyValuePair_2_t3961451802_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3961451802_0_0_0 = { 1, GenInst_KeyValuePair_2_t3961451802_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t3961451802_0_0_0_Types[] = { (&ICanvasElement_t2902062777_0_0_0), (&Int32_t499004851_0_0_0), (&KeyValuePair_2_t3961451802_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t3961451802_0_0_0 = { 3, GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t3961451802_0_0_0_Types };
static const RuntimeType* GenInst_Type_t74284121_0_0_0_Types[] = { (&Type_t74284121_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t74284121_0_0_0 = { 1, GenInst_Type_t74284121_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t4202801712_0_0_0_Types[] = { (&FillMethod_t4202801712_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t4202801712_0_0_0 = { 1, GenInst_FillMethod_t4202801712_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t59827793_0_0_0_Types[] = { (&ContentType_t59827793_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t59827793_0_0_0 = { 1, GenInst_ContentType_t59827793_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t1398395467_0_0_0_Types[] = { (&LineType_t1398395467_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t1398395467_0_0_0 = { 1, GenInst_LineType_t1398395467_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t2454186138_0_0_0_Types[] = { (&InputType_t2454186138_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t2454186138_0_0_0 = { 1, GenInst_InputType_t2454186138_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t2115689981_0_0_0_Types[] = { (&TouchScreenKeyboardType_t2115689981_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t2115689981_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t2115689981_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t2898689831_0_0_0_Types[] = { (&CharacterValidation_t2898689831_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t2898689831_0_0_0 = { 1, GenInst_CharacterValidation_t2898689831_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t4149322243_0_0_0_Types[] = { (&Mask_t4149322243_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t4149322243_0_0_0 = { 1, GenInst_Mask_t4149322243_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasRaycastFilter_t3610902803_0_0_0_Types[] = { (&ICanvasRaycastFilter_t3610902803_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t3610902803_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t3610902803_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3959772657_0_0_0_Types[] = { (&List_1_t3959772657_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3959772657_0_0_0 = { 1, GenInst_List_1_t3959772657_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t1299097536_0_0_0_Types[] = { (&RectMask2D_t1299097536_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t1299097536_0_0_0 = { 1, GenInst_RectMask2D_t1299097536_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4258280777_0_0_0_Types[] = { (&IClipper_t4258280777_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4258280777_0_0_0 = { 1, GenInst_IClipper_t4258280777_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1109547950_0_0_0_Types[] = { (&List_1_t1109547950_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1109547950_0_0_0 = { 1, GenInst_List_1_t1109547950_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t4246164788_0_0_0_Types[] = { (&Navigation_t4246164788_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t4246164788_0_0_0 = { 1, GenInst_Navigation_t4246164788_0_0_0_Types };
static const RuntimeType* GenInst_Link_t1432496056_0_0_0_Types[] = { (&Link_t1432496056_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t1432496056_0_0_0 = { 1, GenInst_Link_t1432496056_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t2752781413_0_0_0_Types[] = { (&Direction_t2752781413_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t2752781413_0_0_0 = { 1, GenInst_Direction_t2752781413_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t199987819_0_0_0_Types[] = { (&Selectable_t199987819_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t199987819_0_0_0 = { 1, GenInst_Selectable_t199987819_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t3336380680_0_0_0_Types[] = { (&Transition_t3336380680_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t3336380680_0_0_0 = { 1, GenInst_Transition_t3336380680_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t2305809087_0_0_0_Types[] = { (&SpriteState_t2305809087_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t2305809087_0_0_0 = { 1, GenInst_SpriteState_t2305809087_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t370922105_0_0_0_Types[] = { (&CanvasGroup_t370922105_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t370922105_0_0_0 = { 1, GenInst_CanvasGroup_t370922105_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t571314014_0_0_0_Types[] = { (&Direction_t571314014_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t571314014_0_0_0 = { 1, GenInst_Direction_t571314014_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t1253440912_0_0_0_Types[] = { (&MatEntry_t1253440912_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t1253440912_0_0_0 = { 1, GenInst_MatEntry_t1253440912_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3640703094_0_0_0_Types[] = { (&Toggle_t3640703094_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3640703094_0_0_0 = { 1, GenInst_Toggle_t3640703094_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3640703094_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Toggle_t3640703094_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3640703094_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Toggle_t3640703094_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&IClipper_t4258280777_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&IClipper_t4258280777_0_0_0), (&Int32_t499004851_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2141035082_0_0_0_Types[] = { (&KeyValuePair_2_t2141035082_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2141035082_0_0_0 = { 1, GenInst_KeyValuePair_2_t2141035082_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2141035082_0_0_0_Types[] = { (&IClipper_t4258280777_0_0_0), (&Int32_t499004851_0_0_0), (&KeyValuePair_2_t2141035082_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2141035082_0_0_0 = { 3, GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2141035082_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t2023416516_0_0_0_Types[] = { (&AspectMode_t2023416516_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t2023416516_0_0_0 = { 1, GenInst_AspectMode_t2023416516_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t3728304995_0_0_0_Types[] = { (&FitMode_t3728304995_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t3728304995_0_0_0 = { 1, GenInst_FitMode_t3728304995_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t15861704_0_0_0_Types[] = { (&RectTransform_t15861704_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t15861704_0_0_0 = { 1, GenInst_RectTransform_t15861704_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t2374704703_0_0_0_Types[] = { (&LayoutRebuilder_t2374704703_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2374704703_0_0_0 = { 1, GenInst_LayoutRebuilder_t2374704703_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t632884531_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&ILayoutElement_t632884531_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t632884531_0_0_0_Single_t1863352746_0_0_0 = { 2, GenInst_ILayoutElement_t632884531_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t140159775_0_0_0_Types[] = { (&List_1_t140159775_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t140159775_0_0_0 = { 1, GenInst_List_1_t140159775_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2598598263_0_0_0_Types[] = { (&List_1_t2598598263_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2598598263_0_0_0 = { 1, GenInst_List_1_t2598598263_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2867512982_0_0_0_Types[] = { (&List_1_t2867512982_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2867512982_0_0_0 = { 1, GenInst_List_1_t2867512982_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t191085541_0_0_0_Types[] = { (&List_1_t191085541_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t191085541_0_0_0 = { 1, GenInst_List_1_t191085541_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t309455265_0_0_0_Types[] = { (&List_1_t309455265_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t309455265_0_0_0 = { 1, GenInst_List_1_t309455265_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3925577645_0_0_0_Types[] = { (&List_1_t3925577645_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3925577645_0_0_0 = { 1, GenInst_List_1_t3925577645_0_0_0_Types };
static const RuntimeType* GenInst_Link_t1967700103_0_0_0_Types[] = { (&Link_t1967700103_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t1967700103_0_0_0 = { 1, GenInst_Link_t1967700103_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&String_t_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1863352746_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1179652112_0_0_0_Types[] = { (&KeyValuePair_2_t1179652112_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1179652112_0_0_0 = { 1, GenInst_KeyValuePair_2_t1179652112_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t1863352746_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t1863352746_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t1179652112_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t1863352746_0_0_0), (&KeyValuePair_2_t1179652112_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t1179652112_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t1179652112_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&String_t_0_0_0), (&Single_t1863352746_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4275311322_0_0_0_Types[] = { (&KeyValuePair_2_t4275311322_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4275311322_0_0_0 = { 1, GenInst_KeyValuePair_2_t4275311322_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t4275311322_0_0_0_Types[] = { (&String_t_0_0_0), (&Single_t1863352746_0_0_0), (&KeyValuePair_2_t4275311322_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t4275311322_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t4275311322_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t1183581528_0_0_0_Types[] = { (&ARHitTestResult_t1183581528_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t1183581528_0_0_0 = { 1, GenInst_ARHitTestResult_t1183581528_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResultType_t3376578133_0_0_0_Types[] = { (&ARHitTestResultType_t3376578133_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResultType_t3376578133_0_0_0 = { 1, GenInst_ARHitTestResultType_t3376578133_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&Single_t1863352746_0_0_0), (&Single_t1863352746_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0 = { 3, GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types[] = { (&Single_t1863352746_0_0_0), (&Single_t1863352746_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0 = { 2, GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Types };
static const RuntimeType* GenInst_ParticleSystem_t1097441456_0_0_0_Types[] = { (&ParticleSystem_t1097441456_0_0_0) };
extern const Il2CppGenericInst GenInst_ParticleSystem_t1097441456_0_0_0 = { 1, GenInst_ParticleSystem_t1097441456_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2270868264_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0 = { 2, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_Types };
static const RuntimeType* GenInst_ARPlaneAnchorGameObject_t2270868264_0_0_0_Types[] = { (&ARPlaneAnchorGameObject_t2270868264_0_0_0) };
extern const Il2CppGenericInst GenInst_ARPlaneAnchorGameObject_t2270868264_0_0_0 = { 1, GenInst_ARPlaneAnchorGameObject_t2270868264_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2270868264_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t387859544_0_0_0_Types[] = { (&KeyValuePair_2_t387859544_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t387859544_0_0_0 = { 1, GenInst_KeyValuePair_2_t387859544_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_KeyValuePair_2_t387859544_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2270868264_0_0_0), (&KeyValuePair_2_t387859544_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_KeyValuePair_2_t387859544_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_KeyValuePair_2_t387859544_0_0_0_Types };
static const RuntimeType* GenInst_UnityARSessionRunOption_t2827180039_0_0_0_Types[] = { (&UnityARSessionRunOption_t2827180039_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARSessionRunOption_t2827180039_0_0_0 = { 1, GenInst_UnityARSessionRunOption_t2827180039_0_0_0_Types };
static const RuntimeType* GenInst_UnityARAlignment_t2394317114_0_0_0_Types[] = { (&UnityARAlignment_t2394317114_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARAlignment_t2394317114_0_0_0 = { 1, GenInst_UnityARAlignment_t2394317114_0_0_0_Types };
static const RuntimeType* GenInst_UnityARPlaneDetection_t4236545235_0_0_0_Types[] = { (&UnityARPlaneDetection_t4236545235_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARPlaneDetection_t4236545235_0_0_0 = { 1, GenInst_UnityARPlaneDetection_t4236545235_0_0_0_Types };
static const RuntimeType* GenInst_Light_t3883945566_0_0_0_Types[] = { (&Light_t3883945566_0_0_0) };
extern const Il2CppGenericInst GenInst_Light_t3883945566_0_0_0 = { 1, GenInst_Light_t3883945566_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t536933947_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t536933947_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t536933947_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t536933947_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m734803740_gp_0_0_0_0_Array_Sort_m734803740_gp_0_0_0_0_Types[] = { (&Array_Sort_m734803740_gp_0_0_0_0), (&Array_Sort_m734803740_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m734803740_gp_0_0_0_0_Array_Sort_m734803740_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m734803740_gp_0_0_0_0_Array_Sort_m734803740_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m787042007_gp_0_0_0_0_Array_Sort_m787042007_gp_1_0_0_0_Types[] = { (&Array_Sort_m787042007_gp_0_0_0_0), (&Array_Sort_m787042007_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m787042007_gp_0_0_0_0_Array_Sort_m787042007_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m787042007_gp_0_0_0_0_Array_Sort_m787042007_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Types[] = { (&Array_Sort_m3596954745_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3596954745_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Array_Sort_m3596954745_gp_0_0_0_0_Types[] = { (&Array_Sort_m3596954745_gp_0_0_0_0), (&Array_Sort_m3596954745_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Array_Sort_m3596954745_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Array_Sort_m3596954745_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Types[] = { (&Array_Sort_m2095751586_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2095751586_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Array_Sort_m2095751586_gp_1_0_0_0_Types[] = { (&Array_Sort_m2095751586_gp_0_0_0_0), (&Array_Sort_m2095751586_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Array_Sort_m2095751586_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Array_Sort_m2095751586_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m925910360_gp_0_0_0_0_Array_Sort_m925910360_gp_0_0_0_0_Types[] = { (&Array_Sort_m925910360_gp_0_0_0_0), (&Array_Sort_m925910360_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m925910360_gp_0_0_0_0_Array_Sort_m925910360_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m925910360_gp_0_0_0_0_Array_Sort_m925910360_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2097743790_gp_0_0_0_0_Array_Sort_m2097743790_gp_1_0_0_0_Types[] = { (&Array_Sort_m2097743790_gp_0_0_0_0), (&Array_Sort_m2097743790_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2097743790_gp_0_0_0_0_Array_Sort_m2097743790_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2097743790_gp_0_0_0_0_Array_Sort_m2097743790_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Types[] = { (&Array_Sort_m3972326822_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3972326822_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Array_Sort_m3972326822_gp_0_0_0_0_Types[] = { (&Array_Sort_m3972326822_gp_0_0_0_0), (&Array_Sort_m3972326822_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Array_Sort_m3972326822_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Array_Sort_m3972326822_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Types[] = { (&Array_Sort_m2038482742_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2038482742_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2038482742_gp_1_0_0_0_Types[] = { (&Array_Sort_m2038482742_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2038482742_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m2038482742_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Array_Sort_m2038482742_gp_1_0_0_0_Types[] = { (&Array_Sort_m2038482742_gp_0_0_0_0), (&Array_Sort_m2038482742_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Array_Sort_m2038482742_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Array_Sort_m2038482742_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4074792876_gp_0_0_0_0_Types[] = { (&Array_Sort_m4074792876_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4074792876_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4074792876_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1131242284_gp_0_0_0_0_Types[] = { (&Array_Sort_m1131242284_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1131242284_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1131242284_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m267516494_gp_0_0_0_0_Types[] = { (&Array_qsort_m267516494_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m267516494_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m267516494_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m267516494_gp_0_0_0_0_Array_qsort_m267516494_gp_1_0_0_0_Types[] = { (&Array_qsort_m267516494_gp_0_0_0_0), (&Array_qsort_m267516494_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m267516494_gp_0_0_0_0_Array_qsort_m267516494_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m267516494_gp_0_0_0_0_Array_qsort_m267516494_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m2330141893_gp_0_0_0_0_Types[] = { (&Array_compare_m2330141893_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m2330141893_gp_0_0_0_0 = { 1, GenInst_Array_compare_m2330141893_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m1179209724_gp_0_0_0_0_Types[] = { (&Array_qsort_m1179209724_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m1179209724_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1179209724_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m2794471509_gp_0_0_0_0_Types[] = { (&Array_Resize_m2794471509_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m2794471509_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2794471509_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m2758149500_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m2758149500_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2758149500_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2758149500_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m2910560811_gp_0_0_0_0_Types[] = { (&Array_ForEach_m2910560811_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m2910560811_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m2910560811_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m3816593202_gp_0_0_0_0_Array_ConvertAll_m3816593202_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m3816593202_gp_0_0_0_0), (&Array_ConvertAll_m3816593202_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m3816593202_gp_0_0_0_0_Array_ConvertAll_m3816593202_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m3816593202_gp_0_0_0_0_Array_ConvertAll_m3816593202_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m4047168095_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m4047168095_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m4047168095_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m4047168095_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2722629763_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2722629763_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2722629763_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2722629763_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1417763420_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1417763420_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1417763420_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1417763420_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3608062671_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3608062671_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3608062671_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3608062671_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3859049537_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3859049537_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3859049537_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3859049537_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1091091128_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1091091128_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1091091128_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1091091128_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3933600019_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3933600019_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3933600019_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3933600019_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m915595758_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m915595758_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m915595758_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m915595758_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3668675172_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3668675172_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3668675172_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3668675172_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3050703072_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3050703072_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3050703072_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3050703072_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m1079732850_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m1079732850_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1079732850_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1079732850_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m3254707976_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m3254707976_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3254707976_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3254707976_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m43705925_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m43705925_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m43705925_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m43705925_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m3752110915_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m3752110915_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3752110915_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3752110915_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1647032180_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1647032180_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1647032180_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1647032180_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1323951861_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1323951861_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1323951861_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1323951861_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m1602332273_gp_0_0_0_0_Types[] = { (&Array_FindAll_m1602332273_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1602332273_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1602332273_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m1616247547_gp_0_0_0_0_Types[] = { (&Array_Exists_m1616247547_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m1616247547_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1616247547_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m443996534_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m443996534_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m443996534_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m443996534_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m2550374671_gp_0_0_0_0_Types[] = { (&Array_Find_m2550374671_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m2550374671_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2550374671_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m2838405935_gp_0_0_0_0_Types[] = { (&Array_FindLast_m2838405935_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m2838405935_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m2838405935_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t4134619510_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t4134619510_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t4134619510_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t4134619510_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t555026515_gp_0_0_0_0_Types[] = { (&IList_1_t555026515_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t555026515_gp_0_0_0_0 = { 1, GenInst_IList_1_t555026515_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1055842719_gp_0_0_0_0_Types[] = { (&ICollection_1_t1055842719_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1055842719_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1055842719_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t3261863354_gp_0_0_0_0_Types[] = { (&Nullable_1_t3261863354_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t3261863354_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t3261863354_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t4288707744_gp_0_0_0_0_Types[] = { (&Comparer_1_t4288707744_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t4288707744_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t4288707744_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t4214145415_gp_0_0_0_0_Types[] = { (&DefaultComparer_t4214145415_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4214145415_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4214145415_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t1346033315_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t1346033315_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1346033315_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1346033315_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0), (&Dictionary_2_t4241968728_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t228248057_0_0_0_Types[] = { (&KeyValuePair_2_t228248057_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t228248057_0_0_0 = { 1, GenInst_KeyValuePair_2_t228248057_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0), (&Dictionary_2_t4241968728_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0), (&Dictionary_2_t4241968728_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0), (&Dictionary_2_t4241968728_gp_1_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 3, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t3171201328_gp_0_0_0_0_ShimEnumerator_t3171201328_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t3171201328_gp_0_0_0_0), (&ShimEnumerator_t3171201328_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3171201328_gp_0_0_0_0_ShimEnumerator_t3171201328_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3171201328_gp_0_0_0_0_ShimEnumerator_t3171201328_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3672918140_gp_0_0_0_0_Enumerator_t3672918140_gp_1_0_0_0_Types[] = { (&Enumerator_t3672918140_gp_0_0_0_0), (&Enumerator_t3672918140_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3672918140_gp_0_0_0_0_Enumerator_t3672918140_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3672918140_gp_0_0_0_0_Enumerator_t3672918140_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1695844585_0_0_0_Types[] = { (&KeyValuePair_2_t1695844585_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1695844585_0_0_0 = { 1, GenInst_KeyValuePair_2_t1695844585_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types[] = { (&ValueCollection_t2778072946_gp_0_0_0_0), (&ValueCollection_t2778072946_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2778072946_gp_1_0_0_0_Types[] = { (&ValueCollection_t2778072946_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2778072946_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2778072946_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t936271527_gp_0_0_0_0_Enumerator_t936271527_gp_1_0_0_0_Types[] = { (&Enumerator_t936271527_gp_0_0_0_0), (&Enumerator_t936271527_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t936271527_gp_0_0_0_0_Enumerator_t936271527_gp_1_0_0_0 = { 2, GenInst_Enumerator_t936271527_gp_0_0_0_0_Enumerator_t936271527_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t936271527_gp_1_0_0_0_Types[] = { (&Enumerator_t936271527_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t936271527_gp_1_0_0_0 = { 1, GenInst_Enumerator_t936271527_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types[] = { (&ValueCollection_t2778072946_gp_0_0_0_0), (&ValueCollection_t2778072946_gp_1_0_0_0), (&ValueCollection_t2778072946_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types[] = { (&ValueCollection_t2778072946_gp_1_0_0_0), (&ValueCollection_t2778072946_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2422360636_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types[] = { (&DictionaryEntry_t2422360636_0_0_0), (&DictionaryEntry_t2422360636_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2422360636_0_0_0_DictionaryEntry_t2422360636_0_0_0 = { 2, GenInst_DictionaryEntry_t2422360636_0_0_0_DictionaryEntry_t2422360636_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_KeyValuePair_2_t228248057_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_0_0_0_0), (&Dictionary_2_t4241968728_gp_1_0_0_0), (&KeyValuePair_2_t228248057_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_KeyValuePair_2_t228248057_0_0_0 = { 3, GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_KeyValuePair_2_t228248057_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t228248057_0_0_0_KeyValuePair_2_t228248057_0_0_0_Types[] = { (&KeyValuePair_2_t228248057_0_0_0), (&KeyValuePair_2_t228248057_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t228248057_0_0_0_KeyValuePair_2_t228248057_0_0_0 = { 2, GenInst_KeyValuePair_2_t228248057_0_0_0_KeyValuePair_2_t228248057_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t4241968728_gp_1_0_0_0_Types[] = { (&Dictionary_2_t4241968728_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4241968728_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t4241968728_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t2843542286_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t2843542286_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2843542286_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2843542286_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t4044621341_gp_0_0_0_0_Types[] = { (&DefaultComparer_t4044621341_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4044621341_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4044621341_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t2574266403_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t2574266403_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2574266403_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2574266403_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t567457625_0_0_0_Types[] = { (&KeyValuePair_2_t567457625_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t567457625_0_0_0 = { 1, GenInst_KeyValuePair_2_t567457625_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3270403520_gp_0_0_0_0_IDictionary_2_t3270403520_gp_1_0_0_0_Types[] = { (&IDictionary_2_t3270403520_gp_0_0_0_0), (&IDictionary_2_t3270403520_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3270403520_gp_0_0_0_0_IDictionary_2_t3270403520_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3270403520_gp_0_0_0_0_IDictionary_2_t3270403520_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t694182271_gp_0_0_0_0_KeyValuePair_2_t694182271_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t694182271_gp_0_0_0_0), (&KeyValuePair_2_t694182271_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t694182271_gp_0_0_0_0_KeyValuePair_2_t694182271_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t694182271_gp_0_0_0_0_KeyValuePair_2_t694182271_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1825915537_gp_0_0_0_0_Types[] = { (&List_1_t1825915537_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1825915537_gp_0_0_0_0 = { 1, GenInst_List_1_t1825915537_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1634006062_gp_0_0_0_0_Types[] = { (&Enumerator_t1634006062_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1634006062_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1634006062_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t2715885535_gp_0_0_0_0_Types[] = { (&Collection_1_t2715885535_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t2715885535_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2715885535_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t2737108554_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t2737108554_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2737108554_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2737108554_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t1129464372_gp_0_0_0_0_Types[] = { (&Queue_1_t1129464372_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t1129464372_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1129464372_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t63438097_gp_0_0_0_0_Types[] = { (&Enumerator_t63438097_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t63438097_gp_0_0_0_0 = { 1, GenInst_Enumerator_t63438097_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t2792833190_gp_0_0_0_0_Types[] = { (&Stack_1_t2792833190_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t2792833190_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2792833190_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4232829344_gp_0_0_0_0_Types[] = { (&Enumerator_t4232829344_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4232829344_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4232829344_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t1434642847_gp_0_0_0_0_Types[] = { (&HashSet_1_t1434642847_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t1434642847_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t1434642847_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3119472212_gp_0_0_0_0_Types[] = { (&Enumerator_t3119472212_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3119472212_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3119472212_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t3200753757_gp_0_0_0_0_Types[] = { (&PrimeHelper_t3200753757_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3200753757_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3200753757_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m2805250219_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m2805250219_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2805250219_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2805250219_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Types[] = { (&Enumerable_Single_m1169226985_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Enumerable_Single_m1169226985_gp_0_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToList_m2871797791_gp_0_0_0_0_Types[] = { (&Enumerable_ToList_m2871797791_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m2871797791_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m2871797791_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m2312884896_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Enumerable_Where_m2312884896_gp_0_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m1455327788_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m1455327788_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m1455327788_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m1455327788_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m327293654_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m327293654_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m327293654_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m327293654_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m2020898487_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m2020898487_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2020898487_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2020898487_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m4081353059_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m4081353059_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m4081353059_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m4081353059_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m3335520185_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m3335520185_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m3335520185_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m3335520185_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m3381760768_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m3381760768_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3381760768_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3381760768_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m370193599_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m370193599_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m370193599_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m370193599_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m613625000_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m613625000_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m613625000_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m613625000_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_FindObjectsOfType_m895778613_gp_0_0_0_0_Types[] = { (&Object_FindObjectsOfType_m895778613_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m895778613_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m895778613_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t2384605844_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t2384605844_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t2384605844_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t2384605844_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t880801182_0_0_0_Types[] = { (&UnityAction_1_t880801182_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t880801182_0_0_0 = { 1, GenInst_UnityAction_1_t880801182_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_InvokableCall_2_t686556727_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t686556727_gp_0_0_0_0), (&InvokableCall_2_t686556727_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_InvokableCall_2_t686556727_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_InvokableCall_2_t686556727_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_2_t3720805410_0_0_0_Types[] = { (&UnityAction_2_t3720805410_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_2_t3720805410_0_0_0 = { 1, GenInst_UnityAction_2_t3720805410_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t686556727_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t686556727_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t686556727_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t686556727_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t686556727_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t686556727_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_InvokableCall_3_t1114138605_gp_1_0_0_0_InvokableCall_3_t1114138605_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1114138605_gp_0_0_0_0), (&InvokableCall_3_t1114138605_gp_1_0_0_0), (&InvokableCall_3_t1114138605_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_InvokableCall_3_t1114138605_gp_1_0_0_0_InvokableCall_3_t1114138605_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_InvokableCall_3_t1114138605_gp_1_0_0_0_InvokableCall_3_t1114138605_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_3_t2206700587_0_0_0_Types[] = { (&UnityAction_3_t2206700587_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_3_t2206700587_0_0_0 = { 1, GenInst_UnityAction_3_t2206700587_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t1114138605_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1114138605_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t1114138605_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1114138605_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t1114138605_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1114138605_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1114138605_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1114138605_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t1114138605_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_InvokableCall_4_t3841286312_gp_1_0_0_0_InvokableCall_4_t3841286312_gp_2_0_0_0_InvokableCall_4_t3841286312_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3841286312_gp_0_0_0_0), (&InvokableCall_4_t3841286312_gp_1_0_0_0), (&InvokableCall_4_t3841286312_gp_2_0_0_0), (&InvokableCall_4_t3841286312_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_InvokableCall_4_t3841286312_gp_1_0_0_0_InvokableCall_4_t3841286312_gp_2_0_0_0_InvokableCall_4_t3841286312_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_InvokableCall_4_t3841286312_gp_1_0_0_0_InvokableCall_4_t3841286312_gp_2_0_0_0_InvokableCall_4_t3841286312_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t3841286312_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3841286312_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t3841286312_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3841286312_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3841286312_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3841286312_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t3841286312_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3841286312_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3841286312_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3841286312_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3841286312_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3841286312_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3841286312_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t269288053_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t269288053_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t269288053_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t269288053_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t1144529152_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t1144529152_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1144529152_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t1144529152_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t4235607108_gp_0_0_0_0_UnityEvent_2_t4235607108_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t4235607108_gp_0_0_0_0), (&UnityEvent_2_t4235607108_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4235607108_gp_0_0_0_0_UnityEvent_2_t4235607108_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4235607108_gp_0_0_0_0_UnityEvent_2_t4235607108_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t1018889849_gp_0_0_0_0_UnityEvent_3_t1018889849_gp_1_0_0_0_UnityEvent_3_t1018889849_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t1018889849_gp_0_0_0_0), (&UnityEvent_3_t1018889849_gp_1_0_0_0), (&UnityEvent_3_t1018889849_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1018889849_gp_0_0_0_0_UnityEvent_3_t1018889849_gp_1_0_0_0_UnityEvent_3_t1018889849_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t1018889849_gp_0_0_0_0_UnityEvent_3_t1018889849_gp_1_0_0_0_UnityEvent_3_t1018889849_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t963784060_gp_0_0_0_0_UnityEvent_4_t963784060_gp_1_0_0_0_UnityEvent_4_t963784060_gp_2_0_0_0_UnityEvent_4_t963784060_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t963784060_gp_0_0_0_0), (&UnityEvent_4_t963784060_gp_1_0_0_0), (&UnityEvent_4_t963784060_gp_2_0_0_0), (&UnityEvent_4_t963784060_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t963784060_gp_0_0_0_0_UnityEvent_4_t963784060_gp_1_0_0_0_UnityEvent_4_t963784060_gp_2_0_0_0_UnityEvent_4_t963784060_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t963784060_gp_0_0_0_0_UnityEvent_4_t963784060_gp_1_0_0_0_UnityEvent_4_t963784060_gp_2_0_0_0_UnityEvent_4_t963784060_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m871388233_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m871388233_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m871388233_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m871388233_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t2880223937_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t2880223937_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2880223937_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2880223937_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t1694099753_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&IndexedSet_1_t1694099753_gp_0_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t1470686486_gp_0_0_0_0_Types[] = { (&ListPool_1_t1470686486_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t1470686486_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1470686486_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1928769349_0_0_0_Types[] = { (&List_1_t1928769349_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1928769349_0_0_0 = { 1, GenInst_List_1_t1928769349_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t2798048145_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t2798048145_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t2798048145_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t2798048145_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t2771548650_0_0_0_Types[] = { (&DefaultExecutionOrder_t2771548650_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2771548650_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2771548650_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t781534341_0_0_0_Types[] = { (&PlayerConnection_t781534341_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t781534341_0_0_0 = { 1, GenInst_PlayerConnection_t781534341_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t2694382279_0_0_0_Types[] = { (&GUILayer_t2694382279_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t2694382279_0_0_0 = { 1, GenInst_GUILayer_t2694382279_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t2438176915_0_0_0_Types[] = { (&AxisEventData_t2438176915_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t2438176915_0_0_0 = { 1, GenInst_AxisEventData_t2438176915_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t3043448750_0_0_0_Types[] = { (&SpriteRenderer_t3043448750_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t3043448750_0_0_0 = { 1, GenInst_SpriteRenderer_t3043448750_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t3324891182_0_0_0_Types[] = { (&GraphicRaycaster_t3324891182_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t3324891182_0_0_0 = { 1, GenInst_GraphicRaycaster_t3324891182_0_0_0_Types };
static const RuntimeType* GenInst_Image_t61690498_0_0_0_Types[] = { (&Image_t61690498_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t61690498_0_0_0 = { 1, GenInst_Image_t61690498_0_0_0_Types };
static const RuntimeType* GenInst_Button_t3188123007_0_0_0_Types[] = { (&Button_t3188123007_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t3188123007_0_0_0 = { 1, GenInst_Button_t3188123007_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t3370942616_0_0_0_Types[] = { (&Dropdown_t3370942616_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t3370942616_0_0_0 = { 1, GenInst_Dropdown_t3370942616_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t3206453365_0_0_0_Types[] = { (&CanvasRenderer_t3206453365_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t3206453365_0_0_0 = { 1, GenInst_CanvasRenderer_t3206453365_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t3476260122_0_0_0_Types[] = { (&Corner_t3476260122_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t3476260122_0_0_0 = { 1, GenInst_Corner_t3476260122_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t3803614164_0_0_0_Types[] = { (&Axis_t3803614164_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t3803614164_0_0_0 = { 1, GenInst_Axis_t3803614164_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t920180522_0_0_0_Types[] = { (&Constraint_t920180522_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t920180522_0_0_0 = { 1, GenInst_Constraint_t920180522_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t1747690399_0_0_0_Types[] = { (&SubmitEvent_t1747690399_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t1747690399_0_0_0 = { 1, GenInst_SubmitEvent_t1747690399_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t2451064907_0_0_0_Types[] = { (&OnChangeEvent_t2451064907_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2451064907_0_0_0 = { 1, GenInst_OnChangeEvent_t2451064907_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t1213950634_0_0_0_Types[] = { (&OnValidateInput_t1213950634_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1213950634_0_0_0 = { 1, GenInst_OnValidateInput_t1213950634_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t2105357831_0_0_0_Types[] = { (&LayoutElement_t2105357831_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t2105357831_0_0_0 = { 1, GenInst_LayoutElement_t2105357831_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t83369214_0_0_0_Types[] = { (&RectOffset_t83369214_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t83369214_0_0_0 = { 1, GenInst_RectOffset_t83369214_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t3885169356_0_0_0_Types[] = { (&TextAnchor_t3885169356_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t3885169356_0_0_0 = { 1, GenInst_TextAnchor_t3885169356_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t3813953280_0_0_0_Types[] = { (&AnimationTriggers_t3813953280_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3813953280_0_0_0 = { 1, GenInst_AnimationTriggers_t3813953280_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t3267699442_0_0_0_Types[] = { (&Animator_t3267699442_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t3267699442_0_0_0 = { 1, GenInst_Animator_t3267699442_0_0_0_Types };
static const RuntimeType* GenInst_UnityARVideo_t2486736449_0_0_0_Types[] = { (&UnityARVideo_t2486736449_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARVideo_t2486736449_0_0_0 = { 1, GenInst_UnityARVideo_t2486736449_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t2414658676_0_0_0_Types[] = { (&MeshRenderer_t2414658676_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t2414658676_0_0_0 = { 1, GenInst_MeshRenderer_t2414658676_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t3224615444_0_0_0_Types[] = { (&Slider_t3224615444_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t3224615444_0_0_0 = { 1, GenInst_Slider_t3224615444_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t4037952033_0_0_0_Types[] = { (&RawImage_t4037952033_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t4037952033_0_0_0 = { 1, GenInst_RawImage_t4037952033_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t4206798601_0_0_0_Types[] = { (&InputField_t4206798601_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t4206798601_0_0_0 = { 1, GenInst_InputField_t4206798601_0_0_0_Types };
static const RuntimeType* GenInst_BoxSlider_t1119607061_0_0_0_Types[] = { (&BoxSlider_t1119607061_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxSlider_t1119607061_0_0_0 = { 1, GenInst_BoxSlider_t1119607061_0_0_0_Types };
static const RuntimeType* GenInst_UnityARUserAnchorComponent_t2848590843_0_0_0_Types[] = { (&UnityARUserAnchorComponent_t2848590843_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARUserAnchorComponent_t2848590843_0_0_0 = { 1, GenInst_UnityARUserAnchorComponent_t2848590843_0_0_0_Types };
static const RuntimeType* GenInst_serializableFromEditorMessage_t1383667999_0_0_0_Types[] = { (&serializableFromEditorMessage_t1383667999_0_0_0) };
extern const Il2CppGenericInst GenInst_serializableFromEditorMessage_t1383667999_0_0_0 = { 1, GenInst_serializableFromEditorMessage_t1383667999_0_0_0_Types };
static const RuntimeType* GenInst_DontDestroyOnLoad_t2332563878_0_0_0_Types[] = { (&DontDestroyOnLoad_t2332563878_0_0_0) };
extern const Il2CppGenericInst GenInst_DontDestroyOnLoad_t2332563878_0_0_0 = { 1, GenInst_DontDestroyOnLoad_t2332563878_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t1334808991_0_0_0_Types[] = { (&MeshFilter_t1334808991_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t1334808991_0_0_0 = { 1, GenInst_MeshFilter_t1334808991_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0_Types[] = { (&Int32_t499004851_0_0_0), (&Int32_t499004851_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0 = { 2, GenInst_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_CustomAttributeNamedArgument_t3690676406_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t3690676406_0_0_0), (&CustomAttributeNamedArgument_t3690676406_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_CustomAttributeNamedArgument_t3690676406_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_CustomAttributeNamedArgument_t3690676406_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_CustomAttributeTypedArgument_t2525571267_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t2525571267_0_0_0), (&CustomAttributeTypedArgument_t2525571267_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_CustomAttributeTypedArgument_t2525571267_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_CustomAttributeTypedArgument_t2525571267_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2788147849_0_0_0_Color32_t2788147849_0_0_0_Types[] = { (&Color32_t2788147849_0_0_0), (&Color32_t2788147849_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2788147849_0_0_0_Color32_t2788147849_0_0_0 = { 2, GenInst_Color32_t2788147849_0_0_0_Color32_t2788147849_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t463000792_0_0_0_RaycastResult_t463000792_0_0_0_Types[] = { (&RaycastResult_t463000792_0_0_0), (&RaycastResult_t463000792_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t463000792_0_0_0_RaycastResult_t463000792_0_0_0 = { 2, GenInst_RaycastResult_t463000792_0_0_0_RaycastResult_t463000792_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t854420848_0_0_0_UICharInfo_t854420848_0_0_0_Types[] = { (&UICharInfo_t854420848_0_0_0), (&UICharInfo_t854420848_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t854420848_0_0_0_UICharInfo_t854420848_0_0_0 = { 2, GenInst_UICharInfo_t854420848_0_0_0_UICharInfo_t854420848_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t285775551_0_0_0_UILineInfo_t285775551_0_0_0_Types[] = { (&UILineInfo_t285775551_0_0_0), (&UILineInfo_t285775551_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t285775551_0_0_0_UILineInfo_t285775551_0_0_0 = { 2, GenInst_UILineInfo_t285775551_0_0_0_UILineInfo_t285775551_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t4115127231_0_0_0_UIVertex_t4115127231_0_0_0_Types[] = { (&UIVertex_t4115127231_0_0_0), (&UIVertex_t4115127231_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t4115127231_0_0_0_UIVertex_t4115127231_0_0_0 = { 2, GenInst_UIVertex_t4115127231_0_0_0_UIVertex_t4115127231_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t3057062568_0_0_0_Vector2_t3057062568_0_0_0_Types[] = { (&Vector2_t3057062568_0_0_0), (&Vector2_t3057062568_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t3057062568_0_0_0_Vector2_t3057062568_0_0_0 = { 2, GenInst_Vector2_t3057062568_0_0_0_Vector2_t3057062568_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t329709361_0_0_0_Vector3_t329709361_0_0_0_Types[] = { (&Vector3_t329709361_0_0_0), (&Vector3_t329709361_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t329709361_0_0_0_Vector3_t329709361_0_0_0 = { 2, GenInst_Vector3_t329709361_0_0_0_Vector3_t329709361_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t380635127_0_0_0_Vector4_t380635127_0_0_0_Types[] = { (&Vector4_t380635127_0_0_0), (&Vector4_t380635127_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t380635127_0_0_0_Vector4_t380635127_0_0_0 = { 2, GenInst_Vector4_t380635127_0_0_0_Vector4_t380635127_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t1183581528_0_0_0_ARHitTestResult_t1183581528_0_0_0_Types[] = { (&ARHitTestResult_t1183581528_0_0_0), (&ARHitTestResult_t1183581528_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t1183581528_0_0_0_ARHitTestResult_t1183581528_0_0_0 = { 2, GenInst_ARHitTestResult_t1183581528_0_0_0_ARHitTestResult_t1183581528_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1687969195_0_0_0_KeyValuePair_2_t1687969195_0_0_0_Types[] = { (&KeyValuePair_2_t1687969195_0_0_0), (&KeyValuePair_2_t1687969195_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1687969195_0_0_0_KeyValuePair_2_t1687969195_0_0_0 = { 2, GenInst_KeyValuePair_2_t1687969195_0_0_0_KeyValuePair_2_t1687969195_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1687969195_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1687969195_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1687969195_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1687969195_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t420362637_0_0_0_KeyValuePair_2_t420362637_0_0_0_Types[] = { (&KeyValuePair_2_t420362637_0_0_0), (&KeyValuePair_2_t420362637_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t420362637_0_0_0_KeyValuePair_2_t420362637_0_0_0 = { 2, GenInst_KeyValuePair_2_t420362637_0_0_0_KeyValuePair_2_t420362637_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t420362637_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t420362637_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t420362637_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t420362637_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0_Types[] = { (&Boolean_t569405246_0_0_0), (&Boolean_t569405246_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0 = { 2, GenInst_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4180671908_0_0_0_KeyValuePair_2_t4180671908_0_0_0_Types[] = { (&KeyValuePair_2_t4180671908_0_0_0), (&KeyValuePair_2_t4180671908_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180671908_0_0_0_KeyValuePair_2_t4180671908_0_0_0 = { 2, GenInst_KeyValuePair_2_t4180671908_0_0_0_KeyValuePair_2_t4180671908_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4180671908_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t4180671908_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180671908_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4180671908_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4110271513_0_0_0_KeyValuePair_2_t4110271513_0_0_0_Types[] = { (&KeyValuePair_2_t4110271513_0_0_0), (&KeyValuePair_2_t4110271513_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4110271513_0_0_0_KeyValuePair_2_t4110271513_0_0_0 = { 2, GenInst_KeyValuePair_2_t4110271513_0_0_0_KeyValuePair_2_t4110271513_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4110271513_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t4110271513_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4110271513_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4110271513_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3986364620_0_0_0_KeyValuePair_2_t3986364620_0_0_0_Types[] = { (&KeyValuePair_2_t3986364620_0_0_0), (&KeyValuePair_2_t3986364620_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3986364620_0_0_0_KeyValuePair_2_t3986364620_0_0_0 = { 2, GenInst_KeyValuePair_2_t3986364620_0_0_0_KeyValuePair_2_t3986364620_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3986364620_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3986364620_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3986364620_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3986364620_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1179652112_0_0_0_KeyValuePair_2_t1179652112_0_0_0_Types[] = { (&KeyValuePair_2_t1179652112_0_0_0), (&KeyValuePair_2_t1179652112_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1179652112_0_0_0_KeyValuePair_2_t1179652112_0_0_0 = { 2, GenInst_KeyValuePair_2_t1179652112_0_0_0_KeyValuePair_2_t1179652112_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1179652112_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1179652112_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1179652112_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1179652112_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1863352746_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Single_t1863352746_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1863352746_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Single_t1863352746_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[619] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t499004851_0_0_0,
	&GenInst_Char_t2157028800_0_0_0,
	&GenInst_Int64_t3070791913_0_0_0,
	&GenInst_UInt32_t3311932136_0_0_0,
	&GenInst_UInt64_t96233451_0_0_0,
	&GenInst_Byte_t2815932036_0_0_0,
	&GenInst_SByte_t1268219320_0_0_0,
	&GenInst_Int16_t508617419_0_0_0,
	&GenInst_UInt16_t3440013504_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t435918070_0_0_0,
	&GenInst_IComparable_t1276860510_0_0_0,
	&GenInst_IEnumerable_t2515817764_0_0_0,
	&GenInst_ICloneable_t94813995_0_0_0,
	&GenInst_IComparable_1_t3371938004_0_0_0,
	&GenInst_IEquatable_1_t1378251015_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t1617407174_0_0_0,
	&GenInst__Type_t271421363_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t3385495939_0_0_0,
	&GenInst__MemberInfo_t3153976712_0_0_0,
	&GenInst_Double_t2078998952_0_0_0,
	&GenInst_Single_t1863352746_0_0_0,
	&GenInst_Decimal_t3984207949_0_0_0,
	&GenInst_Boolean_t569405246_0_0_0,
	&GenInst_Delegate_t1563516729_0_0_0,
	&GenInst_ISerializable_t1949570287_0_0_0,
	&GenInst_ParameterInfo_t2372523953_0_0_0,
	&GenInst__ParameterInfo_t3363811512_0_0_0,
	&GenInst_ParameterModifier_t1946944212_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t494648311_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t506103983_0_0_0,
	&GenInst_MethodBase_t3535115348_0_0_0,
	&GenInst__MethodBase_t2512713776_0_0_0,
	&GenInst_ConstructorInfo_t1575920019_0_0_0,
	&GenInst__ConstructorInfo_t598873286_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t550209432_0_0_0,
	&GenInst_TailoringInfo_t310506661_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_KeyValuePair_2_t4110271513_0_0_0,
	&GenInst_Link_t2671721688_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t4110271513_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t2910963427_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2910963427_0_0_0,
	&GenInst_Contraction_t350025005_0_0_0,
	&GenInst_Level2Map_t3911096064_0_0_0,
	&GenInst_BigInteger_t3167099825_0_0_0,
	&GenInst_KeySizes_t652359446_0_0_0,
	&GenInst_KeyValuePair_2_t3986364620_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3986364620_0_0_0,
	&GenInst_Slot_t2203870133_0_0_0,
	&GenInst_Slot_t2314815295_0_0_0,
	&GenInst_StackFrame_t795761381_0_0_0,
	&GenInst_Calendar_t2104464680_0_0_0,
	&GenInst_ModuleBuilder_t353271061_0_0_0,
	&GenInst__ModuleBuilder_t55528917_0_0_0,
	&GenInst_Module_t829555369_0_0_0,
	&GenInst__Module_t4246123975_0_0_0,
	&GenInst_CustomAttributeBuilder_t2861075622_0_0_0,
	&GenInst__CustomAttributeBuilder_t4202319907_0_0_0,
	&GenInst_MonoResource_t1625298336_0_0_0,
	&GenInst_MonoWin32Resource_t1452687774_0_0_0,
	&GenInst_RefEmitPermissionSet_t3060109815_0_0_0,
	&GenInst_ParameterBuilder_t1450031896_0_0_0,
	&GenInst__ParameterBuilder_t1925184101_0_0_0,
	&GenInst_TypeU5BU5D_t1460120061_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t1903847002_0_0_0,
	&GenInst_IList_t2170747348_0_0_0,
	&GenInst_IList_1_t4171725588_0_0_0,
	&GenInst_ICollection_1_t1991481984_0_0_0,
	&GenInst_IEnumerable_1_t845252796_0_0_0,
	&GenInst_IList_1_t975384358_0_0_0,
	&GenInst_ICollection_1_t3090108050_0_0_0,
	&GenInst_IEnumerable_1_t1943878862_0_0_0,
	&GenInst_IList_1_t3924365843_0_0_0,
	&GenInst_ICollection_1_t1744122239_0_0_0,
	&GenInst_IEnumerable_1_t597893051_0_0_0,
	&GenInst_IList_1_t2241863952_0_0_0,
	&GenInst_ICollection_1_t61620348_0_0_0,
	&GenInst_IEnumerable_1_t3210358456_0_0_0,
	&GenInst_IList_1_t2743473123_0_0_0,
	&GenInst_ICollection_1_t563229519_0_0_0,
	&GenInst_IEnumerable_1_t3711967627_0_0_0,
	&GenInst_IList_1_t2511953896_0_0_0,
	&GenInst_ICollection_1_t331710292_0_0_0,
	&GenInst_IEnumerable_1_t3480448400_0_0_0,
	&GenInst_IList_1_t4028042438_0_0_0,
	&GenInst_ICollection_1_t1847798834_0_0_0,
	&GenInst_IEnumerable_1_t701569646_0_0_0,
	&GenInst_LocalBuilder_t2129824983_0_0_0,
	&GenInst__LocalBuilder_t2362914606_0_0_0,
	&GenInst_LocalVariableInfo_t4058697059_0_0_0,
	&GenInst_ILTokenInfo_t1423368019_0_0_0,
	&GenInst_LabelData_t3746295612_0_0_0,
	&GenInst_LabelFixup_t362107953_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1917656400_0_0_0,
	&GenInst_TypeBuilder_t4094040294_0_0_0,
	&GenInst__TypeBuilder_t3514614725_0_0_0,
	&GenInst_MethodBuilder_t2052015890_0_0_0,
	&GenInst__MethodBuilder_t3265230778_0_0_0,
	&GenInst_FieldBuilder_t375034881_0_0_0,
	&GenInst__FieldBuilder_t956530906_0_0_0,
	&GenInst_ConstructorBuilder_t2019506662_0_0_0,
	&GenInst__ConstructorBuilder_t1009537191_0_0_0,
	&GenInst_PropertyBuilder_t2696548899_0_0_0,
	&GenInst__PropertyBuilder_t131710825_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t4092521575_0_0_0,
	&GenInst_EventBuilder_t814990589_0_0_0,
	&GenInst__EventBuilder_t2566635139_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0,
	&GenInst_CustomAttributeData_t612787047_0_0_0,
	&GenInst_ResourceInfo_t3106642170_0_0_0,
	&GenInst_ResourceCacheItem_t3011921618_0_0_0,
	&GenInst_IContextProperty_t3459958298_0_0_0,
	&GenInst_Header_t3846250252_0_0_0,
	&GenInst_ITrackingHandler_t1663093004_0_0_0,
	&GenInst_IContextAttribute_t3303384032_0_0_0,
	&GenInst_DateTime_t972933412_0_0_0,
	&GenInst_TimeSpan_t457147580_0_0_0,
	&GenInst_TypeTag_t1982705342_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2371975271_0_0_0,
	&GenInst_IBuiltInEvidence_t3244916114_0_0_0,
	&GenInst_IIdentityPermissionFactory_t574536121_0_0_0,
	&GenInst_WaitHandle_t697867384_0_0_0,
	&GenInst_IDisposable_t3363289168_0_0_0,
	&GenInst_MarshalByRefObject_t532690895_0_0_0,
	&GenInst_DateTimeOffset_t4244044333_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1723234366_0_0_0,
	&GenInst_BigInteger_t3167099826_0_0_0,
	&GenInst_ByteU5BU5D_t3287329517_0_0_0,
	&GenInst_IList_1_t2173909220_0_0_0,
	&GenInst_ICollection_1_t4288632912_0_0_0,
	&GenInst_IEnumerable_1_t3142403724_0_0_0,
	&GenInst_X509Certificate_t934499855_0_0_0,
	&GenInst_IDeserializationCallback_t3407183497_0_0_0,
	&GenInst_ClientCertificateType_t1976286434_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_KeyValuePair_2_t4180671908_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t4180671908_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t2981363822_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t569405246_0_0_0_KeyValuePair_2_t2981363822_0_0_0,
	&GenInst_X509ChainStatus_t3509631940_0_0_0,
	&GenInst_Capture_t3723888528_0_0_0,
	&GenInst_Group_t4116754232_0_0_0,
	&GenInst_Mark_t3700292706_0_0_0,
	&GenInst_UriScheme_t1574639958_0_0_0,
	&GenInst_Link_t2401875721_0_0_0,
	&GenInst_AsyncOperation_t1227466744_0_0_0,
	&GenInst_Camera_t989002943_0_0_0,
	&GenInst_Behaviour_t2441856611_0_0_0,
	&GenInst_Component_t789413749_0_0_0,
	&GenInst_Object_t1970767703_0_0_0,
	&GenInst_Display_t882507400_0_0_0,
	&GenInst_SphericalHarmonicsL2_t298540216_0_0_0,
	&GenInst_Keyframe_t1682423392_0_0_0,
	&GenInst_Vector3_t329709361_0_0_0,
	&GenInst_Vector4_t380635127_0_0_0,
	&GenInst_Vector2_t3057062568_0_0_0,
	&GenInst_Color32_t2788147849_0_0_0,
	&GenInst_Playable_t348555058_0_0_0,
	&GenInst_PlayableOutput_t1141860262_0_0_0,
	&GenInst_Scene_t548444562_0_0_0_LoadSceneMode_t2763863609_0_0_0,
	&GenInst_Scene_t548444562_0_0_0,
	&GenInst_Scene_t548444562_0_0_0_Scene_t548444562_0_0_0,
	&GenInst_SpriteAtlas_t3138271588_0_0_0,
	&GenInst_DisallowMultipleComponent_t1081793821_0_0_0,
	&GenInst_Attribute_t3852256153_0_0_0,
	&GenInst__Attribute_t2807569607_0_0_0,
	&GenInst_ExecuteInEditMode_t814488931_0_0_0,
	&GenInst_RequireComponent_t4152270991_0_0_0,
	&GenInst_HitInfo_t214434488_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t1257714207_0_0_0,
	&GenInst_BaseInvokableCall_t3782853021_0_0_0,
	&GenInst_WorkRequest_t4243437884_0_0_0,
	&GenInst_PlayableBinding_t181640494_0_0_0,
	&GenInst_MessageEventArgs_t2086846075_0_0_0,
	&GenInst_MessageTypeSubscribers_t3285514618_0_0_0,
	&GenInst_MessageTypeSubscribers_t3285514618_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t420362637_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t420362637_0_0_0,
	&GenInst_WeakReference_t1069295502_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t1114560181_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1069295502_0_0_0_KeyValuePair_2_t1114560181_0_0_0,
	&GenInst_Rigidbody2D_t3506664974_0_0_0,
	&GenInst_Font_t801544967_0_0_0,
	&GenInst_UIVertex_t4115127231_0_0_0,
	&GenInst_UICharInfo_t854420848_0_0_0,
	&GenInst_UILineInfo_t285775551_0_0_0,
	&GenInst_AnimationClipPlayable_t3277800016_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t3926483291_0_0_0,
	&GenInst_AnimationMixerPlayable_t1623951654_0_0_0,
	&GenInst_AnimationOffsetPlayable_t2049487209_0_0_0,
	&GenInst_AnimatorControllerPlayable_t2116071640_0_0_0,
	&GenInst_AudioSpatializerExtensionDefinition_t1514572524_0_0_0,
	&GenInst_AudioAmbisonicExtensionDefinition_t3557186147_0_0_0,
	&GenInst_AudioSourceExtension_t2588224584_0_0_0,
	&GenInst_ScriptableObject_t2962125979_0_0_0,
	&GenInst_AudioMixerPlayable_t1613331552_0_0_0,
	&GenInst_AudioClipPlayable_t765244045_0_0_0,
	&GenInst_Boolean_t569405246_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t569405246_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t3476369140_0_0_0,
	&GenInst_IAchievementDescription_t2434259086_0_0_0,
	&GenInst_UserProfile_t3121054929_0_0_0,
	&GenInst_IUserProfile_t2190722809_0_0_0,
	&GenInst_GcLeaderboard_t2259580636_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t958143867_0_0_0,
	&GenInst_IAchievementU5BU5D_t3241716440_0_0_0,
	&GenInst_IAchievement_t3151655605_0_0_0,
	&GenInst_GcAchievementData_t2938297544_0_0_0,
	&GenInst_Achievement_t3727036008_0_0_0,
	&GenInst_IScoreU5BU5D_t912747562_0_0_0,
	&GenInst_IScore_t3344695883_0_0_0,
	&GenInst_GcScoreData_t3653438230_0_0_0,
	&GenInst_Score_t2147121913_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3279866116_0_0_0,
	&GenInst_GUILayoutOption_t3076073054_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1687969195_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1687969195_0_0_0,
	&GenInst_LayoutCache_t3924992466_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t942896407_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_LayoutCache_t3924992466_0_0_0_KeyValuePair_2_t942896407_0_0_0,
	&GenInst_GUILayoutEntry_t2655444992_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_IntPtr_t_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_Exception_t2123675094_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_GUIStyle_t4058570518_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t2175561798_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t4058570518_0_0_0_KeyValuePair_2_t2175561798_0_0_0,
	&GenInst_Particle_t651781316_0_0_0,
	&GenInst_RaycastHit_t2851673566_0_0_0,
	&GenInst_ContactPoint_t2838489305_0_0_0,
	&GenInst_EventSystem_t1635553419_0_0_0,
	&GenInst_UIBehaviour_t2076195789_0_0_0,
	&GenInst_MonoBehaviour_t3829899482_0_0_0,
	&GenInst_BaseInputModule_t3513845355_0_0_0,
	&GenInst_RaycastResult_t463000792_0_0_0,
	&GenInst_IDeselectHandler_t1079439781_0_0_0,
	&GenInst_IEventSystemHandler_t2072879830_0_0_0,
	&GenInst_List_1_t1883330244_0_0_0,
	&GenInst_List_1_t185548372_0_0_0,
	&GenInst_List_1_t599864163_0_0_0,
	&GenInst_ISelectHandler_t287548082_0_0_0,
	&GenInst_BaseRaycaster_t2347538179_0_0_0,
	&GenInst_Entry_t4245925096_0_0_0,
	&GenInst_BaseEventData_t1619969158_0_0_0,
	&GenInst_IPointerEnterHandler_t2829503976_0_0_0,
	&GenInst_IPointerExitHandler_t2553928545_0_0_0,
	&GenInst_IPointerDownHandler_t3783019842_0_0_0,
	&GenInst_IPointerUpHandler_t2582372469_0_0_0,
	&GenInst_IPointerClickHandler_t3256793649_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t1510685835_0_0_0,
	&GenInst_IBeginDragHandler_t253960210_0_0_0,
	&GenInst_IDragHandler_t3031146797_0_0_0,
	&GenInst_IEndDragHandler_t2378561395_0_0_0,
	&GenInst_IDropHandler_t26677246_0_0_0,
	&GenInst_IScrollHandler_t2953857311_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3805117774_0_0_0,
	&GenInst_IMoveHandler_t3674718662_0_0_0,
	&GenInst_ISubmitHandler_t160027500_0_0_0,
	&GenInst_ICancelHandler_t1737091460_0_0_0,
	&GenInst_Transform_t532597831_0_0_0,
	&GenInst_GameObject_t1318052361_0_0_0,
	&GenInst_BaseInput_t2641534846_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0,
	&GenInst_PointerEventData_t2787773033_0_0_0,
	&GenInst_AbstractEventData_t40423571_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t4100644270_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_KeyValuePair_2_t4100644270_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_PointerEventData_t2787773033_0_0_0_PointerEventData_t2787773033_0_0_0,
	&GenInst_ButtonState_t1610769254_0_0_0,
	&GenInst_RaycastHit2D_t266689378_0_0_0,
	&GenInst_Color_t460381780_0_0_0,
	&GenInst_ICanvasElement_t2902062777_0_0_0,
	&GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_ColorBlock_t266244752_0_0_0,
	&GenInst_OptionData_t1608351530_0_0_0,
	&GenInst_DropdownItem_t3503496286_0_0_0,
	&GenInst_FloatTween_t3352968507_0_0_0,
	&GenInst_Sprite_t3012664695_0_0_0,
	&GenInst_Canvas_t852644723_0_0_0,
	&GenInst_List_1_t663095137_0_0_0,
	&GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0,
	&GenInst_Text_t866332725_0_0_0,
	&GenInst_Link_t2893110488_0_0_0,
	&GenInst_ILayoutElement_t632884531_0_0_0,
	&GenInst_MaskableGraphic_t92248932_0_0_0,
	&GenInst_IClippable_t3700685589_0_0_0,
	&GenInst_IMaskable_t3175584775_0_0_0,
	&GenInst_IMaterialModifier_t2216105711_0_0_0,
	&GenInst_Graphic_t1165931317_0_0_0,
	&GenInst_HashSet_1_t2686032385_0_0_0,
	&GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t3484435490_0_0_0,
	&GenInst_Font_t801544967_0_0_0_HashSet_1_t2686032385_0_0_0_KeyValuePair_2_t3484435490_0_0_0,
	&GenInst_ColorTween_t1361506897_0_0_0,
	&GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0,
	&GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_IndexedSet_1_t2847635963_0_0_0,
	&GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t3364176000_0_0_0,
	&GenInst_Canvas_t852644723_0_0_0_IndexedSet_1_t2847635963_0_0_0_KeyValuePair_2_t3364176000_0_0_0,
	&GenInst_KeyValuePair_2_t101362478_0_0_0,
	&GenInst_Graphic_t1165931317_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t101362478_0_0_0,
	&GenInst_KeyValuePair_2_t3961451802_0_0_0,
	&GenInst_ICanvasElement_t2902062777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t3961451802_0_0_0,
	&GenInst_Type_t74284121_0_0_0,
	&GenInst_FillMethod_t4202801712_0_0_0,
	&GenInst_ContentType_t59827793_0_0_0,
	&GenInst_LineType_t1398395467_0_0_0,
	&GenInst_InputType_t2454186138_0_0_0,
	&GenInst_TouchScreenKeyboardType_t2115689981_0_0_0,
	&GenInst_CharacterValidation_t2898689831_0_0_0,
	&GenInst_Mask_t4149322243_0_0_0,
	&GenInst_ICanvasRaycastFilter_t3610902803_0_0_0,
	&GenInst_List_1_t3959772657_0_0_0,
	&GenInst_RectMask2D_t1299097536_0_0_0,
	&GenInst_IClipper_t4258280777_0_0_0,
	&GenInst_List_1_t1109547950_0_0_0,
	&GenInst_Navigation_t4246164788_0_0_0,
	&GenInst_Link_t1432496056_0_0_0,
	&GenInst_Direction_t2752781413_0_0_0,
	&GenInst_Selectable_t199987819_0_0_0,
	&GenInst_Transition_t3336380680_0_0_0,
	&GenInst_SpriteState_t2305809087_0_0_0,
	&GenInst_CanvasGroup_t370922105_0_0_0,
	&GenInst_Direction_t571314014_0_0_0,
	&GenInst_MatEntry_t1253440912_0_0_0,
	&GenInst_Toggle_t3640703094_0_0_0,
	&GenInst_Toggle_t3640703094_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t2141035082_0_0_0,
	&GenInst_IClipper_t4258280777_0_0_0_Int32_t499004851_0_0_0_KeyValuePair_2_t2141035082_0_0_0,
	&GenInst_AspectMode_t2023416516_0_0_0,
	&GenInst_FitMode_t3728304995_0_0_0,
	&GenInst_RectTransform_t15861704_0_0_0,
	&GenInst_LayoutRebuilder_t2374704703_0_0_0,
	&GenInst_ILayoutElement_t632884531_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_List_1_t140159775_0_0_0,
	&GenInst_List_1_t2598598263_0_0_0,
	&GenInst_List_1_t2867512982_0_0_0,
	&GenInst_List_1_t191085541_0_0_0,
	&GenInst_List_1_t309455265_0_0_0,
	&GenInst_List_1_t3925577645_0_0_0,
	&GenInst_Link_t1967700103_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_KeyValuePair_2_t1179652112_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t1179652112_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t4275311322_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1863352746_0_0_0_KeyValuePair_2_t4275311322_0_0_0,
	&GenInst_ARHitTestResult_t1183581528_0_0_0,
	&GenInst_ARHitTestResultType_t3376578133_0_0_0,
	&GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_Single_t1863352746_0_0_0_Single_t1863352746_0_0_0,
	&GenInst_ParticleSystem_t1097441456_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0,
	&GenInst_ARPlaneAnchorGameObject_t2270868264_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_KeyValuePair_2_t387859544_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2270868264_0_0_0_KeyValuePair_2_t387859544_0_0_0,
	&GenInst_UnityARSessionRunOption_t2827180039_0_0_0,
	&GenInst_UnityARAlignment_t2394317114_0_0_0,
	&GenInst_UnityARPlaneDetection_t4236545235_0_0_0,
	&GenInst_Light_t3883945566_0_0_0,
	&GenInst_IEnumerable_1_t536933947_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4253116400_gp_0_0_0_0,
	&GenInst_Array_Sort_m734803740_gp_0_0_0_0_Array_Sort_m734803740_gp_0_0_0_0,
	&GenInst_Array_Sort_m787042007_gp_0_0_0_0_Array_Sort_m787042007_gp_1_0_0_0,
	&GenInst_Array_Sort_m3596954745_gp_0_0_0_0,
	&GenInst_Array_Sort_m3596954745_gp_0_0_0_0_Array_Sort_m3596954745_gp_0_0_0_0,
	&GenInst_Array_Sort_m2095751586_gp_0_0_0_0,
	&GenInst_Array_Sort_m2095751586_gp_0_0_0_0_Array_Sort_m2095751586_gp_1_0_0_0,
	&GenInst_Array_Sort_m925910360_gp_0_0_0_0_Array_Sort_m925910360_gp_0_0_0_0,
	&GenInst_Array_Sort_m2097743790_gp_0_0_0_0_Array_Sort_m2097743790_gp_1_0_0_0,
	&GenInst_Array_Sort_m3972326822_gp_0_0_0_0,
	&GenInst_Array_Sort_m3972326822_gp_0_0_0_0_Array_Sort_m3972326822_gp_0_0_0_0,
	&GenInst_Array_Sort_m2038482742_gp_0_0_0_0,
	&GenInst_Array_Sort_m2038482742_gp_1_0_0_0,
	&GenInst_Array_Sort_m2038482742_gp_0_0_0_0_Array_Sort_m2038482742_gp_1_0_0_0,
	&GenInst_Array_Sort_m4074792876_gp_0_0_0_0,
	&GenInst_Array_Sort_m1131242284_gp_0_0_0_0,
	&GenInst_Array_qsort_m267516494_gp_0_0_0_0,
	&GenInst_Array_qsort_m267516494_gp_0_0_0_0_Array_qsort_m267516494_gp_1_0_0_0,
	&GenInst_Array_compare_m2330141893_gp_0_0_0_0,
	&GenInst_Array_qsort_m1179209724_gp_0_0_0_0,
	&GenInst_Array_Resize_m2794471509_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2758149500_gp_0_0_0_0,
	&GenInst_Array_ForEach_m2910560811_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m3816593202_gp_0_0_0_0_Array_ConvertAll_m3816593202_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m4047168095_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m2722629763_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1417763420_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3608062671_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3859049537_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1091091128_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3933600019_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m915595758_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3668675172_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3050703072_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1079732850_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3254707976_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m43705925_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3752110915_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1647032180_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1323951861_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1602332273_gp_0_0_0_0,
	&GenInst_Array_Exists_m1616247547_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m443996534_gp_0_0_0_0,
	&GenInst_Array_Find_m2550374671_gp_0_0_0_0,
	&GenInst_Array_FindLast_m2838405935_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t4134619510_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t4011792957_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t420612394_gp_0_0_0_0,
	&GenInst_IList_1_t555026515_gp_0_0_0_0,
	&GenInst_ICollection_1_t1055842719_gp_0_0_0_0,
	&GenInst_Nullable_1_t3261863354_gp_0_0_0_0,
	&GenInst_Comparer_1_t4288707744_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4214145415_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1346033315_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t228248057_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3703788233_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m205750437_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_ShimEnumerator_t3171201328_gp_0_0_0_0_ShimEnumerator_t3171201328_gp_1_0_0_0,
	&GenInst_Enumerator_t3672918140_gp_0_0_0_0_Enumerator_t3672918140_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1695844585_0_0_0,
	&GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0,
	&GenInst_ValueCollection_t2778072946_gp_1_0_0_0,
	&GenInst_Enumerator_t936271527_gp_0_0_0_0_Enumerator_t936271527_gp_1_0_0_0,
	&GenInst_Enumerator_t936271527_gp_1_0_0_0,
	&GenInst_ValueCollection_t2778072946_gp_0_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0,
	&GenInst_ValueCollection_t2778072946_gp_1_0_0_0_ValueCollection_t2778072946_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t2422360636_0_0_0_DictionaryEntry_t2422360636_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_0_0_0_0_Dictionary_2_t4241968728_gp_1_0_0_0_KeyValuePair_2_t228248057_0_0_0,
	&GenInst_KeyValuePair_2_t228248057_0_0_0_KeyValuePair_2_t228248057_0_0_0,
	&GenInst_Dictionary_2_t4241968728_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2843542286_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4044621341_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2574266403_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t567457625_0_0_0,
	&GenInst_IDictionary_2_t3270403520_gp_0_0_0_0_IDictionary_2_t3270403520_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t694182271_gp_0_0_0_0_KeyValuePair_2_t694182271_gp_1_0_0_0,
	&GenInst_List_1_t1825915537_gp_0_0_0_0,
	&GenInst_Enumerator_t1634006062_gp_0_0_0_0,
	&GenInst_Collection_1_t2715885535_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2737108554_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1101529066_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1101529066_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m3745044644_gp_0_0_0_0,
	&GenInst_Queue_1_t1129464372_gp_0_0_0_0,
	&GenInst_Enumerator_t63438097_gp_0_0_0_0,
	&GenInst_Stack_1_t2792833190_gp_0_0_0_0,
	&GenInst_Enumerator_t4232829344_gp_0_0_0_0,
	&GenInst_HashSet_1_t1434642847_gp_0_0_0_0,
	&GenInst_Enumerator_t3119472212_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3200753757_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2805250219_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m1169226985_gp_0_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m667756508_gp_0_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_Enumerable_ToList_m2871797791_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2312884896_gp_0_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3636380240_gp_0_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3433939259_gp_0_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_Component_GetComponentInChildren_m1455327788_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1613593302_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3291453366_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m327293654_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2020898487_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m4081353059_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m2403101899_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m3335520185_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m2295395095_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m2455478835_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m2037798547_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3381760768_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m370193599_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m613625000_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m2568723115_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m895778613_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t2384605844_gp_0_0_0_0,
	&GenInst_UnityAction_1_t880801182_0_0_0,
	&GenInst_InvokableCall_2_t686556727_gp_0_0_0_0_InvokableCall_2_t686556727_gp_1_0_0_0,
	&GenInst_UnityAction_2_t3720805410_0_0_0,
	&GenInst_InvokableCall_2_t686556727_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t686556727_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0_InvokableCall_3_t1114138605_gp_1_0_0_0_InvokableCall_3_t1114138605_gp_2_0_0_0,
	&GenInst_UnityAction_3_t2206700587_0_0_0,
	&GenInst_InvokableCall_3_t1114138605_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t1114138605_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1114138605_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0_InvokableCall_4_t3841286312_gp_1_0_0_0_InvokableCall_4_t3841286312_gp_2_0_0_0_InvokableCall_4_t3841286312_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3841286312_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3841286312_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3841286312_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3841286312_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t269288053_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t1144529152_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4235607108_gp_0_0_0_0_UnityEvent_2_t4235607108_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t1018889849_gp_0_0_0_0_UnityEvent_3_t1018889849_gp_1_0_0_0_UnityEvent_3_t1018889849_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t963784060_gp_0_0_0_0_UnityEvent_4_t963784060_gp_1_0_0_0_UnityEvent_4_t963784060_gp_2_0_0_0_UnityEvent_4_t963784060_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m871388233_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1878598892_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m218033630_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3953271158_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m1357021089_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2880223937_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m2817621765_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m3152988929_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1694099753_gp_0_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_ListPool_1_t1470686486_gp_0_0_0_0,
	&GenInst_List_1_t1928769349_0_0_0,
	&GenInst_ObjectPool_1_t2798048145_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t2771548650_0_0_0,
	&GenInst_PlayerConnection_t781534341_0_0_0,
	&GenInst_GUILayer_t2694382279_0_0_0,
	&GenInst_AxisEventData_t2438176915_0_0_0,
	&GenInst_SpriteRenderer_t3043448750_0_0_0,
	&GenInst_GraphicRaycaster_t3324891182_0_0_0,
	&GenInst_Image_t61690498_0_0_0,
	&GenInst_Button_t3188123007_0_0_0,
	&GenInst_Dropdown_t3370942616_0_0_0,
	&GenInst_CanvasRenderer_t3206453365_0_0_0,
	&GenInst_Corner_t3476260122_0_0_0,
	&GenInst_Axis_t3803614164_0_0_0,
	&GenInst_Constraint_t920180522_0_0_0,
	&GenInst_SubmitEvent_t1747690399_0_0_0,
	&GenInst_OnChangeEvent_t2451064907_0_0_0,
	&GenInst_OnValidateInput_t1213950634_0_0_0,
	&GenInst_LayoutElement_t2105357831_0_0_0,
	&GenInst_RectOffset_t83369214_0_0_0,
	&GenInst_TextAnchor_t3885169356_0_0_0,
	&GenInst_AnimationTriggers_t3813953280_0_0_0,
	&GenInst_Animator_t3267699442_0_0_0,
	&GenInst_UnityARVideo_t2486736449_0_0_0,
	&GenInst_MeshRenderer_t2414658676_0_0_0,
	&GenInst_Slider_t3224615444_0_0_0,
	&GenInst_RawImage_t4037952033_0_0_0,
	&GenInst_InputField_t4206798601_0_0_0,
	&GenInst_BoxSlider_t1119607061_0_0_0,
	&GenInst_UnityARUserAnchorComponent_t2848590843_0_0_0,
	&GenInst_serializableFromEditorMessage_t1383667999_0_0_0,
	&GenInst_DontDestroyOnLoad_t2332563878_0_0_0,
	&GenInst_MeshFilter_t1334808991_0_0_0,
	&GenInst_Int32_t499004851_0_0_0_Int32_t499004851_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3690676406_0_0_0_CustomAttributeNamedArgument_t3690676406_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2525571267_0_0_0_CustomAttributeTypedArgument_t2525571267_0_0_0,
	&GenInst_Color32_t2788147849_0_0_0_Color32_t2788147849_0_0_0,
	&GenInst_RaycastResult_t463000792_0_0_0_RaycastResult_t463000792_0_0_0,
	&GenInst_UICharInfo_t854420848_0_0_0_UICharInfo_t854420848_0_0_0,
	&GenInst_UILineInfo_t285775551_0_0_0_UILineInfo_t285775551_0_0_0,
	&GenInst_UIVertex_t4115127231_0_0_0_UIVertex_t4115127231_0_0_0,
	&GenInst_Vector2_t3057062568_0_0_0_Vector2_t3057062568_0_0_0,
	&GenInst_Vector3_t329709361_0_0_0_Vector3_t329709361_0_0_0,
	&GenInst_Vector4_t380635127_0_0_0_Vector4_t380635127_0_0_0,
	&GenInst_ARHitTestResult_t1183581528_0_0_0_ARHitTestResult_t1183581528_0_0_0,
	&GenInst_KeyValuePair_2_t1687969195_0_0_0_KeyValuePair_2_t1687969195_0_0_0,
	&GenInst_KeyValuePair_2_t1687969195_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t420362637_0_0_0_KeyValuePair_2_t420362637_0_0_0,
	&GenInst_KeyValuePair_2_t420362637_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t569405246_0_0_0_Boolean_t569405246_0_0_0,
	&GenInst_KeyValuePair_2_t4180671908_0_0_0_KeyValuePair_2_t4180671908_0_0_0,
	&GenInst_KeyValuePair_2_t4180671908_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t4110271513_0_0_0_KeyValuePair_2_t4110271513_0_0_0,
	&GenInst_KeyValuePair_2_t4110271513_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3986364620_0_0_0_KeyValuePair_2_t3986364620_0_0_0,
	&GenInst_KeyValuePair_2_t3986364620_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1179652112_0_0_0_KeyValuePair_2_t1179652112_0_0_0,
	&GenInst_KeyValuePair_2_t1179652112_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Single_t1863352746_0_0_0_RuntimeObject_0_0_0,
};
